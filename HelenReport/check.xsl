<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output indent="yes"/>
    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
        <root>
            <xsl:apply-templates select="records/record" />
        </root>        
    </xsl:template>
    
    <xsl:template match="record">
        <xsl:choose>
            <xsl:when test="not(exists(authors))">
                <no-author>
                    <xsl:value-of select="escholar-id/text()" />
                </no-author>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>