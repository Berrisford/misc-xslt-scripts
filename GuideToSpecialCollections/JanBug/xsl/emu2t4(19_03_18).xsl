<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <!--<xsl:result-document href="narratives1.xml">-->
        <table name="enarratives">
            <xsl:for-each select="//ARTICLE|//article">
                <xsl:sort select="normalize-space(SECTION[@class='scg-order']/SECTION[@class='scg-content'])"/>
                <xsl:sort select="normalize-space(section[@class='scg-order']/section[@class='scg-content'])"/>
                <GuideEntry>
                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-order']/SECTION[@class='scg-content'])">
                            <order><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(SECTION[@class='scg-order']/SECTION[@class='scg-content']), ']]&gt;')"/></order>                            
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-order']/section[@class='scg-content'])">
                            <order><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(section[@class='scg-order']/section[@class='scg-content']), ']]&gt;')"/></order>                            
                        </xsl:when>
                    </xsl:choose>

                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-title']/SECTION[@class='scg-content'])">
                            <title><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(SECTION[@class='scg-title']/SECTION[@class='scg-content']/H1), ']]&gt;')"/></title>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-title']/section[@class='scg-content'])">
                            <title><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(section[@class='scg-title']/section[@class='scg-content']/h1), ']]&gt;')"/></title>
                        </xsl:when>
                    </xsl:choose>
                    
                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-daterange']/SECTION[@class='scg-content'])">
                            <DateRange><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(SECTION[@class='scg-daterange']/SECTION[@class='scg-content']), ']]&gt;')"/></DateRange>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-daterange']/section[@class='scg-content'])">
                            <DateRange><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(section[@class='scg-daterange']/section[@class='scg-content']), ']]&gt;')"/></DateRange>
                        </xsl:when>
                    </xsl:choose>
                    
                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-medium']/SECTION[@class='scg-content'])">
                            <Medium><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(SECTION[@class='scg-medium']/SECTION[@class='scg-content']), ']]&gt;')"/></Medium>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-medium']/section[@class='scg-content'])">
                            <Medium><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(section[@class='scg-medium']/section[@class='scg-content']), ']]&gt;')"/></Medium>
                        </xsl:when>
                    </xsl:choose>

                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-number-of-items']/SECTION[@class='scg-content'])">
                            <NumberofItems><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(SECTION[@class='scg-number-of-items']/SECTION[@class='scg-content']), ']]&gt;')"/></NumberofItems>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-number-of-items']/section[@class='scg-content'])">
                            <NumberofItems><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(section[@class='scg-number-of-items']/section[@class='scg-content']), ']]&gt;')"/></NumberofItems>
                        </xsl:when>
                    </xsl:choose>

                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-description']/SECTION[@class='scg-content'])">
                            <Description><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="SECTION[@class='scg-description']/SECTION[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></Description>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-description']/section[@class='scg-content'])">
                            <Description><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="section[@class='scg-description']/section[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></Description>
                        </xsl:when>
                    </xsl:choose>
                    
                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-see-also']/SECTION[@class='scg-content'])">
                            <See_Also><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="SECTION[@class='scg-see-also']/SECTION[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></See_Also>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-see-also']/section[@class='scg-content'])">
                            <See_Also><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="section[@class='scg-see-also']/section[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></See_Also>
                        </xsl:when>
                    </xsl:choose>
                    
                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-further-info']/SECTION[@class='scg-content'])">
                            <FurtherInfo><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="SECTION[@class='scg-further-info']/SECTION[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></FurtherInfo>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-further-info']/section[@class='scg-content'])">
                            <FurtherInfo><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="section[@class='scg-further-info']/section[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></FurtherInfo>
                        </xsl:when>
                    </xsl:choose>

                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-other-resources']/SECTION[@class='scg-content'])">
                            <OtherResources><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="SECTION[@class='scg-other-resources']/SECTION[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></OtherResources>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-other-resources']/section[@class='scg-content'])">
                            <OtherResources><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="section[@class='scg-other-resources']/section[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></OtherResources>
                        </xsl:when>
                    </xsl:choose>
                    
                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-alt-form']/SECTION[@class='scg-content'])">
                            <AltForm><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="SECTION[@class='scg-alt-form']/SECTION[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></AltForm>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-alt-form']/section[@class='scg-content'])">
                            <AltForm><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="section[@class='scg-alt-form']/section[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></AltForm>
                        </xsl:when>
                    </xsl:choose>
                    
                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-location']/SECTION[@class='scg-content'])">
                            <Location><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(SECTION[@class='scg-location']/SECTION[@class='scg-content']), ']]&gt;')"/></Location>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-location']/section[@class='scg-content'])">
                            <Location><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(section[@class='scg-location']/section[@class='scg-content']), ']]&gt;')"/></Location>
                        </xsl:when>
                    </xsl:choose>
                    
                </GuideEntry>                    
            </xsl:for-each>
        </table>
        <!--</xsl:result-document>-->
    </xsl:template>        
</xsl:stylesheet>