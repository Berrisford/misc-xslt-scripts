<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <!--<xsl:result-document href="output/emuImport.xml">-->
        <table name="enarratives">
            <xsl:apply-templates select="//tuple"/>
        </table>
        <!--</xsl:result-document>-->
    </xsl:template>

    <xsl:template match="tuple">
        <tuple>
            <atom name="NarNarrative" type="text" size="short">
                <xsl:apply-templates select="atom/GuideEntry"/>
            </atom>
            <atom name="DesPurpose" type="text" size="short">SCGuideNew</atom>
            <xsl:apply-templates select="atom[@name='NarTitle']" mode="element"/>
            <xsl:apply-templates select="atom[@name='DetNarrativeIdentifier']" mode="element"/>
        </tuple>
    </xsl:template>

    <xsl:template match="GuideEntry">        
        &lt;article xmlns="http://www.w3.org/1999/xhtml"&gt;
            <!-- Instructions - start -->
            &lt;section class="scg-instructions" style="display:block;"&gt;
            &lt;hr/&gt;
            &lt;header&gt;
            &lt;h1&gt;Instructions:&lt;/h1&gt;
            &lt;/header&gt;
            &lt;ol&gt;
            &lt;li&gt;Open this document in oXygen xml editor.&lt;/li&gt;
            &lt;li&gt;Click: &lt;b&gt;Document -> Edit mode -> Author&lt;/b&gt;.&lt;/li&gt;
            &lt;li&gt;Click: &lt;b&gt;XHTML -> Tags display mode -> No tags&lt;/b&gt;.&lt;/li&gt;
            &lt;li&gt;Edit the content (where indicated) as desired.  &lt;b&gt;NB: &lt;/b&gt;Observe the toolbars at the top of the screen that are used to format the text, add bullet points, number lists etc.&lt;/li&gt;
            &lt;li&gt;Press &lt;b&gt;[CTRL + Shift + V]&lt;/b&gt; to validate the document. You should see a
            &lt;?oxy_custom_start type="oxy_content_highlight" color="180,215,3"?&gt;GREEN
            SQUARE&lt;?oxy_custom_end?&gt; at the bottom of your screen with the text "&lt;b&gt;&lt;i&gt;Document
            is valid&lt;/i&gt;&lt;/b&gt;". If not, the document contains errors that &lt;b&gt;MUST&lt;/b&gt; be
            corrected before pasting into EMu.&lt;/li&gt;
            &lt;li&gt;If you see a &lt;?oxy_custom_start type="oxy_content_highlight" color="255,64,0"?&gt;RED
            SQUARE&lt;?oxy_custom_end?&gt; with the text "&lt;b&gt;&lt;i&gt;Validation - failed. Error:&lt;/i&gt;&lt;/b&gt;"
            (follwed by a number,) the document is not valid and MUST be corrected before being
            pasted into EMu. One way to do this is: &lt;ol&gt;
            &lt;li&gt;Click: &lt;b&gt;Document -> Edit mode -> Text&lt;/b&gt;&lt;/li&gt;
            &lt;li&gt;You will see a
            &lt;?oxy_custom_start type="oxy_content_highlight" color="255,64,0"?&gt;RED
            SQUARE&lt;?oxy_custom_end?&gt; with one or more red rectangles in the right margin
            of the screen. The invalid HTML will also be underlined RED in the text
            of the document.&lt;/li&gt;
            &lt;li&gt;Edit the document to get rid of all errors (upon which a green square will
            be displayed in the right margin of the document.&lt;/li&gt;
            &lt;li&gt;Repeat step 5 (above) to ensure validation.&lt;/li&gt;
            &lt;/ol&gt;
            &lt;/li&gt;            
            &lt;li&gt;Copy the content from the &lt;b&gt;SOUCE VIEW&lt;/b&gt;. Easiest way to do this is to: &lt;ol&gt;
            &lt;li&gt;Click: &lt;b&gt;Document -> Edit mode -> Text&lt;/b&gt;&lt;/li&gt;
            &lt;li&gt;Press &lt;b&gt;[CTRL + A]&lt;/b&gt; then &lt;b&gt;[CTRL + C]&lt;/b&gt;.&lt;/li&gt;
            &lt;/ol&gt;
            &lt;/li&gt;
            &lt;li&gt;In EMu, press &lt;b&gt;[CTRL + V]&lt;/b&gt; to paste this markup into the narratives &lt;b&gt;MARKUP
            TAB&lt;/b&gt;. You may switch to the EMu visual view to observe the output if you
            like.&lt;/li&gt;
            &lt;li&gt;&lt;?oxy_custom_start type="oxy_content_highlight" color="255,255,0"?&gt;&lt;b&gt;NEVER&lt;/b&gt;&lt;?oxy_custom_end?&gt;
            edit the narrative in EMu. There is currently a bug in the EMu editor that corrupts
            the html source code if edited in EMu. This will result in failure to display ALL
            narratives in the A-Z guide on the website (&lt;a
            href="http://www.library.manchester.ac.uk/search-resources/special-collections/guide-to-special-collections/atoz/"
            target="_blank"
            &gt;http://www.library.manchester.ac.uk/search-resources/special-collections/guide-to-special-collections/atoz/&lt;/a&gt;).&lt;/li&gt;
            &lt;li&gt;&lt;b&gt;&lt;?oxy_custom_start type="oxy_content_highlight" color="255,255,0"?&gt;NEVER&lt;?oxy_custom_end?&gt;&lt;/b&gt;
            paste invalid HTML source code into EMu. This will also break the A-Z
            guide.&lt;/li&gt;
            &lt;li&gt;To edit a narrative, copy the source from EMu, paste and edit it in oXygen, then overwrite the existing EMu source with the validated source from oXygen.&lt;/li&gt;
            &lt;li&gt;(Until further notice) &lt;b&gt;ALWAYS&lt;/b&gt; use the procedure outlined here to edit or create new narrative items in EMu.&lt;/li&gt;
            &lt;/ol&gt;
            &lt;hr/&gt;
            &lt;/section&gt;
            &lt;p&gt;    &lt;/p&gt;
            &lt;p&gt;    &lt;/p&gt;
            <!-- Instructions - end -->
        
            &lt;section class="scg-title" style="display:block; margin-bottom: 1em;"&gt;
                &lt;header&gt;&lt;h1&gt;Title:&lt;/h1&gt;&lt;/header&gt;
                &lt;section class="scg-content"&gt;
                    &lt;h1&gt;<xsl:value-of select="normalize-space(title)"/>&lt;/h1&gt;
                &lt;/section&gt;
            &lt;/section&gt;
            &lt;section class="scg-order" style="display:block; margin-bottom: 1em;"&gt;
                &lt;header&gt;&lt;b&gt;Sort Order:&lt;/b&gt;&lt;/header&gt;
                &lt;section class="scg-content"&gt;
        <xsl:value-of select="normalize-space(order)"/>
                &lt;/section&gt;
            &lt;/section&gt;        
            &lt;section class="scg-daterange" style="display:block; margin-bottom: 1em;"&gt;
                &lt;header&gt;&lt;b&gt;Date range:&lt;/b&gt;&lt;/header&gt;
                &lt;section class="scg-content"&gt;
        <xsl:value-of select="normalize-space(DateRange)"/>
                &lt;/section&gt;
            &lt;/section&gt;
            &lt;section class="scg-medium" style="display:block; margin-bottom: 1em;"&gt;
                &lt;header&gt;&lt;b&gt;Medium:&lt;/b&gt;&lt;/header&gt;
                &lt;section class="scg-content"&gt;
        <xsl:value-of select="normalize-space(Medium)"/>
                &lt;/section&gt;
            &lt;/section&gt;
            &lt;section class="scg-number-of-items" style="display:block; margin-bottom: 1em;"&gt;
                &lt;header&gt;&lt;b&gt;Number of items:&lt;/b&gt;&lt;/header&gt;
                &lt;section class="scg-content"&gt;
        <xsl:value-of select="normalize-space(NumberofItems)"/>
                &lt;/section&gt;
            &lt;/section&gt;
            &lt;section class="scg-description" style="display:block; margin-bottom: 1em;"&gt;
                &lt;header&gt;&lt;b&gt;Description:&lt;/b&gt;&lt;/header&gt;
                &lt;section class="scg-content" style="margin-bottom: 3em;"&gt;
                    &lt;p&gt;
                        <xsl:apply-templates select="Description/node()"/>
                    &lt;/p&gt;
                &lt;/section&gt;
            &lt;/section&gt;
            &lt;section class="scg-see-also" style="display:block; margin-bottom: 1em;"&gt;
                &lt;header&gt;&lt;b&gt;See also:&lt;/b&gt;&lt;/header&gt;
                &lt;section class="scg-content" style="margin-bottom: 3em;"&gt;
                    &lt;p&gt;
                        <xsl:apply-templates select="See_Also/node()"/>
                    &lt;/p&gt;
                &lt;/section&gt;
            &lt;/section&gt;
            &lt;section class="scg-further-info" style="display:block; margin-bottom: 1em;"&gt;
                &lt;header&gt;&lt;b&gt;Further information:&lt;/b&gt;&lt;/header&gt;
                &lt;section class="scg-content" style="margin-bottom: 3em;"&gt;
                    &lt;p&gt;
                        <xsl:apply-templates select="FindingAids/node()"/>
                    &lt;/p&gt;
                &lt;/section&gt;
            &lt;/section&gt;
            &lt;section class="scg-other-resources" style="display:block; margin-bottom: 1em;"&gt;
                &lt;header&gt;&lt;b&gt;Other resources:&lt;/b&gt;&lt;/header&gt;
                &lt;section class="scg-content" style="margin-bottom: 3em;"&gt;
                    &lt;p&gt;
                        Currently no other resources.
                    &lt;/p&gt;
                &lt;/section&gt;
            &lt;/section&gt;
            &lt;section class="scg-alt-form" style="display:block; margin-bottom: 1em;"&gt;
                &lt;header&gt;&lt;b&gt;Alternative Form:&lt;/b&gt;&lt;/header&gt;
                &lt;section class="scg-content" style="margin-bottom: 3em;"&gt;
                    &lt;p&gt;               
                        Currently no alternative form.
                    &lt;/p&gt;
                &lt;/section&gt;
            &lt;/section&gt;
            &lt;section class="scg-location" style="display:block; margin-bottom: 1em;"&gt;
                &lt;header&gt;&lt;b&gt;Location:&lt;/b&gt;&lt;/header&gt;
                &lt;section class="scg-content"&gt;
                    <xsl:value-of select="normalize-space(Location)"/> 
                &lt;/section&gt;
            &lt;/section&gt;
        &lt;/article&gt;
    </xsl:template>

    <xsl:template match="*" >
        <xsl:value-of select="concat(' &lt;', name())"/>
        <xsl:apply-templates select ="@*"/>
        <xsl:value-of select="'&gt;'"/>
        <xsl:apply-templates/>
        <xsl:value-of select="concat('&lt;/', name(), '&gt;')"/>
    </xsl:template> 
    
    <xsl:template match="@*" >
        <xsl:value-of select="concat(' ', name(), '=&quot;', ., '&quot;')"/>
    </xsl:template> 

    <xsl:template match="*" mode="element">                
        <xsl:element name="{name()}">
            <xsl:apply-templates select ="@*" mode="element"/> 
            <xsl:apply-templates mode="element"/>
        </xsl:element>        
    </xsl:template> 
    
    <xsl:template match="@*" mode="element">                
        <xsl:attribute name="{name()}" >
            <xsl:value-of select ="."/> 
        </xsl:attribute>
    </xsl:template> 
    
    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>    
    
</xsl:stylesheet>