<?php


function removeslashes($string)
{
    $string=implode("",explode("\\",$string));
    return stripslashes(trim($string));
}

$results = simplexml_load_file('../_data/narratives.xml');

$inputKey = (urldecode($_REQUEST['match']));


// Don't trust anyone to meddle with the parameter value

$titleKey = removeslashes($inputKey);

if (is_null($titleKey) || ($titleKey == "") || (strlen($titleKey) == 0)) {
	echo "<p>No match found</p>";
}
else {
	urldecode($titleKey);

	$titleKey = strtolower($titleKey); // lowercase string for consistent matching
		
//echo "Key: ".$titleKey."<br>";
	//Get the data and store in an array for searching
	foreach ($results as $result)
	{
		$id = $result->GuideEntry;
		$title = trim($result->title);
		$titleMatch = strtolower($title); // lowercase string for consistent matching
	

//echo "Title: ".$title."<br>";
//echo "title: ".$title.", dates: ".$CollDates.", \nDescription\n".$description."\nImage: ".$image."\nSee also: ".$seealso."\nFinding aids:\n".$findingaids."\nLocation:\n".$location."<br>\n";
//echo "Title".$title."<br>";

		if ($titleKey == $titleMatch) {
			// Get the rest of the details
			$collDates = $result->DateRange;
			$medium = $result->Medium;
			$size = $result->NumberofItems;
			$description = $result->Description;
			//$image = $result->Image;
			$seealso = $result->See_Also;
			// BA: [15/02/18] - renaming finding aids to further information (FurtherInfo)
			//$findingaids = $result->FindingAids;
			$furtherinfo = $result->FurtherInfo;
						
			//BA: [12/02/18] - add new fields - start
			$otherresources = $result->OtherResources;	
			$altform = $result->AltForm;	
			//BA: [12/02/18] - add new fields - end
			$location = $result->Location;	
			
			// Now output to web page			
			print("  <div class='event_detail'>\n");
			print("    <h1>".$title."</h1>");
			if (strlen($collDates) > 5) {
				echo "<p>Date range: ".$collDates."</p>\n";
			}
			if  (strpos($size, "unknown") === false) {
				echo "<p>Number of items: ".$size."</p>\n";
			}
			else {
				echo "<p>Number of items: unknown</p>\n";
			}			
			print($description."\n");
			if (strlen($seealso) > 10) {
				echo "<h2>See also:</h2>\n".$seealso."\n";
			}
			if (strlen($furtherinfo) > 10){
				echo "<h2>Further Information:</h2>\n".$furtherinfo."\n";
			}

			//BA: [12/02/18] - add new fields - start
			if (strlen($otherresources) > 10){
				echo "<h2>Other resources:</h2>\n".$otherresources."\n";
			}

			if (strlen($altform) > 10){
				echo "<h2>Alternative form:</h2>\n".$altform."\n";
			}
			//BA: [12/02/18] - add new fields - start

			print("<h2>Location:</h2>");
                        switch ($location) {
				case "Main":
					echo "<ul><li><a href=\"/locations-and-opening-hours/main-library/\">Main Library</a> <a href=\"http://www.manchester.ac.uk/discover/maps/interactive-map/?id=53\" title=\"See campus map for the Main Library in a new window\" target=\"ml_map\">[map]</a></li>";
					echo "<li>Find out how to <a href=\"http://www.library.manchester.ac.uk/special-collections/access-the-special-collections/using-the-reading-rooms/reading-room-main-library/\">use the reading room in the Main Library</a></li></ul>";
					break;
				case "JRL":
					echo "<ul><li><a href=\"/locations-and-opening-hours/the-john-rylands-library/\">The John Rylands Library</a> <a href=\"http://www.manchester.ac.uk/discover/maps/interactive-map/?id=414\" title=\"See campus map for The John Rylands Library in a new window\" target=\"jrl_map\">[map]</a></li>";
					echo "<li>Find out how to <a href=\"http://www.library.manchester.ac.uk/special-collections/access-the-special-collections/using-the-reading-rooms/reading-rooms-john-rylands-library/\">use the reading rooms in The John Rylands Library</a></li></ul>";
					break;
				case "Both":
					echo "<ul><li><a href=\"/locations-and-opening-hours/main-library/\">Main Library</a> <a href=\"http://www.manchester.ac.uk/discover/maps/interactive-map/?id=53\" title=\"See campus map for the Main Library in a new window\" target=\"ml_map\">[map]</a> and <a href=\"/locations-and-opening-hours/the-john-rylands-library/\">The John Rylands Library</a> <a href=\"http://www.manchester.ac.uk/discover/maps/interactive-map/?id=414\" title=\"See campus map for The John Rylands Library in a new window\" target=\"jrl_map\">[map]</a></li>";
					echo "<li>Find out how to <a href=\"http://www.library.manchester.ac.uk/special-collections/access-the-special-collections/using-the-reading-rooms/reading-room-main-library/\">use the reading room in the Main Library</a></li>";
					echo "<li>Find out how to <a href=\"http://www.library.manchester.ac.uk/special-collections/access-the-special-collections/using-the-reading-rooms/reading-rooms-john-rylands-library/\">use the reading rooms in The John Rylands Library</a></li></ul>";
					break;
				default:
					echo "<ul><li>Location not specified</li></ul>";
					break;
			}			
			print("  </div>\n");
			break;
		}

	}
}
?>