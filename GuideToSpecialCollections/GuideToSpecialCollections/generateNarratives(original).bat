@echo off &setlocal
:: BA - Change the directory below to match wherever the scripts are saved 
::cd /D D:\Users\mtlssbe2\eclipse\workspace\test_stuff(misc)\GuideToSpecialCollections 
:: BA - Change directory to where ever the batch file is being run from
cd /D %cd% 
java -jar "saxon\saxon9pe.jar" -s:emuExport.xml -xsl:xsl\cleanExport.xsl -o:emuExport1.xml
set "search0=&lt;?xml version=\"1.0\" encoding=\"UTF-8\"?&gt;"
set "replace0="
set "search=&lt;"
set "replace=<"
set "search2=&gt;"
set "replace2=>"
set "search3=&amp;amp;nbsp;"
set "replace3= "
set "search4=&amp;rsquo;"
set "replace4='"
set "search5=&amp;ndash;"
set "replace5=-"
set "search6=&amp;nbsp;"
set "replace6= " 
set "search7=xmlns"
set "replace7=data-xmlns" 
set "search8=&amp;amp;"
set "replace8=&amp;"

set "textfile=emuExport1.xml"
set "newfile=emuExport2.xml"
(for /f "delims=" %%i in ('findstr "^" "%textfile%"') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    set "line=!line:%search0%=%replace0%!"
    set "line=!line:%search%=%replace%!"
    set "line=!line:%search2%=%replace2%!"
    set "line=!line:%search3%=%replace3%!"
    set "line=!line:%search4%=%replace4%!"
    set "line=!line:%search5%=%replace5%!"
    set "line=!line:%search6%=%replace6%!"
    set "line=!line:%search7%=%replace7%!"
    set "line=!line:%search8%=%replace8%!"
    echo(!line!
    endlocal
))>"%newfile%"
type "%newfile%"

java -jar "saxon\saxon9pe.jar" -s:emuExport2.xml -xsl:xsl\emu2t4.xsl -o:narratives1.xml

set "search=&lt;"
set "replace=<"
set "search2=&gt;"
set "replace2=>"
set "search3=&amp;nbsp;"
set "replace3= "
set "search4=&amp;gt;"
set "replace4=>"
set "textfile=narratives1.xml"
set "newfile=narratives.xml"
(for /f "delims=" %%i in ('findstr "^" "%textfile%"') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    set "line=!line:%search%=%replace%!"
    set "line=!line:%search2%=%replace2%!"
    set "line=!line:%search3%=%replace3%!"
    set "line=!line:%search4%=%replace4%!"
    echo(!line!
    endlocal
))>"%newfile%"
type "%newfile%"

::BA - Delete temporary files...
del emuExport1.xml
del emuExport2.xml
del narratives1.xml
pause
