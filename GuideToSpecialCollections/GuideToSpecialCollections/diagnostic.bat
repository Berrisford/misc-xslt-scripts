@echo off &setlocal
:: BA - Change the directory below to match wherever the scripts are saved 
::cd /D D:\Users\mtlssbe2\eclipse\workspace\test_stuff(misc)\GuideToSpecialCollections 
:: BA - Change directory to where ever the batch file is being run from
cd /D %cd% 
java -jar "saxon\saxon9pe.jar" -s:emuExport.xml -xsl:xsl\cleanExport.xsl -o:emuExport1.xml
set "search=&lt;"
set "replace=<"
set "search2=&gt;"
set "replace2=>"
set "search3=&amp;amp;nbsp;"
set "replace3= "
set "search4=&amp;rsquo;"
set "replace4='"
set "search5=&amp;ndash;"
set "replace5=-"
set "search6=&amp;nbsp;"
set "replace6= " 
set "search7=xmlns"
set "replace7=data-xmlns" 

set "textfile=emuExport1.xml"
set "newfile=diagnostic.xml"
(for /f "delims=" %%i in ('findstr "^" "%textfile%"') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    set "line=!line:%search%=%replace%!"
    set "line=!line:%search2%=%replace2%!"
    set "line=!line:%search3%=%replace3%!"
    set "line=!line:%search4%=%replace4%!"
    set "line=!line:%search5%=%replace5%!"
    set "line=!line:%search6%=%replace6%!"
    set "line=!line:%search7%=%replace7%!"
    echo(!line!
    endlocal
))>"%newfile%"
type "%newfile%"

del emuExport1.xml
pause
