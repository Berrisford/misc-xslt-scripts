<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math" 
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation=" http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd"
    exclude-result-prefixes="xs math"
    version="3.0">
    
    <xsl:output method="xml" indent="yes" />
    
    <!-- BA: Comment/uncomment this below to match sandbox/prd start -->
    <!-- Sandbox -->
    <xsl:variable name="strPrimoRoot" select="'https://manchester-psb.alma.exlibrisgroup.com/discovery/search'"/>    
    <!-- Prd -->
    <!--<xsl:variable name="strPrimoRoot" select="'https://manchester.alma.exlibrisgroup.com/discovery/search'"/>-->
    <!-- BA: Comment/uncomment this below to match sandbox/prd end -->
    
    <xsl:template match="/">
        <xsl:result-document href="../narrativesMARC21Sample.xml">        
            <xsl:apply-templates/>            
        </xsl:result-document>
    </xsl:template>
    
    
    <!-- BA: Copy element contents - start -->
<!--    
    <xsl:template match ="/"> 
        <xsl:apply-templates/> 
    </xsl:template>
-->    
    <xsl:template match="*" >
        <xsl:element name="{name()}" >
            <xsl:apply-templates select ="@*"/> 
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template> 
    
    <xsl:template match="@*" >
        <xsl:attribute name="{name()}" >
            <xsl:value-of select ="."/> 
        </xsl:attribute> 
    </xsl:template> 
<!--
    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>
-->

    <xsl:template match="text()">
        <xsl:choose>
            <xsl:when test="../../@tag = '787' and ../@code='t'">
                <xsl:value-of select="normalize-space(replace(replace(replace(., '&lt;p&gt;', ''), '&lt;/p&gt;', ''), '>', '&gt;'))"/>                 
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="replace(., '>', '&gt;')" />                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- BA: Copy element contents - end -->
    
<!--    
    <xsl:template match="text()">
        <xsl:value-of select="replace(., '>', '&gt;')" />
        <xsl:if test="../../@tag = '787' and ../@code='t'">
            <xsl:value-of select="normalize-space(replace(replace(., '&lt;p&gt;', ''), '&lt;/p&gt;', ''))"/> 
        </xsl:if>
    </xsl:template>
-->    
<!--    
    <xsl:template match="//marc:datafield[@tag='787'][@ind1='0'][@ind2=' ']/marc:subfield[@code='t']//text()">
        <xsl:value-of select="normalize-space(replace(replace(., '&lt;p&gt;', ''), '&lt;/p&gt;', ''))"/>
    </xsl:template>
-->    
</xsl:stylesheet>