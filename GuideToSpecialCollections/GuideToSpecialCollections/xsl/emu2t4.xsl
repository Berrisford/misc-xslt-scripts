<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <!--<xsl:result-document href="../output.xml">-->
        <table name="enarratives">
            <xsl:for-each select="//atom/ARTICLE[1]|//atom/article[1]">
                <xsl:sort select="normalize-space(SECTION[@class='scg-order']/SECTION[@class='scg-content'])"/>
                <xsl:sort select="normalize-space(section[@class='scg-order']/section[@class='scg-content'])"/>
                <GuideEntry>
                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-order']/SECTION[@class='scg-content'])">
                            <order><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(SECTION[@class='scg-order']/SECTION[@class='scg-content']), ']]&gt;')"/></order>                            
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-order']/section[@class='scg-content'])">
                            <order><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(section[@class='scg-order']/section[@class='scg-content']), ']]&gt;')"/></order>                            
                        </xsl:when>
                    </xsl:choose>

                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-title']/SECTION[@class='scg-content'])">
                            <title><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(SECTION[@class='scg-title']/SECTION[@class='scg-content']/H1), ']]&gt;')"/></title>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-title']/section[@class='scg-content'])">
                            <title><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(section[@class='scg-title']/section[@class='scg-content']/h1), ']]&gt;')"/></title>
                        </xsl:when>
                    </xsl:choose>
                    
                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-daterange']/SECTION[@class='scg-content'])">
                            <DateRange><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(SECTION[@class='scg-daterange']/SECTION[@class='scg-content']), ']]&gt;')"/></DateRange>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-daterange']/section[@class='scg-content'])">
                            <DateRange><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(section[@class='scg-daterange']/section[@class='scg-content']), ']]&gt;')"/></DateRange>
                        </xsl:when>
                    </xsl:choose>
                    
                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-medium']/SECTION[@class='scg-content'])">
                            <Medium><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(SECTION[@class='scg-medium']/SECTION[@class='scg-content']), ']]&gt;')"/></Medium>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-medium']/section[@class='scg-content'])">
                            <Medium><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(section[@class='scg-medium']/section[@class='scg-content']), ']]&gt;')"/></Medium>
                        </xsl:when>
                    </xsl:choose>

                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-number-of-items']/SECTION[@class='scg-content']) and normalize-space(SECTION[@class='scg-number-of-items']/SECTION[@class='scg-content']) ne '' and normalize-space(SECTION[@class='scg-number-of-items']/SECTION[@class='scg-content']) ne 'Number of items:'">
                            <NumberofItems><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(SECTION[@class='scg-number-of-items']/SECTION[@class='scg-content']), ']]&gt;')"/></NumberofItems>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-number-of-items']/section[@class='scg-content']) and normalize-space(section[@class='scg-number-of-items']/section[@class='scg-content']) ne '' and normalize-space(section[@class='scg-number-of-items']/section[@class='scg-content']) ne 'Number of items:'">
                            <NumberofItems><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(section[@class='scg-number-of-items']/section[@class='scg-content']), ']]&gt;')"/></NumberofItems>
                        </xsl:when>
                    </xsl:choose>

                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-description']/SECTION[@class='scg-content'])">
                            <Description><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="SECTION[@class='scg-description']/SECTION[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></Description>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-description']/section[@class='scg-content'])">
                            <Description><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="section[@class='scg-description']/section[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></Description>
                        </xsl:when>
                    </xsl:choose>
                    
                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-see-also']/SECTION[@class='scg-content']) and normalize-space(SECTION[@class='scg-see-also']/SECTION[@class='scg-content']/P[1]) ne '' and not(contains(SECTION[@class='scg-see-also']/SECTION[@class='scg-content']/P[1], 'Replace this'))">
                            <See_Also><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="SECTION[@class='scg-see-also']/SECTION[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></See_Also>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-see-also']/section[@class='scg-content']) and normalize-space(section[@class='scg-see-also']/section[@class='scg-content']/p[1]) ne '' and not(contains(section[@class='scg-see-also']/section[@class='scg-content']/p[1], 'Replace this'))">
                            <See_Also><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="section[@class='scg-see-also']/section[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></See_Also>
                        </xsl:when>
                        <!-- BA: 29/10/2018 - Fix bug where see also with <li> tags are not generated - start -->
                        <xsl:when test="exists(section[@class='scg-see-also']/section[@class='scg-content'])">
                            <xsl:try>
                                <See_Also><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="section[@class='scg-see-also']/section[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></See_Also>                                
                                <xsl:catch></xsl:catch>
                            </xsl:try>
                        </xsl:when>                        
                        <!-- BA: 29/10/2018 - Fix bug where see also with <li> tags are not generated - end -->
                    </xsl:choose>
                    
                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-further-info']/SECTION[@class='scg-content']) and normalize-space(SECTION[@class='scg-further-info']/SECTION[@class='scg-content']/P[1]) ne '' and not(contains(SECTION[@class='scg-further-info']/SECTION[@class='scg-content']/P[1], 'Replace this'))">
                            <FurtherInfo><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="SECTION[@class='scg-further-info']/SECTION[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></FurtherInfo>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-further-info']/section[@class='scg-content']) and normalize-space(section[@class='scg-further-info']/section[@class='scg-content']/p[1]) ne '' and not(contains(section[@class='scg-further-info']/section[@class='scg-content']/p[1], 'Replace this'))">
                            <FurtherInfo><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="section[@class='scg-further-info']/section[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></FurtherInfo>
                        </xsl:when>
                        <!-- BA: 11/10/2018 - Fix bug where further info with <li> tags are not generated - start -->
                        <xsl:when test="exists(section[@class='scg-further-info']/section[@class='scg-content'])">
                            <xsl:try>
                                <FurtherInfo><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="section[@class='scg-further-info']/section[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></FurtherInfo>                                
                                <xsl:catch></xsl:catch>
                            </xsl:try>
                        </xsl:when>                        
                        <!-- BA: 11/10/2018 - Fix bug where further info with <li> tags are not generated - end -->
                    </xsl:choose>

                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-other-resources']/SECTION[@class='scg-content']) and normalize-space(SECTION[@class='scg-other-resources']/SECTION[@class='scg-content']/P[1]) ne '' and not(contains(SECTION[@class='scg-other-resources']/SECTION[@class='scg-content']/P[1], 'Currently no other resources')) and not(contains(SECTION[@class='scg-other-resources']/SECTION[@class='scg-content']/P[1], 'Replace this'))">
                            <OtherResources><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="SECTION[@class='scg-other-resources']/SECTION[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></OtherResources>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-other-resources']/section[@class='scg-content']) and normalize-space(section[@class='scg-other-resources']/section[@class='scg-content']/p[1]) ne '' and not(contains(section[@class='scg-other-resources']/section[@class='scg-content']/p[1], 'Currently no other resources')) and not(contains(section[@class='scg-other-resources']/section[@class='scg-content']/p[1], 'Replace this'))">
                            <OtherResources><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="section[@class='scg-other-resources']/section[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></OtherResources>
                        </xsl:when>
                    </xsl:choose>
                    
                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-alt-form']/SECTION[@class='scg-content']) and normalize-space(SECTION[@class='scg-alt-form']/SECTION[@class='scg-content']/P[1]) ne '' and not(contains(SECTION[@class='scg-alt-form']/SECTION[@class='scg-content']/P[1], 'Currently no alternative form')) and not(contains(SECTION[@class='scg-alt-form']/SECTION[@class='scg-content']/P[1], 'Replace this'))">
                            <AltForm><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="SECTION[@class='scg-alt-form']/SECTION[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></AltForm>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-alt-form']/section[@class='scg-content']) and normalize-space(section[@class='scg-alt-form']/section[@class='scg-content']/p[1]) ne '' and not(contains(section[@class='scg-alt-form']/section[@class='scg-content']/p[1], 'Currently no alternative form')) and not(contains(section[@class='scg-alt-form']/section[@class='scg-content']/p[1], 'Replace this'))">
                            <AltForm><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="section[@class='scg-alt-form']/section[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></AltForm>
                        </xsl:when>
                        <!-- BA: 29/10/2018 - Fix bug where alt form with <li> tags are not generated - start -->
                        <xsl:when test="exists(section[@class='scg-alt-form']/section[@class='scg-content']) and normalize-space(section[@class='scg-alt-form']/section[@class='scg-content']) ne ''">
                            <xsl:try>
                                <AltForm><xsl:text>&lt;![CDATA[</xsl:text><xsl:copy-of select="section[@class='scg-alt-form']/section[@class='scg-content']"/><xsl:text>]]&gt;</xsl:text></AltForm>                                
                                <xsl:catch></xsl:catch>
                            </xsl:try>
                        </xsl:when>                        
                        <!-- BA: 29/10/2018 - Fix bug where alt form with <li> tags are not generated - end -->
                    </xsl:choose>
                    
                    <xsl:choose>
                        <xsl:when test="exists(SECTION[@class='scg-location']/SECTION[@class='scg-content'])">
                            <Location><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(SECTION[@class='scg-location']/SECTION[@class='scg-content']), ']]&gt;')"/></Location>
                        </xsl:when>
                        <xsl:when test="exists(section[@class='scg-location']/section[@class='scg-content'])">
                            <Location><xsl:value-of select="concat('&lt;![CDATA[', normalize-space(section[@class='scg-location']/section[@class='scg-content']), ']]&gt;')"/></Location>
                        </xsl:when>
                    </xsl:choose>
                    
                </GuideEntry>                    
            </xsl:for-each>
        </table>
        <!--</xsl:result-document>-->
    </xsl:template>        
</xsl:stylesheet>