<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math" 
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation=" http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd"
    exclude-result-prefixes="xs math"
    version="3.0">

    <xsl:output method="xml" indent="yes" />

    <!-- BA: Comment/uncomment this below to match sandbox/prd start -->
    <!-- Sandbox -->
    <xsl:variable name="strPrimoRoot" select="'https://manchester-psb.alma.exlibrisgroup.com/discovery/search'"/>    
    <!-- Prd -->
    <!--<xsl:variable name="strPrimoRoot" select="'https://manchester.alma.exlibrisgroup.com/discovery/search'"/>-->
    <!-- BA: Comment/uncomment this below to match sandbox/prd end -->
    

    <xsl:template match="/">
        <!--<xsl:result-document href="../narrativesMARC21Sample.xml">-->
        <!--<xsl:result-document href="../emu2MARC21-Output.xml">-->
        <xsl:result-document href="../narrativesMARC21Sample-unreplaced.xml">
            <marc:collection>

                <xsl:for-each select="//atom/ARTICLE[1]|//atom/article[1]">
                    <!--<xsl:sort select="normalize-space(SECTION[@class='scg-order']/SECTION[@class='scg-content'])" />-->
                    <!--<xsl:sort select="normalize-space(section[@class='scg-order']/section[@class='scg-content'])" />-->

                    <marc:record>
                        <!--<marc:leader>00266nkm a22000973ua4500</marc:leader>-->
                        <marc:leader>05630npc a22002893i 4500</marc:leader>
                        <!--<marc:controlfield tag="008">170106s2016    xx nnn go     o00 kneng d</marc:controlfield>-->
                        <marc:controlfield tag="001" />
                        <marc:controlfield tag="006">aa 000 0 </marc:controlfield>
                        <marc:controlfield tag="007">td</marc:controlfield>
                        <marc:controlfield tag="008">210211k18921923xxk mul d</marc:controlfield>

                        <marc:datafield tag="100" ind1="1" ind2=" ">
                            <marc:subfield code="a">Berrisford Narratives Test</marc:subfield>
                        </marc:datafield>
                        <marc:datafield tag="336" ind1=" " ind2=" ">
                            <marc:subfield code="a">text</marc:subfield>
                            <marc:subfield code="b">txt</marc:subfield>
                            <marc:subfield code="2">rdacontent</marc:subfield>
                        </marc:datafield>
                        <marc:datafield tag="337" ind1=" " ind2=" ">
                            <marc:subfield code="a">unmediated</marc:subfield>
                            <marc:subfield code="b">n</marc:subfield>
                            <marc:subfield code="2">rdamedia</marc:subfield>
                        </marc:datafield>
                        <marc:datafield tag="338" ind1=" " ind2=" ">
                            <marc:subfield code="a">other</marc:subfield>
                            <marc:subfield code="b">nz</marc:subfield>
                            <marc:subfield code="2">rdacarrier</marc:subfield>
                        </marc:datafield>

                        <xsl:choose>
                            <xsl:when test="exists(SECTION[@class='scg-title']/SECTION[@class='scg-content'])">
                                <marc:datafield tag="245" ind1="0" ind2=" ">
                                    <marc:subfield code="a">
                                        <xsl:value-of select="normalize-space(SECTION[@class='scg-title']/SECTION[@class='scg-content']/H1)"/>
                                    </marc:subfield>
                                </marc:datafield>
                            </xsl:when>
                            <xsl:when test="exists(section[@class='scg-title']/section[@class='scg-content'])">
                                <marc:datafield tag="245" ind1="1" ind2="0">
                                    <marc:subfield code="a">
                                        <xsl:value-of select="normalize-space(section[@class='scg-title']/section[@class='scg-content']/h1)"/>
                                    </marc:subfield>
                                </marc:datafield>
                            </xsl:when>
                        </xsl:choose>

                        <xsl:choose>
                            <xsl:when test="exists(SECTION[@class='scg-daterange']/SECTION[@class='scg-content'])">
                                <marc:datafield tag="362" ind1="1" ind2=" ">
                                    <marc:subfield code="a">
                                        <xsl:value-of select="normalize-space(SECTION[@class='scg-daterange']/SECTION[@class='scg-content'])"/>
                                    </marc:subfield>
                                </marc:datafield>
                            </xsl:when>
                            <xsl:when test="exists(section[@class='scg-daterange']/section[@class='scg-content'])">
                                <marc:datafield tag="362" ind1="1" ind2=" ">
                                    <marc:subfield code="a">
                                        <xsl:value-of select="normalize-space(section[@class='scg-daterange']/section[@class='scg-content'])"/>
                                    </marc:subfield>
                                </marc:datafield>
                            </xsl:when>
                        </xsl:choose>

                        <xsl:choose>
                            <xsl:when test="exists(SECTION[@class='scg-medium']/SECTION[@class='scg-content'])">
                                <marc:datafield tag="300" ind1=" " ind2=" ">
                                    <marc:subfield code="a">
                                        <xsl:value-of select="normalize-space(SECTION[@class='scg-medium']/SECTION[@class='scg-content'])"/>
                                    </marc:subfield>
                                </marc:datafield>
                            </xsl:when>
                            <xsl:when test="exists(section[@class='scg-medium']/section[@class='scg-content'])">
                                <marc:datafield tag="300" ind1=" " ind2=" ">
                                    <marc:subfield code="a">
                                        <xsl:value-of select="normalize-space(section[@class='scg-medium']/section[@class='scg-content'])"/>
                                    </marc:subfield>
                                </marc:datafield>
                            </xsl:when>
                        </xsl:choose>

                        <xsl:choose>
                            <xsl:when test="exists(SECTION[@class='scg-number-of-items']/SECTION[@class='scg-content']) and normalize-space(SECTION[@class='scg-number-of-items']/SECTION[@class='scg-content']) ne '' and normalize-space(SECTION[@class='scg-number-of-items']/SECTION[@class='scg-content']) ne 'Number of items:'">
                                <marc:datafield tag="300" ind1=" " ind2=" ">
                                    <marc:subfield code="a">
                                        <xsl:value-of select="normalize-space(SECTION[@class='scg-number-of-items']/SECTION[@class='scg-content'])"/>
                                    </marc:subfield>
                                </marc:datafield>
                            </xsl:when>
                            <xsl:when test="exists(section[@class='scg-number-of-items']/section[@class='scg-content']) and normalize-space(section[@class='scg-number-of-items']/section[@class='scg-content']) ne '' and normalize-space(section[@class='scg-number-of-items']/section[@class='scg-content']) ne 'Number of items:'">
                                <marc:datafield tag="300" ind1=" " ind2=" ">
                                    <marc:subfield code="a">
                                        <xsl:value-of select="normalize-space(section[@class='scg-number-of-items']/section[@class='scg-content'])"/>
                                    </marc:subfield>
                                </marc:datafield>
                            </xsl:when>
                        </xsl:choose>

                        <xsl:choose>
                            <xsl:when test="exists(SECTION[@class='scg-description']/SECTION[@class='scg-content'])">
                                <marc:datafield tag="500" ind1=" " ind2=" ">
                                    <marc:subfield code="a">
                                        <!--<xsl:copy-of select="SECTION[@class='scg-description']/SECTION[@class='scg-content']"/>-->
                                        <xsl:apply-templates select="SECTION[@class='scg-description']/SECTION[@class='scg-content']"/>                                        
                                    </marc:subfield>
                                </marc:datafield>
                                <marc:datafield tag="505" ind1="0" ind2=" ">
                                    <marc:subfield code="a">
                                        <!--<xsl:copy-of select="SECTION[@class='scg-description']/SECTION[@class='scg-content']"/>-->                                                                                
                                        <xsl:apply-templates select="SECTION[@class='scg-description']/SECTION[@class='scg-content']"/>                                        
                                    </marc:subfield>
                                </marc:datafield>
                                <marc:datafield tag="520" ind1=" " ind2=" ">
                                    <marc:subfield code="a">
                                        <!--<xsl:copy-of select="SECTION[@class='scg-description']/SECTION[@class='scg-content']"/>-->
                                        <xsl:apply-templates select="SECTION[@class='scg-description']/SECTION[@class='scg-content']"/>                                        
                                    </marc:subfield>
                                </marc:datafield>                                
                            </xsl:when>
                            <xsl:when test="exists(section[@class='scg-description']/section[@class='scg-content'])">
                                <marc:datafield tag="500" ind1=" " ind2=" ">
                                    <marc:subfield code="a">
                                        <!--<xsl:copy-of select="section[@class='scg-description']/section[@class='scg-content']"/>-->
                                        <xsl:apply-templates select="section[@class='scg-description']/section[@class='scg-content']"/>                                        
                                    </marc:subfield>
                                </marc:datafield>
                                <marc:datafield tag="505" ind1="0" ind2=" ">
                                    <marc:subfield code="a">
                                        <!--<xsl:copy-of select="section[@class='scg-description']/section[@class='scg-content']"/>-->
                                        <xsl:apply-templates select="section[@class='scg-description']/section[@class='scg-content']"/>
                                    </marc:subfield>
                                </marc:datafield>
                                <marc:datafield tag="520" ind1=" " ind2=" ">
                                    <marc:subfield code="a">
                                        <!--<xsl:copy-of select="section[@class='scg-description']/section[@class='scg-content']"/>-->
                                        <xsl:apply-templates select="section[@class='scg-description']/section[@class='scg-content']"/>
                                    </marc:subfield>
                                </marc:datafield>                                
                            </xsl:when>
                        </xsl:choose>

                        <xsl:choose>
                            <xsl:when test="exists(SECTION[@class='scg-see-also']/SECTION[@class='scg-content']) and normalize-space(SECTION[@class='scg-see-also']/SECTION[@class='scg-content']/*[1]) ne '' and not(contains(SECTION[@class='scg-see-also']/SECTION[@class='scg-content']/P[1], 'Replace this'))">
                                <marc:datafield tag="787" ind1="0" ind2=" ">
                                    <!--<marc:subfield code="i">See also:</marc:subfield>-->
                                    <!--<marc:subfield code="a"><xsl:copy-of select="SECTION[@class='scg-see-also']/SECTION[@class='scg-content']"/></marc:subfield>-->
                                    <!--<marc:subfield code="t"><xsl:copy-of select="SECTION[@class='scg-see-also']/SECTION[@class='scg-content']"/></marc:subfield>-->
                                    <marc:subfield code="t"><xsl:apply-templates select="SECTION[@class='scg-see-also']/SECTION[@class='scg-content']"/></marc:subfield>                                    
                                    <!--<marc:subfield code="w">[MMS id of record]</marc:subfield>-->
                                </marc:datafield>
                            </xsl:when>
                            
                            <!-- BA: Handle unordered lists start...************************************************* -->
                            <xsl:when test="exists(section[@class='scg-see-also']/section[@class='scg-content']/ul/li/a)">
                                <xsl:apply-templates select="section[@class='scg-see-also']/section[@class='scg-content']/ul/li/a" mode="related856"/>                                
                            </xsl:when>
                            <!-- BA: Handle unordered lists end...************************************************* -->
                            
                            <xsl:when test="exists(section[@class='scg-see-also']/section[@class='scg-content']) and normalize-space(section[@class='scg-see-also']/section[@class='scg-content']/*[1]) ne '' and not(contains(section[@class='scg-see-also']/section[@class='scg-content']/p[1], 'Replace this'))">
                                <marc:datafield tag="787" ind1="0" ind2=" ">
                                    <!--<marc:subfield code="i">See also:</marc:subfield>-->
                                    <!--<marc:subfield code="a"><xsl:copy-of select="section[@class='scg-see-also']/section[@class='scg-content']"/></marc:subfield>-->
                                    <!--<marc:subfield code="t"><xsl:copy-of select="section[@class='scg-see-also']/section[@class='scg-content']"/></marc:subfield>-->
                                    <marc:subfield code="t"><xsl:apply-templates select="section[@class='scg-see-also']/section[@class='scg-content']"/></marc:subfield>
                                    <!--<marc:subfield code="w">[MMS id of record]</marc:subfield>-->
                                </marc:datafield>
                                <xsl:apply-templates select="section[@class='scg-see-also']/section[@class='scg-content']//a" mode="related758"/>
                            </xsl:when>
                            <!-- BA: 29/10/2018 - Fix bug where see also with <li> tags are not generated - start -->
                            <xsl:when test="exists(section[@class='scg-see-also']/section[@class='scg-content']) and normalize-space(section[@class='scg-see-also']/section[@class='scg-content']/*[1]) ne ''">
                                <!--<xsl:when test="exists(section[@class='scg-see-also']/section[@class='scg-content'])">-->
                                    <xsl:try>
                                        <marc:datafield tag="787" ind1="0" ind2=" ">
                                            <!--<marc:subfield code="i">See also:</marc:subfield>-->
                                            <!--<marc:subfield code="a"><xsl:copy-of select="section[@class='scg-see-also']/section[@class='scg-content']"/></marc:subfield>-->
                                            <!--<marc:subfield code="t"><xsl:copy-of select="section[@class='scg-see-also']/section[@class='scg-content']"/></marc:subfield>-->
                                            <marc:subfield code="t"><xsl:apply-templates select="section[@class='scg-see-also']/section[@class='scg-content']"/></marc:subfield>
                                            <!--<marc:subfield code="w">[MMS id of record]</marc:subfield>-->
                                        </marc:datafield>
                                        <xsl:catch></xsl:catch>
                                    </xsl:try>
                                </xsl:when>                        
                                <!-- BA: 29/10/2018 - Fix bug where see also with <li> tags are not generated - end -->
                        </xsl:choose>
                        
                        <!-- TODO: Merge "see also" and "other resources" !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
                        
                        <xsl:choose>
                            <xsl:when test="exists(SECTION[@class='scg-other-resources']/SECTION[@class='scg-content']) and normalize-space(SECTION[@class='scg-other-resources']/SECTION[@class='scg-content']/*[1]) ne '' and not(contains(SECTION[@class='scg-other-resources']/SECTION[@class='scg-content']/P[1], 'Currently no other resources')) and not(contains(SECTION[@class='scg-other-resources']/SECTION[@class='scg-content']/P[1], 'Replace this'))">
                                <marc:datafield tag="787" ind1="0" ind2=" ">
                                    <marc:subfield code="i">Other resources:</marc:subfield>
                                    <marc:subfield code="a"><xsl:copy-of select="SECTION[@class='scg-other-resources']/SECTION[@class='scg-content']"/></marc:subfield>
                                    <!--<marc:subfield code="t">Other resources: <xsl:copy-of select="SECTION[@class='scg-see-also']/SECTION[@class='scg-content']"/></marc:subfield>-->

                                    <marc:subfield code="a"><xsl:apply-templates select="SECTION[@class='scg-other-resources']/SECTION[@class='scg-content']"/></marc:subfield>
                                    
                                    <!--<marc:subfield code="t"><xsl:copy-of select="section[@class='scg-see-also']/section[@class='scg-content']"/></marc:subfield>-->
                                    <marc:subfield code="t"><xsl:apply-templates select="SECTION[@class='scg-other-resources']/SECTION[@class='scg-content']"/></marc:subfield>
                                </marc:datafield>
                            </xsl:when>
                            <xsl:when test="exists(section[@class='scg-other-resources']/section[@class='scg-content']) and normalize-space(section[@class='scg-other-resources']/section[@class='scg-content']/*[1]) ne '' and not(contains(section[@class='scg-other-resources']/section[@class='scg-content']/p[1], 'Currently no other resources')) and not(contains(section[@class='scg-other-resources']/section[@class='scg-content']/p[1], 'Replace this'))">
                                <marc:datafield tag="787" ind1="0" ind2=" ">
                                    <marc:subfield code="i">Other resources:</marc:subfield>
                                    <!--<marc:subfield code="a"><xsl:copy-of select="section[@class='scg-other-resources']/section[@class='scg-content']"/></marc:subfield>-->
                                    <marc:subfield code="a"><xsl:apply-templates select="section[@class='scg-other-resources']/section[@class='scg-content']"/></marc:subfield>
                                    
                                    <!--<marc:subfield code="t"><xsl:copy-of select="section[@class='scg-see-also']/section[@class='scg-content']"/></marc:subfield>-->
                                    <marc:subfield code="t"><xsl:apply-templates select="section[@class='scg-other-resources']/section[@class='scg-content']"/></marc:subfield>
                                </marc:datafield>
                            </xsl:when>
                            <!-- BA: 29/10/2018 - Fix bug where see also with <li> tags are not generated - start -->
                            <xsl:when test="exists(section[@class='scg-other-resources']/section[@class='scg-content']) and normalize-space(section[@class='scg-other-resources']/section[@class='scg-content']/*[1]) ne '' and not(contains(section[@class='scg-other-resources']/section[@class='scg-content']/p[1], 'Currently no other resources')) and not(contains(section[@class='scg-other-resources']/section[@class='scg-content']/p[1], 'Replace this'))">
                                <xsl:try>
                                    <marc:datafield tag="787" ind1="0" ind2=" ">
                                        <marc:subfield code="i">Other resources:</marc:subfield>
                                        <!--<marc:subfield code="a"><xsl:copy-of select="section[@class='scg-other-resources']/section[@class='scg-content']"/></marc:subfield>-->
                                        <marc:subfield code="a"><xsl:apply-templates select="section[@class='scg-other-resources']/section[@class='scg-content']"/></marc:subfield>
                                        
                                        <!--<marc:subfield code="t"><xsl:copy-of select="section[@class='scg-see-also']/section[@class='scg-content']"/></marc:subfield>-->
                                        <marc:subfield code="t"><xsl:apply-templates select="section[@class='scg-see-also']/section[@class='scg-content']"/></marc:subfield>
                                    </marc:datafield>
                                    <xsl:catch></xsl:catch>
                                </xsl:try>
                            </xsl:when>                        
                            <!-- BA: 29/10/2018 - Fix bug where see also with <li> tags are not generated - end -->
                        </xsl:choose>                        

                        <xsl:choose>
                            <xsl:when test="exists(SECTION[@class='scg-further-info']/SECTION[@class='scg-content']) and normalize-space(SECTION[@class='scg-further-info']/SECTION[@class='scg-content']/*[1]) ne '' and not(contains(SECTION[@class='scg-further-info']/SECTION[@class='scg-content']/P[1], 'Replace this'))">
                                <marc:datafield tag="856" ind1="4" ind2="2">
                                    <!--<marc:subfield code="z">Catalogue for Parliamentary Committee for Women’s Suffrage.</marc:subfield>-->
                                    <!--<marc:subfield code="z"><xsl:copy-of select="SECTION[@class='scg-further-info']/SECTION[@class='scg-content']"/></marc:subfield>-->
                                    <marc:subfield code="z"><xsl:apply-templates select="SECTION[@class='scg-further-info']/SECTION[@class='scg-content']"/></marc:subfield>
                                </marc:datafield>                                
                            </xsl:when>
                            <xsl:when test="exists(section[@class='scg-further-info']/section[@class='scg-content']) and normalize-space(section[@class='scg-further-info']/section[@class='scg-content']/*[1]) ne '' and not(contains(section[@class='scg-further-info']/section[@class='scg-content']/p[1], 'Replace this'))">
                                <marc:datafield tag="856" ind1="4" ind2="2">
                                    <!--<marc:subfield code="z"><xsl:copy-of select="section[@class='scg-further-info']/section[@class='scg-content']"/></marc:subfield>-->
                                    <marc:subfield code="z"><xsl:apply-templates select="section[@class='scg-further-info']/section[@class='scg-content']"/></marc:subfield>
                                </marc:datafield>
                                
                                <xsl:try>
                                    <xsl:apply-templates select="section[@class='scg-further-info']/section[@class='scg-content'][not(has-children())]" mode="related856Text"/>                                    
                                    <xsl:catch></xsl:catch>
                                </xsl:try>
                                
                                <xsl:apply-templates select="section[@class='scg-further-info']/section[@class='scg-content']//a" mode="related856"/>
                                
                            </xsl:when>
                                <!-- BA: 11/10/2018 - Fix bug where further info with <li> tags are not generated - start -->
                                <xsl:when test="exists(section[@class='scg-further-info']/section[@class='scg-content'])">
                                    <xsl:try>
                                        <marc:datafield tag="856" ind1="4" ind2="2">
                                            <!--<marc:subfield code="z"><xsl:copy-of select="section[@class='scg-further-info']/section[@class='scg-content']"/></marc:subfield>-->
                                            <marc:subfield code="z"><xsl:apply-templates select="section[@class='scg-further-info']/section[@class='scg-content']"/></marc:subfield>
                                        </marc:datafield>                                                                        
                                        <xsl:catch></xsl:catch>
                                    </xsl:try>
                                </xsl:when>                        
                                <!-- BA: 11/10/2018 - Fix bug where further info with <li> tags are not generated - end -->
                        </xsl:choose>

                        <xsl:choose>
                            <xsl:when test="exists(SECTION[@class='scg-location']/SECTION[@class='scg-content'])">
                                <xsl:choose>
                                    <xsl:when test="normalize-space(SECTION[@class='scg-location']/SECTION[@class='scg-content'])='JRL'">
                                        <!--<marc:datafield tag="710" ind1="2" ind2=" ">
                                            <marc:subfield code="a">Location: https://www.library.manchester.ac.uk/rylands/visit/opening-hours/</marc:subfield>
                                            <marc:subfield code="e"></marc:subfield>
                                        </marc:datafield>-->
                                        <marc:datafield tag="856" ind1="4" ind2="1">
                                            <marc:subfield code="z">Location: The John Rylands Library, Deansgate</marc:subfield>
                                            <marc:subfield code="u">https://www.library.manchester.ac.uk/rylands/visit/opening-hours/</marc:subfield>
                                        </marc:datafield>                                        
                                    </xsl:when>
                                    <xsl:when test="normalize-space(SECTION[@class='scg-location']/SECTION[@class='scg-content'])='Main'">
                                        <!--<marc:datafield tag="710" ind1="2" ind2=" ">
                                            <marc:subfield code="a">Main Library.</marc:subfield>
                                            <marc:subfield code="e"></marc:subfield>
                                        </marc:datafield>-->
                                        <marc:datafield tag="856" ind1="4" ind2="1">
                                            <marc:subfield code="z">Location: The University of Manchester Main Library, Oxford Rd</marc:subfield>
                                            <marc:subfield code="u">https://www.library.manchester.ac.uk/locations-and-opening-hours/main-library/</marc:subfield>
                                        </marc:datafield>                                        
                                    </xsl:when>
                                    <xsl:when test="normalize-space(SECTION[@class='scg-location']/SECTION[@class='scg-content'])='Both'">
                                        <!--<marc:datafield tag="710" ind1="2" ind2=" ">
                                            <marc:subfield code="a">Location: https://www.library.manchester.ac.uk/rylands/visit/opening-hours/</marc:subfield>
                                            <marc:subfield code="e"></marc:subfield>
                                        </marc:datafield>-->
                                        <marc:datafield tag="856" ind1="4" ind2="1">
                                            <marc:subfield code="z">Location: The John Rylands Library, Deansgate</marc:subfield>
                                            <marc:subfield code="u">https://www.library.manchester.ac.uk/rylands/visit/opening-hours/</marc:subfield>
                                        </marc:datafield>                                        

                                        <!--<marc:datafield tag="710" ind1="2" ind2=" ">
                                            <marc:subfield code="a">Main Library.</marc:subfield>
                                            <marc:subfield code="e"></marc:subfield>
                                        </marc:datafield>-->
                                        <marc:datafield tag="856" ind1="4" ind2="1">
                                            <marc:subfield code="z">Location: The University of Manchester Main Library, Oxford Rd</marc:subfield>
                                            <marc:subfield code="u">https://www.library.manchester.ac.uk/locations-and-opening-hours/main-library/</marc:subfield>
                                        </marc:datafield>                                        
                                    </xsl:when>                                    
                                </xsl:choose>
                            </xsl:when>
                            <xsl:when test="exists(section[@class='scg-location']/section[@class='scg-content'])">
                                <xsl:choose>
                                    <xsl:when test="normalize-space(section[@class='scg-location']/section[@class='scg-content'])='JRL'">
                                        <!--<marc:datafield tag="710" ind1="2" ind2=" ">
                                            <marc:subfield code="a">https://www.library.manchester.ac.uk/rylands/visit/opening-hours/</marc:subfield>
                                        </marc:datafield>-->
                                        <marc:datafield tag="856" ind1="4" ind2="1">
                                            <marc:subfield code="z">Location: The John Rylands Library, Deansgate</marc:subfield>
                                            <marc:subfield code="u">https://www.library.manchester.ac.uk/rylands/visit/opening-hours/</marc:subfield>
                                        </marc:datafield>                                        
                                    </xsl:when>
                                    <xsl:when test="normalize-space(section[@class='scg-location']/section[@class='scg-content'])='Main'">
                                        <!--<marc:datafield tag="710" ind1="2" ind2=" ">
                                            <marc:subfield code="a">https://www.library.manchester.ac.uk/locations-and-opening-hours/main-library/</marc:subfield>
                                        </marc:datafield>-->
                                        <marc:datafield tag="856" ind1="4" ind2="1">
                                            <marc:subfield code="z">Location: The University of Manchester Main Library, Oxford Rd</marc:subfield>
                                            <marc:subfield code="u">https://www.library.manchester.ac.uk/locations-and-opening-hours/main-library/</marc:subfield>
                                        </marc:datafield>                                        
                                    </xsl:when>
                                    <xsl:when test="normalize-space(section[@class='scg-location']/section[@class='scg-content'])='Both'">
                                        <!--<marc:datafield tag="710" ind1="2" ind2=" ">
                                            <marc:subfield code="a">https://www.library.manchester.ac.uk/rylands/visit/opening-hours/</marc:subfield>
                                        </marc:datafield>-->
                                        <!--<marc:datafield tag="710" ind1="2" ind2=" ">
                                            <marc:subfield code="a">https://www.library.manchester.ac.uk/locations-and-opening-hours/main-library/</marc:subfield>
                                        </marc:datafield>-->
                                        <marc:datafield tag="856" ind1="4" ind2="1">
                                            <marc:subfield code="z">Location: The John Rylands Library, Deansgate</marc:subfield>
                                            <marc:subfield code="u">https://www.library.manchester.ac.uk/rylands/visit/opening-hours/</marc:subfield>
                                        </marc:datafield>                                        
                                        
                                        <marc:datafield tag="856" ind1="4" ind2="1">
                                            <marc:subfield code="z">Location: The University of Manchester Main Library, Oxford Rd</marc:subfield>
                                            <marc:subfield code="u">https://www.library.manchester.ac.uk/locations-and-opening-hours/main-library/</marc:subfield>
                                        </marc:datafield>                                        
                                    </xsl:when>                                    
                                </xsl:choose>
                            </xsl:when>
                        </xsl:choose>                        
                    </marc:record>
                </xsl:for-each>
                <!--</table>-->
            </marc:collection>
        </xsl:result-document>
    </xsl:template>
<!--    
    <xsl:template match="li">
        <xsl:if test="exists(a)">
            <marc:datafield tag="856" ind1="4" ind2="2">
                <marc:subfield code="z"><xsl:value-of select="a/text()"/></marc:subfield>
                <marc:subfield code="u"><xsl:value-of select="a/@href"/></marc:subfield>
            </marc:datafield>  
        </xsl:if>
    </xsl:template>
-->
    <!-- BA: Need to amend this to copy any internal text in tags??????.... -->
    <xsl:template match="li" mode="related856Text">
        <marc:datafield tag="856" ind1="4" ind2="2">
            <!--<marc:subfield code="z"><xsl:value-of select="./text()"/></marc:subfield>-->
            <marc:subfield code="z"><xsl:copy-of select="."/></marc:subfield>
        </marc:datafield>  
    </xsl:template>

    <xsl:template match="a" mode="related758">
        <marc:datafield tag="758" ind1=" " ind2=" ">
            <marc:subfield code="i"><xsl:value-of select="normalize-space(./text())"/></marc:subfield>
            <!--<marc:subfield code="1"><xsl:value-of select="concat(replace(./@href, 'match=', 'query=title,begins_with,'), ',AND&amp;vid=44MAN_INST:MU_NUI&amp;mode=simple')"/></marc:subfield>-->
            <marc:subfield code="1"><xsl:value-of select="concat($strPrimoRoot, replace(./@href, 'match=', 'query=title,begins_with,'), ',AND&amp;vid=44MAN_INST:MU_NUI&amp;mode=simple')"/></marc:subfield>            
        </marc:datafield>  
    </xsl:template>
    
    <xsl:template match="a" mode="related856">
        <marc:datafield tag="856" ind1="4" ind2="2">
            <marc:subfield code="z"><xsl:value-of select="normalize-space(./text())"/></marc:subfield>
            <xsl:choose>
                <xsl:when test="contains(./@href, 'archives') or contains(./@href, 'luna.manchester')">
                    <marc:subfield code="u"><xsl:value-of select="./@href"/></marc:subfield>                    
                </xsl:when>
                <xsl:when test="./@href = 'http://www.manchester.ac.uk/librarysearch'">
                    <marc:subfield code="u"><xsl:value-of select ="'http://www.manchester.ac.uk/librarysearch?mode=simple'"/></marc:subfield>
                </xsl:when>                                
                <xsl:otherwise>
                    <marc:subfield code="u"><xsl:value-of select="concat(replace(./@href, 'match=', 'query=title,begins_with,'), ',AND&amp;vid=44MAN_INST:MU_NUI&amp;mode=simple')"/></marc:subfield>                    
                </xsl:otherwise>
            </xsl:choose>
        </marc:datafield>  
    </xsl:template>
    
    <!-- BA: Copy element contents - start -->
    <xsl:template match="*">        
        <xsl:choose>
            <xsl:when test=".[@class='scg-content']">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:when test="../../../@class='scg-see-also'">
                <xsl:value-of select="."/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="{name()}" >
                    <xsl:apply-templates select ="@*"/> 
                    <xsl:apply-templates/>
                </xsl:element>                
            </xsl:otherwise>
        </xsl:choose>        
    </xsl:template> 
    
    <xsl:template match="@*" >
        <xsl:attribute name="{name()}" >
            <xsl:choose>
                <xsl:when test="contains(., 'archives') or contains(., 'luna.manchester')">
                    <xsl:value-of select ="normalize-space(.)"/>
                </xsl:when>
                <xsl:when test=". = 'http://www.manchester.ac.uk/librarysearch'">
                    <xsl:value-of select ="'http://www.manchester.ac.uk/librarysearch?mode=simple'"/>
                </xsl:when>                
                <xsl:otherwise>
                    <xsl:value-of select="replace(., 'match=', 'mode=simple&amp;vid=44MAN_INST:MU_NUI&amp;query=title,begins_with,')"/>
                </xsl:otherwise>
            </xsl:choose>            
        </xsl:attribute> 
    </xsl:template> 
    
    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
        <!--<xsl:value-of select="concat(replace(., 'match=', 'mode=simple&amp;vid=44MAN_INST:MU_NUI&amp;query=title,begins_with,'), ',AND')"/>-->
    </xsl:template>
    
    <!-- BA: Copy element contents - end -->
    
</xsl:stylesheet>
