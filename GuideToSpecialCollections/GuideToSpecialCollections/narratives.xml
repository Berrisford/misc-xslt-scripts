<?xml version="1.0" encoding="UTF-8"?>
<table name="enarratives">
   <GuideEntry>
      <order><![CDATA[Wills, W. M., Home Front Diaries]]></order>
      <title><![CDATA[W. M. Wills Home Front Diaries]]></title>
      <DateRange><![CDATA[1903-1919]]></DateRange>
      <Medium><![CDATA[Archive]]></Medium>
      <Description><![CDATA[<section class="scg-content">
            <p>Fifteen volumes of manuscript diaries of Winifred Myers Wills, a member of the
                wealthy Bristol-based tobacco family.</p>
            <p>The pre-war diaries detail the routine of a well-off family in England and the United
                States, with comments on events in the wider world, such as the San Francisco
                earthquake and the sinking of the Titanic. During the First World War the volumes
                document the participation of members of the family in the conflict, and they
                constitute a rare record of home life during the war. </p>
            <p>The diaries are interspersed with photographs, newspaper cuttings, letters and
                postcards.</p>
        </section>]]></Description>
      <See_Also><![CDATA[<section class="scg-content">
            <p/>
        </section>]]></See_Also>
      <FurtherInfo><![CDATA[<section class="scg-content">
            <p>Uncatalogued.</p>
        </section>]]></FurtherInfo>
      <Location><![CDATA[JRL]]></Location>
   </GuideEntry>
   <GuideEntry>
      <order><![CDATA[Women's Suffrage Movement Archives]]></order>
      <title><![CDATA[Women's Suffrage Movement Archives]]></title>
      <DateRange><![CDATA[1892-1923]]></DateRange>
      <Medium><![CDATA[Archive]]></Medium>
      <Description><![CDATA[<section class="scg-content">
            <p>The Library holds four key archives of the women’s suffrage movement:</p>
            <ul>
                <li>The Parliamentary Committee for Women’s Suffrage (1892-1903; 1923);</li>
                <li>The Manchester Men’s League for Women’s Suffrage (1909-18);</li>
                <li>The National Union of Women’s Suffrage Societies (1910-14);</li>
                <li>The International Woman Suffrage Alliance (1913-20).</li>
            </ul>
            <p>
               <b>Parliamentary Committee for Women's Suffrage</b>
            </p>
            <p>The Parliamentary Committee for Women’s Suffrage was founded in December 1893 and
                reached a peak membership of thirty-one MPs in 1897. The Committee began life as a
                non-party organization but in 1897 it became Conservative. Its object was to secure
                the Parliamentary franchise for women and it promoted the passage of all Bills and
                amendments which would further this cause.</p>
            <p>The archive consists of four minute books and a couple of annual reports for the
                period 1895-1903, and a few printed items relating to the women’s suffrage movement,
                dated 1892.</p>
           <p> 
               <b>Manchester Men’s League for Women’s Suffrage</b>
            </p>
            <p>The Manchester Men’s League for Women’s Suffrage was founded in 1908 and was active
                in propaganda activities until the outbreak of the First World War. Initially
                affiliated to the London Men’s League for Women’s Suffrage, it later became
                independent. It dissolved after the passing of the Representation of the People Act
                in 1918.</p>
            <p>The archive consists of minute books, chronological correspondence files, League
                ephemera, ephemera collected by the League and news cuttings indexed by subject,
                covering the period 1909-18. The material occasionally touches on other reform
                campaigns of the early 20th century, such as the campaigns against venereal disease
                and ‘white slavery’, or the procurement of girls for prostitution; lobbying for
                change in the divorce laws; and the temperance movement.</p>
            <p>  
               <b>National Union of Women’s Suffrage Societies</b>
            </p>
            <p>The National Union of Women’s Suffrage Societies was founded in 1897 to provide an
                umbrella organization for the various regional societies devoted to the cause of
                women’s suffrage. Its headquarters were in London and its President was Millicent
                Garrett Fawcett. Its methods were constitutional and non-militant, in contrast to
                the Women’s Social and Political Union (WSPU).</p>
            <p>The archive consists of thirty bound volumes of news cuttings, 1910-14, which offer a
                very full chronological record of the social and political position of women and of
                all aspects of the women’s suffrage movement during this period.</p>
           <p> 
               <b>International Woman Suffrage Alliance</b>
            </p>
            <p>The International Woman Suffrage Alliance was founded in 1902 at the initiative of
                Carrie Chapman Catt, President of the National American Woman Suffrage Association,
                and by the end of 1920 it had affiliated societies in thirty countries throughout
                the world, with its headquarters in London. The aim of the Alliance was to aid the
                enfranchisement of the women of all nations through the international co-operation
                of the national societies. The Alliance held biennial international Congresses,
                published a monthly journal, <em>Jus Suffragii</em>, and ran an international
                Information Bureau.</p>
            <p>The archive consists of almost 300 files from the period 1913-20: subject files
                relating to the work of the IWSA; alphabetical correspondence files; and files of
                news cuttings classified by subject. Britain is the country most fully represented
                in the archive, but there is a wealth of information relating to suffrage movements
                in France, Germany and the United States amongst other countries.</p>
            <p>All the collections are valuable for the study of the women’s suffrage movement in
                Britain. The archives of the MMLWS, the NUWSS and the IWSA give valuable insights
                into the social and economic, as well as political, position of women in the early
                20th century and also touch upon the concerns of other reform movements of the
                period: prostitution and the ‘white slave trade’; divorce law; venereal disease;
                prisons; poor law; and other issues. The archive of the IWSA is also a source for
                the study of international conditions and attitudes during the First World War and
                its immediate aftermath.</p>
        </section>]]></Description>
      <See_Also><![CDATA[<section class="scg-content">
            <p>Correspondence of C. P. Scott with Emmeline and Christabel Pankhurst within the <a href="?match=guardian+formerly+manhester+guardian+archive">Guardian Archive</a>.</p>
        </section>]]></See_Also>
      <FurtherInfo><![CDATA[<section class="scg-content">
            <p>Catalogues available online via ELGAR for:</p>
            <ul>
                <li>
                    <a href="https://archiveshub.jisc.ac.uk/manchesteruniversity/data/gb133-pcws">Parliamentary Committee for Women’s Suffrage</a>
               </li>
                <li>
                    <a href="https://archiveshub.jisc.ac.uk/manchesteruniversity/data/gb133-mml">Manchester Men’s League for Women’s Suffrage</a>
               </li>
                <li>
                    <a href="https://archiveshub.jisc.ac.uk/manchesteruniversity/data/gb133-nuws">National Union of Women’s Suffrage Societies</a>
               </li>
                <li>
                    <a href="https://archiveshub.jisc.ac.uk/manchesteruniversity/data/gb133-iwsa">International Woman Suffrage Alliance</a>
               </li>
            </ul>
               </section>]]></FurtherInfo>
      <Location><![CDATA[JRL]]></Location>
   </GuideEntry>
</table>
