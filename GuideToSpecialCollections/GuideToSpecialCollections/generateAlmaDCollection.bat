@echo off &setlocal
:: BA - Change the directory below to match wherever the scripts are saved 
::cd /D D:\Users\mtlssbe2\eclipse\workspace\test_stuff(misc)\GuideToSpecialCollections 
:: BA - Change directory to where ever the batch file is being run from
cd /D %cd% 

:: BA - Do initial clean (xsl transform)
java -jar "saxon\saxon9pe.jar" -s:emuExport.xml -xsl:xsl\cleanExport.xsl -o:emuExport1.xml
:: BA - emuExport.xml > emuExport1.xml


:: BA - Do replacements 1
set "search0=&lt;?xml version=\"1.0\" encoding=\"UTF-8\"?&gt;"
set "replace0="
set "search=&lt;"
set "replace=<"
set "search2=&gt;"
set "replace2=>"
set "search3=&amp;amp;nbsp;"
set "replace3= "
set "search4=&amp;rsquo;"
set "replace4='"
set "search5=&amp;ndash;"
set "replace5=-"
set "search6=&amp;nbsp;"
set "replace6= " 
set "search7=xmlns"
set "replace7=data-xmlns" 
set "search8=&amp;amp;"
set "replace8=&amp;"

set "textfile=emuExport1.xml"
set "newfile=emuExport2.xml"
(for /f "delims=" %%i in ('findstr "^" "%textfile%"') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    set "line=!line:%search0%=%replace0%!"
    set "line=!line:%search%=%replace%!"
    set "line=!line:%search2%=%replace2%!"
    set "line=!line:%search3%=%replace3%!"
    set "line=!line:%search4%=%replace4%!"
    set "line=!line:%search5%=%replace5%!"
    set "line=!line:%search6%=%replace6%!"
    set "line=!line:%search7%=%replace7%!"
    set "line=!line:%search8%=%replace8%!"
    echo(!line!
    endlocal
))>"%newfile%"
type "%newfile%"
:: BA - emuExport1.xml > emuExport2.xml

:: BA - Do MARC21 xsl mapping transform 
java -jar "saxon\saxon9pe.jar" -s:emuExport2.xml -xsl:xsl\emu2MARC21.xsl -o:narrativesMARC21Sample-unreplaced.xml
:: BA - emuExport2.xml > narrativesMARC21Sample-unreplaced.xml

:: BA - Do replacements 2
set "search2=<p>"
set "replace2=&lt;p&gt;"
set "search3=</p>"
set "replace3=&lt;/p&gt;"
set "search4=</section>"
set "replace4=&lt;/section&gt;"
set "search5=<a href"
set "replace5=&lt;a href"
set "search6=</a>"
set "replace6=&lt;/a&gt;"
set "search7=<em>"
set "replace7=&lt;em&gt;"
set "search8=</em>"
set "replace8=&lt;/em&gt;"
set "search9=<li>"
set "replace9=&lt;li&gt;"
set "search10=</li>"
set "replace10=&lt;/li&gt;"
set "search11=<ul>"
set "replace11=&lt;ul&gt;"
set "search12=</ul>"
set "replace12=&lt;/ul&gt;"

set "textfile=narrativesMARC21Sample-unreplaced.xml"
set "newfile=narrativesMARC21Sample-unreplaced2.xml"
(for /f "delims=" %%i in ('findstr "^" "%textfile%"') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    set "line=!line:%search2%=%replace2%!"
    set "line=!line:%search3%=%replace3%!"
    set "line=!line:%search4%=%replace4%!"
    set "line=!line:%search5%=%replace5%!"
    set "line=!line:%search6%=%replace6%!"
    set "line=!line:%search7%=%replace7%!"
    set "line=!line:%search8%=%replace8%!"
    set "line=!line:%search9%=%replace9%!"
    set "line=!line:%search10%=%replace10%!"
    set "line=!line:%search11%=%replace11%!"
    set "line=!line:%search12%=%replace12%!"
    echo(!line!
    endlocal
))>"%newfile%"
:: type "%newfile%"
:: BA - narrativesMARC21Sample-unreplaced.xml > narrativesMARC21Sample-unreplaced2.xml

:: BA - Do replacements 3 (replacing rogue '>' and p tags from MARC21 787 0\ $t fields) 
java -jar "saxon\saxon9pe.jar" -s:narrativesMARC21Sample-unreplaced2.xml -xsl:xsl\replaceGT.xsl -o:narrativesMARC21Sample.xml
:: BA - narrativesMARC21Sample-unreplaced2.xml > narrativesMARC21Sample.xml


::BA - Delete temporary files...
::del emuExport1.xml
::del emuExport2.xml
::del narratives1.xml
pause
