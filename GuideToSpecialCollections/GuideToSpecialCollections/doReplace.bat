@echo off &setlocal
:: BA - Change the directory below to match wherever the scripts are saved 
::cd /D D:\Users\mtlssbe2\eclipse\workspace\test_stuff(misc)\GuideToSpecialCollections 
:: BA - Change directory to where ever the batch file is being run from
cd /D %cd% 

::set "search=<section class=\\"scg-content\\">"
::set "replace=&lt;section class=\\"scg-content\\"&gt;"
set "search2=<p>"
set "replace2=&lt;p&gt;"
set "search3=</p>"
set "replace3=&lt;/p&gt;"
set "search4=</section>"
set "replace4=&lt;/section&gt;"
set "search5=<a href"
set "replace5=&lt;a href"
set "search6=</a>"
set "replace6=&lt;/a&gt;"
set "search7=<em>"
set "replace7=&lt;em&gt;"
set "search8=</em>"
set "replace8=&lt;/em&gt;"
set "search9=<li>"
set "replace9=&lt;li&gt;"
set "search10=</li>"
set "replace10=&lt;/li&gt;"
set "search11=<ul>"
set "replace11=&lt;ul&gt;"
set "search12=</ul>"
set "replace12=&lt;/ul&gt;"

set "textfile=narrativesMARC21Sample-unreplaced.xml"
::set "newfile=narrativesMARC21Sample.xml"
set "newfile=narrativesMARC21Sample-unreplaced2.xml"
(for /f "delims=" %%i in ('findstr "^" "%textfile%"') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
::    set "line=!line:%search%=%replace%!"
    set "line=!line:%search2%=%replace2%!"
    set "line=!line:%search3%=%replace3%!"
    set "line=!line:%search4%=%replace4%!"
    set "line=!line:%search5%=%replace5%!"
    set "line=!line:%search6%=%replace6%!"
    set "line=!line:%search7%=%replace7%!"
    set "line=!line:%search8%=%replace8%!"
    set "line=!line:%search9%=%replace9%!"
    set "line=!line:%search10%=%replace10%!"
    set "line=!line:%search11%=%replace11%!"
    set "line=!line:%search12%=%replace12%!"
    echo(!line!
    endlocal
))>"%newfile%"
type "%newfile%"

::BA - Delete temporary files...
::del emuExport1.xml
::del emuExport2.xml
::del narratives1.xml
pause
