<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"     xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0">
    <xsl:template match="/">
        <docs>
            <xsl:apply-templates select="response/result/doc"/>
        </docs>    
    </xsl:template>
    <xsl:template match="doc">
        <doc>
            <pid><xsl:value-of select="str[@name='PID']"/></pid>
            <isrestrictedby><xsl:value-of select="str[@name='r.isrestrictedby.displayname']"/></isrestrictedby>
            <submissionstate><xsl:value-of select="str[@name='e.submissionstate']"/></submissionstate>
            <restrictionenddate><xsl:value-of select="str[@name='e.restrictionenddate']"/></restrictionenddate>
            <createddate><xsl:value-of select="substring(date[@name='x.createddate'],0,11)"/></createddate>
           
        </doc>
    </xsl:template>
</xsl:stylesheet>
