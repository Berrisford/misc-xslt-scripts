<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:local="http://localhost" xmlns:saxon="http://saxon.sf.net/" exclude-result-prefixes="#all" version="2.0">
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:output name="serialise1" encoding="utf-8" method="xhtml" indent="no" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:preserve-space elements="text"/>
	<!-- base uri 16-17 33-34 / rows 22 / 18-19 -->
	
	
	<xsl:template match="/">
		
		<xsl:variable name="baseuri" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/etd/select/'"/> 								
		<!--<xsl:variable name="query" select="encode-for-uri('e.isrestricted:Y AND (r.isofetdtype.pid:uk-ac-man-etdtype\:3 OR r.isofetdtype.pid:uk-ac-man-etdtype\:4) AND (e.submissionstate:ACKNOWLEDGED OR e.submissionstate:SENT_TO_LIBRARY OR e.submissionstate:LIBRARY_PROCESSED)&amp;fl=PID,e.submissionstate,e.restrictionenddate,r.isrestrictedby.displayname,r.isofetdtype.pid,r.isofetdtype.displayname,x.createddate,e.date.restricted')"/>-->	
		<xsl:variable name="query" select="encode-for-uri('e.isrestricted:Y AND (r.isofetdtype.pid:uk-ac-man-etdtype\:3 OR r.isofetdtype.pid:uk-ac-man-etdtype\:4) AND (e.submissionstate:ACKNOWLEDGED OR e.submissionstate:SENT_TO_LIBRARY OR e.submissionstate:LIBRARY_PROCESSED OR e.submissionstate:REJECTED)&amp;fl=PID,e.submissionstate,e.restrictionenddate,r.isrestrictedby.displayname,r.isofetdtype.pid,r.isofetdtype.displayname,x.createddate,e.date.restricted,e.comments.restrict,e.comments.restricted,r.isofetdtype.displayname')"/>	
		<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=10000&amp;indent=off')"/>
		
		<xsl:variable name="doc" select="doc($uri)"/>
		
		<xsl:apply-templates select="$doc/response"/>
	</xsl:template>

<!--
	<xsl:template match="/">
		<docs>
			<xsl:apply-templates select="response/result/doc"/>
		</docs>    
	</xsl:template>
-->	
	
	<xsl:template match="response">
		<xsl:result-document href="restricted_eTheses.xml">
			<objects>
				<xsl:apply-templates select="result/doc"/>
			</objects>
		</xsl:result-document>
	</xsl:template>	
	
	<xsl:template match="doc">
		<doc>
			<pid><xsl:value-of select="str[@name='PID']"/></pid>
			<is_restricted_by><xsl:value-of select="str[@name='r.isrestrictedby.displayname']"/></is_restricted_by>
			<submission_state><xsl:value-of select="str[@name='e.submissionstate']"/></submission_state>
			<restriction_end_date><xsl:value-of select="str[@name='e.restrictionenddate']"/></restriction_end_date>
			<created_date><xsl:value-of select="substring(date[@name='x.createddate'],0,11)"/></created_date>
			<!-- e.comments.restrict; e.comments.restricted -->
			<xsl:if test="exists(str[@name='e.comments.restrict'])">
				<restricted_comment><xsl:value-of select="str[@name='e.comments.restrict']"/></restricted_comment>
			</xsl:if>			
			<xsl:if test="exists(arr[@name='e.comments.restricted'])">
				<restricted_comment><xsl:value-of select="arr[@name='e.comments.restricted']/str/."/></restricted_comment>
			</xsl:if>
			<window_type><xsl:value-of select="str[@name='r.isofetdtype.displayname']"/></window_type>			
		</doc>
	</xsl:template>


<!--
	<xsl:template match="response">
		<xsl:result-document href="pg_admins.xml">
			<objects>
				<xsl:apply-templates select="result/doc"/>
			</objects>
		</xsl:result-document>
	</xsl:template>
-->	
	
	
<!--	<xsl:template match="doc">		
		<object>			
			<SPOTID><xsl:value-of select="str[@name='p.partynumber']"/></SPOTID>
			<NAME><xsl:value-of select="str[@name='p.displayname']"/></NAME>
			<EMAIL><xsl:value-of select="str[@name='p.email']"/></EMAIL>
			<!-\-<ROLE><xsl:apply-templates select="arr[@name='p.role']"/></ROLE>-\->
			<!-\-<xsl:apply-templates select="arr[@name='p.role']"/>-\->
			
		</object>
	</xsl:template>
-->	

	<xsl:template match="arr[@name='p.role']">
		<!--<xsl:template match="arr[@name='p.role']" mode="distinct">-->
		<xsl:for-each select="str">		
		<xsl:variable name="count">
			<xsl:number/>
		</xsl:variable>
		
		<xsl:variable name="id_node_name" select="concat('ROLE_', $count)"/>
		<xsl:element name="{$id_node_name}">
			<xsl:value-of select="./text()"></xsl:value-of>
		</xsl:element>
	</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>
