<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:local="http://localhost" xmlns:saxon="http://saxon.sf.net/" exclude-result-prefixes="#all" version="2.0">
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:output name="serialise1" encoding="utf-8" method="xhtml" indent="no" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:preserve-space elements="text"/>
	<!-- base uri 16-17 33-34 / rows 22 / 18-19 -->

	<!-- BA: [07/10/15] - Add qualifications key doc  - start-->
	<xsl:variable name="obj_qualifications">
		<xsl:copy-of select="doc('qualifications.xml')/qualifications"/>
	</xsl:variable>	
	<!-- BA: [07/10/15] - Add qualifications key doc  - end-->
	
	<xsl:template match="/">
		<!-- BA: 14/02/24 - Change/swap baseuri for live deployment !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
		
		<!--<xsl:variable name="baseuri" select="'http://localhost:8085/solr/scw/select/'"/>-->
		<xsl:variable name="baseuri" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/scw/select/'"/> 
		
		
		<xsl:variable name="query" select="encode-for-uri('r.isofcontenttype.pid:uk-ac-man-con\:20 AND m.genre.version:*examination* AND x.createddate:[2018-06-01T00:00:00Z TO NOW] AND -r.isbelongsto.pid:uk-ac-man-per\:87860 AND -r.isbelongsto.pid:uk-ac-man-per\:87861 AND -r.isbelongsto.pid:uk-ac-man-per\:87862 AND -r.isbelongsto.pid:uk-ac-man-per\:87863 AND -r.isbelongsto.pid:uk-ac-man-per\:87864 AND -r.isbelongsto.pid:uk-ac-man-per\:247138 AND -r.isbelongsto.pid:uk-ac-man-per\:245721 AND -r.isbelongsto.pid:uk-ac-man-per\:190812 AND -r.isbelongsto.pid:uk-ac-man-per\:abcde115 AND -r.isbelongsto.pid:uk-ac-man-per\:109112')"/>	
		<!--<xsl:variable name="query" select="encode-for-uri('r.isofcontenttype.pid:uk-ac-man-con\:20 AND m.genre.version:*final* AND -r.isbelongsto.pid:uk-ac-man-per\:87860 AND -r.isbelongsto.pid:uk-ac-man-per\:87861 AND -r.isbelongsto.pid:uk-ac-man-per\:87862 AND -r.isbelongsto.pid:uk-ac-man-per\:87863 AND -r.isbelongsto.pid:uk-ac-man-per\:87864 AND -r.isbelongsto.pid:uk-ac-man-per\:247138 AND -r.isbelongsto.pid:uk-ac-man-per\:245721 AND -r.isbelongsto.pid:uk-ac-man-per\:190812 AND -r.isbelongsto.pid:uk-ac-man-per\:abcde115 AND -r.isbelongsto.pid:uk-ac-man-per\:109112')"/>-->
		<!--<xsl:variable name="query" select="'r.isofcontenttype.pid:uk-ac-man-con%5C:20+AND+m.genre.version:*final*'"/>-->
		<!--<xsl:variable name="query" select="'r.isofcontenttype.pid:uk-ac-man-con%5C:20+AND+m.genre.version:%22Doctoral+level+ETD+-+final%22'"/>-->	
		<!--<xsl:variable name="query" select="'r.isofcontenttype.pid:uk-ac-man-con%5C:20+AND+x.state:Active+AND+m.genre.version:%22Doctoral+level+ETD+-+final%22'"/>-->
		<!--<xsl:variable name="query" select="'PID:uk-ac-man-scw%5C:79954'"/>-->		
		<!--<xsl:variable name="query" select="'r.isofcontenttype.pid:uk-ac-man-con%5C:20 AND x.state:Active AND m.genre.version:%22Doctoral+level+ETD+-+final%22'"/>-->
	    <!--<xsl:variable name="query" select="'x.lastmodifieddate:[NOW-3DAY TO NOW] AND r.isofcontenttype.pid:uk-ac-man-con%5C:20 AND x.state:Active AND m.genre.version:%22Doctoral+level+ETD+-+final%22'"/>-->
		
		<!--  m.genre.version:&quot;Doctoral level ETD - final&quot;-->
		<!-- BA: 14/02/24 - Change/swap rows to run entire set !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
		<!--<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=100000&amp;indent=off')"/>-->
		<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=100000&amp;indent=off')"/>
		
		<!--<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=100&amp;rows=5&amp;indent=off')"/>-->
		<xsl:variable name="doc" select="doc($uri)"/>
		
		<xsl:apply-templates select="$doc/response"/>
	</xsl:template>
	<xsl:template match="response">
<!--		<xsl:result-document href="final_full.xml">-->
		<objects>
			<!--<xsl:text>&#xa;</xsl:text>-->			
			<!--<xsl:comment>
				BA: 14/02/24 - NB: Direct mapping from solr result where "." is replaced with "_" here to form element names.
				Prefixes correspond to the different datastreams: 
				X_ = properties (audit)
				R_ = relationships
				M_ = mods
				F_ = full text
				D_ = ETD (eThesis) Declaration
				E_ = ETD (eThesis)
			</xsl:comment>-->			
			<!--<xsl:text>&#xa;</xsl:text>-->						
			<xsl:apply-templates select="result/doc"/>
		</objects>
		<!--</xsl:result-document>-->
	</xsl:template>
	<xsl:template match="doc">
		<!-- BA: 14/02/24 - Change/swap baseurietd for live deployment !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
		<xsl:variable name="baseurietd" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/etd/select/'"/> 
		<!--<xsl:variable name="baseurietd" select="'http://localhost:8085/solr/etd/select/'"/>-->

		<!-- BA: 14/02/24 - Change/swap baseuri_nonscw for live deployment !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
		<xsl:variable name="baseuri_nonscw" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/nonscw/select/'"/> 
		<!--<xsl:variable name="baseuri_nonscw" select="'http://localhost:8085/solr/nonscw/select/'"/>--> 
		

		<xsl:variable name="pid"><xsl:value-of select="arr[@name='r.isbelongstoetdwindow.pid']/str"/></xsl:variable>
		<!--<xsl:variable name="queryetd" select="concat('PID:',replace($pid,':','?'), '%20AND%0D%0A%28r.isofetdtype.pid%3Auk-ac-man-etdtype%5C%3A3%20OR%20r.isofetdtype.pid%3Auk-ac-man-etdtype%5C%3A4%29%20AND%0D%0A%21e.windowstate%3AEXPIRED%20AND%0D%0A%21e.windowstate%3ACANCELLED')"/>-->
		<!-- (r.isofetdtype.pid:uk-ac-man-etdtype\:3 OR r.isofetdtype.pid:uk-ac-man-etdtype\:4) AND !e.windowstate:EXPIRED AND !e.windowstate:CANCELLED AND (e.submissionstate:REJECTED OR e.submissionstate:ACKNOWLEDGED OR e.submissionstate:SUBMITTED OR e.submissionstate:SENT_TO_LIBRARY OR e.submissionstate:LIBRARY_PROCESSED) -->
		<!--<xsl:variable name="queryetd" select="concat('PID:',replace($pid,':','?'), encode-for-uri(' AND (r.isofetdtype.pid:uk-ac-man-etdtype\:3 OR r.isofetdtype.pid:uk-ac-man-etdtype\:4) AND !e.windowstate:EXPIRED AND !e.windowstate:CANCELLED'))"/>-->
		<xsl:variable name="queryetd" select="concat('PID:',replace($pid,':','?'), encode-for-uri(' AND (r.isofetdtype.pid:uk-ac-man-etdtype\:1 OR r.isofetdtype.pid:uk-ac-man-etdtype\:2) AND !e.windowstate:EXPIRED AND !e.windowstate:CANCELLED AND (e.submissionstate:SUBMITTED)'))"/>				
		<!--		<xsl:variable name="queryetd" select="concat('PID:',replace($pid,':','?'), encode-for-uri(' AND (r.isofetdtype.pid:uk-ac-man-etdtype\:3 OR r.isofetdtype.pid:uk-ac-man-etdtype\:4) AND !e.windowstate:EXPIRED AND !e.windowstate:CANCELLED AND (e.submissionstate:SUBMITTED)'))"/>-->
		<!--<xsl:variable name="queryetd" select="concat('PID:',replace($pid,':','?'), encode-for-uri(' AND (r.isofetdtype.pid:uk-ac-man-etdtype\:3 OR r.isofetdtype.pid:uk-ac-man-etdtype\:4) AND !e.windowstate:EXPIRED AND !e.windowstate:CANCELLED AND (e.submissionstate:REJECTED OR e.submissionstate:ACKNOWLEDGED OR e.submissionstate:SUBMITTED OR e.submissionstate:SENT_TO_LIBRARY OR e.submissionstate:LIBRARY_PROCESSED)'))"/>-->
		<xsl:variable name="urietd" select="concat($baseurietd,'?q=',$queryetd,'&amp;version=2.2&amp;start=0&amp;rows=1&amp;indent=off')"/>
		<xsl:variable name="docetd">
			<xsl:copy-of select="doc($urietd)/response/result/doc"/>
		</xsl:variable>		
		
		<xsl:if test="exists($docetd/doc/str[@name='PID'])">					
			<object>			
				<!--				<xsl:text>&#xa;</xsl:text>
				<xsl:comment>
					BA: 14/02/24 - (NEW) Add all scholarly works (scw) elements for eProg Migration - start
				</xsl:comment>
				<xsl:text>&#xa;</xsl:text>
-->				<!-- BA: 14/02/24 - (NEW) Add all scholarly works (scw) elements for eProg Migration - start -->
				<!--<scw>-->
				<!--<xsl:comment>PID (Item Unique ID)</xsl:comment>-->
				<xsl:text>&#xa;</xsl:text>	
<!--				<PID><xsl:apply-templates select="str[@name='PID']"/></PID>-->
				<!--<xsl:comment>State: active = publicly accessible; inactive = not publicly accessible</xsl:comment>-->
<!--				<STATE><xsl:apply-templates select="str[@name='x.state']"/></STATE>-->
				<!--<xsl:comment>Created date</xsl:comment>-->
<!--				<CREATEDDATE><xsl:apply-templates select="date[@name='x.createddate']"/></CREATEDDATE>-->
				<!--<xsl:comment>Is belongs to</xsl:comment>-->			
<!--				<STUDENT_SPOTID><xsl:apply-templates select="arr[@name='r.isbelongsto.source']"/></STUDENT_SPOTID>					-->
				<!--<xsl:comment>Has main supervisor</xsl:comment>-->
<!--				<MAIN_SUPERVISOR_SPOTID><xsl:apply-templates select="arr[@name='r.hasmainsupervisor.source']"/></MAIN_SUPERVISOR_SPOTID>					-->
				<!--<xsl:comment>Has co supervisor</xsl:comment>-->					
<!--				<COSUPERVISOR_SPOTIDS><xsl:apply-templates select="arr[@name='r.hascosupervisor.source']" mode="raw"/></COSUPERVISOR_SPOTIDS>-->
				<!--<xsl:comment>Is belongs to organisation</xsl:comment>-->
<!--				<ORG_IDS><xsl:apply-templates select="arr[@name='r.isbelongstoorg.source']"/></ORG_IDS>-->
<!--				<ABSTRACT><xsl:apply-templates select="str[@name='m.abstract']"/></ABSTRACT>-->
<!--				<VERSION><xsl:apply-templates select="str[@name='m.genre.version']"/></VERSION>-->
<!--				<FORMAT><xsl:apply-templates select="str[@name='m.genre.form']"/></FORMAT>-->
				<!--<xsl:comment>Language term</xsl:comment>-->
<!--				<LANGUAGE_CODE><xsl:apply-templates select="str[@name='m.languageterm.code']"/></LANGUAGE_CODE>-->
				<!--<xsl:comment>Originator (Main supervisor)</xsl:comment>-->
<!--				<MAIIN_SUPERVISOR_NAME><xsl:apply-templates select="arr[@name='m.name.org']"/></MAIIN_SUPERVISOR_NAME>-->
				<!--<M_NAME_ORG_SOURCE><xsl:apply-templates select="arr[@name='m.name.org.source']"/></M_NAME_ORG_SOURCE>-->
				<!--<xsl:comment>Co supervisor</xsl:comment>-->			
<!--				<CO_SUPERVISOR_NAMES><xsl:apply-templates select="arr[@name='m.name.ths']" mode="raw"/></CO_SUPERVISOR_NAMES>-->
				<!--<M_NAME_THS_SOURCE><xsl:apply-templates select="arr[@name='m.name.ths.source']" mode="raw"/></M_NAME_THS_SOURCE>-->
				<!--<xsl:comment>Author</xsl:comment>-->			
<!--				<AUTHOR_NAME><xsl:apply-templates select="arr[@name='m.name.aut']"/></AUTHOR_NAME>-->
				<!--<M_NAME_AUT_SOURCE><xsl:apply-templates select="arr[@name='m.name.aut.source']"/></M_NAME_AUT_SOURCE>-->
				<!--<xsl:comment>Funder</xsl:comment>-->			
<!--				<FUNDER_NAMES><xsl:apply-templates select="arr[@name='m.name.fnd']" mode="raw"/></FUNDER_NAMES>-->
				<!--<M_NAME_FND_SOURCE><xsl:apply-templates select="arr[@name='m.name.fnd.source']" mode="raw"/></M_NAME_FND_SOURCE>-->			
				<!--<xsl:comment>Degree granter</xsl:comment>-->			
<!--				<DEGREE_GRANTOR><xsl:apply-templates select="arr[@name='m.name.dgg']"/></DEGREE_GRANTOR>-->
				<!--<M_NAME_DGG_SOURCE><xsl:apply-templates select="arr[@name='m.name.dgg.source']"/></M_NAME_DGG_SOURCE>-->
				<!--<xsl:comment>Degree level</xsl:comment>-->			
<!--				<DEGREELEVEL><xsl:apply-templates select="str[@name='m.note.degreelevel']"/></DEGREELEVEL>-->
				<!--<xsl:comment>Degree programme</xsl:comment>-->
<!--				<DEGREEPROGRAMME><xsl:apply-templates select="str[@name='m.note.degreeprogramme']"/></DEGREEPROGRAMME>-->
				<!--<xsl:comment>Digital material not submitted</xsl:comment>-->
<!--				<DIGITALMATERIALNOTSUBMITTED><xsl:apply-templates select="str[@name='m.note.digitalmaterialnotsubmitted']"/></DIGITALMATERIALNOTSUBMITTED>-->
				<!--<xsl:comment>Non-Digital material not submitted</xsl:comment>-->
<!--				<NONDIGITALMATERIALNOTSUBMITTED><xsl:apply-templates select="str[@name='m.note.nondigitalmaterialnotsubmitted']"/></NONDIGITALMATERIALNOTSUBMITTED>-->
<!--				<PUBLISHER><xsl:apply-templates select="str[@name='m.publisher']"/></PUBLISHER>-->
<!--				<PAGE_TOTAL><xsl:apply-templates select="str[@name='m.page.total']"/></PAGE_TOTAL>-->
				<!--<xsl:comment>Record content source</xsl:comment>-->
<!--				<RECORD_CONTENT_SOURCE><xsl:apply-templates select="str[@name='m.recordcontentsource']"/></RECORD_CONTENT_SOURCE>-->
<!--				<TOPIC><xsl:apply-templates select="arr[@name='m.topic']" mode="raw"/></TOPIC>-->
<!--				<TITLE><xsl:apply-templates select="str[@name='m.title']"/></TITLE>-->
				<!--<xsl:comment>Terms and conditions accepted</xsl:comment>-->
<!--				<TERMS_AND_CONDITIONS_ACCEPTED><xsl:apply-templates select="str[@name='d.termsandconditionsaccepted']"/></TERMS_AND_CONDITIONS_ACCEPTED>-->
				<!--<xsl:comment>No copyright infringement</xsl:comment>-->
<!--				<NO_COPYRIGHT_INFRINGEMENT><xsl:apply-templates select="str[@name='d.nocopyrightinfringement']"/></NO_COPYRIGHT_INFRINGEMENT>-->
				<!--<xsl:comment>Copyright comment</xsl:comment>-->
<!--				<COPYRIGHTCOMMENT><xsl:apply-templates select="str[@name='d.copyrightcomment']"/></COPYRIGHTCOMMENT>-->
				<!--<xsl:comment>No third party agreements</xsl:comment>-->
<!--				<NO_THIRD_PARTY_AGGREEMENTS><xsl:apply-templates select="str[@name='d.nothirdpartyaggreements']"/></NO_THIRD_PARTY_AGGREEMENTS>-->
				<!--<xsl:comment>True and accurate representation</xsl:comment>-->
<!--				<TRUE_AND_ACCURATE_REPRESENTATION><xsl:apply-templates select="str[@name='d.trueandaccuraterepresentation']"/></TRUE_AND_ACCURATE_REPRESENTATION>-->
				<!--<xsl:comment>Access restriction - duration</xsl:comment>-->					
<!--				
				<xsl:choose>					
					<xsl:when test="(not(exists(str[@name='d.accessrestriction.duration'])) or str[@name='d.accessrestriction.duration'] = '') and str[@name='d.permissiontodownload'] = 'Y' ">
						<ACCESSRESTRICTION_DURATION>Immediate Open Access</ACCESSRESTRICTION_DURATION>						
					</xsl:when>
					<xsl:otherwise>
						<ACCESSRESTRICTION_DURATION><xsl:apply-templates select="str[@name='d.accessrestriction.duration']"/></ACCESSRESTRICTION_DURATION>						
					</xsl:otherwise>
				</xsl:choose>
-->				
				<!--<xsl:comment>Access restriction - reason</xsl:comment>-->
<!--				<ACCESSRESTRICTION_REASON><xsl:apply-templates select="str[@name='d.accessrestriction.reason']"/></ACCESSRESTRICTION_REASON>-->
				<!--<xsl:comment>Access restriction - reason - other</xsl:comment>-->
<!--				<ACCESSRESTRICTION_REASON_OTHER><xsl:apply-templates select="str[@name='d.accessrestriction.reasonother']"/></ACCESSRESTRICTION_REASON_OTHER>-->
				<!--<xsl:comment>Blocked career</xsl:comment>-->
				<!--<BLOCKED_CAREER><xsl:apply-templates select="str[@name='d.blockedcareer']"/></BLOCKED_CAREER>-->
				<!--<xsl:comment>Initial restriction duration</xsl:comment>-->
				<!--<INITIAL_RESTRICTION_DURATION><xsl:apply-templates select="str[@name='d.initialrestrictionduration']"/></INITIAL_RESTRICTION_DURATION>-->
				<!--<xsl:comment>Embargo approval status</xsl:comment>-->
				<!--<EMBARGOAPPROVALSTATUS><xsl:apply-templates select="str[@name='d.embargoapprovalstatus']"/></EMBARGOAPPROVALSTATUS>-->
				<!--<xsl:comment>Supervisor overrides</xsl:comment>-->
				<!--<SUPERVISOR_OVERRIDES><xsl:apply-templates select="str[@name='d.supervisoroverrides']"/></SUPERVISOR_OVERRIDES>-->
				<!--<xsl:comment>Embargo approved date</xsl:comment>-->
				<!--<EMBARGO_APPROVED_DATE><xsl:apply-templates select="str[@name='d.embargoapproveddate']"/></EMBARGO_APPROVED_DATE>-->
				<!--<xsl:comment>Embargo approved by</xsl:comment>-->
				<!--<EMBARGO_APPROVEDBY><xsl:apply-templates select="str[@name='d.embargoapprovedby']"/></EMBARGO_APPROVEDBY>-->
				
				<!--				<xsl:text>&#xa;</xsl:text>			
				<xsl:text>&#xa;</xsl:text>							
				<xsl:comment>BA: 15/02/24 - Legacy fields - start</xsl:comment>
				<xsl:text>&#xa;</xsl:text>						
-->				<!--<AUTHOR_ID><xsl:apply-templates select="arr[@name='r.isbelongsto.source']"/></AUTHOR_ID>--> 
				<!--<PERSON_ID><xsl:apply-templates select="arr[@name='r.isbelongsto.source']"/></PERSON_ID>--> 
<!--				<FILEURL><xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?dsid=FULL-TEXT.PDF&amp;pid=' , str[@name='PID'])"/></FILEURL>-->
				<!--				<xsl:text>&#xa;</xsl:text>										
				<xsl:comment>BA: 15/02/24 - Legacy fields - end</xsl:comment>			
				<xsl:text>&#xa;</xsl:text>															
				<!-\-</scw>-\->	
				<xsl:text>&#xa;</xsl:text>															
				<xsl:comment>BA: 14/02/24 - (NEW) Add all scholarly works (scw) elements for eProg Migration - end</xsl:comment> 						
				<!-\- BA: 14/02/24 - (NEW) Add all scholarly works (scw) elements for eProg Migration - end -\->
	
				<xsl:text>&#xa;</xsl:text>				
				<xsl:text>&#xa;</xsl:text>							
				<xsl:comment>BA: 19/02/24 - (NEW) Add all ETD elements for eProg Migration - start</xsl:comment> 						
				<!-\- BA: 19/02/24 - (NEW) Add all ETD elements for eProg Migration - start -\->
				<xsl:text>&#xa;</xsl:text>					
-->
				<!--<etd>-->
				<!--<xsl:comment>is of ETD type - display name</xsl:comment>-->			
<!--				<WINDOW_TYPE><xsl:apply-templates select="$docetd/doc/str[@name='r.isofetdtype.displayname']"/></WINDOW_TYPE>-->
				<!-- BA: 20/02/24 - Fetch created by SPOT ID - start -->
				<xsl:variable name="created_by_pid">
					<xsl:value-of select="$docetd/doc/str[@name='r.iscreatedby.pid']"/>
				</xsl:variable>
				
				<xsl:variable name="created_by_query">
					<xsl:value-of select="concat('PID:',replace($created_by_pid,':','?'))"/>						
				</xsl:variable> 	
				
				<xsl:variable name="created_by_uri" select="concat($baseuri_nonscw,'?q=',$created_by_query,'&amp;version=2.2&amp;fl=p.partynumber&amp;wt=xml&amp;start=0&amp;rows=1&amp;indent=off')"/>
				
				<xsl:variable name="doc_created_by">
					<xsl:copy-of select="doc($created_by_uri)/response/result/doc"/>
				</xsl:variable>											
				<!--<test><xsl:value-of select="$doc_created_by/doc/str[@name='p.partynumber']"></xsl:value-of></test>-->
				<!-- BA: 20/02/24 - Fetch created by SPOT ID - end -->					
				<!--<xsl:comment>is created by - SPOT ID</xsl:comment>-->
				<WINDOW_ID><xsl:apply-templates select="$docetd/doc/str[@name='PID']"/></WINDOW_ID>
				<STUDENT_SPOTID><xsl:apply-templates select="arr[@name='r.isbelongsto.source']"/></STUDENT_SPOTID>					
				
<!--				<WINDOW_CREATEDBY><xsl:apply-templates select="$doc_created_by/doc/str[@name='p.partynumber']"/></WINDOW_CREATEDBY>-->
				<!--<R_ISCREATEDBY_DISPLAYNAME><xsl:apply-templates select="$docetd/doc/str[@name='r.iscreatedby.displayname']"/></R_ISCREATEDBY_DISPLAYNAME>-->
				<!--<xsl:comment>is submitted by - spot id</xsl:comment>-->
<!--				<WINDOW_SUBMITTEDBY><xsl:apply-templates select="arr[@name='r.isbelongsto.source']"/></WINDOW_SUBMITTEDBY>-->
									
					<!--<xsl:comment>is opened by - spot id</xsl:comment>-->
				<!-- BA: 20/02/24 - Fetch opened by SPOT ID - start -->
				<xsl:variable name="opened_by_pid">
					<xsl:value-of select="$docetd/doc/str[@name='r.iscreatedby.pid']"/>
				</xsl:variable>
				
				<xsl:variable name="opened_by_query">
					<xsl:value-of select="concat('PID:',replace($opened_by_pid,':','?'))"/>						
				</xsl:variable> 	
				
				<xsl:variable name="opened_by_uri" select="concat($baseuri_nonscw,'?q=',$opened_by_query,'&amp;version=2.2&amp;fl=p.partynumber&amp;wt=xml&amp;start=0&amp;rows=1&amp;indent=off')"/>
				
				<xsl:variable name="doc_opened_by">
					<xsl:copy-of select="doc($opened_by_uri)/response/result/doc"/>
				</xsl:variable>											
				<!-- BA: 20/02/24 - Fetch created by SPOT ID - end -->					
				<WINDOW_OPENEDBY><xsl:apply-templates select="$doc_opened_by/doc/str[@name='p.partynumber']"/></WINDOW_OPENEDBY>
<!--					
				<xsl:if test="exists($docetd/doc/str[@name='r.isacknowledgedby.pid'])">
					<!-\-<xsl:comment>is acknowledged by - SPOT ID</xsl:comment>-\->
					<!-\- BA: 20/02/24 - Fetch opened by SPOT ID - start -\->
					<xsl:variable name="acknowledged_by_pid">
						<xsl:value-of select="$docetd/doc/str[@name='r.isacknowledgedby.pid']"/>
					</xsl:variable>
					
					<xsl:variable name="acknowledged_by_query">
						<xsl:value-of select="concat('PID:',replace($acknowledged_by_pid,':','?'))"/>						
					</xsl:variable> 	
					
					<xsl:variable name="acknowledged_by_uri" select="concat($baseuri_nonscw,'?q=',$acknowledged_by_query,'&amp;version=2.2&amp;fl=p.partynumber&amp;wt=xml&amp;start=0&amp;rows=1&amp;indent=off')"/>
					
					<xsl:variable name="doc_opened_by">
						<xsl:copy-of select="doc($acknowledged_by_uri)/response/result/doc"/>
					</xsl:variable>
					
					<!-\- BA: 20/02/24 - Fetch created by SPOT ID - end -\->					
<!-\-					<WINDOW_ACKNOWLEDGEDBY><xsl:apply-templates select="$doc_opened_by/doc/str[@name='p.partynumber']"/></WINDOW_ACKNOWLEDGEDBY>-\->
				</xsl:if>
-->				
				<!--<R_ISACKNOWLEDGEDBY_DISPLAYNAME><xsl:apply-templates select="$docetd/doc/str[@name='r.isacknowledgedby.displayname']"/></R_ISACKNOWLEDGEDBY_DISPLAYNAME>-->
				
					
				<!--<xsl:comment>qualification </xsl:comment>-->
<!--				<WINDOW_QUALIFICATION><xsl:apply-templates select="$docetd/doc/str[@name='e.qualification']"/></WINDOW_QUALIFICATION>-->
				<!--<xsl:comment>programme name </xsl:comment>-->
<!--				<WINDOW_PROGRAMMENAME><xsl:apply-templates select="$docetd/doc/str[@name='e.programmename']"/></WINDOW_PROGRAMMENAME>-->
				<!--<xsl:comment>programme id </xsl:comment>-->
<!--				<WINDOW_PROGRAMMEID><xsl:apply-templates select="$docetd/doc/str[@name='e.programmeid']"/></WINDOW_PROGRAMMEID>-->
				<!--<xsl:comment>plan name </xsl:comment>-->
<!--				<WINDOW_PLANNAME><xsl:apply-templates select="$docetd/doc/str[@name='e.planname']"/></WINDOW_PLANNAME>-->
				<!--<xsl:comment>plan id </xsl:comment>-->
<!--				<WINDOW_PLANID><xsl:apply-templates select="$docetd/doc/str[@name='e.planid']"/></WINDOW_PLANID>-->
				<!--<xsl:comment>is restricted </xsl:comment>-->
<!--				<WINDOW_ISRESTRICTED><xsl:apply-templates select="$docetd/doc/str[@name='e.isrestricted']"/></WINDOW_ISRESTRICTED>-->
				<!--<xsl:comment>submission state </xsl:comment>-->
<!--				<WINDOW_SUBMISSIONSTATE><xsl:apply-templates select="$docetd/doc/str[@name='e.submissionstate']"/></WINDOW_SUBMISSIONSTATE>-->
				<!--<xsl:comment>expected open date </xsl:comment>-->
				<!--<xsl:comment>dates... </xsl:comment>-->
				<WINDOW_DATE_CREATED><xsl:apply-templates select="$docetd/doc/str[@name='e.date.created']"/></WINDOW_DATE_CREATED>
<!--				<WINDOW_DATE_OPENED><xsl:apply-templates select="$docetd/doc/str[@name='e.date.opened']"/></WINDOW_DATE_OPENED>-->
				<WINDOW_DATE_SUBMITTED><xsl:apply-templates select="$docetd/doc/str[@name='e.date.submitted']"/></WINDOW_DATE_SUBMITTED>
<!--				<WINDOW_DATE_ACKNOWLEDGED><xsl:apply-templates select="$docetd/doc/str[@name='e.date.acknowledged']"/></WINDOW_DATE_ACKNOWLEDGED>-->
<!--				<WINDOW_DATE_SENT-TO-LIBRARY><xsl:apply-templates select="$docetd/doc/str[@name='e.date.sent_to_library']"/></WINDOW_DATE_SENT-TO-LIBRARY>-->
				<WINDOW_TYPE><xsl:apply-templates select="$docetd/doc/str[@name='r.isofetdtype.displayname']"/></WINDOW_TYPE>
				<SUBMISSION_STATE><xsl:apply-templates select="$docetd/doc/str[@name='e.submissionstate']"/></SUBMISSION_STATE>
				
				<xsl:if test="not($docetd/doc/str[@name='e.submissionstate'] = 'SENT_TO_LIBRARY')">
<!--					<WINDOW_DATE_LIBRARY-PROCESSED><xsl:apply-templates select="$docetd/doc/str[@name='e.date.library_processed']"/></WINDOW_DATE_LIBRARY-PROCESSED>					-->
				</xsl:if>				
<!--				<WINDOW_DATE_REJECTED><xsl:apply-templates select="$docetd/doc/str[@name='e.date.rejected']"/></WINDOW_DATE_REJECTED>-->
<!--				<xsl:if test="contains($docetd/doc/str[@name='r.isofetdtype.displayname'], 're-classification')">-->
<!--					<WINDOW_COMMENTS_RESTRICTED><xsl:apply-templates select="$docetd/doc/str[@name='e.comments.restrict']"/></WINDOW_COMMENTS_RESTRICTED>-->
<!--					<WINDOW_COMMENTS_ACKNOWLEDGED><xsl:apply-templates select="$docetd/doc/str[@name='e.comments.acknowledged']"/></WINDOW_COMMENTS_ACKNOWLEDGED>-->
<!--					<WINDOW_COMMENTS_REJECTED><xsl:apply-templates select="$docetd/doc/str[@name='e.comments.rejected']"/></WINDOW_COMMENTS_REJECTED>-->
<!--					<WINDOW_COMMENTS_SENT-TO-LIBRARY><xsl:apply-templates select="$docetd/doc/str[@name='e.comments.sent_to_library']"/></WINDOW_COMMENTS_SENT-TO-LIBRARY>-->
<!--					<WINDOW_COMMENTS_LIBRARY-PROCESSED><xsl:apply-templates select="$docetd/doc/str[@name='e.comments.library_processed']"/></WINDOW_COMMENTS_LIBRARY-PROCESSED>					-->
				<!--</xsl:if>-->
<!--				<PHYSICAL-LOCATION>The University of Manchester John Rylands University Library</PHYSICAL-LOCATION>-->
<!--				<WINDOW_EMBARGO-END-DATE><xsl:apply-templates select="$docetd/doc/str[@name='e.restrictionenddate']"/></WINDOW_EMBARGO-END-DATE>-->
				
				<!--</etd>-->
				<!--				<xsl:comment>BA: 19/02/24 - (NEW) Add all ETD elements for eProg Migration - end</xsl:comment> 						
				<xsl:text>&#xa;</xsl:text>				
-->				<!-- BA: 19/02/24 - (NEW) Add all ETD elements for eProg Migration - end -->
				
			</object>
		</xsl:if>
	</xsl:template>
	<xsl:template match="arr[@name='r.isbelongsto.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.hasmainsupervisor.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.hascosupervisor.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.fnd']">
		<xsl:param name="pid"/>
		<xsl:for-each select="str">
			<sponsor>
				<xsl:value-of select="concat($pid,'_', replace(.,' ',''))"/>
			</sponsor>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.topic']">
		<xsl:for-each select="str">
			<topic>
				<xsl:value-of select="."/>
			</topic>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='r.wasbelongstoorg.source'] | arr[@name='r.isbelongstoorg.source']">
		<xsl:for-each select="str">
			<org>
				<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
			</org>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.org'] | arr[@name='m.name.org.source'] | arr[@name='r.hasmainsupervisor.pid'] | arr[@name='r.hasmainsupervisor.source']" mode="raw">
		<xsl:for-each select="str">
			<mainsupervisor>
				<xsl:value-of select="."/>
			</mainsupervisor>		
		</xsl:for-each>
	</xsl:template>	
	<xsl:template match="arr[@name='r.hascosupervisor.pid'] | arr[@name='m.name.ths'] | arr[@name='m.name.ths.source']" mode="raw">
		<xsl:for-each select="str">
			<cosupervisor>
				<xsl:value-of select="."/>
			</cosupervisor>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='r.hascosupervisor.source']" mode="raw">
		<xsl:for-each select="str">
			<cosupervisor>
				<xsl:value-of select="tokenize(.,'\|\|')[3]"/>								
			</cosupervisor>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.aut'] | arr[@name='m.name.aut.source']">
		<xsl:for-each select="str">
			<!--<author>-->
				<xsl:value-of select="."/>
			<!--</author>-->		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.fnd'] | arr[@name='m.name.fnd.source']" mode="raw">
		<xsl:for-each select="str">
			<funder>
				<xsl:value-of select="."/>
			</funder>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.dgg'] | arr[@name='m.name.dgg.source']">
		<xsl:for-each select="str">
			<!--<institution>-->
				<xsl:value-of select="."/>
			<!--</institution>-->		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.topic']" mode="raw">
		<xsl:for-each select="str">
			<keyword>
				<xsl:value-of select="."/>
			</keyword>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.typeofresource']">
		<xsl:for-each select="str">
			<type>
				<xsl:value-of select="."/>
			</type>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.isfileattached']">
		<xsl:for-each select="str">
			<is-attached>
				<xsl:value-of select="."/>
			</is-attached>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.id']">
		<xsl:for-each select="str">
			<id>
				<xsl:value-of select="."/>
			</id>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.mimetype']">
		<xsl:for-each select="str">
			<mime-type>
				<xsl:value-of select="."/>
			</mime-type>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.size']">
		<xsl:for-each select="double">
			<size>
				<xsl:value-of select="."/>
			</size>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.state']">
		<xsl:for-each select="str">
			<state>
				<xsl:value-of select="."/>
			</state>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.source']">
		<xsl:for-each select="str">
			<source>
				<xsl:value-of select="."/>
			</source>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='r.islibraryprocessedby.pid']">
		<xsl:for-each select="str">
			<pid>
				<xsl:value-of select="."/>
			</pid>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='x.label']">
		<xsl:for-each select="str">
			<txt>
				<xsl:value-of select="."/>
			</txt>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='x.ownerid']">
		<xsl:for-each select="str">
			<id>
				<xsl:value-of select="."/>
			</id>		
		</xsl:for-each>
	</xsl:template>
	


	<!-- BA: 07/01/16 - Define explicit templates for title and abstract so as to do manual escape of extended ASCII chars - start -->
	<xsl:template match="str[@name='m.abstract'] | str[@name='m.title']">
		<xsl:variable name="find">‘’“”„–—−-čłśćęİşńĭŠḥīźṣā™′ğÖ†Å</xsl:variable>
		<xsl:variable name="replace">''"""----clsceIsniShizsa 'gO A</xsl:variable>
		<xsl:value-of select="replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(translate(.,$find,$replace),'α','alpha'),'κ','kappa'),'∞','infinity'),'œ','oe'),'λ','Gamma'),'…','...'),'ﬁ','fi'),'σ','sigma'),'π','pie'),'μm','micro metre'),'Δ','Delta'),'≥','greater than or equal to'),'Œ','OE'),'≤','smaller or equal to'),'δ','delta')"/>		
	</xsl:template>
	<!-- BA: 07/01/16 - Define explicit templates for title and abstract so as to do manual escape of extended ASCII chars - end -->
	<xsl:template match="arr[@name='r.hasmainsupervisor.source']/str">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.hascosupervisor.source']/str">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
</xsl:stylesheet>
