<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="3.0">
    <xsl:output indent="yes" method="xml"/>
<!--    
    <xsl:variable name="spot_ids">
        <xsl:copy-of select="doc('./attr_spot-ids.xml')/spot_ids"/>
    </xsl:variable>
-->    

    <xsl:variable name="pure_feed">
        <xsl:copy-of select="doc('./pure_feed_min.xml')/objects"/>
    </xsl:variable>
    

    <xsl:template match="/">
        <xsl:result-document href="pure_feed_analysis.xml">
        <pure_feed_analysis>
            <xsl:apply-templates select="spot_ids/spot_id"/>
        </pure_feed_analysis>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="spot_id">
        <item>
        <xsl:variable name="current_id" select="."/>
        <xsl:choose>
            <!--<xsl:when test="exists($excluded_pids/excluded_pids/pid[@id = $pid])">-->
            <xsl:when test="exists($pure_feed/objects/object[@spot_id = $current_id])">
                <!--<current-id><xsl:value-of select="$current_id"/></current-id>-->
                <!--<in_Pure><xsl:value-of select="."/></in_Pure>-->
                <in_Pure><xsl:value-of select="'True'"/></in_Pure>
                <spot_id><xsl:value-of select="$pure_feed/objects/object[@spot_id = $current_id]/spot_id/text()"/></spot_id>
                <scw_pid><xsl:value-of select="$pure_feed/objects/object[@spot_id = $current_id]/scw_pid/text()"/></scw_pid>
                <programme_id><xsl:value-of select="$pure_feed/objects/object[@spot_id = $current_id]/programme_id/text()"/></programme_id>
                <plan_id><xsl:value-of select="$pure_feed/objects/object[@spot_id = $current_id]/plan_id/text()"/></plan_id>                
            </xsl:when>
            <xsl:otherwise>
                <!--<current-id><xsl:value-of select="$current_id"/></current-id>-->
                <in_Pure><xsl:value-of select="'False'"/></in_Pure>
                <spot_id><xsl:value-of select="$current_id"/></spot_id>
            </xsl:otherwise>
        </xsl:choose>        
        </item>
    </xsl:template>

</xsl:stylesheet>