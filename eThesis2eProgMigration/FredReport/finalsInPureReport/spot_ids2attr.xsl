<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="3.0">
    <xsl:output indent="yes" method="xml"/>    

    <xsl:template match="/">
        <xsl:result-document href="attr_spot-ids.xml">
        <spot_ids>
            <xsl:apply-templates select="spot_ids/spot_id"/>
        </spot_ids>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="spot_id">
        <xsl:element name="spot_id">
            <xsl:attribute name="id" select="."></xsl:attribute>
            <xsl:value-of select="."/>
        </xsl:element>
<!--        <new_spot_id><xsl:value-of select="."/></new_spot_id>-->
    </xsl:template>
    
</xsl:stylesheet>