<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="3.0">
    <xsl:output indent="yes" method="xml"/>    

    <xsl:template match="/">
        <xsl:result-document href="pure_feed_min.xml">
        <objects>
            <xsl:apply-templates select="objects/object"/>
        </objects>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="object">
        <xsl:element name="object">
            <xsl:attribute name="spot_id" select="AUTHOR_ID/text()"></xsl:attribute>
            <spot_id><xsl:value-of select="AUTHOR_ID/text()"/></spot_id>
            <scw_pid><xsl:value-of select="STUDENT_THESIS_ID/text()"/></scw_pid>
            <programme_id><xsl:value-of select="PROGRAMME_ID/text()"/></programme_id>
            <plan_id><xsl:value-of select="PLAN_ID/text()"/></plan_id>
        </xsl:element>
    </xsl:template>
    
</xsl:stylesheet>