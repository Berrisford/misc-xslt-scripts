<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:local="http://localhost" xmlns:saxon="http://saxon.sf.net/" exclude-result-prefixes="#all" version="2.0">
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:output name="serialise1" encoding="utf-8" method="xhtml" indent="no" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:preserve-space elements="text"/>
	<!-- base uri 16-17 33-34 / rows 22 / 18-19 -->
	
	
	<xsl:template match="/">
		
		<xsl:variable name="baseuri" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/nonscw/select/'"/> 
		<!--<xsl:variable name="baseuri" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/etd/select/'"/>--> 
		
		<!--<xsl:variable name="query" select="encode-for-uri('p.partynumber:2060273')"/>-->	
		<xsl:variable name="query" select="encode-for-uri('p.role:pgadmin')"/>	
		<!--<xsl:variable name="query" select="encode-for-uri('p.role:*')"/>-->	
		<!-- BA: 14/02/24 - Change/swap rows to run entire set !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
		<!--<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=100000&amp;indent=off')"/>-->
		<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=1000&amp;indent=off')"/>
		
		<!--<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=100&amp;rows=5&amp;indent=off')"/>-->
		<xsl:variable name="doc" select="doc($uri)"/>
		
		<xsl:apply-templates select="$doc/response"/>
	</xsl:template>
	
	<xsl:template match="response">
		<xsl:result-document href="pg_admins.xml">
			<objects>
				<xsl:apply-templates select="result/doc"/>
			</objects>
		</xsl:result-document>
	</xsl:template>
	<xsl:template match="doc">		
		<object>			
			<SPOTID><xsl:value-of select="str[@name='p.partynumber']"/></SPOTID>
			<NAME><xsl:value-of select="str[@name='p.displayname']"/></NAME>
			<EMAIL><xsl:value-of select="str[@name='p.email']"/></EMAIL>
			<!--<ROLE><xsl:apply-templates select="arr[@name='p.role']"/></ROLE>-->
			<!--<xsl:apply-templates select="arr[@name='p.role']"/>-->
			
		</object>
	</xsl:template>
	
<!--
	<xsl:template match="arr[@name='p.role']">
		<xsl:for-each select="str">
			<role>
				<xsl:value-of select="."/>
			</role>		
		</xsl:for-each>
	</xsl:template>
	-->

	<xsl:template match="arr[@name='p.role']">
		<!--<xsl:template match="arr[@name='p.role']" mode="distinct">-->
		<xsl:for-each select="str">		
		<xsl:variable name="count">
			<xsl:number/>
		</xsl:variable>
		
		<xsl:variable name="id_node_name" select="concat('ROLE_', $count)"/>
		<xsl:element name="{$id_node_name}">
			<xsl:value-of select="./text()"></xsl:value-of>
		</xsl:element>
	</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>
