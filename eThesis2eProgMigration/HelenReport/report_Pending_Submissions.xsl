<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:local="http://localhost" xmlns:saxon="http://saxon.sf.net/" exclude-result-prefixes="#all" version="2.0">
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:output name="serialise1" encoding="utf-8" method="xhtml" indent="no" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:preserve-space elements="text"/>
	<!-- base uri 16-17 33-34 / rows 22 / 18-19 -->

	
	<xsl:template match="/">
		
		<xsl:variable name="baseuri" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/etd/select/'"/> 
		
		<xsl:variable name="query" select="encode-for-uri('e.expectedclosedate:2024* AND e.windowstate:OPENED')"/>	
		<!-- BA: 14/02/24 - Change/swap rows to run entire set !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
		<!--<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=100000&amp;indent=off')"/>-->
		<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=10000&amp;indent=off')"/>
		
		<!--<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=100&amp;rows=5&amp;indent=off')"/>-->
		<xsl:variable name="doc" select="doc($uri)"/>
		
		<xsl:apply-templates select="$doc/response"/>
	</xsl:template>
	
	<xsl:template match="response">
		<xsl:result-document href="report_pending_submissions.xml">
		<objects>
			<xsl:apply-templates select="result/doc"/>
		</objects>
		</xsl:result-document>
	</xsl:template>
	<xsl:template match="doc">		
		<object>			
			<xsl:text>&#xa;</xsl:text>	
			<PGR_SPOTID><xsl:value-of select="str[@name='e.partynumber']"/></PGR_SPOTID>								
			<PGR_NAME><xsl:value-of select="str[@name='e.displayname']"/></PGR_NAME>									
			<SUBMISSION_STATE><xsl:value-of select="str[@name='e.submissionstate']"/></SUBMISSION_STATE>
			<THESIS_TYPE><xsl:value-of select="str[@name='r.isofetdtype.displayname']"/></THESIS_TYPE>
			<WINDOW_CREATED_BY_NAME><xsl:value-of select="str[@name='r.isopenedby.displayname']"/></WINDOW_CREATED_BY_NAME>
			<WINDOW_OPENED_DATE><xsl:value-of select="str[@name='e.date.opened']"/></WINDOW_OPENED_DATE>				
			<WINDOW_EXPECTED_CLOSED_DATE><xsl:value-of select="str[@name='e.expectedclosedate']"/></WINDOW_EXPECTED_CLOSED_DATE>				
		</object>
	</xsl:template>


	<xsl:template match="arr[@name='r.isbelongsto.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.isbelongsto.source']" mode="name">
		<xsl:value-of select="tokenize(.,'\|\|')[1]"/>
	</xsl:template>
	
	<xsl:template match="arr[@name='r.hasmainsupervisor.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.hascosupervisor.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.fnd']">
		<xsl:param name="pid"/>
		<xsl:for-each select="str">
			<sponsor>
				<xsl:value-of select="concat($pid,'_', replace(.,' ',''))"/>
			</sponsor>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.topic']">
		<xsl:for-each select="str">
			<topic>
				<xsl:value-of select="."/>
			</topic>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='r.wasbelongstoorg.source'] | arr[@name='r.isbelongstoorg.source']">
		<xsl:for-each select="str">
			<org>
				<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
			</org>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.org'] | arr[@name='m.name.org.source'] | arr[@name='r.hasmainsupervisor.pid'] | arr[@name='r.hasmainsupervisor.source']" mode="raw">
		<xsl:for-each select="str">
			<mainsupervisor>
				<xsl:value-of select="."/>
			</mainsupervisor>		
		</xsl:for-each>
	</xsl:template>	
	<xsl:template match="arr[@name='r.hascosupervisor.pid'] | arr[@name='m.name.ths'] | arr[@name='m.name.ths.source']" mode="raw">
		<xsl:for-each select="str">
			<cosupervisor>
				<xsl:value-of select="."/>
			</cosupervisor>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='r.hascosupervisor.source']" mode="raw">
		<xsl:for-each select="str">
			<cosupervisor>
				<xsl:value-of select="tokenize(.,'\|\|')[3]"/>								
			</cosupervisor>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.aut'] | arr[@name='m.name.aut.source']">
		<xsl:for-each select="str">
			<!--<author>-->
				<xsl:value-of select="."/>
			<!--</author>-->		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.fnd'] | arr[@name='m.name.fnd.source']" mode="raw">
		<xsl:for-each select="str">
			<funder>
				<xsl:value-of select="."/>
			</funder>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.dgg'] | arr[@name='m.name.dgg.source']">
		<xsl:for-each select="str">
			<!--<institution>-->
				<xsl:value-of select="."/>
			<!--</institution>-->		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.topic']" mode="raw">
		<xsl:for-each select="str">
			<keyword>
				<xsl:value-of select="."/>
			</keyword>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.typeofresource']">
		<xsl:for-each select="str">
			<type>
				<xsl:value-of select="."/>
			</type>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.isfileattached']">
		<xsl:for-each select="str">
			<is-attached>
				<xsl:value-of select="."/>
			</is-attached>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.id']">
		<xsl:for-each select="str">
			<id>
				<xsl:value-of select="."/>
			</id>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.mimetype']">
		<xsl:for-each select="str">
			<mime-type>
				<xsl:value-of select="."/>
			</mime-type>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.size']">
		<xsl:for-each select="double">
			<size>
				<xsl:value-of select="."/>
			</size>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.state']">
		<xsl:for-each select="str">
			<state>
				<xsl:value-of select="."/>
			</state>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.source']">
		<xsl:for-each select="str">
			<source>
				<xsl:value-of select="."/>
			</source>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='r.islibraryprocessedby.pid']">
		<xsl:for-each select="str">
			<pid>
				<xsl:value-of select="."/>
			</pid>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='x.label']">
		<xsl:for-each select="str">
			<txt>
				<xsl:value-of select="."/>
			</txt>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='x.ownerid']">
		<xsl:for-each select="str">
			<id>
				<xsl:value-of select="."/>
			</id>		
		</xsl:for-each>
	</xsl:template>
	


	<!-- BA: 07/01/16 - Define explicit templates for title and abstract so as to do manual escape of extended ASCII chars - start -->
	<xsl:template match="str[@name='m.abstract'] | str[@name='m.title']">
		<xsl:variable name="find">‘’“”„–—−-čłśćęİşńĭŠḥīźṣā™′ğÖ†Å</xsl:variable>
		<xsl:variable name="replace">''"""----clsceIsniShizsa 'gO A</xsl:variable>
		<xsl:value-of select="replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(translate(.,$find,$replace),'α','alpha'),'κ','kappa'),'∞','infinity'),'œ','oe'),'λ','Gamma'),'…','...'),'ﬁ','fi'),'σ','sigma'),'π','pie'),'μm','micro metre'),'Δ','Delta'),'≥','greater than or equal to'),'Œ','OE'),'≤','smaller or equal to'),'δ','delta')"/>		
	</xsl:template>
	<!-- BA: 07/01/16 - Define explicit templates for title and abstract so as to do manual escape of extended ASCII chars - end -->
	<xsl:template match="arr[@name='r.hasmainsupervisor.source']/str">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.hascosupervisor.source']/str">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
</xsl:stylesheet>
