<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:local="http://localhost" xmlns:saxon="http://saxon.sf.net/" exclude-result-prefixes="#all" version="2.0">
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:output name="serialise1" encoding="utf-8" method="xhtml" indent="no" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:preserve-space elements="text"/>
	<!-- base uri 16-17 33-34 / rows 22 / 18-19 -->

	<!-- BA: [07/10/15] - Add qualifications key doc  - start-->
	<xsl:variable name="obj_qualifications">
		<xsl:copy-of select="doc('qualifications.xml')/qualifications"/>
	</xsl:variable>	
	<!-- BA: [07/10/15] - Add qualifications key doc  - end-->
	
	<xsl:template match="/">
		<!-- BA: 14/02/24 - Change/swap baseuri for live deployment !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
		
		<!--<xsl:variable name="baseuri" select="'http://localhost:8085/solr/scw/select/'"/>-->
		  <xsl:variable name="baseuri" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/scw/select/'"/> 
		
		<xsl:variable name="query" select="'r.isofcontenttype.pid:uk-ac-man-con%5C:20+AND+m.genre.version:%22Doctoral+level+ETD+-+final%22'"/>	
		<!--<xsl:variable name="query" select="'r.isofcontenttype.pid:uk-ac-man-con%5C:20+AND+x.state:Active+AND+m.genre.version:%22Doctoral+level+ETD+-+final%22'"/>-->
		<!--<xsl:variable name="query" select="'PID:uk-ac-man-scw%5C:82668'"/>-->		
		<!--<xsl:variable name="query" select="'r.isofcontenttype.pid:uk-ac-man-con%5C:20 AND x.state:Active AND m.genre.version:%22Doctoral+level+ETD+-+final%22'"/>-->
	    <!--<xsl:variable name="query" select="'x.lastmodifieddate:[NOW-3DAY TO NOW] AND r.isofcontenttype.pid:uk-ac-man-con%5C:20 AND x.state:Active AND m.genre.version:%22Doctoral+level+ETD+-+final%22'"/>-->
		
		<!--  m.genre.version:&quot;Doctoral level ETD - final&quot;-->
		<!-- BA: 14/02/24 - Change/swap rows to run entire set !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
		<!--<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=100000&amp;indent=off')"/>-->
		<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=10&amp;indent=off')"/>
		
		<!--<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=100&amp;rows=5&amp;indent=off')"/>-->
		<xsl:variable name="doc" select="doc($uri)"/>
		
		<xsl:apply-templates select="$doc/response"/>
	</xsl:template>
	<xsl:template match="response">
		<objects>
			<xsl:text>&#xa;</xsl:text>			
			<xsl:comment>
				BA: 14/02/24 - NB: Direct mapping from solr result where "." is replaced with "_" here to form element names.
				Prefixes correspond to the different datastreams: 
				X_ = properties (audit)
				R_ = relationships
				M_ = mods
				F_ = full text
				D_ = ETD (eThesis) Declaration
				E_ = ETD (eThesis)
			</xsl:comment>			
			<xsl:text>&#xa;</xsl:text>						
			<xsl:apply-templates select="result/doc"/>
		</objects>
	</xsl:template>
	<xsl:template match="doc">
		<!-- BA: 14/02/24 - Change/swap baseurietd for live deployment !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
		  <xsl:variable name="baseurietd" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/etd/select/'"/> 
		<!--<xsl:variable name="baseurietd" select="'http://localhost:8085/solr/etd/select/'"/>-->
		<object>			
			<xsl:text>&#xa;</xsl:text>
			<xsl:comment>
				BA: 14/02/24 - (NEW) Add all scholarly works (scw) elements for eProg Migration - start
			</xsl:comment>
			<xsl:text>&#xa;</xsl:text>
			<!-- BA: 14/02/24 - (NEW) Add all scholarly works (scw) elements for eProg Migration - start -->
			<scw>
			<xsl:comment>PID (Item Unique ID)</xsl:comment>
			<xsl:text>&#xa;</xsl:text>	
			<PID><xsl:apply-templates select="str[@name='PID']"/></PID>
			<xsl:comment>State: active = publicly accessible; inactive = not publicly accessible</xsl:comment>
			<X_STATE><xsl:apply-templates select="str[@name='x.state']"/></X_STATE>
			<xsl:comment>Created date</xsl:comment>
			<X_CREATEDDATE><xsl:apply-templates select="date[@name='x.createddate']"/></X_CREATEDDATE>
			<X_CREATEDDATE_DAY><xsl:apply-templates select="str[@name='x.createddate.day']"/></X_CREATEDDATE_DAY>
			<X_CREATEDDATE_MONTH><xsl:apply-templates select="str[@name='x.createddate.month']"/></X_CREATEDDATE_MONTH>
			<X_CREATEDDATE_YEAR><xsl:apply-templates select="str[@name='x.createddate.year']"/></X_CREATEDDATE_YEAR>
			<xsl:comment>Last modified date</xsl:comment>
			<X_LASTMODIFIEDDATE><xsl:apply-templates select="date[@name='x.lastmodifieddate']"/></X_LASTMODIFIEDDATE>
			<xsl:comment>is derivation of</xsl:comment>
			<R_ISDERIVATIONOF_PID><xsl:apply-templates select="str[@name='r.isderivationof.pid']"/></R_ISDERIVATIONOF_PID>
			<xsl:comment>Is of content type</xsl:comment>
			<R_ISOFCONTENTTYPE_PID><xsl:apply-templates select="str[@name='r.isofcontenttype.pid']"/></R_ISOFCONTENTTYPE_PID>
			<R_ISOFCONTENTTYPE_SOURCE><xsl:apply-templates select="str[@name='r.isofcontenttype.source']"/></R_ISOFCONTENTTYPE_SOURCE>
			<xsl:comment>Is created by</xsl:comment>
			<R_ISCREATEDBY_PID><xsl:apply-templates select="arr[@name='r.iscreatedby.pid']/str/."/></R_ISCREATEDBY_PID>
			<R_ISCREATEDBY_SOURCE><xsl:apply-templates select="arr[@name='r.iscreatedby.source']/str/."/></R_ISCREATEDBY_SOURCE>
			<xsl:comment>Is Last modified by</xsl:comment>
			<R_ISLASTMODIFIEDBY_PID><xsl:apply-templates select="arr[@name='r.islastmodifiedby.pid']/str/."/></R_ISLASTMODIFIEDBY_PID>
			<R_ISLASTMODIFIEDBY_SOURCE><xsl:apply-templates select="arr[@name='r.islastmodifiedby.source']/str/."/></R_ISLASTMODIFIEDBY_SOURCE>
			<xsl:comment>Is belongs to</xsl:comment>			
			<R_ISBELONGSTO_PID><xsl:apply-templates select="arr[@name='r.isbelongsto.pid']/str/."/></R_ISBELONGSTO_PID>
			<R_ISBELONGSTO_SOURCE><xsl:apply-templates select="arr[@name='r.isbelongsto.source']/str/."/></R_ISBELONGSTO_SOURCE>
			<xsl:comment>Is belongs to ETD window</xsl:comment>
			<R_ISBELONGSTOETDWINDOW_PID><xsl:apply-templates select="arr[@name='r.isbelongstoetdwindow.pid']/str/."/></R_ISBELONGSTOETDWINDOW_PID>
			<xsl:comment>Was belongs to organisation</xsl:comment>			
			<R_WASBELONGSTOORG_PID><xsl:apply-templates select="arr[@name='r.wasbelongstoorg.pid']"/></R_WASBELONGSTOORG_PID>
			<R_WASBELONGSTOORG_SOURCE><xsl:apply-templates select="arr[@name='r.wasbelongstoorg.source']"/></R_WASBELONGSTOORG_SOURCE>
			<xsl:comment>Has main supervisor</xsl:comment>
			<R_HASMAINSUPERVISOR_PID><xsl:apply-templates select="arr[@name='r.hasmainsupervisor.pid']" mode="raw"/></R_HASMAINSUPERVISOR_PID>
			<R_HASMAINSUPERVISOR_SOURCE><xsl:apply-templates select="arr[@name='r.hasmainsupervisor.source']" mode="raw"/></R_HASMAINSUPERVISOR_SOURCE>
			<xsl:comment>Has co supervisor</xsl:comment>
			<R_HASCOSUPERVISOR_PID><xsl:apply-templates select="arr[@name='r.hascosupervisor.pid']" mode="raw"/></R_HASCOSUPERVISOR_PID>
			<R_HASCOSUPERVISOR_SOURCE><xsl:apply-templates select="arr[@name='r.hascosupervisor.source']" mode="raw"/></R_HASCOSUPERVISOR_SOURCE>
			<xsl:comment>Is belongs to organisation</xsl:comment>
			<R_ISBELONGSTOORG_PID><xsl:apply-templates select="arr[@name='r.isbelongstoorg.pid']"/></R_ISBELONGSTOORG_PID>
			<R_ISBELONGSTOORG_SOURCE><xsl:apply-templates select="arr[@name='r.isbelongstoorg.source']"/></R_ISBELONGSTOORG_SOURCE>
			<M_ABSTRACT><xsl:apply-templates select="str[@name='m.abstract']"/></M_ABSTRACT>
			<M_GENRE_CONTENT-TYPE><xsl:apply-templates select="str[@name='m.genre.content-type']"/></M_GENRE_CONTENT-TYPE>
			<M_GENRE_VERSION><xsl:apply-templates select="str[@name='m.genre.version']"/></M_GENRE_VERSION>
			<M_GENRE_FORM><xsl:apply-templates select="str[@name='m.genre.form']"/></M_GENRE_FORM>
			<xsl:comment>Language term</xsl:comment>
			<M_LANGUAGETERM_CODE><xsl:apply-templates select="str[@name='m.languageterm.code']"/></M_LANGUAGETERM_CODE>
			<xsl:comment>Originator (Main supervisor)</xsl:comment>
			<M_NAME_ORG><xsl:apply-templates select="arr[@name='m.name.org']"/></M_NAME_ORG>
			<M_NAME_ORG_SOURCE><xsl:apply-templates select="arr[@name='m.name.org.source']"/></M_NAME_ORG_SOURCE>
			<xsl:comment>Co supervisor</xsl:comment>			
			<M_NAME_THS><xsl:apply-templates select="arr[@name='m.name.ths']" mode="raw"/></M_NAME_THS>
			<M_NAME_THS_SOURCE><xsl:apply-templates select="arr[@name='m.name.ths.source']" mode="raw"/></M_NAME_THS_SOURCE>
			<xsl:comment>Author</xsl:comment>			
			<M_NAME_AUT><xsl:apply-templates select="arr[@name='m.name.aut']"/></M_NAME_AUT>
			<M_NAME_AUT_SOURCE><xsl:apply-templates select="arr[@name='m.name.aut.source']"/></M_NAME_AUT_SOURCE>
			<xsl:comment>Funder</xsl:comment>			
			<M_NAME_FND><xsl:apply-templates select="arr[@name='m.name.fnd']" mode="raw"/></M_NAME_FND>
			<M_NAME_FND_SOURCE><xsl:apply-templates select="arr[@name='m.name.fnd.source']" mode="raw"/></M_NAME_FND_SOURCE>			
			<xsl:comment>Degree granter</xsl:comment>			
			<M_NAME_DGG><xsl:apply-templates select="arr[@name='m.name.dgg']"/></M_NAME_DGG>
			<M_NAME_DGG_SOURCE><xsl:apply-templates select="arr[@name='m.name.dgg.source']"/></M_NAME_DGG_SOURCE>
			<xsl:comment>Degree level</xsl:comment>			
			<M_NOTE_DEGREELEVEL><xsl:apply-templates select="str[@name='m.note.degreelevel']"/></M_NOTE_DEGREELEVEL>
			<xsl:comment>Degree programme</xsl:comment>
			<M_NOTE_DEGREEPROGRAMME><xsl:apply-templates select="str[@name='m.note.degreeprogramme']"/></M_NOTE_DEGREEPROGRAMME>
			<xsl:comment>Digital material not submitted</xsl:comment>
			<M_NOTE_DIGITALMATERIALNOTSUBMITTED><xsl:apply-templates select="str[@name='m.note.digitalmaterialnotsubmitted']"/></M_NOTE_DIGITALMATERIALNOTSUBMITTED>
			<xsl:comment>Non-Digital material not submitted</xsl:comment>
			<M_NOTE_NONDIGITALMATERIALNOTSUBMITTED><xsl:apply-templates select="str[@name='m.note.nondigitalmaterialnotsubmitted']"/></M_NOTE_NONDIGITALMATERIALNOTSUBMITTED>
			<xsl:comment>Date issued</xsl:comment>
			<M_DATEISSUED><xsl:apply-templates select="str[@name='m.dateissued']"/></M_DATEISSUED>
			<M_YEAR><xsl:apply-templates select="str[@name='m.year']"/></M_YEAR>
			<xsl:comment>Place term</xsl:comment>
			<M_PLACETERM><xsl:apply-templates select="str[@name='m.placeterm']"/></M_PLACETERM>
			<M_PUBLISHER><xsl:apply-templates select="str[@name='m.publisher']"/></M_PUBLISHER>
			<M_PAGE_TOTAL><xsl:apply-templates select="str[@name='m.page.total']"/></M_PAGE_TOTAL>
			<xsl:comment>Record creation date</xsl:comment>
			<M_RECORDCREATIONDATE><xsl:apply-templates select="str[@name='m.recordcreationdate']"/></M_RECORDCREATIONDATE>
			<xsl:comment>Record change date</xsl:comment>
			<M_RECORDCHANGEDATE><xsl:apply-templates select="str[@name='m.recordchangedate']"/></M_RECORDCHANGEDATE>
			<xsl:comment>Record content source</xsl:comment>
			<M_RECORDCONTENTSOURCE><xsl:apply-templates select="str[@name='m.recordcontentsource']"/></M_RECORDCONTENTSOURCE>
			<xsl:comment>Record identifier - manchester escholar</xsl:comment>
			<M_RECORDIDENTIFIER_MANCHESTERESCHOLAR><xsl:apply-templates select="str[@name='m.recordidentifier.manchesterescholar']"/></M_RECORDIDENTIFIER_MANCHESTERESCHOLAR>
			<M_TOPIC><xsl:apply-templates select="arr[@name='m.topic']" mode="raw"/></M_TOPIC>
			<M_TITLE><xsl:apply-templates select="str[@name='m.title']"/></M_TITLE>
			<xsl:comment>Type of resource</xsl:comment>
			<M_TYPEOFRESOURCE><xsl:apply-templates select="arr[@name='m.typeofresource']"/></M_TYPEOFRESOURCE>
			<xsl:comment>Is file attached</xsl:comment>
			<F_ISFILEATTACHED><xsl:apply-templates select="arr[@name='f.isfileattached']"/></F_ISFILEATTACHED>
			<F_FULL-TEXT_STATE><xsl:apply-templates select="str[@name='f.full-text.state']"/></F_FULL-TEXT_STATE>
			<F_FULL-TEXT_SOURCE><xsl:apply-templates select="str[@name='f.full-text.source']"/></F_FULL-TEXT_SOURCE>
			<xsl:comment>File attached...</xsl:comment>
			<F_FILEATTACHED_ID><xsl:apply-templates select="arr[@name='f.fileattached.id']"/></F_FILEATTACHED_ID>
			<F_FILEATTACHED_MIMETYPE><xsl:apply-templates select="arr[@name='f.fileattached.mimetype']"/></F_FILEATTACHED_MIMETYPE>
			<F_FILEATTACHED_SIZE><xsl:apply-templates select="arr[@name='f.fileattached.size']"/></F_FILEATTACHED_SIZE>
			<F_FILEATTACHED_STATE><xsl:apply-templates select="arr[@name='f.fileattached.state']"/></F_FILEATTACHED_STATE>
			<F_FILEATTACHED_SOURCE><xsl:apply-templates select="arr[@name='f.fileattached.source']"/></F_FILEATTACHED_SOURCE>
			<xsl:comment>Terms and conditions accepted</xsl:comment>
			<D_TERMSANDCONDITIONSACCEPTED><xsl:apply-templates select="str[@name='d.termsandconditionsaccepted']"/></D_TERMSANDCONDITIONSACCEPTED>
			<xsl:comment>No copyright infringement</xsl:comment>
			<D_NOCOPYRIGHTINFRINGEMENT><xsl:apply-templates select="str[@name='d.nocopyrightinfringement']"/></D_NOCOPYRIGHTINFRINGEMENT>
			<xsl:comment>Copyright comment</xsl:comment>
			<D_COPYRIGHTCOMMENT><xsl:apply-templates select="str[@name='d.copyrightcomment']"/></D_COPYRIGHTCOMMENT>
			<xsl:comment>No third party agreements</xsl:comment>
			<D_NOTHIRDPARTYAGGREEMENTS><xsl:apply-templates select="str[@name='d.nothirdpartyaggreements']"/></D_NOTHIRDPARTYAGGREEMENTS>
			<xsl:comment>True and accurate representation</xsl:comment>
			<D_TRUEANDACCURATEREPRESENTATION><xsl:apply-templates select="str[@name='d.trueandaccuraterepresentation']"/></D_TRUEANDACCURATEREPRESENTATION>
			<xsl:comment>Permission to download</xsl:comment>
			<D_PERMISSIONTODOWNLOAD><xsl:apply-templates select="str[@name='d.permissiontodownload']"/></D_PERMISSIONTODOWNLOAD>
			<xsl:comment>Access restriction - is restricted</xsl:comment>
			<D_ACCESSRESTRICTION_ISRESTRICTED><xsl:apply-templates select="str[@name='d.accessrestriction.isrestricted']"/></D_ACCESSRESTRICTION_ISRESTRICTED>
			<xsl:comment>Access restriction - duration</xsl:comment>
			<D_ACCESSRESTRICTION_DURATION><xsl:apply-templates select="str[@name='d.accessrestriction.duration']"/></D_ACCESSRESTRICTION_DURATION>
			<xsl:comment>Access restriction - reason</xsl:comment>
			<D_ACCESSRESTRICTION_REASON><xsl:apply-templates select="str[@name='d.accessrestriction.reason']"/></D_ACCESSRESTRICTION_REASON>
			<xsl:comment>Access restriction - reason - other</xsl:comment>
			<D_ACCESSRESTRICTION_REASONOTHER><xsl:apply-templates select="str[@name='d.accessrestriction.reasonother']"/></D_ACCESSRESTRICTION_REASONOTHER>
			<xsl:comment>Blocked career</xsl:comment>
			<D_BLOCKEDCAREER><xsl:apply-templates select="str[@name='d.blockedcareer']"/></D_BLOCKEDCAREER>
			<xsl:comment>Initial restriction duration</xsl:comment>
			<D_INITIALRESTRICTIONDURATION><xsl:apply-templates select="str[@name='d.initialrestrictionduration']"/></D_INITIALRESTRICTIONDURATION>
			<xsl:comment>Embargo approval status</xsl:comment>
			<D_EMBARGOAPPROVALSTATUS><xsl:apply-templates select="str[@name='d.embargoapprovalstatus']"/></D_EMBARGOAPPROVALSTATUS>
			<xsl:comment>Supervisor overrides</xsl:comment>
			<D_SUPERVISOROVERRIDES><xsl:apply-templates select="str[@name='d.supervisoroverrides']"/></D_SUPERVISOROVERRIDES>
			<xsl:comment>Embargo approved date</xsl:comment>
			<D_EMBARGOAPPROVEDDATE><xsl:apply-templates select="str[@name='d.embargoapproveddate']"/></D_EMBARGOAPPROVEDDATE>
			<xsl:comment>Embargo approved by</xsl:comment>
			<D_EMBARGOAPPROVEDBY><xsl:apply-templates select="str[@name='d.embargoapprovedby']"/></D_EMBARGOAPPROVEDBY>
			<TIMESTAMP><xsl:apply-templates select="date[@name='timestamp']"/></TIMESTAMP>
			</scw>

			<xsl:comment>BA: 14/02/24 - (NEW) Add all scholarly works (scw) elements for eProg Migration - end</xsl:comment> 						
			<!-- BA: 14/02/24 - (NEW) Add all scholarly works (scw) elements for eProg Migration - end -->
			<xsl:text>&#xa;</xsl:text>			
			<xsl:text>&#xa;</xsl:text>			
			
			<xsl:comment>BA: 15/02/24 - Legacy fields - start</xsl:comment>
			<xsl:text>&#xa;</xsl:text>						
			<STUDENT_THESIS_ID><xsl:apply-templates select="str[@name='PID']"/></STUDENT_THESIS_ID>
			
			<!-- BA: [07/10/15] - Add qualifications key from key doc  - start -->
			<!--<QUALIFICATION_LEVEL><xsl:apply-templates select="str[@name='m.note.degreelevel']"/></QUALIFICATION_LEVEL>-->			
			<xsl:variable name="qual" select="normalize-space(str[@name='m.note.degreelevel'])"/>			                  
			<xsl:choose>
				<xsl:when test="exists($obj_qualifications/qualifications/qualification[@str = $qual]/key)">
					<QUALIFICATION_LEVEL><xsl:value-of select="$obj_qualifications/qualifications/qualification[@str = $qual]/key"/></QUALIFICATION_LEVEL>					
				</xsl:when>
				<xsl:otherwise>
					<QUALIFICATION_LEVEL><xsl:value-of select="'unkn'"/></QUALIFICATION_LEVEL>					
				</xsl:otherwise>
			</xsl:choose>
			<!-- BA: [07/10/15] - Add qualifications key from key doc  - end-->
			
			<ORIGINAL_LANGUAGE><xsl:apply-templates select="str[@name='m.languageterm.code']"/></ORIGINAL_LANGUAGE>
			<TITLE_ORIGINAL_LANGUAGE><xsl:apply-templates select="str[@name='m.title']"/></TITLE_ORIGINAL_LANGUAGE>
			<ABSTRACT><xsl:apply-templates select="str[@name='m.abstract']"/></ABSTRACT>
			<VISIBILITY><xsl:text>public</xsl:text></VISIBILITY>
			<MANAGED_IN_PURE><xsl:text>FALSE</xsl:text></MANAGED_IN_PURE>
			<AUTHOR_ID><xsl:apply-templates select="arr[@name='r.isbelongsto.source']"/></AUTHOR_ID> 
			<PERSON_ID><xsl:apply-templates select="arr[@name='r.isbelongsto.source']"/></PERSON_ID> 
			<ROLE><xsl:text>author</xsl:text></ROLE>
			<SPONSOR_ID>
				<xsl:apply-templates select="arr[@name='m.name.fnd']">
					<xsl:with-param name="pid"><xsl:value-of select="substring-after(str[@name='PID'],':')"/></xsl:with-param>	
				</xsl:apply-templates>
			</SPONSOR_ID>
			<FREE_KEYWORD><xsl:apply-templates select="arr[@name='m.topic']"/></FREE_KEYWORD>
			<DOCUMENT_ID><xsl:value-of select="concat(substring-after(str[@name='PID'],':'),'_','FULLTEXT')"/></DOCUMENT_ID>
			<TYPE><xsl:text>thesis</xsl:text></TYPE>
			<VALUE><xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?dsid=FULL-TEXT.PDF&amp;pid=' , str[@name='PID'])"/></VALUE>
			<FILE_NAME><xsl:text>FULL_TEXT.PDF</xsl:text></FILE_NAME>
			<MIME_TYPE><xsl:text>application/pdf</xsl:text></MIME_TYPE>
			<STUDENT_THESIS_DOCUMENT_VISIBILITY><xsl:text>public</xsl:text></STUDENT_THESIS_DOCUMENT_VISIBILITY>
			
			<xsl:variable name="pid"><xsl:value-of select="arr[@name='r.isbelongstoetdwindow.pid']/str"/></xsl:variable>
			<xsl:variable name="queryetd" select="concat('PID:',replace($pid,':','?'))"/>
			<xsl:variable name="urietd" select="concat($baseurietd,'?q=',$queryetd,'&amp;version=2.2&amp;start=0&amp;rows=1&amp;indent=off')"/>
			<xsl:variable name="docetd">
				<xsl:copy-of select="doc($urietd)/response/result/doc"/>
			</xsl:variable>
			
			<PROGRAMME_ID><xsl:apply-templates select="$docetd/doc/str[@name='e.programmeid']"/></PROGRAMME_ID>
			<PLAN_ID><xsl:apply-templates select="$docetd/doc/str[@name='e.planid']"/></PLAN_ID>
			<xsl:for-each select="arr[@name='r.hasmainsupervisor.source']/str">
				<STUDENT_THESIS_SUPERVISOR><xsl:apply-templates select="."/></STUDENT_THESIS_SUPERVISOR>
			</xsl:for-each>
			<xsl:for-each select="arr[@name='r.hascosupervisor.source']/str">
				<STUDENT_THESIS_SUPERVISOR><xsl:apply-templates select="."/></STUDENT_THESIS_SUPERVISOR>
			</xsl:for-each>
			
			<xsl:comment>BA: 15/02/24 - Legacy fields - end</xsl:comment>			

			<xsl:text>&#xa;</xsl:text>				
			<xsl:text>&#xa;</xsl:text>							
			<xsl:comment>BA: 19/02/24 - (NEW) Add all ETD elements for eProg Migration - start</xsl:comment> 						
			<!-- BA: 19/02/24 - (NEW) Add all ETD elements for eProg Migration - start -->
			<xsl:text>&#xa;</xsl:text>				
			<etd>
				<xsl:comment>PID (Item Unique ID)</xsl:comment>
				<xsl:text>&#xa;</xsl:text>	
				<PID><xsl:apply-templates select="$docetd/doc/str[@name='PID']"/></PID>
				<xsl:comment>State: active = publicly accessible; inactive = not publicly accessible</xsl:comment>
				<X_STATE><xsl:apply-templates select="$docetd/doc/str[@name='x.state']"/></X_STATE>
				<xsl:comment>label</xsl:comment>
				<X_LABEL><xsl:apply-templates select="$docetd/doc/arr[@name='x.label']"/></X_LABEL>
				<xsl:comment>owner IDs</xsl:comment>
				<X_OWNERID><xsl:apply-templates select="$docetd/doc/arr[@name='x.ownerid']"/></X_OWNERID>				
				<xsl:comment>Created date</xsl:comment>
				<X_CREATEDDATE><xsl:apply-templates select="$docetd/doc/date[@name='x.createddate']"/></X_CREATEDDATE>			
				<xsl:comment>Last modified date</xsl:comment>
				<X_LASTMODIFIEDDATE><xsl:apply-templates select="$docetd/doc/date[@name='x.lastmodifieddate']"/></X_LASTMODIFIEDDATE>
				<xsl:comment>is derivation of</xsl:comment>
				<R_ISDERIVATIONOF_PID><xsl:apply-templates select="$docetd/doc/str[@name='r.isderivationof.pid']"/></R_ISDERIVATIONOF_PID>
				<xsl:comment>is belongs to student - pid</xsl:comment>
				<R_ISBELONGSTOSTUDENT_PID><xsl:apply-templates select="$docetd/doc/str[@name='r.isbelongstostudent.pid']"/></R_ISBELONGSTOSTUDENT_PID>
				<xsl:comment>is belongs to student - display name</xsl:comment>
				<R_ISBELONGSTOSTUDENT_DISPLAYNAME><xsl:apply-templates select="$docetd/doc/str[@name='r.isbelongstostudent.displayname']"/></R_ISBELONGSTOSTUDENT_DISPLAYNAME>
				<xsl:comment>is of ETD type - pid</xsl:comment>			
				<R_ISOFETDTYPE_PID><xsl:apply-templates select="$docetd/doc/str[@name='r.isofetdtype.pid']"/></R_ISOFETDTYPE_PID>
				<xsl:comment>is of ETD type - display name</xsl:comment>			
				<R_ISOFETDTYPE_DISPLAYNAME><xsl:apply-templates select="$docetd/doc/str[@name='r.isofetdtype.displayname']"/></R_ISOFETDTYPE_DISPLAYNAME>
				<xsl:comment>is belongs created by - pid</xsl:comment>
				<R_ISCREATEDBY_PID><xsl:apply-templates select="$docetd/doc/str[@name='r.iscreatedby.pid']"/></R_ISCREATEDBY_PID>
				<xsl:comment>is created by - display name</xsl:comment>
				<R_ISCREATEDBY_DISPLAYNAME><xsl:apply-templates select="$docetd/doc/str[@name='r.iscreatedby.displayname']"/></R_ISCREATEDBY_DISPLAYNAME>
				<xsl:comment>is opened by - pid</xsl:comment>
				<R_ISOPENEDBY_PID><xsl:apply-templates select="$docetd/doc/str[@name='r.isopenedby.pid']"/></R_ISOPENEDBY_PID>
				<xsl:comment>is opened by - display name</xsl:comment>
				<R_ISOPENED_DISPLAYNAME><xsl:apply-templates select="$docetd/doc/str[@name='r.isopenedby.displayname']"/></R_ISOPENED_DISPLAYNAME>
				<xsl:comment>is acknowledged by - pid</xsl:comment>
				<R_ISACKNOWLEDGEDBY_PID><xsl:apply-templates select="$docetd/doc/str[@name='r.isacknowledgedby.pid']"/></R_ISACKNOWLEDGEDBY_PID>
				<xsl:comment>is acknowledged by - display name</xsl:comment>
				<R_ISACKNOWLEDGEDBY_DISPLAYNAME><xsl:apply-templates select="$docetd/doc/str[@name='r.isacknowledgedby.displayname']"/></R_ISACKNOWLEDGEDBY_DISPLAYNAME>
				<xsl:comment>is rejected by - pid</xsl:comment>
				<R_ISREJECTEDBY_PID><xsl:apply-templates select="$docetd/doc/str[@name='r.isrejectedby.pid']"/></R_ISREJECTEDBY_PID>
				<xsl:comment>is rejected by - display name</xsl:comment>
				<R_ISREJECTEDBY_DISPLAYNAME><xsl:apply-templates select="$docetd/doc/str[@name='r.isrejectedby.displayname']"/></R_ISREJECTEDBY_DISPLAYNAME>
				<xsl:comment>is sent to library by - pid</xsl:comment>
				<R_ISSENTTOLIBRARY_PID><xsl:apply-templates select="$docetd/doc/str[@name='r.issenttolibraryby.pid']"/></R_ISSENTTOLIBRARY_PID>
				<xsl:comment>is sent to library by - display name</xsl:comment>
				<R_ISSENTTOLIBRARYBY_DISPLAYNAME><xsl:apply-templates select="$docetd/doc/str[@name='r.issenttolibraryby.displayname']"/></R_ISSENTTOLIBRARYBY_DISPLAYNAME>
				<xsl:comment>is library processed by - pid (no display name)</xsl:comment>
				<R_ISLIBRARYPROCESSEDBY_PID><xsl:apply-templates select="$docetd/doc/arr[@name='r.islibraryprocessedby.pid']"/></R_ISLIBRARYPROCESSEDBY_PID>
				<xsl:comment>is expired by - pid (no display name)</xsl:comment>
				<R_ISEXPIRED_PID><xsl:apply-templates select="$docetd/doc/arr[@name='r.isexpiredby.pid']"/></R_ISEXPIRED_PID>
				<xsl:comment>party number = SPOT ID = Library card number </xsl:comment>
				<xsl:comment>is cancelled by - pid</xsl:comment>
				<R_ISCANCELLEDBY_PID><xsl:apply-templates select="$docetd/doc/str[@name='r.iscancelledby.pid']"/></R_ISCANCELLEDBY_PID>
				<xsl:comment>is cancelled by - display name</xsl:comment>
				<R_ISCANCELLEDBY_DISPLAYNAME><xsl:apply-templates select="$docetd/doc/str[@name='r.iscancelledby.displayname']"/></R_ISCANCELLEDBY_DISPLAYNAME>
				<xsl:comment>is acknowledged by - pid</xsl:comment>				
				<E_PARTYNUMBER><xsl:apply-templates select="$docetd/doc/str[@name='e.partynumber']"/></E_PARTYNUMBER>
				<xsl:comment>display name </xsl:comment>
				<E_DISPLAYNAME><xsl:apply-templates select="$docetd/doc/str[@name='e.displayname']"/></E_DISPLAYNAME>
				<xsl:comment>full name </xsl:comment>
				<E_FULLNAME><xsl:apply-templates select="$docetd/doc/str[@name='e.fullname']"/></E_FULLNAME>
				<xsl:comment>qualification </xsl:comment>
				<E_QUALIFICATION><xsl:apply-templates select="$docetd/doc/str[@name='e.qualification']"/></E_QUALIFICATION>
				<xsl:comment>family name </xsl:comment>
				<E_FAMILYNAME><xsl:apply-templates select="$docetd/doc/str[@name='e.familyname']"/></E_FAMILYNAME>
				<xsl:comment>fore name </xsl:comment>
				<E_FORENAME><xsl:apply-templates select="$docetd/doc/str[@name='e.forename']"/></E_FORENAME>
				<xsl:comment>middle names </xsl:comment>
				<E_MIDDLENAMES><xsl:apply-templates select="$docetd/doc/str[@name='e.middlenames']"/></E_MIDDLENAMES>
				<xsl:comment>programme name </xsl:comment>
				<E_PROGRAMMENAME><xsl:apply-templates select="$docetd/doc/str[@name='e.programmename']"/></E_PROGRAMMENAME>
				<xsl:comment>programme id </xsl:comment>
				<E_PROGRAMMEID><xsl:apply-templates select="$docetd/doc/str[@name='e.programmeid']"/></E_PROGRAMMEID>
				<xsl:comment>plan name </xsl:comment>
				<E_PLANNAME><xsl:apply-templates select="$docetd/doc/str[@name='e.planname']"/></E_PLANNAME>
				<xsl:comment>plan id </xsl:comment>
				<E_PLANID><xsl:apply-templates select="$docetd/doc/str[@name='e.planid']"/></E_PLANID>
				<xsl:comment>is restricted </xsl:comment>
				<E_ISRESTRICTED><xsl:apply-templates select="$docetd/doc/str[@name='e.isrestricted']"/></E_ISRESTRICTED>
				<xsl:comment>window state </xsl:comment>
				<E_WINDOWSTATE><xsl:apply-templates select="$docetd/doc/str[@name='e.windowstate']"/></E_WINDOWSTATE>
				<xsl:comment>submission state </xsl:comment>
				<E_SUBMISSIONSTATE><xsl:apply-templates select="$docetd/doc/str[@name='e.submissionstate']"/></E_SUBMISSIONSTATE>
				<xsl:comment>expected open date </xsl:comment>
				<E_EXPECTEDOPENDATE><xsl:apply-templates select="$docetd/doc/str[@name='e.expectedopendate']"/></E_EXPECTEDOPENDATE>
				<xsl:comment>expected close date </xsl:comment>
				<E_EXPECTEDCLOSEDATE><xsl:apply-templates select="$docetd/doc/str[@name='e.expectedclosedate']"/></E_EXPECTEDCLOSEDATE>
				<xsl:comment>email options - email admin when opened </xsl:comment>
				<E_EMAILADMIN_OPENED><xsl:apply-templates select="$docetd/doc/str[@name='e.emailadmin.opened']"/></E_EMAILADMIN_OPENED>
				<xsl:comment>email options - email admin when expired </xsl:comment>
				<E_EMAILADMIN_EXPIRED><xsl:apply-templates select="$docetd/doc/str[@name='e.emailadmin.expired']"/></E_EMAILADMIN_EXPIRED>
				<xsl:comment>email options - email admin when submitted </xsl:comment>
				<E_EMAILADMIN_SUBMITTED><xsl:apply-templates select="$docetd/doc/str[@name='e.emailadmin.submitted']"/></E_EMAILADMIN_SUBMITTED>
				<xsl:comment>email options - email student when opened </xsl:comment>
				<E_EMAILSTUDENT_OPENED><xsl:apply-templates select="$docetd/doc/str[@name='e.emailstudent.opened']"/></E_EMAILSTUDENT_OPENED>
				<xsl:comment>email options - email student when expired </xsl:comment>
				<E_EMAILSTUDENT_EXPIRED><xsl:apply-templates select="$docetd/doc/str[@name='e.emailstudent.expired']"/></E_EMAILSTUDENT_EXPIRED>
				<xsl:comment>dates... </xsl:comment>
				<E_DATE_CREATED><xsl:apply-templates select="$docetd/doc/str[@name='e.date.created']"/></E_DATE_CREATED>
				<E_DATE_OPENED><xsl:apply-templates select="$docetd/doc/str[@name='e.date.opened']"/></E_DATE_OPENED>
				<E_DATE_SUBMITTED><xsl:apply-templates select="$docetd/doc/str[@name='e.date.submitted']"/></E_DATE_SUBMITTED>
				<E_DATE_ACKNOWLEDGED><xsl:apply-templates select="$docetd/doc/str[@name='e.date.acknowledged']"/></E_DATE_ACKNOWLEDGED>
				<E_DATE_SENT-TO-LIBRARY><xsl:apply-templates select="$docetd/doc/str[@name='e.date.sent_to_library']"/></E_DATE_SENT-TO-LIBRARY>
				<E_DATE_LIBRARY-PROCESSED><xsl:apply-templates select="$docetd/doc/str[@name='e.date.library_processed']"/></E_DATE_LIBRARY-PROCESSED>				
				<E_DATE_REJECTED><xsl:apply-templates select="$docetd/doc/str[@name='e.date.rejected']"/></E_DATE_REJECTED>
				<E_DATE_EXPIRED><xsl:apply-templates select="$docetd/doc/str[@name='e.date.expired']"/></E_DATE_EXPIRED>
				<E_DATE_CANCELLED><xsl:apply-templates select="$docetd/doc/str[@name='e.date.cancelled']"/></E_DATE_CANCELLED>
				<xsl:comment>is restricted by student </xsl:comment>
				<E_ISRESTRICTEDBYSTUDENT><xsl:apply-templates select="$docetd/doc/str[@name='e.isrestrictedbystudent']"/></E_ISRESTRICTEDBYSTUDENT>
				<xsl:comment>library processed state </xsl:comment>
				<E_LIBRARYPROCESSEDSTATE><xsl:apply-templates select="$docetd/doc/str[@name='e.libraryprocessedstate']"/></E_LIBRARYPROCESSEDSTATE>				
				<TIMESTAMP><xsl:apply-templates select="date[@name='timestamp']"/></TIMESTAMP>				
				
			</etd>
			<xsl:comment>BA: 19/02/24 - (NEW) Add all ETD elements for eProg Migration - end</xsl:comment> 						
			<xsl:text>&#xa;</xsl:text>				
			<!-- BA: 19/02/24 - (NEW) Add all ETD elements for eProg Migration - end -->
			
		</object>
		
	</xsl:template>
	<xsl:template match="arr[@name='r.isbelongsto.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.hasmainsupervisor.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.hascosupervisor.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.fnd']">
		<xsl:param name="pid"/>
		<xsl:for-each select="str">
			<sponsor>
				<xsl:value-of select="concat($pid,'_', replace(.,' ',''))"/>
			</sponsor>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.topic']">
		<xsl:for-each select="str">
			<topic>
				<xsl:value-of select="."/>
			</topic>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='r.wasbelongstoorg.pid'] | arr[@name='r.wasbelongstoorg.source'] | arr[@name='r.isbelongstoorg.pid'] | arr[@name='r.isbelongstoorg.source']">
		<xsl:for-each select="str">
			<org>
				<xsl:value-of select="."/>
			</org>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.org'] | arr[@name='m.name.org.source'] | arr[@name='r.hasmainsupervisor.pid'] | arr[@name='r.hasmainsupervisor.source']" mode="raw">
		<xsl:for-each select="str">
			<mainsupervisor>
				<xsl:value-of select="."/>
			</mainsupervisor>		
		</xsl:for-each>
	</xsl:template>	
	<xsl:template match="arr[@name='r.hascosupervisor.pid'] | arr[@name='r.hascosupervisor.source'] | arr[@name='m.name.ths'] | arr[@name='m.name.ths.source']" mode="raw">
		<xsl:for-each select="str">
			<cosupervisor>
				<xsl:value-of select="."/>
			</cosupervisor>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.aut'] | arr[@name='m.name.aut.source']">
		<xsl:for-each select="str">
			<author>
				<xsl:value-of select="."/>
			</author>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.fnd'] | arr[@name='m.name.fnd.source']" mode="raw">
		<xsl:for-each select="str">
			<funder>
				<xsl:value-of select="."/>
			</funder>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.dgg'] | arr[@name='m.name.dgg.source']">
		<xsl:for-each select="str">
			<institution>
				<xsl:value-of select="."/>
			</institution>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.topic']" mode="raw">
		<xsl:for-each select="str">
			<keyword>
				<xsl:value-of select="."/>
			</keyword>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.typeofresource']">
		<xsl:for-each select="str">
			<type>
				<xsl:value-of select="."/>
			</type>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.isfileattached']">
		<xsl:for-each select="str">
			<is-attached>
				<xsl:value-of select="."/>
			</is-attached>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.id']">
		<xsl:for-each select="str">
			<id>
				<xsl:value-of select="."/>
			</id>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.mimetype']">
		<xsl:for-each select="str">
			<mime-type>
				<xsl:value-of select="."/>
			</mime-type>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.size']">
		<xsl:for-each select="double">
			<size>
				<xsl:value-of select="."/>
			</size>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.state']">
		<xsl:for-each select="str">
			<state>
				<xsl:value-of select="."/>
			</state>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.source']">
		<xsl:for-each select="str">
			<source>
				<xsl:value-of select="."/>
			</source>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='r.islibraryprocessedby.pid']">
		<xsl:for-each select="str">
			<pid>
				<xsl:value-of select="."/>
			</pid>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='x.label']">
		<xsl:for-each select="str">
			<txt>
				<xsl:value-of select="."/>
			</txt>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='x.ownerid']">
		<xsl:for-each select="str">
			<id>
				<xsl:value-of select="."/>
			</id>		
		</xsl:for-each>
	</xsl:template>
	


	<!-- BA: 07/01/16 - Define explicit templates for title and abstract so as to do manual escape of extended ASCII chars - start -->
	<xsl:template match="str[@name='m.abstract'] | str[@name='m.title']">
		<xsl:variable name="find">‘’“”„–—−-čłśćęİşńĭŠḥīźṣā™′ğÖ†Å</xsl:variable>
		<xsl:variable name="replace">''"""----clsceIsniShizsa 'gO A</xsl:variable>
		<xsl:value-of select="replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(translate(.,$find,$replace),'α','alpha'),'κ','kappa'),'∞','infinity'),'œ','oe'),'λ','Gamma'),'…','...'),'ﬁ','fi'),'σ','sigma'),'π','pie'),'μm','micro metre'),'Δ','Delta'),'≥','greater than or equal to'),'Œ','OE'),'≤','smaller or equal to'),'δ','delta')"/>		
	</xsl:template>
	<!-- BA: 07/01/16 - Define explicit templates for title and abstract so as to do manual escape of extended ASCII chars - end -->
	<xsl:template match="arr[@name='r.hasmainsupervisor.source']/str">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.hascosupervisor.source']/str">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
</xsl:stylesheet>
