<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:local="http://localhost" xmlns:saxon="http://saxon.sf.net/" exclude-result-prefixes="#all" version="2.0">
	<xsl:output method="text" indent="no" encoding="UTF-8"/>
	<xsl:output name="serialise1" encoding="utf-8" method="xhtml" indent="no" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:preserve-space elements="text"/>
	<!-- base uri 16-17 33-34 / rows 22 / 18-19 -->

	
	<xsl:template match="/">
		<xsl:apply-templates select="./objects"/>
	</xsl:template>
	
	<xsl:template match="objects">
		scwPID|Stud_ID|Main_Sup_ID|Window_ID|Window_Type|Program_ID|Plan_ID|Submission_State|scwCreated_Date|Window_Created_By|Window_Acknowledged_By
		<xsl:apply-templates select="object"/>
	</xsl:template>
	
	<xsl:template match="object">
		<xsl:value-of select="concat(PID, '|')"/>
		<xsl:value-of select="concat(STUDENT_SPOTID, '|')"/>
		<xsl:value-of select="concat(MAIN_SUPERVISOR_SPOTID, '|')"/>
		<xsl:value-of select="concat(WINDOW_ID, '|')"/>
		<xsl:value-of select="concat(WINDOW_TYPE, '|')"/>
		<xsl:value-of select="concat(WINDOW_PROGRAMMEID, '|')"/>
		<xsl:value-of select="concat(WINDOW_PLANID, '|')"/>
		<xsl:value-of select="concat(WINDOW_SUBMISSIONSTATE, '|')"/>
		<xsl:value-of select="concat(substring-before(CREATEDDATE, 'T'), '|')"/>
		<xsl:value-of select="concat(WINDOW_CREATEDBY, '|')"/>
		<xsl:value-of select="concat(WINDOW_ACKNOWLEDGEDBY, '&#10;')"/>
	</xsl:template>	
</xsl:stylesheet>
