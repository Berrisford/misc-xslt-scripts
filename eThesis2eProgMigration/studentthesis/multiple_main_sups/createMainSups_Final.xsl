<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:local="http://localhost" xmlns:saxon="http://saxon.sf.net/" exclude-result-prefixes="#all" version="2.0">
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:output name="serialise1" encoding="utf-8" method="xhtml" indent="no" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:preserve-space elements="text"/>
	<!-- base uri 16-17 33-34 / rows 22 / 18-19 -->
	
	<!-- BA: [07/10/15] - Add qualifications key doc  - start-->
	<xsl:variable name="obj_qualifications">
		<xsl:copy-of select="doc('qualifications.xml')/qualifications"/>
	</xsl:variable>	
	<!-- BA: [07/10/15] - Add qualifications key doc  - end-->
	
	<xsl:template match="/">
		<!-- BA: 14/02/24 - Change/swap baseuri for live deployment !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
		
		<!--<xsl:variable name="baseuri" select="'http://localhost:8085/solr/scw/select/'"/>-->
		<xsl:variable name="baseuri" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/scw/select/'"/> 
		
		
		<xsl:variable name="query" select="encode-for-uri('r.isofcontenttype.pid:uk-ac-man-con\:20 AND m.genre.version:*final* AND -r.isbelongsto.pid:uk-ac-man-per\:87860 AND -r.isbelongsto.pid:uk-ac-man-per\:87861 AND -r.isbelongsto.pid:uk-ac-man-per\:87862 AND -r.isbelongsto.pid:uk-ac-man-per\:87863 AND -r.isbelongsto.pid:uk-ac-man-per\:87864 AND -r.isbelongsto.pid:uk-ac-man-per\:247138 AND -r.isbelongsto.pid:uk-ac-man-per\:245721 AND -r.isbelongsto.pid:uk-ac-man-per\:190812 AND -r.isbelongsto.pid:uk-ac-man-per\:abcde115 AND -r.isbelongsto.pid:uk-ac-man-per\:109112')"/>		
		<!--<xsl:variable name="query" select="'PID:uk-ac-man-scw%5C:87324'"/>-->
				
		<!--  m.genre.version:&quot;Doctoral level ETD - final&quot;-->
		<!-- BA: 14/02/24 - Change/swap rows to run entire set !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
		<!--<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=100000&amp;indent=off')"/>-->
		<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=100000&amp;indent=off')"/>
		
		<!--<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=100&amp;rows=5&amp;indent=off')"/>-->
		<xsl:variable name="doc" select="doc($uri)"/>
		
		<xsl:apply-templates select="$doc/response"/>
	</xsl:template>
	<xsl:template match="response">
		<!--<xsl:result-document href="final_full.xml">-->
		<objects>
			<xsl:apply-templates select="result/doc"/>
		</objects>
		<!--</xsl:result-document>-->
	</xsl:template>
	<xsl:template match="doc">
		<!-- BA: 14/02/24 - Change/swap baseurietd for live deployment !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
		<xsl:variable name="baseurietd" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/etd/select/'"/> 
		<!--<xsl:variable name="baseurietd" select="'http://localhost:8085/solr/etd/select/'"/>-->
		
		<!-- BA: 14/02/24 - Change/swap baseuri_nonscw for live deployment !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
		<xsl:variable name="baseuri_nonscw" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/nonscw/select/'"/> 
		<!--<xsl:variable name="baseuri_nonscw" select="'http://localhost:8085/solr/nonscw/select/'"/>--> 
		
		
		<xsl:variable name="pid"><xsl:value-of select="arr[@name='r.isbelongstoetdwindow.pid']/str"/></xsl:variable>
		<xsl:variable name="queryetd" select="concat('PID:',replace($pid,':','?'), encode-for-uri(' AND (r.isofetdtype.pid:uk-ac-man-etdtype\:3 OR r.isofetdtype.pid:uk-ac-man-etdtype\:4) AND !e.windowstate:EXPIRED AND !e.windowstate:CANCELLED AND (e.submissionstate:REJECTED OR e.submissionstate:ACKNOWLEDGED OR e.submissionstate:SUBMITTED OR e.submissionstate:SENT_TO_LIBRARY OR e.submissionstate:LIBRARY_PROCESSED)'))"/>
		<xsl:variable name="urietd" select="concat($baseurietd,'?q=',$queryetd,'&amp;version=2.2&amp;start=0&amp;rows=1&amp;indent=off')"/>
		<xsl:variable name="docetd">
			<xsl:copy-of select="doc($urietd)/response/result/doc"/>
		</xsl:variable>		
		
		<xsl:if test="exists($docetd/doc/str[@name='PID'])">					
			<xsl:if test="count(arr[@name='r.hasmainsupervisor.source']/str) > 1">
			<object>			
				<xsl:text>&#xa;</xsl:text>	
				<PID><xsl:apply-templates select="str[@name='PID']"/></PID>
				<!--<xsl:comment>State: active = publicly accessible; inactive = not publicly accessible</xsl:comment>-->
				<!--<xsl:comment>Created date</xsl:comment>-->
				<SCW_CREATED_DATE><xsl:apply-templates select="date[@name='x.createddate']"/></SCW_CREATED_DATE>
				<!--<xsl:comment>Is belongs to</xsl:comment>-->			
				<STUDENT_SPOTID><xsl:apply-templates select="arr[@name='r.isbelongsto.source']"/></STUDENT_SPOTID>					
				<STUDENT_NAME><xsl:apply-templates select="arr[@name='r.isbelongsto.source']" mode="name"/></STUDENT_NAME>					
				<!--<xsl:comment>Has main supervisor</xsl:comment>-->
				
				<!--<MAIN_SUPERVISOR_SPOTID><xsl:apply-templates select="arr[@name='r.hasmainsupervisor.source']"/></MAIN_SUPERVISOR_SPOTID>-->					
				<xsl:apply-templates select="arr[@name='r.hasmainsupervisor.source']"/>					
				
				<xsl:apply-templates select="arr[@name='r.hasmainsupervisor.source']" mode="name"/>									
				
				<!--<COSUPERVISOR_SPOTIDS><xsl:apply-templates select="arr[@name='r.hascosupervisor.source']" mode="raw"/></COSUPERVISOR_SPOTIDS>-->
				<xsl:apply-templates select="arr[@name='r.hascosupervisor.source']"/>
				
				<WINDOW_ID><xsl:value-of select="$docetd/doc/str[@name='PID']"/></WINDOW_ID>
				<WINDOW_TYPE><xsl:apply-templates select="$docetd/doc/str[@name='r.isofetdtype.displayname']"/></WINDOW_TYPE>
				<xsl:variable name="created_by_pid">
					<xsl:value-of select="$docetd/doc/str[@name='r.iscreatedby.pid']"/>
				</xsl:variable>
				
				<xsl:variable name="created_by_query">
					<xsl:value-of select="concat('PID:',replace($created_by_pid,':','?'))"/>						
				</xsl:variable> 	
				
				<xsl:variable name="created_by_uri" select="concat($baseuri_nonscw,'?q=',$created_by_query,'&amp;version=2.2&amp;fl=p.partynumber&amp;wt=xml&amp;start=0&amp;rows=1&amp;indent=off')"/>
				
				<xsl:variable name="doc_created_by">
					<xsl:copy-of select="doc($created_by_uri)/response/result/doc"/>
				</xsl:variable>											
				<WINDOW_CREATEDBY><xsl:apply-templates select="$doc_created_by/doc/str[@name='p.partynumber']"/></WINDOW_CREATEDBY>
				<xsl:if test="exists($docetd/doc/str[@name='r.isacknowledgedby.pid'])">
					<!--<xsl:comment>is acknowledged by - SPOT ID</xsl:comment>-->
					<!-- BA: 20/02/24 - Fetch opened by SPOT ID - start -->
					<xsl:variable name="acknowledged_by_pid">
						<xsl:value-of select="$docetd/doc/str[@name='r.isacknowledgedby.pid']"/>
					</xsl:variable>
					
						<xsl:variable name="acknowledged_by_query">
							<xsl:value-of select="concat('PID:',replace($acknowledged_by_pid,':','?'))"/>						
					</xsl:variable> 	
					
						<xsl:variable name="acknowledged_by_uri" select="concat($baseuri_nonscw,'?q=',$acknowledged_by_query,'&amp;version=2.2&amp;fl=p.partynumber&amp;wt=xml&amp;start=0&amp;rows=1&amp;indent=off')"/>
					
					<xsl:variable name="doc_opened_by">
						<xsl:copy-of select="doc($acknowledged_by_uri)/response/result/doc"/>
					</xsl:variable>											
					<!-- BA: 20/02/24 - Fetch created by SPOT ID - end -->					
					<WINDOW_ACKNOWLEDGEDBY><xsl:apply-templates select="$doc_opened_by/doc/str[@name='p.partynumber']"/></WINDOW_ACKNOWLEDGEDBY>
				</xsl:if>										
				<WINDOW_PROGRAMMEID><xsl:apply-templates select="$docetd/doc/str[@name='e.programmeid']"/></WINDOW_PROGRAMMEID>
				<WINDOW_PROGRAMME_NAME><xsl:apply-templates select="$docetd/doc/str[@name='e.programmename']"/></WINDOW_PROGRAMME_NAME>
				<WINDOW_PLANID><xsl:apply-templates select="$docetd/doc/str[@name='e.planid']"/></WINDOW_PLANID>
				<WINDOW_PLAN_NAME><xsl:apply-templates select="$docetd/doc/str[@name='e.planname']"/></WINDOW_PLAN_NAME>
				<WINDOW_DATE_CREATED><xsl:apply-templates select="$docetd/doc/str[@name='e.date.created']"/></WINDOW_DATE_CREATED>
				<WINDOW_DATE_ACKNOWLEDGED><xsl:apply-templates select="$docetd/doc/str[@name='e.date.acknowledged']"/></WINDOW_DATE_ACKNOWLEDGED>
			</object>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	
	
	<xsl:template match="arr[@name='r.isbelongsto.pid']">
		<xsl:value-of select="replace(str/.,':','\\:')"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.isbelongsto.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	
	<xsl:template match="arr[@name='r.isbelongsto.source']" mode="name">
		<xsl:value-of select="tokenize(.,'\|\|')[1]"/>
	</xsl:template>
	
	<xsl:template match="arr[@name='r.hasmainsupervisor.source']">
		<!--<MAIN_SUPERVISOR_SPOTID>-->
		<!--<xsl:value-of select="tokenize(.,'\|\|')[3]"/>-->
		<xsl:for-each select="str">			
			<xsl:variable name="count">
				<xsl:number/>
			</xsl:variable>
			
			<xsl:variable name="id_node_name" select="concat('MAIN_SUPERVISOR_', $count)"/>			
			
			<xsl:element name="{$id_node_name}">
				<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
			</xsl:element>
			<!--<xsl:value-of select="tokenize(.,'\|\|')[3]"/>-->
		</xsl:for-each>		
	</xsl:template>
	
	<xsl:template match="arr[@name='r.hasmainsupervisor.source']" mode="name">
		<!--<MAIN_SUPERVISOR_SPOTID>-->
		<!--<xsl:value-of select="tokenize(.,'\|\|')[3]"/>-->
		<xsl:for-each select="str">			
			<xsl:variable name="count">
				<xsl:number/>
			</xsl:variable>
			
			<xsl:variable name="id_node_name" select="concat('MAIN_SUPERVISOR_NAME_', $count)"/>			
			
			<xsl:element name="{$id_node_name}">
				<xsl:value-of select="tokenize(.,'\|\|')[1]"/>
			</xsl:element>
			<!--<xsl:value-of select="tokenize(.,'\|\|')[3]"/>-->
		</xsl:for-each>		
	</xsl:template>
	
	<xsl:template match="arr[@name='r.hascosupervisor.source']">
		<!--<xsl:value-of select="tokenize(.,'\|\|')[3]"/>-->
		<xsl:for-each select="str">			
			<xsl:variable name="count">
				<xsl:number/>
			</xsl:variable>
			
			<xsl:variable name="id_node_name" select="concat('COSUPERVISOR_', $count)"/>			
			
			<xsl:element name="{$id_node_name}">
				<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
			</xsl:element>
			<!--<xsl:value-of select="tokenize(.,'\|\|')[3]"/>-->
		</xsl:for-each>				
	</xsl:template>
	
	<xsl:template match="arr[@name='m.name.fnd']">
		<xsl:param name="pid"/>
		<xsl:for-each select="str">
			<sponsor>
				<xsl:value-of select="concat($pid,'_', replace(.,' ',''))"/>
			</sponsor>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.topic']">
		<xsl:for-each select="str">
			<topic>
				<xsl:value-of select="."/>
			</topic>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='r.wasbelongstoorg.source'] | arr[@name='r.isbelongstoorg.source']">
		<xsl:for-each select="str">
			<org>
				<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
			</org>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.org'] | arr[@name='m.name.org.source'] | arr[@name='r.hasmainsupervisor.pid'] | arr[@name='r.hasmainsupervisor.source']" mode="raw">
		<xsl:for-each select="str">
			<mainsupervisor>
				<xsl:value-of select="."/>
			</mainsupervisor>		
		</xsl:for-each>
	</xsl:template>	
	<xsl:template match="arr[@name='r.hascosupervisor.pid'] | arr[@name='m.name.ths'] | arr[@name='m.name.ths.source']" mode="raw">
		<xsl:for-each select="str">
			<cosupervisor>
				<xsl:value-of select="."/>
			</cosupervisor>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='r.hascosupervisor.source']" mode="raw">
		<xsl:for-each select="str">
			<cosupervisor>
				<xsl:value-of select="tokenize(.,'\|\|')[3]"/>								
			</cosupervisor>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.aut'] | arr[@name='m.name.aut.source']">
		<xsl:for-each select="str">
			<!--<author>-->
				<xsl:value-of select="."/>
			<!--</author>-->		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.fnd'] | arr[@name='m.name.fnd.source']" mode="raw">
		<xsl:for-each select="str">
			<funder>
				<xsl:value-of select="."/>
			</funder>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.dgg'] | arr[@name='m.name.dgg.source']">
		<xsl:for-each select="str">
			<!--<institution>-->
				<xsl:value-of select="."/>
			<!--</institution>-->		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.topic']" mode="raw">
		<xsl:for-each select="str">
			<keyword>
				<xsl:value-of select="."/>
			</keyword>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.typeofresource']">
		<xsl:for-each select="str">
			<type>
				<xsl:value-of select="."/>
			</type>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.isfileattached']">
		<xsl:for-each select="str">
			<is-attached>
				<xsl:value-of select="."/>
			</is-attached>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.id']">
		<xsl:for-each select="str">
			<id>
				<xsl:value-of select="."/>
			</id>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.mimetype']">
		<xsl:for-each select="str">
			<mime-type>
				<xsl:value-of select="."/>
			</mime-type>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.size']">
		<xsl:for-each select="double">
			<size>
				<xsl:value-of select="."/>
			</size>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.state']">
		<xsl:for-each select="str">
			<state>
				<xsl:value-of select="."/>
			</state>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='f.fileattached.source']">
		<xsl:for-each select="str">
			<source>
				<xsl:value-of select="."/>
			</source>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='r.islibraryprocessedby.pid']">
		<xsl:for-each select="str">
			<pid>
				<xsl:value-of select="."/>
			</pid>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='x.label']">
		<xsl:for-each select="str">
			<txt>
				<xsl:value-of select="."/>
			</txt>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='x.ownerid']">
		<xsl:for-each select="str">
			<id>
				<xsl:value-of select="."/>
			</id>		
		</xsl:for-each>
	</xsl:template>
	


	<!-- BA: 07/01/16 - Define explicit templates for title and abstract so as to do manual escape of extended ASCII chars - start -->
	<xsl:template match="str[@name='m.abstract'] | str[@name='m.title']">
		<xsl:variable name="find">‘’“”„–—−-čłśćęİşńĭŠḥīźṣā™′ğÖ†Å</xsl:variable>
		<xsl:variable name="replace">''"""----clsceIsniShizsa 'gO A</xsl:variable>
		<xsl:value-of select="replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(translate(.,$find,$replace),'α','alpha'),'κ','kappa'),'∞','infinity'),'œ','oe'),'λ','Gamma'),'…','...'),'ﬁ','fi'),'σ','sigma'),'π','pie'),'μm','micro metre'),'Δ','Delta'),'≥','greater than or equal to'),'Œ','OE'),'≤','smaller or equal to'),'δ','delta')"/>		
	</xsl:template>
	<!-- BA: 07/01/16 - Define explicit templates for title and abstract so as to do manual escape of extended ASCII chars - end -->
	<xsl:template match="arr[@name='r.hasmainsupervisor.source']/str">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.hascosupervisor.source']/str">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
</xsl:stylesheet>
