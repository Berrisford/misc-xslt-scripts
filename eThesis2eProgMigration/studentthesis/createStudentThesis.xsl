<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:local="http://localhost" xmlns:saxon="http://saxon.sf.net/" exclude-result-prefixes="#all" version="2.0">
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:output name="serialise1" encoding="utf-8" method="xhtml" indent="no" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:preserve-space elements="text"/>
	<!-- base uri 16-17 33-34 / rows 22 / 18-19 -->

	<!-- BA: [07/10/15] - Add qualifications key doc  - start-->
	<xsl:variable name="obj_qualifications">
		<xsl:copy-of select="doc('qualifications.xml')/qualifications"/>
	</xsl:variable>	
	<!-- BA: [07/10/15] - Add qualifications key doc  - end-->
	
	<xsl:template match="/">
		<!-- BA: 14/02/24 - Change/swap baseuri for live deployment !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
		
		<!--<xsl:variable name="baseuri" select="'http://localhost:8085/solr/scw/select/'"/>-->
		  <xsl:variable name="baseuri" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/scw/select/'"/> 
		
		<!-- BA: 14/02/24 - Change query to deal with examinations < 6 years old !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->		
		<!--<xsl:variable name="query" select="'r.isofcontenttype.pid:uk-ac-man-con%5C:20+AND+x.state:Active+AND+m.genre.version:%22Doctoral+level+ETD+-+final%22'"/>-->
		<xsl:variable name="query" select="'PID:uk-ac-man-scw%5C:334875'"/>		
		<!--<xsl:variable name="query" select="'r.isofcontenttype.pid:uk-ac-man-con%5C:20 AND x.state:Active AND m.genre.version:%22Doctoral+level+ETD+-+final%22'"/>-->
	    <!--<xsl:variable name="query" select="'x.lastmodifieddate:[NOW-3DAY TO NOW] AND r.isofcontenttype.pid:uk-ac-man-con%5C:20 AND x.state:Active AND m.genre.version:%22Doctoral+level+ETD+-+final%22'"/>-->
		
		<!--  m.genre.version:&quot;Doctoral level ETD - final&quot;-->
		<!-- BA: 14/02/24 - Change/swap rows to run entire set !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
		<!--<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=100000&amp;indent=off')"/>-->
		<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=1&amp;indent=off')"/>
		
		<!--<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=100&amp;rows=5&amp;indent=off')"/>-->
		<xsl:variable name="doc" select="doc($uri)"/>
		
		<xsl:apply-templates select="$doc/response"/>
	</xsl:template>
	<xsl:template match="response">
		<objects>
			<xsl:apply-templates select="result/doc"/>
		</objects>
	</xsl:template>
	<xsl:template match="doc">
		<!-- BA: 14/02/24 - Change/swap baseurietd for live deployment !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
		  <xsl:variable name="baseurietd" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/etd/select/'"/> 
		<!--<xsl:variable name="baseurietd" select="'http://localhost:8085/solr/etd/select/'"/>-->
		<object>
			
			<!-- BA: 14/02/24 - Add all elements for eProg Migration - start -->
			
			<xsl:comment>BA: 14/02/24 - Add all elements for eProg Migration - start</xsl:comment>
			<xsl:comment>BA: 14/02/24 - NB: Direct mapping from solr result where "." is replaced with "_" here to form element names.</xsl:comment>
			<PID><xsl:apply-templates select="str[@name='PID']"/></PID>
			<X_STATE><xsl:apply-templates select="str[@name='x.state']"/></X_STATE>
			<X_CREATEDDATE><xsl:apply-templates select="date[@name='x.createddate']"/></X_CREATEDDATE>								
			<X_LASTMODIFIEDDATE><xsl:apply-templates select="date[@name='x.lastmodifieddate']"/></X_LASTMODIFIEDDATE>
			<X_CREATEDDATE_DAY><xsl:apply-templates select="str[@name='x.createddate.day']"/></X_CREATEDDATE_DAY>
			<X_CREATEDDATE_MONTH><xsl:apply-templates select="str[@name='x.createddate.month']"/></X_CREATEDDATE_MONTH>
			<X_CREATEDDATE_YEAR><xsl:apply-templates select="str[@name='x.createddate.year']"/></X_CREATEDDATE_YEAR>
			<R_ISDERIVATIONOF_PID><xsl:apply-templates select="str[@name='r.isderivationof.pid']"/></R_ISDERIVATIONOF_PID>
			<R_ISOFCONTENTTYPE_PID><xsl:apply-templates select="str[@name='r.isofcontenttype.pid']"/></R_ISOFCONTENTTYPE_PID>
			<R_ISOFCONTENTTYPE_SOURCE><xsl:apply-templates select="str[@name='r.isofcontenttype.source']"/></R_ISOFCONTENTTYPE_SOURCE>
			<R_ISCREATEDBY_PID><xsl:apply-templates select="arr[@name='r.iscreatedby.pid']/str/."/></R_ISCREATEDBY_PID>
			<R_ISCREATEDBY_SOURCE><xsl:apply-templates select="arr[@name='r.iscreatedby.source']/str/."/></R_ISCREATEDBY_SOURCE>
			<R_ISLASTMODIFIEDBY_PID><xsl:apply-templates select="arr[@name='r.islastmodifiedby.pid']/str/."/></R_ISLASTMODIFIEDBY_PID>
			<R_ISLASTMODIFIEDBY_SOURCE><xsl:apply-templates select="arr[@name='r.islastmodifiedby.source']/str/."/></R_ISLASTMODIFIEDBY_SOURCE>			
			<R_ISBELONGSTO_PID><xsl:apply-templates select="arr[@name='r.isbelongsto.pid']/str/."/></R_ISBELONGSTO_PID>
			<R_ISBELONGSTO_SOURCE><xsl:apply-templates select="arr[@name='r.isbelongsto.source']/str/."/></R_ISBELONGSTO_SOURCE>
			<R_ISBELONGSTOETDWINDOW_PID><xsl:apply-templates select="arr[@name='r.isbelongstoetdwindow.pid']/str/."/></R_ISBELONGSTOETDWINDOW_PID>			
			<R_WASBELONGSTOORG_PID><xsl:apply-templates select="arr[@name='r.wasbelongstoorg.pid']"/></R_WASBELONGSTOORG_PID>
			<R_WASBELONGSTOORG_SOURCE><xsl:apply-templates select="arr[@name='r.wasbelongstoorg.source']"/></R_WASBELONGSTOORG_SOURCE>
			<R_HASMAINSUPERVISOR_PID><xsl:apply-templates select="arr[@name='r.hasmainsupervisor.pid']/str/."/></R_HASMAINSUPERVISOR_PID>
			<R_HASMAINSUPERVISOR_SOURCE><xsl:apply-templates select="arr[@name='r.hasmainsupervisor.source']/str/."/></R_HASMAINSUPERVISOR_SOURCE>
			<R_HASCOSUPERVISOR_PID><xsl:apply-templates select="arr[@name='r.hascosupervisor.pid']" mode="raw"/></R_HASCOSUPERVISOR_PID>
			<R_HASCOSUPERVISOR_SOURCE><xsl:apply-templates select="arr[@name='r.hascosupervisor.source']" mode="raw"/></R_HASCOSUPERVISOR_SOURCE>
			<R_ISBELONGSTOORG_PID><xsl:apply-templates select="arr[@name='r.isbelongstoorg.pid']"/></R_ISBELONGSTOORG_PID>
			<R_ISBELONGSTOORG_SOURCE><xsl:apply-templates select="arr[@name='r.isbelongstoorg.source']"/></R_ISBELONGSTOORG_SOURCE>
			
			
			<xsl:comment>BA: 14/02/24 - Add all elements for eProg Migration - end</xsl:comment> 
			
			
			<!-- BA: 14/02/24 - Add all elements for eProg Migration - end -->
			
			<STUDENT_THESIS_ID><xsl:apply-templates select="str[@name='PID']"/></STUDENT_THESIS_ID>
			
			<!-- BA: [07/10/15] - Add qualifications key from key doc  - start-->
			<!--<QUALIFICATION_LEVEL><xsl:apply-templates select="str[@name='m.note.degreelevel']"/></QUALIFICATION_LEVEL>-->			
			<xsl:variable name="qual" select="normalize-space(str[@name='m.note.degreelevel'])"/>			                  
			<xsl:choose>
				<xsl:when test="exists($obj_qualifications/qualifications/qualification[@str = $qual]/key)">
					<QUALIFICATION_LEVEL><xsl:value-of select="$obj_qualifications/qualifications/qualification[@str = $qual]/key"/></QUALIFICATION_LEVEL>					
				</xsl:when>
				<xsl:otherwise>
					<QUALIFICATION_LEVEL><xsl:value-of select="'unkn'"/></QUALIFICATION_LEVEL>					
				</xsl:otherwise>
			</xsl:choose>
			<!-- BA: [07/10/15] - Add qualifications key from key doc  - end-->
			
			<ORIGINAL_LANGUAGE><xsl:apply-templates select="str[@name='m.languageterm.code']"/></ORIGINAL_LANGUAGE>
			<TITLE_ORIGINAL_LANGUAGE><xsl:apply-templates select="str[@name='m.title']"/></TITLE_ORIGINAL_LANGUAGE>
			<ABSTRACT><xsl:apply-templates select="str[@name='m.abstract']"/></ABSTRACT>
			<VISIBILITY><xsl:text>public</xsl:text></VISIBILITY>
			<MANAGED_IN_PURE><xsl:text>FALSE</xsl:text></MANAGED_IN_PURE>
			<AUTHOR_ID><xsl:apply-templates select="arr[@name='r.isbelongsto.source']"/></AUTHOR_ID> 
			<PERSON_ID><xsl:apply-templates select="arr[@name='r.isbelongsto.source']"/></PERSON_ID> 
			<ROLE><xsl:text>author</xsl:text></ROLE>
			<SPONSOR_ID>
				<xsl:apply-templates select="arr[@name='m.name.fnd']">
					<xsl:with-param name="pid"><xsl:value-of select="substring-after(str[@name='PID'],':')"/></xsl:with-param>	
				</xsl:apply-templates>
			</SPONSOR_ID>
			<FREE_KEYWORD><xsl:apply-templates select="arr[@name='m.topic']"/></FREE_KEYWORD>
			<DOCUMENT_ID><xsl:value-of select="concat(substring-after(str[@name='PID'],':'),'_','FULLTEXT')"/></DOCUMENT_ID>
			<TYPE><xsl:text>thesis</xsl:text></TYPE>
			<VALUE><xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?dsid=FULL-TEXT.PDF&amp;pid=' , str[@name='PID'])"/></VALUE>
			<FILE_NAME><xsl:text>FULL_TEXT.PDF</xsl:text></FILE_NAME>
			<MIME_TYPE><xsl:text>application/pdf</xsl:text></MIME_TYPE>
			<STUDENT_THESIS_DOCUMENT_VISIBILITY><xsl:text>public</xsl:text></STUDENT_THESIS_DOCUMENT_VISIBILITY>
			
			<xsl:variable name="pid"><xsl:value-of select="arr[@name='r.isbelongstoetdwindow.pid']/str"/></xsl:variable>
			<xsl:variable name="queryetd" select="concat('PID:',replace($pid,':','?'))"/>
			<xsl:variable name="urietd" select="concat($baseurietd,'?q=',$queryetd,'&amp;version=2.2&amp;start=0&amp;rows=1&amp;indent=off')"/>
			<xsl:variable name="docetd">
				<xsl:copy-of select="doc($urietd)/response/result/doc"/>
			</xsl:variable>
			
			<PROGRAMME_ID><xsl:apply-templates select="$docetd/doc/str[@name='e.programmeid']"/></PROGRAMME_ID>
			<PLAN_ID><xsl:apply-templates select="$docetd/doc/str[@name='e.planid']"/></PLAN_ID>
			<xsl:for-each select="arr[@name='r.hasmainsupervisor.source']/str">
				<STUDENT_THESIS_SUPERVISOR><xsl:apply-templates select="."/></STUDENT_THESIS_SUPERVISOR>
			</xsl:for-each>
			<xsl:for-each select="arr[@name='r.hascosupervisor.source']/str">
				<STUDENT_THESIS_SUPERVISOR><xsl:apply-templates select="."/></STUDENT_THESIS_SUPERVISOR>
			</xsl:for-each>
			
			
		</object>
	</xsl:template>
	<xsl:template match="arr[@name='r.isbelongsto.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.hasmainsupervisor.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.hascosupervisor.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.fnd']">
		<xsl:param name="pid"/>
		<xsl:for-each select="str">
			<sponsor>
				<xsl:value-of select="concat($pid,'_', replace(.,' ',''))"/>
			</sponsor>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.topic']">
		<xsl:for-each select="str">
			<topic>
				<xsl:value-of select="."/>
			</topic>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='r.wasbelongstoorg.pid'] | arr[@name='r.wasbelongstoorg.source'] | arr[@name='r.isbelongstoorg.pid'] | arr[@name='r.isbelongstoorg.source']">
		<xsl:for-each select="str">
			<org>
				<xsl:value-of select="."/>
			</org>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='r.hascosupervisor.pid'] | arr[@name='r.hascosupervisor.source']" mode="raw">
		<xsl:for-each select="str">
			<cosupervisor>
				<xsl:value-of select="."/>
			</cosupervisor>		
		</xsl:for-each>
	</xsl:template>
	


	<!-- BA: 07/01/16 - Define explicit templates for title and abstract so as to do manual escape of extended ASCII chars - start -->
	<xsl:template match="str[@name='m.abstract'] | str[@name='m.title']">
		<xsl:variable name="find">‘’“”„–—−-čłśćęİşńĭŠḥīźṣā™′ğÖ†Å</xsl:variable>
		<xsl:variable name="replace">''"""----clsceIsniShizsa 'gO A</xsl:variable>
		<xsl:value-of select="replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(translate(.,$find,$replace),'α','alpha'),'κ','kappa'),'∞','infinity'),'œ','oe'),'λ','Gamma'),'…','...'),'ﬁ','fi'),'σ','sigma'),'π','pie'),'μm','micro metre'),'Δ','Delta'),'≥','greater than or equal to'),'Œ','OE'),'≤','smaller or equal to'),'δ','delta')"/>		
	</xsl:template>
	<!-- BA: 07/01/16 - Define explicit templates for title and abstract so as to do manual escape of extended ASCII chars - end -->
	<xsl:template match="arr[@name='r.hasmainsupervisor.source']/str">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.hascosupervisor.source']/str">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
</xsl:stylesheet>
