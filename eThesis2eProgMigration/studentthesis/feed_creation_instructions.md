# Feed Creation Instructions #

1.   /Apps/apache2/htdocs/internal/api/trusted/getdatabasefeed
	1.  In this folder create folder orcid
	2.  In this folder create folder xml
	3.  In this folder create index.xml (blank xml file) and </objects> and .htaccess with required permission
```
		DirectoryIndex index.xml
		#Allow from all
		Order deny,allow
		Deny from all
		Allow from 130.88.3.129
		Allow from 10.2.123.82
		Allow from 10.2.123.83
		Allow from 10.2.123.78
		Allow from 130.88.139.143
```

2.   /home/fedora/scripts/datafeeds/xml
	1.  In this folder create folders "eThesis_Exam" & "eThesis_Final"
	2.  In EACH folder create folder xml
	3.  In EACH folder create a symbolic link
3.  cd /home/fedora/scripts/datafeeds/xml/eThesis_Exam/xml
   cd /home/fedora/scripts/datafeeds/xml/eThesis_Final/xml
	1.  ln -s  /Apps/apache2/htdocs/internal/api/trusted/getdatabasefeed/eThesis_Exam/xml/index.xml ./index.xml
	2.  ln -s  /Apps/apache2/htdocs/internal/api/trusted/getdatabasefeed/eThesis_Final/xml/index.xml ./index.xml
4.  cd /home/fedora/scripts/datafeeds/xsl/
	1.  In this folder create folders eThesis_Exam & eThesis_Final
	2.  Create the xsl file createeThesis_Exam.xsl & createeThesis_Final.xsl
5.  Edit /home/fedora/scripts/create-feeds.sh
	1.  Include eThesis_Exam
	2.  Include eThesis_Final
6.  Test the feed
	1.  go the folder /home/fedora
	2.  sh scripts/create-feeds.sh eThesis_Exam
	3.  sh scripts/create-feeds.sh eThesis_Final
	4.  Check the xmls at /Apps/apache2/htdocs/internal/api/trusted/getdatabasefeed/eThesis_Exam/xml/ &
	   /Apps/apache2/htdocs/internal/api/trusted/getdatabasefeed/eThesis_Final/xml/
7.  Inform the URL to ???
	1.  https://www.escholar.manchester.ac.uk/api/trusted/getdatabasefeed/orcid/xml/index.xml

## Feed URLs:  
-   https://www.escholar.manchester.ac.uk/api/trusted/getdatabasefeed/eThesis_Exam/xml/index.xml  
-   https://www.escholar.manchester.ac.uk/api/trusted/getdatabasefeed/eThesis_Final/xml/index.xml  
