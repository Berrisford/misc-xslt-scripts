<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    exclude-result-prefixes="xs math oai_dc dc"
    version="3.0">        
    <xsl:output encoding="UTF-8" method="xml" indent="yes"/>

    <xsl:template match="/">
        <Metadata schemaURI="http://www.openarchives.org/OAI/2.0/oai_dc.xsd">
            <title>Dublin Core Metadata</title>
            <description>UoM to CMIS transformation</description>
            <xsl:apply-templates select="//record"/>
        </Metadata>            
    </xsl:template>

    <xsl:template match="record">
        <group>            
            <title>
                <xsl:value-of select="//field[@name='title']/value"></xsl:value-of>
            </title>            
            <xsl:apply-templates select="//field[not(@name='title') and not(@name='filename')]"/>
            <item>
                <name>publisher</name>
                <value></value>
                <type>dc:publisher</type>
            </item>
            <item>
                <name>contributor</name>
                <value></value>
                <type>dc:contributor</type>
            </item>
            <item>
                <name>source</name>
                <value>University of Manchester</value>
                <type>dc:source</type>
            </item>
            <item>
                <name>relation</name>
                <value></value>
                <type>dc:relation</type>
            </item>
            <item>
                <name>coverage</name>
                <value></value>
                <type>dc:coverage</type>
            </item>
            <item>
                <name>rights</name>
                <value>Copyright University of Manchester</value>
                <type>dc:rights</type>
            </item>
        </group>
    </xsl:template>

    <xsl:template match="field">
        <item>
            <name><xsl:value-of select="@name"/></name>
            <value><xsl:value-of select="value"/></value>
            <type><xsl:value-of select="concat('dc:', @name)"/></type>
        </item>
    </xsl:template>

</xsl:stylesheet>