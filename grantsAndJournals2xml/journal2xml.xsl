<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">    
    <xsl:output indent="yes" method="xml"/>
    
    <!--<xsl:variable name="root_url" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/scw/select/'"></xsl:variable>-->
    <xsl:variable name="root_url" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/journal/select/'"></xsl:variable>
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con%5C%3A13+AND+r.isbelongsto.pid:uk*', '&amp;fl=PID,r.isbelongsto.pid,x.state,m.genre.version,m.genre.form&amp;omitHeader=true')"></xsl:variable>-->
    <xsl:variable name="url" select="concat($root_url, '?q=PID%3A%5B*+TO+*%5D', '&amp;omitHeader=true')"></xsl:variable>
    
    <xsl:variable name="rows" select="1000"/>
    <xsl:variable name="num_found">
        <xsl:value-of select="doc(concat($url, '&amp;start=0&amp;rows=0'))/response/result/@numFound"/>
        <!--<xsl:value-of select="5"/>-->
    </xsl:variable>
    
    <xsl:template match="/">
        <add>
            <xsl:call-template name="recurse">
                <xsl:with-param name="start" select="0"/>
                <xsl:with-param name="end" select="$rows"/>
            </xsl:call-template>
        </add>
    </xsl:template>
    
    <xsl:template name="recurse">
        <xsl:param name="start"/>
        <xsl:param name="end"/>
        <xsl:variable name="obj_response">
            <xsl:copy-of select="doc(concat($url, '&amp;start=', $start, '&amp;rows=', $rows))"/>
        </xsl:variable>
        <xsl:for-each select="$obj_response/response/result/doc">            
            <doc>
                <xsl:apply-templates select="str"/>
            </doc>                        
        </xsl:for-each>
        
        <xsl:if test="$end &lt; $num_found">
            <xsl:call-template name="recurse">
                <xsl:with-param name="start" select="$end + 1"/>
                <xsl:with-param name="end" select="$end + $rows"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="str">
        <field>
            <xsl:attribute name="name" select="@name"/>
            <xsl:value-of select="."/>
        </field>        
    </xsl:template>
    
    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>
    
</xsl:stylesheet>
