<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">    
    <xsl:output indent="yes" method="xml"/>
        
    <xsl:template match="/">
        <add>
            <xsl:for-each select="response/result/doc">
                <doc>
                    <field>
                        <xsl:attribute name="name" select="'PID'"/>
                        <xsl:value-of select="str[@name='PID']"/>
                    </field>
                    <field>
                        <xsl:attribute name="name" select="'g.id'"/>
                        <xsl:value-of select="str[@name='g.id']"/>
                    </field>
                    <field>
                        <xsl:attribute name="name" select="'g.code'"/>
                        <xsl:value-of select="str[@name='g.code']"/>
                    </field>
                    <field>
                        <xsl:attribute name="name" select="'g.title'"/>
                        <xsl:value-of select="str[@name='g.title']"/>
                    </field>
                    <xsl:apply-templates select="arr[@name='g.funding.name']/str"/>
                    <xsl:apply-templates select="arr[@name='g.funding.source']/str"/>
                    <xsl:apply-templates select="arr[@name='g.funding.source']/str" mode="grant-code"/>
                </doc>                        
            </xsl:for-each>            
        </add>
    </xsl:template>
                
    <xsl:template match="arr[@name='g.funding.name']/str">
        <field>
            <xsl:attribute name="name" select="'g.funding.name'"/>
            <xsl:value-of select="."></xsl:value-of>
        </field>
    </xsl:template>

    <xsl:template match="arr[@name='g.funding.source']/str">
        <field>
            <xsl:attribute name="name" select="'g.funding.source'"/>
            <xsl:value-of select="."></xsl:value-of>
        </field>
    </xsl:template>

    <xsl:template match="arr[@name='g.funding.source']/str" mode="grant-code">
        <field>
            <xsl:attribute name="name" select="'g.funding.grantcode'"/>
            <xsl:value-of select="tokenize(., '\|\|')[3]"/>
        </field>
    </xsl:template>
    
    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>
    
</xsl:stylesheet>
