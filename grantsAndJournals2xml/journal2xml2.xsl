<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">    
    <xsl:output indent="yes" method="xml"/>
        
    <xsl:template match="/">
        <add>
            <xsl:for-each select="response/result/doc">            
                <doc>
                    <xsl:apply-templates select="str"/>
                    <!-- BA: 11/03/16 - Add autocomplete - start -->
                    <field>
                        <xsl:attribute name="name" select="'j.title.autocomplete'"/>
                        <xsl:value-of select="replace(normalize-space(str[@name='j.title']), ' ', '_')"></xsl:value-of>
                    </field>
                    <!-- BA: 11/03/16 - Add autocomplete - end -->
                    
                    <!-- BA: 11/03/16 - Add allfields - start -->
                    <field>
                        <xsl:attribute name="name" select="'allfields'"/>
                        <xsl:value-of select="str[@name='j.issn']"/>
                        <xsl:value-of select="concat(' ', replace(str[@name='j.issn'], '-', ''), ' ')"/>
                        <xsl:value-of select="concat(str[@name='j.title'], ' ')"/>
                        <xsl:value-of select="substring(replace(replace(normalize-space(str[@name='j.title']), ' ', ''), '&quot;', ''), 1, 10)"/>
                        <xsl:value-of select="concat(' ', str[@name='j.title.abbreviated'], ' ', str[@name='j.publisher'])"/>
                    </field>
                    <!-- BA: 11/03/16 - Add allfields - end -->
                </doc>                        
            </xsl:for-each>
        </add>
    </xsl:template>
       
    <xsl:template match="str">
        <field>
            <xsl:attribute name="name" select="@name"/>
            <xsl:value-of select="."/>
        </field>        
    </xsl:template>
    
    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>
    
</xsl:stylesheet>
