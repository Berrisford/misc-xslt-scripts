<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:local="http://www.example.com/functions/local" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="local xs mods" xmlns:mods="http://www.loc.gov/mods/v3" version="2.0">
	
	<xsl:template name="mods2biblio2" match="doc[str[@name='r.isofcontenttype.pid']='uk-ac-man-con:2']" mode="biblio">

		<!-- Conference contribution -->

		<xsl:param name="citationstyle"/>

		<xsl:if test="$citationstyle='harvard'">
			<!--SURNAME, J.R., SURNAME, B. & SURNAME, C. (2007) The conference paper title. 
                        IN SURNAME, D. (Ed.) The Conference Name. Place, Publisher.        -->
			<!-- No Oxford comma -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors != ''">
				<xsl:value-of select="upper-case(local:truncateAuthors(local:stripPunct($sAuthors),500))"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:text>(</xsl:text>
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>) </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:text>(</xsl:text>
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>) </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>			
			<!--  The title -->
			<xsl:if test="str[@name='m.title'] != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Editors IN SURNAME, D. (Ed.)                    -->
			<xsl:if test="str[@name='m.note.editors'] !=''">
				<xsl:text>IN </xsl:text>
				<xsl:value-of select="normalize-space(upper-case(str[@name='m.note.editors']))"/>
				<xsl:text> (Ed.) </xsl:text>
			</xsl:if>
			<!-- The Host Title -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<em>
					<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				</em>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(str[@name='m.placeterm'])">
				<xsl:value-of select="normalize-space(str[@name='m.placeterm'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(str[@name='m.publisher'])">
				<xsl:value-of select="normalize-space(str[@name='m.publisher'])"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
			<!-- start - end -->
			<xsl:choose>
				<xsl:when test="str[@name='m.host.pages.list'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.list'] != ''">
					<xsl:value-of select="str[@name='m.host.page.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != '' and str[@name='m.host.pages.end'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.pages.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != '' and str[@name='m.host.page.end'] != ''">
					<xsl:value-of select="str[@name='m.host.page.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.page.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:if>

		<xsl:if test="$citationstyle='chicago'">
			<!-- Oxford comma is correct-->
			<!-- <citation type="Chicago">Surname, John Rylands, Bob Surname, and Chris Surname. "The journal article title." The Journal Title Volume, no.? (2007): 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- Oxford comma is correct-->
			<!-- Surname, John Rylands, Bob Surname, and Chris Surname. "The Conference Paper Title." 
                        In The Conference Name, edited by Dave Surname, 382-418. Place: Publisher, 2007.-->
			<xsl:if test="$sAuthors != ''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>

			<!--  The  title -->
			<xsl:if test="local:stripPunct(str[@name='m.title'])!=''">
				<xsl:text> "</xsl:text>
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>." </xsl:text>
			</xsl:if>

			<!-- The host Title -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<!-- The italics are not present in the spec but this seems inconsistent... -->
				<em>
					<xsl:text>In </xsl:text>
					<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				</em>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Editors-->
			<xsl:if test="str[@name='m.note.editors'] !=''">
				<xsl:text>edited by </xsl:text>
				<xsl:value-of select="normalize-space(str[@name='m.note.editors'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- start - end -->
			<xsl:choose>
				<xsl:when test="str[@name='m.host.pages.list'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.list']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.list'] != ''">
					<xsl:value-of select="str[@name='m.host.page.list']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != '' and str[@name='m.host.pages.end'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.pages.end']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != '' and str[@name='m.host.page.end'] != ''">
					<xsl:value-of select="str[@name='m.host.page.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.page.end']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
			</xsl:choose>
			<!-- Place-->
			<xsl:if test="normalize-space(str[@name='m.placeterm'])">
				<xsl:value-of select="normalize-space(str[@name='m.placeterm'])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(str[@name='m.publisher'])">
				<xsl:value-of select="normalize-space(str[@name='m.publisher'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>.</xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>

		<xsl:if test="$citationstyle='turabian'">
			<!--Surname, John Rylands, Bob Surname, and Chris Surname. 2007. The conference paper title. In <em>The Conference Name</em>, ed. Dave Surname:65-69. Place: Publisher.-->
			<!-- No Oxford comma -->

			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<!-- Editors -->
			<xsl:variable name="sEditors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.editors']))">
						<xsl:for-each select="arr[@name='m.host.name.edt']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.host.name.edt']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.editors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<xsl:if test="$sAuthors != ''">
				<xsl:value-of select="local:truncateAuthors(local:stripPunct($sAuthors),500)"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) != '.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>

			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>. </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			
			<!--  The title -->
			<xsl:if test="str[@name='m.title'] != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:if test="substring($sAuthors,string-length(str[@name='m.title']),1) != '?'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>

			<!-- The Conference title, editors and page numbers -->
			<xsl:variable name="conference">
				<!-- start - end -->
				<xsl:variable name="pages">
					<xsl:choose>
						<xsl:when test="str[@name='m.host.pages.list'] != '' and normalize-space(str[@name='m.host.pages.list']) != '-'">
							<xsl:value-of select="str[@name='m.host.pages.list']"/>
						</xsl:when>
						<xsl:when test="str[@name='m.host.page.list'] != ''  and normalize-space(str[@name='m.host.page.list']) != '-'">
							<xsl:value-of select="str[@name='m.host.page.list']"/>
						</xsl:when>
						<xsl:when test="str[@name='m.host.pages.start'] != '' and str[@name='m.host.pages.end'] != ''">
							<xsl:value-of select="str[@name='m.host.pages.start']"/>
							<xsl:text>-</xsl:text>
							<xsl:value-of select="str[@name='m.host.pages.end']"/>
						</xsl:when>
						<xsl:when test="str[@name='m.host.page.start'] != '' and str[@name='m.host.page.end'] != ''">
							<xsl:value-of select="str[@name='m.host.page.start']"/>
							<xsl:text>-</xsl:text>
							<xsl:value-of select="str[@name='m.host.page.end']"/>
						</xsl:when>
						<xsl:when test="str[@name='m.host.pages.start'] != ''">
							<xsl:value-of select="str[@name='m.host.pages.start']"/>
						</xsl:when>
						<xsl:when test="str[@name='m.host.page.start'] != ''">
							<xsl:value-of select="str[@name='m.host.pages.start']"/>
						</xsl:when>
					</xsl:choose>
				</xsl:variable>
							
				<!-- conference title -->
				<xsl:choose>
					<xsl:when test="str[@name='m.host.host.title'] != ''">
						<xsl:text>In </xsl:text>
						<em>
							<xsl:value-of select="local:stripPunct(str[@name='m.host.host.title'])"/>
						</em>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="str[@name='m.host.title'] != ''">
							<xsl:text>In </xsl:text>
							<em>
								<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
							</em>							
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="str[@name='m.host.host.dateother.start'] != ''">
					<xsl:if test="str[@name='m.host.title'] != ''">
						<xsl:text> </xsl:text>
					</xsl:if>
					<xsl:if test="str[@name='m.host.host.dateother.start'] castable as xs:date">
						<xsl:value-of select="format-date(xs:date(str[@name='m.host.host.dateother.start']),'[D01] [MNn] [Y0001]')"/>
						<xsl:if test="str[@name='m.host.host.dateother.end'] castable as xs:date">
							<xsl:text> - </xsl:text>
							<xsl:value-of select="format-date(xs:date(str[@name='m.host.host.dateother.end']),'[D01] [MNn] [Y0001]')"/>
						</xsl:if>
					</xsl:if>
				</xsl:if>
				<!-- Editors -->
				<xsl:if test="$sEditors != ''">
					<xsl:if test="str[@name='m.host.host.title'] != ''">
						<xsl:text>, </xsl:text>
					</xsl:if>
					<xsl:text>edited by </xsl:text>
					<xsl:value-of select="local:truncateAuthors(local:stripPunct($sEditors),500)"/>
				</xsl:if>
	
				<xsl:if test="$pages != ''">
					<xsl:if test="str[@name='m.host.host.title'] != '' and $sEditors != ''">
						<xsl:text>, </xsl:text>
					</xsl:if>
					<xsl:value-of select="$pages"/>
				</xsl:if>
			</xsl:variable>
			
			<xsl:if test="$conference != ''">
				<xsl:copy-of select="$conference"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			
			<!-- Place and publisher-->
			<xsl:variable name="place-publisher">
			<!-- Place-->
				<xsl:if test="normalize-space(str[@name='m.host.host.placeterm']) != ''">
					<xsl:value-of select="normalize-space(str[@name='m.host.host.placeterm'])"/>
				</xsl:if>
			<!-- Publisher -->
				<xsl:if test="normalize-space(str[@name='m.publisher']) != ''">
					<xsl:if test="normalize-space(str[@name='m.host.host.placeterm'])">
						<xsl:text>: </xsl:text>
					</xsl:if>
					<xsl:value-of select="normalize-space(str[@name='m.publisher'])"/>
				</xsl:if>
			</xsl:variable>
			<xsl:if test="$place-publisher != ''">
				<xsl:value-of select="$place-publisher"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:if>

		<xsl:if test="$citationstyle='mla'">
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- Oxford comma is correct-->
			<!-- mla
                        Surname, John Rylands, Bob Surname, and Chris Surname. 2007. The conference paper title. 
                        In The Conference Name, ed. Dave Surname:65-69. Place: Publisher. -->
			<xsl:if test="$sAuthors !=''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The object title -->
			<xsl:if test="local:stripPunct(str[@name='m.title']) !=''">
				<xsl:text> "</xsl:text>
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>." </xsl:text>
			</xsl:if>
			<!-- The host Title -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<span style="text-decoration: underline;">
					<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				</span>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Editors-->
			<xsl:if test="str[@name='m.note.editors'] !=''">
				<xsl:text>Ed. </xsl:text>
				<xsl:value-of select="normalize-space(str[@name='m.note.editors'])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(str[@name='m.placeterm'])">
				<xsl:value-of select="normalize-space(str[@name='m.placeterm'])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(str[@name='m.publisher'])">
				<xsl:value-of select="normalize-space(str[@name='m.publisher'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>. </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="$xsd-date != ''">
				<xsl:value-of select="substring($xsd-date,1,4)"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- start - end -->
			<xsl:choose>
				<xsl:when test="str[@name='m.host.pages.list'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.list'] != ''">
					<xsl:value-of select="str[@name='m.host.page.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != '' and str[@name='m.host.pages.end'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.pages.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != '' and str[@name='m.host.page.end'] != ''">
					<xsl:value-of select="str[@name='m.host.page.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.page.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		
		<xsl:if test="$citationstyle='apa'">
			<!-- apa 
                        Surname, John Rylands, Bob Surname, and Chris Surname. "The Conference Paper Title." The Conference Name. Ed. Dave surname. 
                        Place: Publisher, 2007. 65-69 of Title of Conference Proceedings. Ed. Ernie Surname -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space                       -->
							<xsl:if test="position() !=1">
								<xsl:if test="position() != last()">
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() !=1">
									<xsl:text> &amp; </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:if test="exists($name/name/family)">
										<xsl:value-of select="$name/name/family"/>
									</xsl:if>
									<xsl:if test="exists($name/name/given)">
										<xsl:for-each select="$name/name/given">
											<xsl:if test="position() = 1">
												<xsl:text>, </xsl:text>
											</xsl:if>
											<xsl:if test=". != ''">
												<xsl:value-of select="substring(.,1,1)"/>
												<xsl:text>.</xsl:text>
											</xsl:if>
										</xsl:for-each>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors !=''">
				<xsl:value-of select="local:truncateAuthors(local:stripPunct($sAuthors),500)"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:text>(</xsl:text>
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>) </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:text>(</xsl:text>
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>). </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			
			<!--  The object title -->
			<xsl:if test="str[@name='m.title'] != ''">
				<em>
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				</em>
				<xsl:text>. </xsl:text>
			</xsl:if>
			
			<!-- Editors-->
<!--			<xsl:if test="str[@name='m.note.editors'] !=''">
				<xsl:value-of select="normalize-space(str[@name='m.note.editors'])"/>
				<xsl:text>(Ed.), </xsl:text>
			</xsl:if>-->
			
			<!-- The host Title -->
			<xsl:if test="str[@name='m.host.host.title'] != ''">
				<xsl:text> Presented at </xsl:text>
					<xsl:value-of select="local:stripPunct(str[@name='m.host.host.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			
			<!-- Place and publisher-->
			<xsl:variable name="place-publisher">
				<!-- Place-->
				<xsl:if test="normalize-space(str[@name='m.host.host.placeterm']) != ''">
					<xsl:value-of select="normalize-space(str[@name='m.host.host.placeterm'])"/>
				</xsl:if>
				<!-- Publisher -->
				<xsl:if test="normalize-space(str[@name='m.publisher']) != ''">
					<xsl:if test="normalize-space(str[@name='m.host.host.placeterm'])">
						<xsl:text>: </xsl:text>
					</xsl:if>
					<xsl:value-of select="normalize-space(str[@name='m.publisher'])"/>
				</xsl:if>
			</xsl:variable>
			<xsl:if test="$place-publisher != ''">
				<xsl:value-of select="$place-publisher"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:if>

		<xsl:if test="$citationstyle='vancouver'">
			<!-- Authors -->
					<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- vancouver
                        Surname JR, Surname B, Surname C. The Conference Paper Title. In: Surname D, ed. 
                        The Conference Name. Place: Publisher: Place 2007:65-69
                    -->
			<xsl:if test="$sAuthors !=''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The object title -->
			<xsl:if test="local:stripPunct(str[@name='m.title']) != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Editors-->
			<xsl:if test="str[@name='m.note.editors'] !=''">
				<xsl:text>In: </xsl:text>
				<xsl:value-of select="local:stripPunct(normalize-space(str[@name='m.note.editors']))"/>
				<xsl:text>, ed. </xsl:text>
			</xsl:if>
			<!-- The host Title -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(str[@name='m.placeterm'])">
				<xsl:value-of select="normalize-space(str[@name='m.placeterm'])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(str[@name='m.publisher'])">
				<xsl:value-of select="normalize-space(str[@name='m.publisher'])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- the additional place is a mistake surely -->
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:if test="str[@name='m.dateissued']!=''">
				<xsl:value-of select="substring($xsd-date,1,4)"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- start - end -->
			<xsl:choose>
				<xsl:when test="str[@name='m.host.pages.list'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.list'] != ''">
					<xsl:value-of select="str[@name='m.host.page.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != '' and str[@name='m.host.pages.end'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.pages.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != '' and str[@name='m.host.page.end'] != ''">
					<xsl:value-of select="str[@name='m.host.page.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.page.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
