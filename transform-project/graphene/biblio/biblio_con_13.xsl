<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:local="http://www.example.com/functions/local" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="local xs mods" xmlns:mods="http://www.loc.gov/mods/v3" version="2.0">
	<xsl:template name="mods2biblio13" match="doc[str[@name='r.isofcontenttype.pid']='uk-ac-man-con:13']" mode="biblio">
		<xsl:param name="citationstyle"/>
		<xsl:if test="$citationstyle='harvard'">
			<!-- No Oxford comma -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.pth']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="upper-case(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- authors -->
			<xsl:if test="normalize-space($sAuthors) !=''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:text>(</xsl:text>
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>) </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:text>(</xsl:text>
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>) </xsl:text>
					</xsl:if>					
				</xsl:otherwise>
			</xsl:choose>
			<!--  The object title -->
			<xsl:if test="normalize-space(local:stripPunct(str[@name='m.title'])) != ''">
				<xsl:value-of select="normalize-space(local:stripPunct(str[@name='m.title']))"/>
				<xsl:text>.</xsl:text>
				<xsl:if test="str[@name='m.host.title'] != ''">
					<xsl:text> </xsl:text>
				</xsl:if>
			</xsl:if>
			<!-- Issuing org / number is in the host field-->
			<!-- The host Title -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<em>
					<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				</em>
				<xsl:if test="normalize-space(mods:relatedItem[@type='host']/mods:part/mods:extent/mods:total) != ''">
					<xsl:text>, </xsl:text>
				</xsl:if>
			</xsl:if>
			<!-- Volume is empty in all cases -->
			<!-- total -->
			<xsl:if test="normalize-space(mods:relatedItem[@type='host']/mods:part/mods:extent/mods:total) != ''">
				<xsl:value-of select="normalize-space(mods:relatedItem[@type='host']/mods:part/mods:extent/mods:total)"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$citationstyle='chicago'">
			<!-- Oxford comma is correct-->
			<!-- <citation type="Chicago">Surname, John Rylands, Bob Surname, and Chris Surname. "The journal article title." The Journal Title Volume, no.? (2007): 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="mods:name">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() !=1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() !=1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:choose>
										<xsl:when test="exists(mods:namePart)">
											<xsl:if test="mods:namePart[@type='given']">
												<!-- is not null again -->
												<xsl:value-of select="mods:namePart[@type='given']"/>
												<xsl:text> </xsl:text>
											</xsl:if>
											<xsl:if test="mods:namePart[@type='family']">
												<!-- is not null again -->
												<xsl:value-of select="mods:namePart[@type='family']"/>
											</xsl:if>
										</xsl:when>
										<xsl:when test="(@type='corporate')">
											<xsl:value-of select="mods:displayForm"/>
										</xsl:when>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(./mods:name)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="str[@name='m.note.authors']"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors != ''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The journal article title -->
			<xsl:if test="local:stripPunct(str[@name='m.title'])!=''">
				<xsl:text>"</xsl:text>
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>."</xsl:text>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- The Journal Title -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<em>
					<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				</em>
				<xsl:if test="mods:part/mods:extent/mods:total != ''">
					<xsl:text>. </xsl:text>
				</xsl:if>
			</xsl:if>
			<!-- start - end -->
			<xsl:if test="mods:part/mods:extent/mods:total != ''">
				<xsl:value-of select="mods:part/mods:extent/mods:total"/>
				<xsl:text>.</xsl:text>
				<xsl:if test="str[@name='m.dateissued'] !=''">
					<xsl:text> </xsl:text>
				</xsl:if>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>.</xsl:text>
					</xsl:if>					
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="$citationstyle='turabian'">
			<!-- Oxford comma is correct-->
			<!-- <citation type="Turabian">Surname, John Rylands, Bob Surname, and Chris Surname. The journal article title. The Journal Title Volume, no.? (Part): 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="mods:name">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() !=1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() !=1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:choose>
										<xsl:when test="exists(mods:namePart)">
											<xsl:if test="mods:namePart[@type='given']">
												<!-- is not null again -->
												<xsl:value-of select="mods:namePart[@type='given']"/>
												<xsl:text> </xsl:text>
											</xsl:if>
											<xsl:if test="mods:namePart[@type='family']">
												<!-- is not null again -->
												<xsl:value-of select="mods:namePart[@type='family']"/>
											</xsl:if>
										</xsl:when>
										<xsl:when test="(@type='corporate')">
											<xsl:value-of select="mods:displayForm"/>
										</xsl:when>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(./mods:name)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="str[@name='m.note.authors']"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors !=''">
				<!--  The journal article title -->
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>. </xsl:text>
					</xsl:if>					
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="$xsd-date != ''">
				<xsl:value-of select="substring($xsd-date,1,4)"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<xsl:if test="local:stripPunct(str[@name='m.title']) !=''">
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- The Journal Title -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<em>
					<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				</em>
				<xsl:if test="mods:part/mods:extent/mods:total != ''">
					<xsl:text> </xsl:text>
				</xsl:if>
				<xsl:if test="mods:part/mods:extent/mods:total = ''">
					<xsl:text>.</xsl:text>
				</xsl:if>
			</xsl:if>
			<xsl:if test="mods:part/mods:extent/mods:total != ''">
				<xsl:value-of select="mods:part/mods:extent/mods:total"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$citationstyle='mla'">
			<!-- Oxford comma is correct-->
			<!-- <citation type="MLA">Surname, John Rylands, Bob Surname, and Chris Surname. "The Journal Article Title" The Journal Title Volume.Part (2007): 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="mods:name">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() !=1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() !=1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:choose>
										<xsl:when test="exists(mods:namePart)">
											<xsl:if test="mods:namePart[@type='given']">
												<!-- is not null again -->
												<xsl:value-of select="mods:namePart[@type='given']"/>
												<xsl:text> </xsl:text>
											</xsl:if>
											<xsl:if test="mods:namePart[@type='family']">
												<!-- is not null again -->
												<xsl:value-of select="mods:namePart[@type='family']"/>
											</xsl:if>
										</xsl:when>
										<xsl:when test="(@type='corporate')">
											<xsl:value-of select="mods:displayForm"/>
										</xsl:when>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(./mods:name)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="str[@name='m.note.authors']"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors !=''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The object title -->
			<xsl:if test="local:stripPunct(str[@name='m.title']) !=''">
				<xsl:text>"</xsl:text>
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>." </xsl:text>
			</xsl:if>
			<!-- The Host Title -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				 <span style="text-decoration: underline;">
					<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				 </span>
				<xsl:text>,</xsl:text>
				<xsl:if test="str[@name='m.dateissued']!=''">
					<xsl:text> </xsl:text>
				</xsl:if>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>. </xsl:text>
					</xsl:if>					
				</xsl:otherwise>
			</xsl:choose>
			<!-- start - end -->
			<xsl:if test="mods:part/mods:extent/mods:total != ''">
				<xsl:value-of select="mods:part/mods:extent/mods:total"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$citationstyle='apa'">
			<!-- <citation type="APA">Surname, J.R., Surname, B.B. & Surname, C. (2007) The journal article title. The Journal Title, Volume(Part), 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.patentholders']))">
						<xsl:for-each select="arr[@name='m.name.pth']/str">
							<!-- If the position is <> 1 then comma space                       -->
							<xsl:if test="position() !=1">
								<xsl:if test="position() != last()">
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() !=1">
									<xsl:text> &amp; </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:if test="exists($name/name/family)">
										<xsl:value-of select="$name/name/family"/>
									</xsl:if>
									<xsl:if test="exists($name/name/given)">
										<xsl:for-each select="$name/name/given">
											<xsl:if test="position() = 1">
												<xsl:text>, </xsl:text>
											</xsl:if>
											<xsl:if test=". != ''">
												<xsl:value-of select="substring(.,1,1)"/>
												<xsl:text>.</xsl:text>
											</xsl:if>
										</xsl:for-each>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="str[@name='m.note.patentholders']"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors !=''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:text>(</xsl:text>
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>) </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:text>(</xsl:text>
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>) </xsl:text>
					</xsl:if>					
				</xsl:otherwise>
			</xsl:choose>			
			<!--  The journal article title -->
			<xsl:if test="str[@name='m.title'] != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Patent Number -->
			<xsl:if test="mods:identifier[@type='patentNumber']!=''">
				<xsl:if test="normalize-space(local:stripPunct(mods:identifier[@type='patentNumber'])) !=''">
					<xsl:value-of select="normalize-space(local:stripPunct(mods:identifier[@type='patentNumber']))"/>
					<xsl:text>. </xsl:text>
				</xsl:if>
			</xsl:if>
			<!-- The Journal Title -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<em>
					<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				</em>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- start - end -->
			<xsl:if test="mods:relatedItem[@type='host']/mods:part/mods:extent/mods:total != ''">
				<xsl:value-of select="mods:relatedItem[@type='host']/mods:part/mods:extent/mods:total"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$citationstyle='vancouver'">
			<!-- <citation type="Vancouver">Surname JR, Surname B, Surname C. The journal article title. The Journal Title. 2007 March; Volume(Part): 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="mods:name">
							<!-- If the position is <> 1 then comma space                       -->
							<xsl:if test="position() !=1">
								<!-- This is different to other formats because there is no conjunction -->
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<!--<xsl:if test="position() !=1"><xsl:text> &amp; </xsl:text></xsl:if>-->
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:choose>
										<xsl:when test="exists(mods:namePart)">
											<xsl:if test="mods:namePart[@type='family']">
												<!-- is not null again -->
												<!--  If not the first family name                              -->
												<xsl:value-of select="mods:namePart[@type='family']"/>
												<xsl:text> </xsl:text>
											</xsl:if>
											<xsl:if test="mods:namePart[@type='given']">
												<!-- is not null again -->
												<xsl:for-each select="mods:namePart[@type='given']">
													<xsl:value-of select="substring(upper-case(.),1,1)"/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:when test="(@type='corporate')">
											<xsl:value-of select="mods:displayForm"/>
										</xsl:when>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(./mods:name)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="str[@name='m.note.authors']"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors !=''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The journal article title -->
			<xsl:if test="local:stripPunct(str[@name='m.title']) != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:if test="str[@name='m.host.title'] = ''">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- The Journal Title -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Patent Number -->
			<xsl:if test="mods:identifier[@type='patentNumber']!=''">
				<xsl:if test="normalize-space(local:stripPunct(mods:identifier[@type='patentNumber']))!=''">
					<xsl:value-of select="normalize-space(local:stripPunct(mods:identifier[@type='patentNumber']))"/>
					<xsl:text>.</xsl:text>
					<xsl:if test="str[@name='m.dateissued']!=''">
						<xsl:text> </xsl:text>
					</xsl:if>
				</xsl:if>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="str[@name='m.dateissued']!=''">
						<xsl:variable name="month" select="substring($xsd-date,6,2)"/>
						<xsl:variable name="day" select="substring($xsd-date,9,2)"/>
						<!-- If the string is not null -->
						<xsl:if test="$xsd-date != ''">
							<xsl:value-of select="substring($xsd-date,1,4)"/>
							<xsl:if test="$month !=''">
								<xsl:text> </xsl:text>
								<!-- We have to normalise space of the output. Changing the return values will upset the other data to date -->
								<xsl:value-of select="normalize-space(local:getMonthName($month))"/>
							</xsl:if>
							<xsl:if test="$month !='' and $day !=''">
								<xsl:text> </xsl:text>
								<xsl:value-of select="$day"/>
							</xsl:if>
							<xsl:text>.</xsl:text>
						</xsl:if>
					</xsl:if>				
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<!-- End of Journal Articles -->
	</xsl:template>
</xsl:stylesheet>
