<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:local="http://www.example.com/functions/local" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="local xs mods" xmlns:mods="http://www.loc.gov/mods/v3" version="2.0">
	<xsl:template name="mods2biblio10" match="doc[str[@name='r.isofcontenttype.pid']='uk-ac-man-con:10' or str[@name='r.isofcontenttype.pid']='uk-ac-man-con:11']" mode="biblio">
		<xsl:param name="citationstyle"/>
		<xsl:variable name="sAuthors">
			<xsl:choose>
				<xsl:when test="not(exists(str[@name='m.note.authors']))">
					<xsl:for-each select="mods:name">
						<!-- If the position is <> 1 then comma space                       -->
						<xsl:if test="position() !=1">
							<xsl:if test="position() != last()">
								<xsl:text>, </xsl:text>
							</xsl:if>
						</xsl:if>
						<!-- Squeeze in an 'and' before the last name -->
						<xsl:if test="position() = last()">
							<xsl:if test="position() !=1">
								<xsl:text> &amp; </xsl:text>
							</xsl:if>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="position() &lt; 999">
								<!-- Some formats have a limit to the authors -->
								<xsl:choose>
									<xsl:when test="exists(mods:namePart)">
										<xsl:if test="mods:namePart[@type='family']">
											<!-- is not null again -->
											<!--  If not the first family name                              -->
											<xsl:value-of select="upper-case(mods:namePart[@type='family'])"/>
										</xsl:if>
										<xsl:if test="mods:namePart[@type='given']">
											<!-- is not null again -->
											<xsl:text> </xsl:text>
											<!-- Initial -->
											<xsl:value-of select="substring(upper-case(mods:namePart[@type='given']),1,1)"/>
											<xsl:text>.</xsl:text>
										</xsl:if>
									</xsl:when>
									<xsl:when test="(@type='corporate')">
										<xsl:value-of select="upper-case(mods:displayForm)"/>
									</xsl:when>
								</xsl:choose>
							</xsl:when>
							<xsl:when test="position() = count(./mods:name)">
								<xsl:text> et al.</xsl:text>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
					<!-- capitalise lower down in this instance!!! -->
					<xsl:value-of select="str[@name='m.note.authors']"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- Date -->
		<xsl:variable name="xsd-date">
			<xsl:choose>
				<xsl:when test="mods:originInfo/mods:dateOther[@point='end']!=''">
					<xsl:value-of select="mods:originInfo/mods:dateOther[@point='end']"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="str[@name='m.dateissued']"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="citation-harvard">
			<xsl:value-of select="upper-case($sAuthors)"/>
			<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
				<xsl:text>.</xsl:text>
			</xsl:if>
			<xsl:text> </xsl:text>
			<!--  The title -->
			<xsl:if test="str[@name='m.title'] != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<xsl:text> </xsl:text>
			<!-- Place-->
			<xsl:if test="normalize-space(str[@name='m.placeterm'])">
				<xsl:value-of select="normalize-space(str[@name='m.placeterm'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<xsl:if test="$xsd-date != ''">
				<xsl:variable name="month" select="substring($xsd-date,6,2)"/>
				<xsl:variable name="day" select="substring($xsd-date,9,2)"/>
				<xsl:if test="$day !=''">
					<xsl:value-of select="$day"/>
					<xsl:text> </xsl:text>
				</xsl:if>
				<xsl:if test="$month !=''">
					<xsl:value-of select="local:getMonthName($month)"/>
					<xsl:text> </xsl:text>
				</xsl:if>
				<xsl:value-of select="substring($xsd-date,1,4)"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="citation">
			<xsl:value-of select="$sAuthors"/>
			<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
				<xsl:text>.</xsl:text>
			</xsl:if>
			<xsl:text> </xsl:text>
			<!--  The title -->
			<xsl:if test="str[@name='m.title'] != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<xsl:text> </xsl:text>
			<!-- Place-->
			<xsl:if test="normalize-space(str[@name='m.placeterm'])">
				<xsl:value-of select="normalize-space(str[@name='m.placeterm'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<xsl:if test="$xsd-date != ''">
				<xsl:variable name="month" select="substring($xsd-date,6,2)"/>
				<xsl:variable name="day" select="substring($xsd-date,9,2)"/>
				<xsl:if test="$day !=''">
					<xsl:value-of select="$day"/>
					<xsl:text> </xsl:text>
				</xsl:if>
				<xsl:if test="$month !=''">
					<xsl:value-of select="local:getMonthName($month)"/>
					<xsl:text> </xsl:text>
				</xsl:if>
				<xsl:value-of select="substring($xsd-date,1,4)"/>
				<!-- If there is a value for month, then prefix with 0s and take the right-most 2 chars (to pad single days wit zero) -->
				<xsl:if test="$month !='' and $day !=''">
					<xsl:text> </xsl:text>
					<xsl:value-of select="$day"/>
				</xsl:if>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:variable>
		<xsl:if test="$citationstyle='harvard'">
			<xsl:value-of select="$citation-harvard"/>
		</xsl:if>
		<xsl:if test="$citationstyle='chicago'">
			<xsl:value-of select="$citation"/>
		</xsl:if>
		<xsl:if test="$citationstyle='turabian'">
			<xsl:value-of select="$citation"/>
		</xsl:if>
		<xsl:if test="$citationstyle='mla'">
			<xsl:value-of select="$citation"/>
		</xsl:if>
		<xsl:if test="$citationstyle='apa'">
			<xsl:value-of select="$citation"/>
		</xsl:if>
		<xsl:if test="$citationstyle='vancouver'">
			<xsl:value-of select="$citation"/>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
