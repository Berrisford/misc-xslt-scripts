<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:local="http://www.example.com/functions/local" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="local xs mods" xmlns:mods="http://www.loc.gov/mods/v3" version="2.0">
	<xsl:template name="mods2biblio3" match="object[field[@name='r.isofcontenttype.pid']='uk-ac-man-con:3']" mode="biblio">

		<xsl:param name="citationstyle"/>

		<!--Book contribution-->

		<xsl:if test="$citationstyle='harvard'">
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(field[@name='m.note.authors']))">
						<xsl:for-each select="mods:name">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() !=1">
								<xsl:if test="position() != last()">
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() !=1">
									<xsl:text> &amp; </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:choose>
										<xsl:when test="exists(mods:namePart)">
											<xsl:if test="mods:namePart[@type='family']">
												<!-- is not null again -->
												<!--  If not the first family name -->
												<xsl:value-of select="upper-case(mods:namePart[@type='family'])"/>
											</xsl:if>
											<xsl:if test="mods:namePart[@type='given']">
												<!-- is not null again -->
												<xsl:text> </xsl:text>
												<!-- Initial -->
												<xsl:value-of select="substring(upper-case(mods:namePart[@type='given']),1,1)"/>
												<xsl:text>.</xsl:text>
											</xsl:if>
										</xsl:when>
										<xsl:when test="(@type='corporate')">
											<xsl:value-of select="upper-case(mods:displayForm)"/>
										</xsl:when>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(./mods:name)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="upper-case(field[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- No Oxford comma -->
			<xsl:value-of select="$sAuthors"/>
			<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
				<xsl:text>.</xsl:text>
			</xsl:if>
			<xsl:text> </xsl:text>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="field[@name='m.dateissued']"/>
			<xsl:if test="$xsd-date != ''">
				<xsl:text>(</xsl:text>
				<xsl:value-of select="substring($xsd-date,1,4)"/>
				<xsl:text>) </xsl:text>
			</xsl:if>
			<!--  The title -->
			<xsl:if test="field[@name='m.title'] != ''">
				<xsl:value-of select="local:stripPunct(field[@name='m.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Editors IN SURNAME, D. (Ed.)                    -->
			<xsl:if test="field[@name='m.note.editors'] !=''">
				<xsl:text>IN </xsl:text>
				<xsl:value-of select="normalize-space(upper-case(field[@name='m.note.editors']))"/>
				<xsl:text> (Ed.) </xsl:text>
			</xsl:if>
			<!-- The Host Title -->
			<xsl:if test="field[@name='m.host.title'] != ''">
				<em>
					<xsl:value-of select="local:stripPunct(field[@name='m.host.title'])"/>
				</em>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Edition -->
			<!-- Place-->
			<xsl:if test="normalize-space(mods:originInfo/place/placeTerm)">
				<xsl:value-of select="normalize-space(mods:originInfo/place/placeTerm)"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(mods:originInfo/publisher)">
				<xsl:value-of select="normalize-space(mods:originInfo/publisher)"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
		</xsl:if>

		<xsl:if test="$citationstyle='chicago'">
			<!-- Oxford comma is correct-->
			<!-- <citation type="Chicago">Surname, John Rylands, Bob Surname, and Chris Surname. "The journal article title." The Journal Title Volume, no.? (2007): 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(field[@name='m.note.authors']))">
						<xsl:for-each select="field[@name='m.name.aut']">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(field[@name='m.name.aut'])">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(field[@name='m.note.authors'][1])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- Oxford comma is correct
                    chicago -->
			<xsl:if test="$sAuthors != ''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>

			<!--  The  title -->
			<xsl:if test="field[@name='m.title'][1] !=''">
				<xsl:if test="local:stripPunct(field[@name='m.title'][1])!=''">
					<xsl:text> "</xsl:text>
					<xsl:value-of select="local:stripPunct(field[@name='m.title'][1])"/>
					<xsl:text>." </xsl:text>
				</xsl:if>
			</xsl:if>

			<!-- The host Title -->
			<xsl:if test="field[@name='m.host.title'][1] != ''">
				<!-- The italics are not present in the spec but this seems inconsistent... -->
				<em>
					<xsl:text>In </xsl:text>
					<xsl:value-of select="local:stripPunct(field[@name='m.host.title'][1])"/>
				</em>
				<xsl:text>, </xsl:text>
			</xsl:if>

			<!-- Editors-->
			<xsl:if test="field[@name='m.note.editors'][1] !=''">
				<xsl:text>edited by </xsl:text>
				<xsl:value-of select="normalize-space(field[@name='m.note.editors'][1])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>

			<!-- start - end -->
			<xsl:choose>
				<xsl:when test="field[@name='m.host.pages.list'][1] != ''">
					<xsl:value-of select="field[@name='m.host.pages.list'][1]"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:when test="field[@name='m.host.page.list'][1] != ''">
					<xsl:value-of select="field[@name='m.host.page.list'][1]"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:when test="field[@name='m.host.pages.start'][1] != '' and field[@name='m.host.pages.end'][1] != ''">
					<xsl:value-of select="field[@name='m.host.pages.start'][1]"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="field[@name='m.host.pages.end'][1]"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:when test="field[@name='m.host.page.start'][1] != '' and field[@name='m.host.page.end'][1] != ''">
					<xsl:value-of select="field[@name='m.host.page.start'][1]"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="field[@name='m.host.page.end'][1]"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:when test="field[@name='m.host.pages.start'][1] != ''">
					<xsl:value-of select="field[@name='m.host.pages.start'][1]"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:when test="field[@name='m.host.page.start'][1] != ''">
					<xsl:value-of select="field[@name='m.host.pages.start'][1]"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
			</xsl:choose>

			<!-- Place-->
			<xsl:if test="normalize-space(field[@name='m.placeterm'][1])">
				<xsl:value-of select="normalize-space(field[@name='m.placeterm'][1])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(field[@name='m.publisher'][1])">
				<xsl:value-of select="normalize-space(field[@name='m.publisher'][1])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="field[@name='m.dateissued'][1]"/>
			<xsl:if test="$xsd-date != ''">
				<xsl:value-of select="substring($xsd-date,1,4)"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:if>

		<xsl:if test="$citationstyle='turabian'">
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(field[@name='m.note.authors']))">

						<xsl:for-each select="field[@name='m.name.aut']">

							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>

							<xsl:variable name="name" select="local:splitDisplayName(.)"/>
							<xsl:if test="exists($name/name/family)">
								<xsl:value-of select="$name/name/family"/>
							</xsl:if>
							<xsl:if test="exists($name/name/given)">
								<xsl:text>,</xsl:text>
								<xsl:for-each select="$name/name/given">
									<xsl:if test=". != ''">
										<xsl:text> </xsl:text>
									</xsl:if>
									<xsl:value-of select="."/>
								</xsl:for-each>
							</xsl:if>

						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(field[@name='m.note.authors'][1])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<xsl:if test="$sAuthors != ''">
				<xsl:value-of select="local:stripPunct($sAuthors)"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>

			<!--  The object title -->
			<xsl:if test="field[@name='m.title'][1] !=''">
				<xsl:if test="local:stripPunct(field[@name='m.title'][1]) !=''">
					<xsl:text>"</xsl:text>
					<xsl:value-of select="local:stripPunct(field[@name='m.title'][1])"/>
					<xsl:text>." </xsl:text>
				</xsl:if>
			</xsl:if>

			<!-- The host Title -->
			<xsl:if test="field[@name='m.host.title'][1] != ''">
				<xsl:text>In </xsl:text>
				<em>
					<xsl:value-of select="local:stripPunct(field[@name='m.host.title'][1])"/>
				</em>
				<xsl:text>, </xsl:text>
			</xsl:if>

			<!-- Editors-->
			<xsl:if test="field[@name='m.note.editors'][1] !=''">
				<xsl:text>ed. </xsl:text>
				<xsl:value-of select="normalize-space(field[@name='m.note.editors'][1])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>

			<!-- start - end pages -->
			<xsl:choose>
				<xsl:when test="field[@name='m.host.pages.list'][1] != ''">
					<xsl:value-of select="field[@name='m.host.pages.list'][1]"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="field[@name='m.host.page.list'][1] != ''">
					<xsl:value-of select="field[@name='m.host.page.list'][1]"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="field[@name='m.host.pages.start'][1] != '' and field[@name='m.host.pages.end'][1] != ''">
					<xsl:value-of select="field[@name='m.host.pages.start'][1]"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="field[@name='m.host.pages.end'][1]"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="field[@name='m.host.page.start'][1] != '' and field[@name='m.host.page.end'][1] != ''">
					<xsl:value-of select="field[@name='m.host.page.start'][1]"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="field[@name='m.host.page.end'][1]"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="field[@name='m.host.pages.start'][1] != ''">
					<xsl:value-of select="field[@name='m.host.pages.start'][1]"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="field[@name='m.host.page.start'][1] != ''">
					<xsl:value-of select="field[@name='m.host.pages.start'][1]"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
			</xsl:choose>
			<!-- Place-->
			<xsl:if test="normalize-space(field[@name='m.placeterm'][1])">
				<xsl:text> </xsl:text>
				<xsl:value-of select="normalize-space(field[@name='m.placeterm'][1])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(field[@name='m.publisher'][1])">
				<xsl:value-of select="normalize-space(field[@name='m.publisher'][1])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="field[@name='m.dateissued'][1]"/>
			<xsl:if test="$xsd-date != ''">
				<xsl:value-of select="substring($xsd-date,1,4)"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:if>

		<xsl:if test="$citationstyle='mla'">
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(field[@name='m.note.authors']))">
						<xsl:for-each select="mods:name">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() !=1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() !=1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:choose>
										<xsl:when test="exists(mods:namePart)">
											<xsl:if test="mods:namePart[@type='given']">
												<!-- is not null again -->
												<xsl:value-of select="mods:namePart[@type='given']"/>
												<xsl:text> </xsl:text>
											</xsl:if>
											<xsl:if test="mods:namePart[@type='family']">
												<!-- is not null again -->
												<xsl:value-of select="mods:namePart[@type='family']"/>
											</xsl:if>
										</xsl:when>
										<xsl:when test="(@type='corporate')">
											<xsl:value-of select="mods:displayForm"/>
										</xsl:when>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(./mods:name)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="field[@name='m.note.authors']"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- Oxford comma is correct-->
			<!-- mla
                        -->
			<xsl:if test="$sAuthors !=''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The object title -->
			<xsl:if test="field[@name='m.title'] !=''">
				<xsl:if test="local:stripPunct(field[@name='m.title']) !=''">
					<xsl:text> "</xsl:text>
					<xsl:value-of select="local:stripPunct(field[@name='m.title'])"/>
					<xsl:text>." </xsl:text>
				</xsl:if>
			</xsl:if>
			<!-- The host Title -->
			<xsl:if test="field[@name='m.host.title'] != ''">
				<span style="text-decoration: underline;">
					<xsl:value-of select="local:stripPunct(field[@name='m.host.title'])"/>
				</span>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Editors-->
			<xsl:if test="field[@name='m.note.editors'] !=''">
				<xsl:text>Ed. </xsl:text>
				<xsl:value-of select="normalize-space(field[@name='m.note.editors'])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Volume: This is present in HUM data-->
			<xsl:if test="field[@name='m.host.volume.number'] != ''">
				<xsl:text> </xsl:text>
				<xsl:value-of select="local:stripPunct(field[@name='m.host.volume.number'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(mods:originInfo/place/placeTerm)">
				<xsl:value-of select="normalize-space(mods:originInfo/place/placeTerm)"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(mods:originInfo/publisher)">
				<xsl:value-of select="normalize-space(mods:originInfo/publisher)"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="field[@name='m.dateissued']"/>
			<xsl:if test="$xsd-date != ''">
				<xsl:value-of select="substring($xsd-date,1,4)"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- start - end -->
			<xsl:if test="field[@name='m.host.pages.list'] != ''">
				<xsl:value-of select="field[@name='m.host.pages.list']"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:if>

		<xsl:if test="$citationstyle='apa'">

			<!-- apa 
                        Surname, John Rylands, Bob Surname, and Chris Surname. "The Conference Paper Title." The Conference Name. Ed. Dave surname. 
                        Place: Publisher, 2007. 65-69 of Title of Conference Proceedings. Ed. Ernie Surname -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(field[@name='m.note.authors']))">
						<xsl:for-each select="field[@name='m.name.aut']">
							<!-- If the position is <> 1 then comma space                       -->
							<xsl:if test="position() !=1">
								<xsl:if test="position() != last()">
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() !=1">
									<xsl:text> &amp; </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:if test="exists($name/name/family)">
										<xsl:value-of select="$name/name/family"/>
									</xsl:if>
									<xsl:if test="exists($name/name/given)">
										<xsl:for-each select="$name/name/given">
											<xsl:if test="position() = 1">
												<xsl:text>, </xsl:text>
											</xsl:if>
											<xsl:if test=". != ''">
												<xsl:value-of select="substring(.,1,1)"/>
												<xsl:text>.</xsl:text>
											</xsl:if>
										</xsl:for-each>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() = count(../field[@name='m.name.aut'])">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(field[@name='m.note.authors'][1])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="sEditors">
				<xsl:choose>
					<xsl:when test="not(exists(field[@name='m.note.editors']))">
						<xsl:for-each select="field[@name='m.host.name.edt']">
							<!-- If the position is <> 1 then comma space                       -->
							<xsl:if test="position() !=1">
								<xsl:if test="position() != last()">
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() !=1">
									<xsl:text> &amp; </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:if test="exists($name/name/family)">
										<xsl:value-of select="$name/name/family"/>
									</xsl:if>
									<xsl:if test="exists($name/name/given)">
										<xsl:for-each select="$name/name/given">
											<xsl:if test="position() = 1">
												<xsl:text>, </xsl:text>
											</xsl:if>
											<xsl:if test=". != ''">
												<xsl:value-of select="substring(.,1,1)"/>
												<xsl:text>.</xsl:text>
											</xsl:if>
										</xsl:for-each>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() = count(../field[@name='m.name.edt'])">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(field[@name='m.note.editors'][1])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors !=''">
				<xsl:value-of select="local:truncateAuthors(local:stripPunct($sAuthors),500)"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="field[@name='m.dateissued'][1]"/>
			<xsl:if test="$xsd-date != ''">
				<xsl:text>(</xsl:text>
				<xsl:value-of select="substring($xsd-date,1,4)"/>
				<xsl:text>). </xsl:text>
			</xsl:if>
			<!--  The object title -->
			<xsl:if test="field[@name='m.title'] != ''">
				<xsl:value-of select="local:stripPunct(field[@name='m.title'][1])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Editors-->
			<xsl:if test="$sEditors !=''">
				<xsl:text>In </xsl:text>
				<xsl:value-of select="$sEditors"/>
				<xsl:text> (Ed.), </xsl:text>
			</xsl:if>
			<!-- The host Title -->
			<xsl:if test="field[@name='m.host.title'] != ''">
				<em>
					<xsl:value-of select="local:stripPunct(field[@name='m.host.title'][1])"/>
				</em>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<xsl:if test="(field[@name='m.host.volume.number'] != '') or (field[@name='m.host.page.list'] != '') or (field[@name='m.host.page.start'] != '')">
				<!-- Volume: This is present in HUM data-->
				<xsl:text>(</xsl:text>
				<xsl:if test="field[@name='m.host.volume.number'] != ''">
					<xsl:text> </xsl:text>
					<xsl:value-of select="local:stripPunct(field[@name='m.host.volume.number'][1])"/>
					<xsl:text>, </xsl:text>
				</xsl:if>

				<!-- start - end -->
				<xsl:choose>
					<xsl:when test="field[@name='m.host.pages.list'][1] != ''">
						<xsl:text>pp. </xsl:text>
						<xsl:value-of select="field[@name='m.host.pages.list'][1]"/>
					</xsl:when>
					<xsl:when test="field[@name='m.host.page.list'][1] != ''">
						<xsl:text>pp. </xsl:text>
						<xsl:value-of select="field[@name='m.host.page.list'][1]"/>
					</xsl:when>
					<xsl:when test="field[@name='m.host.pages.start'][1] != '' and field[@name='m.host.pages.end'][1] != ''">
						<xsl:text>pp. </xsl:text>
						<xsl:value-of select="field[@name='m.host.pages.start'][1]"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="field[@name='m.host.pages.end'][1]"/>
					</xsl:when>
					<xsl:when test="field[@name='m.host.page.start'][1] != '' and field[@name='m.host.page.end'][1] != ''">
						<xsl:text>pp. </xsl:text>
						<xsl:value-of select="field[@name='m.host.page.start'][1]"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="field[@name='m.host.page.end'][1]"/>
					</xsl:when>
					<xsl:when test="field[@name='m.host.pages.start'][1] != ''">
						<xsl:text>pp. </xsl:text>
						<xsl:value-of select="field[@name='m.host.pages.start'][1]"/>
					</xsl:when>
					<xsl:when test="field[@name='m.host.page.start'][1] != ''">
						<xsl:text>pp. </xsl:text>
						<xsl:value-of select="field[@name='m.host.pages.start'][1]"/>
					</xsl:when>
				</xsl:choose>
				<xsl:text>). </xsl:text>
			</xsl:if>

			<!-- Place and publisher-->
			<xsl:variable name="place-publisher">
				<!-- Place-->
				<xsl:if test="normalize-space(field[@name='m.placeterm']) != ''">
					<xsl:value-of select="normalize-space(field[@name='m.placeterm'])"/>
				</xsl:if>
				<!-- Publisher -->
				<xsl:if test="normalize-space(field[@name='m.publisher']) != ''">
					<xsl:if test="normalize-space(field[@name='m.placeterm'])">
						<xsl:text>: </xsl:text>
					</xsl:if>
					<xsl:value-of select="normalize-space(field[@name='m.publisher'])"/>
				</xsl:if>
			</xsl:variable>
			<xsl:if test="$place-publisher != ''">
				<xsl:value-of select="$place-publisher"/>
				<xsl:text>.</xsl:text>
			</xsl:if>

		</xsl:if>

		<xsl:if test="$citationstyle='vancouver'">
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(field[@name='m.note.authors']))">
						<xsl:for-each select="mods:name">
							<!-- If the position is <> 1 then comma space                       -->
							<xsl:if test="position() !=1">
								<!-- This is different to other formats because there is no conjunction -->
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<!--<xsl:if test="position() !=1"><xsl:text> &amp; </xsl:text></xsl:if>-->
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:choose>
										<xsl:when test="exists(mods:namePart)">
											<xsl:if test="mods:namePart[@type='family']">
												<!-- is not null again -->
												<!--  If not the first family name                              -->
												<xsl:value-of select="mods:namePart[@type='family']"/>
												<xsl:text> </xsl:text>
											</xsl:if>
											<xsl:if test="mods:namePart[@type='given']">
												<!-- is not null again -->
												<xsl:for-each select="mods:namePart[@type='given']">
													<xsl:value-of select="substring(upper-case(.),1,1)"/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:when test="(@type='corporate')">
											<xsl:value-of select="mods:displayForm"/>
										</xsl:when>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(./mods:name)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="field[@name='m.note.authors']"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- vancouver
                        Surname JR, Surname B, Surname C. The Conference Paper Title. In: Surname D, ed. 
                        The Conference Name. Place: Publisher: Place 2007:65-69
                    -->
			<xsl:if test="$sAuthors !=''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The object title -->
			<xsl:if test="field[@name='m.title'] != ''">
				<xsl:if test="local:stripPunct(field[@name='m.title']) != ''">
					<xsl:value-of select="local:stripPunct(field[@name='m.title'])"/>
					<xsl:text>. </xsl:text>
				</xsl:if>
			</xsl:if>
			<!-- Editors-->
			<xsl:if test="field[@name='m.note.editors'] !=''">
				<xsl:text>In: </xsl:text>
				<xsl:value-of select="local:stripPunct(normalize-space(field[@name='m.note.editors']))"/>
				<xsl:text>, ed. </xsl:text>
			</xsl:if>
			<!-- The host Title -->
			<xsl:if test="field[@name='m.host.title'] != ''">
				<xsl:value-of select="local:stripPunct(field[@name='m.host.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Volume info -->
			<xsl:if test="field[@name='m.host.volume.number'] != ''">
				<xsl:text> </xsl:text>
				<xsl:value-of select="local:stripPunct(field[@name='m.host.volume.number'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(mods:originInfo/place/placeTerm)">
				<xsl:value-of select="normalize-space(mods:originInfo/place/placeTerm)"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(mods:originInfo/publisher)">
				<xsl:value-of select="normalize-space(mods:originInfo/publisher)"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="field[@name='m.dateissued']"/>
			<xsl:if test="field[@name='m.dateissued']!=''">
				<xsl:value-of select="substring($xsd-date,1,4)"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- start - end -->
			<xsl:if test="field[@name='m.host.pages.list'] != ''">
				<xsl:value-of select="field[@name='m.host.pages.list']"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
