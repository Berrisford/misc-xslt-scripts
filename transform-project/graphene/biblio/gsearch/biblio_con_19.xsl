<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:local="http://www.example.com/functions/local" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="local xs mods" xmlns:mods="http://www.loc.gov/mods/v3" version="2.0">
    <xsl:template name="mods2biblio19" match="object[field[@name='r.isofcontenttype.pid']='uk-ac-man-con:19']"
        mode="biblio">
        <!-- lite-cite" pid="uk-ac-man-con:19"
            <name>Lite-Cite submission</name>
            <description>basic description of a scholarly work in a citation format</description> -->
        <!--        Dev note: I am aware that the following is highly redundant.
            In cases where the content type cannot be established, the records are going into lite-cite.
            Therefore, I have created an identical citation format of each type so that target websites do
            not need to include conditional processing. It is likely that consumers will utilise one of the 
            available formats initially.  The dc:identifier element uses unformatted
            Vancouver format and including that here simplifies processing later on.
            If lite-cite is altered prior to ingest (please God no) this allows any additional fields to 
            to be implemented quickly. WS 29/06/2009.
        -->
        <xsl:param name="citationstyle"/>
        <xsl:if test="$citationstyle='harvard'">
            <!--  The journal article title -->
            <xsl:if test="field[@name='m.title'] != ''">
                <xsl:value-of select="local:stripPunct(field[@name='m.title'])"/>
                <xsl:text>. </xsl:text>
            </xsl:if>
        </xsl:if>
        <xsl:if test="$citationstyle='chicago'">
            <!--  The journal article title -->
            <xsl:if test="field[@name='m.title'] != ''">
                <xsl:value-of select="local:stripPunct(field[@name='m.title'])"/>
                <xsl:text>. </xsl:text>
            </xsl:if>
        </xsl:if>
        <xsl:if test="$citationstyle='turabian'">
            <!--  The journal article title -->
            <xsl:if test="field[@name='m.title'] != ''">
                <xsl:value-of select="local:stripPunct(field[@name='m.title'])"/>
                <xsl:text>. </xsl:text>
            </xsl:if>
        </xsl:if>
        <xsl:if test="$citationstyle='mla'">
            <!--  The journal article title -->
            <xsl:if test="field[@name='m.title'] != ''">
                <xsl:value-of select="local:stripPunct(field[@name='m.title'])"/>
                <xsl:text>. </xsl:text>
            </xsl:if>
        </xsl:if>
        <xsl:if test="$citationstyle='apa'">
            <!--  The journal article title -->
            <xsl:if test="field[@name='m.title'] != ''">
                <xsl:value-of select="local:stripPunct(field[@name='m.title'])"/>
                <xsl:text>. </xsl:text>
            </xsl:if>
        </xsl:if>
        <xsl:if test="$citationstyle='vancouver'">
            <!--  The journal article title -->
            <xsl:if test="field[@name='m.title'] != ''">
                <xsl:value-of select="local:stripPunct(field[@name='m.title'])"/>
                <xsl:text>. </xsl:text>
            </xsl:if>
        </xsl:if>
        <!-- End of Lite-cite -->
    </xsl:template>
</xsl:stylesheet>
