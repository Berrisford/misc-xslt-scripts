<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:local="http://www.example.com/functions/local" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="local xs mods" xmlns:mods="http://www.loc.gov/mods/v3" version="2.0">
	<xsl:template name="mods2biblio8" match="object[field[@name='r.isofcontenttype.pid']='uk-ac-man-con:8' or field[@name='r.isofcontenttype.pid']='uk-ac-man-con:7']" mode="biblio">
		<xsl:param name="citationstyle"/>
		<xsl:if test="$citationstyle='harvard'">
			<!-- No Oxford comma -->
			<!-- <citation type="Harvard">SURNAME, J.R., SURNAME, B. & SURNAME, C. (2007) The journal article title. The Journal Title, 8, 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(field[@name='m.note.authors']))">
						<xsl:for-each select="mods:name">
							<!-- If the position is <> 1 then comma space                       -->
							<xsl:if test="position() !=1">
								<xsl:if test="position() != last()">
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() !=1">
									<xsl:text> &amp; </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:choose>
										<xsl:when test="exists(mods:namePart)">
											<xsl:if test="mods:namePart[@type='family']">
												<!-- is not null again -->
												<!--  If not the first family name                              -->
												<xsl:value-of select="upper-case(mods:namePart[@type='family'])"/>
											</xsl:if>
											<xsl:if test="mods:namePart[@type='given']">
												<!-- is not null again -->
												<xsl:text> </xsl:text>
												<!-- Initial -->
												<xsl:value-of select="substring(upper-case(mods:namePart[@type='given']),1,1)"/>
												<xsl:text>.</xsl:text>
											</xsl:if>
										</xsl:when>
										<xsl:when test="(@type='corporate')">
											<xsl:value-of select="upper-case(mods:displayForm)"/>
										</xsl:when>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(./mods:name)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="upper-case(field[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:value-of select="$sAuthors"/>
			<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
				<xsl:text>.</xsl:text>
			</xsl:if>
			<xsl:text> </xsl:text>
			<!--  The title -->
			<xsl:if test="field[@name='m.title'] != ''">
				<xsl:value-of select="local:stripPunct(field[@name='m.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- The host Title -->
			<xsl:if test="field[@name='m.host.title'] != ''">
				<em>
					<xsl:value-of select="local:stripPunct(field[@name='m.host.title'])"/>
				</em>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- start - end = file size -->
			<xsl:choose>
				<xsl:when test="field[@name='m.host.pages.list'] != ''">
					<xsl:value-of select="field[@name='m.host.pages.list']"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> </xsl:text>
					<xsl:value-of select="field[@name='m.host.pages.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="field[@name='m.host.pages.end']"/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>.</xsl:text>
			<!-- Place-->
			<xsl:if test="normalize-space(mods:originInfo/place/placeTerm)">
				<xsl:value-of select="normalize-space(mods:originInfo/place/placeTerm)"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(mods:originInfo/publisher)">
				<xsl:value-of select="normalize-space(mods:originInfo/publisher)"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="field[@name='m.dateissued']"/>
			<xsl:if test="$xsd-date != ''">
				<xsl:value-of select="substring($xsd-date,1,4)"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$citationstyle='chicago'">
			<!-- Oxford comma is correct-->
			<!-- <citation type="Chicago">Surname, John Rylands, Bob Surname, and Chris Surname. "The journal article title." The Journal Title Volume, no.? (2007): 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(field[@name='m.note.authors']))">
						<xsl:for-each select="mods:name">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() !=1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() !=1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:choose>
										<xsl:when test="exists(mods:namePart)">
											<xsl:if test="mods:namePart[@type='given']">
												<!-- is not null again -->
												<xsl:value-of select="mods:namePart[@type='given']"/>
												<xsl:text> </xsl:text>
											</xsl:if>
											<xsl:if test="mods:namePart[@type='family']">
												<!-- is not null again -->
												<xsl:value-of select="mods:namePart[@type='family']"/>
											</xsl:if>
										</xsl:when>
										<xsl:when test="(@type='corporate')">
											<xsl:value-of select="mods:displayForm"/>
										</xsl:when>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(./mods:name)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="field[@name='m.note.authors']"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors != ''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The object title -->
			<xsl:if test="local:stripPunct(field[@name='m.title'])!=''">
				<xsl:value-of select="local:stripPunct(field[@name='m.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- The host Title -->
			<xsl:if test="field[@name='m.host.title'] != ''">
				<em>
					<xsl:value-of select="local:stripPunct(field[@name='m.host.title'])"/>
				</em>
			</xsl:if>
			<!-- start - end -->
			<xsl:if test="field[@name='m.host.pages.list'] != ''">
				<xsl:value-of select="field[@name='m.host.pages.list']"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(mods:originInfo/place/placeTerm)">
				<xsl:value-of select="normalize-space(mods:originInfo/place/placeTerm)"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(mods:originInfo/publisher)">
				<xsl:value-of select="normalize-space(mods:originInfo/publisher)"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="field[@name='m.dateissued']"/>
			<xsl:if test="$xsd-date != ''">
				<xsl:value-of select="substring($xsd-date,1,4)"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$citationstyle='turabian'">
			<!-- Oxford comma is correct-->
			<!-- <citation type="Turabian">Surname, John Rylands, Bob Surname, and Chris Surname. The journal article title. The Journal Title Volume, no.? (Part): 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(field[@name='m.note.authors']))">
						<xsl:for-each select="mods:name">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() !=1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() !=1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:choose>
										<xsl:when test="exists(mods:namePart)">
											<xsl:if test="mods:namePart[@type='given']">
												<!-- is not null again -->
												<xsl:value-of select="mods:namePart[@type='given']"/>
												<xsl:text> </xsl:text>
											</xsl:if>
											<xsl:if test="mods:namePart[@type='family']">
												<!-- is not null again -->
												<xsl:value-of select="mods:namePart[@type='family']"/>
											</xsl:if>
										</xsl:when>
										<xsl:when test="(@type='corporate')">
											<xsl:value-of select="mods:displayForm"/>
										</xsl:when>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(./mods:name)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="field[@name='m.note.authors']"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors !=''">
				<!--  The object title -->
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<xsl:if test="local:stripPunct(field[@name='m.title']) !=''">
				<xsl:value-of select="local:stripPunct(field[@name='m.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- The Host Title -->
			<xsl:if test="field[@name='m.host.title'] != ''">
				<em>
					<xsl:value-of select="local:stripPunct(field[@name='m.host.title'])"/>
				</em>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- start - end -->
			<xsl:if test="field[@name='m.host.pages.list'] != ''">
				<xsl:value-of select="field[@name='m.host.pages.list']"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(mods:originInfo/place/placeTerm)">
				<xsl:value-of select="normalize-space(mods:originInfo/place/placeTerm)"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(mods:originInfo/publisher)">
				<xsl:value-of select="normalize-space(mods:originInfo/publisher)"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="field[@name='m.dateissued']"/>
			<xsl:if test="$xsd-date != ''">
				<xsl:value-of select="substring($xsd-date,1,4)"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$citationstyle='mla'">
			<!-- Oxford comma is correct-->
			<!-- <citation type="MLA">Surname, John Rylands, Bob Surname, and Chris Surname. "The Journal Article Title" The Journal Title Volume.Part (2007): 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(field[@name='m.note.authors']))">
						<xsl:for-each select="mods:name">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() !=1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() !=1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:choose>
										<xsl:when test="exists(mods:namePart)">
											<xsl:if test="mods:namePart[@type='given']">
												<!-- is not null again -->
												<xsl:value-of select="mods:namePart[@type='given']"/>
												<xsl:text> </xsl:text>
											</xsl:if>
											<xsl:if test="mods:namePart[@type='family']">
												<!-- is not null again -->
												<xsl:value-of select="mods:namePart[@type='family']"/>
											</xsl:if>
										</xsl:when>
										<xsl:when test="(@type='corporate')">
											<xsl:value-of select="mods:displayForm"/>
										</xsl:when>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(./mods:name)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="field[@name='m.note.authors']"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors !=''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The object title -->
			<xsl:if test="local:stripPunct(field[@name='m.title']) !=''">
				<xsl:value-of select="local:stripPunct(field[@name='m.title'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- The host Title -->
			<xsl:if test="field[@name='m.host.title'] != ''">
				 <span style="text-decoration: underline;">
					<xsl:value-of select="local:stripPunct(field[@name='m.host.title'])"/>
				 </span>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- start - end -->
			<xsl:if test="field[@name='m.host.pages.list'] != ''">
				<xsl:value-of select="field[@name='m.host.pages.list']"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(mods:originInfo/place/placeTerm)">
				<xsl:value-of select="normalize-space(mods:originInfo/place/placeTerm)"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(mods:originInfo/publisher)">
				<xsl:value-of select="normalize-space(mods:originInfo/publisher)"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="field[@name='m.dateissued']"/>
			<xsl:if test="$xsd-date != ''">
				<xsl:value-of select="substring($xsd-date,1,4)"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$citationstyle='apa'">
			<!-- <citation type="APA">Surname, J.R., Surname, B.B. & Surname, C. (2007) The journal article title. The Journal Title, Volume(Part), 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(field[@name='m.note.authors']))">
						<xsl:for-each select="mods:name">
							<!-- If the position is <> 1 then comma space                       -->
							<xsl:if test="position() !=1">
								<xsl:if test="position() != last()">
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() !=1">
									<xsl:text> &amp; </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:choose>
										<xsl:when test="exists(mods:namePart)">
											<xsl:if test="mods:namePart[@type='family']">
												<!-- is not null again -->
												<!--  If not the first family name                              -->
												<xsl:value-of select="mods:namePart[@type='family']"/>
											</xsl:if>
											<xsl:if test="mods:namePart[@type='given']">
												<!-- is not null again -->
												<xsl:text>, </xsl:text>
												<xsl:for-each select="mods:namePart[@type='given']">
													<!-- Initial -->
													<xsl:value-of select="substring(upper-case(.),1,1)"/>
												</xsl:for-each>
												<xsl:text>.</xsl:text>
											</xsl:if>
										</xsl:when>
										<xsl:when test="(@type='corporate')">
											<xsl:value-of select="mods:displayForm"/>
										</xsl:when>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(./mods:name)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="field[@name='m.note.authors']"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!--apa                     -->
			<xsl:if test="$sAuthors !=''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  Object -->
			<xsl:if test="field[@name='m.title'] != ''">
				<xsl:value-of select="local:stripPunct(field[@name='m.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- The Host Title -->
			<xsl:if test="field[@name='m.host.title'] != ''">
				<em>
					<xsl:value-of select="local:stripPunct(field[@name='m.host.title'])"/>
				</em>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- start - end -->
			<xsl:if test="field[@name='m.host.pages.list'] != ''">
				<xsl:value-of select="field[@name='m.host.pages.list']"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(mods:originInfo/place/placeTerm)">
				<xsl:value-of select="normalize-space(mods:originInfo/place/placeTerm)"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(mods:originInfo/publisher)">
				<xsl:value-of select="normalize-space(mods:originInfo/publisher)"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="field[@name='m.dateissued']"/>
			<xsl:if test="$xsd-date != ''">
				<xsl:value-of select="substring($xsd-date,1,4)"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$citationstyle='vancouver'">
			<!-- <citation type="Vancouver">Surname JR, Surname B, Surname C. The journal article title. The Journal Title. 2007 March; Volume(Part): 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(field[@name='m.note.authors']))">
						<xsl:for-each select="mods:name">
							<!-- If the position is <> 1 then comma space                       -->
							<xsl:if test="position() !=1">
								<!-- This is different to other formats because there is no conjunction -->
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<!--<xsl:if test="position() !=1"><xsl:text> &amp; </xsl:text></xsl:if>-->
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:choose>
										<xsl:when test="exists(mods:namePart)">
											<xsl:if test="mods:namePart[@type='family']">
												<!-- is not null again -->
												<!--  If not the first family name                              -->
												<xsl:value-of select="mods:namePart[@type='family']"/>
												<xsl:text> </xsl:text>
											</xsl:if>
											<xsl:if test="mods:namePart[@type='given']">
												<!-- is not null again -->
												<xsl:for-each select="mods:namePart[@type='given']">
													<xsl:value-of select="substring(upper-case(.),1,1)"/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:when test="(@type='corporate')">
											<xsl:value-of select="mods:displayForm"/>
										</xsl:when>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(./mods:name)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="field[@name='m.note.authors']"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors !=''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The journal article title -->
			<xsl:if test="local:stripPunct(field[@name='m.title']) != ''">
				<xsl:value-of select="local:stripPunct(field[@name='m.title'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- The Journal Title -->
			<xsl:if test="field[@name='m.host.title'] != ''">
				<xsl:value-of select="local:stripPunct(field[@name='m.host.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- start - end -->
			<xsl:if test="field[@name='m.host.pages.list'] != ''">
				<xsl:value-of select="field[@name='m.host.pages.list']"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(mods:originInfo/place/placeTerm)">
				<xsl:value-of select="normalize-space(mods:originInfo/place/placeTerm)"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(mods:originInfo/publisher)">
				<xsl:value-of select="normalize-space(mods:originInfo/publisher)"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="field[@name='m.dateissued']"/>
			<xsl:if test="$xsd-date != ''">
				<xsl:value-of select="substring($xsd-date,1,4)"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
