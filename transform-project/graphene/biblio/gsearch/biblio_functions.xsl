<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:local="http://www.example.com/functions/local" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="local xs mods" xmlns:mods="http://www.loc.gov/mods/v3" version="2.0">

	<xsl:function name="local:truncateAuthors">
		<xsl:param name="sInput"/>
		<xsl:param name="iMax"/>
		<xsl:choose>
			<xsl:when test="string-length($sInput) > $iMax">
				<xsl:value-of select="concat(substring-before($sInput,' '),' et al')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$sInput"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

	<xsl:function name="local:stripPunct">
		<xsl:param name="sInput"/>
		<xsl:variable name="sInput" select="normalize-space($sInput)"/>
		<xsl:choose>
			<xsl:when test="substring($sInput,string-length($sInput),1) = '.'">
				<xsl:value-of select="substring($sInput,1,string-length($sInput)-1)"/>
			</xsl:when>
			<xsl:when test="substring($sInput,string-length($sInput),1) = ','">
				<xsl:value-of select="substring($sInput,1,string-length($sInput)-1)"/>
			</xsl:when>
			<xsl:when test="substring($sInput,string-length($sInput),1) = ':'">
				<xsl:value-of select="substring($sInput,1,string-length($sInput)-1)"/>
			</xsl:when>
			<xsl:when test="substring($sInput,string-length($sInput),1) = ';'">
				<xsl:value-of select="substring($sInput,1,string-length($sInput)-1)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$sInput"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

	<xsl:function name="local:getMonthName">
		<xsl:param name="monthNumberPadded"/>
		<xsl:variable name="output">
			<xsl:choose>
				<xsl:when test="$monthNumberPadded = '01'">
					<xsl:text> January</xsl:text>
				</xsl:when>
				<xsl:when test="$monthNumberPadded = '02'">
					<xsl:text> February</xsl:text>
				</xsl:when>
				<xsl:when test="$monthNumberPadded = '03'">
					<xsl:text> March</xsl:text>
				</xsl:when>
				<xsl:when test="$monthNumberPadded = '04'">
					<xsl:text> April</xsl:text>
				</xsl:when>
				<xsl:when test="$monthNumberPadded =  '05'">
					<xsl:text> May</xsl:text>
				</xsl:when>
				<xsl:when test="$monthNumberPadded = '06'">
					<xsl:text> June</xsl:text>
				</xsl:when>
				<xsl:when test="$monthNumberPadded = '07'">
					<xsl:text> July</xsl:text>
				</xsl:when>
				<xsl:when test="$monthNumberPadded = '08'">
					<xsl:text> August</xsl:text>
				</xsl:when>
				<xsl:when test="$monthNumberPadded = '09'">
					<xsl:text> September</xsl:text>
				</xsl:when>
				<xsl:when test="$monthNumberPadded = '10'">
					<xsl:text> October</xsl:text>
				</xsl:when>
				<xsl:when test="$monthNumberPadded = '11'">
					<xsl:text> November</xsl:text>
				</xsl:when>
				<xsl:when test="$monthNumberPadded = '12'">
					<xsl:text> December</xsl:text>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="$output"/>
	</xsl:function>

	<xsl:function name="local:splitDisplayName">
		<xsl:param name="displayname"/>
		<xsl:variable name="format" select="if (contains($displayname,',')) then 2 else 1"/>
		<xsl:variable name="namesequence" select="if ($format = 1) then tokenize($displayname,' ') else tokenize($displayname,',')"/>
		<xsl:variable name="first" select="if ($format = 1) then $namesequence[1] else $namesequence[last()]"/>
		<xsl:variable name="middle">
			<xsl:if test="$format = 1">
				<xsl:for-each select="$namesequence">
					<xsl:if test="position() != 1 and position() != last()">
						<xsl:value-of select="."/>
					</xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="last" select="if ($format = 1) then $namesequence[last()] else $namesequence[1]"/>
		<xsl:variable name="output">
			<name>
				<given>
					<xsl:value-of select="normalize-space($first)"/>
				</given>
				<given>
					<xsl:value-of select="normalize-space($middle)"/>
				</given>
				<family>
					<xsl:value-of select="normalize-space($last)"/>
				</family>
			</name>
		</xsl:variable>
		<xsl:copy-of select="$output"/>
	</xsl:function>

</xsl:stylesheet>
