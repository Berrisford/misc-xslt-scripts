<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:local="http://www.example.com/functions/local" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="local xs mods" xmlns:mods="http://www.loc.gov/mods/v3" version="2.0">
	<xsl:template name="mods2biblio5" match="doc[str[@name='r.isofcontenttype.pid']='uk-ac-man-con:5' or str[@name='r.isofcontenttype.pid']='uk-ac-man-con:14']" mode="biblio">
		<xsl:param name="citationstyle"/>
		<!--  report and technical-report -->
		<xsl:if test="$citationstyle='harvard'">
			<!-- authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors != ''">
				<xsl:value-of select="upper-case(local:truncateAuthors(local:stripPunct($sAuthors),500))"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:text>(</xsl:text>
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>) </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:text>(</xsl:text>
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>) </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<!--  The title -->
			<xsl:if test="str[@name='m.title'] != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Editors IN SURNAME, D. (Ed.)                    -->
			<xsl:if test="str[@name='m.note.editors'] !=''">
				<xsl:text>IN </xsl:text>
				<xsl:value-of select="normalize-space(upper-case(str[@name='m.note.editors']))"/>
				<xsl:text> (Ed.) </xsl:text>
			</xsl:if>
			<!-- The Host Title -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<em>
					<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				</em>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(str[@name='m.placeterm']) !=''">
				<xsl:value-of select="normalize-space(str[@name='m.placeterm'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(str[@name='m.publisher']) != ''">
				<xsl:value-of select="normalize-space(str[@name='m.publisher'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$citationstyle='chicago'">
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- Oxford comma is correct chicago -->
			<xsl:if test="$sAuthors != ''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The  title -->
			<xsl:if test="local:stripPunct(str[@name='m.title'])!=''">
				<xsl:text> "</xsl:text>
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>." </xsl:text>
			</xsl:if>
			<!-- The host Title -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<!-- The italics are not present in the spec but this seems inconsistent... -->
				<xsl:text>In </xsl:text>
				<em>
					<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				</em>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Editors-->
			<xsl:if test="str[@name='m.note.editors'] !=''">
				<xsl:text>edited by </xsl:text>
				<xsl:value-of select="normalize-space(str[@name='m.note.editors'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- start - end -->
			<xsl:choose>
				<xsl:when test="str[@name='m.host.pages.list'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.list'] != ''">
					<xsl:value-of select="str[@name='m.host.page.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != '' and str[@name='m.host.pages.end'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.pages.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != '' and str[@name='m.host.page.end'] != ''">
					<xsl:value-of select="str[@name='m.host.page.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.page.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
			</xsl:choose>
			<!-- Place-->
			<xsl:if test="normalize-space(str[@name='m.placeterm']) !=''">
				<xsl:value-of select="normalize-space(str[@name='m.placeterm'])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(str[@name='m.publisher']) != ''">
				<xsl:value-of select="normalize-space(str[@name='m.publisher'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>. </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="$citationstyle='turabian'">
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors != ''">
				<xsl:value-of select="local:truncateAuthors(local:stripPunct($sAuthors),500)"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>. </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<!--  The object title -->
			<xsl:if test="local:stripPunct(str[@name='m.title']) !=''">
				<em>
					<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				</em>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(str[@name='m.placeterm']) !=''">
				<xsl:value-of select="normalize-space(str[@name='m.placeterm'])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(str[@name='m.publisher']) != ''">
				<xsl:value-of select="normalize-space(str[@name='m.publisher'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Institution Type -->
			<!-- spec says report number whatever this is. It could be volume field, but i'm using pages here! -->
			<!-- start - end -->
			<xsl:choose>
				<xsl:when test="str[@name='m.host.pages.list'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.list'] != ''">
					<xsl:value-of select="str[@name='m.host.page.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != '' and str[@name='m.host.pages.end'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.pages.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != '' and str[@name='m.host.page.end'] != ''">
					<xsl:value-of select="str[@name='m.host.page.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.page.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="$citationstyle='mla'">
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors != ''">
				<xsl:value-of select="local:truncateAuthors(local:stripPunct($sAuthors),500)"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The object title -->
			<xsl:if test="local:stripPunct(str[@name='m.title']) !=''">
				<span style="text-decoration: underline;">
					<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				</span>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(str[@name='m.placeterm']) !=''">
				<xsl:value-of select="normalize-space(str[@name='m.placeterm'])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(str[@name='m.publisher']) != ''">
				<xsl:value-of select="normalize-space(str[@name='m.publisher'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<!--                       Full stop is not in spec but assumed -->
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>. </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="$citationstyle='apa'">
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors != ''">
				<xsl:value-of select="local:truncateAuthors(local:stripPunct($sAuthors),500)"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:text>(</xsl:text>
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>). </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:text>(</xsl:text>
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>). </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<!--  The object title -->
			<xsl:if test="str[@name='m.title'] != ''">
				<em>
					<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				</em>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Check this with real data                    -->
			<xsl:if test="(str[@name='m.host.volume'] != '') or (str[@name='m.host.pages.list'] != '')">
				<!-- Volume: This is present in HUM data-->
				<xsl:text>(</xsl:text>
				<xsl:if test="str[@name='m.host.volume'] != ''">
					<xsl:text> </xsl:text>
					<xsl:value-of select="local:stripPunct(str[@name='m.host.volume'])"/>
					<xsl:text>, </xsl:text>
				</xsl:if>
				<!-- start - end -->
				<xsl:choose>
					<xsl:when test="str[@name='m.host.pages.list'] != ''">
						<xsl:value-of select="str[@name='m.host.pages.list']"/>
						<xsl:text>.</xsl:text>
					</xsl:when>
					<xsl:when test="str[@name='m.host.page.list'] != ''">
						<xsl:value-of select="str[@name='m.host.page.list']"/>
						<xsl:text>.</xsl:text>
					</xsl:when>
					<xsl:when test="str[@name='m.host.pages.start'] != '' and str[@name='m.host.pages.end'] != ''">
						<xsl:value-of select="str[@name='m.host.pages.start']"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="str[@name='m.host.pages.end']"/>
						<xsl:text>.</xsl:text>
					</xsl:when>
					<xsl:when test="str[@name='m.host.page.start'] != '' and str[@name='m.host.page.end'] != ''">
						<xsl:value-of select="str[@name='m.host.page.start']"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="str[@name='m.host.page.end']"/>
						<xsl:text>.</xsl:text>
					</xsl:when>
					<xsl:when test="str[@name='m.host.pages.start'] != ''">
						<xsl:value-of select="str[@name='m.host.pages.start']"/>
						<xsl:text>.</xsl:text>
					</xsl:when>
					<xsl:when test="str[@name='m.host.page.start'] != ''">
						<xsl:value-of select="str[@name='m.host.pages.start']"/>
						<xsl:text>.</xsl:text>
					</xsl:when>
				</xsl:choose>
				<xsl:text>). </xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(str[@name='m.placeterm']) !=''">
				<xsl:value-of select="normalize-space(str[@name='m.placeterm'])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(str[@name='m.publisher']) != ''">
				<xsl:value-of select="normalize-space(str[@name='m.publisher'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$citationstyle='vancouver'">
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors != ''">
				<xsl:value-of select="local:truncateAuthors(local:stripPunct($sAuthors),500)"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The object title -->
			<xsl:if test="local:stripPunct(str[@name='m.title']) != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Type? not present in the data-->
			<!-- Place-->
			<xsl:if test="normalize-space(str[@name='m.placeterm']) !=''">
				<xsl:value-of select="normalize-space(str[@name='m.placeterm'])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(str[@name='m.publisher']) != ''">
				<xsl:value-of select="normalize-space(str[@name='m.publisher'])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="str[@name='m.dateissued']!=''">
						<xsl:variable name="month" select="substring($xsd-date,6,2)"/>
						<xsl:variable name="day" select="substring($xsd-date,9,2)"/>
						<xsl:if test="$xsd-date != ''">
							<xsl:value-of select="substring($xsd-date,1,4)"/>
							<xsl:if test="$month !=''">
								<xsl:value-of select="local:getMonthName($month)"/>
							</xsl:if>
							<!-- If there is a value for month, then prefix with 0s and take the right-most 2 chars (to pad single days wit zero) -->
							<xsl:if test="$month !='' and $day !=''">
								<xsl:text> </xsl:text>
								<xsl:value-of select="$day"/>
							</xsl:if>
							<xsl:text>. </xsl:text>
						</xsl:if>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<!-- start - end -->
			<xsl:choose>
				<xsl:when test="str[@name='m.host.pages.list'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.list'] != ''">
					<xsl:value-of select="str[@name='m.host.page.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != '' and str[@name='m.host.pages.end'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.pages.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != '' and str[@name='m.host.page.end'] != ''">
					<xsl:value-of select="str[@name='m.host.page.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.page.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
