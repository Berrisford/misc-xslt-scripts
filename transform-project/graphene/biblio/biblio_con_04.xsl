<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:local="http://www.example.com/functions/local" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="local xs mods" xmlns:mods="http://www.loc.gov/mods/v3" version="2.0">
	<xsl:template name="mods2biblio4" match="doc[str[@name='r.isofcontenttype.pid']='uk-ac-man-con:4' or str[@name='r.isofcontenttype.pid']='uk-ac-man-con:9' or str[@name='r.isofcontenttype.pid']='uk-ac-man-con:15'  or str[@name='r.isofcontenttype.pid']='uk-ac-man-con:20']" mode="biblio">
		<xsl:param name="citationstyle"/>
		<xsl:if test="$citationstyle='harvard'">
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="arr[@name='m.name.aut']/str">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() !=1">
								<xsl:if test="position() != last()">
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() !=1">
									<xsl:text> &amp; </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:choose>
							<xsl:when test="str[@name='m.genre.version'] = 'Edited book'">
								<xsl:value-of select="str[@name='m.note.editors']"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="str[@name='m.note.authors']"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- No Oxford comma -->
			<xsl:value-of select="upper-case($sAuthors)"/>
			<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
				<xsl:text>.</xsl:text>
			</xsl:if>
			<xsl:text> </xsl:text>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:text>(</xsl:text>
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>) </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:text>(</xsl:text>
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>) </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<!--  The title -->
			<xsl:if test="str[@name='m.title'] != ''">
				<em>
					<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				</em>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(str[@name='m.placeterm'])">
				<xsl:value-of select="normalize-space(str[@name='m.placeterm'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(str[@name='m.publisher'])">
				<xsl:value-of select="normalize-space(str[@name='m.publisher'])"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
		</xsl:if>
		
		<xsl:if test="$citationstyle='chicago'">
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- Oxford comma is correct
                    chicago -->
			<xsl:if test="$sAuthors != ''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The  title -->
			<xsl:if test="local:stripPunct(str[@name='m.title'])!=''">
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Editors-->
			<xsl:if test="str[@name='m.note.editors'] !=''">
				<xsl:text>Edited by </xsl:text>
				<xsl:value-of select="normalize-space(str[@name='m.note.editors'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- The host Title - Series -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Volume -->
			<xsl:if test="str[@name='m.host.volume'] != ''">
				<xsl:value-of select="str[@name='m.host.volume']"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(str[@name='m.placeterm'])">
				<xsl:value-of select="normalize-space(str[@name='m.placeterm'])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(str[@name='m.publisher'])">
				<xsl:value-of select="normalize-space(str[@name='m.publisher'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>. </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<!-- Total pages -->
			<xsl:variable name="totalpages">
				<xsl:choose>
					<xsl:when test="str[@name='m.pages.total'] != ''">
						<xsl:value-of select="str[@name='m.pages.total']"/>
					</xsl:when>
					<xsl:when test="str[@name='m.page.total'] != ''">
						<xsl:value-of select="str[@name='m.page.total']"/>
					</xsl:when>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$totalpages != ''">
				<xsl:value-of select="$totalpages"/>
				<xsl:text> p.</xsl:text>
			</xsl:if>
		</xsl:if>

		<xsl:if test="$citationstyle='turabian'">
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
						
			<!-- Editors -->
			<xsl:variable name="sEditors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.editors']))">
						<xsl:for-each select="arr[@name='m.name.edt']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.edt']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.editors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			
			<xsl:variable name="authorsformatted">
				<xsl:if test="$sAuthors != ''">
					<xsl:value-of select="$sAuthors"/>
					<xsl:if test="substring($sAuthors,string-length($sAuthors),1)!='.'">
						<xsl:text>.</xsl:text>
					</xsl:if>
					<xsl:text> </xsl:text>
				</xsl:if>
			</xsl:variable>

			<xsl:variable name="editorsformatted">
				<xsl:if test="$sEditors != ''">
					<xsl:value-of select="$sEditors"/>
					<xsl:if test="substring($sEditors,string-length($sEditors),1)!='.'">
						<xsl:text>.</xsl:text>
					</xsl:if>
					<xsl:text> </xsl:text>
				</xsl:if>
			</xsl:variable>
			
			<!--  The  title -->
			<xsl:variable name="titleformatted">
				<xsl:if test="local:stripPunct(str[@name='m.title'])!=''">
					<em>
						<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
						<xsl:text>.</xsl:text>
					</em>
					<xsl:text> </xsl:text>
				</xsl:if>
			</xsl:variable>

			<xsl:choose>
				<xsl:when test="str[@name='m.genre.version']='Authored book'">
					<!-- Editors-->
					<xsl:value-of select="$authorsformatted"/>
					<xsl:copy-of select="$titleformatted"/>
					<xsl:if test="$editorsformatted !=''">
						<xsl:text>Edited by </xsl:text>
						<xsl:value-of select="$editorsformatted"/>
						<xsl:text>, </xsl:text>
					</xsl:if>
				</xsl:when>
				<xsl:when test="str[@name='m.genre.version']='Edited book'">
					<xsl:value-of select="$authorsformatted"/>
					<xsl:if test="$editorsformatted !=''">
						<xsl:value-of select="$editorsformatted"/>
						<xsl:text>ed. </xsl:text>
					</xsl:if>
					<xsl:copy-of select="$titleformatted"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$authorsformatted"/>
					<xsl:copy-of select="$titleformatted"/>
					<xsl:if test="$editorsformatted !=''">
						<xsl:text>Edited by </xsl:text>
						<xsl:value-of select="$editorsformatted"/>
						<xsl:text>, </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose> 
				
			<!-- The host Title - Series -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			
			<!-- Volume -->
			<xsl:if test="str[@name='m.host.volume'] != ''">
				<xsl:value-of select="str[@name='m.host.volume']"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			
			<!-- Place-->
			<xsl:if test="normalize-space(str[@name='m.placeterm'])">
				<xsl:value-of select="normalize-space(str[@name='m.placeterm'])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			
			<!-- Publisher -->
			<xsl:if test="normalize-space(str[@name='m.publisher'])">
				<xsl:value-of select="normalize-space(str[@name='m.publisher'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>. </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			
		</xsl:if>

		<xsl:if test="$citationstyle='mla'">
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="mods:name">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() !=1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() !=1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:choose>
										<xsl:when test="exists(mods:namePart)">
											<xsl:if test="mods:namePart[@type='given']">
												<!-- is not null again -->
												<xsl:value-of select="mods:namePart[@type='given']"/>
												<xsl:text> </xsl:text>
											</xsl:if>
											<xsl:if test="mods:namePart[@type='family']">
												<!-- is not null again -->
												<xsl:value-of select="mods:namePart[@type='family']"/>
											</xsl:if>
										</xsl:when>
										<xsl:when test="(@type='corporate')">
											<xsl:value-of select="mods:displayForm"/>
										</xsl:when>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(./mods:name)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="str[@name='m.note.authors']"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- Oxford comma is correct-->
			<!-- mla
                        -->
			<xsl:if test="$sAuthors !=''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The object title -->
			<xsl:if test="local:stripPunct(str[@name='m.title']) !=''">
				 <span style="text-decoration: underline;">
					<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				 </span>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- The series Title? -->
			<!--<xsl:if test="str[@name='m.host.title'] != ''">
                        <xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
                        <xsl:text>. </xsl:text></xsl:if>
                    -->
			<!-- Editors-->
			<xsl:if test="str[@name='m.note.editors'] !=''">
				<xsl:text>Ed. </xsl:text>
				<xsl:value-of select="normalize-space(str[@name='m.note.editors'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Volume: This is present in HUM data-->
			<xsl:if test="str[@name='m.host.volume'] != ''">
				<xsl:text> </xsl:text>
				<xsl:value-of select="local:stripPunct(str[@name='m.host.volume'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(str[@name='m.placeterm'])">
				<xsl:value-of select="normalize-space(str[@name='m.placeterm'])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(str[@name='m.publisher'])">
				<xsl:value-of select="normalize-space(str[@name='m.publisher'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>. </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<!-- start - end -->
			<!--<xsl:if test="str[@name='m.host.pages.list'] != ''">
                        <xsl:value-of select="str[@name='m.host.pages.list']"/>
                        <xsl:text>.</xsl:text></xsl:if>   -->
		</xsl:if>

		<xsl:if test="$citationstyle='apa'">
			
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			
			<!-- Editors -->
			<xsl:variable name="sEditors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.editors']))">
						<xsl:for-each select="arr[@name='m.name.edt']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.edt']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.editors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			
			<xsl:variable name="authorsformatted">
				<xsl:if test="$sAuthors != ''">
					<xsl:value-of select="$sAuthors"/>
					<xsl:if test="substring($sAuthors,string-length($sAuthors),1)!='.'">
						<xsl:text>.</xsl:text>
					</xsl:if>
					<xsl:text> </xsl:text>
				</xsl:if>
			</xsl:variable>
			
			<xsl:variable name="editorsformatted">
				<xsl:if test="$sEditors != ''">
					<xsl:value-of select="$sEditors"/>
					<xsl:if test="substring($sEditors,string-length($sEditors),1)!='.'">
						<xsl:text>.</xsl:text>
					</xsl:if>
					<xsl:text> </xsl:text>
				</xsl:if>
			</xsl:variable>
			
			<!--  The  title -->
			<xsl:variable name="titleformatted">
				<xsl:if test="local:stripPunct(str[@name='m.title'])!=''">
					<em>
						<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
						<xsl:text>.</xsl:text>
					</em>
					<xsl:text> </xsl:text>
				</xsl:if>
			</xsl:variable>
			
			<!-- Year -->
			<xsl:variable name="year">
				<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
				<xsl:choose>
					<xsl:when test="str[@name='m.genre.form']='In-press'">
						<xsl:text>(</xsl:text>
						<xsl:value-of select="str[@name='m.genre.form']"/>
						<xsl:text>). </xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="$xsd-date != ''">
							<xsl:text>(</xsl:text>
							<xsl:value-of select="substring($xsd-date,1,4)"/>
							<xsl:text>). </xsl:text>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.version']='Authored book'">
					<!-- Editors-->
					<xsl:value-of select="$authorsformatted"/>
					<xsl:value-of select="$year"/>
					<xsl:copy-of select="$titleformatted"/>
					<xsl:if test="$editorsformatted !=''">
						<xsl:text>Edited by </xsl:text>
						<xsl:value-of select="$editorsformatted"/>
						<xsl:text>, </xsl:text>
					</xsl:if>
				</xsl:when>
				<xsl:when test="str[@name='m.genre.version']='Edited book'">
					<xsl:value-of select="$authorsformatted"/>
					<xsl:if test="$editorsformatted !=''">
						<xsl:value-of select="$editorsformatted"/>
						<xsl:text>(Ed.) </xsl:text>
					</xsl:if>
					<xsl:value-of select="$year"/>
					<xsl:copy-of select="$titleformatted"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$authorsformatted"/>
					<xsl:value-of select="$year"/>
					<xsl:copy-of select="$titleformatted"/>
					<xsl:if test="$editorsformatted !=''">
						<xsl:text>Edited by </xsl:text>
						<xsl:value-of select="$editorsformatted"/>
						<xsl:text>, </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			
			<!-- Volume: This is present in HUM data-->
			<xsl:if test="str[@name='m.host.volume'] != ''">
				<xsl:text>(</xsl:text>
				<xsl:value-of select="local:stripPunct(str[@name='m.host.volume'])"/>
				<xsl:text>). </xsl:text>
			</xsl:if>
			
			<!-- Place-->
			<xsl:if test="str[@name='m.placeterm'] != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.placeterm'])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			
			<!-- Publisher -->
			<xsl:if test="str[@name='m.publisher'] != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.publisher'])"/>
				<xsl:text>.</xsl:text>
			</xsl:if>
			
		</xsl:if>
		
		<xsl:if test="$citationstyle='vancouver'">
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="mods:name">
							<!-- If the position is <> 1 then comma space                       -->
							<xsl:if test="position() !=1">
								<!-- This is different to other formats because there is no conjunction -->
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<!--<xsl:if test="position() !=1"><xsl:text> &amp; </xsl:text></xsl:if>-->
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:choose>
										<xsl:when test="exists(mods:namePart)">
											<xsl:if test="mods:namePart[@type='family']">
												<!-- is not null again -->
												<!--  If not the first family name                              -->
												<xsl:value-of select="mods:namePart[@type='family']"/>
												<xsl:text> </xsl:text>
											</xsl:if>
											<xsl:if test="mods:namePart[@type='given']">
												<!-- is not null again -->
												<xsl:for-each select="mods:namePart[@type='given']">
													<xsl:value-of select="substring(upper-case(.),1,1)"/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:when test="(@type='corporate')">
											<xsl:value-of select="mods:displayForm"/>
										</xsl:when>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(./mods:name)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="str[@name='m.note.authors']"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- vancouver
                        Surname JR, Surname B, Surname C. The Conference Paper Title. In: Surname D, ed. 
                        The Conference Name. Place: Publisher: Place 2007:65-69
                    -->
			<xsl:if test="$sAuthors !=''">
				<xsl:value-of select="$sAuthors"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The object title -->
			<xsl:if test="local:stripPunct(str[@name='m.title']) != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Volume info -->
			<xsl:if test="str[@name='m.host.volume'] != ''">
				<xsl:text/>
				<xsl:value-of select="local:stripPunct(str[@name='m.host.volume'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Place-->
			<xsl:if test="normalize-space(str[@name='m.placeterm'])">
				<xsl:value-of select="normalize-space(str[@name='m.placeterm'])"/>
				<xsl:text>: </xsl:text>
			</xsl:if>
			<!-- Publisher -->
			<xsl:if test="normalize-space(str[@name='m.publisher'])">
				<xsl:value-of select="normalize-space(str[@name='m.publisher'])"/>
				<xsl:text>:</xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>. </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>

		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
