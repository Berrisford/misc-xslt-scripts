<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:local="http://www.example.com/functions/local" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="local xs mods" xmlns:mods="http://www.loc.gov/mods/v3" version="2.0">
	<xsl:template name="mods2biblio1" match="doc[str[@name='r.isofcontenttype.pid']='uk-ac-man-con:1'  or str[@name='r.isofcontenttype.pid']='uk-ac-man-con:16' or str[@name='r.isofcontenttype.pid']='uk-ac-man-con:17' or str[@name='r.isofcontenttype.pid']='uk-ac-man-con:18']" mode="biblio">
		<xsl:param name="citationstyle"/>
		<!--  Journal Articles -->
		<xsl:if test="$citationstyle='harvard'">
			<!-- No Oxford comma -->
			<!-- <citation type="Harvard">SURNAME, J.R., SURNAME, B. & SURNAME, C. (2007) The journal article title. The Journal Title, 8, 65-69.</xsl:if> -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors != ''">
				<xsl:value-of select="upper-case(local:truncateAuthors(local:stripPunct($sAuthors),500))"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:text>(</xsl:text>
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>) </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:text>(</xsl:text>
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>) </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<!--  The journal article title -->
			<xsl:if test="str[@name='m.title'] != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- The Journal Title -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<em>
					<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				</em>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Volume -->
			<xsl:if test="str[@name='m.host.volume'] != ''">
				<xsl:value-of select="str[@name='m.host.volume']"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- start - end -->
			<xsl:choose>
				<xsl:when test="str[@name='m.host.pages.list'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.list'] != ''">
					<xsl:value-of select="str[@name='m.host.page.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != '' and str[@name='m.host.pages.end'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.pages.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != '' and str[@name='m.host.page.end'] != ''">
					<xsl:value-of select="str[@name='m.host.page.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.page.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="$citationstyle='chicago'">
			<!-- Oxford comma is correct-->
			<!-- <citation type="Chicago">Surname, John Rylands, Bob Surname, and Chris Surname. "The journal article title." The Journal Title Volume, no.? (2007): 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors != ''">
				<xsl:value-of select="local:truncateAuthors(local:stripPunct($sAuthors),500)"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The journal article title -->
			<xsl:if test="local:stripPunct(str[@name='m.title'])!=''">
				<xsl:text>"</xsl:text>
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:if test="substring(str[@name='m.title'],string-length(str[@name='m.title']),1) !='.' and substring(str[@name='m.title'],string-length(str[@name='m.title']),1) !='?'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text>" </xsl:text>
			</xsl:if>
			<!-- The Journal Title -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<em>
					<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				</em>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- Volume -->
			<xsl:if test="str[@name='m.host.volume'] != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.host.volume'])"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Issue -->
			<xsl:if test="str[@name='m.host.issue'] != ''">
				<xsl:text>no. </xsl:text>
				<xsl:value-of select="str[@name='m.host.issue']"/>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:text>(</xsl:text>
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>) </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:text>(</xsl:text>
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>) </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<!-- start - end -->
			<xsl:choose>
				<xsl:when test="str[@name='m.host.pages.list'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.list'] != ''">
					<xsl:value-of select="str[@name='m.host.page.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != '' and str[@name='m.host.pages.end'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.pages.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != '' and str[@name='m.host.page.end'] != ''">
					<xsl:value-of select="str[@name='m.host.page.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.page.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="$citationstyle='turabian'">
			<!-- Oxford comma is correct-->
			<!-- <citation type="Turabian">Surname, John Rylands, Surname, Bob and Surname, Chris. "The journal article title." <em>The Journal Title</em> Volume, no.? (year): 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:variable name="name" select="local:splitDisplayName(.)"/>
							<xsl:if test="exists($name/name/family)">
								<xsl:value-of select="$name/name/family"/>
							</xsl:if>
							<xsl:if test="exists($name/name/given)">
								<xsl:text>,</xsl:text>
								<xsl:for-each select="$name/name/given">
									<xsl:if test=". != ''">
										<xsl:text> </xsl:text>
									</xsl:if>
									<xsl:value-of select="."/>
								</xsl:for-each>
							</xsl:if>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors != ''">
				<xsl:value-of select="local:stripPunct($sAuthors)"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
			</xsl:if>
			<!--  The article title -->
			<xsl:if test="str[@name='m.title'] != ''">
				<xsl:text> "</xsl:text>
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:if test="substring(str[@name='m.title'],string-length(str[@name='m.title']),1) != '?'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text>"</xsl:text>
			</xsl:if>
			<!-- The Journal Title -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<xsl:text> </xsl:text>
				<em>
					<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				</em>
			</xsl:if>
			<!-- Volume -->
			<xsl:if test="str[@name='m.host.volume'] != ''">
				<xsl:text> </xsl:text>
				<xsl:value-of select="str[@name='m.host.volume']"/>
			</xsl:if>
			<!-- Issue -->
			<xsl:if test="str[@name='m.host.issue'] != ''">
				<xsl:if test="str[@name='m.host.volume'] != ''">
					<xsl:text>,</xsl:text>
				</xsl:if>
				<xsl:text> no. </xsl:text>
				<xsl:value-of select="str[@name='m.host.issue']"/>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:text> (</xsl:text>
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>) </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:text>(</xsl:text>
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>) </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<!-- start - end pages -->
			<xsl:variable name="pages">
				<xsl:choose>
					<xsl:when test="str[@name='m.host.pages.list'] != ''">
						<xsl:value-of select="str[@name='m.host.pages.list']"/>
					</xsl:when>
					<xsl:when test="str[@name='m.host.page.list'] != ''">
						<xsl:value-of select="str[@name='m.host.page.list']"/>
					</xsl:when>
					<xsl:when test="str[@name='m.host.pages.start'] != '' and str[@name='m.host.pages.end'] != ''">
						<xsl:value-of select="str[@name='m.host.pages.start']"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="str[@name='m.host.pages.end']"/>
					</xsl:when>
					<xsl:when test="str[@name='m.host.page.start'] != '' and str[@name='m.host.page.end'] != ''">
						<xsl:value-of select="str[@name='m.host.page.start']"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="str[@name='m.host.page.end']"/>
					</xsl:when>
					<xsl:when test="str[@name='m.host.pages.start'] != ''">
						<xsl:value-of select="str[@name='m.host.pages.start']"/>
					</xsl:when>
					<xsl:when test="str[@name='m.host.page.start'] != ''">
						<xsl:value-of select="str[@name='m.host.pages.start']"/>
					</xsl:when>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$pages != ''">
				<xsl:if test="str[@name='m.dateissued'] != ''">
					<xsl:text>: </xsl:text>
				</xsl:if>
				<xsl:value-of select="local:stripPunct($pages)"/>
			</xsl:if>
			<xsl:text>.</xsl:text>
		</xsl:if>
		<xsl:if test="$citationstyle='mla'">
			<!-- Oxford comma is correct-->
			<!-- <citation type="MLA">Surname, John Rylands, Bob Surname, and Chris Surname. "The Journal Article Title" The Journal Title Volume.Part (2007): 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() != 1">
									<xsl:text>and </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:choose>
										<xsl:when test="position() = 1">
											<xsl:if test="exists($name/name/family)">
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
											<xsl:if test="exists($name/name/given)">
												<xsl:text>,</xsl:text>
												<xsl:for-each select="$name/name/given">
													<xsl:if test=". != ''">
														<xsl:text> </xsl:text>
													</xsl:if>
													<xsl:value-of select="."/>
												</xsl:for-each>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$name/name/given">
												<xsl:if test="position() != 1">
													<xsl:text> </xsl:text>
												</xsl:if>
												<xsl:value-of select="."/>
											</xsl:for-each>
											<xsl:if test="exists($name/name/family)">
												<xsl:text> </xsl:text>
												<xsl:value-of select="$name/name/family"/>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors !=''">
				<xsl:value-of select="local:truncateAuthors(local:stripPunct($sAuthors),500)"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The journal article title -->
			<xsl:if test="local:stripPunct(str[@name='m.title']) !=''">
				<xsl:text>"</xsl:text>
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>." </xsl:text>
			</xsl:if>
			<!-- The Journal Title -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<span style="text-decoration: underline;">
					<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				</span>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<!-- Volume -->
			<xsl:if test="str[@name='m.host.volume'] != ''">
				<xsl:value-of select="str[@name='m.host.volume']"/>
			</xsl:if>
			<xsl:if test="str[@name='m.host.issue'] = ''">
				<xsl:text>.</xsl:text>
			</xsl:if>
			<!-- Issue -->
			<xsl:if test="str[@name='m.host.issue'] != ''">
				<!-- This is deliverate, the dot is to delimit vol and issue. If vol only then no dot! -->
				<xsl:text>.</xsl:text>
				<xsl:value-of select="str[@name='m.host.issue']"/>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:text> (</xsl:text>
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>): </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:text> (</xsl:text>
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>): </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<!-- start - end -->
			<xsl:choose>
				<xsl:when test="str[@name='m.host.pages.list'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.list'] != ''">
					<xsl:value-of select="str[@name='m.host.page.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != '' and str[@name='m.host.pages.end'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.pages.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != '' and str[@name='m.host.page.end'] != ''">
					<xsl:value-of select="str[@name='m.host.page.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.page.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="$citationstyle='apa'">
			<!-- <citation type="APA">Surname, J.R., Surname, B.B. & Surname, C. (2007) The journal article title. The Journal Title, Volume(Part), 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space                       -->
							<xsl:if test="position() !=1">
								<xsl:if test="position() != last()">
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:if>
							<!-- Squeeze in an 'and' before the last name -->
							<xsl:if test="position() = last()">
								<xsl:if test="position() !=1">
									<xsl:text> &amp; </xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<!-- Some formats have a limit to the authors -->
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:if test="exists($name/name/family)">
										<xsl:value-of select="$name/name/family"/>
									</xsl:if>
									<xsl:if test="exists($name/name/given)">
										<xsl:for-each select="$name/name/given">
											<xsl:if test="position() = 1">
												<xsl:text>, </xsl:text>
											</xsl:if>
											<xsl:if test=". != ''">
												<xsl:value-of select="substring(.,1,1)"/>
												<xsl:text>.</xsl:text>
											</xsl:if>
										</xsl:for-each>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() = count(../arr[@name='m.name.aut']/str)">
									<xsl:text> et al.</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors !=''">
				<xsl:value-of select="local:truncateAuthors(local:stripPunct($sAuthors),500)"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:text>(</xsl:text>
					<xsl:value-of select="str[@name='m.genre.form']"/>
					<xsl:text>). </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date != ''">
						<xsl:text>(</xsl:text>
						<xsl:value-of select="substring($xsd-date,1,4)"/>
						<xsl:text>). </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<!--  The journal article title -->
			<xsl:if test="str[@name='m.title'] != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:if test="substring(str[@name='m.title'],string-length(str[@name='m.title']),1) != '?'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!-- The Journal Title -->
			<xsl:choose>
				<xsl:when test="str[@name='m.host.title.abbreviated'] != ''">
					<em>
						<xsl:value-of select="local:stripPunct(str[@name='m.host.title.abbreviated'])"/>
					</em>
					<xsl:text>, </xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.title'] != ''">
					<em>
						<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
					</em>
					<xsl:text>, </xsl:text>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			<!-- Volume -->
			<xsl:if test="str[@name='m.host.volume'] != ''">
				<xsl:value-of select="str[@name='m.host.volume']"/>
				<xsl:if test="normalize-space(str[@name='m.host.issue']) = ''">
					<xsl:text>, </xsl:text>
				</xsl:if>
			</xsl:if>
			<!-- Issue -->
			<xsl:if test="normalize-space(str[@name='m.host.issue']) != ''">
				<xsl:text>(</xsl:text>
				<xsl:value-of select="str[@name='m.host.issue']"/>
				<xsl:text>), </xsl:text>
			</xsl:if>
			<!-- start - end -->
			<xsl:choose>
				<xsl:when test="str[@name='m.host.pages.list'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.list'] != ''">
					<xsl:value-of select="str[@name='m.host.page.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != '' and str[@name='m.host.pages.end'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.pages.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != '' and str[@name='m.host.page.end'] != ''">
					<xsl:value-of select="str[@name='m.host.page.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.page.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != ''">
					<xsl:value-of select="str[@name='m.host.page.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="$citationstyle='vancouver'">
			<!-- <citation type="Vancouver">Surname JR, Surname B, Surname C. The journal article title. The Journal Title. 2007 March; Volume(Part): 65-69.</xsl:if> -->
			<!-- Authors -->
			<xsl:variable name="sAuthors">
				<xsl:choose>
					<xsl:when test="not(exists(str[@name='m.note.authors']))">
						<xsl:for-each select="arr[@name='m.name.aut']/str">
							<!-- If the position is <> 1 then comma space -->
							<xsl:if test="position() != 1">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="position() &lt; 999">
									<xsl:variable name="name" select="local:splitDisplayName(.)"/>
									<xsl:if test="exists($name/name/family)">
										<xsl:value-of select="$name/name/family"/>
									</xsl:if>
									<xsl:if test="exists($name/name/given)">
										<xsl:text> </xsl:text>
										<xsl:for-each select="$name/name/given">
											<xsl:value-of select="substring(upper-case(.),1,1)"/>
										</xsl:for-each>
									</xsl:if>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<!-- Output authors string without any special formatting. This means losing the and / & before the last author -->
						<xsl:value-of select="local:stripPunct(str[@name='m.note.authors'])"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$sAuthors !=''">
				<xsl:value-of select="local:truncateAuthors(local:stripPunct($sAuthors),500)"/>
				<xsl:if test="substring($sAuthors,string-length($sAuthors),1) !='.'">
					<xsl:text>.</xsl:text>
				</xsl:if>
				<xsl:text> </xsl:text>
			</xsl:if>
			<!--  The journal article title -->
			<xsl:if test="local:stripPunct(str[@name='m.title']) != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- The Journal Title -->
			<xsl:if test="str[@name='m.host.title'] != ''">
				<xsl:value-of select="local:stripPunct(str[@name='m.host.title'])"/>
				<xsl:text>. </xsl:text>
			</xsl:if>
			<!-- Year -->
			<xsl:variable name="xsd-date" select="str[@name='m.dateissued']"/>
			<xsl:choose>
				<xsl:when test="str[@name='m.genre.form']='In-press'">
					<xsl:value-of select="str[@name='m.genre.form']"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$xsd-date!=''">
						<xsl:variable name="month" select="substring($xsd-date,6,2)"/>
						<xsl:if test="$xsd-date != ''">
							<xsl:value-of select="substring($xsd-date,1,4)"/>
							<xsl:text/>
						</xsl:if>
						<xsl:if test="$month !=''">
							<xsl:value-of select="local:getMonthName($month)"/>
						</xsl:if>
						<xsl:text>; </xsl:text>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<!-- Volume -->
			<xsl:if test="str[@name='m.host.volume'] != ''">
				<xsl:value-of select="str[@name='m.host.volume']"/>
				<xsl:if test="normalize-space(str[@name='m.host.issue']) = '' or not(exists(str[@name='m.host.issue']))">
					<xsl:text>: </xsl:text>
				</xsl:if>
			</xsl:if>
			<!-- Issue -->
			<xsl:if test="normalize-space(str[@name='m.host.issue']) != ''">
				<xsl:text>(</xsl:text>
				<xsl:value-of select="normalize-space(str[@name='m.host.issue'])"/>
				<xsl:text>): </xsl:text>
			</xsl:if>
			<!-- start - end -->
			<xsl:choose>
				<xsl:when test="str[@name='m.host.pages.list'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.list'] != ''">
					<xsl:value-of select="str[@name='m.host.page.list']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != '' and str[@name='m.host.pages.end'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.pages.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != '' and str[@name='m.host.page.end'] != ''">
					<xsl:value-of select="str[@name='m.host.page.start']"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="str[@name='m.host.page.end']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.pages.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='m.host.page.start'] != ''">
					<xsl:value-of select="str[@name='m.host.pages.start']"/>
					<xsl:text>.</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<!-- End of Journal Articles -->
	</xsl:template>
</xsl:stylesheet>
