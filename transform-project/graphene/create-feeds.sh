#!/bin/bash
# Creates incremental and count datafeeds for Manchester eScholar consumers.
# Usually this script will be called from cron with the parameter all,
# however it may be used to create individual feeds using a single parameter such as wolfson.
# This script depends upon symbolic links being present in the directory
# /export/home/fedora/scripts/datafeeds/xml

TODAY=`date +%F` # Today's date used for logging
LOGFILE=scripts/datafeeds/logs/`date +%F`.log # Logfile location and name
REFLOGFILE=scripts/datafeeds/logs/`date +%F`-ref.log # REF logfile location and name
FEED_ROOT=/opt/coolstack/apache2/htdocs/internal/api/trusted/getdatabasefeed # All feeds are place under here for consumption, included as a variable for brevity elsewhere in the script

# Function to backup older feeds
backup_feed() {
	cp $FEED_ROOT/$1/xml/index.xml scripts/datafeeds/xml/$1/xml/index_old.xml
}

# Function to execute the transformations that create the feeds
exec_java() {
/usr/java/bin/java -Xms1800m -Xmx1800m -jar bin/saxon/saxon9.jar -xsl:scripts/datafeeds/xsl/$1/$2 -s:scripts/datafeeds/xml/$1/xml/$3 -o:$FEED_ROOT/$1/xml/$4
}

# Function to write something meaningful to the log file
status() {
if [ $1 -eq 0 ]
	then
	echo `date '+%H:%M:%S'`" "$2" complete."
else
	echo `date '+%H:%M:%S'`" "$2" failed."
	exit 1
fi
}

# The main statement that this script is based upon
case $1 in 
	all)
		# Call the script itself with individual feed parameters
		./$0 mhs
		./$0 ls
		./$0 cs
		./$0 mbs
		./$0 imp
		./$0 wolfson
		./$0 rip
		./$0 ripheadings
		./$0 eprogoutputscount
		./$0 mweoutputscount
		;;
	mhs)
		backup_feed $1
		status $? "MHS backup" >> $LOGFILE
		exec_java $1 createMHSIncremental.xsl index_old.xml index.xml
		status $? "MHS incremental" >> $LOGFILE
		exec_java $1 countMHSObjects.xsl index.xml count.xml
		status $? "MHS count" >> $LOGFILE
		;;
	ls)
		backup_feed $1
		status $? "LS backup" >> $LOGFILE
		exec_java $1 createLSIncremental.xsl index_old.xml index.xml
		status $? "LS incremental" >> $LOGFILE
		exec_java $1 countLSObjects.xsl index.xml count.xml
		status $? "LS count" >> $LOGFILE
		;;
	cs)
		backup_feed $1
		status $? "CS backup" >> $LOGFILE
		exec_java $1 createCSIncremental.xsl index_old.xml index.xml
		status $? "CS incremental" >> $LOGFILE
		exec_java $1 countCSObjects.xsl index.xml count.xml
		status $? "CS count" >> $LOGFILE
		;;
	mbs)
		backup_feed $1
		status $? "MBS backup" >> $LOGFILE
		exec_java $1 createMBSIncremental.xsl index_old.xml index.xml
		status $? "MBS incremental" >> $LOGFILE
		exec_java $1 countMBSObjects.xsl index.xml count.xml
		status $? "MBS count" >> $LOGFILE
		;;
	imp)
		backup_feed $1
		status $? "IMP backup" >> $LOGFILE
		exec_java $1 createIMPIncremental.xsl index_old.xml index.xml
		status $? "IMP incremental" >> $LOGFILE
		exec_java $1 countIMPObjects.xsl index.xml count.xml
		status $? "IMP count" >> $LOGFILE
		;;
	wolfson)
		backup_feed $1
		status $? "Wolfson backup" >> $LOGFILE
		exec_java $1 createWolfsonIncremental.xsl index_old.xml index.xml
		status $? "Wolfson incremental" >> $LOGFILE
		exec_java $1 countWolfsonObjects.xsl index.xml count.xml
		status $? "Wolfson count" >> $LOGFILE
		;;
	rip)
		backup_feed $1
		status $? "RIP backup" >> $LOGFILE
		exec_java $1 createRIPFeed.xsl index_old.xml index.xml
		status $? "RIP" >> $LOGFILE
		exec_java $1 countRIPFeed.xsl index.xml count.xml
		status $? "RIP count" >> $LOGFILE
		;;
	ripheadings)
		echo `date '+%H:%M:%S'`" No backup specified for RIP headings..." >> $LOGFILE
		exec_java $1 createRIPHeadingsFeed.xsl index.xml index.xml # Note that RIP headings uses the previous output of RIP...
		status $? "RIP headings" >> $LOGFILE
		exec_java $1 countRIPHeadingsFeed.xsl index.xml count.xml
		status $? "RIP headings count" >> $LOGFILE
		;;
	eprogoutputscount)
		backup_feed $1
		status $? "eProgOuputsCount backup" >> $LOGFILE
		exec_java $1 createeProgOutputsCount.xsl index_old.xml index.xml
		status $? "eProgOuputsCount" >> $LOGFILE
		;;
	mweoutputscount)
		backup_feed $1
		status $? "MWEOuputsCount backup" >> $LOGFILE
		exec_java $1 createMWEOutputsCount.xsl index_old.xml index.xml
		status $? "MWEOuputsCount" >> $LOGFILE
		;;
	# Note that ref isn't included in 'all', it is called more frequently from cron
	refincremental)
		backup_feed $1
		status $? "REF backup" >> $REFLOGFILE
		exec_java $1 createREFIncremental.xsl index_old.xml index.xml
		status $? "REF" >> $REFLOGFILE
		exec_java $1 countREFObjects.xsl index.xml count.xml
		status $? "REF count" >> $REFLOGFILE
		;;
	*)
		echo $1 Unknown option. Try all, mhs, ls, cs, mbs, imp, wolfson, rip, ripheadings or refincremental >&2
		exit 1
	;;
	
esac

exit 0