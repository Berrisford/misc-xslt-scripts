<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:local="http://localhost" xmlns:saxon="http://saxon.sf.net/" exclude-result-prefixes="#all" version="2.0">
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:output name="serialise1" encoding="utf-8" method="xhtml" indent="no" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:preserve-space elements="text"/>

	<xsl:include href="../biblio/solr2biblio_v1-0.xsl"/>
<!--	<xsl:include href="biblio/solr2biblio_v1-0.xsl"/>-->
	<xsl:param name="personGroup">
		<objects>
			<object>
				<pid>uk-ac-man-per:abcde02</pid>
			</object>
		</objects>
	</xsl:param>
	<xsl:param name="citationstyle" select="'apa'"/>

	<xsl:variable name="baseuri" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/scw/select/'"/>
	<xsl:variable name="query" select="'x.lastmodifieddate:[NOW-3DAY TO NOW]'"/>

	<xsl:variable name="docsource" select="doc('../../xml/imp/xml/index_old.xml')"/>
<!--	<xsl:variable name="docsource" select="doc('source.xml')"/>-->
	<xsl:variable name="docrows" select="doc(concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=0&amp;indent=off'))/response/result[@name='response']/@numFound"/>

	<xsl:variable name="docupdates">
		<xsl:variable name="docmodified" select="doc(concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=',$docrows,'&amp;indent=off'))"/>
<!--		<xsl:variable name="docmodified" select="doc('update.xml')"/>-->
		<xsl:apply-templates select="$docmodified" mode="update"/>
	</xsl:variable>
	
	<xsl:template match="/">
		<objects>
			<xsl:attribute name="lastmodified" select="current-dateTime()"/>
			<xsl:apply-templates select="$docupdates/objects/object[output_state!='delete'] | $docsource/objects/object[not(output_pid = $docupdates/objects/object/output_pid)]"/>
		</objects>
	</xsl:template>
	
	<xsl:template match="object">
		<object>
			<xsl:apply-templates/>
		</object>
	</xsl:template>
	
	<xsl:template match="*">
		<xsl:copy-of select="."/>
	</xsl:template>
	
	<xsl:template match="/" mode="update">
		<xsl:apply-templates select="response/result[@name='response']" mode="#current"/>
	</xsl:template>
	
	<xsl:template match="result" mode="update">
		<objects>
			<xsl:apply-templates select="doc" mode="#current"/>
		</objects>
	</xsl:template>

	<xsl:template match="doc" mode="update">
		<xsl:variable name="outputpid" select="str[@name='PID']"/>
		<xsl:variable name="outputtype" select="if (str[@name='r.isofcontenttype.pid']='uk-ac-man-con:20') then 'Thesis (University required)' else tokenize(str[@name='r.isofcontenttype.source'],'\|\|')[1]"/>
		<xsl:variable name="outputstate" select="if (arr[@name='r.isbelongsto.pid']/str=$personGroup/objects/object/pid) then (if (str[@name='x.state']='Active' or str[@name='x.state']='A') then 'open' else 'closed') else 'delete'"/>
		<xsl:variable name="outputcreateddate" select="date[@name='x.createddate']"/>
		<xsl:variable name="outputyear" select="str[@name='m.year']"/>
		<xsl:variable name="outputdateissued" select="local:pad-date-to-length(str[@name='m.dateissued'])"/>
		<xsl:variable name="outputdoi" select="str[@name='m.identifier.doi']"/>
		<xsl:variable name="outputcitation">
			<xsl:apply-templates select="." mode="biblio">
				<xsl:with-param name="citationstyle" select="$citationstyle"/>
			</xsl:apply-templates>
			<xsl:if test="(str[@name='x.state']='Active' or str[@name='x.state']='A') and str[@name='r.isofcontenttype.pid']!='uk-ac-man-con:19'">
				<xsl:text> </xsl:text>
				<xsl:value-of select="' eScholarID:'"/>
				<xsl:element name="a">
					<xsl:attribute name="class" select="'escholarid'"/>
					<xsl:attribute name="href" select="concat('http://www.manchester.ac.uk/escholar/',$outputpid)"/>
					<xsl:value-of select="substring-after($outputpid,':')"/>
				</xsl:element>
				<xsl:if test="$outputdoi != ''">
					<xsl:text> | </xsl:text>
					<xsl:value-of select="'DOI:'"/>
					<xsl:element name="a">
						<xsl:attribute name="class" select="'doi'"/>
						<xsl:attribute name="href" select="concat('http://dx.doi.org/',$outputdoi)"/>
						<xsl:value-of select="str[@name='m.identifier.doi']"/>
					</xsl:element>
				</xsl:if>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="outputjournal" select="str[@name='m.host.title'][../str[@name='r.isofcontenttype.pid']='uk-ac-man-con:1']"/>
		<object>
			<output_pid>
				<xsl:value-of select="$outputpid"/>
			</output_pid>
			<output_type>
				<xsl:value-of select="$outputtype"/>
			</output_type>
			<output_state>
				<xsl:value-of select="$outputstate"/>
			</output_state>
			<output_createddate>
				<xsl:value-of select="$outputcreateddate"/>
			</output_createddate>
			<output_issueddate>
				<xsl:value-of select="$outputdateissued"/>
			</output_issueddate>
			<output_year>
				<xsl:value-of select="$outputyear"/>
			</output_year>
			<output_citation>
				<xsl:copy-of select="$outputcitation"/>
			</output_citation>
			<output_journal>
				<xsl:value-of select="$outputjournal"/>
			</output_journal>
		</object>
	</xsl:template>

	<xsl:function name="local:pad-date-to-length">
		<xsl:param name="sDateToPad" as="xs:anyAtomicType?"/>
		<xsl:variable name="year" select="if (tokenize($sDateToPad,'-')[1] !='') then local:pad-integer-to-length(tokenize($sDateToPad,'-')[1],4) else ''"/>
		<xsl:variable name="month" select="if (tokenize($sDateToPad,'-')[2] !='') then local:pad-integer-to-length(tokenize($sDateToPad,'-')[2],2) else ''"/>
		<xsl:variable name="day" select="if (tokenize($sDateToPad,'-')[3] !='') then local:pad-integer-to-length(tokenize($sDateToPad,'-')[3],2) else ''"/>
		<xsl:variable name="output">
			<xsl:if test="$year != ''">
				<xsl:value-of select="$year"/>
				<xsl:if test="$month != ''">
					<xsl:value-of select="concat('-',$month)"/>
					<xsl:if test="$day != ''">
						<xsl:value-of select="concat('-',$day)"/>
					</xsl:if>
				</xsl:if>
			</xsl:if>
		</xsl:variable>
		<xsl:value-of select="normalize-space($output)"/>
	</xsl:function>
	<xsl:function name="local:pad-integer-to-length" as="xs:string">
		<xsl:param name="integerToPad" as="xs:anyAtomicType?"/>
		<xsl:param name="length" as="xs:integer"/>
		<xsl:sequence select="if ($length &lt; string-length(string($integerToPad))) then $integerToPad else concat(local:repeat-string('0',$length - string-length(string($integerToPad))),string($integerToPad))"/>
	</xsl:function>
	<xsl:function name="local:repeat-string" as="xs:string">
		<xsl:param name="stringToRepeat" as="xs:string?"/>
		<xsl:param name="count" as="xs:integer"/>
		<xsl:sequence select="string-join((for $i in 1 to $count return $stringToRepeat),'')"/>
	</xsl:function>
</xsl:stylesheet>
