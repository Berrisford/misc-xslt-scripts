<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:datetime="http://exslt.org/dates-and-times" exclude-result-prefixes="#all" version="2.0">
	<xsl:output indent="yes" method="xml"/>
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="objects">
		<count>
			<xsl:attribute name="created">
				<xsl:value-of select="datetime:dateTime()"/>
			</xsl:attribute>
			<object>
				<xsl:value-of select="count(object)"/>
			</object>
		</count>
	</xsl:template>
</xsl:stylesheet>
