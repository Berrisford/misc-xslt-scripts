console.log(Math.random());

var canvas = document.createElement("canvas");
canvas.width = 100;
canvas.height = 100;
var ctx = canvas.getContext("2d");
ctx.fillStyle = "red";
ctx.fillRect(0, 0, 100, 100);

for(x=0; x < 100; x++) {
  for(y=0; y < 100; y++) {
    var color = Math.floor(Math.random() * 64 + 64);
    ctx.fillStyle = 'rgb(' + color + ',' + color + ',' + color + ')';
    ctx.fillRect(x,y,1,1);
  }
}

var img = document.createElement("img");
img.src = canvas.toDataURL("image/png");
document.body.appendChild(img);