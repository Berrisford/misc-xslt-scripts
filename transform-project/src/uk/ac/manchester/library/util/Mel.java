package uk.ac.manchester.library.util;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by tom on 01/04/2014.
 */
public class Mel {
    public void displayFromExcel (String xlsPath)
    {
        InputStream inputStream = null;
        try
        {
            inputStream = new FileInputStream (xlsPath);
        }
        catch (FileNotFoundException e)
        {
            System.out.println ("File not found in the specified path.");
            e.printStackTrace ();
        }

        POIFSFileSystem fileSystem = null;

        try {
            //Initializing the XML document
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.newDocument();
            Element rootElement = document.createElement("publications");
            document.appendChild(rootElement);


            fileSystem = new POIFSFileSystem (inputStream);
            HSSFWorkbook      workBook = new HSSFWorkbook (fileSystem);
            HSSFSheet         sheet    = workBook.getSheetAt (0);
            Iterator<?> rows     = sheet.rowIterator ();

            ArrayList<ArrayList<String>> data = new ArrayList<ArrayList<String>>();
            while (rows.hasNext ())
            {
                HSSFRow row = (HSSFRow) rows.next();

                int rowNumber = row.getRowNum ();
//                System.out.println ("Row No.: " + rowNumber);

                Iterator<?> cells = row.cellIterator ();

                int cellCount = row.getLastCellNum();

                ArrayList<String> rowData = new ArrayList<String>();
                for( int n = 0; n < cellCount; n++) {
                    HSSFCell cell = (HSSFCell) row.getCell(n);
                    if(cell == null) {
                        rowData.add("");
                    } else {
                        switch (cell.getCellType()) {
                            case HSSFCell.CELL_TYPE_NUMERIC: {
                                System.out.println("Numeric: " + cell.getNumericCellValue());
                                rowData.add((long) Math.floor(cell.getNumericCellValue()) + "");
                                break;
                            }
                            case HSSFCell.CELL_TYPE_STRING: {
                                HSSFRichTextString richTextString = cell.getRichStringCellValue();

                                System.out.println("String: " + richTextString.getString());
                                rowData.add(richTextString.getString());
                                break;
                            }

                            case HSSFCell.CELL_TYPE_BLANK: {
                                rowData.add("");
                                System.out.println("Empty: " + "[]");
                                break;
                            }
                            default: {
                                // types other than String and Numeric.
                                System.out.println("Type not supported.");
                                break;
                            }
                        }
                    }

                }

                if(!"".equals(rowData.get(0))) {
                    data.add(rowData);
                }


            }

            int numOfProduct = data.size();

            for (int i = 1; i < numOfProduct; i++){
                Element productElement = document.createElement("publication");
                rootElement.appendChild(productElement);

                int index = 0;
                for(String s: data.get(i)) {
                    String headerString = data.get(0).get(index);

                    headerString = headerString.replace("(", "");
                    headerString = headerString.replace(")", "");
                    headerString = WordUtils.capitalizeFully(headerString);
                    headerString = headerString.replace(" ", "");

                    if(!"".equals(headerString)) {
                        Element headerElement = document.createElement(headerString);
                        productElement.appendChild(headerElement);
                        headerElement.appendChild(document.createTextNode(s));
                    }
                    index++;
                }
            }

            TransformerFactory tFactory = TransformerFactory.newInstance();

            Transformer transformer = tFactory.newTransformer();
            //Add indentation to output
            transformer.setOutputProperty
                    (OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(
                    "{http://xml.apache.org/xslt}indent-amount", "2");

            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File("melilah/outputm2.xml"));
            //StreamResult result = new StreamResult(System.out);
            transformer.transform(source, result);

        }
        catch(IOException e)
        {
            System.out.println("IOException " + e.getMessage());
        } catch (ParserConfigurationException e) {
            System.out.println("ParserConfigurationException " + e.getMessage());
        } catch (TransformerConfigurationException e) {
            System.out.println("TransformerConfigurationException "+ e.getMessage());
        } catch (TransformerException e) {
            System.out.println("TransformerException " + e.getMessage());
        }
    }


    public static void main (String[] args)
    {
        Mel poiExample = new Mel();
        String xlsPath ="melilah/m2.xls";
        poiExample.displayFromExcel (xlsPath);
    }
}
