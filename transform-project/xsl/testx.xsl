<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="publication">
        <mods:mods version="3.3" xmlns:mods="http://www.loc.gov/mods/v3"
                   xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd">
            <mods:genre type="content-type" authority="local">newspaper-magazine-contribution</mods:genre>
            <mods:genre type="version">Newspaper article</mods:genre>
            <mods:language>
                <mods:languageTerm type="code" authority="iso639-3"><!--code for English --></mods:languageTerm>
            </mods:language>
            <mods:originInfo>
                <mods:dateIssued encoding="iso8601"><xsl:call-template name="showDate"/></mods:dateIssued>
                <mods:place>
                    <mods:placeTerm><xsl:value-of select="PlaceOfPublication"/></mods:placeTerm>
                </mods:place>
                <mods:publisher><xsl:value-of select="Publisher"/></mods:publisher>
            </mods:originInfo>
            <mods:recordInfo>
                <mods:recordCreationDate encoding="iso8601"><!-- Datetime when record first created YYYY-MM-DDThh:mm:ssTZD e.g. 2009-03-25T12:05:31Z --></mods:recordCreationDate>
                <mods:recordChangeDate encoding="iso8601"><!-- Datetime when record first created YYYY-MM-DDThh:mm:ssTZD e.g. 2009-03-25T12:05:31Z --></mods:recordChangeDate>
                <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
                <mods:recordIdentifier source="Manchester eScholar"><!-- PID --></mods:recordIdentifier>
            </mods:recordInfo>
            <!-- newspaper/magazine -->
            <mods:relatedItem type="host">
                <mods:identifier type="issn"/>
                <!-- newspaper/magazine title -->
                <mods:titleInfo>
                    <mods:title><xsl:value-of select="Title"/> <xsl:value-of select="IssueNumberNewSeries"/><!-- Name of the newspaper/ Dominic--></mods:title>
                </mods:titleInfo>
                <mods:part>
                    <mods:detail type="issue">
                        <mods:number><!--Issue --></mods:number>
                    </mods:detail>
                    <!-- pagination -->
                    <mods:extent unit="page">
                        <mods:start><xsl:value-of select="FirstPage"/></mods:start>
                        <mods:end><xsl:value-of select="LastPage"/></mods:end>
                        <mods:total><xsl:value-of select="LastPage - FirstPage"/></mods:total>
                        <mods:list><xsl:value-of select="FirstPage"/>-<xsl:value-of select="lastPage"/></mods:list>
                    </mods:extent>
                </mods:part>
            </mods:relatedItem>
            <mods:titleInfo>
                <mods:title><xsl:value-of select="Title"/></mods:title>
            </mods:titleInfo>
            <mods:typeOfResource>mixed material</mods:typeOfResource>
        </mods:mods>
    </xsl:template>


    <xsl:template name="createPid">
        <xsl:param name="IssueNumberNewSeries"/>
        <xsl:value-of select="IssueNumberNewSeries"/>
    </xsl:template>

    <xsl:template name="showDate">
        <xsl:value-of select="Year"/><xsl:text>-</xsl:text>
        <xsl:call-template name="monthToInt">
            <xsl:with-param name="monthName" select="Month"/>
        </xsl:call-template>
        <xsl:text>-</xsl:text><xsl:value-of
            select="Day"/>
    </xsl:template>

    <xsl:template name="monthToInt">
        <xsl:param name="monthName"/>
        <xsl:variable name="month">
            <xsl:choose>
                <xsl:when test="$monthName = 'January'">1</xsl:when>
                <xsl:when test="$monthName = 'February'">3</xsl:when>
                <xsl:when test="$monthName = 'March'">3</xsl:when>
                <xsl:when test="$monthName = 'April'">4</xsl:when>
                <xsl:when test="$monthName = 'May'">5</xsl:when>
                <xsl:when test="$monthName = 'June'">6</xsl:when>
                <xsl:when test="$monthName = 'July'">7</xsl:when>
                <xsl:when test="$monthName = 'August'">8</xsl:when>
                <xsl:when test="$monthName = 'September'">9</xsl:when>
                <xsl:when test="$monthName = 'October'">10</xsl:when>
                <xsl:when test="$monthName = 'Novemeber'">11</xsl:when>
                <xsl:when test="$monthName = 'December'">12</xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="$month"/>
    </xsl:template>
</xsl:stylesheet>