<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" name="xml"/>

    <xsl:template match="/">
        <xsl:for-each select="publications/publication">
            <xsl:variable name="pid_count">
                <xsl:number />
            </xsl:variable>
            <xsl:variable name="pid_name">
                <!-- BA: Edit here... -->
                <xsl:value-of select="3701 + $pid_count" />
            </xsl:variable>
            <xsl:variable name="file">
                <!-- BA: Edit here... -->
                <xsl:value-of select="concat('outputArani/', '15m', $pid_name, '.xml')" />
            </xsl:variable>
            <xsl:result-document href="{$file}" method="xml" indent="yes">
                <xsl:call-template name="publication_foxml" />
            </xsl:result-document>            
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="publication_foxml">
        <xsl:variable name="sequence"><xsl:number/></xsl:variable>
        <foxml:digitalObject VERSION="1.1" xmlns:foxml="info:fedora/fedora-system:def/foxml#">
            <xsl:attribute name="PID"><xsl:call-template name="createPid"><xsl:with-param name="sequence" select="$sequence"/></xsl:call-template></xsl:attribute>
            <foxml:objectProperties>
                <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
            </foxml:objectProperties>
            <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
                <!-- BA: Edit here... (global replace date) -->
                <foxml:datastreamVersion CREATED="2015-01-07T00:00:00Z" ID="MODS.0"
                                         LABEL="Metadata Object Description Schema Record" MIMETYPE="text/xml" SIZE="3787">
                    <foxml:xmlContent>
                        <mods:mods version="3.3" xmlns:mods="http://www.loc.gov/mods/v3"
                                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                                   xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd">

                            <mods:genre type="content-type" authority="local">newspaper-magazine-contribution</mods:genre>
                            <mods:genre type="version">Newspaper article</mods:genre>
                            <mods:language>
                                <mods:languageTerm type="code" authority="iso639-3">ira</mods:languageTerm>
                            </mods:language>
                            <mods:originInfo>
                                <mods:dateIssued encoding="iso8601"><xsl:call-template name="showDate"/></mods:dateIssued>
                                <mods:place>
                                    <mods:placeTerm><xsl:value-of select="PlaceofPublicationPersian"/></mods:placeTerm>
                                </mods:place>
                                <!-- BA: Edit here... (may or may not have to swap these around depending on xml) -->
                                <mods:publisher><xsl:value-of select="PublisherPersian"/></mods:publisher>
                                <!--<mods:publisher></mods:publisher>-->
                            </mods:originInfo>
                            <mods:recordInfo>
                                <mods:recordCreationDate encoding="iso8601">2015-01-07T00:00:00Z</mods:recordCreationDate>
                                <mods:recordChangeDate encoding="iso8601">2015-01-07T00:00:00Z</mods:recordChangeDate>
                                <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
                                <mods:recordIdentifier source="Manchester eScholar"><xsl:call-template name="createPid"><xsl:with-param name="sequence" select="$sequence"/></xsl:call-template></mods:recordIdentifier>
                            </mods:recordInfo>
                            <mods:relatedItem type="host">
                                <mods:identifier type="issn"/>
                                <mods:titleInfo>
                                    <mods:title><xsl:value-of select="TitleTransliterated"/></mods:title>
                                </mods:titleInfo>
                                <mods:part>
                                    <mods:detail type="issue">
                                        <mods:number><xsl:value-of select="IssueNumber"/></mods:number>
                                    </mods:detail>
                                    <!-- pagination -->
                                    <mods:extent unit="page">
                                        <mods:total><xsl:value-of select="NumberOfPages"/></mods:total>
                                    </mods:extent>
                                </mods:part>
                            </mods:relatedItem>
                            <mods:titleInfo>
                                <mods:title><xsl:value-of select="TitlePersian"/>&#8206;<xsl:text> </xsl:text><xsl:call-template name="showDate"/></mods:title>
                            </mods:titleInfo>
                            <mods:typeOfResource>mixed material</mods:typeOfResource>
                        </mods:mods>
                    </foxml:xmlContent>
                </foxml:datastreamVersion>

            </foxml:datastream>
            <foxml:datastream CONTROL_GROUP="M" ID="FULL-TEXT.PDF" STATE="A" VERSIONABLE="false">
                <foxml:datastreamVersion ID="FULL-TEXT.PDF.0" LABEL="Attachment" MIMETYPE="application/pdf">

                    <foxml:contentLocation TYPE="URL">
                        <xsl:attribute name="REF"><xsl:call-template name="createUrl"/></xsl:attribute>
                    </foxml:contentLocation>
                </foxml:datastreamVersion>
            </foxml:datastream>
            <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
                <foxml:datastreamVersion CREATED="2015-01-07T00:00:00Z" ID="RELS-EXT.1" LABEL="Relationships"
                                         MIMETYPE="text/xml" SIZE="920">
                    <foxml:xmlContent>
                        <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                            <rdf:Description>
                                <xsl:attribute name="rdf:about">info:fedora/<xsl:call-template name="createPid"><xsl:with-param name="sequence" select="$sequence"/></xsl:call-template></xsl:attribute>
                                <!-- BA: Edit here... -->
                                <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde26"/>
                                <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:6"/>
                                <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:22"/>
                                <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                                <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                                <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                                <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:18"/>
                            </rdf:Description>
                        </rdf:RDF>
                    </foxml:xmlContent>
                </foxml:datastreamVersion>
            </foxml:datastream>
        </foxml:digitalObject>
    </xsl:template>


    <xsl:template name="createPid">
        <xsl:param name="sequence"/>
        <!-- BA: Edit here... -->
        <xsl:text>uk-ac-man-scw:15m</xsl:text><xsl:value-of select="$sequence + 3701"/>
    </xsl:template>

    <xsl:template name="showDate">
        <xsl:value-of select="Year"/><xsl:text>-&#8206;</xsl:text>
        <xsl:value-of select="Month"/>
        <xsl:text>-&#8206;</xsl:text><xsl:value-of select="Day"/>
    </xsl:template>

    <xsl:template name="createUrl">
        <xsl:variable name="name" select="replace(tokenize(FileLocationOfCompletedImage,'\\')[last()], ' ', '_')"/>
        <!-- BA: Edit here... -->
        <xsl:text>http://fedprdir.library.manchester.ac.uk/uploaded/Arani/</xsl:text>
        <!--<xsl:value-of select="concat($name, '.pdf')"/>-->
        <xsl:value-of select="$name"/>
    </xsl:template>
</xsl:stylesheet>