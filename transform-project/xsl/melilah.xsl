<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" name="xml"/>

    <xsl:template match="publication">
        <xsl:variable name="sequence"><xsl:number/></xsl:variable>
        <xsl:variable name="filename">
            <xsl:text>melilah/1m</xsl:text>
            <xsl:value-of select="$sequence + 4999"/>
            <xsl:text>.xml</xsl:text>
        </xsl:variable>
        <xsl:result-document href="{$filename}" format="xml">
        <foxml:digitalObject VERSION="1.1" xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                             >
            <xsl:attribute name="PID"><xsl:call-template name="createPid"><xsl:with-param name="sequenceNumber" select="$sequence"/></xsl:call-template></xsl:attribute>
            <foxml:objectProperties>
                <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
            </foxml:objectProperties>
            <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
                <foxml:datastreamVersion CREATED="2014-04-07T12:05:31Z" ID="MODS.0"
                                         LABEL="Metadata Object Description Schema Record" MIMETYPE="text/xml" SIZE="3787">
                    <foxml:xmlContent>
                        <mods:mods version="3.3" xmlns:mods="http://www.loc.gov/mods/v3"
                                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                                   xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd">

                            <mods:genre type="content-type" authority="local">journal-article</mods:genre>
                            <mods:genre type="version">Original work</mods:genre>
                            <mods:genre type="form">Academic journal article</mods:genre>
                            <mods:language>
                                <mods:languageTerm type="code" authority="iso639-3">
                                    <xsl:choose>
                                        <xsl:when test="Language = 'English'">eng</xsl:when>
                                        <xsl:when test="Language = 'Hebrew'">heb</xsl:when>
                                    </xsl:choose>
                                </mods:languageTerm>
                            </mods:language>
                            <!-- author(s) -->
                            <mods:name type="personal">
                                <mods:namePart type="given"><xsl:value-of select="Author1_Firstname"/></mods:namePart>
                                <mods:namePart type="family"><xsl:value-of select="Author1_Surname"/></mods:namePart>
                                <mods:displayForm><xsl:value-of select="Author1_Firstname"/><xsl:text> </xsl:text><xsl:value-of select="Author1_Surname"/></mods:displayForm>
                                <mods:role>
                                    <mods:roleTerm type="code" authority="marcrelator">aut</mods:roleTerm>
                                    <mods:roleTerm type="text" authority="marcrelator">author</mods:roleTerm>
                                </mods:role>
                            </mods:name>
                            <xsl:if test="Author2Firstname != ''">
                                <mods:name type="personal">
                                    <mods:namePart type="given"><xsl:value-of select="Author2_Firstname"/></mods:namePart>
                                    <mods:namePart type="family"><xsl:value-of select="Author2_Surname"/></mods:namePart>
                                    <mods:displayForm><xsl:value-of select="Author2_Firstname"/><xsl:text> </xsl:text><xsl:value-of select="Author2_Surname"/></mods:displayForm>
                                    <mods:role>
                                        <mods:roleTerm type="code" authority="marcrelator">aut</mods:roleTerm>
                                        <mods:roleTerm type="text" authority="marcrelator">author</mods:roleTerm>
                                    </mods:role>
                                </mods:name>
                            </xsl:if>
                            <mods:name type="personal">
                                <mods:namePart type="given">Edward</mods:namePart>
                                <mods:namePart type="family">Robertson</mods:namePart>
                                <mods:displayForm>Edward Robertson</mods:displayForm>
                                <mods:role>
                                    <mods:roleTerm type="code" authority="marcrelator">edt</mods:roleTerm>
                                    <mods:roleTerm type="text" authority="marcrelator">editor</mods:roleTerm>
                                </mods:role>
                            </mods:name>
                            <mods:name type="personal">
                                <mods:namePart type="given">Meir</mods:namePart>
                                <mods:namePart type="family">Wallenstein</mods:namePart>
                                <mods:displayForm>Meir Wallenstein</mods:displayForm>
                                <mods:role>
                                    <mods:roleTerm type="code" authority="marcrelator">edt</mods:roleTerm>
                                    <mods:roleTerm type="text" authority="marcrelator">editor</mods:roleTerm>
                                </mods:role>
                            </mods:name>
                            <mods:originInfo>
                                <mods:dateIssued encoding="iso8601"><xsl:value-of select="DateofPublication"/></mods:dateIssued>
                                <mods:place>
                                    <mods:placeTerm>Manchester, UK</mods:placeTerm>
                                </mods:place>
                                <mods:publisher><xsl:value-of select="Publisher"/></mods:publisher>
                            </mods:originInfo>
                            <mods:recordInfo>
                                <mods:recordCreationDate encoding="iso8601">2014-04-15T12:05:31Z</mods:recordCreationDate>
                                <mods:recordChangeDate encoding="iso8601">2014-04-15T12:05:31Z</mods:recordChangeDate>
                                <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
                                <mods:recordIdentifier source="Manchester eScholar"><xsl:call-template name="createPid"><xsl:with-param name="sequenceNumber" select="$sequence"/></xsl:call-template></mods:recordIdentifier>
                            </mods:recordInfo>
                            <mods:relatedItem type="host">
                                <mods:titleInfo>
                                    <mods:title><xsl:value-of select="JournalTitle"/></mods:title>
                                </mods:titleInfo>
                                <mods:part>
                                    <mods:detail type="volume">
                                        <mods:number><xsl:value-of select="Volumenumber"/></mods:number>
                                    </mods:detail>
                                    <!-- pagination -->
                                    <mods:extent unit="page">
                                        <xsl:variable name="firstPage" select="tokenize(PageNumbers,'-')[1]"/>
                                        <xsl:variable name="lastPage" select="tokenize(PageNumbers,'-')[last()]"/>
                                        <mods:start><xsl:value-of select="$firstPage"/></mods:start>
                                        <mods:end><xsl:value-of select="$lastPage"/></mods:end>
                                        <xsl:if test="string(number($firstPage)) != 'NaN'">
                                            <mods:total><xsl:value-of select="number($lastPage) - number($firstPage) + 1"/></mods:total>
                                        </xsl:if>
                                        <mods:list><xsl:value-of select="PageNumbers"/></mods:list>
                                    </mods:extent>
                                </mods:part>
                            </mods:relatedItem>
                            <mods:titleInfo>
                                <mods:title><xsl:value-of select="TranslatedArticleTitle"/></mods:title>
                            </mods:titleInfo>
                            <mods:typeOfResource>text</mods:typeOfResource>
                        </mods:mods>
                    </foxml:xmlContent>
                </foxml:datastreamVersion>

            </foxml:datastream>
            <foxml:datastream CONTROL_GROUP="M" ID="FULL-TEXT.PDF" STATE="A" VERSIONABLE="false">
                <foxml:datastreamVersion ID="FULL-TEXT.PDF.0" LABEL="Attachment" MIMETYPE="application/pdf">

                    <foxml:contentLocation TYPE="URL">
                        <xsl:attribute name="REF"><xsl:call-template name="createUrl"/></xsl:attribute>
                    </foxml:contentLocation>
                </foxml:datastreamVersion>
            </foxml:datastream>
            <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
                <foxml:datastreamVersion CREATED="2014-04-07T12:05:31Z" ID="RELS-EXT.1" LABEL="Relationships"
                                         MIMETYPE="text/xml" SIZE="920">
                    <foxml:xmlContent>
                        <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                            <rdf:Description>
                                <xsl:attribute name="rdf:about">info:fedora/<xsl:call-template name="createPid"><xsl:with-param name="sequenceNumber" select="$sequence"/></xsl:call-template></xsl:attribute>
                                <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde09"/>
                                <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:6"/>
                                <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:22"/>
                                <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                                <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                                <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                                <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:1"/>
                            </rdf:Description>
                        </rdf:RDF>
                    </foxml:xmlContent>
                </foxml:datastreamVersion>
            </foxml:datastream>
        </foxml:digitalObject>
        </xsl:result-document>
    </xsl:template>


    <xsl:template name="createPid">
        <xsl:param name="sequenceNumber"/>
        <xsl:text>uk-ac-man-scw:1m</xsl:text><xsl:value-of select="$sequenceNumber + 4999"/>
    </xsl:template>



    <!--CHNAGE THE REF
        Replace 'V:\DSU\Dom\Methodist Newspapers\United Methodist\MULTI PAGE PDF VERSION \' with 'http://localhost/uploaded/unitedmethodist/'
        and then concatenate the rest after changing the slashes e.g. '1919\10_October\16.pdf' to '1919/10_October/16.pdf'

        e.g.
        Location in the spreadsheet V:\DSU\Dom\Methodist Newspapers\United Methodist\MULTI PAGE PDF VERSION\1919\10_October\16.pdf
        REF in the foxml http://localhost/uploaded/unitedmethodist/1919/10_October/16.pdf
    -->
    <xsl:template name="createUrl">
        <xsl:variable name="name" select="tokenize(File_Directory,'\\')[last()]"/>
        <xsl:text>http://fedprdir.library.manchester.ac.uk/uploaded/Melilah/</xsl:text>
        <xsl:value-of select="$name"/>
    </xsl:template>
</xsl:stylesheet>