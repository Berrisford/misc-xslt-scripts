<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" name="xml"/>

    <xsl:template match="publication">
        <xsl:variable name="filename">
            <xsl:text>output/</xsl:text>
            <xsl:value-of select="IssueNumberNewSeries"/>
            <xsl:text>.xml</xsl:text>
        </xsl:variable>
        <xsl:result-document href="{$filename}" format="xml">
        <foxml:digitalObject VERSION="1.1" xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="info:fedora/fedora-system:def/foxml# http://www.fedora.info/definitions/1/0/foxml1-1.xsd">
            <xsl:attribute name="PID"><xsl:call-template name="createPid"><xsl:with-param name="IssueNumberNewSeries" select="IssueNumberNewSeries"/></xsl:call-template></xsl:attribute>
            <foxml:objectProperties>
                <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
            </foxml:objectProperties>
            <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
                <foxml:datastreamVersion CREATED="2014-04-03T12:05:31Z" ID="MODS.0"
                                         LABEL="Metadata Object Description Schema Record" MIMETYPE="text/xml" SIZE="3787">
                    <foxml:xmlContent>
                        <mods:mods version="3.3" xmlns:mods="http://www.loc.gov/mods/v3"
                                   xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                                   xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd">

                            <mods:genre type="content-type" authority="local">newspaper-magazine-contribution</mods:genre>
                            <mods:genre type="version">Newspaper article</mods:genre>
                            <mods:language>
                                <mods:languageTerm type="code" authority="iso639-3">eng</mods:languageTerm>
                            </mods:language>
                            <mods:originInfo>
                                <mods:dateIssued encoding="iso8601"><xsl:call-template name="showDate"/></mods:dateIssued>
                                <mods:place>
                                    <mods:placeTerm><xsl:value-of select="PlaceOfPublication"/></mods:placeTerm>
                                </mods:place>
                                <mods:publisher><xsl:value-of select="Publisher"/></mods:publisher>
                            </mods:originInfo>
                            <mods:recordInfo>
                                <mods:recordCreationDate encoding="iso8601">2014-04-03T12:05:31Z</mods:recordCreationDate>
                                <mods:recordChangeDate encoding="iso8601">2014-04-03T12:05:31Z</mods:recordChangeDate>
                                <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
                                <mods:recordIdentifier source="Manchester eScholar"><xsl:call-template name="createPid"><xsl:with-param name="IssueNumberNewSeries" select="IssueNumberNewSeries"/></xsl:call-template></mods:recordIdentifier>
                            </mods:recordInfo>
                            <mods:relatedItem type="host">
                                <mods:identifier type="issn"/>
                                <mods:titleInfo>
                                    <mods:title><xsl:value-of select="Title"/></mods:title>
                                </mods:titleInfo>
                                <mods:part>
                                    <mods:detail type="issue">
                                        <mods:number>IssueNumberNewSeries</mods:number>
                                    </mods:detail>
                                    <!-- pagination -->
                                    <mods:extent unit="page">
                                        <mods:start><xsl:value-of select="FirstPage"/></mods:start>
                                        <mods:end><xsl:value-of select="LastPage"/></mods:end>
                                        <mods:total><xsl:value-of select="LastPage - FirstPage"/></mods:total>
                                        <mods:list><xsl:value-of select="FirstPage"/>-<xsl:value-of select="LastPage"/></mods:list>
                                    </mods:extent>
                                </mods:part>
                            </mods:relatedItem>
                            <mods:titleInfo>
                                <mods:title><xsl:value-of select="Title"/><xsl:text> </xsl:text><xsl:call-template name="showDate"/></mods:title>
                            </mods:titleInfo>
                            <mods:typeOfResource>mixed material</mods:typeOfResource>
                        </mods:mods>
                    </foxml:xmlContent>
                </foxml:datastreamVersion>

            </foxml:datastream>
            <foxml:datastream CONTROL_GROUP="M" ID="FULL-TEXT.PDF" STATE="A" VERSIONABLE="false">
                <foxml:datastreamVersion ID="FULL-TEXT.PDF.0" LABEL="Attachment" MIMETYPE="application/pdf">

                    <foxml:contentLocation TYPE="URL">
                        <xsl:attribute name="REF"><xsl:call-template name="createUrl"/></xsl:attribute>
                    </foxml:contentLocation>
                </foxml:datastreamVersion>
            </foxml:datastream>
            <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
                <foxml:datastreamVersion CREATED="2014-04-03T12:05:31Z" ID="RELS-EXT.1" LABEL="Relationships"
                                         MIMETYPE="text/xml" SIZE="920">
                    <foxml:xmlContent>
                        <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                            <rdf:Description>
                                <xsl:attribute name="rdf:about"><xsl:call-template name="createPid"><xsl:with-param name="IssueNumberNewSeries" select="IssueNumberNewSeries"/></xsl:call-template></xsl:attribute>
                                <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde07"/>
                                <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:6"/>
                                <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:22"/>
                                <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                                <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                                <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                                <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:18"/>
                            </rdf:Description>
                        </rdf:RDF>
                    </foxml:xmlContent>
                </foxml:datastreamVersion>
            </foxml:datastream>
        </foxml:digitalObject>
        </xsl:result-document>
    </xsl:template>


    <xsl:template name="createPid">
        <xsl:param name="IssueNumberNewSeries"/>
        <xsl:text>uk-ac-man-scw:15m</xsl:text><xsl:value-of select="IssueNumberNewSeries"/>
    </xsl:template>

    <xsl:template name="showDate">
        <xsl:value-of select="Year"/><xsl:text>-</xsl:text>
        <xsl:call-template name="monthToInt">
            <xsl:with-param name="monthName" select="Month"/>
        </xsl:call-template>
        <xsl:text>-</xsl:text><xsl:value-of
            select="format-number(Day, '00')"/>
    </xsl:template>

    <xsl:template name="monthToInt">
        <xsl:param name="monthName"/>
        <xsl:variable name="month">
            <xsl:choose>
                <xsl:when test="$monthName = 'January'">01</xsl:when>
                <xsl:when test="$monthName = 'February'">03</xsl:when>
                <xsl:when test="$monthName = 'March'">03</xsl:when>
                <xsl:when test="$monthName = 'April'">04</xsl:when>
                <xsl:when test="$monthName = 'May'">05</xsl:when>
                <xsl:when test="$monthName = 'June'">06</xsl:when>
                <xsl:when test="$monthName = 'July'">07</xsl:when>
                <xsl:when test="$monthName = 'August'">08</xsl:when>
                <xsl:when test="$monthName = 'September'">09</xsl:when>
                <xsl:when test="$monthName = 'October'">10</xsl:when>
                <xsl:when test="$monthName = 'Novemeber'">11</xsl:when>
                <xsl:when test="$monthName = 'December'">12</xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="$month"/>
    </xsl:template>



    <!--CHNAGE THE REF
        Replace 'V:\DSU\Dom\Methodist Newspapers\United Methodist\MULTI PAGE PDF VERSION \' with 'http://localhost/uploaded/unitedmethodist/'
        and then concatenate the rest after changing the slashes e.g. '1919\10_October\16.pdf' to '1919/10_October/16.pdf'

        e.g.
        Location in the spreadsheet V:\DSU\Dom\Methodist Newspapers\United Methodist\MULTI PAGE PDF VERSION\1919\10_October\16.pdf
        REF in the foxml http://localhost/uploaded/unitedmethodist/1919/10_October/16.pdf
    -->
    <xsl:template name="createUrl">
        <xsl:variable name="name" select="tokenize(Location,'\\')[last()]"/>
        <xsl:text>http://feduatir.library.manchester.ac.uk/uploaded/unitedmethodist/</xsl:text>
        <xsl:value-of select="Year"/>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="format-number(Day, '00')"/>
        <xsl:text>_</xsl:text>
        <xsl:value-of select="Month"/>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$name"/>
    </xsl:template>
</xsl:stylesheet>