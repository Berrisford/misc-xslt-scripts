<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:variable name="filename" select="'result-sample.json'"/>
    <xsl:variable name="file">
        <xsl:copy-of select="doc($filename)"/>
    </xsl:variable>

    <xsl:template match="/">
        <xsl:apply-templates select="json-to-xml('{top:[]}')"/>
    </xsl:template>

</xsl:stylesheet>