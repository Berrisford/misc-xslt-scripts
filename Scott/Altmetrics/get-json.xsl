<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="text" indent="yes"/>
    
    <xsl:template match="/">
        <xsl:for-each select="doi">
            
            <xsl:variable as="xs:string" name="filenumber">
                <xsl:number/>                
            </xsl:variable>
            
            <xsl:variable name="filename">
                <xsl:value-of select="concat('output/file-', $filenumber)"/>
            </xsl:variable>
            
            <xsl:variable name="result">
                <xsl:copy-of select="doc(.)"/>
            </xsl:variable>
            
            <xsl:result-document href="{$filename}">
                <xsl:copy-of select="$result"/>                
            </xsl:result-document>
            
        </xsl:for-each>
    </xsl:template>


</xsl:stylesheet>