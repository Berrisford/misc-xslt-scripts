<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">    
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <xsl:result-document href="modified-dois-test.xml">
            <dois>
                <xsl:apply-templates select="//doi"/>
            </dois>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="doi">
        <doi>
            <xsl:value-of select="concat('https://api.altmetric.com/v1/fetch/doi/', ., '?key=76abb434fc30c2eb3e797c3dce0bd73b&amp;include_sources=blogs,twitter')"/>
        </doi>
    </xsl:template>
    
</xsl:stylesheet>