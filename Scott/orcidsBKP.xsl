<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:variable name="organisations">
        <xsl:copy-of select="doc('organisations.xml')/response/result"/>
    </xsl:variable>
    
    <xsl:template match="/">
        <records>
            <xsl:for-each select="//doc">
                <record>
                    <name>
                        <xsl:value-of select="str[@name='p.displayname']"/>
                    </name>
                    <orcid>
                        <xsl:value-of select="str[@name='orcid.orcid']"/>
                    </orcid>
                    <datestamp>
                        <xsl:value-of select="date[@name='timestamp']"/>
                    </datestamp>
                    <xsl:for-each select="arr[@name='r.ismemberof.pid']/str">
                        <xsl:variable name="node-name">
                            <xsl:value-of select="concat('organisation-', position())"/>
                        </xsl:variable>
                        <xsl:element name="{$node-name}">
                            <xsl:call-template name="getOrg">
                                <xsl:with-param name="org-id" as="xs:string">
                                    <xsl:value-of select="."/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </xsl:element>
                    </xsl:for-each>
                        
                </record>
            </xsl:for-each>            
        </records>        
    </xsl:template>
    
    <xsl:template name="getOrg">
        <xsl:param name="org-id" as="xs:string"/>
        <xsl:variable name="doc">
            <xsl:copy-of select="$organisations/result/doc[@PID = $org-id]"/>
        </xsl:variable>
        <xsl:value-of select="$doc/doc/str[@name='o.name']"/>
    </xsl:template>
    
    
</xsl:stylesheet>