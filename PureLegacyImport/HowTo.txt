1.  Put all accompanying files in the same folder as the folder containing all the record folders.
2.  Run files.bat.
3.  Open files.xml with UltraEdit (or other editor).
4.  Search and replace (all) ^p with </file>^p.
5.  Search and replace (all) "uk-ac-man" with "<file>[THE PATH OF YOUR FILES (using "/" NOT "\"]/uk-ac-man". 
6.  Prepend "<?xml version="1.0" encoding="UTF-8"?>
                <files>" to the top of the file.
7.  Append "</files>" to the end of the file and save.
8.  Edit escholar2pure.xsl to change the "file_path" variable to match the path where your files are located.
9.  Edit escholar2pure.xsl to change the "url" variable to match the content type you're dealing with (uk-ac-man-con?1 is journal-article).
10. Edit escholar2pure.xsl to change the "obj_files" variable to use "files.xml".
11. Edit legacy2pure.bat to match the path of your Saxon.
12. Run legacy2pure.bat to get all Scopus matched file data.
13. Edit escholar2pure.bat to match the path of your Saxon.
14. Run escholar2pure.bat to get all unmatched file data.
15. (If desired) Merge the outputs of legacy2pure.bat and escholar2pure.bat (output.xml and output_escholar.xml) to get the entire list of Pure files.
