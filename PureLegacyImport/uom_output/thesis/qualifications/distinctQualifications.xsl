<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <xsl:result-document href="distinctQualifications.xml">            
            <qualifications>
                <xsl:for-each select="distinct-values(//str[@name='m.note.degreelevel'])">
                    <xsl:element name="str">
                        <xsl:attribute name="name" select="'m.note.degreelevel'"/>
                        <xsl:value-of select="."/>
                    </xsl:element>
                </xsl:for-each>
            </qualifications>
        </xsl:result-document>
    </xsl:template>
</xsl:stylesheet>