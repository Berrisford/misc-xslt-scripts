<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">    
    <xsl:output indent="yes" method="xml"/>
    
    <!-- BA: This script iterates across all records (of a particular content type) in chunks of 1000 (rows) and checks to see if the record -->
    <!--  has been matched by Scopus. If not, it generates the requisite Pure xml entirely from eScolar data. Change $rows, $num_found, $obj_files -->
    <!--  accordingly to test quickly with small sets of data. Change $file_path to match where your files are located. -->
    
    <xsl:variable name="root_url" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/scw/select/'" />
    
    <!-- BA: 22/04/16 - Search for all content types except uk-ac-man-con:8, uk-ac-man-con:19, uk-ac-man-con:20, uk-ac-man-con:21 -->        
    <xsl:variable name="url" select="concat($root_url, '?q=-r.isofcontenttype.pid%3Auk-ac-man-con%5C%3A8+AND%0A-r.isofcontenttype.pid%3Auk-ac-man-con%5C%3A19+AND%0A-r.isofcontenttype.pid%3Auk-ac-man-con%5C%3A20+AND%0A-r.isofcontenttype.pid%3Auk-ac-man-con%5C%3A21', '&amp;fl=PID,r.isbelongsto.pid,x.state,m.genre.version,m.genre.form&amp;omitHeader=true')"></xsl:variable>        
    
    <!-- BA: 22/04/16 Test for item with more than 1 owner, 1 of whom is excluded (should still return PID) -->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=PID%3Auk-ac-man-scw%5C%3A1b1518', '&amp;fl=PID,r.isbelongsto.pid,x.state,m.genre.version,m.genre.form&amp;omitHeader=true')"></xsl:variable>-->        
    
    <!-- BA: 22/04/16 Test for item with more than 1 owner, 1 of whom is an abcde user (should still return PID) -->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=PID%3Auk-ac-man-scw%5C%3A1a1579', '&amp;fl=PID,r.isbelongsto.pid,x.state,m.genre.version,m.genre.form&amp;omitHeader=true')"></xsl:variable>-->        
    
    <xsl:variable name="rows" select="1000"/>
    <xsl:variable name="num_found">
        <xsl:value-of select="doc(concat($url, '&amp;start=0&amp;rows=0'))/response/result/@numFound"/>
        <!--<xsl:value-of select="doc(concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?17', '&amp;fl=PID&amp;omitHeader=true&amp;start=0&amp;rows=0'))/response/result/@numFound"/>-->
        <!--<xsl:value-of select="10"/>-->
    </xsl:variable>
    
    <!-- Retrieve the file document here. We can then just check this to see if a selected eScholar pid is in the document -->
    <!--<xsl:variable name="file_path" select="'C:/PureLegacyImport/uom_output/'"/>-->
    <xsl:variable name="obj_files">
        <xsl:copy-of select="doc('filtered-pure-pids.xml')"/>
        <!--<xsl:copy-of select="doc('pure-pids(short).xml')"/>-->
    </xsl:variable>    


    <!-- BA: 01/12/15 - Ignore matched abcde items and items belonging to me and NG etc... - start  -->                
    <xsl:variable name="excluded_pids">
        <xsl:copy-of select="doc('excluded_pids.xml')/excluded_pids"/>
    </xsl:variable>
    <!-- BA: 01/12/15 - Ignore matched abcde items and items belonging to me and NG etc... - end  -->                
        
    <xsl:template match="/">
        <records>
        <xsl:call-template name="recurse">
            <xsl:with-param name="start" select="0"/>
            <xsl:with-param name="end" select="$rows"/>
        </xsl:call-template>
        </records>
    </xsl:template>
        
    <xsl:template name="recurse">
        <xsl:param name="start"/>
        <xsl:param name="end"/>
        <xsl:variable name="obj_response">
            <xsl:copy-of select="doc(concat($url, '&amp;start=', $start, '&amp;rows=', $rows))"/>
        </xsl:variable>
        <xsl:for-each select="$obj_response/response/result/doc">
            <!--<xsl:variable name="filename" select="replace(str[@name='PID'], ':', '_')"/>-->
            <xsl:variable name="current-pid" select="str[@name='PID']/text()"/>
            <xsl:choose>
                <!-- exists($excluded_pids/excluded_pids/pid[@id = $pid]) -->
                <xsl:when test="exists($obj_files/pids/pid[text() = $current-pid])">
                    <!--<xsl:when test="exists($obj_files/pids/pid/$current-pid)">-->
                    <!--<xsl:when test="exists($obj_files/pids/pid/text() = ./str[@name='PID'])">-->
                    <!--exists($obj_files/pids/pid/text() = 'uk-ac-man-scw:1a100')-->
                    <!--<xsl:when test="exists($obj_files/pids/pid[. = concat($file_path, $filename)])">-->
                    <!-- ignore... -->
                </xsl:when>
                <xsl:otherwise>
                    <xsl:variable name="excluded">
                        <xsl:call-template name="test_exclusion">
                            <xsl:with-param name="doc">
                                <xsl:copy-of select="."/>
                            </xsl:with-param>    
                        </xsl:call-template>                        
                    </xsl:variable>
                    <xsl:variable name="real_person">
                        <xsl:call-template name="test_real_person">
                            <xsl:with-param name="doc">
                                <xsl:copy-of select="."/>
                            </xsl:with-param>    
                        </xsl:call-template>                        
                    </xsl:variable>
                    <!-- BA 22/04/16 - Change this to exclude only if all PIDs are excluded -->                    
                    <xsl:if test="contains($excluded, 'False') and contains($real_person, 'True')">
                        <!--<xsl:if test="not(contains($excluded, 'True')) and contains($real_person, 'True')">-->
                        <record>
                            <xsl:call-template name="unmatched">
                                <xsl:with-param name="pid" select="replace(str[@name='PID'], ':', '?')"/>
                            </xsl:call-template>
                        </record>                        
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
        
        <xsl:if test="$end &lt; $num_found">
            <xsl:call-template name="recurse">
                <xsl:with-param name="start" select="$end + 1"/>
                <xsl:with-param name="end" select="$end + $rows"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="test_exclusion">
        <xsl:param name="doc"/>
        <result>
            <xsl:for-each select="$doc/doc/arr[@name='r.isbelongsto.pid']/str">
                <xsl:variable name="pid" select="."/>
                <xsl:choose>
                    <!-- BA: 22/04/16 - Edit this to only be true if not abcde user -->
                    <xsl:when test="exists($excluded_pids/excluded_pids/pid[@id = $pid]) or contains($pid, 'abcde')">
                        <!--<xsl:when test="exists($excluded_pids/excluded_pids/pid[@id = $pid])">-->
                        <xsl:value-of select="'True'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'False'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </result>
    </xsl:template>

    <xsl:template name="test_real_person">
        <!-- BA: Test to exclude items owned only by abcde persons -->
        <xsl:param name="doc"/>
        <result>
            <xsl:for-each select="$doc/doc/arr[@name='r.isbelongsto.pid']/str">
                <xsl:variable name="pid" select="."/>
                <xsl:choose>
                    <xsl:when test="not(contains($pid, 'abcde'))">
                        <xsl:value-of select="'True'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'False'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </result>
    </xsl:template>
    
    <xsl:template name="unmatched">
        <xsl:param name="pid"/>
        <xsl:variable name="obj_doc">
            <xsl:copy-of select="doc(concat($root_url, '?q=PID:', $pid, '&amp;omitHeader=true&amp;start=0&amp;rows=10'))/response/result/doc"/>            
        </xsl:variable>                
        <escholar-id><xsl:value-of select="$obj_doc/doc/str[@name='PID']"/></escholar-id>
        <content-type><xsl:value-of select="$obj_doc/doc/str[@name='m.genre.content-type']"></xsl:value-of></content-type>        
        <created-date><xsl:value-of select="replace(substring($obj_doc/doc/date[@name='x.createddate'], 1, 16), 'T', ' T ')"/></created-date>
        <visibility><xsl:value-of select="replace(replace($obj_doc/doc/str[@name='x.state'], 'Active', 'Visible'), 'Inactive', 'Hidden')"></xsl:value-of></visibility>
        <!-- BA: 06/05/16 - Add new fields - start -->
        <year><xsl:value-of select="$obj_doc/doc/str[@name='m.year']"/></year>
        <xsl:choose>
            <xsl:when test="$obj_doc/doc/arr[@name='f.isfileattached']/str">
                <file-attached>Yes</file-attached>
            </xsl:when>
            <xsl:otherwise>
                <file-attached>No</file-attached>
            </xsl:otherwise>
        </xsl:choose>
        <owners>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='r.isbelongsto.source']/str"></xsl:apply-templates>
        </owners>
        <!-- BA: 06/05/16 - Add new fields - end -->

    </xsl:template>
    
    <xsl:template match="arr[@name='r.isbelongsto.source']/str">
        <owner>
            <display-name>
                <xsl:value-of select="tokenize(., '\|\|')[1]"></xsl:value-of>
            </display-name>            
            <spot-id>
                <xsl:value-of select="tokenize(., '\|\|')[3]"></xsl:value-of>
            </spot-id>            
        </owner>        
    </xsl:template>
    
    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>
    
</xsl:stylesheet>
