<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <xsl:result-document href="null-names.xml">
            <records>
                <xsl:apply-templates select="//info[contains(., 'null')]"/>
            </records>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="info">
        <null-name-record>
<!--            
            <info>
                <xsl:value-of select="."/>
            </info>
-->            
            <escholar-id>
                <xsl:value-of select="substring-before(., ';')"/>
            </escholar-id>
        </null-name-record>
    </xsl:template>
    
</xsl:stylesheet>