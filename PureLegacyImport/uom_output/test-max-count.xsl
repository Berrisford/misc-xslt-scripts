<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <count>
            <xsl:value-of select="max(count(//record/owners/owner))"/>
        </count>
        
        <xsl:for-each select="//record">
            <xsl:if test="count(owners/owner) &gt; 4">
                <xsl:copy-of select="."/>
            </xsl:if>            
        </xsl:for-each>
        
        
    </xsl:template>
    
</xsl:stylesheet>