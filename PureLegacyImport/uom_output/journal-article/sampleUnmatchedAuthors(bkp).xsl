<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">    
    <xsl:output indent="yes" method="xml"/>
    
    <!-- BA: This script iterates across all records (of a particular content type) in chunks of 1000 (rows) and checks to see if the record -->
    <!--  has been matched by Scopus. If not, it generates the requisite Pure xml entirely from eScolar data. Change $rows, $num_found, $obj_files -->
    <!--  accordingly to test quickly with small sets of data. Change $file_path to match where your files are located. -->
    
    <xsl:variable name="root_url" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/scw/select/'"></xsl:variable>
    <xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?1', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?1', '&amp;fl=PID&amp;omitHeader=true&amp;start=0&amp;rows=0')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A1+AND+m.preceding.source:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A1+AND+r.hasauthor.source:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A1+AND+m.host.title.abbreviated:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A1+AND+m.genre.status:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    
    <xsl:variable name="rows" select="2"/>
    <xsl:variable name="num_found">
        <!--<xsl:value-of select="doc(concat($url, '&amp;start=0&amp;rows=0'))/response/result/@numFound"/>-->
        <!--<xsl:value-of select="doc(concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?1', '&amp;fl=PID&amp;omitHeader=true&amp;start=0&amp;rows=0'))/response/result/@numFound"/>-->
        <xsl:value-of select="2"/>
    </xsl:variable>
    
    <!-- Retrieve the file document here. We can then just check this to see if a selected eScholar pid is in the document -->
    <xsl:variable name="file_path" select="'C:/PureLegacyImport/uom_output/journal-article/'"/>
    <xsl:variable name="obj_files">
        <xsl:copy-of select="doc('files.xml')"/>
    </xsl:variable>    
    
    <xsl:template match="/">
        <records>
        <xsl:call-template name="recurse">
            <xsl:with-param name="start" select="0"/>
            <xsl:with-param name="end" select="$rows"/>
        </xsl:call-template>
        </records>
    </xsl:template>
        
    <xsl:template name="recurse">
        <xsl:param name="start"/>
        <xsl:param name="end"/>
        <xsl:variable name="obj_response">
            <xsl:copy-of select="doc(concat($url, '&amp;start=', $start, '&amp;rows=', $rows))"/>
        </xsl:variable>
        <xsl:for-each select="$obj_response/response/result/doc">
            <xsl:variable name="filename" select="replace(str[@name='PID'], ':', '_')"/>
            <xsl:choose>
                <xsl:when test="exists($obj_files/files/file[. = concat($file_path, $filename)])">
                    <!-- ignore... -->
                    <!--<FOUND><xsl:value-of select="str"/></FOUND>-->
                </xsl:when>
                <xsl:otherwise>
                    <record>
                        <escholar-pid><xsl:value-of select="str"/></escholar-pid>
                        <xsl:call-template name="unmatched">
                            <xsl:with-param name="pid" select="replace(str, ':', '?')"/>
                        </xsl:call-template>
                    </record>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>                
        
        <xsl:if test="number($end) lt number($num_found)">
            <xsl:call-template name="recurse">
                <xsl:with-param name="start" select="$end + 1"/>
                <xsl:with-param name="end" select="$end + $rows"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="unmatched">
        <xsl:param name="pid"/>
        <xsl:variable name="obj_doc">
            <xsl:copy-of select="doc(concat($root_url, '?q=PID:', $pid, '&amp;omitHeader=true&amp;start=0&amp;rows=10'))/response/result/doc"/>            
        </xsl:variable>

        <xsl:variable name="node_names">
            <node-names>
                <node-name role="author">r.isbelongsto.source</node-name>
                <node-name role="author">r.hasauthor.source</node-name>
                <node-name role="coauthor">r.hascoauthor.source</node-name>
                <node-name role="corresponding_author">r.hascorrespondingauthor.source</node-name>
                <node-name role="co_senior_author">r.hascoseniorauthor.source</node-name>
                <node-name role="first_author">r.hasfirstauthor.source</node-name>
                <node-name role="last_author">r.haslastauthor.source</node-name>
                <node-name role="senior_author">r.hasseniorauthor.source</node-name>
            </node-names>
        </xsl:variable>
        
<!--
        <owners>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='r.isbelongstoorg.source']"/>
        </owners>
-->
        <xsl:variable name="internal-persons">
            <xsl:call-template name="get_authors">
                <xsl:with-param name="obj_doc">
                    <xsl:copy-of select="$obj_doc/doc"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:variable>
        
        
<!--        
        <internal-person-list>
            <xsl:for-each select="$node_names/node-names/node-name">
                <xsl:call-template name="author_assoc">                        
                    <xsl:with-param name="node_name">
                        <xsl:copy-of select="."/>
                    </xsl:with-param>
                    <xsl:with-param name="solr_node">
                        <xsl:copy-of select="$obj_doc"/>
                    </xsl:with-param>
                </xsl:call-template>                            
            </xsl:for-each>
        </internal-person-list>
-->
        <internal-person-list>
            <xsl:for-each select="distinct-values($internal-persons/internal-persons/internal-person)">
<!--
                <internalperson>
                    <xsl:value-of select="."/>                    
                </internalperson>
-->
                                
                <internalperson>
                    <id><xsl:value-of select="tokenize(., '; ')[1]"/></id>
                    <role><xsl:value-of select="tokenize(., '; ')[2]"/></role>
                </internalperson>
                
            </xsl:for-each>            
        </internal-person-list>
        
        <author-list>
<!--
            <xsl:for-each select="$node_names/node-names/node-name">
                <xsl:call-template name="author_list">                        
                    <xsl:with-param name="node_name">
                        <xsl:copy-of select="."/>
                    </xsl:with-param>
                    <xsl:with-param name="solr_node">
                        <xsl:copy-of select="$obj_doc"/>
                    </xsl:with-param>
                </xsl:call-template>                            
            </xsl:for-each>
-->
            <xsl:choose>
                <xsl:when test="$obj_doc/doc/str[@name='m.note.authors']">
                    <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.note.authors']"/></author-string>
                    <role>author</role>
                </xsl:when>
                <xsl:when test="$obj_doc/doc/str[@name='m.note.collaborators']">
                    <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.note.collaborators']"/></author-string>
                    <role>collaborator</role>
                </xsl:when>
                <xsl:when test="$obj_doc/doc/str[@name='m.note.translators']">
                    <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.note.translators']"/></author-string>
                    <role>translator</role>
                </xsl:when>
                <xsl:when test="$obj_doc/doc/arr[@name='m.name.aut.source']">
                    <author-string><xsl:apply-templates select="$obj_doc/doc/arr[@name='m.name.aut.source']"/></author-string>                    
                    <role>author</role>
                </xsl:when>
            </xsl:choose>
            
        </author-list>

        <authors-as-a-string>
            <xsl:choose>
                <xsl:when test="$obj_doc/doc/str[@name='m.note.authors']">
                    <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.note.authors']"/></author-string>
                    <role>author</role>
                </xsl:when>
                <xsl:when test="$obj_doc/doc/str[@name='m.note.collaborators']">
                    <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.note.collaborators']"/></author-string>
                    <role>collaborator</role>
                </xsl:when>
                <xsl:when test="$obj_doc/doc/str[@name='m.note.translators']">
                    <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.note.translators']"/></author-string>
                    <role>translator</role>
                </xsl:when>
                <xsl:when test="$obj_doc/doc/arr[@name='m.name.aut.source']">
                    <author-string><xsl:apply-templates select="$obj_doc/doc/arr[@name='m.name.aut.source']"/></author-string>                    
                    <role>author</role>
                </xsl:when>
            </xsl:choose>
        </authors-as-a-string>
        
    </xsl:template>
            
    <xsl:template match="arr[@name='m.name.aut.source']">
        <xsl:for-each select="str">
            <xsl:variable name="count" as="xs:integer">
                <xsl:number/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$count lt 2">
                    <xsl:value-of select="tokenize(., '\|\|')[1]"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat('; ', tokenize(., '\|\|')[1])"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>


    <xsl:template name="get_authors">
        <xsl:param name="obj_doc"/>
        <xsl:variable name="node_names">
            <node-names>
                <node-name role="author">r.isbelongsto.source</node-name>
                <node-name role="author">r.hasauthor.source</node-name>
                <node-name role="coauthor">r.hascoauthor.source</node-name>
                <node-name role="corresponding_author">r.hascorrespondingauthor.source</node-name>
                <node-name role="co_senior_author">r.hascoseniorauthor.source</node-name>
                <node-name role="first_author">r.hasfirstauthor.source</node-name>
                <node-name role="last_author">r.haslastauthor.source</node-name>
                <node-name role="senior_author">r.hasseniorauthor.source</node-name>
            </node-names>
        </xsl:variable>
        
        <internal-persons>
            <xsl:for-each select="$node_names/node-names/node-name">
                <xsl:call-template name="author_assoc">                        
                    <xsl:with-param name="node_name">
                        <xsl:copy-of select="."/>
                    </xsl:with-param>
                    <xsl:with-param name="solr_node">
                        <xsl:copy-of select="$obj_doc"/>
                    </xsl:with-param>
                </xsl:call-template>                            
            </xsl:for-each>   
        </internal-persons>        
    </xsl:template>

    <xsl:template name="author_assoc">
        <xsl:param name="node_name"/>
        <xsl:param name="solr_node"/>
        <xsl:for-each select="$solr_node/doc/arr[@name=$node_name/node-name]/str">
<!--            
            <internalperson>
                <id>
                    <xsl:value-of select="tokenize(., '\|\|')[3]"/>                
                </id>                                        
                <role>
                    <xsl:value-of select="$node_name/node-name/@role"></xsl:value-of>
                </role>
            </internalperson>
-->
            
            <internal-person>
                <xsl:value-of select="concat(tokenize(., '\|\|')[3], '; ', $node_name/node-name/@role)"/>                
            </internal-person>

        </xsl:for-each>
    </xsl:template>

    <xsl:template name="author_list">
        <xsl:param name="node_name"/>
        <xsl:param name="solr_node"/>
        <xsl:for-each select="$solr_node/doc/arr[@name=$node_name/node-name]/str">
            <author>
                <first-name>
                    <xsl:value-of select="normalize-space(tokenize(tokenize(., '\|\|')[1], ',')[2])"/>                
                </first-name>                                        
                <last-name>
                    <xsl:value-of select="normalize-space(tokenize(tokenize(., '\|\|')[1], ',')[1])"/>                
                </last-name>                                        
                <role>
                    <xsl:value-of select="$node_name/node-name/@role"></xsl:value-of>
                </role>
            </author>            
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="author_string">
        <xsl:param name="node_name"/>
        <xsl:param name="solr_node"/>
        <xsl:param name="node_name_count" as="xs:integer"/>
        <xsl:for-each select="$solr_node/doc/arr[@name=$node_name/node-name]/str">
            <xsl:variable name="count" as="xs:integer">
                <xsl:number/>
            </xsl:variable>            
                <xsl:choose>
                    <xsl:when test="($count lt 2) and ($node_name_count lt 2)">
                        <xsl:value-of select="tokenize(., '\|\|')[1]"></xsl:value-of>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat('; ', tokenize(., '\|\|')[1])"/>
                    </xsl:otherwise>
                </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="author_role">
        <xsl:param name="node_name"/>
        <xsl:param name="solr_node"/>
        <xsl:param name="node_name_count" as="xs:integer"/>
        <xsl:for-each select="$solr_node/doc/arr[@name=$node_name/node-name]/str">
            <xsl:variable name="count" as="xs:integer">
                <xsl:number/>
            </xsl:variable>            
            <xsl:choose>
                <xsl:when test="($count lt 2) and ($node_name_count lt 2)">
                    <xsl:value-of select="$node_name/node-name/@role"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat('; ', $node_name/node-name/@role)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>
    

    <xsl:template match="arr[@name='r.hasauthor.source'] | arr[@name='r.isbelongsto.source']">
        <xsl:for-each select="str">
            <author>
                <first-name>
                    <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[2]"/>
                </first-name>
                <last-name>
                    <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[1]"/>
                </last-name>
                <role>author</role>
            </author>
        </xsl:for-each>
    </xsl:template>    
    

    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>
    
</xsl:stylesheet>
