<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:ait="http://www.elsevier.com/xml/ani/ait"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xoe="http://www.elsevier.com/xml/xoe/dtd" 
    xmlns:ce="http://www.elsevier.com/xml/ani/common" 
    xmlns:cto="http://www.elsevier.com/xml/cto/dtd" 
    xmlns:xocs="http://www.elsevier.com/xml/xocs/dtd"            
    exclude-result-prefixes="xs math ait xsi xoe ce cto xocs"
    version="3.0">
    <xsl:output indent="yes" method="xml"/>    
    
    <xsl:variable name="root_uri" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/scw/select/'" />
    
    <xsl:template match="/">
        <files>
            <xsl:apply-templates select="files" />
        </files>
    </xsl:template>

    <xsl:template match="files">
        <xsl:for-each select="file">
            <xsl:variable name="pid" select="replace(tokenize(., '/')[last()], '_', '?')" />
            <xsl:variable name="query" select="concat($root_uri, '?q=PID%3A', replace($pid, ':', '?'))"></xsl:variable>
            <xsl:variable name="obj_solr">
                <xsl:copy-of select="doc($query)"/>
            </xsl:variable>
            <xsl:variable name="scopus_abstract_path" select="concat(tokenize(., '/')[last()], '/', 'abstract-citations-response_scopus_data_20141101.xml')"></xsl:variable>
            <xsl:variable name="obj_scopus_abstract">
                <xsl:copy-of select="doc($scopus_abstract_path)"/>
            </xsl:variable>
            <xsl:variable name="scopus_id" select="$obj_scopus_abstract/abstract-citations-response/identifier-legend/identifier/scopus_id"/>
            <xsl:variable name="scopus_file_path" select="concat(tokenize(., '/')[last()], '/2-s2.0-', $scopus_id, '.xml')"></xsl:variable>
            <xsl:variable name="obj_scopus_file">
                <xsl:copy-of select="doc($scopus_file_path)"/>
            </xsl:variable>

            <xsl:variable name="solr_title">
                <xsl:choose>
                    <xsl:when test="ends-with($obj_solr/response/result/doc/str[@name='m.title'], '.')">
                        <!--<xsl:when test="substring($obj_solr/response/result/doc/str[@name='m.title'], string-length($obj_solr/response/result/doc/str[@name='m.title']), string-length($obj_solr/response/result/doc/str[@name='m.title'])) = '.'">-->
                        <xsl:value-of select="substring(normalize-space(lower-case($obj_solr/response/result/doc/str[@name='m.title'])), 0, string-length($obj_solr/response/result/doc/str[@name='m.title']))"/>                        
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="normalize-space(lower-case($obj_solr/response/result/doc/str[@name='m.title']))"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>            

            <xsl:variable name="scopus_title">
                <xsl:for-each select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/citation-title/titletext">
                    <xsl:choose>
                        <xsl:when test="ends-with(., '.')">
                            <xsl:variable name="tmp_scopus_title">
                                <xsl:value-of select="substring(normalize-space(lower-case(.)), 0, string-length(.))"/>
                                <!--<xsl:value-of select="substring(normalize-space(.), 0, string-length(.))"/>-->
                            </xsl:variable>
                            <xsl:choose>
                                <xsl:when test="$tmp_scopus_title = $solr_title">
                                    <xsl:value-of select="$tmp_scopus_title"/>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:when test="not(ends-with(., '.'))">
                            <xsl:variable name="tmp_scopus_title">
                                <xsl:value-of select="normalize-space(lower-case(.))"/>
                                <!--<xsl:value-of select="normalize-space(.)"/>-->
                            </xsl:variable>
                            <xsl:choose>
                                <xsl:when test="$tmp_scopus_title = $solr_title">
                                    <xsl:value-of select="$tmp_scopus_title"/>
                                </xsl:when>
                            </xsl:choose>                        
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>                                              
            </xsl:variable>

            <xsl:variable name="scopus_title_cased">
                <xsl:for-each select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/citation-title/titletext">
                    <xsl:choose>
                        <xsl:when test="ends-with(., '.')">
                            <xsl:variable name="tmp_scopus_title">
                                <xsl:value-of select="substring(normalize-space(lower-case(.)), 0, string-length(.))"/>
                                <!--<xsl:value-of select="substring(normalize-space(.), 0, string-length(.))"/>-->
                            </xsl:variable>
                            <xsl:choose>
                                <xsl:when test="$tmp_scopus_title = $solr_title">
                                    <xsl:value-of select="."/>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:when test="not(ends-with(., '.'))">
                            <xsl:variable name="tmp_scopus_title">
                                <xsl:value-of select="normalize-space(lower-case(.))"/>
                                <!--<xsl:value-of select="normalize-space(.)"/>-->
                            </xsl:variable>
                            <xsl:choose>
                                <xsl:when test="$tmp_scopus_title = $solr_title">
                                    <xsl:value-of select="."/>
                                </xsl:when>
                            </xsl:choose>                        
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>                                        
            </xsl:variable>
                        
            <xsl:variable name="item_match">
                <xsl:choose>
                    <xsl:when test="$obj_scopus_file/xocs:doc/xocs:meta/xocs:doi and $obj_solr/response/result/doc/str[@name='m.identifier.doi']">
                        <xsl:if test="$obj_scopus_file/xocs:doc/xocs:meta/xocs:doi = $obj_solr/response/result/doc/str[@name='m.identifier.doi']">
                            <xsl:value-of select="'true'"/>
                        </xsl:if>
                    </xsl:when>
                    <xsl:when test="not($obj_scopus_file/xocs:doc/xocs:meta/xocs:doi) or not($obj_solr/response/result/doc/str[@name='m.identifier.doi'])">
                        <xsl:choose>
                            <xsl:when test="$scopus_title = $solr_title">
                                <xsl:value-of select="'true'"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="'false'"/>
                            </xsl:otherwise>
                        </xsl:choose>                                                
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'false'"/>
                    </xsl:otherwise>                    
                </xsl:choose>    
            </xsl:variable>
            
            <file>
<!--                
                <match>
                    <xsl:choose>
                        <xsl:when test="$item_match = 'false'">
                            <xsl:value-of select="'false'"/>                                                        
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="'true'"/>                            
                        </xsl:otherwise>
                    </xsl:choose>
                </match>
-->
                <publication-status>published</publication-status>
                <!--<original-language><xsl:value-of select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/citation-info/citation-language"/></original-language>-->
                <original-language><xsl:value-of select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/citation-title/titletext[@original='y']/@xml:lang"/></original-language>
                <title>
                    <xsl:choose>
                        <xsl:when test="$scopus_title_cased != ''">
                            <xsl:value-of select="$scopus_title_cased"/>                        
                        </xsl:when>
                        <xsl:otherwise>
                                <xsl:value-of select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/citation-title/titletext[@original='y']"/>                       
                        </xsl:otherwise>
                    </xsl:choose>                    
                </title>
<!--
                <esc-title>
                    <xsl:value-of select="$obj_solr/response/result/doc/str[@name='m.title']"/>                    
                </esc-title>
-->
                
                <number-of-pages>
                    <xsl:choose>
                        <xsl:when test="string(number(replace($obj_scopus_file/xocs:doc/xocs:meta/xocs:lastpage, 'i', '')) - number(replace($obj_scopus_file/xocs:doc/xocs:meta/xocs:firstpage, 'i', ''))) != 'NaN'">
                            <xsl:value-of select="number(replace($obj_scopus_file/xocs:doc/xocs:meta/xocs:lastpage, 'i', '')) - number(replace($obj_scopus_file/xocs:doc/xocs:meta/xocs:firstpage, 'i', ''))"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="''"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </number-of-pages>
                <publication-date>
                    <year><xsl:value-of select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/source/publicationdate/year"/></year>
                    <month><xsl:value-of select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/source/publicationdate/month"/></month>
                    <day><xsl:value-of select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/source/publicationdate/day"/></day>
                </publication-date>
                
                <xsl:choose>
                    <xsl:when test="$item_match = 'false'">
                        <scopus-authors>
                            <xsl:apply-templates select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head"/>                            
                        </scopus-authors>
                        <esc-authors>
                            <xsl:apply-templates select="$obj_solr/response/result/doc/arr[@name='m.name.aut']"/>
                        </esc-authors>
                    </xsl:when>
                    <xsl:otherwise>
                        <owners>
                            <xsl:apply-templates select="$obj_solr/response/result/doc/arr[@name='r.isbelongstoorg.source']"></xsl:apply-templates>
                        </owners>                        
                    </xsl:otherwise>
                </xsl:choose>
                
                <abstracts>
                    <xsl:apply-templates select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/abstracts"/>
                </abstracts>
                <keywords>
                    <xsl:apply-templates select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/citation-info/author-keywords"/>
                </keywords>
                <xsl:choose>
                    <xsl:when test="$obj_scopus_file/xocs:doc/xocs:meta/xocs:doi">
                        <doi><xsl:value-of select="$obj_scopus_file/xocs:doc/xocs:meta/xocs:doi"/></doi>                        
                    </xsl:when>
                    <xsl:otherwise>
                        <doi><xsl:value-of select="$obj_solr/response/result/doc/str[@name='m.identifier.doi']"/></doi>                        
                    </xsl:otherwise>
                </xsl:choose> 
                <urls>
                    <xsl:apply-templates select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/source/website"/>
                </urls>
                <bibliographical-note>
                    <xsl:apply-templates select="$obj_solr/response/result/doc/arr[@name='m.note.general']"/>
                </bibliographical-note>                
                <internal-person-list>
                    <xsl:variable name="node_names">
                        <node-names>
                            <node-name role="author">r.isbelongsto.source</node-name>
                            <node-name role="author">r.hasauthor.source</node-name>
                            <node-name role="coauthor">r.hascoauthor.source</node-name>
                            <node-name role="corresponding_author">r.hascorrespondingauthor.source</node-name>
                            <node-name role="co_senior_author">r.hascoseniorauthor.source</node-name>
                            <node-name role="first_author">r.hasfirstauthor.source</node-name>
                            <node-name role="last_author">r.haslastauthor.source</node-name>
                            <node-name role="senior_author">r.hasseniorauthor.source</node-name>
                            <node-name role="thesis_advisor">r.hasadvisor.source</node-name>
                            <node-name role="main_supervisor">r.hasmainsupervisor.source</node-name>
                            <node-name role="co_supervisor">r.hascosupervisor.source</node-name>
                        </node-names>
                    </xsl:variable>
                    <xsl:for-each select="$node_names/node-names/node-name">
                        <xsl:call-template name="author_assoc">                        
                            <xsl:with-param name="node_name">
                                <xsl:copy-of select="."/>
                            </xsl:with-param>
                            <xsl:with-param name="solr_node">
                                <xsl:copy-of select="$obj_solr"/>
                            </xsl:with-param>
                        </xsl:call-template>                            
                    </xsl:for-each>
                </internal-person-list>
                <xsl:apply-templates select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head" mode="author-list"/>
                <journal-title>
                    <xsl:value-of select="concat($obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/source/sourcetitle, '|', $obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/source/sourcetitle-abbrev)"/>
                </journal-title>
                <pages>
                    <startpage><xsl:value-of select="replace($obj_scopus_file/xocs:doc/xocs:meta/xocs:firstpage, 'i', '')"/></startpage>
                    <endpage><xsl:value-of select="replace($obj_scopus_file/xocs:doc/xocs:meta/xocs:lastpage, 'i', '')"/></endpage>
                </pages>
                <article-number><xsl:value-of select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/source/article-number"/></article-number>
                <volume><xsl:value-of select="$obj_scopus_file/xocs:doc/xocs:meta/xocs:volume"/></volume>
                <issue><xsl:value-of select="$obj_scopus_file/xocs:doc/xocs:meta/xocs:issue"/></issue>
                <print-issn><xsl:value-of select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/source/issn [@type='print']"/></print-issn>
                <electronic-issn><xsl:value-of select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/source/issn [@type='electronic']"/></electronic-issn>
                <peer-review>Yes</peer-review>                
                <visibility>
                    <xsl:apply-templates select="$obj_solr/response/result/doc/str[@name='x.state']/text()"></xsl:apply-templates>
                </visibility>
            </file>
        </xsl:for-each>    
    </xsl:template>
    
    <xsl:template match="abstracts">
        <xsl:for-each select="abstract">
            <abstract>
                <xsl:value-of select="normalize-space(.)"></xsl:value-of>
            </abstract>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="author-keywords">
        <xsl:for-each select="author-keyword">
            <keyword>
                <xsl:value-of select="normalize-space(.)"></xsl:value-of>                                    
            </keyword>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="arr[@name='r.isbelongstoorg.source']">
        <xsl:for-each select="str">
            <owner>
                <xsl:value-of select="normalize-space(.)"></xsl:value-of>
            </owner>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="arr[@name='m.name.aut']">
        <xsl:for-each select="str">
            <xsl:variable name="count_aut">
                <xsl:number/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$count_aut &gt; 1">
                    <xsl:value-of select="concat('; ', normalize-space(.))"></xsl:value-of>                    
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="normalize-space(.)"></xsl:value-of>                    
                </xsl:otherwise>                
            </xsl:choose>
        </xsl:for-each>            
    </xsl:template>

    <xsl:template match="head">
        <xsl:for-each select="author-group">
            <xsl:variable name="count_author_group">
                <xsl:number/>
            </xsl:variable>
            <xsl:for-each select="author">
                <xsl:variable name="count_author">
                    <xsl:number/>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="$count_author_group &gt; 1 or $count_author &gt; 1">
                        <xsl:value-of select="concat('; ', ce:indexed-name)"></xsl:value-of>                    
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="ce:indexed-name"></xsl:value-of>                    
                    </xsl:otherwise>                
                </xsl:choose>                
            </xsl:for-each>    
        </xsl:for-each>            
    </xsl:template>

    <xsl:template match="head" mode="author-list">
        <author-list>            
            <xsl:for-each select="author-group">
                <xsl:for-each select="author">
                    <author>
                        <author-first-name><xsl:value-of select="preferred-name/ce:given-name"/></author-first-name>
                        <author-last-name><xsl:value-of select="preferred-name/ce:surname"/></author-last-name>
                        <role>author</role>
                    </author>
                </xsl:for-each>    
            </xsl:for-each>
        </author-list>            
    </xsl:template>    

    <xsl:template match="website">
        <url>
            <xsl:value-of select="ce:e-address"/>
        </url>
    </xsl:template>
    
    <xsl:template match="arr[@name='m.note.general']">
        <xsl:for-each select="str">
            <xsl:variable name="count">
                <xsl:number/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$count &gt; 1">
                    <xsl:value-of select="concat('&#xa;', normalize-space(.))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="normalize-space(.)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template name="author_assoc">
        <xsl:param name="node_name"/>
        <xsl:param name="solr_node"/>
        <xsl:for-each select="$solr_node/response/result/doc/arr[@name=$node_name/node-name]/str">
            <internalperson>
                <id>
                    <xsl:value-of select="tokenize(., '\|\|')[3]"/>                
                </id>                                        
                <role>
                    <xsl:value-of select="$node_name/node-name/@role"></xsl:value-of>
                </role>
            </internalperson>            
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="text()">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>
    
</xsl:stylesheet>