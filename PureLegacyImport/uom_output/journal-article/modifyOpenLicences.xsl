<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <xsl:result-document href="open-licences.xml">
            <xsl:apply-templates/>            
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="licence">
        <licence>
            <xsl:attribute name="name" select="name"/>
            <xsl:apply-templates/>
        </licence>    
    </xsl:template>
    
    <xsl:template match="*" >
        <xsl:element name="{name()}" >
            <xsl:apply-templates select ="@*"/> 
            <xsl:apply-templates/>
        </xsl:element> 
    </xsl:template> 
    
    <xsl:template match="@*" >
        <xsl:attribute name="{name()}" >
            <xsl:value-of select ="."/> 
        </xsl:attribute> 
    </xsl:template> 
    
    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>
    
</xsl:stylesheet>