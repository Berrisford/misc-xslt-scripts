<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <xsl:apply-templates select="//doc"/>
    </xsl:template>
    
    <xsl:template match="doc">
        <record>
            <pid><xsl:value-of select="str[@name='PID']"/></pid>
            
            <author-list>
                <xsl:apply-templates select="arr[@name='m.name.aut.source']/str">
                    <xsl:with-param name="role" select="'author'"/>
                </xsl:apply-templates>
                
            </author-list>
                        
            <authors-as-a-string>
                <xsl:choose>
                    <xsl:when test="str[@name='m.note.authors']">
                        <author-string><xsl:value-of select="str[@name='m.note.authors']"/></author-string>
                        <role>author</role>
                    </xsl:when>
                    <xsl:when test="str[@name='m.note.collaborators']">
                        <author-string><xsl:value-of select="str[@name='m.note.collaborators']"/></author-string>
                        <role>collaborator</role>
                    </xsl:when>
                    <xsl:when test="str[@name='m.note.translators']">
                        <author-string><xsl:value-of select="str[@name='m.note.translators']"/></author-string>
                        <role>translator</role>
                    </xsl:when>
                    <xsl:when test="arr[@name='m.name.aut.source']">
                        <author-string><xsl:apply-templates select="arr[@name='m.name.aut.source']"/></author-string>                    
                        <role>author</role>            
                    </xsl:when>
                </xsl:choose>
            </authors-as-a-string>
            
        </record>
    </xsl:template>

    <xsl:template match="arr[@name='m.name.aut.source']">
        <xsl:for-each select="str">
            <xsl:variable name="count" as="xs:integer">
                <xsl:number/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$count lt 2">
                    <xsl:value-of select="tokenize(., '\|\|')[1]"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat('; ', tokenize(., '\|\|')[1])"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="arr[@name='m.name.aut.source']/str">
        <xsl:param name="role"/>
        <author>
            <first-name>
                <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[2]"/>                
            </first-name>
            <last-name>
                <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[1]"/>                
            </last-name>
            <role><xsl:value-of select="$role"/></role>
        </author>
    </xsl:template>
    
    
</xsl:stylesheet>