<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xoe="http://www.elsevier.com/xml/xoe/dtd" 
    xmlns:ce="http://www.elsevier.com/xml/ani/common" 
    xmlns:cto="http://www.elsevier.com/xml/cto/dtd" 
    xmlns:xocs="http://www.elsevier.com/xml/xocs/dtd"            
    exclude-result-prefixes="xs math xsi xoe ce cto xocs"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>

    <xsl:variable name="root_uri" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/scw/select/'" />
    
    <xsl:variable name="journals">
        <xsl:call-template name="get_journals"/>
    </xsl:variable>
    
    <xsl:template match="/">        
        <distinct-values>            
            <xsl:for-each select="distinct-values($journals/journals/journal)">
                <journal>
                    <xsl:copy-of select="."/>
                </journal>
<!--                
                <journal>
                    <esc-id><xsl:value-of select="tokenize(., '\|\|')[1]"/></esc-id>
                    <scopus-id><xsl:value-of select="tokenize(., '\|\|')[2]"/></scopus-id>
                    <title><xsl:value-of select="tokenize(., '\|\|')[3]"/></title>
                    <abbreviated-title><xsl:value-of select="tokenize(., '\|\|')[4]"/></abbreviated-title>
                    <print-issn><xsl:value-of select="tokenize(., '\|\|')[5]"/></print-issn>
                    <electronic-issn><xsl:value-of select="tokenize(., '\|\|')[6]"/></electronic-issn>
                </journal>
-->                
            </xsl:for-each>                        
        </distinct-values>
    </xsl:template>

    <xsl:template name="get_journals">
        <journals>
        <xsl:for-each select="/files/file">
            <xsl:variable name="pid" select="replace(tokenize(., '/')[last()], '_', '?')" />
<!--            
            <xsl:variable name="query" select="concat($root_uri, '?q=PID%3A', replace($pid, ':', '?'))"></xsl:variable>
            <xsl:variable name="obj_solr">
                <xsl:copy-of select="doc($query)"/>
            </xsl:variable>
-->            
            <xsl:variable name="scopus_abstract_path" select="concat(tokenize(., '/')[last()], '/', 'abstract-citations-response_scopus_data_20141101.xml')"></xsl:variable>
            <xsl:variable name="obj_scopus_abstract">
                <xsl:copy-of select="doc($scopus_abstract_path)"/>
            </xsl:variable>
            <xsl:variable name="scopus_id" select="$obj_scopus_abstract/abstract-citations-response/identifier-legend/identifier/scopus_id"/>
            <xsl:variable name="scopus_file_path" select="concat(tokenize(., '/')[last()], '/2-s2.0-', $scopus_id, '.xml')"></xsl:variable>
            <xsl:variable name="obj_scopus_file">
                <xsl:copy-of select="doc($scopus_file_path)"/>
            </xsl:variable>
            <xsl:variable name="count" as="xs:integer">
                <xsl:number/>
            </xsl:variable>
            <xsl:variable name="esc-id">
                <xsl:choose>
                    <xsl:when test="$count lt 10">
                        <xsl:value-of select="concat('esc-id-', '00000', $count)"/>
                    </xsl:when>
                    <xsl:when test="$count gt 9 and $count lt 100">
                        <xsl:value-of select="concat('esc-id-', '0000', $count)"/>
                    </xsl:when>
                    <xsl:when test="$count gt 99 and $count lt 1000">
                        <xsl:value-of select="concat('esc-id-', '000', $count)"/>
                    </xsl:when>
                    <xsl:when test="$count gt 999 and $count lt 10000">
                        <xsl:value-of select="concat('esc-id-', '00', $count)"/>
                    </xsl:when>
                    <xsl:when test="$count gt 9999 and $count lt 100000">
                        <xsl:value-of select="concat('esc-id-', '0', $count)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat('esc-id-', $count)"/>                        
                    </xsl:otherwise>
                </xsl:choose>                
            </xsl:variable>
            <journal>
                <!-- BA: scopus-id||journal-title||journal-abbreviated-title||print-issn||electronic-issn -->
                <xsl:value-of select="$esc-id"/>                
                <xsl:text>||</xsl:text>                
                <xsl:value-of select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/source/@srcid"/>
                <xsl:text>||</xsl:text>                
                <xsl:value-of select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/source/sourcetitle"/> 
                <xsl:text>||</xsl:text>                
                <xsl:value-of select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/source/sourcetitle-abbrev"/>
                <xsl:text>||</xsl:text>                
                <xsl:value-of select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/source/issn [@type='print']"/>
                <xsl:text>||</xsl:text>                
                <xsl:value-of select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/source/issn [@type='electronic']"/>                    
            </journal>            
        </xsl:for-each>
        </journals>
    </xsl:template>

</xsl:stylesheet>