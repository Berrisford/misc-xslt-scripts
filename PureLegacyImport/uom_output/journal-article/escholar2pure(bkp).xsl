<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">    
    <xsl:output indent="yes" method="xml"/>
    
    <!-- BA: This script iterates across all records (of a particular content type) in chunks of 1000 (rows) and checks to see if the record -->
    <!--  has been matched by Scopus. If not, it generates the requisite Pure xml entirely from eScolar data. Change $rows, $num_found, $obj_files -->
    <!--  accordingly to test quickly with small sets of data. Change $file_path to match where your files are located. -->
    
    <xsl:variable name="root_url" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/scw/select/'"></xsl:variable>
    <xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?1', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?1', '&amp;fl=PID&amp;omitHeader=true&amp;start=0&amp;rows=0')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A1+AND+m.preceding.source:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A1+AND+r.hasauthor.source:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A1+AND+m.host.title.abbreviated:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A1+AND+m.genre.status:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    
    <xsl:variable name="rows" select="1000"/>
    <xsl:variable name="num_found">
        <xsl:value-of select="doc(concat($url, '&amp;start=0&amp;rows=0'))/response/result/@numFound"/>
        <!--<xsl:value-of select="doc(concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?1', '&amp;fl=PID&amp;omitHeader=true&amp;start=0&amp;rows=0'))/response/result/@numFound"/>-->
        <!--<xsl:value-of select="5"/>-->
    </xsl:variable>
    
    <!-- Retrieve the file document here. We can then just check this to see if a selected eScholar pid is in the document -->
    <xsl:variable name="file_path" select="'C:/PureLegacyImport/uom_output/journal-article/'"/>
    <xsl:variable name="obj_files">
        <xsl:copy-of select="doc('files(short).xml')"/>
    </xsl:variable>    
    
    <xsl:template match="/">
        <unmatched-files>
        <xsl:call-template name="recurse">
            <xsl:with-param name="start" select="0"/>
            <xsl:with-param name="end" select="$rows"/>
        </xsl:call-template>
        </unmatched-files>
    </xsl:template>
        
    <xsl:template name="recurse">
        <xsl:param name="start"/>
        <xsl:param name="end"/>
        <xsl:variable name="obj_response">
            <xsl:copy-of select="doc(concat($url, '&amp;start=', $start, '&amp;rows=', $rows))"/>
        </xsl:variable>
        <xsl:for-each select="$obj_response/response/result/doc">
            <xsl:variable name="filename" select="replace(str[@name='PID'], ':', '_')"/>
            <xsl:choose>
                <xsl:when test="exists($obj_files/files/file[. = concat($file_path, $filename)])">
                    <!-- ignore... -->
                    <!--<FOUND><xsl:value-of select="str"/></FOUND>-->
                </xsl:when>
                <xsl:otherwise>
                    <file>
                        <!--<PID><xsl:value-of select="str"/></PID>-->
                        <xsl:call-template name="unmatched">
                            <xsl:with-param name="pid" select="replace(str, ':', '?')"/>
                        </xsl:call-template>
                    </file>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
        
        <xsl:if test="$end &lt; $num_found">
            <xsl:call-template name="recurse">
                <xsl:with-param name="start" select="$end + 1"/>
                <xsl:with-param name="end" select="$end + $rows"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="unmatched">
        <xsl:param name="pid"/>
        <xsl:variable name="obj_doc">
            <xsl:copy-of select="doc(concat($root_url, '?q=PID:', $pid, '&amp;omitHeader=true&amp;start=0&amp;rows=10'))/response/result/doc"/>            
        </xsl:variable>
        <publication-status><xsl:value-of select="$obj_doc/doc/str[@name='m.genre.status']"/></publication-status>
        <original-language>
            <xsl:choose>
                <xsl:when test="$obj_doc/doc/arr[@name='m.languageterm.code']">
                    <xsl:value-of select="$obj_doc/doc/arr[@name='m.languageterm.code'][1]"/>
                </xsl:when>
                <xsl:when test="$obj_doc/doc/str[@name='t.language']">
                    <xsl:value-of select="$obj_doc/doc/str[@name='t.language']"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'eng'"/>
                </xsl:otherwise>
            </xsl:choose>
        </original-language>
        <title>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.title']"/>
        </title>
        <abstracts>
            <abstract>
                <xsl:value-of select="$obj_doc/doc/str[@name='m.abstract']"/>
            </abstract>
        </abstracts>
        <number-of-pages>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.host.page.total']"/>            
        </number-of-pages>         
        <publication-date>
            <xsl:try>
                <year>
                    <xsl:value-of select="tokenize($obj_doc/doc/str[@name='m.dateissued'], '-')[1]"/>
                </year>
                <xsl:catch></xsl:catch>
            </xsl:try>                       
            <xsl:try>
                <month>
                    <xsl:value-of select="tokenize($obj_doc/doc/str[@name='m.dateissued'], '-')[2]"/>
                </month>
                <xsl:catch></xsl:catch>
            </xsl:try>                       
            <xsl:try>
                <day>
                    <xsl:value-of select="tokenize($obj_doc/doc/str[@name='m.dateissued'], '-')[3]"/>
                </day>
                <xsl:catch></xsl:catch>
            </xsl:try>                       
        </publication-date>
        <keywords>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.topic']/str"/>
        </keywords>
        <doi>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.identifier.doi']"/>
        </doi>
        <urls>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.preceding.source']/str"/>
        </urls>
        <bibliographical-note>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.note.general']"/>
        </bibliographical-note>
        <owners>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='r.isbelongstoorg.source']"/>
        </owners>
        <visibility>
            <xsl:apply-templates select="$obj_doc/doc/str[@name='x.state']"/>
        </visibility>
        <internal-person-list>
            <xsl:variable name="node_names">
                <node-names>
                    <node-name role="author">r.isbelongsto.source</node-name>
                    <node-name role="author">r.hasauthor.source</node-name>
                    <node-name role="coauthor">r.hascoauthor.source</node-name>
                    <node-name role="corresponding_author">r.hascorrespondingauthor.source</node-name>
                    <node-name role="co_senior_author">r.hascoseniorauthor.source</node-name>
                    <node-name role="first_author">r.hasfirstauthor.source</node-name>
                    <node-name role="last_author">r.haslastauthor.source</node-name>
                    <node-name role="senior_author">r.hasseniorauthor.source</node-name>
                    <node-name role="thesis_advisor">r.hasadvisor.source</node-name>
                    <node-name role="main_supervisor">r.hasmainsupervisor.source</node-name>
                    <node-name role="co_supervisor">r.hascosupervisor.source</node-name>
                </node-names>
            </xsl:variable>
            <xsl:for-each select="$node_names/node-names/node-name">
                <xsl:call-template name="author_assoc">                        
                    <xsl:with-param name="node_name">
                        <xsl:copy-of select="."/>
                    </xsl:with-param>
                    <xsl:with-param name="solr_node">
                        <xsl:copy-of select="$obj_doc"/>
                    </xsl:with-param>
                </xsl:call-template>                            
            </xsl:for-each>
        </internal-person-list>
        <author-list>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='r.isbelongsto.source']"/>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='r.hasauthor.source']"/>
        </author-list>
        <pages>
            <start-page>
                <xsl:value-of select="$obj_doc/doc/str[@name='m.host.page.start']"/>                
            </start-page>
            <end-page>
                <xsl:value-of select="$obj_doc/doc/str[@name='m.host.page.end']"/>                
            </end-page>
        </pages>
        <article-number>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.identifier.articlenumber']"/>
        </article-number>
        <volume>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.host.volume']"/>                
        </volume>
        <issue>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.host.issue']"/>                
        </issue>
        <peer-review>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.ispeerreviewed']"/>                
        </peer-review>
        <journal-title>
            <xsl:value-of select="concat($obj_doc/doc/str[@name='m.host.title'], '|', $obj_doc/doc/str[@name='m.host.title.abbreviated'])"/>                
        </journal-title>
        <print-issn>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.host.identifier.issn']"/>
        </print-issn>
    </xsl:template>
    
    <xsl:template match="arr[@name='m.topic']/str">
        <keyword>
            <xsl:value-of select="normalize-space(.)"/>
        </keyword>
    </xsl:template>
    
    <xsl:template match="arr[@name='m.preceding.source']/str">
        <url>
            <xsl:value-of select="tokenize(., '\|\|')[2]"/>
        </url>
    </xsl:template>
        
    <xsl:template match="arr[@name='m.note.general']">
        <xsl:for-each select="str">
            <xsl:variable name="count">
                <xsl:number/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$count &gt; 1">
                    <xsl:value-of select="concat('; ', normalize-space(.))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="normalize-space(.)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>    

    <xsl:template match="arr[@name='r.isbelongstoorg.source']">
        <xsl:for-each select="str">
            <owner>
                <xsl:value-of select="normalize-space(.)"></xsl:value-of>
            </owner>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="author_assoc">
        <xsl:param name="node_name"/>
        <xsl:param name="solr_node"/>
        <xsl:for-each select="$solr_node/doc/arr[@name=$node_name/node-name]/str">
            <internalperson>
                <id>
                    <xsl:value-of select="tokenize(., '\|\|')[3]"/>                
                </id>                                        
                <role>
                    <xsl:value-of select="$node_name/node-name/@role"></xsl:value-of>
                </role>
            </internalperson>            
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="arr[@name='r.hasauthor.source'] | arr[@name='r.isbelongsto.source']">
        <xsl:for-each select="str">
            <author>
                <author-first-name>
                    <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[2]"/>
                </author-first-name>
                <author-last-name>
                    <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[1]"/>
                </author-last-name>
                <role>author</role>
            </author>
        </xsl:for-each>
    </xsl:template>    

    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>
    
</xsl:stylesheet>
