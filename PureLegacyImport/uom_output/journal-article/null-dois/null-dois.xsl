<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <xsl:result-document href="null-dois.xml">
            <records xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:noNamespaceSchemaLocation="null-dois-schema.xsd">
            <xsl:apply-templates select="//record[doi='']"/>
        </records>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="record">
        <record>
            <xsl:copy-of select="ids/escholar-id"/>
        </record>
    </xsl:template>
    
</xsl:stylesheet>