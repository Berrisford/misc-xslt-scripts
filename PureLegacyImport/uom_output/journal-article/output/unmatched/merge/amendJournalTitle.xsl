<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output indent="yes" method="xml"/>
    
    <xsl:template match ="/"> 
        <xsl:apply-templates/> 
    </xsl:template>

    <xsl:template match="journal-title">
        <journal-title>
            <xsl:value-of select="substring-before(., '|')"/>
        </journal-title>
    </xsl:template>

    <!--<xsl:template match="*[name() != 'journal-title']" >-->
    <xsl:template match="*" >
            <xsl:element name="{name()}" >
            <xsl:apply-templates select ="@*"/> 
            <xsl:apply-templates/>
        </xsl:element> 
    </xsl:template> 
    
    <xsl:template match="@*" >
        <xsl:attribute name="{name()}" >
            <xsl:value-of select ="."/> 
        </xsl:attribute> 
    </xsl:template> 
    
    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>
    
</xsl:stylesheet>