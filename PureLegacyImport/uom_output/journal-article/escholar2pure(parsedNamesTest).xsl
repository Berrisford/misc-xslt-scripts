<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math" exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output indent="yes" method="xml"/>

    <xsl:template match="/">
        <records>
            <xsl:apply-templates select="//record"/>
        </records>
    </xsl:template>

    <xsl:template match="record">
        <xsl:if test="authors-as-a-string/author-string">
            <record>
                <escholar-id>
                    <xsl:value-of select="ids/escholar-id"/>
                </escholar-id>
                <xsl:apply-templates select="authors-as-a-string/author-string"/>
                <xsl:call-template name="name-parser">
                    <xsl:with-param name="name-string" select="authors-as-a-string/author-string"/>
                </xsl:call-template>
            </record>
        </xsl:if>
    </xsl:template>

    <xsl:template match="authors-as-a-string/author-string">
        <author-string>
            <xsl:value-of select="normalize-space(.)"/>
        </author-string>
    </xsl:template>

    <xsl:template match="text()">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>

    <xsl:template name="name-parser">
        <xsl:param name="name-string"/>
        <xsl:choose>
            <xsl:when test="contains($name-string, '; ') and contains($name-string, ', ')">
                <!-- BA: handle format: Moradian, S; Shahidsales, S; Nasiri, M.R.G; Pilling, M; Walshe, C; Molassiotis, A -->
                <xsl:for-each select="tokenize($name-string, '; ')">
                    <author>
                        <given-name>
                            <xsl:value-of select="tokenize(., ', ')[2]"/>
                        </given-name>
                        <surname>
                            <xsl:value-of select="tokenize(., ', ')[1]"/>
                        </surname>
                    </author>
                </xsl:for-each>
            </xsl:when>
            <!--               
                <xsl:when test="contains($name-string, ' and ')">
                    <!-\- BA: handle format: William J. Parnell and Carmen Calvo-Jurado -\->
                    <xsl:for-each select="tokenize($name-string, ' and ')">
                        <author>
                            <given-name><xsl:value-of select="normalize-space(substring-before(., tokenize(., ' ')[last()]))"/></given-name>
                            <surname><xsl:value-of select="tokenize(., ' ')[last()]"/></surname>
                        </author>
                    </xsl:for-each>                    
                </xsl:when>
-->
            <!-- Does the name contain a comma? -->
            <xsl:when test="contains($name-string,',')">
                <!-- Yes, assume all before the comma is the surname -->
                <xsl:choose>
                    <!--<xsl:when test="contains($name-string, ', ') and count(tokenize($name-string, '.')) &lt; 2">-->                       
                    <xsl:when test="contains($name-string, ', ') and count(tokenize($name-string, ', ')) &lt; 2">                       
                            <!-- BA: handle format: Cheong JL, Warren R, Parslew RAG, Bucknell R. -->
                        <xsl:for-each select="tokenize($name-string, ', ')">
                            <xsl:choose>
                                <xsl:when test="string-length(tokenize(., ' ')[2]) &gt; 2 and string-length(tokenize(., ' ')[1]) &lt;= 1">
                                    <given-name>
                                        <xsl:value-of select="tokenize(., ' ')[1]"/>
                                    </given-name>
                                    <surname>
                                        <xsl:value-of select="tokenize(., ' ')[2]"/>
                                    </surname>                                    
                                </xsl:when>
                                <xsl:otherwise>
                                    <author>
                                        <given-name>
                                            <xsl:value-of select="tokenize(., ' ')[2]"/>
                                            <!--<xsl:value-of select="normalize-space(substring-before(., tokenize(., ' ')[last()]))"/>-->
                                        </given-name>
                                        <surname>
                                            <!--<xsl:value-of select="tokenize(., ' ')[last()]"/>-->
                                            <xsl:value-of select="tokenize(., ' ')[1]"/>
                                        </surname>
                                    </author>                                    
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each>
                    </xsl:when>
                    
                    <!--<xsl:when test="count(tokenize($name-string, '.')) &gt; 2 and contains($name-string, ', ') ">-->
                    <xsl:when test="contains($name-string, ', ') and contains($name-string, '. ')">                        
                        <!-- BA: handle format: R. B. Sparkes, A. Dog&#x9f;rul Selver, J. Bischoff, H. M. Talbot, O&#x96;. Gustafsson, I. P. Semiletov, O. V. Dudarev, and B. E. van Dongen -->
                        <xsl:for-each select="tokenize($name-string, ', ')">
                            <author>
                                <given-name>
                                    <xsl:value-of select="normalize-space(substring-before(., tokenize(., ' ')[last()]))"/>
                                </given-name>
                                <surname>
                                    <xsl:value-of select="tokenize(., ' ')[last()]"/>
                                </surname>
                            </author>
                        </xsl:for-each>
                    </xsl:when>

                    <xsl:when test="count(tokenize($name-string, ', ')) = 2">
                        <author>
                            <given-name>
                                <xsl:value-of select="tokenize($name-string, ', ')[2]"/>
                            </given-name>
                            <surname>
                                <xsl:value-of select="tokenize($name-string, ', ')[1]"/>
                            </surname>
                        </author>
                    </xsl:when>


                    <!-- Does all after the comma (and space) contain a space? -->
                    <xsl:when test="contains(substring-after($name-string,', '),' ')">
                        <!-- Yes, assume that there is a middle name. Strip out any periods. -->
                        <author>
                            <given-name>
                            <xsl:value-of
                                select="translate(substring-before(substring-after($name-string,', '),' '),'.','')"
                            />
                        </given-name>
                        <given-name>
                            <xsl:value-of
                                select="translate(substring-after(substring-after($name-string,', '),' '),'.','')"
                            />
                        </given-name>
                        </author>
                    </xsl:when>
                    <xsl:otherwise>
                        <!-- No, does the string contain any periods? -->
                        <xsl:choose>
                            <xsl:when test="contains(.,'.')">
                                <!-- Yes, assume initials separated with periods -->
                                <xsl:variable name="initials"
                                    select="translate(substring-after($name-string,', '),'.','')"/>
                                <xsl:choose>
                                    <xsl:when test="string-length($initials) = 2">
                                        <author>
                                            <given-name>
                                            <xsl:value-of select="substring($initials,1,1)"/>
                                        </given-name>
                                        <given-name>
                                            <xsl:value-of select="substring($initials,2,2)"/>
                                        </given-name>
                                        </author>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <author>
                                            <given-name>
                                            <xsl:value-of select="$initials"/>
                                        </given-name>
                                        </author>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <!-- No, assume just a first name. Strip out any periods. -->
                                <author>
                                    <given-name>
                                    <xsl:value-of
                                        select="translate(substring-after($name-string,', '),'.','')"
                                    />
                                </given-name>
                                </author>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
                <!--                    
                    <surname>
                        <xsl:value-of select="substring-before($name-string,',')"/>
                    </surname>
-->
            </xsl:when>
            <xsl:otherwise>
                <!-- No, there is no comma -->
                <xsl:choose>
                    <xsl:when test="contains($name-string, ' and ')">
                        <!-- BA: handle format: William J. Parnell and Carmen Calvo-Jurado -->
                        <xsl:for-each select="tokenize($name-string, ' and ')">
                            <author>
                                <given-name>
                                    <xsl:value-of
                                        select="normalize-space(substring-before(., tokenize(., ' ')[last()]))"
                                    />
                                </given-name>
                                <surname>
                                    <xsl:value-of select="tokenize(., ' ')[last()]"/>
                                </surname>
                            </author>
                        </xsl:for-each>
                    </xsl:when>

                    <!-- Does it have any spaces? -->
                    <xsl:when test="contains($name-string,' ')">
                        <xsl:variable name="all-after-last-space"
                            select="tokenize($name-string,' ')[last()]"/>
                        <xsl:variable name="second-string-after-first-space"
                            select="tokenize($name-string,' ')[2]"/>
                        <xsl:variable name="name-length" select="string-length($name-string)"/>
                        <xsl:choose>
                            <xsl:when test="count(tokenize($name-string, ' ')) = 2">
                                <xsl:choose>
                                    <xsl:when test="string-length(tokenize($name-string, ' ')[2]) = 1">
                                        <author>
                                            <given-name>
                                                <xsl:value-of select="tokenize($name-string, ' ')[2]"/>
                                            </given-name>
                                            <surname>
                                                <xsl:value-of select="tokenize($name-string, ' ')[1]"/>
                                            </surname>
                                        </author>                                        
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <author>
                                            <given-name>
                                                <xsl:value-of select="tokenize($name-string, ' ')[1]"/>
                                            </given-name>
                                            <surname>
                                                <xsl:value-of select="tokenize($name-string, ' ')[2]"/>
                                            </surname>
                                        </author>                                        
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            
                            <!-- Yes, does all after the last space have a length of 1 (is it an initial) ? -->
                            <xsl:when test="string-length($all-after-last-space) = 1">
                                <!-- Yes, assume all before the last space is the surname and after is the initial. Strip out any periods. -->
                                <author>
                                    <given-name>
                                        <xsl:value-of
                                            select="translate($all-after-last-space,'.','')"/>
                                    </given-name>
                                    <given-name>
                                        <xsl:value-of
                                            select="substring($name-string,1,$name-length - 2)"/>
                                    </given-name>
                                </author>
                            </xsl:when>
                            <!-- Yes, does all after the last space have a length of 2 (is it two letters concatenated) ? -->
                            <xsl:when test="string-length($all-after-last-space) = 2">
                                <xsl:variable name="second-initial"
                                    select="substring($all-after-last-space,2)"/>
                                <!-- Is the second letter upper case? -->
                                <xsl:analyze-string select="$second-initial" regex="[A-Z]">
                                    <!-- Yes, assume two initials -->
                                    <xsl:matching-substring>
                                        <author>
                                            <given-name>
                                                <xsl:value-of
                                                  select="substring($all-after-last-space,1,1)"/>
                                            </given-name>
                                            <given-name>
                                                <xsl:value-of
                                                  select="substring($all-after-last-space,2)"/>
                                            </given-name>
                                            <surname>
                                                <xsl:value-of
                                                  select="normalize-space(substring-before($name-string,$all-after-last-space))"
                                                />
                                            </surname>
                                        </author>
                                    </xsl:matching-substring>
                                    <!-- No, we must assume the second letter forms the second letter of a first name of length 2 -->
                                    <xsl:non-matching-substring>
                                        <author>
                                            <given-name>
                                                <xsl:value-of select="$all-after-last-space"/>
                                            </given-name>
                                            <surname>
                                                <xsl:value-of
                                                  select="substring($name-string,1,$name-length - 3)"
                                                />
                                            </surname>
                                        </author>
                                    </xsl:non-matching-substring>
                                </xsl:analyze-string>
                            </xsl:when>
                            <!-- Yes, does the second string after the first space have a length of 1 (is it a second name initial) ? -->
                            <xsl:when test="string-length($second-string-after-first-space) = 1">
                                <author>
                                    <given-name>
                                        <xsl:value-of select="substring-before($name-string,' ')"/>
                                    </given-name>
                                    <given-name>
                                        <xsl:value-of select="$second-string-after-first-space"/>
                                    </given-name>
                                    <surname>
                                        <xsl:value-of
                                            select="normalize-space(substring-after($name-string,$second-string-after-first-space))"
                                        />
                                    </surname>
                                </author>
                            </xsl:when>
                            <xsl:otherwise>
                                <!-- No, assume all before the first space is a firstname and the rest is lastname. Strip out any periods. -->
                                <author>
                                    <given-name>
                                        <xsl:value-of
                                            select="translate(substring-before($name-string,' '),'.','')"
                                        />
                                    </given-name>
                                    <surname>
                                        <xsl:value-of
                                            select="normalize-space(translate(substring-after($name-string,' '),'.',''))"
                                        />
                                    </surname>
                                </author>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <!-- No, assume single string surname. -->
                        <!--                            
                            <surname>
                                <xsl:value-of select="$name-string"/>
                            </surname>
-->
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
