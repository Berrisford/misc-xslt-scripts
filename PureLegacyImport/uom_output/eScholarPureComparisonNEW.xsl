<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <!--<xsl:result-document href="eScholarPureComparisonsNEW.xml">-->
            <records>
                <xsl:for-each select="//record">
                    <record>
                        <escholar-id><xsl:value-of select="escholar-id"/></escholar-id>
                        <content-type><xsl:value-of select="content-type"/></content-type>
                        <created-date><xsl:value-of select="created-date"/></created-date>
                        <visibility><xsl:value-of select="visibility"/></visibility>
                        <year><xsl:value-of select="year"/></year>
                        <file-attached><xsl:value-of select="file-attached"/></file-attached>
                        <xsl:for-each select="owners/owner">
                            <xsl:variable name="count">
                                <xsl:number/>
                            </xsl:variable>
                            <xsl:element name="{concat('display-name-', $count)}"><xsl:value-of select="display-name"></xsl:value-of></xsl:element>
                            <xsl:element name="{concat('spot-id-', $count)}"><xsl:value-of select="spot-id"></xsl:value-of></xsl:element>
                        </xsl:for-each>
                    </record>
                </xsl:for-each>
            </records>
        <!--</xsl:result-document>-->
    </xsl:template>
    
</xsl:stylesheet>