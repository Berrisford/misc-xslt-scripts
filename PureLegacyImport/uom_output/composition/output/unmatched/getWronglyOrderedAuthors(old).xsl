<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <records>
            <xsl:apply-templates select="//record[exists(authors-as-a-string/author-string)]"/>
        </records>
    </xsl:template>
    
    <xsl:template match="record[exists(authors-as-a-string/author-string)]">
        <record>
            <pid><xsl:value-of select="ids/escholar-id"/></pid>
            <authors><xsl:value-of select="authors-as-a-string/author-string[following-sibling::role[1]/text()='author']/text()"></xsl:value-of></authors>
            <year><xsl:value-of select="publication-dates/publication-date[1]/year"></xsl:value-of></year>
        </record>
    </xsl:template>
    
</xsl:stylesheet>