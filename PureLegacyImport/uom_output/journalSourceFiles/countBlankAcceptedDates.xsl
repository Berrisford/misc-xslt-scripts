<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	exclude-result-prefixes="xs"
	version="2.0">
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<totals>
			<xsl:for-each select="/records/record/publication-status-dates">
				<xsl:choose>
					<xsl:when test="date[@status='accepted'] = ''">
						<xsl:variable name="count">
							<xsl:number/>
						</xsl:variable>
						<count>
							<xsl:value-of select="$count"></xsl:value-of>
						</count>						
					</xsl:when>
					<xsl:when test="date[@status='submitted'] = ''">
						<xsl:variable name="count">
							<xsl:number/>
						</xsl:variable>
						<count>
							<xsl:value-of select="$count"></xsl:value-of>
						</count>						
					</xsl:when>					
				</xsl:choose>
			</xsl:for-each>
			<!--<xsl:value-of select="count(//date[@status='accepted'] = '')"/>-->
<!--			
			<totalpid>
				<xsl:value-of select="count(distinct-values(add/doc/field[@name='PID']))"/>
			</totalpid>
-->			
		</totals>
	</xsl:template>
</xsl:stylesheet>