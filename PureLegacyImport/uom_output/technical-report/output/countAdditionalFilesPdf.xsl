<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	exclude-result-prefixes="xs"
	version="3.0">
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<files-with-attachments>
		<count>
			<xsl:try>
				<xsl:value-of select="count(/records/record/additional-files[contains(url[1]/text(), 'FULL-TEXT.PDF')] 
					| /records/record/additional-files[contains(url[2]/text(), 'FULL-TEXT.PDF')] 
					| /records/record/additional-files[contains(url[3]/text(), 'FULL-TEXT.PDF')] 
					| /records/record/additional-files[contains(url[4]/text(), 'FULL-TEXT.PDF')] 
					| /records/record/additional-files[contains(url[5]/text(), 'FULL-TEXT.PDF')] 
					| /records/record/additional-files[contains(url[6]/text(), 'FULL-TEXT.PDF')] 
					| /records/record/additional-files[contains(url[7]/text(), 'FULL-TEXT.PDF')] 
					| /records/record/additional-files[contains(url[8]/text(), 'FULL-TEXT.PDF')] 
					| /records/record/additional-files[contains(url[9]/text(), 'FULL-TEXT.PDF')] 
					| /records/record/additional-files[contains(url[10]/text(), 'FULL-TEXT.PDF')] 
					| /records/record/additional-files[contains(url[11]/text(), 'FULL-TEXT.PDF')] 
					| /records/record/additional-files[contains(url[12]/text(), 'FULL-TEXT.PDF')] 
					| /records/record/additional-files[contains(url[13]/text(), 'FULL-TEXT.PDF')])"/>				
			<xsl:catch>				
			</xsl:catch>
			</xsl:try>	
		</count>
		</files-with-attachments>
	</xsl:template>
</xsl:stylesheet>