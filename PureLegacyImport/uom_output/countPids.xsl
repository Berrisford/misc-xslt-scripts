<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	exclude-result-prefixes="xs"
	version="2.0">
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<totals>
		<total-records>
		<xsl:value-of select="count(//pid)"/>
		</total-records>
		</totals>
	</xsl:template>
</xsl:stylesheet>