<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:math="http://www.w3.org/2005/xpath-functions/math" xmlns:ait="http://www.elsevier.com/xml/ani/ait" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xoe="http://www.elsevier.com/xml/xoe/dtd" xmlns:ce="http://www.elsevier.com/xml/ani/common" xmlns:cto="http://www.elsevier.com/xml/cto/dtd" xmlns:xocs="http://www.elsevier.com/xml/xocs/dtd" exclude-result-prefixes="xs math ait xsi xoe ce cto xocs" version="3.0">
    <xsl:output indent="yes" method="xml" />

    <!-- BA: This file iterates across all scopus matched files and generates the requisite Pure specified XML -->

    <xsl:variable name="root_uri" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/scw/select/'" />
    <xsl:variable name="person_uri" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/nonscw/select/'" />

    <xsl:variable name="obj_structured_keywords">
        <xsl:copy-of select="doc('structured-keywords.xml')/keywords" />
    </xsl:variable>

    <xsl:variable name="obj_open_licences">
        <xsl:copy-of select="doc('open-licences.xml')/licences" />
    </xsl:variable>

    <xsl:variable name="node_names">
        <node-names>
            <node-name role="author">r.isbelongsto.source</node-name>
            <node-name role="author">r.hasauthor.source</node-name>
            <node-name role="author">r.hascoauthor.source</node-name>
            <node-name role="author">r.hascorrespondingauthor.source</node-name>
            <node-name role="author">r.hascoseniorauthor.source</node-name>
            <node-name role="author">r.hasfirstauthor.source</node-name>
            <node-name role="author">r.haslastauthor.source</node-name>
            <node-name role="author">r.hasseniorauthor.source</node-name>
        </node-names>
    </xsl:variable>

    <xsl:variable name="pure_types">
        <pure-types>
            <pure-type type="original work">article</pure-type>
            <pure-type type="editorial">editorial</pure-type>
            <pure-type type="note">comment-debate</pure-type>
            <pure-type type="abstract">comment-debate</pure-type>
            <pure-type type="letter">letter</pure-type>
            <pure-type type="review">article</pure-type>
            <pure-type type="original research">article</pure-type>
            <pure-type type="review article">article</pure-type>
            <pure-type type="">article</pure-type>
        </pure-types>
    </xsl:variable>

    <!-- BA: 01/12/15 - Ignore matched abcde items and items belonging to me and NG etc... - start  -->                
    <xsl:variable name="excluded_pids">
        <xsl:copy-of select="doc('../excluded_pids.xml')/excluded_pids"/>
    </xsl:variable>
    <!-- BA: 01/12/15 - Ignore matched abcde items and items belonging to me and NG etc... - end  -->                
                
    <!-- BA: 01/12/15 - Ignore matched abcde items and items belonging to me and NG etc... - start  -->                    
    <xsl:template name="test_exclusion">
        <xsl:param name="doc"/>
        <result>
            <!--<xsl:copy-of select="$doc"/>-->
            <xsl:for-each select="$doc/doc/arr[@name='r.isbelongsto.pid']/str">
                <xsl:variable name="pid" select="."/>
                <xsl:choose>
                    <xsl:when test="exists($excluded_pids/excluded_pids/pid[@id = $pid])">
                        <xsl:value-of select="'True'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'False'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </result>
    </xsl:template>
    
    <xsl:template name="test_real_person">
        <!-- BA: Test to exclude items owned only by abcde persons -->
        <xsl:param name="doc"/>
        <result>
            <xsl:for-each select="$doc/doc/arr[@name='r.isbelongsto.pid']/str">
                <xsl:variable name="pid" select="."/>
                <xsl:choose>
                    <xsl:when test="not(contains($pid, 'abcde'))">
                        <xsl:value-of select="'True'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'False'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </result>
    </xsl:template>
    <!-- BA: 01/12/15 - Ignore matched abcde items and items belonging to me and NG etc... - end  -->                
    

    <xsl:template match="/">
        <records>
            <!--<org-test><xsl:value-of select="$obj_orgs/result/doc[last()]/str[@name='PID']"/></org-test>-->
            <xsl:apply-templates select="files" />
        </records>
    </xsl:template>

    <xsl:template match="files">
        <xsl:for-each select="file">
            <xsl:variable name="pid" select="replace(tokenize(., '/')[last()], '_', '?')" />
            <xsl:variable name="query" select="concat($root_uri, '?q=PID%3A', replace($pid, ':', '?'))" />
            <xsl:variable name="obj_solr_doc">
                <xsl:copy-of select="doc($query)/response/result/doc" />
            </xsl:variable>
            <xsl:variable name="scopus_abstract_path" select="concat(tokenize(., '/')[last()], '/', 'abstract-citations-response_scopus_data_20141101.xml')" />
            <xsl:variable name="obj_scopus_abstract">
                <xsl:copy-of select="doc($scopus_abstract_path)" />
            </xsl:variable>
            <xsl:variable name="scopus_id" select="$obj_scopus_abstract/abstract-citations-response/identifier-legend/identifier/scopus_id" />
            <xsl:variable name="scopus_file_path" select="concat(tokenize(., '/')[last()], '/2-s2.0-', $scopus_id, '.xml')" />
            <xsl:variable name="obj_scopus_file">
                <xsl:copy-of select="doc($scopus_file_path)" />
            </xsl:variable>

            <xsl:variable name="solr_title">
                <xsl:choose>
                    <xsl:when test="ends-with($obj_solr_doc/doc/str[@name='m.title'], '.')">
                        <!--<xsl:when test="substring($obj_solr/doc/str[@name='m.title'], string-length($obj_solr/doc/str[@name='m.title']), string-length($obj_solr/doc/str[@name='m.title'])) = '.'">-->
                        <xsl:value-of select="substring(normalize-space(lower-case($obj_solr_doc/doc/str[@name='m.title'])), 0, string-length($obj_solr_doc/doc/str[@name='m.title']))" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="normalize-space(lower-case($obj_solr_doc/doc/str[@name='m.title']))" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <xsl:variable name="scopus_title">
                <xsl:for-each select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/citation-title/titletext">
                    <xsl:choose>
                        <xsl:when test="ends-with(., '.')">
                            <xsl:variable name="tmp_scopus_title">
                                <xsl:value-of select="substring(normalize-space(lower-case(.)), 0, string-length(.))" />
                                <!--<xsl:value-of select="substring(normalize-space(.), 0, string-length(.))"/>-->
                            </xsl:variable>
                            <xsl:choose>
                                <xsl:when test="$tmp_scopus_title = $solr_title">
                                    <xsl:value-of select="$tmp_scopus_title" />
                                </xsl:when>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:when test="not(ends-with(., '.'))">
                            <xsl:variable name="tmp_scopus_title">
                                <xsl:value-of select="normalize-space(lower-case(.))" />
                                <!--<xsl:value-of select="normalize-space(.)"/>-->
                            </xsl:variable>
                            <xsl:choose>
                                <xsl:when test="$tmp_scopus_title = $solr_title">
                                    <xsl:value-of select="$tmp_scopus_title" />
                                </xsl:when>
                            </xsl:choose>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:variable>

            <xsl:variable name="scopus_title_cased">
                <xsl:for-each select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head/citation-title/titletext">
                    <xsl:choose>
                        <xsl:when test="ends-with(., '.')">
                            <xsl:variable name="tmp_scopus_title">
                                <xsl:value-of select="substring(normalize-space(lower-case(.)), 0, string-length(.))" />
                                <!--<xsl:value-of select="substring(normalize-space(.), 0, string-length(.))"/>-->
                            </xsl:variable>
                            <xsl:choose>
                                <xsl:when test="$tmp_scopus_title = $solr_title">
                                    <xsl:value-of select="." />
                                </xsl:when>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:when test="not(ends-with(., '.'))">
                            <xsl:variable name="tmp_scopus_title">
                                <xsl:value-of select="normalize-space(lower-case(.))" />
                                <!--<xsl:value-of select="normalize-space(.)"/>-->
                            </xsl:variable>
                            <xsl:choose>
                                <xsl:when test="$tmp_scopus_title = $solr_title">
                                    <xsl:value-of select="." />
                                </xsl:when>
                            </xsl:choose>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:variable>

            <xsl:variable name="excluded">
                <xsl:call-template name="test_exclusion">
                    <xsl:with-param name="doc">
                        <xsl:copy-of select="$obj_solr_doc"/>
                    </xsl:with-param>    
                </xsl:call-template>                        
            </xsl:variable>
            
            <xsl:variable name="real_person">
                <xsl:call-template name="test_real_person">
                    <xsl:with-param name="doc">
                        <xsl:copy-of select="$obj_solr_doc"/>
                    </xsl:with-param>    
                </xsl:call-template>                        
            </xsl:variable>                    
            
            <xsl:variable name="item_match">
                <xsl:choose>
                    <xsl:when test="$obj_scopus_file/xocs:doc/xocs:meta/xocs:doi and $obj_solr_doc/doc/str[@name='m.identifier.doi']">
                        <xsl:if test="$obj_scopus_file/xocs:doc/xocs:meta/xocs:doi = $obj_solr_doc/doc/str[@name='m.identifier.doi']">
                            <xsl:value-of select="'true'" />
                        </xsl:if>
                    </xsl:when>
                    <xsl:when test="not($obj_scopus_file/xocs:doc/xocs:meta/xocs:doi) or not($obj_solr_doc/doc/str[@name='m.identifier.doi'])">
                        <xsl:choose>
                            <xsl:when test="$scopus_title = $solr_title">
                                <xsl:value-of select="'true'" />
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="'false'" />
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'false'" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <!-- BA: This template is for MATCHED Conference Papers and others -->
                <xsl:choose>
                    <xsl:when test="not(contains($excluded, 'True')) and contains($real_person, 'True') and contains($obj_solr_doc/doc/str[@name='m.genre.version'], 'Conference paper') or contains($obj_solr_doc/doc/str[@name='m.genre.version'], 'full-paper') or not($obj_solr_doc/doc/str[@name='m.genre.version'])">                            
                        <valid>
                            <valid-escholar-id>
                                <xsl:value-of select="replace(tokenize(., '/')[last()], '_', ':')" />
                            </valid-escholar-id>                            
                        </valid>
                    </xsl:when>
                    <xsl:otherwise>
                        <excluded-pid>                            
                            <excluded-escholar-id>
                                <xsl:value-of select="replace(tokenize(., '/')[last()], '_', ':')" />
                            </excluded-escholar-id>
                            <xsl:copy-of select="$obj_solr_doc/doc/arr[@name='r.isbelongsto.source']"/>                            
                            <excluded-item>
                                <xsl:value-of select="concat('not(contains($excluded, True)) = ', not(contains($excluded, 'True')))"/>
                            </excluded-item>
                            <real_person>
                                <xsl:value-of select="concat('contains($real_person, True) = ', contains($real_person, 'True'))"/>
                            </real_person>
                            <conference-paper>
                                <xsl:value-of select="concat('contains($obj_solr_doc/doc/str[@name=m.genre.version], Conference paper) = ', contains($obj_solr_doc/doc/str[@name='m.genre.version'], 'Conference paper'))"/>
                            </conference-paper>
                            <full-paper>
                                <xsl:value-of select="concat('contains($obj_solr_doc/doc/str[@name=m.genre.version], Full paper) = ', contains($obj_solr_doc/doc/str[@name='m.genre.version'], 'full-paper'))"/>
                            </full-paper>
                            <none>
                                <xsl:value-of select="concat('not($obj_solr_doc/doc/str[@name=m.genre.version]) = ', not($obj_solr_doc/doc/str[@name='m.genre.version']))"/>
                            </none>                            
                        </excluded-pid>
                        
                    </xsl:otherwise>
                </xsl:choose>                        
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="abstract">
        <abstract>
            <xsl:value-of select="normalize-space(.)" />
        </abstract>
    </xsl:template>

    <xsl:template match="author-keyword | arr[@name='m.topic']/str | classification">
        <keyword>
            <xsl:value-of select="normalize-space(.)" />
        </keyword>
    </xsl:template>

    <xsl:template match="head">
        <xsl:for-each select="author-group">
            <xsl:variable name="count_author_group">
                <xsl:number />
            </xsl:variable>
            <xsl:for-each select="author">
                <xsl:variable name="count_author">
                    <xsl:number />
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="$count_author_group &gt; 1 or $count_author &gt; 1">
                        <xsl:value-of select="concat('; ', ce:indexed-name)" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="ce:indexed-name" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="head" mode="author-list">
        <author-list>
            <xsl:apply-templates select="author-group/author" mode="author-list" />
            <!--<xsl:apply-templates select="source/contributor-group/contributor" mode="author-list"/>-->
        </author-list>
    </xsl:template>

    <xsl:template match="author" mode="author-list">
        <author>
            <author-first-name>
                <xsl:value-of select="preferred-name/ce:given-name" />
            </author-first-name>
            <author-last-name>
                <xsl:value-of select="preferred-name/ce:surname" />
            </author-last-name>
            <role>author</role>
            <scopus-author-id>
                <xsl:value-of select="@auid" />
            </scopus-author-id>
            <scopus-author-sequence>
                <xsl:value-of select="@seq" />
            </scopus-author-sequence>
        </author>
    </xsl:template>

    <xsl:template match="website">
        <url>
            <xsl:value-of select="ce:e-address" />
        </url>
    </xsl:template>

    <xsl:template match="arr[@name='m.preceding.source']/str">
        <url>
            <xsl:value-of select="tokenize(., '\|\|')[2]" />
        </url>
    </xsl:template>

    <xsl:template match="arr[@name='m.preceding.identifier.uri']/str" mode="web-address">
        <web-address>
            <xsl:value-of select="normalize-space(.)" />
        </web-address>
    </xsl:template>
    
    <xsl:template match="arr[@name='m.preceding.identifier.uri']/str">
        <url>
            <xsl:value-of select="normalize-space(.)"/>
        </url>
    </xsl:template>    
    
    <xsl:template match="website" mode="web-address">
        <web-address>
            <xsl:value-of select="ce:e-address" />
        </web-address>
    </xsl:template>
    
    <xsl:template match="arr[@name='m.preceding.source']/str" mode="web-address">
        <web-address>
            <xsl:value-of select="tokenize(., '\|\|')[2]" />
        </web-address>
    </xsl:template>
    
    <xsl:template match="arr[@name='m.note.general']/str | arr[@name='m.note.funding']/str | str[@name='m.note.statementonresearchdata']">
        <bibliographical-note>
            <xsl:value-of select="normalize-space(.)" />
        </bibliographical-note>
    </xsl:template>

    <xsl:template name="get_authors">
        <xsl:param name="obj_doc" />
        <internal-persons>
            <xsl:for-each select="$node_names/node-names/node-name">
                <xsl:call-template name="author_assoc">
                    <xsl:with-param name="node_name">
                        <xsl:copy-of select="." />
                    </xsl:with-param>
                    <xsl:with-param name="solr_node">
                        <xsl:copy-of select="$obj_doc" />
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
        </internal-persons>
    </xsl:template>

    <xsl:template name="author_assoc">
        <xsl:param name="node_name" />
        <xsl:param name="solr_node" />
        <xsl:for-each select="$solr_node/doc/arr[@name=$node_name/node-name]/str">
            <xsl:if test="not(contains(., 'uk-ac-man-per:abcde'))">
                <internal-person>
                    <xsl:value-of select="concat(tokenize(., '\|\|')[3], '; ', $node_name/node-name/@role, '; ', tokenize(tokenize(., '\|\|')[1], ', ')[2], '; ', tokenize(tokenize(., '\|\|')[1], ', ')[1])" />
                </internal-person>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="titletext">
        <xsl:element name="title">
            <xsl:attribute name="original" select="@original" />
            <xsl:attribute name="xml:lang" select="@xml:lang" />
            <xsl:value-of select="normalize-space(.)" />
        </xsl:element>
    </xsl:template>

    <xsl:template match="str[@name='f.full-text.source']">
        <xsl:param name="pid" as="xs:string" />
        <attachment>
            <file-version>
                <xsl:value-of select="'publishersversion'" />
            </file-version>
            <file-url>
                <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?pid=', $pid, '&amp;dsid=', tokenize(., '\|\|')[2])" />
            </file-url>
        </attachment>
    </xsl:template>

    <!-- BA: 01/12/15 - change to address ambiguous template match error - start -->
    <xsl:template match="arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW-DOCUMENT.')] | arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS-DOCUMENT.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS-DOCUMENT.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS.')]">
        <xsl:param name="pid" as="xs:string"/>
        <attachment>
            <file-version>
                <xsl:choose>
                    <xsl:when test="contains(., 'PRE-PEER-REVIEW.') or contains(., 'PRE-PEER-REVIEW-DOCUMENT.')">
                        <xsl:value-of select="'preprint'"/>
                    </xsl:when>
                    <xsl:when test="contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS.') or contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS-DOCUMENT.')">
                        <xsl:value-of select="'authorsversion'"/>
                    </xsl:when>                    
                    <xsl:when test="contains(., 'POST-PEER-REVIEW-PUBLISHERS.') or contains(., 'POST-PEER-REVIEW-PUBLISHERS-DOCUMENT.')">
                        <xsl:value-of select="'publishersversion'"/>
                    </xsl:when>                    
                </xsl:choose>
            </file-version>
            <file-url>
                <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?pid=', $pid, '&amp;dsid=', tokenize(., '\|\|')[1])"/>
            </file-url>
        </attachment>
    </xsl:template>
    
    <xsl:template match="arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-1')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-2')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-3')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-4')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-5')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-6')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-7')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-8')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-9')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-10')]">
        <xsl:param name="pid" as="xs:string"/>
        <url>
            <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?pid=', $pid, '&amp;dsid=', tokenize(., '\|\|')[1])"/>            
        </url>
    </xsl:template>
    
    <xsl:template match="arr[@name='f.fileattached.source']/str[contains(., 'FULL-TEXT')]">
        <xsl:param name="pid" as="xs:string"/>
        <url>
            <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?pid=', $pid, '&amp;dsid=', tokenize(., '\|\|')[1])"/>            
        </url>
    </xsl:template>
    <!-- BA: 01/12/15 - change to address ambiguous template match error - end -->
    
    <xsl:template match="isbn">
        <isbn>
            <xsl:value-of select="." />
        </isbn>        
    </xsl:template>

    <xsl:template match="editor">
        <editor>
            <firstname>
                <xsl:choose>
                    <xsl:when test="ce:given-name">
                        <xsl:value-of select="ce:given-name"/>                        
                    </xsl:when>
                    <xsl:when test="ce:initials">
                        <xsl:value-of select="ce:initials"/>
                    </xsl:when>
                </xsl:choose>
            </firstname>
            <lastname>
                <xsl:value-of select="ce:surname"/>                
            </lastname>
        </editor>
    </xsl:template>

    <xsl:template match="text()">
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>

</xsl:stylesheet>
