echo Start time: %DATE% %TIME%
 java -Xms16000m -Xmx16000m -jar "D:\saxon\saxon9pe.jar" -s:newMergedUnmatched.xml -xsl:getWronglyOrderedAuthors.xsl -o:newWronglyOrderedAuthors.xml
echo End time: %DATE% %TIME%
pause