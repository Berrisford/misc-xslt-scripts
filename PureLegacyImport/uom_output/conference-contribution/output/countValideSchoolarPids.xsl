<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	exclude-result-prefixes="xs"
	version="2.0">
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<totals>
			<valid-escholar-pids>
				<xsl:value-of select="count(//valid-escholar-id)"/>
			</valid-escholar-pids>

			<excluded-pids>
				<xsl:value-of select="count(//excluded-pid)"/>
			</excluded-pids>
			<!--			
			<totalpid>
				<xsl:value-of select="count(distinct-values(add/doc/field[@name='PID']))"/>
			</totalpid>
-->			
		</totals>
	</xsl:template>
</xsl:stylesheet>