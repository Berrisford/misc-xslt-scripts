    
    <attachments>
        <!-- BA: 13/04/16 Change to include FULL-TEXT.PDF -->
        <xsl:apply-templates select="$obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW-DOCUMENT.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS-DOCUMENT.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS-DOCUMENT.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'FULL-TEXT.PDF')]">
            <!--<xsl:apply-templates select="$obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW-DOCUMENT.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS-DOCUMENT.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS-DOCUMENT.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS.')]">-->
            <!--<xsl:with-param name="pid" as="xs:string" select="replace(tokenize(., '/')[last()], '_', ':')"/>-->
            <xsl:with-param name="pid" as="xs:string" select="$obj_doc/doc/str[@name='PID']"/>                                
        </xsl:apply-templates>                                
    </attachments>
    
    <additional-files>
        <!-- BA: 13/04/16 Change to exclude FULL-TEXT.PDF -->
        <xsl:apply-templates select="$obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-1')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-2')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-3')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-4')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-5')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-6')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-7')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-8')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-9')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-10')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[not(contains(., 'FULL-TEXT.PDF'))]">
            <!--<xsl:with-param name="pid" as="xs:string" select="replace(tokenize(., '/')[last()], '_', ':')"/>-->
            <xsl:with-param name="pid" as="xs:string" select="$obj_doc/doc/str[@name='PID']"/>                
        </xsl:apply-templates>                                                                                    
        <!--            
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'FULL-TEXT')]">
                <!-\-<xsl:with-param name="pid" as="xs:string" select="replace(tokenize(., '/')[last()], '_', ':')"/>-\->
                <xsl:with-param name="pid" as="xs:string" select="$obj_doc/doc/str[@name='PID']"/>                
            </xsl:apply-templates>
            
            <!-\- BA: 04/03/16 - handle f.full-text.id - start -\->
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='f.full-text.id']/str">
                <xsl:with-param name="pid" as="xs:string" select="$obj_doc/doc/str[@name='PID']"/>                
            </xsl:apply-templates>                                                                                    
            <!-\- BA: 04/03/16 - handle f.full-text.id - end -\->            
-->            
    </additional-files>
    <!-- BA: 01/12/15 - change to address ambiguous template match error - end -->
    

    <!-- BA: 01/12/15 - change to address ambiguous template match error - start -->
    <!-- BA: 13/04/16 Change to include FULL-TEXT.PDF -->
    <xsl:template match="arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW-DOCUMENT.')] | arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS-DOCUMENT.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS-DOCUMENT.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS.')] | arr[@name='f.fileattached.source']/str[contains(., 'FULL-TEXT.PDF')]">
        <!--<xsl:template match="arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW-DOCUMENT.')] | arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS-DOCUMENT.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS-DOCUMENT.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS.')]">-->
        <xsl:param name="pid" as="xs:string"/>
        <attachment>
            <file-version>
                <xsl:choose>
                    <xsl:when test="contains(., 'PRE-PEER-REVIEW.') or contains(., 'PRE-PEER-REVIEW-DOCUMENT.')">
                        <xsl:value-of select="'preprint'"/>
                    </xsl:when>
                    <xsl:when test="contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS.') or contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS-DOCUMENT.')">
                        <xsl:value-of select="'authorsversion'"/>
                    </xsl:when>                    
                    <!-- BA: 13/04/16 Change to include FULL-TEXT.PDF -->
                    <xsl:when test="contains(., 'POST-PEER-REVIEW-PUBLISHERS.') or contains(., 'POST-PEER-REVIEW-PUBLISHERS-DOCUMENT.') or contains(., 'FULL-TEXT.PDF')">
                        <!--<xsl:when test="contains(., 'POST-PEER-REVIEW-PUBLISHERS.') or contains(., 'POST-PEER-REVIEW-PUBLISHERS-DOCUMENT.')">-->
                        <xsl:value-of select="'publishersversion'"/>
                    </xsl:when>                    
                </xsl:choose>
            </file-version>
            <file-url>
                <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?pid=', $pid, '&amp;dsid=', tokenize(., '\|\|')[1])"/>
            </file-url>
        </attachment>
    </xsl:template>
    
    <!-- BA: 13/04/16 Change to exclude FULL-TEXT.PDF -->
    <xsl:template match="arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-1')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-2')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-3')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-4')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-5')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-6')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-7')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-8')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-9')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-10')] | arr[@name='f.fileattached.source']/str[not(contains(., 'FULL-TEXT.PDF'))]">
        <!--<xsl:template match="arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-1')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-2')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-3')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-4')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-5')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-6')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-7')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-8')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-9')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-10')]">-->
        <xsl:param name="pid" as="xs:string"/>
        <url>
            <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?pid=', $pid, '&amp;dsid=', tokenize(., '\|\|')[1])"/>            
        </url>
    </xsl:template>
    
