<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">    
    <xsl:output indent="yes" method="xml"/>
    
    <!-- BA: This script iterates across all records (of a particular content type) in chunks of 1000 (rows) and checks to see if the record -->
    <!--  has been matched by Scopus. If not, it generates the requisite Pure xml entirely from eScolar data. Change $rows, $num_found, $obj_files -->
    <!--  accordingly to test quickly with small sets of data. Change $file_path to match where your files are located. -->
    
    <!--<xsl:variable name="root_url" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/scw/select/'"></xsl:variable>-->
    <xsl:variable name="root_url" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/scw/select/'" />
    
    <!-- BA: 04/04/16 - Change to include only newly created items -->        
    <xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con%5C%3A16+AND+r.isbelongsto.pid:uk*+AND+x.createddate%3A%5B2016-03-07T09%3A45%3A00Z+TO+NOW%5D', '&amp;fl=PID,r.isbelongsto.pid,x.state,m.genre.version,m.genre.form&amp;omitHeader=true')"></xsl:variable>        
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con%5C%3A16+AND+r.isbelongsto.pid:uk*', '&amp;fl=PID,r.isbelongsto.pid,x.state,m.genre.version,m.genre.form&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?17', '&amp;fl=PID,r.isbelongsto.pid,x.state&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?17', '&amp;fl=PID&amp;omitHeader=true&amp;start=0&amp;rows=0')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+m.preceding.source:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+r.hasauthor.source:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+m.host.title.abbreviated:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+m.genre.status:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+m.title.translated:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+m.note.authors:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=PID:', 'uk-ac-man-scw%5C%3A71283Berris', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=PID%3Auk-ac-man-scw%5C%3A202423&amp;fl=PID,r.isbelongsto.pid,x.state&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3Auk-ac-man-con%5C%3A17+AND+%28r.isbelongsto.pid%3Auk-ac-man-per%5C%3A109461+OR+r.isbelongsto.pid%3Auk-ac-man-per%5C%3A48079%29&amp;fl=PID,r.isbelongsto.pid,x.state&amp;omitHeader=true&amp;start=0')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+f.fileattached.source:%5B*+TO+*%5D+AND+PID%3Auk-ac-man-scw%5C%3A10*', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=PID:', 'uk-ac-man-scw%5C%3A267239', '&amp;fl=PID,r.isbelongsto.pid,x.state&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con%5C%3A2+AND+r.isbelongsto.pid:uk*+AND%0D%0Am.host.name.edt.source%3A%5B*+TO+*%5D', '&amp;fl=PID,r.isbelongsto.pid,x.state,m.genre.version&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A7+AND+m.otherformat.note.performers:%5B*+TO+*%5D', '&amp;fl=PID,r.isbelongsto.pid,x.state,m.genre.version,m.genre.form&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A16+AND+m.identifier.workingpapernumber:%5B*+TO+*%5D', '&amp;fl=PID,r.isbelongsto.pid,x.state,m.genre.version,m.genre.form&amp;omitHeader=true')"></xsl:variable>-->
    
    <xsl:variable name="rows" select="1000"/>
    <xsl:variable name="num_found">
        <xsl:value-of select="doc(concat($url, '&amp;start=0&amp;rows=0'))/response/result/@numFound"/>
        <!--<xsl:value-of select="doc(concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?17', '&amp;fl=PID&amp;omitHeader=true&amp;start=0&amp;rows=0'))/response/result/@numFound"/>-->
        <!--<xsl:value-of select="5"/>-->
    </xsl:variable>
    
    <!-- Retrieve the file document here. We can then just check this to see if a selected eScholar pid is in the document -->
    <xsl:variable name="file_path" select="'C:/PureLegacyImport/uom_output/working-paper/'"/>
    <xsl:variable name="obj_files">
        <xsl:copy-of select="doc('files.xml')"/>
    </xsl:variable>    

    <xsl:variable name="obj_structured_keywords">
        <xsl:copy-of select="doc('structured-keywords.xml')/keywords"/>
    </xsl:variable>
    
    <xsl:variable name="obj_open_licences">
        <xsl:copy-of select="doc('open-licences.xml')/licences"/>
    </xsl:variable>

    <xsl:variable name="node_names">
        <node-names>
            <node-name role="author">r.isbelongsto.source</node-name>
<!--            
            <node-name role="author">r.hasauthor.source</node-name>
            <node-name role="author">r.hascoauthor.source</node-name>
            <node-name role="author">r.hascorrespondingauthor.source</node-name>
            <node-name role="author">r.hascoseniorauthor.source</node-name>
            <node-name role="author">r.hasfirstauthor.source</node-name>
            <node-name role="author">r.haslastauthor.source</node-name>
            <node-name role="author">r.hasseniorauthor.source</node-name>
-->            
        </node-names>
    </xsl:variable>    

    <!-- BA: 01/12/15 - Ignore matched abcde items and items belonging to me and NG etc... - start  -->                
    <xsl:variable name="excluded_pids">
        <xsl:copy-of select="doc('../excluded_pids.xml')/excluded_pids"/>
    </xsl:variable>
    <!-- BA: 01/12/15 - Ignore matched abcde items and items belonging to me and NG etc... - end  -->                
    
    <xsl:variable name="pure_types">
        <pure-types>
            <pure-type type="book introduction">foreword-postscript</pure-type>
            <pure-type type="book chapter">chapter-peer-reviewed</pure-type>
            <pure-type type="book section">chapter-peer-reviewed</pure-type>
            <pure-type type="dictionary entry">entry-for-encyclopedia-dictionary</pure-type>
            <pure-type type="encyclopedia entry">entry-for-encyclopedia-dictionary</pure-type>
            <pure-type type="original work">chapter-peer-reviewed</pure-type>
            <pure-type type="section">chapter-peer-reviewed</pure-type>
            <pure-type type="">chapter-peer-reviewed</pure-type>
        </pure-types>
    </xsl:variable>
    
    <xsl:template match="/">
        <records>
        <xsl:call-template name="recurse">
            <xsl:with-param name="start" select="0"/>
            <xsl:with-param name="end" select="$rows"/>
        </xsl:call-template>
        </records>
    </xsl:template>
        
    <xsl:template name="recurse">
        <xsl:param name="start"/>
        <xsl:param name="end"/>
        <xsl:variable name="obj_response">
            <xsl:copy-of select="doc(concat($url, '&amp;start=', $start, '&amp;rows=', $rows))"/>
        </xsl:variable>
        <xsl:for-each select="$obj_response/response/result/doc">
            <xsl:variable name="filename" select="replace(str[@name='PID'], ':', '_')"/>
            <xsl:choose>
                <xsl:when test="exists($obj_files/files/file[. = concat($file_path, $filename)])">
                    <!-- ignore... -->
                    <!--<FOUND><xsl:value-of select="str"/></FOUND>-->
                </xsl:when>
                <xsl:otherwise>
                    <xsl:variable name="excluded">
                        <xsl:call-template name="test_exclusion">
                            <xsl:with-param name="doc">
                                <xsl:copy-of select="."/>
                            </xsl:with-param>    
                        </xsl:call-template>                        
                    </xsl:variable>
                    <xsl:variable name="real_person">
                        <xsl:call-template name="test_real_person">
                            <xsl:with-param name="doc">
                                <xsl:copy-of select="."/>
                            </xsl:with-param>    
                        </xsl:call-template>                        
                    </xsl:variable>                    
                    <xsl:if test="not(contains($excluded, 'True')) and contains($real_person, 'True')">
                        <!--<xsl:if test="not(contains($excluded, 'True')) and contains($real_person, 'True') and (contains(./str[@name='m.genre.version'], 'Conference paper') or contains(./str[@name='m.genre.version'], 'full-paper') or not(./str[@name='m.genre.version']))">-->
                        <!--<xsl:if test="not(contains($excluded, 'True')) or (contains($excluded, 'True') and str[@name='x.state'] = 'Active')">-->
                        <record>
                            <!--<PID><xsl:value-of select="str"/></PID>-->
                            <xsl:call-template name="unmatched">
                                <!--<xsl:with-param name="pid" select="replace(str, ':', '?')"/>-->
                                <xsl:with-param name="pid" select="replace(str[@name='PID'], ':', '?')"/>
                            </xsl:call-template>
                        </record>                        
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
        
        <xsl:if test="$end &lt; $num_found">
            <xsl:call-template name="recurse">
                <xsl:with-param name="start" select="$end + 1"/>
                <xsl:with-param name="end" select="$end + $rows"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="test_exclusion">
        <xsl:param name="doc"/>
        <result>
            <!--<xsl:copy-of select="$doc"/>-->
            <xsl:for-each select="$doc/doc/arr[@name='r.isbelongsto.pid']/str">
                <xsl:variable name="pid" select="."/>
                <xsl:choose>
                    <xsl:when test="exists($excluded_pids/excluded_pids/pid[@id = $pid])">
                        <xsl:value-of select="'True'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'False'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </result>
    </xsl:template>

    <xsl:template name="test_real_person">
        <!-- BA: Test to exclude items owned only by abcde persons -->
        <xsl:param name="doc"/>
        <result>
            <xsl:for-each select="$doc/doc/arr[@name='r.isbelongsto.pid']/str">
                <xsl:variable name="pid" select="."/>
                <xsl:choose>
                    <xsl:when test="not(contains($pid, 'abcde'))">
                        <xsl:value-of select="'True'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'False'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </result>
    </xsl:template>
    
    <xsl:template name="unmatched">
        <xsl:param name="pid"/>
        <xsl:variable name="obj_doc">
            <xsl:copy-of select="doc(concat($root_url, '?q=PID:', $pid, '&amp;omitHeader=true&amp;start=0&amp;rows=10'))/response/result/doc"/>            
        </xsl:variable>
                
        <ids>
            <!--<escholar-id><xsl:value-of select="$pid"/></escholar-id>-->
            <escholar-id><xsl:value-of select="$obj_doc/doc/str[@name='PID']"/></escholar-id>
            <scopus-id></scopus-id>
            <pubmed-id><xsl:value-of select="$obj_doc/doc/str[@name='m.identifier.pmid']"/></pubmed-id>                    
            <isi-accession-number><xsl:value-of select="$obj_doc/doc/str[@name='m.identifier.isiaccessionnumber']"/></isi-accession-number>                    
        </ids>
        <history>
            <created-date><xsl:value-of select="substring-before($obj_doc/doc/date[@name='x.createddate'], 'T')"/></created-date>
            <last-modified-date><xsl:value-of select="substring-before($obj_doc/doc/date[@name='x.lastmodifieddate'], 'T')"/></last-modified-date>
            <createdby><xsl:value-of select="tokenize($obj_doc/doc/arr[@name='r.iscreatedby.source']/str, '\|\|')[3]"/></createdby>
            <last-modifiedby><xsl:value-of select="tokenize($obj_doc/doc/arr[@name='r.islastmodifiedby.source']/str, '\|\|')[3]"/></last-modifiedby>
        </history>        
        <puretype>
            <xsl:value-of select="'working-paper'"/>
            <!--                        
                        <xsl:choose>
                            <xsl:when test="$obj_solr_doc/doc/str[@name='m.genre.version']">
                                <!-\-<xsl:variable name="genre" select="$obj_solr_doc/doc/str[@name='m.genre.version']"/>-\->
                                <!-\-<xsl:value-of select="$pure_types/pure-types/pure-type[@type = $genre]"/>-\->
                                <xsl:choose>
                                    <xsl:when test="$obj_solr_doc/doc/str[@name='m.genre.version'] = 'Authored book'">
                                        <xsl:value-of select="'book'"></xsl:value-of>
                                    </xsl:when>
                                    <xsl:when test="($obj_solr_doc/doc/str[@name='m.genre.version'] = 'Edited book' or $obj_solr_doc/doc/str[@name='m.genre.version'] = 'Editored book') and $obj_solr_doc/doc/str[@name='m.genre.form'] = 'Scholarly edition'">
                                        <xsl:value-of select="'scholarly-edition'"/>
                                    </xsl:when>
                                    <xsl:when test="($obj_solr_doc/doc/str[@name='m.genre.version'] = 'Edited book' or $obj_solr_doc/doc/str[@name='m.genre.version'] = 'Editored book') and $obj_solr_doc/doc/str[@name='m.genre.form'] ne 'Scholarly edition'">
                                        <xsl:value-of select="'anthology'"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="'book'"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="'book'"/>
                            </xsl:otherwise>
                        </xsl:choose>
-->                        
        </puretype>
        <publication-status>unpublished</publication-status>
        
        <publication-dates>
            <publication-date>
                <state>published</state>
                <xsl:try>
                    <year>
                        <xsl:try>
                            <xsl:choose>
                                <xsl:when test="tokenize($obj_doc/doc/str[@name='m.dateissued'], '-')[1] ne ''">
                                    <xsl:value-of select="tokenize($obj_doc/doc/str[@name='m.dateissued'], '-')[1]"/>                            
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="'2099'"/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:catch>
                                <xsl:value-of select="'2099'"/>                            
                            </xsl:catch>
                        </xsl:try>
                    </year>
                    <xsl:catch></xsl:catch>
                </xsl:try>                       
                <xsl:try>
                    <month>
                        <xsl:value-of select="tokenize($obj_doc/doc/str[@name='m.dateissued'], '-')[2]"/>
                    </month>
                    <xsl:catch></xsl:catch>
                </xsl:try>                       
                <xsl:try>
                    <day>
                        <xsl:value-of select="tokenize($obj_doc/doc/str[@name='m.dateissued'], '-')[3]"/>
                    </day>
                    <xsl:catch></xsl:catch>
                </xsl:try>                                    
            </publication-date>                
            <xsl:if test="$obj_doc/doc/str[@name='m.dateother.accepted']">
                <publication-date>
                    <state>accepted</state>
                    <year>
                        <xsl:value-of select="tokenize($obj_doc/doc/str[@name='m.dateother.accepted'], '-')[1]"></xsl:value-of>
                    </year>
                    <month>
                        <xsl:value-of select="tokenize($obj_doc/doc/str[@name='m.dateother.accepted'], '-')[2]"></xsl:value-of>
                    </month>
                    <day>
                        <xsl:value-of select="tokenize($obj_doc/doc/str[@name='m.dateother.accepted'], '-')[3]"></xsl:value-of>
                    </day>
                </publication-date>                
            </xsl:if>
            <xsl:if test="$obj_doc/doc/str[@name='m.dateother.submitted']">
                <publication-date>
                    <state>submitted</state>
                    <year>
                        <xsl:value-of select="tokenize($obj_doc/doc/str[@name='m.dateother.submitted'], '-')[1]"></xsl:value-of>
                    </year>
                    <month>
                        <xsl:value-of select="tokenize($obj_doc/doc/str[@name='m.dateother.submitted'], '-')[2]"></xsl:value-of>
                    </month>
                    <day>
                        <xsl:value-of select="tokenize($obj_doc/doc/str[@name='m.dateother.submitted'], '-')[3]"></xsl:value-of>
                    </day>
                </publication-date> 
            </xsl:if>
        </publication-dates> 
        
        <release-date><xsl:value-of select="$obj_doc/doc/str[@name='perm.releasedate']"/></release-date>
        <misc>
            <pubmed-central-deposit-version><xsl:value-of select="$obj_doc/doc/str[@name='m.note.pmcversion']"/></pubmed-central-deposit-version>
            <pubmed-central-deposit-date><xsl:value-of select="$obj_doc/doc/str[@name='m.dateother.pmcdeposit']"/></pubmed-central-deposit-date>
        </misc>        
        <original-language>
            <xsl:choose>
                <xsl:when test="$obj_doc/doc/str[@name='m.languageterm.code']">
                    <xsl:value-of select="substring($obj_doc/doc/str[@name='m.languageterm.code'], 1, 3)"/>
                </xsl:when>
                <xsl:when test="$obj_doc/doc/str[@name='t.language']">
                    <xsl:value-of select="$obj_doc/doc/str[@name='t.language']"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'eng'"/>
                </xsl:otherwise>
            </xsl:choose>
        </original-language>
        <titles>            
            <!--<xsl:apply-templates select="$obj_doc/doc/str[@name='m.title'] | $obj_doc/doc/str[@name='m.title.translated']"/>-->
            <xsl:apply-templates select="$obj_doc/doc/str[@name='m.title'] | $obj_doc/doc/str[@name='m.title.translated'] | $obj_doc/doc/str[@name='m.title.abbreviated']"/>            
        </titles>
        <sub-title>
            <xsl:value-of select="normalize-space($obj_doc/doc/str[@name='m.subtitle'])"/>
        </sub-title>        
        <number-of-pages>
            <xsl:choose>
                <xsl:when test="$obj_doc/doc/str[@name='m.page.total'] ne 'NaN'">
                    <xsl:value-of select="$obj_doc/doc/str[@name='m.page.total']"/>                                
                </xsl:when>                            
                <xsl:otherwise>
                    <xsl:value-of select="''"/>
                </xsl:otherwise>
            </xsl:choose>
        </number-of-pages>         
        <abstract>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.abstract']"/>
        </abstract>
        <keywords>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.topic']/str"/>
        </keywords>
        <structured-keywords>
            <xsl:for-each select="$obj_doc/doc/arr[@name='r.isbelongsto.source']/str[contains(., 'uk-ac-man-per:abcde')]">
                <xsl:variable name="pid" select="tokenize(., '\|\|')[2]"/>
                <structured-keyword>
                    <xsl:value-of select="$obj_structured_keywords/keywords/keyword[@PID = $pid]/uri"/>
                </structured-keyword>
            </xsl:for-each>
        </structured-keywords>                        
        <doi>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.identifier.doi']"/>
        </doi>

        <urls>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.preceding.source']/str"/>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.preceding.identifier.uri']/str"/>
        </urls>
        
        <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.note.general']/str  | $obj_doc/doc/arr[@name='m.note.funding']/str | $obj_doc/doc/str[@name='m.note.statementonresearchdata']"/>
        <!-- BA: additional bibliographic notes... -->
<!--        
        <xsl:if test="$obj_doc/doc/str[@name='m.publisher']">
            <bibliographical-note>
                <xsl:value-of select="concat('Issuing organisation: ', normalize-space($obj_doc/doc/str[@name='m.publisher']))"/>
            </bibliographical-note>
        </xsl:if>
        <xsl:if test="$obj_doc/doc/str[@name='m.host.title']">
            <bibliographical-note>
                <xsl:value-of select="concat('Published source: ', normalize-space($obj_doc/doc/str[@name='m.host.title']))"/>
            </bibliographical-note>
        </xsl:if>
        <xsl:if test="$obj_doc/doc/str[@name='m.identifier.patentapplicationnumber']">
            <bibliographical-note>
                <xsl:value-of select="concat('Application number: ', normalize-space($obj_doc/doc/str[@name='m.identifier.patentapplicationnumber']))"/>
            </bibliographical-note>
        </xsl:if>
        <xsl:if test="$obj_doc/doc/str[@name='m.placeterm.code']">
            <bibliographical-note>
                <xsl:value-of select="concat('Location issued: ', normalize-space($obj_doc/doc/str[@name='m.placeterm.code']))"/>
            </bibliographical-note>
        </xsl:if>        
-->

        <xsl:variable name="internal-persons">
            <xsl:call-template name="get_authors">
                <xsl:with-param name="obj_doc">
                    <xsl:copy-of select="$obj_doc/doc"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:variable>
        
        <internal-person-list>
            <xsl:for-each select="distinct-values($internal-persons/internal-persons/internal-person)">                                
                <internalperson>
                    <id><xsl:value-of select="tokenize(., '; ')[1]"/></id>
                    <role><xsl:value-of select="tokenize(., '; ')[2]"/></role>
                    <firstname><xsl:value-of select="tokenize(., '; ')[3]"/></firstname>
                    <lastname><xsl:value-of select="tokenize(., '; ')[4]"/></lastname>
                </internalperson>                                            
            </xsl:for-each>            
        </internal-person-list>
        
        <author-list>
            
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.name.aut.source']/str">
                <xsl:with-param name="role" select="'author'"/>
            </xsl:apply-templates>
<!--
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.name.pta.source']/str">
                <xsl:with-param name="role" select="'other'"/>
            </xsl:apply-templates>
-->
            
<!--
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.name.ths.source']/str">
                <xsl:with-param name="role" select="'supervisor'"/>
            </xsl:apply-templates>
-->            

<!--
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.host.name.cur.source']/str">
                <xsl:with-param name="role" select="'other'"/>
            </xsl:apply-templates>
-->            
            
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.name.trl.source']/str">
                <xsl:with-param name="role" select="'translator'"/>                
            </xsl:apply-templates>            
<!--            
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.name.edt.source']/str">
                <xsl:with-param name="role" select="'editor'"/>                
            </xsl:apply-templates>
-->
<!--            
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.host.name.edt.source']/str">
                <xsl:with-param name="role" select="'editor'"/>                
            </xsl:apply-templates>
-->            
        </author-list>
        
        <authors-as-a-string>
<!--            
            <xsl:if test="$obj_doc/doc/str[@name='m.note.patentassignees']">
                <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.note.patentassignees']"/></author-string>
                <role>other</role>                
            </xsl:if>
-->
<!--
            <xsl:if test="$obj_doc/doc/str[@name='m.note.patentholders']">
                <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.note.patentholders']"/></author-string>
                <role>other</role>                
            </xsl:if>
-->            
            
            <xsl:if test="$obj_doc/doc/str[@name='m.note.authors']">
                <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.note.authors']"/></author-string>
                <role>author</role>                
            </xsl:if>

<!--            
            <xsl:if test="$obj_doc/doc/str[@name='m.note.creators']">
                <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.note.creators']"/></author-string>
                <role>other</role>                
            </xsl:if>
-->            
<!--            
            <xsl:if test="$obj_doc/doc/str[@name='m.otherformat.note.performers']">
                <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.otherformat.note.performers']"/></author-string>
                <role>performer</role>                
            </xsl:if>
-->            
<!--            
            <xsl:if test="$obj_doc/doc/str[@name='m.note.translators']">
                <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.note.translators']"/></author-string>
                <role>translator</role>                
            </xsl:if>
-->
            
<!--            
            <xsl:if test="$obj_doc/doc/str[@name='m.note.editors']">
                <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.note.editors']"/></author-string>
                <role>editor</role>                
            </xsl:if>
-->         
            
<!--            
            <xsl:if test="$obj_doc/doc/str[@name='m.host.host.note.editors']">
                <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.host.host.note.editors']"/></author-string>
                <role>editor</role>                
            </xsl:if>
            <xsl:if test="$obj_doc/doc/str[@name='m.host.host.host.note.editors']">
                <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.host.host.host.note.editors']"/></author-string>
                <role>editor</role>                
            </xsl:if>
-->            
        </authors-as-a-string>
<!--
        <editors>
            <!-\- BA: Can't consistently retrieve first and last name as solr node structure is irregular -\->
            <!-\-<xsl:apply-templates select="$obj_doc/doc/arr[@name='m.host.name.edt']/str"/>-\->    
        </editors>
-->        
<!--
        <journal-title>
            <xsl:value-of select="concat($obj_doc/doc/str[@name='m.host.title'], '|', $obj_doc/doc/str[@name='m.host.title.abbreviated'])"/>                
        </journal-title>        
-->
<!--        
        <host-title>
            <xsl:choose>
                <xsl:when test="$obj_doc/doc/str[@name='m.host.title']">
                    <xsl:value-of select="$obj_doc/doc/str[@name='m.host.title']"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'host publication'"/>
                </xsl:otherwise>
            </xsl:choose>
        </host-title>

        <host-sub-title>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.host.subtitle']"/>
        </host-sub-title>
-->        
<!--
        <pages>
            <startpage>
                <xsl:value-of select="$obj_doc/doc/str[@name='m.host.page.start']"/>                
            </startpage>
            <endpage>
                <xsl:value-of select="$obj_doc/doc/str[@name='m.host.page.end']"/>                
            </endpage>
        </pages>
        -->
<!--        
        <article-number>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.identifier.articlenumber']"/>
        </article-number>
-->     
<!--        
        <volume>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.volume']"/>                
        </volume>
-->
<!--        
        <edition>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.edition']"/>
        </edition>
-->
        
        <place-of-publication>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.placeterm']"/>
        </place-of-publication>

<!--        
        <publisher-name>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.publisher']"/>
        </publisher-name>
-->        
<!--        
        <publisher-link>
            <xsl:value-of select="'No publisher link'"/>
        </publisher-link>                    
-->
<!--        
        <awarding-institution>
            <xsl:choose>
                <xsl:when test="$obj_doc/doc/str[@name='m.publisher']">
                    <xsl:value-of select="$obj_doc/doc/str[@name='m.publisher']"/>
                </xsl:when>
<!-\-                
                <xsl:otherwise>
                    <xsl:value-of select="'No publisher name'"/>
                </xsl:otherwise>
-\->                
            </xsl:choose>
        </awarding-institution>
-->        
<!--
        <qualification>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.note.degreelevel']"/>            
        </qualification>
-->        
<!--
        <supervisor-advisors>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.name.clb.source']/str | $obj_doc/doc/arr[@name='m.name.ths.source']/str" mode="supervisor_advisor"/>
        </supervisor-advisors>
-->        
<!--
        <sponsors>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.note.funding']/str" mode="sponsor"/>
        </sponsors>
-->        
        <patent-number>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.identifier.patentnumber']"/>
        </patent-number>
        
<!--
        <issue>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.host.issue']"/>                
        </issue>
-->
        
        <print-issns>
            <!--<xsl:apply-templates select="$obj_doc/doc/str[@name='m.identifier.isbn']"/>-->
            <xsl:apply-templates select="$obj_doc/doc/str[@name='m.identifier.isbn'] | $obj_doc/doc/str[@name='m.identifier.issn']"/>            
        </print-issns>
        
        <!--<electronic-issn></electronic-issn>-->
<!--        
        <peer-review>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.ispeerreviewed']"/>                
        </peer-review>
-->
        <!--<journal-link></journal-link>-->

        <series-title>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.host.title']"/>
        </series-title>


        <series-publisher>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.publisher']"/>
        </series-publisher>        

        <number>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.identifier.workingpapernumber']"/>
        </number>        
        
<!--
        <series-volume>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.host.host.volume']"/>
        </series-volume>        
        
        <series-number>                        
            <xsl:value-of select="$obj_doc/doc/str[@name='m.identifier.reportnumber']"/>
        </series-number>                            
-->        
        <!-- BA: 01/12/15 - change to address ambiguous template match error - start -->
        <attachments>
            <!-- BA: 13/04/16 Change to include FULL-TEXT.PDF -->
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW-DOCUMENT.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS-DOCUMENT.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS-DOCUMENT.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'FULL-TEXT.PDF')]">
                <!--<xsl:apply-templates select="$obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW-DOCUMENT.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS-DOCUMENT.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS-DOCUMENT.')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS.')]">-->
                <!--<xsl:with-param name="pid" as="xs:string" select="replace(tokenize(., '/')[last()], '_', ':')"/>-->
                <xsl:with-param name="pid" as="xs:string" select="$obj_doc/doc/str[@name='PID']"/>                
            </xsl:apply-templates>                                
        </attachments>
        
        <additional-files>
            <!-- BA: 13/04/16 Change to exclude FULL-TEXT.PDF -->
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-1')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-2')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-3')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-4')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-5')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-6')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-7')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-8')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-9')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-10')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[not(contains(., 'FULL-TEXT.PDF'))]">
                <!--<xsl:apply-templates select="$obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-1')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-2')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-3')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-4')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-5')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-6')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-7')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-8')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-9')] | $obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-10')]">-->
                <!--<xsl:with-param name="pid" as="xs:string" select="replace(tokenize(., '/')[last()], '_', ':')"/>-->
                <xsl:with-param name="pid" as="xs:string" select="$obj_doc/doc/str[@name='PID']"/>                
            </xsl:apply-templates>                                                                                    
<!--            
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'FULL-TEXT')]">
                <!-\-<xsl:with-param name="pid" as="xs:string" select="replace(tokenize(., '/')[last()], '_', ':')"/>-\->
                <xsl:with-param name="pid" as="xs:string" select="$obj_doc/doc/str[@name='PID']"/>                
            </xsl:apply-templates> 
            
            <!-\- BA: 04/03/16 - handle f.full-text.id - start -\->
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='f.full-text.id']/str">
                <xsl:with-param name="pid" as="xs:string" select="$obj_doc/doc/str[@name='PID']"/>                
            </xsl:apply-templates>                                                                                    
            <!-\- BA: 04/03/16 - handle f.full-text.id - end -\->            
-->            
        </additional-files>
        <!-- BA: 01/12/15 - change to address ambiguous template match error - end -->
        
        <licence-type>
            <xsl:variable name="licence_name" select="$obj_doc/doc/str[@name='m.accesscondition.licencetype']"/>
            <xsl:value-of select="$obj_open_licences/licences/licence[@name = $licence_name]/uri"/>
        </licence-type>

        <apcpaid>
            <xsl:choose>
                <xsl:when test="$obj_doc/doc/str[@name='m.note.isapcpaid'] = 'Yes'">
                    <xsl:value-of select="'yes'"/>
                </xsl:when>
                <xsl:when test="$obj_doc/doc/str[@name='m.note.isapcwaived'] = 'Yes'">
                    <xsl:value-of select="'waived'"/>
                </xsl:when>
            </xsl:choose>
        </apcpaid>        
<!--
        <event-type>Other</event-type>
        
        <event-title>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.host.title']"/>
            <!-\-<xsl:value-of select="$obj_doc/doc/str[@name='m.otherformat.title']"/>-\->            
        </event-title>
-->
<!--        
        <event-city>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.host.placeterm']"/>
        </event-city>
-->
<!--
        <event-location>
            <!-\-<xsl:value-of select="$obj_doc/doc/str[@name='m.otherformat.placeterm']"/>-\->
            <xsl:value-of select="$obj_doc/doc/str[@name='m.host.placeterm']"/>
        </event-location>
-->        
<!--        
        <web-addresses>
            <xsl:choose>
                <xsl:when test="$obj_doc/doc/arr[@name='m.preceding.source']/str">
                    <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.preceding.source']/str" mode="web-address" />                                                
                </xsl:when>
                <xsl:otherwise>
                    <web-address></web-address>
                </xsl:otherwise>
            </xsl:choose>
            <web-address></web-address>
        </web-addresses>
-->

<!--
        <start-date>
            
            <xsl:choose>
                <xsl:when test="$obj_doc/doc/str[@name='m.dateother.start']">
                    <xsl:value-of select="$obj_doc/doc/str[@name='m.dateother.start']"/>
                </xsl:when>
                
<!-\-                
                <xsl:when test="$obj_doc/doc/str[@name='m.host.dateother.start']">
                    <xsl:value-of select="$obj_doc/doc/str[@name='m.host.dateother.start']"/>
                </xsl:when>
-\->
<!-\-                
                <xsl:when test="$obj_doc/doc/str[@name='m.otherformat.dateother']">
                    <xsl:value-of select="$obj_doc/doc/str[@name='m.otherformat.dateother']"/>
                </xsl:when>
-\->                
                <xsl:otherwise>
                    <xsl:value-of select="'1824'"/>
                </xsl:otherwise>
            </xsl:choose>
            
            <!-\-<xsl:value-of select="'1824'"/>-\->
        </start-date>
-->
<!--        
        <end-date>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.dateother.end']"/>
        </end-date>
-->        
        <visibility>
            <xsl:choose>
                <xsl:when test="$obj_doc/doc/str[@name='x.state'] = 'Active'">
                    <xsl:value-of select="'public'"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'confidential'"/>                            
                </xsl:otherwise>
            </xsl:choose>
        </visibility>
    </xsl:template>
        
    <xsl:template match="arr[@name='m.topic']/str">
        <keyword>
            <xsl:value-of select="normalize-space(.)"/>
        </keyword>
    </xsl:template>
    
    <xsl:template match="arr[@name='m.preceding.source']/str">
        <url>
            <xsl:value-of select="tokenize(., '\|\|')[2]"/>
        </url>
    </xsl:template>

    <xsl:template match="arr[@name='m.preceding.identifier.uri']/str" mode="web-address">
        <web-address>
            <xsl:value-of select="normalize-space(.)" />
        </web-address>
    </xsl:template>
    
    <xsl:template match="arr[@name='m.preceding.identifier.uri']/str">
        <url>
            <xsl:value-of select="normalize-space(.)"/>
        </url>
    </xsl:template>    
    
    <xsl:template match="arr[@name='m.note.general']">
        <xsl:for-each select="str">
            <xsl:variable name="count">
                <xsl:number/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$count &gt; 1">
                    <xsl:value-of select="concat('; ', normalize-space(.))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="normalize-space(.)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>    

    <xsl:template match="arr[@name='r.isbelongstoorg.source']">
        <xsl:for-each select="str">
            <owner>
                <xsl:value-of select="normalize-space(.)"></xsl:value-of>
            </owner>
        </xsl:for-each>
    </xsl:template>
            
    <xsl:template match="arr[@name='r.hasauthor.source'] | arr[@name='r.isbelongsto.source']">
        <xsl:for-each select="str">
            <author>
                <author-first-name>
                    <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[2]"/>
                </author-first-name>
                <author-last-name>
                    <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[1]"/>
                </author-last-name>
                <role>author</role>
            </author>
        </xsl:for-each>
    </xsl:template>    

    <xsl:template match="str[@name='m.title'] | str[@name='m.title.translated']">
        <title>
            <xsl:if test="@name='m.title'">
                <xsl:attribute name="original" select="'y'"/>
            </xsl:if>
            <xsl:value-of select="normalize-space(.)"/>            
        </title>
    </xsl:template>

    <xsl:template match="arr[@name='m.note.general']/str | arr[@name='m.note.funding']/str | str[@name='m.note.statementonresearchdata']">
        <bibliographical-note>
            <xsl:value-of select="normalize-space(.)"/>
        </bibliographical-note>
    </xsl:template>        

    <xsl:template match="arr[@name='m.name.aut.source']/str | arr[@name='m.name.clb.source']/str | arr[@name='m.name.trl.source']/str | arr[@name='m.name.edt.source']/str | arr[@name='m.host.name.edt.source']/str | arr[@name='m.host.name.cur.source']/str | arr[@name='m.name.clb.source']/str | arr[@name='m.name.ths.source']/str | arr[@name='m.name.pta.source']/str">
        <xsl:param name="role"/>
        <author>
            <author-first-name>
                <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[2]"/>                
            </author-first-name>
            <author-last-name>
                <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[1]"/>                
            </author-last-name>
            <role><xsl:value-of select="$role"/></role>
        </author>
    </xsl:template>    

    <xsl:template match="arr[@name='m.name.clb.source']/str | arr[@name='m.name.ths.source']/str" mode="supervisor_advisor">
        <supervisor-advisor>
            <xsl:value-of select="tokenize(., '\|\|')[1]"/>
        </supervisor-advisor>
    </xsl:template>    

    <xsl:template match="arr[@name='m.note.funding']/str" mode="sponsor">
        <sponsor>
            <xsl:value-of select="normalize-space(.)"/>
        </sponsor>
    </xsl:template>        
    
    <xsl:template name="get_authors">
        <xsl:param name="obj_doc"/>        
        <internal-persons>
            <xsl:for-each select="$node_names/node-names/node-name">
                <xsl:call-template name="author_assoc">                        
                    <xsl:with-param name="node_name">
                        <xsl:copy-of select="."/>
                    </xsl:with-param>
                    <xsl:with-param name="solr_node">
                        <xsl:copy-of select="$obj_doc"/>
                    </xsl:with-param>
                </xsl:call-template>                            
            </xsl:for-each>   
        </internal-persons>        
    </xsl:template>
    
    <xsl:template name="author_assoc">
        <xsl:param name="node_name"/>
        <xsl:param name="solr_node"/>
        <xsl:for-each select="$solr_node/doc/arr[@name=$node_name/node-name]/str">
            <xsl:if test="not(contains(., 'uk-ac-man-per:abcde'))">
                <internal-person>
                    <xsl:value-of select="concat(tokenize(., '\|\|')[3], '; ', $node_name/node-name/@role, '; ', tokenize(tokenize(., '\|\|')[1], ', ')[2], '; ', tokenize(tokenize(., '\|\|')[1], ', ')[1])"/>                    
                </internal-person>                
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="arr[@name='r.hasauthor.source'] | arr[@name='r.isbelongsto.source']">
        <xsl:for-each select="str">
            <author>
                <author-first-name>
                    <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[2]"/>
                </author-first-name>
                <author-last-name>
                    <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[1]"/>
                </author-last-name>
                <role>author</role>
            </author>
        </xsl:for-each>
    </xsl:template>    
    
    <!-- BA: 01/12/15 - change to address ambiguous template match error - start -->
    <!-- BA: 13/04/16 Change to include FULL-TEXT.PDF -->
    <xsl:template match="arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW-DOCUMENT.')] | arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS-DOCUMENT.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS-DOCUMENT.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS.')] | arr[@name='f.fileattached.source']/str[contains(., 'FULL-TEXT.PDF')]">
        <!--<xsl:template match="arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW-DOCUMENT.')] | arr[@name='f.fileattached.source']/str[contains(., 'PRE-PEER-REVIEW.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS-DOCUMENT.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS-DOCUMENT.')] | arr[@name='f.fileattached.source']/str[contains(., 'POST-PEER-REVIEW-PUBLISHERS.')]">-->
        <xsl:param name="pid" as="xs:string"/>
        <attachment>
            <file-version>
                <xsl:choose>
                    <xsl:when test="contains(., 'PRE-PEER-REVIEW.') or contains(., 'PRE-PEER-REVIEW-DOCUMENT.')">
                        <xsl:value-of select="'preprint'"/>
                    </xsl:when>
                    <xsl:when test="contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS.') or contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS-DOCUMENT.')">
                        <xsl:value-of select="'authorsversion'"/>
                    </xsl:when>                    
                    <!-- BA: 13/04/16 Change to include FULL-TEXT.PDF -->
                    <xsl:when test="contains(., 'POST-PEER-REVIEW-PUBLISHERS.') or contains(., 'POST-PEER-REVIEW-PUBLISHERS-DOCUMENT.') or contains(., 'FULL-TEXT.PDF')">
                        <!--<xsl:when test="contains(., 'POST-PEER-REVIEW-PUBLISHERS.') or contains(., 'POST-PEER-REVIEW-PUBLISHERS-DOCUMENT.')">-->
                        <xsl:value-of select="'publishersversion'"/>
                    </xsl:when>                    
                </xsl:choose>
            </file-version>
            <file-url>
                <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?pid=', $pid, '&amp;dsid=', tokenize(., '\|\|')[1])"/>
            </file-url>
        </attachment>
    </xsl:template>
    
    <!-- BA: 13/04/16 Change to exclude FULL-TEXT.PDF -->
    <xsl:template match="arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-1')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-2')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-3')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-4')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-5')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-6')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-7')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-8')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-9')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-10')] | arr[@name='f.fileattached.source']/str[not(contains(., 'FULL-TEXT.PDF'))]">
        <!--<xsl:template match="arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-1')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-2')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-3')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-4')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-5')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-6')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-7')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-8')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-9')] | arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY-10')]">-->
        <xsl:param name="pid" as="xs:string"/>
        <url>
            <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?pid=', $pid, '&amp;dsid=', tokenize(., '\|\|')[1])"/>            
        </url>
    </xsl:template>
<!--    
    <xsl:template match="arr[@name='f.fileattached.source']/str[contains(., 'FULL-TEXT')]">
        <xsl:param name="pid" as="xs:string"/>
        <url>
            <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?pid=', $pid, '&amp;dsid=', tokenize(., '\|\|')[1])"/>            
        </url>
    </xsl:template>
    <!-\- BA: 01/12/15 - change to address ambiguous template match error - end -\->
-->
    <!-- BA: 04/03/16 - handle f.full-text.id - start -->
    <xsl:template match="arr[@name='f.full-text.id']/str">
        <xsl:param name="pid" as="xs:string"/>
        <url>
            <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?pid=', $pid, '&amp;dsid=', .)"/>            
        </url>
    </xsl:template>
    <!-- BA: 04/03/16 - handle f.full-text.id - end -->
    
    <xsl:template match="str[@name='f.full-text.source']">
        <xsl:param name="pid" as="xs:string"/>
        <attachment>
            <file-version>
                <xsl:value-of select="'publishersversion'"/>
            </file-version>
            <file-url>
                <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?pid=', $pid, '&amp;dsid=', tokenize(., '\|\|')[2])"/>
            </file-url>
        </attachment>
    </xsl:template>              

    <xsl:template match="arr[@name='m.host.name.edt']/str">
        <editor>
            <firstname>
                <xsl:value-of select="tokenize(., ', ')[2]"/>
            </firstname>
            <lastname>
                <xsl:value-of select="tokenize(., ', ')[1]"/>
            </lastname>
        </editor>
    </xsl:template>

    <xsl:template match="str[@name='m.identifier.isbn'] | str[@name='m.host.identifier.isbn'] | str[@name='m.identifier.issn']">
        <issn>
            <xsl:value-of select="." />
        </issn>                
    </xsl:template>
    
    <xsl:template match="arr[@name='m.preceding.source']/str" mode="web-address">
        <web-address>
            <xsl:value-of select="tokenize(., '\|\|')[2]" />
        </web-address>
    </xsl:template>
    
    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>
    
</xsl:stylesheet>
