<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <xsl:result-document href="unmatched-pids.xml">
        <unmatched-pids>         
            <xsl:apply-templates select="//escholar-id"/>
        </unmatched-pids>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="escholar-id">
        <pid>
            <xsl:value-of select="."/>
        </pid>
    </xsl:template>
    
</xsl:stylesheet>