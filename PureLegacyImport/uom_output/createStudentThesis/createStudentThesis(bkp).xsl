<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:local="http://localhost" xmlns:saxon="http://saxon.sf.net/" exclude-result-prefixes="#all" version="2.0">
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:output name="serialise1" encoding="utf-8" method="xhtml" indent="no" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:preserve-space elements="text"/>

	<xsl:template match="/">
		<xsl:variable name="baseuri" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/scw/select/'"/>
		<!-- <xsl:variable name="query" select="'r.isofcontenttype.pid:uk-ac-man-con%5C:20 AND x.lastmodifieddate:[NOW-3DAY TO NOW]'"/>-->
		<xsl:variable name="query" select="'r.isofcontenttype.pid:uk-ac-man-con%5C:20+AND+x.state:Active+AND+m.genre.version:%22Doctoral+level+ETD+-+final%22'"/>
		<!--  m.genre.version:&quot;Doctoral level ETD - final&quot;-->
		<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=5000&amp;indent=off')"/>
		<xsl:variable name="doc" select="doc($uri)"/>
		
		<xsl:apply-templates select="$doc/response"/>
	</xsl:template>
	<xsl:template match="response">
		<objects>
			<xsl:apply-templates select="result/doc"/>
		</objects>
	</xsl:template>
	<xsl:template match="doc">
		<xsl:variable name="baseurietd" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/etd/select/'"/>
		<object>
			<STUDENT_THESIS_ID><xsl:apply-templates select="str[@name='PID']"/></STUDENT_THESIS_ID>
			<QUALIFICATION_LEVEL><xsl:apply-templates select="str[@name='m.note.degreelevel']"/></QUALIFICATION_LEVEL>
			<ORIGINAL_LANGUAGE><xsl:apply-templates select="str[@name='m.languageterm.code']"/></ORIGINAL_LANGUAGE>
			<TITLE_ORIGINAL_LANGUAGE><xsl:apply-templates select="str[@name='m.title']"/></TITLE_ORIGINAL_LANGUAGE>
			<ABSTRACT><xsl:apply-templates select="str[@name='m.abstract']"/></ABSTRACT>
			<VISIBILITY><xsl:text>public</xsl:text></VISIBILITY>
			<MANAGED_IN_PURE><xsl:text>FALSE</xsl:text></MANAGED_IN_PURE>
			<AUTHOR_ID><xsl:apply-templates select="arr[@name='r.isbelongsto.source']"/></AUTHOR_ID> 
			<PERSON_ID><xsl:apply-templates select="arr[@name='r.isbelongsto.source']"/></PERSON_ID> 
			<ROLE><xsl:text>author</xsl:text></ROLE>
			<SPONSOR_ID>
				<xsl:apply-templates select="arr[@name='m.name.fnd']">
					<xsl:with-param name="pid"><xsl:value-of select="substring-after(str[@name='PID'],':')"/></xsl:with-param>	
				</xsl:apply-templates>
			</SPONSOR_ID>
			<FREE_KEYWORD><xsl:apply-templates select="arr[@name='m.topic']"/></FREE_KEYWORD>
			<DOCUMENT_ID><xsl:value-of select="concat(substring-after(str[@name='PID'],':'),'_','FULLTEXT')"/></DOCUMENT_ID>
			<TYPE><xsl:text>thesis</xsl:text></TYPE>
			<VALUE><xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?dsid=FULL-TEXT.PDF&amp;pid=' , str[@name='PID'])"/></VALUE>
			<FILE_NAME><xsl:text>FULL_TEXT.PDF</xsl:text></FILE_NAME>
			<MIME_TYPE><xsl:text>application/pdf</xsl:text></MIME_TYPE>
			<STUDENT_THESIS_DOCUMENT_VISIBILITY><xsl:text>public</xsl:text></STUDENT_THESIS_DOCUMENT_VISIBILITY>
			
			<!--<xsl:variable name="pid"><xsl:value-of select="'uk-ac-man-etd:252'"/></xsl:variable>-->
			<xsl:variable name="pid"><xsl:value-of select="arr[@name='r.isbelongstoetdwindow.pid']/str"/></xsl:variable>
			<xsl:variable name="queryetd" select="concat('PID:',replace($pid,':','?'))"/>
			<xsl:variable name="urietd" select="concat($baseurietd,'?q=',$queryetd,'&amp;version=2.2&amp;start=0&amp;rows=1&amp;indent=off')"/>
			<xsl:variable name="docetd">
				<xsl:copy-of select="doc($urietd)/response/result/doc"/>
			</xsl:variable>
			
			<PROGRAMME_ID><xsl:apply-templates select="$docetd/doc/str[@name='e.programmeid']"/></PROGRAMME_ID>
			<PLAN_ID><xsl:apply-templates select="$docetd/doc/str[@name='e.planid']"/></PLAN_ID>
			
		</object>
	</xsl:template>
	<xsl:template match="arr[@name='r.isbelongsto.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.fnd']">
		<xsl:param name="pid"/>
		<xsl:for-each select="str">
			<sponsor>
				<xsl:value-of select="concat($pid,'_', replace(.,' ',''))"/>
			</sponsor>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.topic']">
		<xsl:for-each select="str">
			<topic>
				<xsl:value-of select="."/>
			</topic>		
		</xsl:for-each>
	</xsl:template>
	
</xsl:stylesheet>
