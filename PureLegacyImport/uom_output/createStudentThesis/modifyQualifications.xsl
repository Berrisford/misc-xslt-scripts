<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <xsl:result-document href="qualifications.xml">
            <xsl:apply-templates/>            
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="str">
        <qualification>
            <xsl:attribute name="str" select="."/>
            <str>
                <xsl:value-of select="."></xsl:value-of>
            </str>
            <key>
                
            </key>
        </qualification>    
    </xsl:template>

<!-- 
  <str>ClinPsyD</str>
  <key>clinpsyd</key>    
-->

<!--
    <xsl:template match="*" >
        <xsl:element name="{name()}" >
            <xsl:apply-templates select ="@*"/> 
            <xsl:apply-templates/>
        </xsl:element> 
    </xsl:template> 
    
    <xsl:template match="@*" >
        <xsl:attribute name="{name()}" >
            <xsl:value-of select ="."/> 
        </xsl:attribute> 
    </xsl:template> 
-->    
    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>
    
</xsl:stylesheet>