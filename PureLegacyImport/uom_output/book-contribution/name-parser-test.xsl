<?xml version="1.0" encoding="UTF-8"?>
    <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:mods="http://www.loc.gov/mods/v3"
        version="2.0">
        <xsl:output indent="yes"/>
        <xsl:template name="name-parser">
            <xsl:param name="name-string"/>
            <xsl:param name="role-term-code"/>
            <xsl:param name="role-term-text"/>
            <mods:name type="personal">
                <xsl:choose>
                    <!-- Does the name contain a comma? -->
                    <xsl:when test="contains($name-string,',')">
                        <!-- Yes, assume all before the comma is the surname -->
                        <xsl:choose>
                            <!-- Does all after the comma (and space) contain a space? -->
                            <xsl:when test="contains(substring-after($name-string,', '),' ')">
                                <!-- Yes, assume that there is a middle name. Strip out any periods. -->
                                <mods:namePart type="given">
                                    <xsl:value-of select="translate(substring-before(substring-after($name-string,', '),' '),'.','')"/>
                                </mods:namePart>
                                <mods:namePart type="given">
                                    <xsl:value-of select="translate(substring-after(substring-after($name-string,', '),' '),'.','')"/>
                                </mods:namePart>
                            </xsl:when>
                            <xsl:otherwise>
                                <!-- No, does the string contain any periods? -->
                                <xsl:choose>
                                    <xsl:when test="contains(.,'.')">
                                        <!-- Yes, assume initials separated with periods -->
                                        <xsl:variable name="initials" select="translate(substring-after($name-string,', '),'.','')"/>
                                        <xsl:choose>
                                            <xsl:when test="string-length($initials) = 2">
                                                <mods:namePart type="given">
                                                    <xsl:value-of select="substring($initials,1,1)"/>
                                                </mods:namePart>
                                                <mods:namePart type="given">
                                                    <xsl:value-of select="substring($initials,2,2)"/>
                                                </mods:namePart>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <mods:namePart type="given">
                                                    <xsl:value-of select="$initials"/>
                                                </mods:namePart>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <!-- No, assume just a first name. Strip out any periods. -->
                                        <mods:namePart type="given">
                                            <xsl:value-of select="translate(substring-after($name-string,', '),'.','')"/>
                                        </mods:namePart>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                        <mods:namePart type="family">
                            <xsl:value-of select="substring-before($name-string,',')"/>
                        </mods:namePart>
                    </xsl:when>
                    <xsl:otherwise>
                        <!-- No, there is no comma -->
                        <xsl:choose>
                            <!-- Does it have any spaces? -->
                            <xsl:when test="contains($name-string,' ')">
                                <xsl:variable name="all-after-last-space" select="tokenize($name-string,' ')[last()]"/>                            
                                <xsl:variable name="second-string-after-first-space" select="tokenize($name-string,' ')[2]"/>
                                <xsl:variable name="name-length" select="string-length($name-string)"/>
                                <xsl:choose>
                                    <!-- Yes, does all after the last space have a length of 1 (is it an initial) ? -->
                                    <xsl:when test="string-length($all-after-last-space) = 1">
                                        <!-- Yes, assume all before the last space is the surname and after is the initial. Strip out any periods. -->
                                        <mods:namePart type="given">
                                            <xsl:value-of select="translate($all-after-last-space,'.','')"/>
                                        </mods:namePart>
                                        <mods:namePart type="family">
                                            <xsl:value-of select="substring($name-string,1,$name-length - 2)"/>
                                        </mods:namePart>
                                    </xsl:when>
                                    <!-- Yes, does all after the last space have a length of 2 (is it two letters concatenated) ? -->
                                    <xsl:when test="string-length($all-after-last-space) = 2">
                                        <xsl:variable name="second-initial" select="substring($all-after-last-space,2)"/>
                                        <!-- Is the second letter upper case? -->
                                        <xsl:analyze-string select="$second-initial" regex="[A-Z]">
                                            <!-- Yes, assume two initials -->
                                            <xsl:matching-substring>
                                                <mods:namePart type="given">
                                                    <xsl:value-of select="substring($all-after-last-space,1,1)"/>
                                                </mods:namePart>
                                                <mods:namePart type="given">
                                                    <xsl:value-of select="substring($all-after-last-space,2)"/>
                                                </mods:namePart>
                                                <mods:namePart type="family">
                                                    <xsl:value-of select="normalize-space(substring-before($name-string,$all-after-last-space))"/>
                                                </mods:namePart>
                                            </xsl:matching-substring>
                                            <!-- No, we must assume the second letter forms the second letter of a first name of length 2 --> 
                                            <xsl:non-matching-substring>
                                                <mods:namePart type="given">
                                                    <xsl:value-of select="$all-after-last-space"/>
                                                </mods:namePart>
                                                <mods:namePart type="family">
                                                    <xsl:value-of select="substring($name-string,1,$name-length - 3)"/>
                                                </mods:namePart>
                                            </xsl:non-matching-substring>
                                        </xsl:analyze-string>
                                    </xsl:when>
                                    <!-- Yes, does the second string after the first space have a length of 1 (is it a second name initial) ? -->
                                    <xsl:when test="string-length($second-string-after-first-space) = 1">
                                        <mods:namePart type="given">
                                            <xsl:value-of select="substring-before($name-string,' ')"/>
                                        </mods:namePart>
                                        <mods:namePart type="given">
                                            <xsl:value-of select="$second-string-after-first-space"/>
                                        </mods:namePart>
                                        <mods:namePart type="family">
                                            <xsl:value-of select="normalize-space(substring-after($name-string,$second-string-after-first-space))"/>
                                        </mods:namePart>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <!-- No, assume all before the first space is a firstname and the rest is lastname. Strip out any periods. -->
                                        <mods:namePart type="given">
                                            <xsl:value-of select="translate(substring-before($name-string,' '),'.','')"/>
                                        </mods:namePart>
                                        <mods:namePart type="family">
                                            <xsl:value-of select="normalize-space(translate(substring-after($name-string,' '),'.',''))"/>
                                        </mods:namePart>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <!-- No, assume single string surname. -->
                                <mods:namePart type="family">
                                    <xsl:value-of select="$name-string"/>
                                </mods:namePart>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
                <mods:role>
                    <mods:roleTerm type="code" authority="marcrelator"><xsl:value-of select="$role-term-code"/></mods:roleTerm>
                    <mods:roleTerm type="text" authority="marcrelator"><xsl:value-of select="$role-term-text"/></mods:roleTerm>
                </mods:role>
                <mods:affiliation/>
            </mods:name> 
        </xsl:template>
    </xsl:stylesheet>