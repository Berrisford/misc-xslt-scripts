<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">    
    <xsl:output indent="yes" method="xml"/>
    
    <!-- BA: This script iterates across all records (of a particular content type) in chunks of 1000 (rows) and checks to see if the record -->
    <!--  has been matched by Scopus. If not, it generates the requisite Pure xml entirely from eScolar data. Change $rows, $num_found, $obj_files -->
    <!--  accordingly to test quickly with small sets of data. Change $file_path to match where your files are located. -->
    
    <xsl:variable name="root_url" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/scw/select/'"></xsl:variable>
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con%5C%3A15+AND+r.isbelongsto.pid:uk*', '&amp;fl=PID,r.isbelongsto.pid,x.state,m.genre.version,m.genre.form&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+m.note.authors:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=PID:', 'uk-ac-man-scw%5C%3A71283Berris', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=PID%3Auk-ac-man-scw%5C%3A202423&amp;fl=PID,r.isbelongsto.pid,x.state&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A9+AND+m.note.funding:%5B*+TO+*%5D', '&amp;fl=PID,r.isbelongsto.pid,x.state,m.genre.version,m.genre.form&amp;omitHeader=true')"></xsl:variable>-->
    <xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con%5C%3A3+AND+r.isbelongstoorg.pid:uk-ac-man-org%5C%3A37', '&amp;fl=PID,r.isbelongsto.pid,x.state,m.genre.version,m.genre.form&amp;omitHeader=true')"></xsl:variable>
    <!--<xsl:variable name="url" select="concat($root_url, '?q=PID:', 'uk-ac-man-scw%5C%3A3b1893', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->

    <xsl:variable name="rows" select="1000"/>
    <xsl:variable name="num_found">
        <xsl:value-of select="doc(concat($url, '&amp;start=0&amp;rows=0'))/response/result/@numFound"/>
        <!--<xsl:value-of select="doc(concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?17', '&amp;fl=PID&amp;omitHeader=true&amp;start=0&amp;rows=0'))/response/result/@numFound"/>-->
        <!--<xsl:value-of select="20"/>-->
    </xsl:variable>
    
    <!-- Retrieve the file document here. We can then just check this to see if a selected eScholar pid is in the document -->
    <xsl:variable name="file_path" select="'C:/PureLegacyImport/uom_output/SchoolOfLawBookContributions/'"/>
    <xsl:variable name="obj_files">
        <xsl:copy-of select="doc('files.xml')"/>
    </xsl:variable>    
    
    <xsl:variable name="node_names">
        <node-names>
            <node-name role="author">r.isbelongsto.source</node-name>
        </node-names>
    </xsl:variable>    

    
    <xsl:template match="/">
        <records>
        <xsl:call-template name="recurse">
            <xsl:with-param name="start" select="0"/>
            <xsl:with-param name="end" select="$rows"/>
        </xsl:call-template>
        </records>
    </xsl:template>
        
    <xsl:template name="recurse">
        <xsl:param name="start"/>
        <xsl:param name="end"/>
        <xsl:variable name="obj_response">
            <xsl:copy-of select="doc(concat($url, '&amp;start=', $start, '&amp;rows=', $rows))"/>
        </xsl:variable>
        <xsl:for-each select="$obj_response/response/result/doc">
            <xsl:variable name="filename" select="replace(str[@name='PID'], ':', '_')"/>            
            <xsl:call-template name="unmatched">
                <xsl:with-param name="pid" select="replace(str[@name='PID'], ':', '?')"/>
            </xsl:call-template>                                  
        </xsl:for-each>
        
        <xsl:if test="$end &lt; $num_found">
            <xsl:call-template name="recurse">
                <xsl:with-param name="start" select="$end + 1"/>
                <xsl:with-param name="end" select="$end + $rows"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    
    <xsl:template name="unmatched">
        <xsl:param name="pid"/>
        <xsl:variable name="obj_doc">
            <xsl:copy-of select="doc(concat($root_url, '?q=PID:', $pid, '&amp;omitHeader=true&amp;start=0&amp;rows=10'))/response/result/doc"/>            
        </xsl:variable>
        
        <record>
        <escholar-id>
            <xsl:value-of select="$obj_doc/doc/str[@name='PID']"/>
        </escholar-id>
        
        <xsl:apply-templates select="$obj_doc/doc/str[@name='m.title']"/>            

        <publisher>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.publisher']"/>
        </publisher> 
                
        <xsl:variable name="internal-persons">
            <xsl:call-template name="get_authors">
                <xsl:with-param name="obj_doc">
                    <xsl:copy-of select="$obj_doc/doc"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:variable>
        
        <university-owners>
            <xsl:variable name="count" select="count(distinct-values($internal-persons/internal-persons/internal-person))" as="xs:integer"/>
            <xsl:for-each select="distinct-values($internal-persons/internal-persons/internal-person)">
                <xsl:value-of select="."/>                
            </xsl:for-each>            
        </university-owners>
                
        <file-attached>
            <xsl:choose>
                <xsl:when test="$obj_doc/doc/str[@name='f.full-text.source'] or $obj_doc/doc/arr[@name='f.fileattached.source']/str">
                    <xsl:value-of select="'Yes'"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'No'"/>
                </xsl:otherwise>
            </xsl:choose>
        </file-attached>
        </record>      
       
    </xsl:template>
        
    <xsl:template match="arr[@name='m.topic']/str">
        <keyword>
            <xsl:value-of select="normalize-space(.)"/>
        </keyword>
    </xsl:template>
    
    <xsl:template match="arr[@name='m.preceding.source']/str">
        <url>
            <xsl:value-of select="tokenize(., '\|\|')[2]"/>
        </url>
    </xsl:template>
        
    <xsl:template match="arr[@name='m.note.general']">
        <xsl:for-each select="str">
            <xsl:variable name="count">
                <xsl:number/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$count &gt; 1">
                    <xsl:value-of select="concat('; ', normalize-space(.))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="normalize-space(.)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>    

    <xsl:template match="arr[@name='r.isbelongstoorg.source']">
        <xsl:for-each select="str">
            <owner>
                <xsl:value-of select="normalize-space(.)"></xsl:value-of>
            </owner>
        </xsl:for-each>
    </xsl:template>
            
    <xsl:template match="arr[@name='r.hasauthor.source'] | arr[@name='r.isbelongsto.source']">
        <xsl:for-each select="str">
            <author>
                <author-first-name>
                    <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[2]"/>
                </author-first-name>
                <author-last-name>
                    <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[1]"/>
                </author-last-name>
                <role>author</role>
            </author>
        </xsl:for-each>
    </xsl:template>    

    <xsl:template match="str[@name='m.title'] | str[@name='m.title.translated']">
        <title>
            <xsl:if test="@name='m.title'">
                <xsl:attribute name="original" select="'y'"/>
            </xsl:if>
            <xsl:value-of select="normalize-space(.)"/>            
        </title>
    </xsl:template>

    <xsl:template match="arr[@name='m.note.general']/str | arr[@name='m.note.funding']/str | str[@name='m.note.statementonresearchdata']">
        <bibliographical-note>
            <xsl:value-of select="normalize-space(.)"/>
        </bibliographical-note>
    </xsl:template>        

    <xsl:template match="arr[@name='m.name.aut.source']/str | arr[@name='m.name.clb.source']/str | arr[@name='m.name.trl.source']/str | arr[@name='m.name.edt.source']/str | arr[@name='m.host.name.edt.source']/str | arr[@name='m.host.name.cur.source']/str | arr[@name='m.name.clb.source']/str | arr[@name='m.name.ths.source']/str">
        <xsl:param name="role"/>
        <author>
            <first-name>
                <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[2]"/>                
            </first-name>
            <last-name>
                <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[1]"/>                
            </last-name>
            <role><xsl:value-of select="$role"/></role>
        </author>
    </xsl:template>    

    <xsl:template match="arr[@name='m.name.clb.source']/str | arr[@name='m.name.ths.source']/str" mode="supervisor_advisor">
        <supervisor-advisor>
            <xsl:value-of select="tokenize(., '\|\|')[1]"/>
        </supervisor-advisor>
    </xsl:template>    

    <xsl:template match="arr[@name='m.note.funding']/str" mode="sponsor">
        <sponsor>
            <xsl:value-of select="normalize-space(.)"/>
        </sponsor>
    </xsl:template>        
    
    <xsl:template name="get_authors">
        <xsl:param name="obj_doc"/>        
        <internal-persons>
            <xsl:for-each select="$node_names/node-names/node-name">
                <xsl:call-template name="author_assoc">                        
                    <xsl:with-param name="node_name">
                        <xsl:copy-of select="."/>
                    </xsl:with-param>
                    <xsl:with-param name="solr_node">
                        <xsl:copy-of select="$obj_doc"/>
                    </xsl:with-param>
                </xsl:call-template>                            
            </xsl:for-each>   
        </internal-persons>        
    </xsl:template>
    
    <xsl:template name="author_assoc">
        <xsl:param name="node_name"/>
        <xsl:param name="solr_node"/>
        <xsl:for-each select="$solr_node/doc/arr[@name=$node_name/node-name]/str">
            <xsl:variable name="owner-title">
                <xsl:value-of select="doc(concat('http://fedprdir.library.manchester.ac.uk:8085/solr/nonscw/select/?q=PID%3Auk-ac-man-per%5C%3A', substring-after(tokenize(., '\|\|')[2], ':'), '&amp;version=2.2&amp;start=0&amp;rows=10&amp;indent=on'))/response/result/doc/str[@name='p.title']"/>
            </xsl:variable>
            
            <!--<xsl:if test="not(contains(., 'uk-ac-man-per:abcde'))">-->
                <internal-person>
                    <!--<xsl:value-of select="concat(tokenize(., '\|\|')[3], '; ', $node_name/node-name/@role, '; ', tokenize(tokenize(., '\|\|')[1], ', ')[2], '; ', tokenize(tokenize(., '\|\|')[1], ', ')[1])"/>-->                    
                    <xsl:value-of select="concat($owner-title, ' ', tokenize(., '\|\|')[4], ' | ')"/>                    
                </internal-person>                
            <!--</xsl:if>-->
            
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="arr[@name='r.hasauthor.source'] | arr[@name='r.isbelongsto.source']">
        <xsl:for-each select="str">
            <author>
                <first-name>
                    <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[2]"/>
                </first-name>
                <last-name>
                    <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[1]"/>
                </last-name>
                <role>author</role>
            </author>
        </xsl:for-each>
    </xsl:template>    
    
    <xsl:template match="arr[@name='f.fileattached.source']/str[contains(., 'REVIEW')]">
        <xsl:param name="pid" as="xs:string"/>
        <attachment>
            <file-version>
                <xsl:choose>
                    <xsl:when test="contains(., 'PRE-PEER-REVIEW.') or contains(., 'PRE-PEER-REVIEW-DOCUMENT.')">
                        <xsl:value-of select="'preprint'"/>
                    </xsl:when>
                    <xsl:when test="contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS.') or contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS-DOCUMENT.')">
                        <xsl:value-of select="'authorsversion'"/>
                    </xsl:when>                    
                    <xsl:when test="contains(., 'POST-PEER-REVIEW-PUBLISHERS.') or contains(., 'POST-PEER-REVIEW-PUBLISHERS-DOCUMENT.')">
                        <xsl:value-of select="'publishersversion'"/>
                    </xsl:when>                    
                </xsl:choose>
            </file-version>
            <file-url>
                <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?pid=', $pid, '&amp;dsid=', tokenize(., '\|\|')[1])"/>
            </file-url>
        </attachment>
    </xsl:template>

    <xsl:template match="str[@name='f.full-text.source']">
        <xsl:param name="pid" as="xs:string"/>
        <attachment>
            <file-version>
                <xsl:value-of select="'publishersversion'"/>
            </file-version>
            <file-url>
                <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?pid=', $pid, '&amp;dsid=', tokenize(., '\|\|')[2])"/>
            </file-url>
        </attachment>
    </xsl:template>
    
    <xsl:template match="arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY')]">
        <xsl:param name="pid" as="xs:string"/>
        <url>
            <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?pid=', $pid, '&amp;dsid=', tokenize(., '\|\|')[1])"/>            
        </url>
    </xsl:template>

    <xsl:template match="arr[@name='m.host.name.edt']/str">
        <editor>
            <firstname>
                <xsl:value-of select="tokenize(., ', ')[2]"/>
            </firstname>
            <lastname>
                <xsl:value-of select="tokenize(., ', ')[1]"/>
            </lastname>
        </editor>
    </xsl:template>

    <xsl:template match="str[@name='m.identifier.isbn'] | str[@name='m.host.identifier.isbn'] | str[@name='m.identifier.issn']">
        <isbn>
            <xsl:value-of select="." />
        </isbn>                
    </xsl:template>
    
    <xsl:template match="arr[@name='m.preceding.source']/str" mode="web-address">
        <web-address>
            <xsl:value-of select="tokenize(., '\|\|')[2]" />
        </web-address>
    </xsl:template>
    
    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>
    
</xsl:stylesheet>
