echo Start time: %DATE% %TIME%
 java -Xms4000m -Xmx4000m -jar "C:\saxon\saxon9pe.jar" -s:files.xml -xsl:school-of-law-book-contributions.xsl -o:output\school-of-law-book-contributions.xml
echo End time: %DATE% %TIME%
pause