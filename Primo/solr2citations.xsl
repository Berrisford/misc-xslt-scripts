<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output name="default" encoding="UTF-8" indent="no" method="xhtml"/>
    <xsl:template match="str[@name='r.isofcontenttype.pid']" mode="citation">
        <xsl:param name="format"/>
        <xsl:variable name="citationstyles">
            <citationstyles>
                <citationcontenttype pid="uk-ac-man-con:1" name="Journal article">
                    <citationstyle id="contributors">
                        <group sx="">
                            <alternative>
                                <contributors delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="contributors-linked">
                        <group sx="">
                            <alternative>
                                <contributors-linked delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors-linked>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="source">
                        <xsl:comment>Journal title year;volume(issue):start-end</xsl:comment>
                        <xsl:comment>m.host.title m.year;m.host.volume(m.host.issue):m.host.page.start-m.host.page.end</xsl:comment>
                        <group sx=".">
                            <text sx=".">
                                <alternative>
                                    <text>
                                        <xsl:value-of select="../str[@name='m.host.title']"/>
                                    </text>
                                </alternative>
                                <alternative>
                                    <text>
                                        <xsl:value-of select="../str[@name='m.host.title.abbreviated']"/>
                                    </text>
                                </alternative>
                            </text>
                            <group px=" " sx="">
                                <text px="" sx=";">
                                    <xsl:value-of select="../str[@name='m.year']"/>
                                </text>
                                <group px="" sx="">
                                    <text px="" sx="">
                                        <xsl:value-of select="../str[@name='m.host.volume']"/>
                                    </text>
                                    <text px="(" sx=")">
                                        <xsl:value-of select="../str[@name='m.host.issue']"/>
                                    </text>
                                </group>
                                <group px=":">
                                    <alternative>
                                        <text>
                                            <xsl:value-of select="../str[@name='m.host.page.start']"/>
                                        </text>
                                        <text px="-">
                                            <xsl:value-of select="../str[@name='m.host.page.end']"/>
                                        </text>
                                    </alternative>
                                    <alternative>
                                        <text>
                                            <xsl:value-of select="if (../str[@name='m.host.page.list']!='-') then ../str[@name='m.host.page.list'] else ''"/>
                                        </text>
                                    </alternative>
                                </group>
                            </group>
                        </group>
                    </citationstyle>
                </citationcontenttype>
                <citationcontenttype pid="uk-ac-man-con:2" name="Conference contribution">
                    <citationstyle id="contributors">
                        <group sx="">
                            <alternative>
                                <contributors delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="contributors-linked">
                        <group sx="">
                            <alternative>
                                <contributors-linked delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors-linked>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="source">
                        <xsl:comment>In: Surname FM, editor(s). Title of proceedings: conference title; start date-end date; Location of conference. Place of publication: Publisher; Year. p. start page-end page.</xsl:comment>
                        <xsl:comment>In: m.note.editors, editor(s). m.host.title: m.host.host.title; m.host.host.dateother.start-m.host.host.dateother.end; m.host.host.placeterm. m.placeterm: m.publisher; m.year. p. m.host.page.start-m.host.page.end.</xsl:comment>
                        <group px="In: " sx="">
                            <group sx=".">
                                <alternative>
                                    <contributors delimit="; ">
                                        <xsl:copy-of select="../arr[@name='m.host.name.edt']/str"/>
                                    </contributors>
                                </alternative>
                                <alternative>
                                    <text>
                                        <xsl:value-of select="../str[@name='m.host.note.editors']"/>
                                    </text>
                                </alternative>
                            </group>
                            <group px=" " sx=";" delimit=": ">
                                <text>
                                    <xsl:value-of select="../str[@name='m.host.title']"/>
                                </text>
                                <text>
                                    <xsl:value-of select="../str[@name='m.host.host.title']"/>
                                </text>
                            </group>
                            <group px=" " sx=";" delimit="-">
                                <text>
                                    <xsl:value-of select="if (../str[@name='m.host.host.dateother.start'] castable as xs:date) then format-date(xs:date(../str[@name='m.host.host.dateother.start']),'[D01] [MNn,*-3] [Y0001]') else ''"/>
                                </text>
                                <text>
                                    <xsl:value-of select="if (../str[@name='m.host.host.dateother.end'] castable as xs:date) then format-date(xs:date(../str[@name='m.host.host.dateother.end']),'[D01] [MNn,*-3] [Y0001]') else ''"/>
                                </text>
                            </group>
                            <text px=" " sx=".">
                                <xsl:value-of select="../str[@name='m.host.host.placeterm']"/>
                            </text>
                            <group px=" " sx=";" delimit=": ">
                                <text>
                                    <xsl:value-of select="../str[@name='m.placeterm']"/>
                                </text>
                                <text>
                                    <xsl:value-of select="../str[@name='m.publisher']"/>
                                </text>
                            </group>
                            <text px=" " sx=".">
                                <xsl:value-of select="../str[@name='m.year']"/>
                            </text>
                            <group px=" p. " sx=".">
                                <alternative delimit="-">
                                    <text>
                                        <xsl:value-of select="../str[@name='m.host.page.start']"/>
                                    </text>
                                    <text>
                                        <xsl:value-of select="../str[@name='m.host.page.end']"/>
                                    </text>
                                </alternative>
                                <alternative>
                                    <text>
                                        <xsl:value-of select="if (../str[@name='m.host.page.list']!='-') then ../str[@name='m.host.page.list'] else ''"/>
                                    </text>
                                </alternative>
                            </group>
                        </group>
                    </citationstyle>
                </citationcontenttype>
                <citationcontenttype pid="uk-ac-man-con:3" name="Book contribution">
                    <citationstyle id="contributors">
                        <group sx="">
                            <alternative>
                                <contributors delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="contributors-linked">
                        <group sx="">
                            <alternative>
                                <contributors-linked delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors-linked>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="source">
                        <xsl:comment>In: Surname FM, editor(s). Book title: book subtitle. Edition ed. Place of publication: Publisher; Year. p. Start-end.</xsl:comment>
                        <xsl:comment>In: m.note.editors, editor(s). m.host.title: m.host.subtitle. m.host.edition ed. m.placeterm: m.publisher; m.year.</xsl:comment>
                        <group px="In: " sx=".">
                            <text sx=", editor(s).">
                                <alternative>
                                    <contributors delimit="; ">
                                        <xsl:copy-of select="../arr[@name='m.name.edt']/str"/>
                                    </contributors>
                                </alternative>
                                <alternative>
                                    <text>
                                        <xsl:value-of select="../str[@name='m.note.editors']"/>
                                    </text>
                                </alternative>
                            </text>
                            <group px=" " sx="." delimit=": ">
                                <text>
                                    <xsl:value-of select="../str[@name='m.host.title']"/>
                                </text>
                                <text>
                                    <xsl:value-of select="../str[@name='m.host.subtitle']"/>
                                </text>
                            </group>
                            <text px=" " sx=" ed.">
                                <xsl:value-of select="../str[@name='m.host.edition']"/>
                            </text>
                            <group px=" " sx=";" delimit=": ">
                                <text px="" sx="">
                                    <xsl:value-of select="../str[@name='m.placeterm']"/>
                                </text>
                                <text>
                                    <xsl:value-of select="../str[@name='m.publisher']"/>
                                </text>
                            </group>
                            <text px=" " sx=".">
                                <xsl:value-of select="../str[@name='m.year']"/>
                            </text>
                            <group px=" p. ">
                                <alternative delimit="-">
                                    <text>
                                        <xsl:value-of select="../str[@name='m.host.page.start']"/>
                                    </text>
                                    <text>
                                        <xsl:value-of select="../str[@name='m.host.page.end']"/>
                                    </text>
                                </alternative>
                                <alternative>
                                    <text>
                                        <xsl:value-of select="if (../str[@name='m.host.page.list']!='-') then ../str[@name='m.host.page.list'] else ''"/>
                                    </text>
                                </alternative>
                            </group>
                        </group>
                    </citationstyle>
                </citationcontenttype>
                <citationcontenttype pid="uk-ac-man-con:4" name="Book">
                    <citationstyle id="contributors">
                        <group sx="">
                            <alternative>
                                <contributors delimit="; ">
                                    <xsl:choose>
                                        <xsl:when test="../str[@name='m.genre.version']='Edited book'">
                                            <xsl:copy-of select="../arr[@name='m.name.edt']/str"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </contributors>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:choose>
                                        <xsl:when test="../str[@name='m.genre.version']='Edited book'">
                                            <xsl:value-of select="../str[@name='m.note.editors']"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="../str[@name='m.note.authors']"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="contributors-linked">
                        <group sx="">
                            <alternative>
                                <contributors-linked delimit="; ">
                                    <xsl:choose>
                                        <xsl:when test="../str[@name='m.genre.version']='Edited book'">
                                            <xsl:copy-of select="../arr[@name='m.name.edt']/str"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </contributors-linked>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:choose>
                                        <xsl:when test="../str[@name='m.genre.version']='Edited book'">
                                            <xsl:value-of select="../str[@name='m.note.editors']"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="../str[@name='m.note.authors']"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="source">
                        <xsl:comment>Edition ed. Place of publication: Publisher; Year.</xsl:comment>
                        <xsl:comment>m.host.edition ed. m.placeterm: m.publisher; m.year.</xsl:comment>
                        <group px="" sx="." delimit=" ">
                            <text px="" sx=" ed.">
                                <xsl:value-of select="../str[@name='m.edition']"/>
                            </text>
                            <group px=" " sx=";" delimit=": ">
                                <text px="" sx="">
                                    <xsl:value-of select="../str[@name='m.placeterm']"/>
                                </text>
                                <text>
                                    <xsl:value-of select="../str[@name='m.publisher']"/>
                                </text>
                            </group>
                            <text px="" sx="">
                                <xsl:value-of select="../str[@name='m.year']"/>
                            </text>
                        </group>
                    </citationstyle>
                </citationcontenttype>
                <citationcontenttype pid="uk-ac-man-con:5" name="Report">
                    <citationstyle id="contributors">
                        <group sx="">
                            <alternative>
                                <contributors delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="contributors-linked">
                        <group sx="">
                            <alternative>
                                <contributors-linked delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors-linked>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="source">
                        <xsl:comment>Place of publication: Publisher; Year. Report No.: number.</xsl:comment>
                        <xsl:comment>m.placeterm: m.publisher; m.year. Report No.: m.identifier.reportnumber.</xsl:comment>
                        <group px="" sx="" delimit=" ">
                            <group px="" sx=";" delimit=": ">
                                <text px="" sx="">
                                    <xsl:value-of select="../str[@name='m.placeterm']"/>
                                </text>
                                <text>
                                    <xsl:value-of select="../str[@name='m.publisher']"/>
                                </text>
                            </group>
                            <group delimit=" ">
                                <text px="" sx=".">
                                    <xsl:value-of select="../str[@name='m.year']"/>
                                </text>
                                <text px="Report No. " sx=".">
                                    <xsl:value-of select="../str[@name='m.identifier.reportnumber']"/>
                                </text>
                            </group>
                        </group>
                    </citationstyle>
                </citationcontenttype>
                <citationcontenttype pid="uk-ac-man-con:6" name="Conference proceeding">
                    <citationstyle id="contributors">
                        <group sx="">
                            <alternative>
                                <contributors delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.edt']/str"/>
                                </contributors>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.editors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="contributors-linked">
                        <group sx="">
                            <alternative>
                                <contributors-linked delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.edt']/str"/>
                                </contributors-linked>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.editors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="source">
                        <xsl:comment>Conference title; start date-end date; Location of conference. Place of publication: Publisher; Year.</xsl:comment>
                        <xsl:comment>m.host.title; m.host.dateother.start-m.host.dateother.end; m.host.placeterm. m.placeterm: m.publisher; m.year.</xsl:comment>
                        <group px="" sx="." delimit=" ">
                            <group px="" sx=";">
                                <text>
                                    <xsl:value-of select="../str[@name='m.host.title']"/>
                                </text>
                            </group>
                            <group px="" sx=";" delimit="-">
                                <text>
                                    <xsl:value-of select="if (../str[@name='m.host.dateother.start'] castable as xs:date) then format-date(xs:date(../str[@name='m.host.dateother.start']),'[D01] [MNn,*-3] [Y0001]') else ''"/>
                                </text>
                                <text>
                                    <xsl:value-of select="if (../str[@name='m.host.dateother.end'] castable as xs:date) then format-date(xs:date(../str[@name='m.host.dateother.end']),'[D01] [MNn,*-3] [Y0001]') else ''"/>
                                </text>
                            </group>
                            <group px="" sx=".">
                                <text px="" sx="">
                                    <xsl:value-of select="../str[@name='m.host.placeterm']"/>
                                </text>
                            </group>
                            <group px="" sx=";" delimit=": ">
                                <text>
                                    <xsl:value-of select="../str[@name='m.placeterm']"/>
                                </text>
                                <text>
                                    <xsl:value-of select="../str[@name='m.publisher']"/>
                                </text>
                            </group>
                            <group px="">
                                <text px="" sx="">
                                    <xsl:value-of select="../str[@name='m.year']"/>
                                </text>
                            </group>
                        </group>
                    </citationstyle>
                </citationcontenttype>
                <citationcontenttype pid="uk-ac-man-con:7" name="Composition">
                    <citationstyle id="contributors">
                        <group sx="">
                            <alternative>
                                <contributors delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="contributors-linked">
                        <group sx="">
                            <alternative>
                                <contributors-linked delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors-linked>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="source">
                        <xsl:comment>Premiere title: Premiere venue; Premiere date (premiere). Place of publication: Publisher; Year.</xsl:comment>
                        <xsl:comment>m.otherformat.title: m.otherformat.placeterm; m.otherformat.dateother. m.placeterm: m.publisher; m.year.</xsl:comment>
                        <group px="" sx="." delimit=" ">
                            <group px="" sx=" (premiere).">
                                <text px="" sx=": ">
                                    <xsl:value-of select="../str[@name='m.otherformat.title']"/>
                                </text>
                                <text sx="; ">
                                    <xsl:value-of select="../str[@name='m.otherformat.placeterm']"/>
                                </text>
                                <text sx="">
                                    <xsl:value-of select="if (../str[@name='m.otherformat.dateother'] castable as xs:date) then format-date(xs:date(../str[@name='m.otherformat.dateother']),'[Y0001] [MNn,*-3] [D01]') else ''"/>
                                </text>
                            </group>
                            <group px="" sx="; " delimit=": ">
                                <text px="" sx="">
                                    <xsl:value-of select="../str[@name='m.placeterm']"/>
                                </text>
                                <text>
                                    <xsl:value-of select="../str[@name='m.publisher']"/>
                                </text>
                            </group>
                            <text px="" sx="">
                                <xsl:value-of select="../str[@name='m.year']"/>
                            </text>
                        </group>
                    </citationstyle>
                </citationcontenttype>
                <citationcontenttype pid="uk-ac-man-con:9" name="Dissertation">
                    <citationstyle id="contributors">
                        <group sx="">
                            <alternative>
                                <contributors delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="contributors-linked">
                        <group sx="">
                            <alternative>
                                <contributors-linked delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors-linked>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="source">
                        <xsl:comment>[Dissertation]. Place of publication: Publisher; Year.</xsl:comment>
                        <xsl:comment>[Dissertation]. m.placeterm: m.publisher; m.year.</xsl:comment>
                        <group px="[Dissertation]." sx="." delimit=" ">
                            <group px="" sx=";" delimit=": ">
                                <text px="" sx="">
                                    <xsl:value-of select="../str[@name='m.placeterm']"/>
                                </text>
                                <text>
                                    <xsl:value-of select="../str[@name='m.publisher']"/>
                                </text>
                            </group>
                            <text px="" sx="">
                                <xsl:value-of select="../str[@name='m.year']"/>
                            </text>
                        </group>
                    </citationstyle>
                </citationcontenttype>
                <citationcontenttype pid="uk-ac-man-con:10" name="Exhibition">
                    <citationstyle id="contributors">
                        <group sx="">
                            <alternative>
                                <contributors delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.cur']/str"/>
                                </contributors>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.curators']"/>
                                </text>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="contributors-linked">
                        <group sx="">
                            <alternative>
                                <contributors-linked delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.cur']/str"/>
                                </contributors-linked>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.curators']"/>
                                </text>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="source">
                        <xsl:comment>Exhibition venue. Start date - end date.</xsl:comment>
                        <xsl:comment>m.placeterm. m.dateother.start-m.dateother.end.</xsl:comment>
                        <group px="" sx="." delimit=" ">
                            <group px="" sx="">
                                <text sx=".">
                                    <xsl:value-of select="../str[@name='m.placeterm']"/>
                                </text>
                            </group>
                            <group px="" sx="" delimit="-">
                                <text>
                                    <xsl:value-of select="if (../str[@name='m.dateother.start'] castable as xs:date) then format-date(xs:date(../str[@name='m.dateother.start']),'[D01] [MNn,*-3] [Y0001]') else ''"/>
                                </text>
                                <text>
                                    <xsl:value-of select="if (../str[@name='m.dateother.end'] castable as xs:date) then format-date(xs:date(../str[@name='m.dateother.end']),'[D01] [MNn,*-3] [Y0001]') else ''"/>
                                </text>
                            </group>
                        </group>
                    </citationstyle>
                </citationcontenttype>
                <citationcontenttype pid="uk-ac-man-con:11" name="Exhibition contribution">
                    <citationstyle id="contributors">
                        <group sx="">
                            <alternative>
                                <contributors delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.cre']/str"/>
                                </contributors>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.creators']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="contributors-linked">
                        <group sx="">
                            <alternative>
                                <contributors-linked delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.cre']/str"/>
                                </contributors-linked>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.creators']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="source">
                        <xsl:comment>In: Exhibition title: Exhibition venue. Start date - end date.</xsl:comment>
                        <xsl:comment>In: m.host.title: m.host.placeterm. m.host.dateother.start-m.host.dateother.end.</xsl:comment>
                        <group px="In: " sx="." delimit=" ">
                            <group px="" sx="." delimit=": ">
                                <text sx="">
                                    <xsl:value-of select="../str[@name='m.host.title']"/>
                                </text>
                                <text sx="">
                                    <xsl:value-of select="../str[@name='m.host.placeterm']"/>
                                </text>
                            </group>
                            <group px="" sx="" delimit="-">
                                <text>
                                    <xsl:value-of select="if (../str[@name='m.host.dateother.start'] castable as xs:date) then format-date(xs:date(../str[@name='m.host.dateother.start']),'[D01] [MNn,*-3] [Y0001]') else ''"/>
                                </text>
                                <text>
                                    <xsl:value-of select="if (../str[@name='m.host.dateother.end'] castable as xs:date) then format-date(xs:date(../str[@name='m.host.dateother.end']),'[D01] [MNn,*-3] [Y0001]') else ''"/>
                                </text>
                            </group>
                        </group>
                    </citationstyle>
                </citationcontenttype>
                <citationcontenttype pid="uk-ac-man-con:13" name="Patent">
                    <citationstyle id="contributors">
                        <group sx="">
                            <alternative>
                                <contributors delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.pth']/str"/>
                                </contributors>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.patentholders']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="contributors-linked">
                        <group sx="">
                            <alternative>
                                <contributors-linked delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.pth']/str"/>
                                </contributors-linked>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.patentholders']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="source">
                        <xsl:comment>Assignee, Issuing organisation. Patent number. Published date. p. Start-End</xsl:comment>
                        <xsl:comment>m.note.patentassignees, m.publisher. m.identifier.patentnumber. m.dateissued. p. m.host.page.start-m.host.page.end</xsl:comment>
                        <group px="" sx="." delimit=" ">
                            <group sx="" delimit=" ">
                                <text sx=",">
                                    <xsl:value-of select="../str[@name='m.note.patentassignees']"/>
                                </text>
                                <text sx=".">
                                    <xsl:value-of select="../str[@name='m.publisher']"/>
                                </text>
                                <text sx=".">
                                    <xsl:value-of select="../str[@name='m.identifier.patentnumber']"/>
                                </text>
                            </group>
                            <group>
                                <text px="" sx=".">
                                    <xsl:value-of select="if (../str[@name='m.dateissued'] castable as xs:date) then format-date(xs:date(../str[@name='m.dateissued']),'[Y0001] [MNn,*-3] [D01]') else ''"/>
                                </text>
                            </group>
                            <group px=" p. ">
                                <alternative delimit="-">
                                    <text>
                                        <xsl:value-of select="../str[@name='m.host.page.start']"/>
                                    </text>
                                    <text>
                                        <xsl:value-of select="../str[@name='m.host.page.end']"/>
                                    </text>
                                </alternative>
                                <alternative>
                                    <text>
                                        <xsl:value-of select="if (../str[@name='m.host.page.list']!='-') then ../str[@name='m.host.page.list'] else ''"/>
                                    </text>
                                </alternative>
                            </group>
                        </group>
                    </citationstyle>
                </citationcontenttype>
                <citationcontenttype pid="uk-ac-man-con:14" name="Technical report">
                    <citationstyle id="contributors">
                        <group sx="">
                            <alternative>
                                <contributors delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="contributors-linked">
                        <group sx="">
                            <alternative>
                                <contributors-linked delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors-linked>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="source">
                        <xsl:comment>Place of publication: Publisher; Year. Report No.: number.</xsl:comment>
                        <xsl:comment>m.placeterm: m.publisher; m.year. Report No.: m.identifier.reportnumber.</xsl:comment>
                        <group px="" sx="" delimit=" ">
                            <group px="" sx=";" delimit=": ">
                                <text px="" sx="">
                                    <xsl:value-of select="../str[@name='m.placeterm']"/>
                                </text>
                                <text>
                                    <xsl:value-of select="../str[@name='m.publisher']"/>
                                </text>
                            </group>
                            <group delimit=" ">
                                <text px="" sx=".">
                                    <xsl:value-of select="../str[@name='m.year']"/>
                                </text>
                                <text px="Report No. " sx=".">
                                    <xsl:value-of select="../str[@name='m.identifier.reportnumber']"/>
                                </text>
                            </group>
                        </group>
                    </citationstyle>
                </citationcontenttype>
                <citationcontenttype pid="uk-ac-man-con:15" name="Thesis">
                    <citationstyle id="contributors">
                        <group sx="">
                            <alternative>
                                <contributors delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="contributors-linked">
                        <group sx="">
                            <alternative>
                                <contributors-linked delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors-linked>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="source">
                        <xsl:comment>[Thesis]. Place of publication: Publisher; Year.</xsl:comment>
                        <xsl:comment>[Thesis]. m.placeterm: m.publisher; m.year.</xsl:comment>
                        <group px="[Thesis]." sx="." delimit=" ">
                            <group px="" sx=";" delimit=": ">
                                <text px="" sx="">
                                    <xsl:value-of select="../str[@name='m.placeterm']"/>
                                </text>
                                <text>
                                    <xsl:value-of select="../str[@name='m.publisher']"/>
                                </text>
                            </group>
                            <text px="" sx="">
                                <xsl:value-of select="../str[@name='m.year']"/>
                            </text>
                        </group>
                    </citationstyle>
                </citationcontenttype>
                <citationcontenttype pid="uk-ac-man-con:16" name="Working paper">
                    <citationstyle id="contributors">
                        <group sx="">
                            <alternative>
                                <contributors delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="contributors-linked">
                        <group sx="">
                            <alternative>
                                <contributors-linked delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors-linked>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="source">
                        <xsl:comment>Series title. Place of publication: Publisher; Year. Working paper No. number.</xsl:comment>
                        <xsl:comment>m.host.title. m.placeterm: m.publisher; m.year. Working paper no. m.identifier.workingpapernumber.</xsl:comment>
                        <group sx="." delimit=" ">
                            <group>
                                <text sx=".">
                                    <alternative>
                                        <text>
                                            <xsl:value-of select="../str[@name='m.host.title']"/>
                                        </text>
                                    </alternative>
                                    <alternative>
                                        <text>
                                            <xsl:value-of select="../str[@name='m.host.title.abbreviated']"/>
                                        </text>
                                    </alternative>
                                </text>
                            </group>
                            <group px="" sx=";" delimit=": ">
                                <text px="" sx="">
                                    <xsl:value-of select="../str[@name='m.placeterm']"/>
                                </text>
                                <text>
                                    <xsl:value-of select="../str[@name='m.publisher']"/>
                                </text>
                            </group>
                            <group px="" sx=".">
                                <text px="" sx="">
                                    <xsl:value-of select="../str[@name='m.year']"/>
                                </text>
                            </group>
                            <group px="Working Paper No." sx="">
                                <text px=" " sx="">
                                    <xsl:value-of select="../str[@name='m.identifier.workingpapernumber']"/>
                                </text>
                            </group>
                        </group>
                    </citationstyle>
                </citationcontenttype>
                <citationcontenttype pid="uk-ac-man-con:17" name="Journal contribution">
                    <citationstyle id="contributors">
                        <group sx="">
                            <alternative>
                                <contributors delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="contributors-linked">
                        <group sx="">
                            <alternative>
                                <contributors-linked delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors-linked>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="source">
                        <text sx=".">
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.host.title']"/>
                                </text>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.host.title.abbreviated']"/>
                                </text>
                            </alternative>
                        </text>
                        <group px=" " sx="">
                            <text px="" sx=";">
                                <xsl:value-of select="../str[@name='m.year']"/>
                            </text>
                            <group px="" sx="">
                                <text px="" sx="">
                                    <xsl:value-of select="../str[@name='m.host.volume']"/>
                                </text>
                                <text px="(" sx=")">
                                    <xsl:value-of select="../str[@name='m.host.issue']"/>
                                </text>
                            </group>
                            <group px=":">
                                <alternative>
                                    <text>
                                        <xsl:value-of select="../str[@name='m.host.page.start']"/>
                                    </text>
                                    <text px="-">
                                        <xsl:value-of select="../str[@name='m.host.page.end']"/>
                                    </text>
                                </alternative>
                                <alternative>
                                    <text>
                                        <xsl:value-of select="if (../str[@name='m.host.page.list']!='-') then ../str[@name='m.host.page.list'] else ''"/>
                                    </text>
                                </alternative>
                            </group>
                        </group>
                    </citationstyle>
                </citationcontenttype>
                <citationcontenttype pid="uk-ac-man-con:18" name="Newspaper/magazine contribution">
                    <citationstyle id="contributors">
                        <group sx="">
                            <alternative>
                                <contributors delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="contributors-linked">
                        <group sx="">
                            <alternative>
                                <contributors-linked delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors-linked>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="source">
                        <xsl:comment>Newspaper/magazine title. Edition ed. Date of publication;Volume(issue).section:Pagination.</xsl:comment>
                        <xsl:comment>m.host.title. m.dateissued;m.host.volume(m.host.issue).m.host.section:m.host.page.start-m.host.page.end</xsl:comment>
                        <group px="" sx="." delimit=" ">
                            <group px="" sx=".">
                                <text>
                                    <xsl:value-of select="../str[@name='m.host.title']"/>
                                </text>
                            </group>
                            <group>
                                <text sx=";">
                                    <xsl:value-of select="if (../str[@name='m.dateissued'] castable as xs:date) then format-date(xs:date(../str[@name='m.dateissued']),'[Y0001] [MNn,*-3] [D01]') else ''"/>
                                </text>
                                <text px="" sx="">
                                    <xsl:value-of select="../str[@name='m.host.volume']"/>
                                </text>
                                <text px="(" sx=")">
                                    <xsl:value-of select="../str[@name='m.host.issue']"/>
                                </text>
                                <text px="." sx="">
                                    <xsl:value-of select="../str[@name='m.host.section']"/>
                                </text>
                                <group px=":">
                                    <alternative>
                                        <group delimit="-">
                                            <text>
                                                <xsl:value-of select="../str[@name='m.host.page.start']"/>
                                            </text>
                                            <text>
                                                <xsl:value-of select="../str[@name='m.host.page.end']"/>
                                            </text>
                                        </group>
                                    </alternative>
                                    <alternative>
                                        <text>
                                            <xsl:value-of select="if (../str[@name='m.host.page.list']!='-') then ../str[@name='m.host.page.list'] else ''"/>
                                        </text>
                                    </alternative>
                                </group>
                            </group>
                        </group>
                    </citationstyle>
                </citationcontenttype>
                <citationcontenttype pid="uk-ac-man-con:19" name="Lite-cite submission">
                    <citationstyle id="source">
                        <xsl:comment>Version. Year.</xsl:comment>
                        <xsl:comment>m.genre.version. m.year.</xsl:comment>
                        <group px="" sx="." delimit=" ">
                            <text sx=".">
                                <xsl:value-of select="../str[@name='m.genre.version']"/>
                            </text>
                            <text px="" sx="">
                                <xsl:value-of select="../str[@name='m.year']"/>
                            </text>
                        </group>
                    </citationstyle>
                </citationcontenttype>
                <citationcontenttype pid="uk-ac-man-con:20" name="Administered thesis">
                    <citationstyle id="contributors">
                        <group sx="">
                            <alternative>
                                <contributors delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="contributors-linked">
                        <group sx="">
                            <alternative>
                                <contributors-linked delimit="; ">
                                    <xsl:copy-of select="../arr[@name='m.name.aut']/str"/>
                                </contributors-linked>
                            </alternative>
                            <alternative>
                                <text>
                                    <xsl:value-of select="../str[@name='m.note.authors']"/>
                                </text>
                            </alternative>
                        </group>
                    </citationstyle>
                    <citationstyle id="source">
                        <xsl:comment>[Thesis]. Place of publication: Publisher; Year.</xsl:comment>
                        <xsl:comment>[Thesis]. m.placeterm: m.publisher; m.year.</xsl:comment>
                        <group px="[Thesis]." sx="." delimit=" ">
                            <group px=" " sx=";" delimit=": ">
                                <text>
                                    <xsl:value-of select="../str[@name='m.placeterm']"/>
                                </text>
                                <text>
                                    <xsl:value-of select="../str[@name='m.publisher']"/>
                                </text>
                            </group>
                            <text px=" " sx="">
                                <xsl:value-of select="../str[@name='m.year']"/>
                            </text>
                        </group>
                    </citationstyle>
                </citationcontenttype>
            </citationstyles>
        </xsl:variable>
        <xsl:variable name="contenttypepid" select="."/>
        <xsl:apply-templates select="$citationstyles/citationstyles/citationcontenttype[@pid = $contenttypepid]/citationstyle[@id = $format]" mode="#current">
            <xsl:with-param name="format" select="$format"/>
        </xsl:apply-templates>
    </xsl:template>
    <xsl:template match="citationstyle" mode="citation">
            <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="contributors" mode="citation">
        <xsl:for-each select="*">
            <xsl:variable name="contributor">
                <xsl:apply-templates select="text()" mode="#current"/>
            </xsl:variable>
            <xsl:if test="position()&gt;1">
                <xsl:apply-templates select="../@delimit" mode="#current"/>
            </xsl:if>
            <xsl:value-of select="$contributor"/>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="contributors-linked" mode="citation">
        <xsl:for-each select="*">
            <xsl:variable name="contributor">
                <xsl:apply-templates select="text()" mode="#current"/>
            </xsl:variable>
            <xsl:if test="position()&gt;1">
                <xsl:apply-templates select="../@delimit" mode="#current"/>
            </xsl:if>
            <a>
                <xsl:attribute name="href">
                    <xsl:value-of select="concat($searchurl,$contributor)"/>
                </xsl:attribute>
                <xsl:attribute name="class">
                    <xsl:value-of select="'linktosearch'"/>
                </xsl:attribute>
                <xsl:value-of select="$contributor"/>
            </a>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="text | group" mode="citation">
        <xsl:variable name="pos">
            <xsl:number/>
        </xsl:variable>
        <xsl:variable name="result">
            <xsl:apply-templates mode="#current"/>
        </xsl:variable>
        <xsl:if test="$pos&gt;1 and preceding-sibling::*[1]!='' and .!=''">
            <xsl:apply-templates select="../@delimit" mode="#current"/>
        </xsl:if>
        <xsl:if test="$result!=''">
            <xsl:apply-templates select="@px" mode="#current"/>
            <xsl:value-of select="$result"/>
            <xsl:apply-templates select="@sx" mode="#current"/>
        </xsl:if>
    </xsl:template>
    <xsl:template match="*[alternative]" mode="citation">
        <xsl:variable name="result">
            <xsl:apply-templates select="alternative[*/text() or exists(*/*)][1]/*" mode="#current"/>
        </xsl:variable>
        <xsl:if test="$result!=''">
            <xsl:apply-templates select="@px" mode="#current"/>
            <xsl:copy-of select="$result"/>
            <xsl:apply-templates select="@sx" mode="#current"/>
        </xsl:if>
    </xsl:template>
    <xsl:template match="text()" mode="citation">
        <xsl:choose>
            <xsl:when test="substring(., string-length(.)) = '.'">
                <xsl:value-of select="substring(.,1,string-length(.)-1)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="@px | @sx | @delimit" mode="citation">
        <xsl:value-of select="."/>
    </xsl:template>
    <xsl:template match="@*|*" mode="citation"/>
</xsl:stylesheet>
