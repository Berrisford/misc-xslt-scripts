<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:local="http://localhost" xmlns:saxon="http://saxon.sf.net/" exclude-result-prefixes="#all" version="2.0">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:output name="serialise1" encoding="utf-8" method="xhtml" indent="no" omit-xml-declaration="yes"/>
  <xsl:strip-space elements="*"/>
   <xsl:preserve-space elements="text"/>

    <xsl:include href="solr2citations.xsl"/>
    
    <xsl:variable name="searchurl" select="''"/>
    <xsl:variable name="baseuri" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/scw/select/'"/>
<!-- Use this variable value to update XML with modifications made in last 3 months -->
    <!--	<xsl:variable name="query" select="'x.lastmodifieddate:[NOW-3MONTH TO NOW]'"/> -->
<!-- Use this variable value to update XML of all records -->
    <!--xsl:variable name="query" select="'x.lastmodifieddate:[2012-01-01T00:00:00Z TO NOW]'"/>-->
    <xsl:variable name="query" select="'uk-ac-man-scw?198069'"/>
    <xsl:variable name="docsource" select="doc('index_old.xml')"/>
    <xsl:variable name="docrows" select="doc(concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=0&amp;indent=off'))/response/result[@name='response']/@numFound"/>

    <xsl:variable name="docupdates">
        <xsl:variable name="docmodified" select="doc(concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=',$docrows,'&amp;indent=off'))"/>
        <xsl:apply-templates select="$docmodified" mode="update"/>
    </xsl:variable>

    <xsl:template match="/">
        <collection>
            <xsl:apply-templates select="$docupdates/collection/record[exists(leader) and leader!=''] | $docsource/collection/record[not(controlfield[@tag='001'] = $docupdates/collection/record/controlfield[@tag='001'])]"/>
        </collection>
    </xsl:template>

    <xsl:template match="record">
        <xsl:copy-of select="."/>
    </xsl:template>

    <xsl:template match="/" mode="update">
        <xsl:apply-templates select="response/result[@name='response']" mode="#current"/>
    </xsl:template>

    <xsl:template match="result" mode="update">
        <collection>
            <xsl:apply-templates select="doc" mode="#current"/>
        </collection>
    </xsl:template>

    <xsl:template match="doc" mode="update">
        <record>
            <xsl:apply-templates select=".[str[@name='x.state']='Active' and exists(str[@name='m.title']) and normalize-space(str[@name='m.title'])!='' and exists(arr[@name='r.isbelongsto.pid']/str) and (exists(arr[@name='f.isfileattached']/str[.='Yes'] or exists(str[@name='m.identifier.doi'])))]" mode="leaderfield"/>
            <xsl:apply-templates select="." mode="controlfields"/>
            <datafield tag="245" ind1=" " ind2=" ">
                <xsl:apply-templates select="str[@name='m.title']"/>
                <xsl:apply-templates select="str[@name='m.subtitle']"/>
            </datafield>
            <xsl:apply-templates select="str[@name='m.dateissued']"/>
            <xsl:apply-templates select="str[@name='m.abstract']"/>
            <xsl:apply-templates select="arr[@name='m.topic']"/>
            <xsl:apply-templates select="str[@name='r.isofcontenttype.pid']" mode="contributors"/>
            <xsl:apply-templates select="str[@name='r.isofcontenttype.pid']" mode="source"/>
        </record>
    </xsl:template>

    <xsl:template match="doc" mode="leaderfield">
        <leader>
            <!-- 00-04 -->
            <xsl:text>     </xsl:text>
            <!-- 05 -->
            <xsl:text>n</xsl:text>
            <!-- 06 -->
            <xsl:text>o</xsl:text>
            <!-- 07 -->
            <xsl:text>a</xsl:text>
            <!-- 08 -->
            <xsl:text> </xsl:text>
            <!-- 09 -->
            <xsl:text>a</xsl:text>
            <!-- 10 -->
            <xsl:text>2</xsl:text>
            <!-- 11 -->
            <xsl:text>2</xsl:text>
            <!-- 12-16 -->
            <xsl:text>     </xsl:text>
            <!-- 17 -->
            <xsl:text>z</xsl:text>
            <!-- 18 -->
            <xsl:text>u</xsl:text>
            <!-- 19 -->
            <xsl:text> </xsl:text>
            <!-- 20-23 -->
            <xsl:text>4500</xsl:text>
        </leader>
    </xsl:template>
    <xsl:template match="doc" mode="controlfields">
        <controlfield tag="001">
            <xsl:apply-templates select="str[@name='PID']/text()"/>
        </controlfield>
        <controlfield tag="003">Manchester eScholar</controlfield>
        <controlfield tag="005">
            <xsl:apply-templates select="date[@name='x.lastmodifieddate']" mode="date"/>
        </controlfield>
    </xsl:template>
    <xsl:template match="str[@name='PID']">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="str[@name='m.title']">
        <subfield code="a">
            <xsl:apply-templates select="text()"/>
        </subfield>
    </xsl:template>
    <xsl:template match="str[@name='m.subtitle']">
        <subfield code="b">
            <xsl:apply-templates select="text()"/>
        </subfield>
    </xsl:template>
    <xsl:template match="str[@name='m.dateissued']">
        <datafield tag="260" ind1=" " ind2=" ">
            <subfield code="c">
                <xsl:apply-templates select="text()"/>
            </subfield>
        </datafield>
    </xsl:template>
    <xsl:template match="str[@name='m.abstract']">
        <datafield tag="520" ind1=" " ind2=" ">
            <subfield code="a">
                <xsl:apply-templates select="text()"/>
            </subfield>
        </datafield>
    </xsl:template>
    <xsl:template match="arr[@name='m.topic']">
        <xsl:choose>
            <xsl:when test="str[string-length(.)&lt;50]">
                <datafield tag="650" ind1=" " ind2=" ">
                    <xsl:apply-templates select="str[string-length(.)&lt;50]"/>
                </datafield>                
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="arr[@name='m.topic']/str">
        <subfield code="a">
            <xsl:apply-templates select="text()"/>
        </subfield>
    </xsl:template>
    <xsl:template match="str[@name='r.isofcontenttype.pid']" mode="contributors">
        <datafield tag="700" ind1=" " ind2=" ">
            <subfield code="a">
                <xsl:apply-templates select="." mode="citation">
                    <xsl:with-param name="format" select="'contributors'"/>
                </xsl:apply-templates>
            </subfield>
        </datafield>
    </xsl:template>
    <xsl:template match="str[@name='r.isofcontenttype.pid']" mode="source">
        <datafield tag="773" ind1=" " ind2=" ">
            <subfield code="a">
                <xsl:apply-templates select="." mode="citation">
                    <xsl:with-param name="format" select="'source'"/>
                </xsl:apply-templates>
            </subfield>
        </datafield>
    </xsl:template>
    <xsl:template match="text()">
        <xsl:value-of select="."/>
    </xsl:template>
    <xsl:template match="text()" mode="date">
        <xsl:variable name="date" select="." as="xs:dateTime"/>
        <xsl:value-of select="format-dateTime($date,'[Y0001][M01][D01][H01][m01][s01].[f]')"/>
    </xsl:template>
    <xsl:template match="*"/>
</xsl:stylesheet>
