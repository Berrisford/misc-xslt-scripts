<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    xmlns:commons="v3.commons.pure.atira.dk"
    xmlns:v1="v1.activity.pure.atira.dk"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="v1.activity.pure.atira.dk activity.xsd"    
    version="3.0"
    >
    <xsl:output method="xml" indent="yes" encoding="UTF-8" />
    
    <xsl:template match="/">
        <xsl:result-document href="activity_output.xml">
            <xsl:element name="v1:activities">
                <xsl:copy-of select="document('')/*/@xsi:schemaLocation"/>                
                <xsl:apply-templates select="/activities-awards/activity-award"/>
            </xsl:element>
        </xsl:result-document>
    </xsl:template>
   
   <xsl:template match="activity-award">
        <xsl:element name="v1:award">
            <xsl:attribute name="id" select="id"/>
            <xsl:attribute name="type" select="type"/>
                <v1:personAndPeriode>
                   <xsl:element name="v1:person">
                       <xsl:attribute name="id" select="person_id"/>
                   </xsl:element>
                   <v1:role>recipient</v1:role>
                   <v1:roleDescription>
                        <commons:text><xsl:value-of select="description"/></commons:text>
                   </v1:roleDescription>
                    <v1:startDate>
                       <xsl:choose>
                           <xsl:when test="year ne ''">
                               <commons:year><xsl:value-of select="year"/></commons:year>                               
                           </xsl:when>
                           <xsl:otherwise>
                               <commons:year><xsl:value-of select="'2099'"/></commons:year>                                                              
                           </xsl:otherwise>
                       </xsl:choose>
                   </v1:startDate>  
               </v1:personAndPeriode>
               <v1:organisationAssociations>
                   <xsl:element name="v1:organisationAssociation">
                       <xsl:attribute name="id" select="organisation_id"/>
                   </xsl:element>                   
               </v1:organisationAssociations>
               <v1:visibility>Confidential</v1:visibility>
               <v1:externalRelation>
                   <v1:externalOrganisation>
                       <commons:name><xsl:value-of select="external_org_name"/></commons:name>
                   </v1:externalOrganisation>
               </v1:externalRelation>
               <v1:title>
                   <commons:text country="GB" lang="en"><xsl:value-of select="activity_title"/></commons:text>
               </v1:title>
        </xsl:element>
      
   </xsl:template>
   
</xsl:stylesheet>