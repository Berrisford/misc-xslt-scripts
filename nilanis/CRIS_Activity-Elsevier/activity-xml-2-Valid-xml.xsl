<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0"
    xmlns:commons="v3.commons.pure.atira.dk" xmlns="v1.activity.pure.atira.dk"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="v1.activity.pure.atira.dk activity.xsd"    
    >
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <activities>
            <xsl:apply-templates select="/awards/award"/>
        </activities>
    </xsl:template>
   
   <xsl:template match="award">
        <xsl:element name="award">
            <xsl:attribute name="id" select="id"/>
            <xsl:attribute name="type" select="'other'"/>
               <abstract>
                   <commons:text><xsl:value-of select="description"/></commons:text>
               </abstract>
               <personAndPeriode>
                   <xsl:element name="person">
                       <commons:firstName><xsl:value-of select="first-name"/></commons:firstName>
                       <commons:lastName><xsl:value-of select="last-name"/></commons:lastName>                       
                   </xsl:element>
                   <role>other</role>
                   <xsl:choose>
                       <xsl:when test="start-date ne ''">
                           <startDate>
                               <xsl:choose>
                                   <xsl:when test="not(contains(start-date, '/'))">
                                       <commons:year><xsl:value-of select="start-date"/></commons:year>                                       
                                   </xsl:when>
                                   <xsl:otherwise>
                                       <commons:year><xsl:value-of select="tokenize(start-date, '/')[3]"/></commons:year>                                       
                                       <commons:month><xsl:value-of select="tokenize(start-date, '/')[2]"/></commons:month>
                                       <commons:day><xsl:value-of select="tokenize(start-date, '/')[1]"/></commons:day>
                                   </xsl:otherwise>
                               </xsl:choose>
                           </startDate>
                       </xsl:when>
                   </xsl:choose>
                   <xsl:choose>
                       <xsl:when test="end-date ne ''">
                           <endDate>
                               <xsl:choose>
                                   <xsl:when test="not(contains(end-date, '/'))">
                                       <commons:year><xsl:value-of select="end-date"/></commons:year>                                       
                                   </xsl:when>
                                   <xsl:otherwise>
                                       <commons:year><xsl:value-of select="tokenize(end-date, '/')[3]"/></commons:year>                                       
                                       <commons:month><xsl:value-of select="tokenize(end-date, '/')[2]"/></commons:month>
                                       <commons:day><xsl:value-of select="tokenize(end-date, '/')[1]"/></commons:day>
                                   </xsl:otherwise>
                               </xsl:choose>
                           </endDate>
                       </xsl:when>
                   </xsl:choose>                   
               </personAndPeriode>
               <organisationAssociations>
                   <xsl:element name="organisationAssociation">
                       <xsl:attribute name="id" select="replace(organisation-id, '\|', '')"/>
                       <organisationName>University of Manchester (UOM)</organisationName>                       
                   </xsl:element>                   
               </organisationAssociations>
               <visibility>Public</visibility>
               <externalRelation>
                   <event id="1">
                       <abbreviatedTitle>
                           <commons:text>abbreviatedTitle</commons:text>
                       </abbreviatedTitle>
                       <city>Manchester</city>
                       <conferenceNumber>1234</conferenceNumber>
                       <country>UK</country>
                       <xsl:choose>
                           <xsl:when test="start-date ne ''">
                               <startDate>
                                   <xsl:choose>
                                       <xsl:when test="not(contains(start-date, '/'))">
                                           <commons:year><xsl:value-of select="start-date"/></commons:year>                                       
                                       </xsl:when>
                                       <xsl:otherwise>
                                           <commons:year><xsl:value-of select="tokenize(start-date, '/')[3]"/></commons:year>                                       
                                           <commons:month><xsl:value-of select="tokenize(start-date, '/')[2]"/></commons:month>
                                           <commons:day><xsl:value-of select="tokenize(start-date, '/')[1]"/></commons:day>
                                       </xsl:otherwise>
                                   </xsl:choose>
                               </startDate>
                           </xsl:when>
                       </xsl:choose>
                       <xsl:choose>
                           <xsl:when test="end-date ne ''">
                               <endDate>
                                   <xsl:choose>
                                       <xsl:when test="not(contains(end-date, '/'))">
                                           <commons:year><xsl:value-of select="end-date"/></commons:year>                                       
                                       </xsl:when>
                                       <xsl:otherwise>
                                           <commons:year><xsl:value-of select="tokenize(end-date, '/')[3]"/></commons:year>                                       
                                           <commons:month><xsl:value-of select="tokenize(end-date, '/')[2]"/></commons:month>
                                           <commons:day><xsl:value-of select="tokenize(end-date, '/')[1]"/></commons:day>
                                       </xsl:otherwise>
                                   </xsl:choose>
                               </endDate>
                           </xsl:when>
                       </xsl:choose> 
                       <location>Manchester</location>
                       <sponsorOrganisation>University of Manchester</sponsorOrganisation>
                       <subTitle>
                           <commons:text>sub title</commons:text>
                       </subTitle>
                       <title>
                           <commons:text>title</commons:text>
                       </title>
                   </event>
               </externalRelation>
               <title>
                   <commons:text country="GB" lang="en">Test</commons:text>
               </title>
        </xsl:element>
      
   </xsl:template>
   
</xsl:stylesheet>