<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"/>
    
    <xsl:template match="/">        
        <doc>
            <xsl:for-each select="distinct-values(response/result/doc/str)">
                <str>
                    <xsl:value-of select="."/>
                </str>
            </xsl:for-each>            
        </doc>
    </xsl:template>
<!--    
    <xsl:template match="str">
        <xsl:value-of select="."/>
    </xsl:template>
-->    
</xsl:stylesheet>