<?xml version="1.0"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs">
	<xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="no"/>
	<xsl:variable name="baseuri" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/etd/select/'"/>
	<xsl:variable name="baseuri2" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/nonscw/select/'"/>
	<xsl:template match="/">
		<pendingApprovalSummarys>
			<objects>
				<xsl:apply-templates select="response/result/doc"/>
			</objects>
		</pendingApprovalSummarys>
	</xsl:template>
	<xsl:template match="doc">
		<object>
			<studentname><xsl:apply-templates select="arr[@name='r.isbelongsto.source']" mode="name"/></studentname>
			<studentpartynumber><xsl:apply-templates select="arr[@name='r.isbelongsto.source']" mode="id"/></studentpartynumber>
			<submission-date><xsl:apply-templates select="date[@name='x.createddate']"/></submission-date>
			<restrictionduration><xsl:value-of select="str[@name='d.initialrestrictionduration']"/></restrictionduration>
			<restrictionreason><xsl:value-of select="str[@name='d.accessrestriction.reason']"/></restrictionreason>
			<restrictionreasonother><xsl:value-of select="str[@name='d.accessrestriction.reasonother']"/></restrictionreasonother>
			<blockedcareer><xsl:value-of select="str[@name='d.blockedcareer']"/></blockedcareer>
<!--			<supervisorid><xsl:apply-templates select="arr[@name='r.hasmainsupervisor.source']" mode="id"/></supervisorid>
			<supervisorname><xsl:apply-templates select="arr[@name='r.hasmainsupervisor.source']" mode="name"/></supervisorname>
-->						
			<xsl:variable name="sup_id" select="arr[@name='r.hasmainsupervisor.pid']"/>
			
			<xsl:choose>
				<xsl:when test="exists($sup_id)">
					<xsl:for-each select="arr[@name='r.hasmainsupervisor.pid']/str/text()">
						<xsl:variable name="sup_pid1" select="replace(.,':','%5C:')"/>
						<xsl:variable name="query" select="concat('PID:',$sup_pid1)"/>						
						<xsl:variable name="src" select="concat($baseuri2,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=1&amp;indent=off&amp;fl=p.email,p.displayname,p.partynumber')"/>
						<supervisorid><xsl:apply-templates select="doc($src)/response/result/doc/str[@name='p.partynumber']"/></supervisorid>
						<supervisorname><xsl:apply-templates select="doc($src)/response/result/doc/str[@name='p.displayname']"/></supervisorname>
						<supervisoremail><xsl:apply-templates select="doc($src)/response/result/doc/str[@name='p.email']"/></supervisoremail>						
					</xsl:for-each>
<!--					<xsl:variable name="sup_pid" select="arr[@name='r.hasmainsupervisor.pid']/str/text()"/>
					<xsl:variable name="sup_pid1" select="replace($sup_pid,':','%5C:')"/>
-->
				</xsl:when>
				<xsl:otherwise>
					<supervisorid></supervisorid>
					<supervisorname></supervisorname>
					<supervisoremail></supervisoremail>
				</xsl:otherwise>
			</xsl:choose>
			
			
			
			
			<!--
			<studentpid><xsl:apply-templates select="arr[@name='r.isbelongsto.source']" mode="pid"/></studentpid>
			<scwpid><xsl:apply-templates select="str[@name='PID']"/></scwpid>
			<embargoapprovalstatus><xsl:value-of select="str[@name='d.embargoapprovalstatus']"/></embargoapprovalstatus>
			<permissiontodownload><xsl:value-of select="str[@name='d.permissiontodownload']"/></permissiontodownload>
			<restrictionduration><xsl:value-of select="str[@name='d.accessrestriction.duration']"/></restrictionduration>
			<supervisoroverrides><xsl:value-of select="str[@name='d.supervisoroverrides']"/></supervisoroverrides>
			<etdpid><xsl:value-of select="arr[@name='r.isbelongstoetdwindow.pid']"/></etdpid>
			-->	
			
					
			<xsl:variable name="pid" select="arr[@name='r.isbelongstoetdwindow.pid']/str"/>
			<xsl:variable name="pid1" select="replace($pid,':','%5C:')"/>
			<xsl:variable name="query" select="concat('PID:',$pid1)"/>
		
			<xsl:variable name="src" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=1&amp;indent=off&amp;fl=e.programmeid,e.planid,e.programmename,e.planname,r.iscreatedby.displayname')"/>
			<programmeid><xsl:apply-templates select="doc($src)/response/result/doc/str[@name='e.programmeid']"/></programmeid>
			<programmename><xsl:apply-templates select="doc($src)/response/result/doc/str[@name='e.programmename']"/></programmename>
			<planid><xsl:apply-templates select="doc($src)/response/result/doc/str[@name='e.planid']"/></planid>
			<planname><xsl:apply-templates select="doc($src)/response/result/doc/str[@name='e.planname']"/></planname>
			<window-opened-by><xsl:apply-templates select="doc($src)/response/result/doc/str[@name='r.iscreatedby.displayname']"/></window-opened-by>
			
			
			
			
			
			<!--
			<xsl:variable name="studentpid"><xsl:apply-templates select="arr[@name='r.isbelongsto.source']" mode="pid"/></xsl:variable>
			<xsl:variable name="studentpid1" select="replace($studentpid,':','%5C:')"></xsl:variable>
			<xsl:variable name="query2" select="concat('PID:',$studentpid1)"/>
			<xsl:variable name="src2" select="concat($baseuri2,'?q=',$query2,'&amp;version=2.2&amp;start=0&amp;rows=1&amp;indent=off&amp;fl=p.email')"/>
			<studentemail><xsl:apply-templates select="doc($src2)/response/result/doc/str[@name='p.email']"/></studentemail>
			-->
			
		</object>
	</xsl:template>
	
	<xsl:template match="arr[@name='r.hasmainsupervisor.source']" mode="name">
		<xsl:value-of select="tokenize(.,'\|\|')[4]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.hasmainsupervisor.source']" mode="id">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.isbelongsto.source']" mode="name">
		<xsl:value-of select="tokenize(.,'\|\|')[4]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.isbelongsto.source']" mode="id">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.isbelongsto.source']" mode="pid">
		<xsl:value-of select="tokenize(.,'\|\|')[2]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.isbelongsto.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[2]"/>
	</xsl:template>
	<xsl:template match="date[@name='x.createddate']">
		<xsl:value-of select="format-dateTime(xs:dateTime(.),'[D01] [MNn] [Y0001] [H01]:[m01]:[s01]')"/>
	</xsl:template> 
	<xsl:template match="str[@name='PID']">
		<xsl:value-of select="substring-after(.,':')"/>
	</xsl:template>
</xsl:stylesheet>

