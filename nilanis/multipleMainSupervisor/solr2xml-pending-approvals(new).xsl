<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:math="http://www.w3.org/2005/xpath-functions/math"
	exclude-result-prefixes="xs math"
	version="3.0">    
	<xsl:output indent="yes" method="xml" omit-xml-declaration="no"/>
		
	<xsl:variable name="baseuri" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/etd/select/'"/>
	<xsl:variable name="baseuri2" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/nonscw/select/'"/>
	
	<xsl:template match="/">
		<pendingApprovalSummarys>
			<objects>
				<xsl:apply-templates select="response/result/doc"/>
			</objects>
		</pendingApprovalSummarys>
	</xsl:template>
	
	<xsl:template match="doc">
		<object>
			<studentname><xsl:apply-templates select="arr[@name='r.isbelongsto.source']" mode="name"/></studentname>
			<studentpartynumber><xsl:apply-templates select="arr[@name='r.isbelongsto.source']" mode="id"/></studentpartynumber>
			<submission-date><xsl:apply-templates select="date[@name='x.createddate']"/></submission-date>
			<restrictionduration><xsl:value-of select="str[@name='d.initialrestrictionduration']"/></restrictionduration>
			<restrictionreason><xsl:value-of select="str[@name='d.accessrestriction.reason']"/></restrictionreason>
			<restrictionreasonother><xsl:value-of select="str[@name='d.accessrestriction.reasonother']"/></restrictionreasonother>
			<blockedcareer><xsl:value-of select="str[@name='d.blockedcareer']"/></blockedcareer>
			
			<xsl:variable name="sup_id" select="arr[@name='r.hasmainsupervisor.pid']"/>

			<xsl:choose>
				<xsl:when test="exists($sup_id)">
					<xsl:for-each select="arr[@name='r.hasmainsupervisor.pid']/str">
						<xsl:variable name="sup_pid1" select="replace(.,':','%5C:')"/>
						<xsl:variable name="query" select="concat('PID:',$sup_pid1)"/>						
						
						<xsl:variable name="obj_src">
							<xsl:copy-of select="doc(concat($baseuri2,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=1&amp;indent=off&amp;fl=p.email,p.displayname,p.partynumber'))"/>
						</xsl:variable>

						<xsl:variable name="count">
							<xsl:number/>
						</xsl:variable>
						
						<xsl:variable name="id_node_name" select="concat('supervisorid', $count)"/>
						<xsl:element name="{$id_node_name}">
							<xsl:value-of select="$obj_src/response/result/doc/str[@name='p.partynumber']/text()"></xsl:value-of>
						</xsl:element>

						<xsl:variable name="name_node_name" select="concat('supervisorname', $count)"/>
						<xsl:element name="{$name_node_name}">
							<xsl:value-of select="$obj_src/response/result/doc/str[@name='p.displayname']/text()"></xsl:value-of>
						</xsl:element>
						
						<xsl:variable name="email_node_name" select="concat('supervisoremail', $count)"/>
						<xsl:element name="{$email_node_name}">
							<xsl:value-of select="$obj_src/response/result/doc/str[@name='p.email']/text()"></xsl:value-of>
						</xsl:element>						
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<supervisorid1></supervisorid1>
					<supervisorname1></supervisorname1>
					<supervisoremail1></supervisoremail1>
				</xsl:otherwise>
			</xsl:choose>							
					
			<xsl:variable name="pid" select="arr[@name='r.isbelongstoetdwindow.pid']/str"/>
			<xsl:variable name="pid1" select="replace($pid,':','%5C:')"/>
			<xsl:variable name="query" select="concat('PID:',$pid1)"/>		
<!--			<xsl:variable name="src" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=1&amp;indent=off&amp;fl=e.programmeid,e.planid,e.programmename,e.planname,r.iscreatedby.displayname')"/>-->

			<xsl:variable name="obj_src2">
				<xsl:copy-of select="doc(concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=1&amp;indent=off&amp;fl=e.programmeid,e.planid,e.programmename,e.planname,r.iscreatedby.displayname'))"/>
			</xsl:variable>			

			<programmeid><xsl:apply-templates select="$obj_src2/response/result/doc/str[@name='e.programmeid']"/></programmeid>
			<programmename><xsl:apply-templates select="$obj_src2/response/result/doc/str[@name='e.programmename']"/></programmename>
			<planid><xsl:apply-templates select="$obj_src2/response/result/doc/str[@name='e.planid']"/></planid>
			<planname><xsl:apply-templates select="$obj_src2/response/result/doc/str[@name='e.planname']"/></planname>
			<window-opened-by><xsl:apply-templates select="$obj_src2/response/result/doc/str[@name='r.iscreatedby.displayname']"/></window-opened-by>
						
		</object>
	</xsl:template>
	
	<xsl:template match="arr[@name='r.hasmainsupervisor.source']" mode="name">
		<xsl:value-of select="tokenize(.,'\|\|')[4]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.hasmainsupervisor.source']" mode="id">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.isbelongsto.source']" mode="name">
		<xsl:value-of select="tokenize(.,'\|\|')[4]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.isbelongsto.source']" mode="id">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.isbelongsto.source']" mode="pid">
		<xsl:value-of select="tokenize(.,'\|\|')[2]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.isbelongsto.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[2]"/>
	</xsl:template>
	<xsl:template match="date[@name='x.createddate']">
		<xsl:value-of select="format-dateTime(xs:dateTime(.),'[D01] [MNn] [Y0001] [H01]:[m01]:[s01]')"/>
	</xsl:template> 
	<xsl:template match="str[@name='PID']">
		<xsl:value-of select="substring-after(.,':')"/>
	</xsl:template>
</xsl:stylesheet>

