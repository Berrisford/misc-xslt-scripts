<?xml version="1.0"?>
<xsl:stylesheet version="2.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:date="http://exslt.org/dates-and-times" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="#all">

  <xsl:output indent="no" method="xhtml" omit-xml-declaration="yes"/>
  <!-- 
    *************************************************   
    NOTE: baseuri needs to be changed between servers
    *************************************************    
  -->
  <xsl:variable name="baseuri" select="'https://www.escholar.manchester.ac.uk/'"/>
  <xsl:variable name="windowtypes" select="document(concat('http://localhost:8081/','myetd/getETDWindowTypes.do?para=h0Pw1l'))/results/result"/>  
  <!-- 
    XML variable which defines the prefered precedence of nodes
    Nodes higher up XML have higher display precedence
    NOTE: if a row exists in source XML that doesn't match 
    one of prefered nodes then node message  will not display -->
  <xsl:variable name="order">
    <etdwindows>
      <etdwindow>
        <status>Awaiting submission</status>
      </etdwindow>
      <etdwindow>
        <status>Pending</status>
      </etdwindow>
      <etdwindow>
        <status>Expired</status>
      </etdwindow>
      <etdwindow>
        <status>Cancelled</status>
      </etdwindow>
      <etdwindow>
        <status>Deposited</status>
      </etdwindow>
      <etdwindow>
        <status>Acknowledged</status>
      </etdwindow>
      <etdwindow>
        <status>Rejected</status>
      </etdwindow>
      <etdwindow>
        <status>Sent to library</status>
      </etdwindow>
      <etdwindow>
        <status>Library processed</status>
      </etdwindow>
    </etdwindows>
  </xsl:variable>
  <!-- Defineds a variable for ETDWindow levels e.g. PGR or PGT, used to set customised text e.g. thesis or dissertation, postgraduate office or student support centre, distinguishes between Doctoral and Masters level submissions -->
  <xsl:variable name="levels">
    <levels>
      <level id="PGR" name="thesis" help="your postgraduate administration office">
        <windowType pid="uk-ac-man-etdtype:1"/>
        <windowtype pid="uk-ac-man-etdtype:2"/>
        <windowtype pid="uk-ac-man-etdtype:3"/>
        <windowtype pid="uk-ac-man-etdtype:4"/>
      </level>
      <level id="PGT" name="dissertation" help="the Student Services Centre">
        <windowType pid="uk-ac-man-etdtype:5"/>
        <windowType pid="uk-ac-man-etdtype:6"/>
      </level>
    </levels>
  </xsl:variable>
  
  <xsl:variable name="level">
    <xsl:value-of select="if ($levels/levels/level[windowType/@pid = $ordered/etdwindows/etdwindow[1]/str[@name='r.isofetdtype.pid']]/@id = '') then $levels/levels/level[@id = 'PGR']/@id else $levels/levels/level[windowType/@pid = $ordered/etdwindows/etdwindow[1]/str[@name='r.isofetdtype.pid']]/@id"/>
  </xsl:variable>
  
  <xsl:variable name="thesisdissertation">
    <xsl:value-of select="if ($levels/levels/level[windowType/@pid = $ordered/etdwindows/etdwindow[1]/str[@name='r.isofetdtype.pid']]/@name = '') then $levels/levels/level[@id = 'PGR']/@name else $levels/levels/level[windowType/@pid = $ordered/etdwindows/etdwindow[1]/str[@name='r.isofetdtype.pid']]/@name"/>
  </xsl:variable>
  
  <xsl:variable name="help">
    <xsl:value-of select="if ($levels/levels/level[windowType/@pid = $ordered/etdwindows/etdwindow[1]/str[@name='r.isofetdtype.pid']]/@help = '') then $levels/levels/level[@id = 'PGR']/@help  else $levels/levels/level[windowType/@pid = $ordered/etdwindows/etdwindow[1]/str[@name='r.isofetdtype.pid']]/@help"/>
  </xsl:variable>

  <!-- Copies all source XML into a variable $source, done to enable ordering and selection of most prominent etdWindow as based on $order -->
  <xsl:variable name="source">
    <etdwindows>
      <xsl:for-each select="/response/result[@name='response']/doc">
        <xsl:sort select="date[@name='x.lastmodifieddate']" data-type="text" order="descending"/>
        <etdwindow>
          <status>
            <xsl:if test="str[@name='e.windowstate']='PENDING'"><xsl:value-of select="'Pending'"/></xsl:if>
            <xsl:if test="str[@name='e.windowstate']='OPENED'"><xsl:value-of select="'Awaiting submission'"/></xsl:if>
            <xsl:if test="str[@name='e.windowstate']='EXPIRED'"><xsl:value-of select="'Expired'"/></xsl:if>
            <xsl:if test="str[@name='e.windowstate']='CANCELLED'"><xsl:value-of select="'Cancelled'"/></xsl:if>
            <xsl:if test="str[@name='e.submissionstate']='SUBMITTED'"><xsl:value-of select="'Deposited'"/></xsl:if>
            <xsl:if test="str[@name='e.submissionstate']='ACKNOWLEDGED'"><xsl:value-of select="'Acknowledged'"/></xsl:if>
            <xsl:if test="str[@name='e.submissionstate']='REJECTED'"><xsl:value-of select="'Rejected'"/></xsl:if>
            <xsl:if test="str[@name='e.submissionstate']='SENT_TO_LIBRARY'"><xsl:value-of select="'Sent to library'"/></xsl:if>
            <xsl:if test="str[@name='e.submissionstate']='LIBRARY_PROCESSED'"><xsl:value-of select="'Library processed'"/></xsl:if>
          </status>
          <xsl:copy-of select="*"/>
        </etdwindow>
      </xsl:for-each>
    </etdwindows>
  </xsl:variable>
 
  <!-- Creates an ordered set of etdwindow nodes, uses the etdwindow template in getorder mode -->
  <xsl:variable name="ordered">
    <etdwindows>
      <xsl:apply-templates select="$order/etdwindows/etdwindow" mode="getorder"/>
    </etdwindows>
  </xsl:variable>

  <xsl:template match="/">
<!--    <xsl:copy-of select="$ordered"/>-->
    <html>
      <body>
        <div class="uk-ac-manchester-proxy-escholar">
          <xsl:choose>
            <xsl:when test="exists($windowtypes)">
              <xsl:call-template name="showHeader"/>
              <div class="message">
                <xsl:choose>
                  <xsl:when test="$ordered/etdwindows/etdwindow">
                    <xsl:apply-templates select="$ordered/etdwindows/etdwindow[position()=1]" mode="getmessage"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:call-template name="no_etdwindow"/>
                  </xsl:otherwise>
                </xsl:choose>
              </div>
              <xsl:call-template name="showSubmissionHistory"/>
              <xsl:call-template name="showFooter"/>
            </xsl:when>
            <xsl:otherwise>
              <h3>System fault</h3>
              <p>Unfortunately, we have encountered a problem while recovering information about your eThesis submission.</p>
              <p>This fault is most likely temporary, so please try and reload this portlet later.</p>
              <p>If this fault persists then contact eThesis Support Service for further advice.</p>
              <ul>
                <li>
                  <xsl:element name="a">
                    <xsl:attribute name="href" select="'http://www.manchester.ac.uk/ethesis'"/>
                    <xsl:attribute name="rel" select="'popup noicon 800 800'"/>
                    <xsl:text>eThesis Support Service</xsl:text>
                  </xsl:element>
                </li>
              </ul>
            </xsl:otherwise>
          </xsl:choose>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="showHeader">
<!--  	<p style="color: red;"><strong>Please note:</strong> On Tuesday 2nd August 2011 between 10am - 12pm [BST] we are doing some work on Manchester eScholar Services. As a result you will experience some loss of access during this time. Please excuse any inconvenience this may cause, we are fixing some software bugs and adding new features.</p> -->
    <p>This is a summary of your eThesis submissions.</p>
  </xsl:template>

  <xsl:template name="showFooter">
    <p style="margin-top: 1em;">Visit <xsl:element name="a"><xsl:attribute name="href" select="'http://www.manchester.ac.uk/electronic-thesis'"/><xsl:attribute name="rel" select="'popup'"/><xsl:text>eThesis Support Service - eTheses</xsl:text></xsl:element> to learn more, get help and request support.</p>
    <p class="poweredbymanchesterescholar" style="font-size: 0.85em;">Powered by <xsl:element name="a"><xsl:attribute name="href" select="'http://www.manchester.ac.uk/ethesis'"/><xsl:attribute name="rel" select="'popup'"/><xsl:text>eThesis Support Service</xsl:text></xsl:element></p>
  </xsl:template>

<!--
  <xsl:template name="showDummyWizardLinks">
    <p>You may practice a submission using a Dummy ETD submission wizard.</p>
    <ul>
      <xsl:choose>
        <xsl:when test="$level='PGR'">
          <li>
            <a>
              <xsl:attribute name="rel">popup</xsl:attribute>
              <xsl:attribute name="href">
                <xsl:value-of select="concat($baseuri,'myetd/add/uk-ac-man-etdtype:1d/')"/>
              </xsl:attribute>Dummy ETD submission: Doctoral level ETD - examination (first submission)</a>
          </li>
          <li>
            <a>
              <xsl:attribute name="rel">popup</xsl:attribute>
              <xsl:attribute name="href">
                <xsl:value-of select="concat($baseuri,'myetd/add/uk-ac-man-etdtype:3d/')"/>
              </xsl:attribute>Dummy ETD submission: Doctoral level ETD - final</a>
          </li>
        </xsl:when>
        <xsl:when test="$level='PGT'">
          <li>
            <a>
              <xsl:attribute name="rel">popup</xsl:attribute>
              <xsl:attribute name="href">
                <xsl:value-of select="concat($baseuri,'myetd/add/uk-ac-man-etdtype:5d/')"/>
              </xsl:attribute>Dummy ETD submission: Masters level ETD - examination (first submission)</a>
          </li>
        </xsl:when>
        <xsl:otherwise>
          <li>
            <a>
              <xsl:attribute name="rel">popup</xsl:attribute>
              <xsl:attribute name="href">
                <xsl:value-of select="concat($baseuri,'myetd/add/uk-ac-man-etdtype:1d/')"/>
              </xsl:attribute>Dummy ETD submission: Doctoral level ETD - examination (first submission)</a>
          </li>
        </xsl:otherwise>
      </xsl:choose>
    </ul>
  </xsl:template>
-->
  
  <!--  Message when no etd windows exist -->
  <xsl:template name="no_etdwindow">
    <h3>You have NO OPEN eThesis submission window</h3>
    <p>Currently, you are NOT required to submit your eThesis</p>
    <p>Submission of an eThesis is NECESSARY for you to be examined and graduate. If you are approaching a period when you are ready to submit your post graduate research thesis, contact your postgraduate administration office and report your notification of intent to submit. An eThesis submission window will then be created for you.</p>
    <!-- <xsl:call-template name="showDummyWizardLinks"/> -->
  </xsl:template>

  <xsl:template match="etdwindow" mode="getorder">
    <xsl:variable name="orderedStatus" select="status"/>
    <xsl:copy-of select="$source/etdwindows/etdwindow[status = $orderedStatus]"/>
  </xsl:template>

  <!-- No window open message, applies to status = Pending, status = Cancelled -->
  <xsl:template match="etdwindow[status='Pending' or status='Cancelled']" mode="getmessage">
    <xsl:call-template name="no_etdwindow"/>
  </xsl:template>

  <!-- Window open message, applies to status = Awaiting submission -->
  <xsl:template match="etdwindow[status='Awaiting submission']" mode="getmessage">
    <xsl:variable name="windowtypepid" select="str[@name='r.isofetdtype.pid']"/>
    <xsl:variable name="expectedOpenDate" select="if (substring-before(str[@name='e.expectedopendate'],' ') = '') then xs:date(str[@name='e.expectedopendate']) else xs:date(substring-before(str[@name='e.expectedopendate'],' '))"/>
    <xsl:variable name="expectedCloseDate" select="if (substring-before(str[@name='e.expectedclosedate'],' ') = '') then xs:date(str[@name='e.expectedclosedate']) else xs:date(substring-before(str[@name='e.expectedclosedate'],' '))"/>
    
    <!-- BA: 19/04/22 - Get current date - start -->
    <xsl:variable name="currDate" as="xs:date" select="current-date()"/>
    <!-- BA: 19/04/22 - Get current date - end -->    
    
    <h3>You have an OPEN eThesis submission window</h3>
    <p>You MUST now submit your <strong>
        <xsl:value-of select="$windowtypes/etdtypes/etdtype[@pid=$windowtypepid]/name"/>
      </strong> in an electronic form.</p>
    
    <!-- BA: 19/04/22 - Fix number of days remaining by subtracting current date from expected close date - start -->
    <!--<p>Your submission window was opened on <xsl:value-of select="if ($expectedOpenDate castable as xs:date) then format-date($expectedOpenDate,'[D01] [MNn], [Y1,4]') else 'unkown date'"/> and will close at 23:59 on <xsl:value-of select="if ($expectedCloseDate castable as xs:date) then format-date($expectedCloseDate,'[D01] [MNn], [Y1,4]') else 'unkown date'"/>. You have <strong><xsl:value-of select="if ($expectedOpenDate castable as xs:date and $expectedCloseDate castable as xs:date) then days-from-duration($expectedCloseDate - $expectedOpenDate) else 'unknown'"/> day(s) remaining</strong> to submit the electronic version of your thesis/dissertation. <!-\-In addition, you MUST submit printed and bound copies by the appropriate deadline-\-></p>-->
    <p>Your submission window was opened on <xsl:value-of select="if ($expectedOpenDate castable as xs:date) then format-date($expectedOpenDate,'[D01] [MNn], [Y1,4]') else 'unkown date'"/> and will close at 23:59 on <xsl:value-of select="if ($expectedCloseDate castable as xs:date) then format-date($expectedCloseDate,'[D01] [MNn], [Y1,4]') else 'unkown date'"/>. You have <strong><xsl:value-of select="if ($expectedOpenDate castable as xs:date and $expectedCloseDate castable as xs:date) then days-from-duration($expectedCloseDate - $currDate) else 'unknown'"/> day(s) remaining</strong> to submit the electronic version of your thesis/dissertation. <!--In addition, you MUST submit printed and bound copies by the appropriate deadline--></p>
    <!-- BA: 19/04/22 - Fix number of days remaining by subtracting current date from expected close date - end -->
    
    <p>Use the eThesis submission wizard to submit your electronic <xsl:value-of select="$thesisdissertation"/>.</p>
    <ul>
      <li>
        <a>
          <xsl:attribute name="rel">popup</xsl:attribute>
          <xsl:attribute name="href">
            <xsl:value-of select="concat($baseuri,'myetdv2/submit/',str[@name='PID'])"/>
          </xsl:attribute>eThesis submission wizard: <xsl:value-of select="$windowtypes/etdtypes/etdtype[@pid=$windowtypepid]/name"/></a>
      </li>
    </ul>
    <!--
    <p>You may practice a submission using the Dummy ETD submission wizard.</p>
    <ul>
      <li>
        <a>
          <xsl:attribute name="rel">popup</xsl:attribute>
          <xsl:attribute name="href">
            <xsl:value-of select="concat($baseuri,'myetd/add/',$windowtypepid,'d/')"/>
          </xsl:attribute>Dummy ETD submission: <xsl:value-of select="$windowtypes/etdtypes/etdtype[@pid=$windowtypepid]/name"/></a>
      </li>
    </ul>
    -->
  </xsl:template>

  <!-- Window closed with no submission message, applies to status = Expired -->
  <xsl:template match="etdwindow[status='Expired']" mode="getmessage">
    <xsl:variable name="windowtypepid" select="str[@name='r.isofetdtype.pid']"/>
    <xsl:variable name="windowname" select="$windowtypes/etdtypes/etdtype[@pid=$windowtypepid]/name"/>
    <h3>Your eThesis submission window has EXPIRED</h3>
    <p>Unfortunately, our records indicate you have NOT successfully submitted your <strong><xsl:value-of select="$windowname"/></strong> in an electronic form by the deadline, <xsl:value-of select="format-date(str[@name='e.expectedclosedate'],'[D01] [MNn], [Y1,4]')"/>, which has now passed.</p>
    <p>As a consequence your <xsl:value-of select="$thesisdissertation"/> will not be examined and you will not be able to graduate if this is not resolved.</p>
    <p>If this information is incorrect or extenuating circumstances exist you should immediately contact <xsl:value-of select="$help"/>.</p>
    <p>Use your submission history to check on the status of your past and pending eThesis submissions.</p>
  </xsl:template>

  <!-- Window closed with submission message, applies to status = Deposited -->
  <xsl:template match="etdwindow[status='Deposited']" mode="getmessage">
    <xsl:variable name="windowtypepid" select="str[@name='r.isofetdtype.pid']"/>
    <xsl:variable name="windowname" select="$windowtypes/etdtypes/etdtype[@pid=$windowtypepid]/name"/>
    <h3>Your eThesis submission window is CLOSED</h3>
    <p>You have SUCCESSFULLY submitted your <strong><xsl:value-of select="$windowname"/></strong>.</p>
   <!-- <p>Before receipt of your submission can be acknowledged, you MUST submit the appropriate number of printed and bound versions of your <xsl:value-of select="$thesisdissertation"/> containing the approved cover-page by the appropriate deadline.</p>-->
    <p>Access your metadata, download your PDF and download the approved cover-page.</p>
    <ul>
      <li>
        <xsl:element name="a">
          <xsl:attribute name="rel" select="'popup'"/>
          <xsl:attribute name="href" select="concat($baseuri,'myetd/view/',str[@name='PID'])"/>
          <xsl:value-of select="$windowname"/>
        </xsl:element>
      </li>
    </ul>
  </xsl:template>

  <!-- Window submission acknowledged message, applies to status = Acknowledged, status = Sent to Library -->
  <xsl:template match="etdwindow[status='Acknowledged' or status='Sent to library']" mode="getmessage">
    <xsl:variable name="windowtypepid" select="str[@name='r.isofetdtype.pid']"/>
    <xsl:variable name="windowname" select="$windowtypes/etdtypes/etdtype[@pid=$windowtypepid]/name"/>
    <h3>Receipt of your eThesis submission has been ACKNOWLEDGED</h3>
    <p>Submission of your <strong><xsl:value-of select="$windowname"/></strong> has been acknowledged.</p>
    <xsl:choose>
      <xsl:when test="$windowtypepid != 'uk-ac-man-etdtype:3'">
        <p>Your submission will now be examined in line with the rules and regulations governing your degree programme.</p>
      </xsl:when>
      <xsl:otherwise>
        <p>Inline with your intellectual property/copyright and access declaration, and with the approval of your School/Faculty, your submission will now be preserved in the institutional repository and assigned the appropriate access rights so that others may download and view it via the World Wide Web.</p>
      </xsl:otherwise>
    </xsl:choose>
    <p>This completes this eThesis submission.</p>
    <p>Access your metadata, download your PDF and download the approved cover-page.</p>
    <ul>
      <li>
        <xsl:element name="a">
          <xsl:attribute name="rel" select="'popup'"/>
          <xsl:attribute name="href" select="concat($baseuri,'myetd/view/',str[@name='PID'])"/>
          <xsl:value-of select="$windowname"/>
        </xsl:element>
      </li>
    </ul>
  </xsl:template>

  <!-- Window submission rejected message, applies to status = Rejected -->
  <xsl:template match="etdwindow[status='Rejected']" mode="getmessage">
    <xsl:variable name="windowtypepid" select="str[@name='r.isofetdtype.pid']"/>
    <xsl:variable name="windowname" select="$windowtypes/etdtypes/etdtype[@pid=$windowtypepid]/name"/>
    <h3>Receipt of your eThesis submission has been REJECTED</h3>
    <p>Unfortunately, your <strong><xsl:value-of select="$windowname"/></strong> submission does not meet the minimum requirements for your work to be examined as outlined in the University's rules and regulations governing your degree programme.</p>
    <p>As a consequence your <xsl:value-of select="$thesisdissertation"/> will not be examined and you will not be able to graduate if this issue remains unresolved.</p>
    <p>You should immediately contact <xsl:value-of select="$help"/> to resolve this.</p>
  </xsl:template>

  <xsl:template match="etdwindow[status='Library processed']" mode="getmessage">
    <xsl:variable name="windowtypepid" select="str[@name='r.isofetdtype.pid']"/>
    <xsl:variable name="windowname" select="$windowtypes/etdtypes/etdtype[@pid=$windowtypepid]/name"/>
    <h3>Your eThesis submission has been PROCESSED BY THE LIBRARY</h3>
    <p>Your <strong><xsl:value-of select="$windowname"/></strong> submission will be preserved in the institutional repository. Inline with your copyright and access declaration (including any embargo period), your thesis has been assigned the appropriate access rights. For 'open access' theses others may download and view it via the World Wide Web.</p>
    <p>You may continue to securely access your submission here including view all metadata, download your PDF and download the approved cover-page.</p>
    <ul>
      <li>
        <xsl:element name="a">
          <xsl:attribute name="rel" select="'popup noicon 800 800'"/>
          <xsl:attribute name="href" select="concat($baseuri,'myetd/view/',../pid)"/>
          <xsl:value-of select="$windowname"/>
        </xsl:element>
      </li>
    </ul>
  </xsl:template>

  <xsl:template name="showSubmissionHistory">
    <p>Check with <xsl:value-of select="$help"/> for further information and exceptions. Use your submission history to view your eThesis submissions and check on the status of your past and pending submissions.</p>

    <div class="submissionhistory">
      <h3>Your eThesis submission history</h3>
      <div class="grid" style="font-size:0.9em;">
        <table border="1">
          <tr>
            <th style="border-width: 1px ! important; border-style: solid ! important; border-color: rgb(0, 153, 153) white rgb(0, 153, 153) rgb(0, 153, 153) ! important; background-color: rgb(0, 153, 153);">Submission</th>
            <th style="border-width: 1px ! important; border-style: solid ! important; border-color: rgb(0, 153, 153) white rgb(0, 153, 153) rgb(0, 153, 153) ! important; background-color: rgb(0, 153, 153);">Programme</th>
            <th style="border-width: 1px ! important; border-style: solid ! important; border-color: rgb(0, 153, 153) white rgb(0, 153, 153) rgb(0, 153, 153) ! important; background-color: rgb(0, 153, 153);">Open date</th>
            <th style="border-color: rgb(0, 153, 153) rgb(0, 153, 153) rgb(0, 153, 153) rgb(0, 153, 153) ! important; background-color: rgb(0, 153, 153);">Status (expected close date)</th>
          </tr>
          <xsl:choose>
            <xsl:when test="$ordered/etdwindows/etdwindow">
              <xsl:apply-templates select="$ordered/etdwindows/etdwindow" mode="grid"/>
            </xsl:when>
            <xsl:otherwise>
              <td colspan="4"><strong>No records found</strong>: Currently, we are unable to find any eThesis submissions for you in the institutionl repository.</td>
            </xsl:otherwise>
          </xsl:choose>
        </table>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="etdwindow" mode="grid">
    <xsl:variable name="windowtypepid" select="str[@name='r.isofetdtype.pid']"/>
    <xsl:variable name="windowname" select="$windowtypes/etdtypes/etdtype[@pid=$windowtypepid]/name"/>
    <xsl:variable name="expectedOpenDate" select="if (substring-before(str[@name='e.expectedopendate'],' ') = '') then xs:date(str[@name='e.expectedopendate']) else xs:date(substring-before(str[@name='e.expectedopendate'],' '))" as="xs:date"/>
    <xsl:variable name="expectedCloseDate" select="if (substring-before(str[@name='e.expectedclosedate'],' ') = '') then xs:date(str[@name='e.expectedclosedate']) else xs:date(substring-before(str[@name='e.expectedclosedate'],' '))"/>
    <tr>
      <td>
        <xsl:choose>
          <xsl:when test="not(status='Pending' or status='Awaiting submission' or status='Expired' or status='Cancelled')">
            <xsl:element name="a">
              <xsl:attribute name="rel" select="'popup noicon 800 800'"/>
              <xsl:attribute name="href" select="concat($baseuri,'myetd/view/',str[@name='PID'])"/>
              <xsl:value-of select="$windowname"/>
            </xsl:element>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$windowname"/>
          </xsl:otherwise>
        </xsl:choose>
      </td>
      <td>
        <xsl:value-of select="str[@name='e.programmename']"/>
      </td>
      <td>
        <xsl:value-of select="if ($expectedOpenDate castable as xs:date) then format-date($expectedOpenDate,'[D01]/[M01]/[Y1,4]') else 'unknown date'"/>
      </td>
      <td>
        <xsl:value-of select="status"/>
        <xsl:if test="status='Pending' or status='Awaiting submission'">
          <xsl:value-of select="if ($expectedCloseDate castable as xs:date) then concat(' (',format-date($expectedCloseDate,'[D01]/[M01]/[Y1,4]'),')') else 'unknown date'"/>
        </xsl:if>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
