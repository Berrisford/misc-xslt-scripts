<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:date="http://exslt.org/dates-and-times"
    exclude-result-prefixes="xs"
    version="2.0">

    <xsl:output indent="yes" method="xhtml" omit-xml-declaration="yes"/>

    <xsl:template match="/">
        <xsl:variable name="openDate" as="xs:date" select="xs:date('2021-10-11')"/>
        <xsl:variable name="closeDate" as="xs:date" select="xs:date('2022-07-31')"/>
        <xsl:variable name="currDate" as="xs:date" select="current-date()"/>
        <!--<xsl:variable name="interval" as="xs:integer" select="days-from-duration($closeDate - $openDate)"/>-->
        <xsl:variable name="interval" as="xs:integer" select="days-from-duration($closeDate - $currDate)"/>
        
        <root>
            <current-date><xsl:value-of select="$currDate"/></current-date>
            <open-date><xsl:value-of select="$openDate"/></open-date>
            <close-date><xsl:value-of select="$closeDate"/></close-date>
            <interval><xsl:value-of select="$interval"/></interval>
        </root>
    </xsl:template>

</xsl:stylesheet>