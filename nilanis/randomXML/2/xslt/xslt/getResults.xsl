<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <xsl:for-each select="result/child::*[string(node-name()) ne 'count']">
            <item>
                <xsl:attribute name="type" select="node-name()"/>
                <title><xsl:value-of select="normalize-space(title)"/></title>
                <type><xsl:value-of select="type"/></type>
                <year><xsl:value-of select="publicationStatuses/publicationStatus/publicationDate/year"></xsl:value-of></year>
                <!--<publication-status><xsl:value-of select="normalize-space(publicationStatuses/publicationStatus)"/></publication-status>-->
                <xsl:for-each select="publicationStatuses/publicationStatus">
                    <publication-status><xsl:value-of select="normalize-space(.)"/></publication-status>
                </xsl:for-each>
                <isbn><xsl:value-of select="isbns/isbn"/></isbn>
                <publisher><xsl:value-of select="publisher/name"/></publisher>
            </item>
        </xsl:for-each>
    </xsl:template>
    
</xsl:stylesheet>