<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
<!--    
    <xsl:template match="/">
        <result>        
            <xsl:for-each select="//doc[arr/str[not(contains(text(), 'Main Supervisor'))]]">
                <xsl:if test="not(exists(arr/str[contains(text(), 'Main Supervisor')]))">
                    <doc>
                        <party-number>
                            <xsl:value-of select="str[@name='p.partynumber']"/>
                        </party-number>
                        <display-name>
                            <xsl:value-of select="str[@name='p.displayname']"/>
                        </display-name>
                        <xsl:apply-templates select="arr/str"/>
                    </doc>                                        
                </xsl:if>                
            </xsl:for-each>
        </result>
    </xsl:template>
-->

    <xsl:template match="/">
        <xsl:result-document href="get-no-main-supervisor.xml">
        <result>        
            <xsl:for-each select="//doc[not(exists(arr/str[contains(text(), 'Main Supervisor')]))]">
                    <doc>
                        <party-number>
                            <xsl:value-of select="str[@name='p.partynumber']"/>
                        </party-number>
                        <display-name>
                            <xsl:value-of select="str[@name='p.displayname']"/>
                        </display-name>
                        <!--<xsl:apply-templates select="arr/str"/>-->
                    </doc>                                        
            </xsl:for-each>
        </result>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="arr/str">
        <xsl:copy-of select="."/>
    </xsl:template>
    
</xsl:stylesheet>