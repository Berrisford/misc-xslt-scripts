<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">    
    <xsl:output indent="yes" method="xml"/>

    <!-- BA: This script iterates across all records (of a particular content type) in chunks of 1000 (rows) and checks to see if the record -->
    <!--  has been matched by Scopus. If not, it generates the requisite Pure xml entirely from eScolar data. Change $rows, $num_found, $obj_files -->
    <!--  accordingly to test quickly with small sets of data. Change $file_path to match where your files are located. -->
    
    <xsl:variable name="root_url" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/nonscw/select/'"></xsl:variable>
    <!--http://escholarprd.library.manchester.ac.uk:8085/solr/nonscw/select?q=p.pgr%3AY+AND+r.isderivationof.pid%3Auk*&fl=p.partynumber%2C+p.displayname%2C+p.authoritytype%2C+p.programmename%2C+p.programmeid%2C+p.planname%2C+p.planid%2C+r.ismemberof.pid%2C+r.wasmemberof.pid&wt=xml&indent=true-->
    
    <!-- BA: 04/04/16 - Change to include only newly created items -->        
    <xsl:variable name="url" select="concat($root_url, '?q=p.pgr%3AY+AND+r.isderivationof.pid%3Auk*&amp;fl=p.partynumber%2C+p.displayname%2C+p.authoritytype%2C+p.programmename%2C+p.programmeid%2C+p.planname%2C+p.planid%2C+r.ismemberof.pid%2C+r.wasmemberof.pid&amp;wt=xml&amp;indent=true')"></xsl:variable>    
    
    <xsl:variable name="rows" select="100"/>
    
    <xsl:variable name="num_found">
        <xsl:value-of select="doc(concat($url, '&amp;start=0&amp;rows=0'))/response/result/@numFound"/>
        <!--<xsl:value-of select="doc(concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?1', '&amp;fl=PID&amp;omitHeader=true&amp;start=0&amp;rows=0'))/response/result/@numFound"/>-->
        <!--<xsl:value-of select="500"/>-->
    </xsl:variable>
    
    <xsl:variable name="obj_orgs">
        <!--<xsl:copy-of select="doc('schools.xml')/schools"/>-->
        <xsl:copy-of select="doc('organisations-apc-approved.xml')/orgs"/>
    </xsl:variable>
        
    <xsl:template match="/">
        <records>
        <xsl:call-template name="recurse">
            <xsl:with-param name="start" select="0"/>
            <xsl:with-param name="end" select="$rows"/>
        </xsl:call-template>
        </records>
    </xsl:template>
        
    <xsl:template name="recurse">
        <xsl:param name="start"/>
        <xsl:param name="end"/>
        <xsl:variable name="obj_result">
            <xsl:copy-of select="doc(concat($url, '&amp;start=', $start, '&amp;rows=', $rows))/response/result"/>
        </xsl:variable>
        
        <!--<xsl:result-document href="get-no-main-supervisor(new).xml">-->
        <xsl:for-each select="$obj_result/result/doc[count(arr[@name='p.authoritytype']/str[contains(text(), 'Main Supervisor')]) &gt; 1]">
            <doc>
                <xsl:call-template name="matched">
                    <xsl:with-param name="obj_doc">
                        <xsl:copy-of select="."/>
                    </xsl:with-param>
                </xsl:call-template>
            </doc>                                    
            
        </xsl:for-each>
        <!--</xsl:result-document>-->
                        
        <xsl:if test="$end &lt; $num_found">
            <xsl:call-template name="recurse">
                <xsl:with-param name="start" select="$end + 1"/>
                <xsl:with-param name="end" select="$end + $rows"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
      
    <xsl:template name="matched">
        <xsl:param name="obj_doc"/>
        <party-number>
            <xsl:value-of select="$obj_doc/doc/str[@name='p.partynumber']"/>
        </party-number>
        <display-name>
            <xsl:value-of select="$obj_doc/doc/str[@name='p.displayname']"/>
        </display-name>
        <xsl:for-each select="$obj_doc/doc/arr[@name='p.programmename']/str">
            <xsl:variable name="count">
                <xsl:number/>
            </xsl:variable>
            <xsl:variable name="node_name" select="concat('programme-name', $count)"/>
            <xsl:element name="{$node_name}">
                <xsl:value-of select="."></xsl:value-of>
            </xsl:element>
        </xsl:for-each>
        <xsl:for-each select="$obj_doc/doc/arr[@name='p.programmeid']/str">
            <xsl:variable name="count">
                <xsl:number/>
            </xsl:variable>
            <xsl:variable name="node_name" select="concat('programme-id', $count)"/>
            <xsl:element name="{$node_name}">
                <xsl:value-of select="."></xsl:value-of>
            </xsl:element>
        </xsl:for-each>
        <xsl:for-each select="$obj_doc/doc/arr[@name='p.planname']/str">
            <xsl:variable name="count">
                <xsl:number/>
            </xsl:variable>
            <xsl:variable name="node_name" select="concat('plan-name', $count)"/>
            <xsl:element name="{$node_name}">
                <xsl:value-of select="."></xsl:value-of>
            </xsl:element>
        </xsl:for-each>
        <xsl:for-each select="$obj_doc/doc/arr[@name='p.planid']/str">
            <xsl:variable name="count">
                <xsl:number/>
            </xsl:variable>
            <xsl:variable name="node_name" select="concat('plan-id', $count)"/>
            <xsl:element name="{$node_name}">
                <xsl:value-of select="."></xsl:value-of>
            </xsl:element>
        </xsl:for-each>

            <xsl:if test="$obj_doc/doc/arr[@name='r.ismemberof.pid']/str">
                <xsl:for-each select="$obj_doc/doc/arr[@name='r.ismemberof.pid']/str">
                    <xsl:variable name="count">
                        <xsl:number/>
                    </xsl:variable>
                    <xsl:variable name="node_name" select="concat('is-member-of-school', $count)"/>
                    <!--<xsl:variable name="node_name" select="'is-member-of-school'"/>-->
                    <xsl:variable name="school_pid" select="./text()"></xsl:variable>
                    <xsl:if test="$obj_orgs/orgs/faculty/school[@pid=$school_pid]">
                        <xsl:element name="{$node_name}">
                            <xsl:value-of select="$obj_orgs/orgs/faculty/school[@pid=$school_pid]/@label"></xsl:value-of>
                        </xsl:element>                                            
                    </xsl:if>
                </xsl:for-each>
            </xsl:if>
            <xsl:if test="$obj_doc/doc/arr[@name='r.wasmemberof.pid']/str">
                <xsl:for-each select="$obj_doc/doc/arr[@name='r.wasmemberof.pid']/str">
                    <xsl:variable name="count">
                        <xsl:number/>
                    </xsl:variable>
                    <xsl:variable name="node_name" select="concat('was-member-of-school', $count)"/>
                    <!--<xsl:variable name="node_name" select="'was-member-of-school'"/>-->
                    <xsl:variable name="school_pid" select="./text()"></xsl:variable>
                    <xsl:if test="$obj_orgs/orgs/faculty/school[@pid=$school_pid]">
                        <xsl:element name="{$node_name}">
                            <xsl:value-of select="$obj_orgs/orgs/faculty/school[@pid=$school_pid]/@label"></xsl:value-of>
                        </xsl:element>                                            
                    </xsl:if>
                </xsl:for-each>
            </xsl:if>
        

<!--        <xsl:choose>
            <xsl:when test="$obj_doc/doc/arr[@name='r.ismemberof.pid']/str">
                <xsl:for-each select="$obj_doc/doc/arr[@name='r.ismemberof.pid']/str">
                    <xsl:variable name="count">
                        <xsl:number/>
                    </xsl:variable>
                    <xsl:variable name="node_name" select="concat('school', $count)"/>
                    <xsl:variable name="school_pid" select="./text()"></xsl:variable>
                    <xsl:if test="$obj_orgs/orgs/faculty/school[@pid=$school_pid]">
                        <xsl:element name="{$node_name}">
                            <xsl:value-of select="$obj_orgs/orgs/faculty/school[@pid=$school_pid]/@label"></xsl:value-of>
                        </xsl:element>                                            
                    </xsl:if>
                </xsl:for-each>
            </xsl:when>
            <xsl:when test="$obj_doc/doc/arr[@name='r.wasmemberof.pid']/str">
                <xsl:for-each select="$obj_doc/doc/arr[@name='r.wasmemberof.pid']/str">
                    <xsl:variable name="count">
                        <xsl:number/>
                    </xsl:variable>
                    <xsl:variable name="node_name" select="concat('school', $count)"/>
                    <xsl:variable name="school_pid" select="./text()"></xsl:variable>
                    <xsl:if test="$obj_orgs/orgs/faculty/school[@pid=$school_pid]">
                        <xsl:element name="{$node_name}">
                            <xsl:value-of select="$obj_orgs/orgs/faculty/school[@pid=$school_pid]/@label"></xsl:value-of>
                        </xsl:element>                                            
                    </xsl:if>
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>
-->        
<!--        
        <xsl:choose>
            <xsl:when test="$obj_doc/doc/arr[@name='r.ismemberof.pid']/str">
                <xsl:for-each select="distinct-values($obj_doc/doc/arr[@name='r.ismemberof.pid']/str)">
                    <xsl:variable name="school_pid" select="."></xsl:variable>
                    <xsl:element name="school">
                        <!-\-<xsl:value-of select="$obj_schools/schools/school[@pid = 'uk-ac-man-org:31']/name"></xsl:value-of>-\->
                        <!-\-<xsl:value-of select="string($obj_schools/schools/school[@pid = ./text()]/@label)"></xsl:value-of>-\->
                        <xsl:value-of select="$obj_orgs/orgs/faculty/school[@pid=$school_pid]/@label"></xsl:value-of>
                    </xsl:element>                    
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>
-->                

    </xsl:template>
           
</xsl:stylesheet>
