<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="no"/>
    <xsl:variable name="orgs"><xsl:copy-of select="document('organisations-apc-approved.xml')"/>
    </xsl:variable>
    <xsl:template match="/">
        <xsl:apply-templates select="/response"/>
    </xsl:template>
    <xsl:template match="response">
        <records>
            <xsl:apply-templates select="result/doc"/>
        </records>
    </xsl:template>
        
    <xsl:template match="doc">
        <record>
            <id><xsl:apply-templates select="str[@name='PID']"/></id>
            <partynumber><xsl:apply-templates select="str[@name='e.partynumber']"/></partynumber>
            <opened-by><xsl:apply-templates select="str[@name='r.isopenedby.displayname']"/></opened-by>
            <acknowledged-by><xsl:apply-templates select="str[@name='r.isacknowledgedby.displayname']"/></acknowledged-by>
        </record>
        
   </xsl:template>
   
  
</xsl:stylesheet>
