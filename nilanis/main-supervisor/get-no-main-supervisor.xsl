<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>

    <!--http://escholarprd.library.manchester.ac.uk:8085/solr/nonscw/select?q=p.pgr%3AY+AND+r.isderivationof.pid%3Auk*&fl=p.partynumber%2C+p.displayname%2C+p.authoritytype%2C+p.programmename%2C+p.programmeid%2C+p.planname%2C+p.planid%2C+r.ismemberof.pid%2C+r.wasmemberof.pid&wt=xml&indent=true-->

    <xsl:template match="/">
        <xsl:result-document href="get-no-main-supervisor.xml">
        <result>        
            <xsl:for-each select="//doc[not(exists(arr/str[contains(text(), 'Main Supervisor')]))]">
                    <doc>
                        <party-number>
                            <xsl:value-of select="str[@name='p.partynumber']"/>
                        </party-number>
                        <display-name>
                            <xsl:value-of select="str[@name='p.displayname']"/>
                        </display-name>
                    </doc>                                        
            </xsl:for-each>
        </result>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="arr/str">
        <xsl:copy-of select="."/>
    </xsl:template>
    
</xsl:stylesheet>