<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:variable name="new_spotIds">
        <xsl:copy-of select="doc('fulllistNew.xml')/records"/>
    </xsl:variable>
    
    <xsl:template match="/">
        <xsl:result-document href="newSpotIds.xml">            
            <records>
                <xsl:for-each select="records/record">
    
                    <xsl:variable name="pn" as="xs:string">
                        <xsl:value-of select="string(pn)"/>
                    </xsl:variable>
                    
                    <xsl:variable name="to">
                        <!--<xsl:value-of select="$new_spotIds/records/record[@from = $pn]/to"/>-->
                        <xsl:copy-of select="$new_spotIds/records/record[@from = $pn]/to"/>
                    </xsl:variable>
                    
                    <record>
                        <pid>
                            <xsl:value-of select="pid"/>
                        </pid>
                        <pn-old>
                            <xsl:value-of select="pn"/>
                        </pn-old>
                        <xsl:for-each select="$to/to">
                            <pn-new>
                                <xsl:value-of select="."></xsl:value-of>
                            </pn-new>                            
                        </xsl:for-each>                        
                    </record>
                </xsl:for-each>
            </records>
        </xsl:result-document>
    </xsl:template>
</xsl:stylesheet>