<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs commons v1"
    xmlns:commons="v3.commons.pure.atira.dk"
    xmlns:ns2="v3.commons.pure.atira.dk"
    xmlns:v1="v1.activity.pure.atira.dk"
    version="3.0"
    >
  
    <xsl:output method="xml" indent="yes" encoding="UTF-8" />
    
    <xsl:template match="/">
        <xsl:result-document href="talk_output.xml">
            <xsl:element name="activities">
               <!-- <xsl:copy-of select="document('')/*/@xsi:schemaLocation"/>   -->             
                <xsl:apply-templates select="/activities/activity"/>
            </xsl:element>
        </xsl:result-document>
    </xsl:template>
   
    <xsl:template match="activity">
        <talk type="talk/invited_talk">
            <xsl:attribute name="id" select="id"/><!-- ??? -->
           
                <description><xsl:value-of select="description"/></description>
                <degreeofRecognition><xsl:value-of select="degree-of-recognition"/></degreeofRecognition>
                <managedBy><xsl:attribute name="lookupId" select="organisation"/></managedBy>
                <period>
                    <ns2:startdate>
                        <ns2:year><xsl:value-of select="year"/></ns2:year>
                    </ns2:startdate>
                </period>
                <visibility>Restricted</visibility>
                <relatedProjects>
                    <contentReference><xsl:attribute name="contentId" select="related-project"/></contentReference>
                </relatedProjects>
                <title><xsl:value-of select="title"/></title>
                <location>
                    <event><xsl:attribute name="lookupId" select="event"/></event>
                </location>
                <personAssociations>
                    <person lookupHint="personAssociation">
                         <person><xsl:attribute name="lookupId" select="person"/></person>
                         <organisations>
                             <organisation lookupHint="organisationSynchronization"><xsl:attribute name="lookupId" select="organisation"/></organisation>
                         </organisations>
                         <role><xsl:value-of select="role"/></role>
                     </person>
                </personAssociations>
            
           
           
               
        </talk>
        
   </xsl:template>
   
</xsl:stylesheet>