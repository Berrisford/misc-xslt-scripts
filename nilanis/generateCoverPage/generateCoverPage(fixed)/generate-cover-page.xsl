<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="2.0" xmlns:foxml="info:fedora/fedora-system:def/foxml#"
               xmlns:mods="http://www.loc.gov/mods/v3" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
               xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template
            match="/">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="A4-portrait" page-height="29.7cm"
                                       page-width="21.0cm">
                    <fo:region-body margin-top="20mm"
                                    margin-left="40mm" margin-right="10mm" margin-bottom="15mm"/>
                    <fo:region-before extent="20mm"/>
                    <fo:region-after extent="15mm"/>
                    <fo:region-start extent="40mm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="A4-portrait">
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block font-family="Sans-serif" font-size="12pt" text-align="center">Approved electronically generated cover-page version 1.0</fo:block>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:block font-family="Sans-serif" font-size="12pt" line-height="20pt" space-after="20mm"
                              text-align="center" font-weight="bold">THE UNIVERSITY OF
                        MANCHESTER - APPROVED ELECTRONICALLY GENERATED
                        THESIS/DISSERTATION COVER-PAGE</fo:block>
                    <fo:block font-family="Sans-serif" font-size="12pt" space-after="5mm">
                        <xsl:value-of select="concat('Electronic identifier: ',substring-after(//esc-rel:isBelongstoETDwindow/@rdf:resource, 'etd:'))"/>
                    </fo:block>
                    <fo:block font-family="Sans-serif" font-size="12pt" space-after="5mm">
                        <!--<xsl:value-of select="concat('Date of electronic submission: ',format-dateTime(//mods:recordInfo/mods:recordCreationDate,'[D01]/[M01]/[Y1,4]'))"/>-->
                        <!-- BA: 25/07/2018 - Find the last (most recent) version of the MODS datastream and fetch data from there -->
                        <xsl:value-of select="concat('Date of electronic submission: ',format-dateTime(//foxml:datastreamVersion[contains(@ID, 'MODS')][last()]//mods:recordInfo/mods:recordCreationDate,'[D01]/[M01]/[Y1,4]'))"/>
                    </fo:block>
                    <fo:block font-family="Sans-serif" font-size="12pt" space-after="15mm">
                        <!--<xsl:value-of select="concat('Thesis format: ',//mods:genre[@type='form'])"/>-->
                        <!-- BA: 25/07/2018 - Find the last (most recent) version of the MODS datastream and fetch data from there -->
                        <xsl:value-of select="concat('Thesis format: ',//foxml:datastreamVersion[contains(@ID, 'MODS')][last()]//mods:genre[@type='form'])"/>
                    </fo:block>-->
                    <fo:block font-family="Sans-serif" font-size="12pt" line-height="20pt" space-after="15mm">The University of Manchester makes examined, Open Access electronic theses freely available for download and reading online via the University’s Research Explorer http://www.research.manchester.ac.uk.</fo:block>
                    <fo:block font-family="Sans-serif" font-size="12pt" line-height="20pt" space-after="20mm">This print version of my thesis is a TRUE and ACCURATE REPRESENTATION of the electronic version submitted via the University of Manchester's eThesis submission system.
                    </fo:block>
                    <!--<fo:block font-family="Sans-serif" font-size="12pt" line-height="20pt" space-after="10mm">Author's signature ..............................................</fo:block>-->
                    <!--<fo:block font-family="Sans-serif" font-size="12pt" line-height="20pt" space-after="10mm">Date signed .........................................................</fo:block>-->
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
</xsl:transform>