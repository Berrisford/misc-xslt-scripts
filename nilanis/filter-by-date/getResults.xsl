<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
 <!-- createdDate 03-09-2018 to 01-10-2018 -->
    <xsl:template match="/">
        <!-- BA: 14/11/2018 - set start date here -->
        <xsl:variable name="start-date" as="xs:date">
            <xsl:value-of>2016-09-03</xsl:value-of>
        </xsl:variable>

        <!-- BA: 14/11/2018 - set end date here -->        
        <xsl:variable name="end-date" as="xs:date">
            <xsl:value-of>2017-10-01</xsl:value-of>
        </xsl:variable>
        
        <items>
        <xsl:for-each select="result/child::*[string(node-name()) ne 'count']">
            <xsl:variable name="current-date" as="xs:date">
                <xsl:value-of select="substring-before(./info/createdDate, 'T')"/>
            </xsl:variable>
            <xsl:if test="$current-date &gt;= $start-date and $current-date &lt;= $end-date">                
                <item>
                    <pure-id><xsl:value-of select="@pureId"/></pure-id>
                    <content-type><xsl:value-of select="node-name()"/></content-type>
                    <content-type-2><xsl:value-of select="type"/></content-type-2>
                    <author>
                        <xsl:for-each select="personAssociations/personAssociation">
                            <xsl:if test="personRole='Author'">
                                <xsl:choose>
                                    <xsl:when test="position() &gt; 1">
                                        <xsl:choose>
                                            <xsl:when test="person/name"><xsl:value-of select="concat('||', person/name)"></xsl:value-of></xsl:when>
                                            <xsl:when test="externalPerson/name"><xsl:value-of select="concat('||', externalPerson/name)"></xsl:value-of></xsl:when>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:choose>
                                            <xsl:when test="person/name"><xsl:value-of select="person/name"></xsl:value-of></xsl:when>
                                            <xsl:when test="externalPerson/name"><xsl:value-of select="externalPerson/name"></xsl:value-of></xsl:when>	
                                        </xsl:choose>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                        </xsl:for-each>
                    </author>
                      <editor>
                        <xsl:for-each select="personAssociations/personAssociation">
                            <xsl:if test="personRole='Editor'">
                                <xsl:choose>
                                    <xsl:when test="position() &gt; 1">
                                        <xsl:choose>
                                            <xsl:when test="person/name"><xsl:value-of select="concat('||', person/name)"></xsl:value-of></xsl:when>
                                            <xsl:when test="externalPerson/name"><xsl:value-of select="concat('||', externalPerson/name)"></xsl:value-of></xsl:when>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:choose>
                                            <xsl:when test="person/name"><xsl:value-of select="person/name"></xsl:value-of></xsl:when>
                                            <xsl:when test="externalPerson/name"><xsl:value-of select="externalPerson/name"></xsl:value-of></xsl:when>	
                                        </xsl:choose>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                        </xsl:for-each>
                    </editor>
                    <title><xsl:value-of select="normalize-space(title)"/></title>
                    <xsl:for-each select="publicationStatuses/publicationStatus">
                        <xsl:if test="@current='true'">
                            <publication-status><xsl:value-of select="publicationStatus"></xsl:value-of></publication-status>
                            <publication-year><xsl:value-of select="publicationDate/year"></xsl:value-of></publication-year>
                            <publication-month><xsl:value-of select="publicationDate/month"></xsl:value-of></publication-month>
                            <publication-day><xsl:value-of select="publicationDate/day"></xsl:value-of></publication-day>
                        </xsl:if>
                    </xsl:for-each>
                    <isbn><xsl:value-of select="isbns/isbn"/></isbn>
                    <publisher><xsl:value-of select="publisher/name"/></publisher>
                    
                    <author-organisation>
                        <xsl:for-each select="personAssociations/personAssociation">
                            <xsl:if test="personRole='Author'">
                                <xsl:choose>
                                    <xsl:when test="position() &gt; 1">
                                        <xsl:choose>
                                            <xsl:when test="organisationalUnits/organisationalUnit[last()]/name"><xsl:value-of select="concat('||', organisationalUnits/organisationalUnit[last()]/name)"/></xsl:when>
                                       </xsl:choose>
                                            <!-- <xsl:choose>
                                            <xsl:when test="person/name"><xsl:value-of select="concat('||', person/name)"></xsl:value-of></xsl:when>
                                            <xsl:when test="externalPerson/name"><xsl:value-of select="concat('||', externalPerson/name)"></xsl:value-of></xsl:when>
                                        </xsl:choose>-->
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:choose>
                                            <xsl:when test="organisationalUnits/organisationalUnit[last()]/name"><xsl:value-of select="organisationalUnits/organisationalUnit[last()]/name"/></xsl:when>
                                        </xsl:choose>
                                        <!-- <xsl:choose>
                                            <xsl:when test="person/name"><xsl:value-of select="person/name"></xsl:value-of></xsl:when>
                                            <xsl:when test="externalPerson/name"><xsl:value-of select="externalPerson/name"></xsl:value-of></xsl:when>	
                                        </xsl:choose> -->
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                        </xsl:for-each>
                    </author-organisation>
                    
                    
                    <!--
                    <xsl:template match="arr[@name='r.hasmainsupervisor.source']">
                        <xsl:for-each select="./str">
                            <xsl:choose>
                                <xsl:when test="position() &gt; 1">
                                    <xsl:value-of select="concat('||', tokenize(., '\|\|')[1])"/>					
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="tokenize(., '\|\|')[1]"/>						
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each>		
                    </xsl:template>
                    -->
                    <!--
                     <xsl:for-each select="personAssociations/personAssociation">
                        <xsl:if test="personRole='Author'">
                            <xsl:choose>
                                <xsl:when test="person/name"><author><xsl:value-of select="person/name"></xsl:value-of></author></xsl:when>
                                <xsl:when test="externalPerson/name"><author><xsl:value-of select="externalPerson/name"></xsl:value-of></author></xsl:when>
                            </xsl:choose>
                            
                            
                        </xsl:if>
                    </xsl:for-each>
                    -->
                    <!-- <xsl:attribute name="type" select="node-name()"/> -->
                    <!-- <publication-status><xsl:value-of select="publicationStatuses/publicationStatus[last()]/publicationStatus"/></publication-status>-->
                    
                    <!-- 
                    <year>
                        <xsl:if test="publicationStatuses/publicationStatus/publicationStatus/@uri='/dk/atira/pure/researchoutput/status/published'">
                            <xsl:value-of select="publicationStatuses/publicationStatus/publicationDate/year"></xsl:value-of>
                    </xsl:if>
                    </year>
                    -->
                    
                    
                    
                </item>
            </xsl:if>
        </xsl:for-each>
        </items>
    </xsl:template>
    
    
    
</xsl:stylesheet>