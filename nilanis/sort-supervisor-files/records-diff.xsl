<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    exclude-result-prefixes="xs math xd"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <!-- NB: Source document MUST be "without-authority-with-ids-0.xml", i.e. the larger document  -->
    
    <xsl:template match="/">
        <xsl:variable name="with-ids">
            <xsl:copy-of select="doc('without-authority-with-ids.xml')/objects"/>
        </xsl:variable>
                
        <xsl:result-document href="diff.xml">
            <objects>
                <xsl:for-each select="objects/object">
                    <xsl:variable name="current-pid" select="./PID/text()"></xsl:variable>
                    <xsl:if test="not(exists($with-ids/objects/object[PID/text() eq $current-pid]))">
                        <xsl:copy-of select="."/>
                    </xsl:if>
                </xsl:for-each>
            </objects>
        </xsl:result-document>
        
    </xsl:template>
    
</xsl:stylesheet>