<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <!-- http://escholarprd.library.manchester.ac.uk:8085/solr/nonscw/select?q=p.familyname%3AKay+AND%0Ap.prefforename%3AScott&fl=PID&wt=xml&indent=true -->
    <!-- http://escholarprd.library.manchester.ac.uk:8085/solr/nonscw/select?q=p.familyname%3A%22Wildman+-+tarozzi%22+AND%0Ap.prefforename%3ACharlotte&wt=xml&indent=true -->
    
    <xsl:template match="/">
        <xsl:result-document href="co-supervisor-pids.xml">
        <objects>
        <xsl:for-each select="objects/object">
            <object>
                <xsl:copy-of select="./PID"/>
                <xsl:copy-of select="./mods-co-supervisor"/>
                <xsl:copy-of select="./student-id"/>
                <xsl:for-each select="./mods-co-supervisor/name">
                    <!--<xsl:variable name="name-bits" select="tokenize(replace(., ',', ''), ' ')"/>-->
                    <xsl:variable name="surname" select="replace(tokenize(., ',')[1], ' ', '%20')"/>
                    <xsl:variable name="first-and-initials" select="tokenize(., ', ')[2]"/>
                    <xsl:variable name="firstname" select="tokenize($first-and-initials, ' ')[1]"/>
                    <xsl:variable name="url" select="concat('http://escholarprd.library.manchester.ac.uk:8085/solr/nonscw/select?q=p.familyname%3A%22', $surname, '%22+AND%0Ap.prefforename%3A%22', $firstname, '%22&amp;fl=PID&amp;wt=xml&amp;indent=true')"></xsl:variable>
                    <xsl:choose>
                        <xsl:when test="doc-available($url)">
                            <xsl:variable name="result">
                                <xsl:copy-of select="doc($url)/response/result"/>
                            </xsl:variable>
                            <xsl:choose>
                                <xsl:when test="$result/result[@numFound='1']">
                                    <co-supervisor-pid><xsl:value-of select="$result/result/doc/str[@name='PID']"/></co-supervisor-pid>
                                </xsl:when>
                                <xsl:otherwise>
                                    <multiple-co-supervisors/>
                                </xsl:otherwise>
                            </xsl:choose>                                                       
                        </xsl:when>
                        <xsl:otherwise>
                            <no-document-available/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>                
            </object>
        </xsl:for-each>        
        </objects>
        </xsl:result-document>
    </xsl:template>        
</xsl:stylesheet>