<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    exclude-result-prefixes="xs math xd"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <xsl:result-document href="with-authority.xml">
            <objects>
            <xsl:apply-templates select="objects/object[exists(authoritylist/authority)]"/>
            </objects>
        </xsl:result-document>

        <xsl:result-document href="without-authority.xml">
            <objects>
                <xsl:apply-templates select="objects/object[not(exists(authoritylist/authority))]"/>
            </objects>
        </xsl:result-document>
        
    </xsl:template>
    
    <xsl:template match="objects/object[exists(authoritylist/authority)]">
        <xsl:copy-of select="."/>
    </xsl:template>

    <xsl:template match="objects/object[not(exists(authoritylist/authority))]">
        <xsl:copy-of select="."/>
    </xsl:template>
    
</xsl:stylesheet>