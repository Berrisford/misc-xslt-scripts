<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">    
    <xsl:output indent="yes" method="xml"/>
    
    <xsl:variable name="start" select="5083"/>    
    <xsl:variable name="end" select="5505"/>
    <!--<xsl:variable name="end" select="5090"/>-->
    
    <xsl:template match="/">
        <xsl:result-document href="pids.xml">
        <pids>
            <xsl:call-template name="recurse">
                <xsl:with-param name="start" select="$start"/>
            </xsl:call-template>
        </pids>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template name="recurse">
        <xsl:param name="start"/>

        <xsl:variable name="current-pid">
            <xsl:value-of select="concat('uk-ac-man-scw:15m', $start)"/>
        </xsl:variable>        
        
        <pid><xsl:value-of select="$current-pid"/></pid>
        
        <xsl:if test="$start &lt; $end">
            <xsl:call-template name="recurse">
                <xsl:with-param name="start" select="$start + 1"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
        
</xsl:stylesheet>
