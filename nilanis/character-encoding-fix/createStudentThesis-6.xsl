<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:local="http://localhost" xmlns:saxon="http://saxon.sf.net/" exclude-result-prefixes="#all" version="2.0">
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:output name="serialise1" encoding="utf-8" method="xhtml" indent="no" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:preserve-space elements="text"/>

	<!-- BA: [07/10/15] - Add qualifications key doc  - start-->
	<xsl:variable name="obj_qualifications">
		<xsl:copy-of select="doc('qualifications.xml')/qualifications"/>
	</xsl:variable>	
	<!-- BA: [07/10/15] - Add qualifications key doc  - end-->
	
	<xsl:template match="/">
		<xsl:variable name="baseuri" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/scw/select/'"/>
		<!-- <xsl:variable name="query" select="'r.isofcontenttype.pid:uk-ac-man-con%5C:20 AND x.lastmodifieddate:[NOW-3DAY TO NOW]'"/>-->
		<xsl:variable name="query" select="'r.isofcontenttype.pid:uk-ac-man-con%5C:20+AND+x.state:Active+AND+m.genre.version:%22Doctoral+level+ETD+-+final%22'"/>
		<!--  m.genre.version:&quot;Doctoral level ETD - final&quot;-->
		<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=100000&amp;indent=off')"/>
		<!--<xsl:variable name="uri" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=100&amp;rows=5&amp;indent=off')"/>-->
		<xsl:variable name="doc" select="doc($uri)"/>
		
		<xsl:apply-templates select="$doc/response"/>
	</xsl:template>
	<xsl:template match="response">
		<objects>
			<xsl:apply-templates select="result/doc"/>
		</objects>
	</xsl:template>
	<xsl:template match="doc">
		<xsl:variable name="baseurietd" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/etd/select/'"/>
		<object>
			<STUDENT_THESIS_ID><xsl:apply-templates select="str[@name='PID']"/></STUDENT_THESIS_ID>
			
			<!-- BA: [07/10/15] - Add qualifications key from key doc  - start-->
			<!--<QUALIFICATION_LEVEL><xsl:apply-templates select="str[@name='m.note.degreelevel']"/></QUALIFICATION_LEVEL>-->			
			<xsl:variable name="qual" select="normalize-space(str[@name='m.note.degreelevel'])"/>			                  
			<xsl:choose>
				<xsl:when test="exists($obj_qualifications/qualifications/qualification[@str = $qual]/key)">
					<QUALIFICATION_LEVEL><xsl:value-of select="$obj_qualifications/qualifications/qualification[@str = $qual]/key"/></QUALIFICATION_LEVEL>					
				</xsl:when>
				<xsl:otherwise>
					<QUALIFICATION_LEVEL><xsl:value-of select="'unkn'"/></QUALIFICATION_LEVEL>					
				</xsl:otherwise>
			</xsl:choose>
			<!-- BA: [07/10/15] - Add qualifications key from key doc  - end-->
			
			<ORIGINAL_LANGUAGE><xsl:apply-templates select="str[@name='m.languageterm.code']"/></ORIGINAL_LANGUAGE>
			<TITLE_ORIGINAL_LANGUAGE><xsl:apply-templates select="str[@name='m.title']"/></TITLE_ORIGINAL_LANGUAGE>
			<ABSTRACT><xsl:apply-templates select="str[@name='m.abstract']"/></ABSTRACT>
			<VISIBILITY><xsl:text>public</xsl:text></VISIBILITY>
			<MANAGED_IN_PURE><xsl:text>FALSE</xsl:text></MANAGED_IN_PURE>
			<AUTHOR_ID><xsl:apply-templates select="arr[@name='r.isbelongsto.source']"/></AUTHOR_ID> 
			<PERSON_ID><xsl:apply-templates select="arr[@name='r.isbelongsto.source']"/></PERSON_ID> 
			<ROLE><xsl:text>author</xsl:text></ROLE>
			<SPONSOR_ID>
				<xsl:apply-templates select="arr[@name='m.name.fnd']">
					<xsl:with-param name="pid"><xsl:value-of select="substring-after(str[@name='PID'],':')"/></xsl:with-param>	
				</xsl:apply-templates>
			</SPONSOR_ID>
			<FREE_KEYWORD><xsl:apply-templates select="arr[@name='m.topic']"/></FREE_KEYWORD>
			<DOCUMENT_ID><xsl:value-of select="concat(substring-after(str[@name='PID'],':'),'_','FULLTEXT')"/></DOCUMENT_ID>
			<TYPE><xsl:text>thesis</xsl:text></TYPE>
			<VALUE><xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?dsid=FULL-TEXT.PDF&amp;pid=' , str[@name='PID'])"/></VALUE>
			<FILE_NAME><xsl:text>FULL_TEXT.PDF</xsl:text></FILE_NAME>
			<MIME_TYPE><xsl:text>application/pdf</xsl:text></MIME_TYPE>
			<STUDENT_THESIS_DOCUMENT_VISIBILITY><xsl:text>public</xsl:text></STUDENT_THESIS_DOCUMENT_VISIBILITY>
			
			<!--<xsl:variable name="pid"><xsl:value-of select="'uk-ac-man-etd:252'"/></xsl:variable>-->
			<xsl:variable name="pid"><xsl:value-of select="arr[@name='r.isbelongstoetdwindow.pid']/str"/></xsl:variable>
			<xsl:variable name="queryetd" select="concat('PID:',replace($pid,':','?'))"/>
			<xsl:variable name="urietd" select="concat($baseurietd,'?q=',$queryetd,'&amp;version=2.2&amp;start=0&amp;rows=1&amp;indent=off')"/>
			<xsl:variable name="docetd">
				<xsl:copy-of select="doc($urietd)/response/result/doc"/>
			</xsl:variable>
			
			<PROGRAMME_ID><xsl:apply-templates select="$docetd/doc/str[@name='e.programmeid']"/></PROGRAMME_ID>
			<PLAN_ID><xsl:apply-templates select="$docetd/doc/str[@name='e.planid']"/></PLAN_ID>
			<STUDENT_THESIS_SUPERVISOR><xsl:apply-templates select="arr[@name='r.hasmainsupervisor.source']"/></STUDENT_THESIS_SUPERVISOR>
			
		</object>
	</xsl:template>
	<xsl:template match="arr[@name='r.isbelongsto.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='r.hasmainsupervisor.source']">
		<xsl:value-of select="tokenize(.,'\|\|')[3]"/>
	</xsl:template>
	<xsl:template match="arr[@name='m.name.fnd']">
		<xsl:param name="pid"/>
		<xsl:for-each select="str">
			<sponsor>
				<xsl:value-of select="concat($pid,'_', replace(.,' ',''))"/>
			</sponsor>		
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="arr[@name='m.topic']">
		<xsl:for-each select="str">
			<topic>
				<xsl:value-of select="."/>
			</topic>		
		</xsl:for-each>
	</xsl:template>
	
	<!-- BA: 07/01/16 - Define explicit templates for title and abstract so as to do manual escape of extended ASCII chars - start -->
	<xsl:template match="str[@name='m.abstract'] | str[@name='m.title']">
		<xsl:variable name="find">‘’“”„–—−-čłśćęİşńĭŠḥīźṣā™′ğÖ†Å</xsl:variable>
		<xsl:variable name="replace">''"""----clsceIsniShizsa 'gO A</xsl:variable>
		<xsl:value-of select="replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(translate(.,$find,$replace),'α','alpha'),'κ','kappa'),'∞','infinity'),'œ','oe'),'λ','Gamma'),'…','...'),'ﬁ','fi'),'σ','sigma'),'π','pie'),'μm','micro metre'),'Δ','Delta'),'≥','greater than or equal to'),'Œ','OE'),'≤','smaller or equal to'),'δ','delta')"/>		
	</xsl:template>
	<!-- BA: 07/01/16 - Define explicit templates for title and abstract so as to do manual escape of extended ASCII chars - end -->
	
	
</xsl:stylesheet>
