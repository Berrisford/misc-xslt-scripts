<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="xs saxon" version="2.0" xmlns:saxon="http://saxon.sf.net/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="UTF-8" indent="no" method="xml" />
	<xsl:output encoding="UTF-8" indent="no" method="xhtml" name="serialise1" omit-xml-declaration="yes" />
	<xsl:strip-space elements="*" />
	<xsl:preserve-space elements="text" />
	<!--		<xsl:include href="biblio/solr2biblio_v1-0.xsl" />-->
	<xsl:include href="../biblio/solr2biblio_v1-0.xsl" />
	<xsl:variable name="citationstyle" select="'chicago'" />
	<xsl:variable name="baseuri" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/scw/select/'" />
	<!-- <xsl:variable name="query" select="'m.year:[2014 TO 2016] AND NOT(r.isofcontenttype.pid:uk-ac-man-con?20 OR r.isofcontenttype.pid:uk-ac-man-con?21)'"/>-->
	<xsl:variable name="query" select="'x.lastmodifieddate:[NOW-24HOUR TO NOW] AND NOT(r.isofcontenttype.pid:uk-ac-man-con?20 OR r.isofcontenttype.pid:uk-ac-man-con?21)'" />  
	 <xsl:variable name="docrows" select="doc(concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=0&amp;indent=off'))/response/result[@name='response']/@numFound" />
	<!--   <xsl:variable name="docsource" select="doc('index_old.xml')"/>-->
	<xsl:variable name="docsource" select="doc('../../xml/refincremental/xml/index_old.xml')" />
	<xsl:variable name="docupdates">
		<xsl:variable name="pub_modified" select="doc(concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=',$docrows,'&amp;indent=off'))" />
		<xsl:apply-templates mode="update" select="$pub_modified" />
	</xsl:variable>
	<xsl:variable name="authornums" select="doc('numberofauthors.xml')" />
	<xsl:variable name="articlenumbers" select="doc('articlenumbers.xml')" />
	<xsl:template match="/">
		<objects>
			<xsl:attribute name="lastmodified" select="current-dateTime()" />
			<xsl:apply-templates select="$docupdates/objects/object[party_number!='' and (output_year='2014' or output_year='2015' or output_year='2016')] | $docsource/objects/object[not(output_id = $docupdates/objects/object/output_id)]" />
		</objects>
	</xsl:template>
	<xsl:template match="object">
		<object>
			<xsl:apply-templates />
		</object>
	</xsl:template>
	<xsl:template match="*">
		<xsl:copy-of select="." />
	</xsl:template>
	<xsl:template match="/" mode="update">
		<xsl:apply-templates mode="#current" select="response/result[@name='response']" />
	</xsl:template>
	<xsl:template match="result" mode="update">
		<objects>
			<xsl:apply-templates mode="#current" select="doc" />
		</objects>
	</xsl:template>
	<xsl:template match="doc" mode="update">
		<xsl:variable name="pid" select="str[@name='PID']"/>
		<xsl:variable name="numberofauthors" select="$authornums/objects/object[pid=$pid]/numberofauthors"/>
		<xsl:variable name="outputid" select="substring-after($pid,':')" />
		<xsl:variable name="outputstate" select="if (str[@name='x.state']='Active') then 'Open' else 'Closed'" />
		<xsl:variable name="citation">
			<xsl:apply-templates mode="biblio" select=".">
				<xsl:with-param name="citationstyle" select="$citationstyle" />
			</xsl:apply-templates>
		</xsl:variable>
		<xsl:variable name="outputtype" select="tokenize(str[@name='r.isofcontenttype.source'],'\|\|')[1]" />
		<xsl:variable name="outputyear" select="substring(str[@name='m.year'],1,4)" />
		<xsl:variable name="outputtitle">
				<xsl:variable name="find">‘’“”„–—−-čłśćęİşńĭŠḥīźṣā™′ğÖ†Å</xsl:variable>
				<xsl:variable name="replace">''"""----clsceIsniShizsa 'gO A</xsl:variable>
				<xsl:value-of select="replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(translate(str[@name='m.title'],$find,$replace),'α','alpha'),'κ','kappa'),'∞','infinity'),'œ','oe'),'λ','Gamma'),'…','...'),'ﬁ','fi'),'σ','sigma'),'π','pie'),'μm','micro metre'),'Δ','Delta'),'≥','greater than or equal to'),'Œ','OE'),'≤','smaller or equal to'),'δ','delta')"/>
		</xsl:variable>  
		<xsl:variable name="outputdoi">
			<xsl:variable name="tmp" select="replace(replace(replace(replace(replace(str[@name='m.identifier.doi'],'http://dx.doi.org/',''),'Doi:',''),'DOI:',''),'doi:',''),' ','')" />
			<xsl:if test="substring($tmp,1,3)='10.'">
				<xsl:value-of select="substring($tmp,1,255)" />
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="outputdoiformatted">
			<xsl:if test="$outputdoi != ''">
				<xsl:element name="a">
					<xsl:attribute name="href" select="concat('http://dx.doi.org/',$outputdoi)" />
					<xsl:attribute name="target" select="'_doi'" />
					<xsl:value-of select="'doi'" />
				</xsl:element>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="outputidformatted">
			<xsl:if test="$outputstate = 'Open'">
				<xsl:element name="a">
					<xsl:attribute name="href" select="concat('http://www.manchester.ac.uk/escholar/',str[@name='PID'])" />
					<xsl:attribute name="target" select="'_eSch'" />
					<xsl:value-of select="'eScholar'" />
				</xsl:element>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="outputformatted">
			<xsl:copy-of select="$citation" />
			<xsl:if test="$outputstate = 'Open'">
				<xsl:text> eScholarID:</xsl:text>
				<xsl:element name="a">
					<xsl:attribute name="href" select="concat('http://www.manchester.ac.uk/escholar/',str[@name='PID'])" />
					<xsl:value-of select="$outputid" />
				</xsl:element>
			</xsl:if>
			<xsl:if test="$outputstate = 'Open' and $outputdoi != ''">
				<xsl:text> |</xsl:text>
			</xsl:if>
			<xsl:if test="$outputdoi != ''">
				<xsl:text> doi:</xsl:text>
				<xsl:element name="a">
					<xsl:attribute name="href" select="concat('http://dx.doi.org/',$outputdoi)" />
					<xsl:value-of select="$outputdoi" />
				</xsl:element>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="outputunformatted">
			<xsl:value-of select="$outputformatted" />
		</xsl:variable>
		<xsl:variable name="outputreftype">
			<xsl:choose>
				<xsl:when test="str[@name='r.isofcontenttype.pid']='uk-ac-man-con:4'">
					<!--Book-->
					<xsl:choose>
						<xsl:when test="str[@name='m.genre.form']='Scholarly edition'">
							<!--Scholarly edition-->
							<xsl:text>R</xsl:text>
						</xsl:when>
						<xsl:when test="str[@name='m.genre.version']='Authored book'">
							<!--Authored book-->
							<xsl:text>A</xsl:text>
						</xsl:when>
						<xsl:when test="str[@name='m.genre.version']='Edited book'">
							<!--Edited book-->
							<xsl:text>B</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>A</xsl:text>
							<!--Authored book-->
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="str[@name='r.isofcontenttype.pid']='uk-ac-man-con:3'">
					<!--Chapter in book-->
					<xsl:text>C</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='r.isofcontenttype.pid']='uk-ac-man-con:1'">
					<!--Journal article-->
					<xsl:text>D</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='r.isofcontenttype.pid']='uk-ac-man-con:2'">
					<!--Conference contribution-->
					<xsl:text>E</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='r.isofcontenttype.pid']='uk-ac-man-con:10'">
					<!--Exhibition-->
					<xsl:text>M</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='r.isofcontenttype.pid']='uk-ac-man-con:11'">
					<!--Exhibition-->
					<xsl:text>M</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='r.isofcontenttype.pid']='uk-ac-man-con:13'">
					<!--Patent-->
					<xsl:text>F</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='r.isofcontenttype.pid']='uk-ac-man-con:16'">
					<!--Working paper-->
					<xsl:text>U</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='r.isofcontenttype.pid']='uk-ac-man-con:5'">
					<!--Report-->
					<xsl:choose>
						<xsl:when test="str[@name='m.genre.version']='Report for an external body'">
							<!--Report for an external body-->
							<xsl:text>N</xsl:text>
						</xsl:when>
						<xsl:when test="str[@name='m.genre.version']='Confidential report'">
							<!--Confidential report-->
							<xsl:text>O</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<!--Report for an external body-->
							<xsl:text>N</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="str[@name='r.isofcontenttype.pid']='uk-ac-man-con:14'">
					<!--Report-->
					<xsl:text>N</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='r.isofcontenttype.pid']='uk-ac-man-con:7'">
					<!--Composition-->
					<xsl:text>J</xsl:text>
				</xsl:when>
				<xsl:when test="str[@name='r.isofcontenttype.pid']='uk-ac-man-con:19'">
					<!--Lite-Cite submission-->
					<xsl:choose>
						<xsl:when test="str[@name='m.genre.version']='Dataset'">
							<!--Dataset-->
							<xsl:text>S</xsl:text>
						</xsl:when>
						<xsl:when test="str[@name='m.genre.version']='Performance'">
							<!--Confidential report-->
							<xsl:text>I</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>T</xsl:text>						
						</xsl:otherwise>
					</xsl:choose>				
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>T</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="outputplace">
			<xsl:choose>
				<xsl:when test="str[@name='m.placeterm']!=''">
					<xsl:value-of select="substring(str[@name='m.placeterm'],1,255)" />		
				</xsl:when>
				<xsl:when test="str[@name='m.host.placeterm']!=''">
					<xsl:value-of select="substring(str[@name='m.host.placeterm'],1,255)" />		
				</xsl:when>
				<xsl:otherwise/>				
			</xsl:choose>		
		</xsl:variable>
		<xsl:variable name="outputpublisher">
			<xsl:value-of select="substring(str[@name='m.publisher'],1,255)" />
		</xsl:variable>
		<xsl:variable name="outputvolumetitle">
			<xsl:choose>
				<xsl:when test="str[@name='m.host.title']!=''">
					<xsl:value-of select="substring(str[@name='m.host.title'],1,255)" />
				</xsl:when>
				<xsl:when test="str[@name='m.host.host.title']!=''">
					<xsl:value-of select="substring(str[@name='m.host.host.title'],1,255)" />
				</xsl:when>
				<xsl:when test="str[@name='m.host.title.abbreviated']!=''">
					<xsl:value-of select="substring(str[@name='m.host.title.abbreviated'],1,255)" />
				</xsl:when>
				<xsl:otherwise />
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="outputarticlenumber">
			<xsl:if test="exists($articlenumbers/objects/object[pid=$pid])">
				<xsl:choose>
					<xsl:when test="contains(lower-case(str[@name='m.host.page.start']),'article')">
						<xsl:value-of select="str[@name='m.host.page.start']" />
					</xsl:when>
					<xsl:when test="not(contains(lower-case(str[@name='m.host.page.start']),'article')) and contains(str[@name='m.host.page.start'],'art')">
						<xsl:value-of select="substring-after(str[@name='m.host.page.start'],'art')" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="str[@name='m.host.page.start']" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="outputvolume">
			<xsl:value-of select="substring(str[@name='m.host.volume'],1,15)" />
		</xsl:variable>
		<xsl:variable name="outputissue">
			<xsl:value-of select="substring(str[@name='m.host.issue'],1,15)" />
		</xsl:variable>
		<xsl:variable name="outputfirstpage">
			<xsl:if test="not(exists($articlenumbers/objects/object[pid=$pid]))">
				<xsl:choose>
					<xsl:when test="str[@name='m.host.page.start']!=''">
						<xsl:value-of select="substring(str[@name='m.host.page.start'],1,8)" />
					</xsl:when>
					<xsl:when test="str[@name='m.host.page.list']!=''">
						<xsl:value-of select="substring(substring-before(str[@name='m.host.page.list'],'-'),1,8)" />
					</xsl:when>
					<xsl:otherwise />
				</xsl:choose>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="outputisbn">
			<xsl:choose>
				<xsl:when test="str[@name='m.identifier.isbn']!=''">
					<xsl:value-of select="substring(str[@name='m.identifier.isbn'],1,24)" />
				</xsl:when>
				<xsl:when test="str[@name='m.host.identifier.isbn']!=''">
					<xsl:value-of select="substring(str[@name='m.host.identifier.isbn'],1,24)" />
				</xsl:when>
				<xsl:otherwise />
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="outputissn">
			<xsl:choose>
				<xsl:when test="str[@name='m.identifier.issn']!=''">
					<xsl:value-of select="substring(str[@name='m.identifier.issn'],1,24)" />
				</xsl:when>
				<xsl:when test="str[@name='m.host.identifier.issn']!=''">
					<xsl:value-of select="substring(str[@name='m.host.identifier.issn'],1,24)" />
				</xsl:when>
				<xsl:otherwise />
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="outputpatentnumber">
			<xsl:value-of select="substring(str[@name='m.identifier.patentnumber'],1,24)" />
		</xsl:variable>
		<xsl:variable name="outputurl" />
		<xsl:variable name="outputnumberofauthors">
			<xsl:choose>
				<xsl:when test="arr[@name='m.name.aut']/str!=''">
					<xsl:value-of select="count(arr[@name='m.name.aut']/str)" />
				</xsl:when>
				<xsl:when test="if ($numberofauthors castable as xs:integer) then  $numberofauthors&gt;0 else false()">
					<xsl:value-of select="$numberofauthors" />
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="outputmediaofsubmitted" />
		<xsl:for-each select="distinct-values(arr[@name='r.isbelongsto.source']/str)">
			<xsl:call-template name="personObject">
				<xsl:with-param name="id" select="tokenize(.,'\|\|')[3]" />
				<xsl:with-param name="outputid" select="$outputid" />
				<xsl:with-param name="outputtype" select="$outputtype" />
				<xsl:with-param name="outputtitle" select="$outputtitle" />
				<xsl:with-param name="outputyear" select="$outputyear" />
				<xsl:with-param name="outputformatted" select="$outputformatted" />
				<xsl:with-param name="outputunformatted" select="$outputunformatted" />
				<xsl:with-param name="outputidformatted" select="$outputidformatted" />
				<xsl:with-param name="outputdoi" select="$outputdoi" />
				<xsl:with-param name="outputdoiformatted" select="$outputdoiformatted" />
				<xsl:with-param name="outputstate" select="$outputstate" />
				<xsl:with-param name="outputreftype" select="$outputreftype" />
				<xsl:with-param name="outputplace" select="$outputplace" />
				<xsl:with-param name="outputpublisher" select="$outputpublisher" />
				<xsl:with-param name="outputvolumetitle" select="$outputvolumetitle" />
				<xsl:with-param name="outputarticlenumber" select="$outputarticlenumber" />
				<xsl:with-param name="outputvolume" select="$outputvolume" />
				<xsl:with-param name="outputissue" select="$outputissue" />
				<xsl:with-param name="outputfirstpage" select="$outputfirstpage" />
				<xsl:with-param name="outputisbn" select="$outputisbn" />
				<xsl:with-param name="outputissn" select="$outputissn" />
				<xsl:with-param name="outputpatentnumber" select="$outputpatentnumber" />
				<xsl:with-param name="outputurl" select="$outputurl" />
				<xsl:with-param name="outputnumberofauthors" select="$outputnumberofauthors" />
				<xsl:with-param name="outputmediaofsubmitted" select="$outputmediaofsubmitted" />
			</xsl:call-template>
		</xsl:for-each>
		<xsl:for-each select=".[not(arr[@name='r.isbelongsto.source']/str)]">
			<xsl:call-template name="personObject">
				<xsl:with-param name="id" select="''" />
				<xsl:with-param name="outputid" select="$outputid" />
				<xsl:with-param name="outputtype" select="$outputtype" />
				<xsl:with-param name="outputtitle" select="$outputtitle" />
				<xsl:with-param name="outputyear" select="$outputyear" />
				<xsl:with-param name="outputformatted" select="$outputformatted" />
				<xsl:with-param name="outputunformatted" select="$outputunformatted" />
				<xsl:with-param name="outputidformatted" select="$outputidformatted" />
				<xsl:with-param name="outputdoi" select="$outputdoi" />
				<xsl:with-param name="outputdoiformatted" select="$outputdoiformatted" />
				<xsl:with-param name="outputstate" select="$outputstate" />
				<xsl:with-param name="outputreftype" select="$outputreftype" />
				<xsl:with-param name="outputplace" select="$outputplace" />
				<xsl:with-param name="outputpublisher" select="$outputpublisher" />
				<xsl:with-param name="outputvolumetitle" select="$outputvolumetitle" />
				<xsl:with-param name="outputarticlenumber" select="$outputarticlenumber" />
				<xsl:with-param name="outputvolume" select="$outputvolume" />
				<xsl:with-param name="outputissue" select="$outputissue" />
				<xsl:with-param name="outputfirstpage" select="$outputfirstpage" />
				<xsl:with-param name="outputisbn" select="$outputisbn" />
				<xsl:with-param name="outputissn" select="$outputissn" />
				<xsl:with-param name="outputpatentnumber" select="$outputpatentnumber" />
				<xsl:with-param name="outputurl" select="$outputurl" />
				<xsl:with-param name="outputnumberofauthors" select="$outputnumberofauthors" />
				<xsl:with-param name="outputmediaofsubmitted" select="$outputmediaofsubmitted" />
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="personObject">
		<xsl:param name="id" />
		<xsl:param name="outputid" />
		<xsl:param name="sourceid" />
		<xsl:param name="outputtype" />
		<xsl:param name="outputtitle" />
		<xsl:param name="outputyear" />
		<xsl:param name="outputdateissued" />
		<xsl:param name="outputformatted" />
		<xsl:param name="outputunformatted" />
		<xsl:param name="outputidformatted" />
		<xsl:param name="outputdoi" />
		<xsl:param name="outputdoiformatted" />
		<xsl:param name="outputstate" />
		<xsl:param name="outputreftype" />
		<xsl:param name="outputplace" />
		<xsl:param name="outputpublisher" />
		<xsl:param name="outputvolumetitle" />
		<xsl:param name="outputarticlenumber" />
		<xsl:param name="outputvolume" />
		<xsl:param name="outputissue" />
		<xsl:param name="outputfirstpage" />
		<xsl:param name="outputisbn" />
		<xsl:param name="outputissn" />
		<xsl:param name="outputpatentnumber" />
		<xsl:param name="outputurl" />
		<xsl:param name="outputnumberofauthors" />
		<xsl:param name="outputmediaofsubmitted" />
		<object>
			<party_number>
				<xsl:value-of select="$id" />
			</party_number>
			<output_id>
				<xsl:value-of select="$outputid" />
			</output_id>
			<output_type>
				<xsl:value-of select="$outputtype" />
			</output_type>
			<output_year>
				<xsl:value-of select="$outputyear" />
			</output_year>
			<output_title>
				<xsl:value-of select="$outputtitle" />
			</output_title>
			<output>
				<xsl:value-of select="$outputunformatted" />
			</output>
			<output_formatted>
				<!-- <xsl:value-of select="saxon:serialize($outputformatted,'serialise1')" />-->
				<xsl:variable name="output_formatted_before"><xsl:value-of select="saxon:serialize($outputformatted,'serialise1')" /></xsl:variable> 
				<xsl:variable name="find">‘’“”„–—−-čłśćęİşńĭŠḥīźṣā™′ğÖ†Å</xsl:variable>
				<xsl:variable name="replace">''"""----clsceIsniShizsa 'gO A</xsl:variable>
				<xsl:value-of select="replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(translate($output_formatted_before,$find,$replace),'α','alpha'),'κ','kappa'),'∞','infinity'),'œ','oe'),'λ','Gamma'),'…','...'),'ﬁ','fi'),'σ','sigma'),'π','pie'),'μm','micro metre'),'Δ','Delta'),'≥','greater than or equal to'),'Œ','OE'),'≤','smaller or equal to'),'δ','delta')"/>
			</output_formatted>
			<output_doi>
				<xsl:value-of select="$outputdoi" />
			</output_doi>
			<output_doi_formatted>
				<xsl:value-of select="saxon:serialize($outputdoiformatted,'serialise1')" />
			</output_doi_formatted>
			<output_id_formatted>
				<xsl:value-of select="saxon:serialize($outputidformatted,'serialise1')" />
			</output_id_formatted>
			<output_state>
				<xsl:value-of select="$outputstate" />
			</output_state>
			<output_reftype>
				<xsl:value-of select="$outputreftype" />
			</output_reftype>
			<output_place>
				<xsl:value-of select="$outputplace" />
			</output_place>
			<output_publisher>
				<xsl:value-of select="$outputpublisher" />
			</output_publisher>
			<output_volumetitle>
				<xsl:value-of select="$outputvolumetitle" />
			</output_volumetitle>
			<output_articlenumber>
				<xsl:value-of select="normalize-space($outputarticlenumber)" />
			</output_articlenumber>
			<output_volume>
				<xsl:value-of select="$outputvolume" />
			</output_volume>
			<output_issue>
				<xsl:value-of select="$outputissue" />
			</output_issue>
			<output_firstpage>
				<xsl:value-of select="$outputfirstpage" />
			</output_firstpage>
			<output_isbn>
				<xsl:value-of select="$outputisbn" />
			</output_isbn>
			<output_issn>
				<xsl:value-of select="$outputissn" />
			</output_issn>
			<output_patentnumber>
				<xsl:value-of select="$outputpatentnumber" />
			</output_patentnumber>
			<output_url>
				<xsl:value-of select="$outputurl" />
			</output_url>
			<output_numberofauthors>
				<xsl:value-of select="$outputnumberofauthors" />
			</output_numberofauthors>
			<output_mediaofsubmitted>
				<xsl:value-of select="$outputmediaofsubmitted" />
			</output_mediaofsubmitted>
		</object>
	</xsl:template>

</xsl:stylesheet>
