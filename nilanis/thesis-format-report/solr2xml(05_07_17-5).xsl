<?xml version="1.0"?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="no"/>	
	
	<!--<xsl:variable name="baseuri" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/nonscw/select/'"/>-->
	
	<xsl:variable name="orgs">
		<xsl:copy-of select="document('faculties-and-schools.xml')/orgs"/>
	</xsl:variable>
	
	
	<xsl:template match="/">
		<objects> 
			<xsl:apply-templates select="response/result/doc"/>
		</objects>
	</xsl:template>
	
	<xsl:template match="doc">

		<xsl:variable name="all-schools">
			<xsl:for-each select="$orgs/orgs/faculty/school">
				<xsl:value-of select="concat('|', @pid, '|')"/>						
			</xsl:for-each>		
		</xsl:variable>

		<xsl:variable name="valid-school">
			<xsl:call-template name="validate-schools">
				<xsl:with-param name="current-schools">
					<xsl:copy-of select="arr[@name='r.isbelongstoorg.pid']/str"/>
				</xsl:with-param>
				<xsl:with-param name="all-schools">
					<xsl:copy-of select="$all-schools"/>
				</xsl:with-param>
			</xsl:call-template>			
		</xsl:variable>
		
		<object>			
			<id><xsl:value-of select="str[@name='PID']"/></id>  
			<title><xsl:value-of select="str[@name='m.title']"/></title>  
			<submission-date><xsl:value-of select="substring-before(date[@name='x.createddate'], 'T')"/></submission-date>
			<thesis-type><xsl:value-of select="str[@name='m.genre.form']"/></thesis-type>									
			<xsl:choose>				
				<xsl:when test="exists(arr[@name='r.isbelongstoorg.pid']) and contains($valid-school, 'true')">
					<has-valid-isbelongstoorg.pid/>															
					<faculty>
					<xsl:for-each select="arr[@name='r.isbelongstoorg.pid']/str">
						<xsl:variable name="current-pid" as="text()">
							<xsl:value-of select="."/>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="exists($orgs/orgs/faculty/school[@pid=$current-pid])">
									<xsl:value-of select="concat('|', $orgs/orgs/faculty[exists(school[@pid=$current-pid])]/@label, '|')"/>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
					</faculty>
					<school>
						<xsl:for-each select="arr[@name='r.isbelongstoorg.pid']/str">
							<xsl:variable name="current-pid" as="text()">
								<xsl:value-of select="."/>
							</xsl:variable>
							<xsl:choose>
								<xsl:when test="exists($orgs/orgs/faculty/school[@pid=$current-pid])">
									<xsl:value-of select="concat('|', $orgs/orgs/faculty/school[@pid=$current-pid]/@name, '|')"/>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</school>															
				</xsl:when>
				<xsl:when test="exists(arr[@name='r.isbelongstoorg.pid']) and not(contains($valid-school, 'true'))">
					<!--<has-INVALID-isbelongstoorg.pid/>-->
					<faculty>
					<xsl:for-each select="arr[@name='r.wasbelongstoorg.pid']/str">
						<xsl:variable name="current-pid" as="text()">
							<xsl:value-of select="."/>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="exists($orgs/orgs/faculty/school[@pid=$current-pid])">
									<xsl:value-of select="concat('|', $orgs/orgs/faculty[exists(school[@pid=$current-pid])]/@label, '|')"/>
<!--								
								<school>
									<xsl:value-of select="$orgs/orgs/faculty/school[@pid=$current-pid]/@name"/>
								</school>
-->								
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
					</faculty>
					<school>
						<xsl:for-each select="arr[@name='r.wasbelongstoorg.pid']/str">
							<xsl:variable name="current-pid" as="text()">
								<xsl:value-of select="."/>
							</xsl:variable>
							<xsl:choose>
								<xsl:when test="exists($orgs/orgs/faculty/school[@pid=$current-pid])">
									<xsl:value-of select="concat('|', $orgs/orgs/faculty/school[@pid=$current-pid]/@name, '|')"/>
									<!--								
								<school>
									<xsl:value-of select="$orgs/orgs/faculty/school[@pid=$current-pid]/@name"/>
								</school>
-->								
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</school>					

				</xsl:when>				
				<xsl:when test="exists(arr[@name='r.wasbelongstoorg.pid'])">
					<!--<has-WASbelongstoorg.pid/>-->
					<faculty>
					<xsl:for-each select="arr[@name='r.wasbelongstoorg.pid']/str">
						<xsl:variable name="current-pid" as="text()">
							<xsl:value-of select="."/>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="exists($orgs/orgs/faculty/school[@pid=$current-pid])">
								<!--<xsl:value-of select="concat('|', $orgs/orgs/faculty[exists(school[@pid=$current-pid])]/@label, '|')"/>-->
								<xsl:for-each select="$orgs/orgs/faculty[exists(school[@pid=$current-pid])]">
									<xsl:value-of select="concat('|', ./@label, '|')"/>									
								</xsl:for-each>								
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
					</faculty>
					<school>
						<xsl:for-each select="arr[@name='r.wasbelongstoorg.pid']/str">
							<xsl:variable name="current-pid" as="text()">
								<xsl:value-of select="."/>
							</xsl:variable>
							<xsl:choose>
								<xsl:when test="exists($orgs/orgs/faculty/school[@pid=$current-pid])">
									<!--<xsl:value-of select="concat('|', $orgs/orgs/faculty/school[@pid=$current-pid]/@name, '|')"/>-->
									<xsl:for-each select="$orgs/orgs/faculty/school[@pid=$current-pid]">
										<xsl:value-of select="concat('|', ./@name, '|')"/>									
									</xsl:for-each>																	
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</school>
					
				</xsl:when>
			</xsl:choose>
			<xsl:apply-templates select="arr[@name='r.hasmainsupervisor.source']"/>						
		</object>
	</xsl:template>
	
	<xsl:template name="validate-schools">
		<xsl:param name="current-schools"/>
		<xsl:param name="all-schools"/>
		<xsl:for-each select="$current-schools/str">
			<xsl:variable name="test-string">
				<xsl:value-of select="concat('|', ./text(), '|')"/>
			</xsl:variable>
			<xsl:choose>
				<xsl:when test="contains($all-schools, $test-string)">
					<xsl:value-of select="'true'"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'false'"/>									
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>								
	</xsl:template>
	
	<xsl:template match="arr[@name='r.isbelongstoorg.pid']" mode="string">
		<xsl:for-each select="str">
			<xsl:value-of select="concat('|', ., '|')"/>			
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="arr[@name='r.hasmainsupervisor.source']">
		<xsl:for-each select="./str">
			<xsl:variable name="current-node" select="concat('main-supervisor', position())"/>						
			
			<xsl:element name="{$current-node}">
				<xsl:value-of select="tokenize(., '\|\|')[1]"/>						
			</xsl:element>
			
		</xsl:for-each>
		
	</xsl:template>
		
</xsl:stylesheet>
