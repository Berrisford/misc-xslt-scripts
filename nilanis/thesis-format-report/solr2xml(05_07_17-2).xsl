<?xml version="1.0"?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="no"/>	
	
	<xsl:variable name="baseuri" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/nonscw/select/'"/>
	
	<xsl:variable name="orgs">
		<xsl:copy-of select="document('faculties-and-schools.xml')/orgs"/>
	</xsl:variable>
	
	<xsl:template match="/">
		<!--<xsl:result-document href="objects.xml">-->
			<objects> 
				<xsl:apply-templates select="response/result/doc"/>
			</objects>
		<!--</xsl:result-document>-->
	</xsl:template>
	<xsl:template match="doc">
		<object>
			<id><xsl:value-of select="str[@name='PID']"/></id>  
			<title><xsl:value-of select="str[@name='m.title']"/></title>  
			<!-- just the date-->
			<submission-date><xsl:value-of select="substring-before(date[@name='x.createddate'], 'T')"/></submission-date>
			<thesis-type><xsl:value-of select="str[@name='m.genre.form']"/></thesis-type>
			<!--
			<faculty> and <school> - work this out from r.isbelongstoorg.pid and r.wasbelongstoorg.pid + faculties-and-school.xml
			-->
			<!--<xsl:apply-templates select="arr[@name='r.isbelongstoorg.pid']"/>-->
			<!--<xsl:apply-templates select="arr[@name='r.wasbelongstoorg.pid']"/>-->
			<xsl:choose>
				<xsl:when test="exists(arr[@name='r.isbelongstoorg.pid'])">
					<has-isbelongstoorg.pid/>
					<xsl:for-each select="arr[@name='r.isbelongstoorg.pid']/str">
						<org-pid><xsl:value-of select="text()"/></org-pid>
						<xsl:variable name="current-pid" as="text()">
							<xsl:value-of select="."/>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="exists($orgs/orgs/faculty/school[@pid=$current-pid])">
								<current-faculty>
									<xsl:value-of select="$orgs/orgs/faculty[exists(school[@pid=$current-pid])]/@label"/>
								</current-faculty>
								<current-school>
									<xsl:value-of select="$orgs/orgs/faculty/school[@pid=$current-pid]/@name"/>
								</current-school>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
				</xsl:when>
			</xsl:choose>
<!--			
			<xsl:apply-templates select="arr[@name='r.isbelongstoorg.pid']" mode="school"/>
			<xsl:apply-templates select="arr[@name='r.wasbelongstoorg.pid']" mode="school"/>
			<xsl:apply-templates select="arr[@name='r.hasmainsupervisor.source']"/>
-->			
			<!-- <main-supervisor> from r.hasmainsupervisor.source-->
			
			
		</object>
	</xsl:template>
	
	<xsl:template match="arr[@name='r.hasmainsupervisor.source']">
		<xsl:for-each select="./str">
			<xsl:variable name="current-node" select="concat('main-supervisor', position())"/>						
			
			<xsl:element name="{$current-node}">
				<xsl:value-of select="tokenize(., '\|\|')[1]"/>						
			</xsl:element>
			
		</xsl:for-each>
		
	</xsl:template>
	
	
	
	<xsl:template match="arr[@name='r.isbelongstoorg.pid']">
		<xsl:for-each select="./str">
			<xsl:variable name="current-pid" select="./text()"/>
			
			<xsl:if test="$orgs/orgs/faculty/school/@pid=$current-pid">
				<xsl:for-each select="distinct-values($orgs/orgs[faculty/school[@pid=$current-pid]]/faculty/@label)">
					<xsl:variable name="current-node" select="concat('current-faculty0', position())"/>						
					
					<xsl:element name="{$current-node}">
						<xsl:value-of select="."/>						
					</xsl:element>
				</xsl:for-each>
			</xsl:if>
			
			<xsl:if test="$orgs/orgs/faculty/@pid=$current-pid">
				<xsl:for-each select="distinct-values($orgs/orgs/faculty[@pid=$current-pid]/@label)">
					<xsl:variable name="current-node" select="concat('current-faculty1', position())"/>						
					
					<xsl:element name="{$current-node}">
						<xsl:value-of select="."/>						
					</xsl:element>
				</xsl:for-each>
				
			</xsl:if>
			
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template match="arr[@name='r.isbelongstoorg.pid']" mode="school">
		<xsl:for-each select="./str">
			<xsl:variable name="current-pid" select="./text()"/>			
			<xsl:if test="$orgs/orgs/faculty/school/@pid=$current-pid">
				<xsl:for-each select="distinct-values($orgs/orgs/faculty/school[@pid=$current-pid]/@name)">
					<xsl:variable name="current-node" select="concat('current-school0', position())"/>						
					
					<xsl:element name="{$current-node}">
						<xsl:value-of select="."/>						
					</xsl:element>
					
					<!--					
					<current-school>
						<xsl:value-of select="."/>
					</current-school>
-->					
				</xsl:for-each>
			</xsl:if>					
		</xsl:for-each>
	</xsl:template>
	
	
	
	<xsl:template match="arr[@name='r.wasbelongstoorg.pid']">
		<xsl:for-each select="./str">
			<xsl:variable name="current-pid" select="./text()"/>
			
			<xsl:if test="$orgs/orgs/faculty/school/@pid=$current-pid">
				<xsl:for-each select="distinct-values($orgs/orgs[faculty/school[@pid=$current-pid]]/faculty/@label)">
					<xsl:variable name="current-node" select="concat('former-faculty0', position())"/>						
					
					<xsl:element name="{$current-node}">
						<xsl:value-of select="."/>						
					</xsl:element>
					
					<!--
					<former-faculty>
						<xsl:value-of select="."/>
					</former-faculty>								
-->
				</xsl:for-each>
			</xsl:if>
			
			<xsl:if test="$orgs/orgs/faculty/@pid=$current-pid">
				<xsl:for-each select="distinct-values($orgs/orgs/faculty[@pid=$current-pid]/@label)">
					<xsl:variable name="current-node" select="concat('former-faculty1', position())"/>						
					
					<xsl:element name="{$current-node}">
						<xsl:value-of select="."/>						
					</xsl:element>
					
					<!--
					<former-faculty>
						<xsl:value-of select="."/>
					</former-faculty>
-->					
				</xsl:for-each>				
			</xsl:if>
			
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template match="arr[@name='r.wasbelongstoorg.pid']" mode="school">
		<xsl:for-each select="./str">
			<xsl:variable name="current-pid" select="./text()"/>			
			<xsl:if test="$orgs/orgs/faculty/school/@pid=$current-pid">
				<xsl:for-each select="distinct-values($orgs/orgs/faculty/school[@pid=$current-pid]/@name)">
					<xsl:variable name="current-node" select="concat('current-school1', position())"/>						
					
					<xsl:element name="{$current-node}">
						<xsl:value-of select="."/>						
					</xsl:element>
					
					<!--
					<former-school>
						<xsl:value-of select="."/>
					</former-school>
					-->
				</xsl:for-each>				
			</xsl:if>			
		</xsl:for-each>
	</xsl:template>
	
</xsl:stylesheet>
