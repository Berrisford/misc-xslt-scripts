<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output indent="yes" method="xml"/>
    
    <xsl:template match="/">
        <xsl:result-document href="result2.xml">
            <objects>
                <xsl:apply-templates select="//object"/>
            </objects>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="object">
        <object>
            <id><xsl:value-of select="id"/></id>
            <title><xsl:value-of select="title"/></title>
            <submission-date><xsl:value-of select="submission-date"/></submission-date>
            <thesis-type><xsl:value-of select="thesis-type"/></thesis-type>
            <faculty><xsl:value-of select="replace(tokenize(faculty, '\|\|')[1], '\|', '')"/></faculty>
            <school><xsl:value-of select="replace(tokenize(school, '\|\|')[1], '\|', '')"/></school>
            <main-supervisor><xsl:value-of select="main-supervisor"/></main-supervisor>
        </object>
    </xsl:template>
    
</xsl:stylesheet>