<?xml version="1.0"?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="no"/>	
	
	<xsl:variable name="baseuri" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/nonscw/select/'"/>
	
	<xsl:variable name="orgs">
		<xsl:copy-of select="document('faculties-and-schools.xml')/orgs"/>
	</xsl:variable>
	
	<xsl:template match="/">
	 	<objects> 
			<xsl:apply-templates select="response/result/doc"/>
		</objects>
	</xsl:template>
	<xsl:template match="doc">
		<object>
			<id><xsl:value-of select="str[@name='PID']"/></id>  
			<title><xsl:value-of select="str[@name='m.title']"/></title>  
			<!-- just the date-->
			<submission-date><xsl:value-of select="substring-before(date[@name='x.createddate'], 'T')"/></submission-date>
			<thesis-type><xsl:value-of select="str[@name='m.genre.form']"/></thesis-type>
			<!--
			<faculty> and <school> - work this out from r.isbelongstoorg.pid and r.wasbelongstoorg.pid + faculties-and-school.xml
			-->
			<xsl:apply-templates select="arr[@name='r.isbelongstoorg.pid']"/>
			<xsl:apply-templates select="arr[@name='r.wasbelongstoorg.pid']"/>
			
			<!-- <main-supervisor> from r.hasmainsupervisor.source-->
			
			
		</object>
	</xsl:template>

	<xsl:template match="arr[@name='r.isbelongstoorg.pid']">
		<xsl:for-each select="./str">
			<xsl:variable name="current-pid" select="./text()"/>
			
			<xsl:if test="$orgs/orgs/faculty/school/@pid=$current-pid">
				<xsl:for-each select="distinct-values($orgs/orgs[faculty/school[@pid=$current-pid]]/faculty/@label)">
					<current-faculty>
						<xsl:value-of select="."/>
					</current-faculty>								
				</xsl:for-each>
				<!--<xsl:for-each select="distinct-values($orgs/orgs[faculty/school[@pid=$current-pid]]/@name)">-->
				<xsl:for-each select="distinct-values($orgs/orgs/faculty/school[@pid=$current-pid]/@name)">
						<current-school>
						<xsl:value-of select="."/>
					</current-school>								
				</xsl:for-each>
			</xsl:if>
			
			<xsl:if test="$orgs/orgs/faculty/@pid=$current-pid">
				<xsl:for-each select="distinct-values($orgs/orgs/faculty[@pid=$current-pid]/@label)">
					<current-faculty>
						<xsl:value-of select="."/>
					</current-faculty>								
				</xsl:for-each>
				
			</xsl:if>
			
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="arr[@name='r.wasbelongstoorg.pid']">
		<xsl:for-each select="./str">
			<xsl:variable name="current-pid" select="./text()"/>
			
			<xsl:if test="$orgs/orgs/faculty/school/@pid=$current-pid">
				<xsl:for-each select="distinct-values($orgs/orgs[faculty/school[@pid=$current-pid]]/faculty/@label)">
					<former-faculty>
						<xsl:value-of select="."/>
					</former-faculty>								
				</xsl:for-each>
				<xsl:for-each select="distinct-values($orgs/orgs/faculty/school[@pid=$current-pid]/@name)">
					<former-school>
						<xsl:value-of select="."/>
					</former-school>								
				</xsl:for-each>				
			</xsl:if>
			
			<xsl:if test="$orgs/orgs/faculty/@pid=$current-pid">
				<xsl:for-each select="distinct-values($orgs/orgs/faculty[@pid=$current-pid]/@label)">
					<former-faculty>
						<xsl:value-of select="."/>
					</former-faculty>								
				</xsl:for-each>				
			</xsl:if>
			
		</xsl:for-each>
	</xsl:template>
	

</xsl:stylesheet>
