<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"/>
    
    <xsl:template match="/">
        <record>
            <xsl:apply-templates select="record/field | record/entity/field"/>
        </record>
    </xsl:template>
    
    <xsl:template match="field">        
        
        <xsl:element name="{@name}">
            <xsl:value-of select="value/text()"/>
        </xsl:element>
                
    </xsl:template>
    
</xsl:stylesheet>