<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns="http://www.lunaimaging.com/xsd"
    xmlns:record="http://www.lunaimaging.com/xsd"
    xmlns:pres="http://preservica.com/custom/saxon-extensions"    
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"/>
    
    <xsl:template match="/record:record">
        <div xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xdt="http://www.w3.org/2004/07/xpath-datatypes"><span class="XSLTransformTitle">Luna Metadata</span><br/>        
            <table class="XSLTransformTable">
                <colgroup>
                    <col width="200px"/>
                    <col width="450px"/>
                </colgroup>
                <xsl:apply-templates select="record:field"/>
                <xsl:apply-templates select="record:entity"/>                    
            </table>
        </div>
    </xsl:template>
    
    <xsl:template match="record:field">
        <xsl:choose>
            <xsl:when test="@name ne 'description'">
                <xsl:variable name="check" select="position()"/>
                <xsl:for-each select="record:value">
                    <tr xmlns:pres="http://saxon.sf.net/">
                        <td class="standardFieldName"><xsl:value-of select="parent::node()/@name"/></td>
                        <!--                        <xsl:variable name="count" select="../../position()"/>-->
                        <xsl:variable name="unique_name" select="concat('editable-d2e_', $check, '_', position())"/>
                        <td id="{$unique_name}">
                            <xsl:element name="input">
                                <xsl:attribute name="type">text</xsl:attribute>
                                <xsl:attribute name="onchange">changeFlag()</xsl:attribute>
                                <xsl:attribute name="name"><xsl:value-of select="concat('/Q{http://fake.co.uk}record[1]/Q{http://fake.co.uk}', @name, '[1]')"/></xsl:attribute>
                                <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
                                <xsl:attribute name="class">standardTextFieldInput</xsl:attribute>
                            </xsl:element>                        
                        </td>
                    </tr>                                    
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <tr xmlns:pres="http://saxon.sf.net/">
                    <td class="standardFieldName"><xsl:value-of select="'Description'"/></td>
                    <xsl:variable name="check" select="position()"/>                                            
                    <xsl:variable name="unique_name" select="concat('editable-d2e_', $check, '_', record:value/position())"/>
                    <td id="{$unique_name}">
                        <xsl:element name="textArea">
                            <xsl:attribute name="rows">4</xsl:attribute>
                            <xsl:attribute name="cols">80</xsl:attribute>
                            <xsl:attribute name="onchange">changeFlag()</xsl:attribute>
                            <xsl:attribute name="name"><xsl:value-of select="concat('/Q{http://fake.co.uk}record[1]/Q{http://fake.co.uk}', @name, '[1]')"/></xsl:attribute>
                            <xsl:attribute name="class">standardTextAreaInput</xsl:attribute>
                            <xsl:value-of select="record:value"/>
                        </xsl:element>                        
                    </td>                                                                                
                </tr>                
            </xsl:otherwise>
        </xsl:choose>                
    </xsl:template>
    
    <xsl:template match="record:field" mode="entity">
        <xsl:param name="id"></xsl:param>
        <xsl:choose>
            <xsl:when test="@name ne 'description'">
                <tr xmlns:pres="http://saxon.sf.net/">
                    <td class="standardFieldName"><xsl:value-of select="@name"/></td>
                    <xsl:variable name="count">
                        <xsl:number/>
                    </xsl:variable>
                    <xsl:variable name="check" select="position()"/>                    
                    <!--                    <xsl:variable name="unique_name" select="concat('editable-d2e_', $check, '_', position())"/>                    -->
                    <xsl:variable name="unique_name" select="concat('editable-d2e', $id, '_', $count)"/>
                    <!--                    <xsl:variable name="unique_name" select="concat('editable-d2e', $id, '_', ../../position())"/>-->
                    <td id="{$unique_name}">
                        <xsl:element name="input">
                            <xsl:attribute name="type">text</xsl:attribute>
                            <xsl:attribute name="onchange">changeFlag()</xsl:attribute>
                            <!-- TODO: Fix this to address multiple values in a field -->
                            <xsl:attribute name="name"><xsl:value-of select="concat('/Q{http://fake.co.uk}record[1]/Q{http://fake.co.uk}', @name, '[1]')"/></xsl:attribute>
                            <xsl:attribute name="value"><xsl:value-of select="record:value"/></xsl:attribute>
                            <xsl:attribute name="class">standardTextFieldInput</xsl:attribute>
                        </xsl:element>                        
                    </td>
                </tr>                
            </xsl:when>
        </xsl:choose>                
    </xsl:template>
    
    
    <xsl:template match="record:entity">        
        <xsl:choose>
            <xsl:when test="exists(@id)">
                <xsl:apply-templates select="record:field" mode="entity">
                    <xsl:with-param name="id" select="concat('_e', @id)"/>            
                </xsl:apply-templates>
                <xsl:apply-templates select="record:entity" mode="recurse">
                    <xsl:with-param name="id" select="concat('_e', @id)"/>            
                </xsl:apply-templates>                
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="record:field" mode="entity">
                    <!--                    <xsl:with-param name="id" select="concat('_', @name)"/>            -->
                    <xsl:with-param name="id" select="concat('_', @name, '_', position())"/>            
                </xsl:apply-templates>
                <xsl:apply-templates select="record:entity" mode="recurse">
                    <xsl:with-param name="id" select="concat('_', @name)"/>            
                </xsl:apply-templates>                                
            </xsl:otherwise>
        </xsl:choose>        
    </xsl:template>
    
    <xsl:template match="record:entity" mode="recurse">
        <xsl:param name="id"/>                        
        <xsl:choose>
            <xsl:when test="exists(@id)">
                <xsl:variable name="ent_id"><xsl:value-of select="concat($id, '_e', @id)"/></xsl:variable>
                <xsl:apply-templates select="record:field" mode="entity">
                    <xsl:with-param name="id" select="$ent_id"/>
                </xsl:apply-templates>
                <xsl:apply-templates select="record:entity" mode="recurse">
                    <xsl:with-param name="id" select="$ent_id"/>            
                </xsl:apply-templates>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="ent_id"><xsl:value-of select="concat($id, '_', @name)"/></xsl:variable>
                <xsl:apply-templates select="record:field" mode="entity">
                    <xsl:with-param name="id" select="$ent_id"/>
                </xsl:apply-templates>
                <xsl:apply-templates select="record:entity" mode="recurse">
                    <xsl:with-param name="id" select="$ent_id"/>            
                </xsl:apply-templates>
            </xsl:otherwise>
        </xsl:choose>                       
    </xsl:template>
    
    
</xsl:stylesheet>

