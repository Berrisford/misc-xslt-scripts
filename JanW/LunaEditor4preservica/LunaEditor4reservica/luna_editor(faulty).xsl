<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns="http://www.lunaimaging.com/xsd"
    xmlns:record="http://www.lunaimaging.com/xsd"
    xmlns:pres="http://preservica.com/custom/saxon-extensions"    
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"/>
    
    <xsl:template match="/record:record">
        <div xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xdt="http://www.w3.org/2004/07/xpath-datatypes"><span class="XSLTransformTitle">Luna Metadata</span><br/>        
            <table class="XSLTransformTable">
                <colgroup>
                    <col width="200px"/>
                    <col width="450px"/>
                </colgroup>
<!--                <xsl:for-each select="child::node()">-->
                    <xsl:apply-templates select="record:field"/>
<!--                    <xsl:apply-templates select="field"/>-->
                    <xsl:apply-templates select="record:entity"/>                    
                <!--</xsl:for-each>-->
            </table>
        </div>
    </xsl:template>
    
    <xsl:template match="record:field">
        <xsl:choose>
            <xsl:when test="@name ne 'description'">
                <tr xmlns:pres="http://saxon.sf.net/">
                    <td class="standardFieldName"><xsl:value-of select="@name"/></td>
                    <xsl:variable name="count">
                        <xsl:number/>
                    </xsl:variable>
                    <xsl:variable name="unique_name" select="concat('editable-d2e', $count)"/>
                    <td id="{$unique_name}">
                        <xsl:element name="input">
                            <xsl:attribute name="type">text</xsl:attribute>
                            <xsl:attribute name="onchange">changeFlag()</xsl:attribute>
                            <!-- TODO: Fix this to address multiple values in a field -->
                            <xsl:attribute name="name"><xsl:value-of select="concat('/Q{http://fake.co.uk}record[1]/Q{http://fake.co.uk}', @name, '[1]')"/></xsl:attribute>
                            <xsl:attribute name="value"><xsl:value-of select="record:value"/></xsl:attribute>
                            <xsl:attribute name="class">standardTextFieldInput</xsl:attribute>
                        </xsl:element>                        
                    </td>
                </tr>                
            </xsl:when>
            <xsl:otherwise>
                <tr xmlns:pres="http://saxon.sf.net/">
                    <td class="standardFieldName"><xsl:value-of select="'Description'"/></td>
                    <xsl:variable name="count">
                        <xsl:number/>
                    </xsl:variable>
                    <xsl:variable name="unique_name" select="concat('editable-d2e', $count)"/>
                    <td id="{$unique_name}">
                        <xsl:element name="textArea">
                            <xsl:attribute name="rows">4</xsl:attribute>
                            <xsl:attribute name="cols">80</xsl:attribute>
                            <xsl:attribute name="onchange">changeFlag()</xsl:attribute>
                            <xsl:attribute name="name"><xsl:value-of select="concat('/Q{http://fake.co.uk}record[1]/Q{http://fake.co.uk}', record:value[1], '[1]')"/></xsl:attribute>
                            <xsl:attribute name="class">standardTextAreaInput</xsl:attribute>
                            <xsl:value-of select="record:value"/>
                        </xsl:element>                        
                    </td>                                                                                
                </tr>                
            </xsl:otherwise>
        </xsl:choose>                
    </xsl:template>


    <xsl:template name="ent_field" match="record:field" mode="ent_field">
        <xsl:param name="ent_id"/>
        <xsl:choose>
            <xsl:when test="@name ne 'description'">
                <tr xmlns:pres="http://saxon.sf.net/">
                    <td class="standardFieldName"><xsl:value-of select="@name"/></td>
                    <xsl:variable name="count">
                        <xsl:number/>
                    </xsl:variable>
                    <xsl:variable name="unique_name" select="concat('editable-d2e_e', $ent_id, '_f', $count)"/>
                    <td id="{$unique_name}">
                        <xsl:element name="input">
                            <xsl:attribute name="type">text</xsl:attribute>
                            <xsl:attribute name="onchange">changeFlag()</xsl:attribute>
                            <!-- TODO: Fix this to address multiple values in a field -->
                            <xsl:attribute name="name"><xsl:value-of select="concat('/Q{http://fake.co.uk}record[1]/Q{http://fake.co.uk}', @name, '[1]')"/></xsl:attribute>
                            <xsl:attribute name="value"><xsl:value-of select="record:value"/></xsl:attribute>
                            <xsl:attribute name="class">standardTextFieldInput</xsl:attribute>
                        </xsl:element>                        
                    </td>
                </tr>                
            </xsl:when>
        </xsl:choose>                
    </xsl:template>

    <xsl:template match="field">
        <xsl:param name="ent_id"/>
        <xsl:choose>
            <xsl:when test="@name ne 'description'">
                <tr xmlns:pres="http://saxon.sf.net/">
                    <td class="standardFieldName"><xsl:value-of select="@name"/></td>
                    <xsl:variable name="count">
                        <xsl:number/>
                    </xsl:variable>
                    <xsl:variable name="unique_name" select="concat('editable-d2e_e', $ent_id, '_f', $count)"/>
                    <td id="{$unique_name}">
                        <xsl:element name="input">
                            <xsl:attribute name="type">text</xsl:attribute>
                            <xsl:attribute name="onchange">changeFlag()</xsl:attribute>
                            <!-- TODO: Fix this to address multiple values in a field -->
                            <xsl:attribute name="name"><xsl:value-of select="concat('/Q{http://fake.co.uk}record[1]/Q{http://fake.co.uk}', @name, '[1]')"/></xsl:attribute>
                            <xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
                            <xsl:attribute name="class">standardTextFieldInput</xsl:attribute>
                        </xsl:element>                        
                    </td>
                </tr>                
            </xsl:when>
        </xsl:choose>                
    </xsl:template>
    

    <xsl:template match="record:entity">        
        <xsl:variable name="ent_id"><xsl:value-of select="@id"/></xsl:variable>
        <xsl:apply-templates select="record:entity"/>
        <xsl:apply-templates select="field">
            <xsl:with-param name="ent_id" select="$ent_id"/>                                    
        </xsl:apply-templates>
        <xsl:call-template name="ent_field">
            <xsl:with-param name="ent_id" select="$ent_id"/>                        
        </xsl:call-template>
<!--        
        <xsl:apply-templates select="record:field" mode="ent_field">
            <xsl:with-param name="ent_count"/>            
        </xsl:apply-templates>
-->        
    </xsl:template>
    

</xsl:stylesheet>

