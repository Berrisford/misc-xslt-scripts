<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns="http://www.lunaimaging.com/xsd"
    xmlns:record="http://www.lunaimaging.com/xsd"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"/>
    
    <xsl:template match="/record:record">
        <div xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xdt="http://www.w3.org/2004/07/xpath-datatypes"><span class="XSLTransformTitle">Luna Metadata</span><br/>        
        <table class="XSLTransformTable">
            <colgroup>
                <col width="200px"/>
                <col width="450px"/>
            </colgroup>
            <tr>
                <td class="mainSectionHeader" colspan="2">record, Namespace: http://www.lunaimaging.com/xsd</td>
            </tr>
            <xsl:apply-templates select="record:field"/>
            <xsl:apply-templates select="record:entity"/>
        </table>
        </div>
    </xsl:template>
    
    <xsl:template match="record:field">
<!--        <xsl:variable name="fieldName" select="@name"></xsl:variable>-->
        <tr>
        <xsl:element name="td">
            <xsl:attribute name="class" select="'standardFieldName'"/>
            <xsl:value-of select="@name"/>
        </xsl:element>        
        <xsl:element name="td">
            <xsl:attribute name="class" select="'standardFieldValue'"/>
            <xsl:value-of select="record:value/text()"/>
        </xsl:element>        
        </tr>
    </xsl:template>

    <xsl:template match="record:entity">        
<!--        <xsl:element name="{@name}">-->
<!--            <xsl:apply-templates select ="@*"/> -->
            <xsl:apply-templates select="record:entity"/>
            <xsl:apply-templates select="record:field"/>            
        <!--</xsl:element>-->        
    </xsl:template>
<!--
    <xsl:template match="@*" >
        <xsl:attribute name="{name()}" >
            <xsl:value-of select ="."/> 
        </xsl:attribute>
    </xsl:template> 
-->
</xsl:stylesheet>