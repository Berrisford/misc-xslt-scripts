<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns="http://www.lunaimaging.com/xsd"
    xmlns:record="http://www.lunaimaging.com/xsd"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"/>
    
    <xsl:template match="/record:record">
        <record>
            <xsl:apply-templates select="record:field"/>
            <xsl:apply-templates select="record:entity"/>
        </record>
    </xsl:template>
    
    <xsl:template match="record:field">                
        <xsl:element name="{@name}">
            <xsl:value-of select="record:value/text()"/>
        </xsl:element>        
    </xsl:template>

    <xsl:template match="record:entity">        
        <xsl:element name="{@name}">
<!--            <xsl:apply-templates select ="@*"/> -->
            <xsl:apply-templates select="record:field"/>            
        </xsl:element>        
    </xsl:template>
<!--
    <xsl:template match="@*" >
        <xsl:attribute name="{name()}" >
            <xsl:value-of select ="."/> 
        </xsl:attribute>
    </xsl:template> 
-->
</xsl:stylesheet>