<?xml version="1.0"?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="no"/>	
	<!--<xsl:variable name="baseuri" select="'http://localhost:8085/solr/nonscw/select/'"/>-->
	 <xsl:variable name="baseuri" select="'http://escholaruat.library.manchester.ac.uk:8085/solr/nonscw/select/'"/>  
	<xsl:template match="/">
	<!-- 	<etdwindows> -->
			<xsl:apply-templates select="response/result/doc"/>
		<!--	</etdwindows> -->
	</xsl:template>
	<xsl:template match="doc">
		<etdwindow>
			<pid><xsl:value-of select="str[@name='PID']"/></pid>
			<firstname><xsl:value-of select="str[@name='e.forename']"/></firstname>  
			<familyname><xsl:value-of select="str[@name='e.familyname']"/></familyname>  
			<middlenames><xsl:value-of select="str[@name='e.middlenames']"/></middlenames> 
			<partynumber><xsl:value-of select="str[@name='e.partynumber']"/></partynumber>
			<qualification><xsl:value-of select="str[@name='e.qualification']"/></qualification>
			<programmename><xsl:value-of select="str[@name='e.programmename']"/></programmename>
			<windowtype><xsl:value-of select="str[@name='r.isofetdtype.displayname']"/></windowtype>
			<windowstatus>
				<xsl:if test="str[@name='e.windowstate']='PENDING'"><xsl:value-of select="'Pending'"/></xsl:if>
				<xsl:if test="str[@name='e.windowstate']='OPENED'"><xsl:value-of select="'Awaiting submission'"/></xsl:if>
				<xsl:if test="str[@name='e.windowstate']='EXPIRED'"><xsl:value-of select="'Expired'"/></xsl:if>
				<xsl:if test="str[@name='e.windowstate']='CANCELLED'"><xsl:value-of select="'Cancelled'"/></xsl:if>
				<xsl:if test="str[@name='e.submissionstate']='SUBMITTED'"><xsl:value-of select="'Deposited'"/></xsl:if>
				<xsl:if test="str[@name='e.submissionstate']='ACKNOWLEDGED'"><xsl:value-of select="'Acknowledged'"/></xsl:if>
				<xsl:if test="str[@name='e.submissionstate']='REJECTED'"><xsl:value-of select="'Rejected'"/></xsl:if>
				<xsl:if test="str[@name='e.submissionstate']='SENT_TO_LIBRARY'"><xsl:value-of select="'Sent to library'"/></xsl:if>
				<xsl:if test="str[@name='e.submissionstate']='LIBRARY_PROCESSED'"><xsl:value-of select="'Library processed'"/></xsl:if>
			</windowstatus>
			<studentpid><xsl:value-of select="str[@name='r.isbelongstostudent.pid']"/></studentpid>
			
			<xsl:variable name="partynumber" select="str[@name='e.partynumber']/text()"/>
			<xsl:variable name="query" select="concat('p.partynumber:',$partynumber)"/>
			<xsl:variable name="src" select="concat($baseuri,'?q=',$query,'&amp;version=2.2&amp;start=0&amp;rows=1&amp;indent=off&amp;fl=p.email,p.title,p.authoritytype,p.authorityprefforename,p.authorityfamilyname,p.authorityemail,p.authoritypartynumber')"/>
			
			<email><xsl:apply-templates select="doc($src)/response/result/doc/str[@name='p.email']"/></email>
			<title><xsl:apply-templates select="doc($src)/response/result/doc/str[@name='p.title']"/></title>
			
			<xsl:variable name="index">
				<xsl:apply-templates select="doc($src)/response/result/doc/arr[@name='p.authoritytype']"/>
			</xsl:variable>
			
			<authorityfirstname><xsl:value-of select="doc($src)/response/result/doc/arr[@name='p.authorityprefforename']/str[number($index)]"/></authorityfirstname>
			<authorityfamilyname><xsl:value-of select="doc($src)/response/result/doc/arr[@name='p.authorityfamilyname']/str[number($index)]"/></authorityfamilyname>
			<authorityemail><xsl:value-of select="doc($src)/response/result/doc/arr[@name='p.authorityemail']/str[number($index)]"/></authorityemail>
			<authoritypartynumber><xsl:value-of select="doc($src)/response/result/doc/arr[@name='p.authoritypartynumber']/str[number($index)]"/></authoritypartynumber>
			
			<xsl:variable name="partynumberS" select="doc($src)/response/result/doc/arr[@name='p.authoritypartynumber']/str[number($index)]"/>
			<xsl:variable name="queryS" select="concat('p.partynumber:',$partynumberS)"/>
			<xsl:variable name="srcS" select="concat($baseuri,'?q=',$queryS,'&amp;version=2.2&amp;start=0&amp;rows=1&amp;indent=off&amp;fl=PID')"/>
			<authoritypid><xsl:apply-templates select="doc($srcS)/response/result/doc/str[@name='PID']"/></authoritypid>
				
			<!-- Not needed -->
			<!-- <windowType><xsl:value-of select="str[@name='r.isofetdtype.pid']"/></windowType>
			<programmeName><xsl:value-of select="str[@name='e.programmename']"/></programmeName>
			<planName><xsl:value-of select="str[@name='e.planname']"/></planName>
			<programmeId><xsl:value-of select="str[@name='e.programmeid']"/></programmeId>
			<planId><xsl:value-of select="str[@name='e.planid']"/></planId>
			<expectedOpenDate><xsl:value-of select="str[@name='e.expectedopendate']"/></expectedOpenDate>
			<expectedCloseDate><xsl:value-of select="str[@name='e.expectedclosedate']"/></expectedCloseDate>   
	 		-->
		</etdwindow>
	</xsl:template>
	
	<xsl:template match="arr[@name='p.authoritytype']">
		<xsl:for-each select="str">
			<xsl:if test="text()='Main Supervisor' or text()='PGR/PGT Main Supervisor'">
				<xsl:value-of select="number(position())"/>
			</xsl:if>			
		</xsl:for-each>
	</xsl:template>
	
</xsl:stylesheet>
