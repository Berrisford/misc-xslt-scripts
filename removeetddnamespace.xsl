<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:etdd="http://www.escholar.manchester.ac.uk/esc/etdd/v1#"
    exclude-result-prefixes="xs xd xsi etdd"
    version="2.0">
    <xsl:output indent="yes" encoding="UTF-8" method="xml" omit-xml-declaration="no" media-type="application/xml;charset=UTF-8" />
    <xsl:strip-space elements="*"/>

    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>
    
    <xsl:template match="etdd:etdDeclaration">
        <etdDeclaration>
            <xsl:apply-templates select="*" />
        </etdDeclaration>
    </xsl:template>
 
    <xsl:template match="*">
        <xsl:element name="{replace(name(),'etdd:','')}">
            <xsl:apply-templates select="@*" />
            <xsl:apply-templates select="*|text()" />
        </xsl:element>
    </xsl:template>
 
    <xsl:template match="@*">
        <xsl:attribute name="{name()}">
            <xsl:value-of select="." />
        </xsl:attribute>
    </xsl:template>
 
    <xsl:template match="text()">
        <xsl:value-of select="." />
    </xsl:template>
 
</xsl:stylesheet>