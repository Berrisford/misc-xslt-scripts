<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    exclude-result-prefixes="xs"
    version="3.0">
    <xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="no"/>

    <xsl:variable name="values">
        <xsl:call-template name="get_values"/>
    </xsl:variable>

    <xsl:template match="/">        
        <distinct-values>
        <xsl:for-each select="distinct-values($values/values/m1)">
            <m1>
                <xsl:value-of select="substring(., 1, 2)"></xsl:value-of>
            </m1>                        
        </xsl:for-each>
        </distinct-values>
    </xsl:template>
    
    <xsl:template name="get_values">
        <values>
            <xsl:for-each select="distinct-values(/marc:collection/marc:record/marc:datafield[@tag='082']/marc:subfield[@code='a']/text())">
                <m1>
                    <xsl:value-of select="substring(., 1, 2)"></xsl:value-of>
                </m1>                        
            </xsl:for-each>
        </values>
    </xsl:template>    

    <xsl:template match="marc:collection1">
        <menu>
            <xsl:for-each select="marc:record/marc:datafield[@tag='082']/marc:subfield[@code='a']">
            <xsl:variable name="su" select="."></xsl:variable>
                <m1>
                    <xsl:value-of select="distinct-values(substring($su,1,2))"></xsl:value-of>
                </m1>
            </xsl:for-each>
        </menu>
        
        
        <xsl:for-each select="marc:record">
        <ebook>
            <subjects>
                   <xsl:for-each select="marc:datafield[@tag='082']/marc:subfield[@code='a']">
                        <subject>
                            <xsl:variable name="subj" select="."></xsl:variable>
                            <class>
                                <xsl:value-of select="substring($subj,1,1)"></xsl:value-of>
                            </class>
                            <subclass>
                                <xsl:value-of select="substring($subj,2,1)"></xsl:value-of>   
                            </subclass>
                        </subject>
                   </xsl:for-each>
               
            </subjects>
            <title><xsl:apply-templates select="marc:datafield[@tag='245']/marc:subfield[@code='a']"></xsl:apply-templates></title>
            <isbn><xsl:apply-templates select="marc:datafield[@tag='776']/marc:subfield[@code='z']"></xsl:apply-templates></isbn>
        </ebook>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>