<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    exclude-result-prefixes="xs"
    version="3.0">
    <xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="no"/>

    <xsl:template match="/">
        <xsl:apply-templates select="marc:collection"/>
    </xsl:template>
    
    <xsl:template match="marc:collection">
        <xsl:for-each select="marc:record/marc:datafield[@tag='082']/marc:subfield[@code='a']">
            <xsl:call-template name="subfield">
                <xsl:with-param name="processed">
                    <xsl:value-of select="substring(., 1, 2)"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="subfield">
        <xsl:param name="processed"/>
        <subfield><xsl:value-of select="concat('processed: ', $processed)"></xsl:value-of></subfield>
        <xsl:choose>
            <xsl:when test="not(contains($processed, substring(., 1, 2)))">
<!--                
                <xsl:call-template name="subfield">
                    <xsl:with-param name="processed">
                        <xsl:value-of select="concat($processed, '; ', substring(., 1, 2))"/>
                    </xsl:with-param>
                </xsl:call-template>
-->                
                <m1>
                    <xsl:value-of select="substring(., 1, 2)"/>
                </m1>                
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="marc:collection1">
        <menu>
            <xsl:for-each select="marc:record/marc:datafield[@tag='082']/marc:subfield[@code='a']">
            <xsl:variable name="su" select="."></xsl:variable>
                <m1>
                    <xsl:value-of select="distinct-values(substring($su,1,2))"></xsl:value-of>
                </m1>
            </xsl:for-each>
        </menu>
        
        
        <xsl:for-each select="marc:record">
        <ebook>
            <subjects>
                   <xsl:for-each select="marc:datafield[@tag='082']/marc:subfield[@code='a']">
                        <subject>
                            <xsl:variable name="subj" select="."></xsl:variable>
                            <class>
                                <xsl:value-of select="substring($subj,1,1)"></xsl:value-of>
                            </class>
                            <subclass>
                                <xsl:value-of select="substring($subj,2,1)"></xsl:value-of>   
                            </subclass>
                        </subject>
                   </xsl:for-each>
               
            </subjects>
            <title><xsl:apply-templates select="marc:datafield[@tag='245']/marc:subfield[@code='a']"></xsl:apply-templates></title>
            <isbn><xsl:apply-templates select="marc:datafield[@tag='776']/marc:subfield[@code='z']"></xsl:apply-templates></isbn>
        </ebook>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>