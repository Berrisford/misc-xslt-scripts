<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:commons="v3.commons.pure.atira.dk"
    xmlns:v1="v1.impact.pure.atira.dk" 
    xmlns:v3="v3.commons.pure.atira.dk"
    exclude-result-prefixes="xs math commons v1 v3"
    version="3.0"
    >
    
    
    <xsl:output method="xml" indent="yes" encoding="UTF-8" />
    
    <xsl:template match="/">
        <xsl:result-document href="output-liveDataV3.xml">
            <v1:impacts xmlns="v1.impact.pure.atira.dk v3.commons.pure.atira.dk" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="v1.impact.pure.atira.dk file:/C:/Git/misc-xslt-scripts/ImpactTransform/Impact%20data%20to%20BE%20-%20v1.0/Impact%20data%20to%20BE/impact.xsd">                 
            <xsl:apply-templates select="/impacts/impact"/>                
        </v1:impacts>        
        </xsl:result-document>
    </xsl:template>
   
   <xsl:template match="impact">
       <xsl:element name="v1:impact">
            <xsl:attribute name="id" select="concat('id_0', id)"/>
            <xsl:attribute name="type" select="'impact'"/>
            <xsl:attribute name="managedInPure" select="'false'"/>
            
           <v1:title>
               <xsl:choose>
                   <xsl:when test="string-length(title) &lt; 251">
                       <xsl:value-of select="title"/>                       
                   </xsl:when>
                   <xsl:otherwise>
                       <xsl:value-of select="concat(substring(title, 0, 250), '...')"/>                                              
                   </xsl:otherwise>
               </xsl:choose>
           </v1:title>

        <v1:additionalDescriptions>
            <v1:description type="whoisaffected"><xsl:value-of select="who_is_affected"/></v1:description>
            <v1:description type="narrative"><xsl:value-of select="narrative"/></v1:description>
            <v1:description type="impactdescription" lang="en"><xsl:value-of select="impact_description"/></v1:description>
        </v1:additionalDescriptions>
                   
           <v1:startDate>
               <v3:year><xsl:value-of select="start_year"/></v3:year>
           </v1:startDate>
                      
           <v1:impactStatus><xsl:value-of select="lower-case(impact_status)"/></v1:impactStatus>
           
           <v1:impactCategories>
               <xsl:apply-templates select="impact_category"/>
           </v1:impactCategories>
                      
           <v1:impactLevel>benefit</v1:impactLevel>
            
           <xsl:if test="string-length(author1_id) &gt; 0">
               <v1:persons>
                   <xsl:apply-templates select="author1_id | author2_id | author3_id | author4_id"/>
               </v1:persons>
           </xsl:if> 
                                  
           <v1:impactEvidence>
               <v1:impactEvidence>
                   <xsl:attribute name="id">
                       <xsl:value-of select="concat(id, '_evidence', position())" />
                   </xsl:attribute>
                   <v1:evidenceIndicator>qualitative</v1:evidenceIndicator>

                   <v1:startDate>
                       <v3:year><xsl:value-of select="start_year"/></v3:year>
                   </v1:startDate>
                   <xsl:if test="string-length(link_urls) &gt; 0">
                       <v1:links>
                           <xsl:apply-templates select="link_urls">
                               <xsl:with-param name="id" select="id"/>
                           </xsl:apply-templates>
                       </v1:links>                                          
                   </xsl:if>
               </v1:impactEvidence>
           </v1:impactEvidence>
           
           <v1:managedBy>
               <xsl:attribute name="lookupId">
                   <xsl:value-of select="tokenize(lookup_id, '\|')[1]"/>
               </xsl:attribute>
           </v1:managedBy>
                      
           <v1:visibility>Confidential</v1:visibility>
        </xsl:element>
      
   </xsl:template>
   
   <xsl:template match="impact_category">       
       <xsl:if test="string-length(.) &gt; 0">
           <xsl:for-each select="tokenize(., '\|')">
               <xsl:if test="string-length(replace(replace(normalize-space(.), '\?', ''), ' ', '_')) &gt; 0">
                    <v1:impactCategory><xsl:value-of select="lower-case(replace(replace(normalize-space(.), '\?', ''), ' ', '_'))"/></v1:impactCategory>
               </xsl:if>
           </xsl:for-each>           
       </xsl:if>       
   </xsl:template>
   
   <xsl:template match="author1_id">
       <xsl:if test="string-length(.) &gt; 0">
           <xsl:element name="v1:associatedPerson">
               <xsl:attribute name="id" select="."/>
               <xsl:element name="v1:person">
                   <xsl:attribute name="lookupId" select="."/>
               </xsl:element>                  
               <v1:personRole>participant</v1:personRole>
           </xsl:element>                      
       </xsl:if>             
   </xsl:template>

    <xsl:template match="author2_id">
        <xsl:if test="string-length(.) &gt; 0">
            <xsl:element name="v1:associatedPerson">
                <xsl:attribute name="id" select="."/>
                <xsl:element name="v1:person">
                    <xsl:attribute name="lookupId" select="."/>
                </xsl:element>                  
                <v1:personRole>participant</v1:personRole>
            </xsl:element>                      
        </xsl:if>             
    </xsl:template>
    
    <xsl:template match="author3_id">
        <xsl:if test="string-length(.) &gt; 0">
            <xsl:element name="v1:associatedPerson">
                <xsl:attribute name="id" select="."/>
                <xsl:element name="v1:person">
                    <xsl:attribute name="lookupId" select="."/>
                </xsl:element>                  
                <v1:personRole>participant</v1:personRole>
            </xsl:element>                      
        </xsl:if>             
    </xsl:template>
    
    <xsl:template match="author4_id">
        <xsl:if test="string-length(.) &gt; 0">
            <xsl:element name="v1:associatedPerson">
                <xsl:attribute name="id" select="."/>
                <xsl:element name="v1:person">
                    <xsl:attribute name="lookupId" select="."/>
                </xsl:element>                  
                <v1:personRole>participant</v1:personRole>
            </xsl:element>                      
        </xsl:if>             
    </xsl:template>
    
    <xsl:template match="link_urls">
        <xsl:param name="id"/>
        <xsl:if test="string-length(.) &gt; 0">
            <xsl:for-each select="tokenize(., 'http')">
                <xsl:if test="string-length(.) &gt; 0">
                    <v3:link>
                        <xsl:attribute name="id">
                            <xsl:value-of select="concat($id, '_link', position())" />
                        </xsl:attribute>
                        <v3:url><xsl:value-of select="concat('http', .)"/></v3:url>
                    </v3:link>                    
                </xsl:if>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>