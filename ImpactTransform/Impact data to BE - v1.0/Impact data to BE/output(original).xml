<?xml version="1.0" encoding="UTF-8"?>
<v1:impacts xmlns:v1="v1.impact.pure.atira.dk">
   <v1:impact id="i1" type="impact" managedInPure="false">
      <v1:title xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">Holiferm – Winterburn Group Spin Out</v1:title>
      <v1:additionalDescriptions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <v1:description type="whoisaffected">Holiferm, Evonik</v1:description>
         <v1:description type="narrative">Surfactants are a key ingredient used in the manufacture of detergents and personal care products, but current mainstream goods primarily use petrochemical and tropical oil-based surfactants, which cause harm to the environment. While eco-friendly ingredients in this sector do exist, they use costly and slow batch fermentation processes, making these alternatives prohibitively expensive for mass adoption.
Dr James Winterburn and PhD student Ben Dolman identified sophorolipids, a type of glycolipid biosurfactant,  produced by fermentation of a naturally occurring yeast found in honey as one such viable alternative. These sophorolipid biosurfactants offer comparable performance to petrochemical equivalents but with a significantly reduced environmental impact. They developed a novel semi-continuous biosurfactant synthesis and collection process that utilises gravity to separate out the biosurfactant during production. As well as using less energy than existing methods, this more than doubled production rates to around five grams per litre per hour.
The pair secured an array of collaborative network funding grants to develop industrial partnerships and determine if the process could be replicated at larger scales. This included funding from various Networks in Industrial Biotechnology and Bioenergy (NIBB), namely a  BioProNET Business interaction voucher and FoodWasteNet proof-of-concept funding.
In May 2018, James and Ben formed spinout company Holiferm to manufacture eco-friendly biosurfactants for use in home and personal care products, as well as industrial cleaners and other application areas. Their long-term vision is to develop a range of sustainable, green biosurfactants that will remove the global market's dependency on petrochemicals.
Holiferm’s “HoneySurf” sophorolipids are produced from the scaled up integrated gravity separation and fermentation process first performed in their lab. Holiferm’s novel semi-continuous process for biosurfactant synthesis and collection enables production at a competitive price point compared to petrochemical alternatives. Holiferm's technologies, initially developed at The University of Manchester, allow for the production of clean and natural cleaning ingredients at a scale that will make them widely available to all consumers.
Holiferm secured their first strategic investment in 2019: £1 million in equity funding from the ICOS Venture Capital Investment Fund. This was followed in 2021 with &gt;£7m investment from Rhapsody Venture Partners, the Clean Growth fund and ICOS Capital.
In early 2022 Holiferm leased a large commercial premises to house their new commercial demonstration plant capable of producing 1,100 tonnes of biosurfactants a year for global supply. Their initial focus is on sophorolipid production for existing customers and distributors. Whilst expanding their customer base Holiferm is also planning new product launches and is working in collaboration with BASF on new, fermentation derived, biosurfactant molecule development.</v1:description>
         <v1:description type="impactdescription" lang="en">Challenge
Surfactants are a key ingredient in the manufacture of detergents and personal care products, but current mainstream goods primarily use petrochemical and tropical oil-based surfactants, which cause harm to the environment. [2]
Research
UoM research identified a range of novel sophorolipid derived from a naturally occurring yeast found in honey. To avoid biosurfactant accumulation clogging up yeast fermentation process, research also developed a novel semi-continuous biosurfactant synthesis and collection process that utilised gravity to separate out the biosurfactant as it was produced. As well as using less energy than existing methods, this doubled production rates to around five grams per litre per hour in a two litre bioreactor. [6]
The reduced production cost offered by the semi-continuous process is the biggest asset of the work. It allows for the production of a range of biosurfactant to be produced at a comparable or cheaper cost than their petrochemical equivalents.
The biosurfactants produced are rarely like-for-like duplicates of the petrochemical surfactants they replace. The aim of the manufactured biosurfactants is to offer ‘comparable’ performance and cost and would ideally be “drop in replacements”, requiring very little change to an end products production process or formula.
There is an additional downstream process for purification which is performed without organic solvents- this las a likely environmental benefit compared to alternatives and a potential cost benefit (savings on purchase and disposal of organic solvents) but Holiferm are currently exploring this. This was developed in-house at Holiferm post spin out (not our direct impact).
Pathway
Work on IP and Licencing was performed with UMIF/UMIP
The pair secured an array of collaborative network funding grants to develop industrial partnerships and determine if the process could be replicated at larger scales. This included Networks in Industrial Biotechnology and Bioenergy (NIBB) funding, the academic-industry networking club BioProNET’s Business interaction voucher, and FoodWasteNet proof-of-concept funding.
Early industrial scale adoption looked at Licencing the IP of the semi-continuous process to industry (Evonik were an early partner) but the preferred model moving forward is to keep the technology in-house and manufacture chemicals on behalf of clients.
The NIBB helped PhD student Ben Dolman and his supervisor Dr James Winterburn develop contacts that proved crucial to bringing their innovative low-cost production process for biosurfactants to market.
A BBSRC Early Career Innovator award in 2018 further increased Ben and James’s profile and credibility.
In May 2018, James and Ben formed spinout company Holiferm. “The survival of Holiferm depended upon our pitches to investors,” James emphasises. “The fact we’d already collaborated widely and built a network of contacts made our business case much more compelling.”
Holiferm entered an entrepreneurship competition run by Dutch chemicals giant Nouryon. It led to Holiferm securing their first strategic investment in 2019: £1 million in equity funding from the ICOS Venture Capital Investment Fund.
The company’s business development was helped further by working with start-up accelerator Deep Science Ventures and Innovate UK’s ICURe commercialisation support programme.
In April 2019, Holiferm had a rapidly growing workforce, with 14 full-time equivalent jobs (and rising). The company has a research and development lab at Manchester Science Park and a 600 litre, 20 to 25 tonnes a year pilot plant on the brink of being commissioned at SciTech Daresbury. [6]
In 2021 The Clean Growth Fund invested UK£1.8m in Holiferm alongside US-based Rhapsody Venture Partners and Netherlands-based ICOS Capital to complete a UK£6.9m round.[3]
The firm said the $7m investment round, led by US VC firm Rhapsody Ventures, demonstrates "industry confidence" in Holiferm - and will enable it to build a world leading biosurfactant manufacturing facility, as well as support its entry into US chemical markets. [2]
Holiferm’s presence at Sci-Tech Daresbury was supported by a £400,000 grant from the Inward Investment Facilitation Fund set up as part of the £75m Business Growth Package introduced by Steve Rotheram, Metro Mayor of the Liverpool City Region. [2]
Through the strategic technology agreement with Holiferm, a UK-based start-up company, BASF establishes an exclusive cooperation to focus on developing and manufacturing sustainable, non-fossil based, fermentation-derived ingredients for other classes of glycolipids with potential for application in Home Care, Industrial Formulators and Personal Care products. [5]
Combining Holiferm’s unique production technology and process know-how, and BASF’s leading position and knowledge in these industries will enable innovative solutions to be brought to customers.[5]
BASF, through their joint development scheme will gain some access to the process IP. But the partnership has in broad terms outsourced the R&amp;D of some novel biosurfactants that BASF identified to Holiferm to produce in suitable quantity for testing. Should BASF look to adopt these biosurfactant for a product line then Holiferm would have the responsibility for scale-up and mass production on BASF’s behalf.
Impact
At the start of 2021, the firm launched its new pilot plant at Sci-Tech Daresbury, and its headcount has increased from seven to 16 and it is anticipated the investment programme overall will create over 30 new jobs. [2]
In early 2022 Holiferm began a 10-year lease for the warehouse at Ocean Trade and Logistics Park, generating a rental income of £375,000 per year. [1]
The new commercial plant will produce 1,100 tonnes of biosurfactants a year for global supply, focusing initially on sophorolipid production for Holiferm’s existing customers and distributors, such as MixCleanGreen, Starbrands, Azelis and Eurosyn. Holiferm is also set to launch new biosurfactant products and is working in collaboration with BASF on the next two molecules in its pipeline. [4]

[1] https://www.placenorthwest.co.uk/news/biosurfactant-company-signs-for-90000-sq-ft-wallasey-industrial/
[2] https://www.business-live.co.uk/economic-development/holiferm-build-new-plant-create-21911191 
[3] https://www.cleangrowthfund.com/clean-growth-fund-invests-in-holiferm-to-decarbonise-a-wide-range-of-personal-and-home-care-products/ 
[4] https://holiferm.com/news/holiferm-to-bring-eco-friendly-biosurfactants-to-the-mass-market-following-7m-investment-boost/ 
[5] https://www.basf.com/hk/en/media/news-releases/global/2021/03/p-21-148.html 
[6] https://www.ukri.org/about-us/research-outcomes-and-impact/bbsrc/networks-help-spin-out-take-breakthrough-eco-product-to-market/     ====Permissions=====    Permission from the Holiferm board required, this would be simple and quick to obtain. Board meetings are weekly and JBW is involved in the running of Holiferm.   ====USAGE====    research newsletter?    ====NEXT STEPS====    </v1:description>
      </v1:additionalDescriptions>
      <v1:startDate xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <v3:year xmlns:v3="v3.commons.pure.atira.dk">2017</v3:year>
      </v1:startDate>
      <v1:impactStatus xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">inpreparation</v1:impactStatus>
      <v1:impactCategories xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <v1:impactCategory>health_impacts</v1:impactCategory>
      </v1:impactCategories>
      <v1:impactLevel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">benefit</v1:impactLevel>
      <v1:persons xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <v1:associatedPerson id="7181864  |7454635 ">
            <v1:person lookupId="7181864  |7454635 "/>
            <v1:personRole>participant</v1:personRole>
         </v1:associatedPerson>
      </v1:persons>
      <v1:impactEvidence xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <v1:impactEvidence id="i1_evidence01">
            <v1:evidenceIndicator>qualitative</v1:evidenceIndicator>
            <v1:startDate>
               <v3:year xmlns:v3="v3.commons.pure.atira.dk">2017</v3:year>
            </v1:startDate>
            <v1:links>
               <v3:link xmlns:v3="v3.commons.pure.atira.dk" id="i1_link01">
                  <v3:url>https://www.placenorthwest.co.uk/news/biosurfactant-company-signs-for-90000-sq-ft-wallasey-industrial/ |  https://www.business-live.co.uk/economic-development/holiferm-build-new-plant-create-21911191 
| https://www.cleangrowthfund.com/clean-growth-fund-invests-in-holiferm-to-decarbonise-a-wide-range-of-personal-and-home-care-products/ 
|  https://holiferm.com/news/holiferm-to-bring-eco-friendly-biosurfactants-to-the-mass-market-following-7m-investment-boost/ 
| https://www.basf.com/hk/en/media/news-releases/global/2021/03/p-21-148.html 
| https://www.ukri.org/about-us/research-outcomes-and-impact/bbsrc/networks-help-spin-out-take-breakthrough-eco-product-to-market/ </v3:url>
               </v3:link>
            </v1:links>
         </v1:impactEvidence>
      </v1:impactEvidence>
      <v1:managedBy xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lookupId="I3021 "/>
      <v1:visibility xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">Confidential</v1:visibility>
   </v1:impact>
   <v1:impact id="i2" type="impact" managedInPure="false">
      <v1:title xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">Development of heat vulnerability index maps (HVI) to inform city resilience planning to cope with extreme heatwaves</v1:title>
      <v1:additionalDescriptions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <v1:description type="whoisaffected">0</v1:description>
         <v1:description type="narrative">Climate change is causing more regular and extreme heatwaves. High temperatures cause increased mortality and increased heat related illness especially among society’s most vulnerable individuals. Many measures can be taken to reduce these impacts and increase resilience to heatwaves. However, to make these responses more efficient, specific and targeted, information on where in the city the individuals most vulnerable to heat reside is vital. Spatial vulnerability assessment’s or heat vulnerability indexes (HVI) can help do this. These include combining multiple maps of vulnerability modifiers and assigning a vulnerability score to different regions. 

A novel methodology has been developed to improve the process of building HVI’s, increasing the volume and value of input data collected and developing simpler and consistent approaches to construction. During 2021, this methodology was adopted by Bristol City council. Co-development through regular interaction with multiple local authority departments and industry practitioners has allowed the tool to be refined to better meet the City’s specific needs.

The tool provided over 200 HVI maps which are now referred to regularly by council staff in the development of their resilience framework and its three key strategic goals; improve heatwave response to protect vulnerable people, adapt housing for future climates, and improve blue green infrastructure to cool the urban environment. Working with the civil protection unit and the local resilience forum a local ‘summer’ extreme weather plan is being developed which will protect the City’s most vulnerable individuals. This will be one of the first of its kind in the UK. The city design and planning team are also using the tool as a key part of their baseline reports for any major city development, using the information provided by the tool to ensure new developments don’t increase an areas vulnerability to heat. The methods and results are due to be shared with the Core Cities climate resilience working group, providing the 10 biggest cities in the UK with the information required to evaluate and improve their own heat resilience frameworks.</v1:description>
         <v1:description type="impactdescription" lang="en">one year post-doc placement with Bristol council building on Charlotte Brown's PhD (both with UoM as only affiliated institition). 
The methods and results are due to be shared with the Core Cities climate resilience working group, providing the 10 biggest cities in the UK with the information required to evaluate and improve their own heat resilience frameworks.

programme has been shared with MERI who may want to create a blog post about her for their comms material.

details shared with Terri Lucas (Civic engagement manager) to discover if GMCA would be interested in adopting Charlottes existing work on mapping Manchetser- this was her PhD project but was not performmed collaboratively so was not shared with GMCA previously. It may be that an individual bourough follows this up instead.    ====Permissions=====       ====USAGE====    Mad awards 2022
NERC impact summary 2022    ====NEXT STEPS====    </v1:description>
      </v1:additionalDescriptions>
      <v1:startDate xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <v3:year xmlns:v3="v3.commons.pure.atira.dk">2021</v3:year>
      </v1:startDate>
      <v1:impactStatus xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">inpreparation</v1:impactStatus>
      <v1:impactCategories xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <v1:impactCategory>health_impacts</v1:impactCategory>
      </v1:impactCategories>
      <v1:impactLevel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">benefit</v1:impactLevel>
      <v1:persons xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <v1:associatedPerson id="2136079 |10098829 ">
            <v1:person lookupId="2136079 |10098829 "/>
            <v1:personRole>participant</v1:personRole>
         </v1:associatedPerson>
      </v1:persons>
      <v1:impactEvidence xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <v1:impactEvidence id="i2_evidence01">
            <v1:evidenceIndicator>qualitative</v1:evidenceIndicator>
            <v1:startDate>
               <v3:year xmlns:v3="v3.commons.pure.atira.dk">2021</v3:year>
            </v1:startDate>
         </v1:impactEvidence>
      </v1:impactEvidence>
      <v1:managedBy xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lookupId="I4613 "/>
      <v1:visibility xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">Confidential</v1:visibility>
   </v1:impact>
</v1:impacts>
