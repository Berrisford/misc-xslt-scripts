<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:commons="v3.commons.pure.atira.dk"
    xmlns:v1="v1.impact.pure.atira.dk" 
    xmlns:v3="v3.commons.pure.atira.dk"
    exclude-result-prefixes="xs math v1 v3 commons"
    
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    version="3.0"
    >
    
    <!-- xsi:schemaLocation="v1.impact.pure.atira.dk v3.commons.pure.atira.dk impact.xsd"-->
    
    <xsl:output method="xml" indent="yes" encoding="UTF-8" />
    
    <xsl:template match="/">
        <xsl:result-document href="output.xml">
            <xsl:element name="v1:impacts">
                <xsl:copy-of select="document('')/*/@xsi:schemaLocation"/>                
                <xsl:apply-templates select="/impacts/impact"/>
            </xsl:element>
        </xsl:result-document>
    </xsl:template>
   
   <xsl:template match="impact">
       <xsl:element name="v1:impact">
            <xsl:attribute name="id" select="id"/>
            <xsl:attribute name="type" select="'impact'"/>
            <xsl:attribute name="managedInPure" select="'false'"/>
            
           <v1:title><xsl:value-of select="title"/></v1:title><!-- impact_information/title -->
           
           <!-- impact_information/who is affected -->
           <!--
            <v1:description><xsl:value-of select="policy_impact_types"/></v1:description>
           -->
           
            <v1:additionalDescriptions>
                <v1:description type="whoisaffected">
                    <xsl:value-of select="who_is_affected"/></v1:description><!-- impact_information/whoisaffected -->
                <v1:description type="narrative"><xsl:value-of select="narrative"/><!--<xsl:value-of select="' '"/><xsl:value-of select="achievments"/>--></v1:description><!-- impact_information/narrative -->
                <v1:description type="impactdescription" lang="en"><xsl:value-of select="description_of_impact"/>
                    <!--
                    <xsl:if test ="doi">
                        <xsl:value-of select="' DOI: '"/><xsl:value-of select="doi"/>
                    </xsl:if>
                    -->
                </v1:description><!-- impact_information/Description of impact -->
           </v1:additionalDescriptions>
         
          
           <v1:startDate>
               <v3:year><xsl:value-of select="period_start"/></v3:year>
               <!--
                <v3:month>9</v3:month>
               <v3:day>11</v3:day>
               -->
           </v1:startDate>
           
           <v1:impactStatus>inpreparation</v1:impactStatus>
           
           <v1:impactCategories>
               <v1:impactCategory>health_impacts</v1:impactCategory>
           </v1:impactCategories>
           
           <v1:impactLevel>benefit</v1:impactLevel>
            
            <v1:persons>
               <xsl:element name="v1:associatedPerson">
                   <xsl:attribute name="id" select="spotid"/>
                   <xsl:element name="v1:person">
                       <xsl:attribute name="lookupId" select="spotid"/>
                   </xsl:element>                  
                   <v1:personRole>participant</v1:personRole>
               </xsl:element>
               
            </v1:persons>
           
           <v1:impactEvidence>
               <!--<v1:impactEvidence name="id" select="(concat(id, '_evidence01')" >-->
               <v1:impactEvidence>
                   <xsl:attribute name="id">
                       <xsl:value-of select="concat(id, '_evidence01')" />
                   </xsl:attribute>
                   <v1:evidenceIndicator>qualitative</v1:evidenceIndicator>
                   <!--
                   <xsl:choose>
                       <xsl:when test="string-length(publication_cited)> 256">
                           <v1:evidenceTitle><xsl:value-of select="concat (substring(publication_cited, 0, 250), '...')"/></v1:evidenceTitle>
                       </xsl:when>
                       <xsl:otherwise> -->
                   <!--
                   <v1:evidenceTitle>
                       <xsl:if test ="ISRCTN-ID">
                           <xsl:value-of select="ISRCTN-ID"/><xsl:value-of select="'; '"/>
                       </xsl:if>
                       <xsl:if test ="UKCRN-ID">
                           <xsl:value-of select="UKCRN-ID"/><xsl:value-of select="'; '"/>
                       </xsl:if>
                       <xsl:if test ="ClinicalTrials-gov-ID">
                           <xsl:value-of select="ClinicalTrials-gov-ID"/><xsl:value-of select="'; '"/>
                       </xsl:if>
                       <xsl:if test ="EudraCTID">
                           <xsl:value-of select="EudraCTID"/><xsl:value-of select="'; '"/>
                       </xsl:if>
                      
                   </v1:evidenceTitle>
                    -->       
                      <!-- </xsl:otherwise>
                   </xsl:choose> -->
                   <!--
                   <v1:summary>
                       <xsl:if test ="ISRCTN-ID">
                           <xsl:value-of select="ISRCTN-ID"/><xsl:value-of select="'; '"/>
                       </xsl:if>
                       <xsl:if test ="UKCRN-ID">
                           <xsl:value-of select="UKCRN-ID"/><xsl:value-of select="'; '"/>
                       </xsl:if>
                       <xsl:if test ="ClinicalTrials-gov-ID">
                           <xsl:value-of select="ClinicalTrials-gov-ID"/><xsl:value-of select="'; '"/>
                       </xsl:if>
                       <xsl:if test ="EudraCTID">
                           <xsl:value-of select="EudraCTID"/><xsl:value-of select="'; '"/>
                       </xsl:if>
                   </v1:summary>
                   -->
                   <v1:startDate>
                       <v3:year><xsl:value-of select="period_start"/></v3:year>
                   </v1:startDate>
                   <xsl:if test="url">
                       <v1:links>
                           <v3:link>
                               <xsl:attribute name="id">
                                   <xsl:value-of select="concat(id, '_link01')" />
                               </xsl:attribute>
                               <v3:url><xsl:value-of select="url"/></v3:url>
                               <!--
                           <v3:description>
                               <v3:text lang="en">the description</v3:text>
                               <v3:text lang="de">das beschreibung</v3:text>
                           </v3:description>
                           <v3:type>unspecified</v3:type>
                           -->
                           </v3:link>
                       </v1:links>
                   </xsl:if>
                   
               </v1:impactEvidence>
           </v1:impactEvidence>
           
           <v1:managedBy>
               <xsl:attribute name="lookupId">
                   <xsl:value-of select="orgid"/>
               </xsl:attribute></v1:managedBy>
           
           <!--
           <v1:keywords>
               <v1:keyword logicalName="Freetext keywords"><xsl:value-of select="funder"/></v1:keyword>
               <v1:keyword logicalName="Freetext keywords"><xsl:value-of select="geographic_reach"/></v1:keyword>
           </v1:keywords>
           -->
           
           
           <v1:visibility>Confidential</v1:visibility>
                 
            <!--
                <v1:personAndPeriode>
                   <xsl:element name="v1:person">
                       <xsl:attribute name="id" select="'person_id'"/>
                   </xsl:element>
                   <v1:role>other</v1:role>
                   <v1:roleDescription>
                        <commons:text><xsl:value-of select="description"/></commons:text>
                   </v1:roleDescription>
                    <v1:startDate>
                       <xsl:choose>
                           <xsl:when test="year ne ''">
                               <commons:year><xsl:value-of select="year"/></commons:year>                               
                           </xsl:when>
                           <xsl:otherwise>
                               <commons:year><xsl:value-of select="'2099'"/></commons:year>                                                              
                           </xsl:otherwise>
                       </xsl:choose>
                   </v1:startDate>  
               </v1:personAndPeriode>
               <v1:organisationAssociations>
                   <xsl:element name="v1:organisationAssociation">
                       <xsl:attribute name="id" select="organisation_id"/>
                   </xsl:element>                   
               </v1:organisationAssociations>
               <v1:visibility>Confidential</v1:visibility>
               <v1:externalRelation>
                   <v1:title>
                       <commons:text country="GB" lang="en"><xsl:value-of select="activity_title"/></commons:text>
                   </v1:title>
                   <v1:startDate><xsl:value-of select="concat('01-01-',event_start_date)"/></v1:startDate>
                   
               </v1:externalRelation>
              -->
              
        </xsl:element>
      
   </xsl:template>
   
</xsl:stylesheet>