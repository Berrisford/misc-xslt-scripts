<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:commons="v3.commons.pure.atira.dk"
    xmlns:v1="v1.impact.pure.atira.dk" 
    xmlns:v3="v3.commons.pure.atira.dk"
    exclude-result-prefixes="xs commons v1 v3"
    version="3.0">
    <xsl:output method="xml" indent="yes" encoding="UTF-8" />
<!--
    <xsl:template match="/">
        <categories>
<!-\-            <xsl:apply-templates select="distinct-values(//v1:impactCategory)"/>-\->
            <xsl:apply-templates select="distinct-values(//category)"/>
        </categories>
    </xsl:template>
    -->
    <xsl:template match="/">        
        <distinct-categories>
            <xsl:for-each select="distinct-values(//category)">
                <category>
                    <xsl:value-of select="."></xsl:value-of>
                </category>                        
            </xsl:for-each>
        </distinct-categories>
    </xsl:template>    

<!--
    <xsl:template match="v1:impactCategory">
        <category>
            <xsl:value-of select="."/>
        </category>
    </xsl:template>
-->    
</xsl:stylesheet>