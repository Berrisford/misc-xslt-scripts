# README #

## Overview ##
This documents the research impact project.  It transforms research impact data from an Excel spreadsheet to an xml format (defined by the impact schema - impact.xsd) that can then be bulk imported into Pure.

## Transformation Instructions  

1. Create intermediate mapping file:  Excel doesn't support mapping of complex nodes or mapping of attributes so a flat intermediate file MUST be created that maps from Excel, then the result of the XML output from Excel is then transformed to the final XML format defined by the "impact.xsd" schema.  This only needs to be done once.  Guidelines for intermediate mapping file:  
	1. All nodes MUST be direct descendants of the root node  
	2. Convert ALL attributes to nodes  
	3. Create a sample xml document of the flattened structure  
	4. Using oXygen, generate a schema (.xsd) from the flattened xml document  
	5. In oXygen, use the schema editor to edit the schema to set the types, occurrences etc... to what they should be  
2. With spreadsheet:  
	1. Add identity column with these instructions: https://stackoverflow.com/questions/10128153/formula-for-creating-identity-column-in-excel  
	2. Add "start_year" column as integer using formula "=YEAR(<cell>)"   
	3. Replace spaces in column names with "_"  
	4. Sort useful columns in the order in which they appear in the xml mapping file with these instructions: https://www.youtube.com/watch?v=tQSapXiW5mM  
	5. In the "Developer" tab, import the schema (.xsd) and map the column titles to the relevant nodes  
	6. Export the xml (https://www.youtube.com/watch?v=5nb78TdXM8k)  
3. Transform the output from the Excel spreadsheet using transformation xslt ("convert-dataV2.xsl")  
4. In Pure: [https://puretest.ppad.man.ac.uk/](https://puretest.ppad.man.ac.uk/)    
	1. Upload the final xml into Pure using "Bulk upload" thus:  
		1. Bulk Import > Impact  
		2. Click "Open import wizard"  
	2. Test the items using "View uploaded impacts" thus:  
		1. Click "Editor"  
		2. Click "Impacts"  
		3. Search for required ID  
		

## Contact: ##
[Berrisford.Edwards@manchester.ac.uk](mailto:Berrisford.Edwards@manchester.ac.uk) OR  
Nilani Ganeshwaran (DLDT Senior Developer)  [Nilani.Ganeshwaran@manchester.ac.uk](mailto:Nilani.Ganeshwaran@manchester.ac.uk)   