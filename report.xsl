<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output indent="yes"/>
    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
        <results>
            <xsl:apply-templates select="response/result/doc" />
        </results>
    </xsl:template>
    
    <xsl:template match="doc">
        <!-- http://fedprdir.library.manchester.ac.uk:8085/solr/etd/select/?q=PID%3Auk-ac-man-etd%5C%3A5084&version=2.2&start=0&rows=10&indent=on&fl=e.submissionstate,%20e.programmename,%20e.planname -->
        <xsl:variable name="window-pid" select="replace(arr[@name='r.isbelongstoetdwindow.pid']/str/text(), ':', '?')" />
        <xsl:variable name="etd-url">
            <xsl:value-of select="escape-html-uri(concat('http://fedprdir.library.manchester.ac.uk:8085/solr/etd/select/?q=PID:', $window-pid, '&amp;version=2.2&amp;start=0&amp;rows=10&amp;indent=on&amp;fl=e.submissionstate, e.programmename, e.planname, e.partynumber'))"></xsl:value-of>
        </xsl:variable>
        <doc>
            <PID>
                <xsl:value-of select="str[@name='PID']/text()" />
            </PID>
            <Embargo-reason>
                <xsl:value-of select="str[@name='d.accessrestriction.reason']/text()" />
            </Embargo-reason>
            <Degree>
                <xsl:value-of select="str[@name='m.note.degreelevel']/text()" />
            </Degree>
            <Title>
                <xsl:value-of select="str[@name='m.title']/text()" />
            </Title>
            <Window-pid>
                <xsl:value-of select="arr[@name='r.isbelongstoetdwindow.pid']/str/text()" />
            </Window-pid>
            <Created-date>
                <xsl:value-of select="substring-before(date[@name='x.createddate']/text(), 'T')" />  
            </Created-date>
            <Embargo-period>
                <xsl:value-of select="str[@name='d.accessrestriction.duration']/text()" />
            </Embargo-period>
            <ETD_window_status>
                <xsl:value-of select="doc($etd-url)/response/result/doc/str[@name='e.submissionstate']/text()" />
            </ETD_window_status>
            <Student>
                <xsl:value-of select="arr[@name='m.name.aut']/str/text()" />
            </Student>
            <Student-ID>
                <xsl:value-of select="doc($etd-url)/response/result/doc/str[@name='e.partynumber']/text()" />
            </Student-ID>
            <Programme>
                <!--<xsl:value-of select="str[@name='m.note.degreeprogramme']/text()" />-->
                <xsl:value-of select="doc($etd-url)/response/result/doc/str[@name='e.programmename']/text()" />
            </Programme>
            <Plan>
                <xsl:value-of select="doc($etd-url)/response/result/doc/str[@name='e.planname']/text()" />                
            </Plan>
            <xsl:apply-templates select="arr[@name='m.name.fnd']/str" />
        </doc>
    </xsl:template>
    
    <xsl:template match="arr[@name='m.name.fnd']/str">
        <xsl:variable name="count">
            <xsl:number/>
        </xsl:variable>
        <xsl:variable name="name">
            <xsl:value-of select="concat('Funder', $count)" />
        </xsl:variable>
        <xsl:element name="{$name}">
            <xsl:value-of select="text()" />
        </xsl:element>        
    </xsl:template>
    
</xsl:stylesheet>