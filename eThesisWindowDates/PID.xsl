<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"     xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0">
    <xsl:template match="/">
        <pids>
            <xsl:apply-templates select="response/result/doc"/>
        </pids>    
    </xsl:template>
    <xsl:template match="doc">
        <pid>
            <xsl:value-of select="str[@name='PID']"/>
        </pid>
    </xsl:template>
</xsl:stylesheet>
