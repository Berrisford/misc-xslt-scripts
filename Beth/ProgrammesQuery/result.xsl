<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output indent="yes" method="xml" encoding="UTF-8"/>
    
    <xsl:template match="/">
        <xsl:result-document href="flat_result.xml">
            <items>
                <xsl:apply-templates select="result/doc"/>
            </items>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="doc">
        <item>
            <date_submitted><xsl:value-of select="substring-before(date, 'T')"/></date_submitted>
            <name><xsl:value-of select="tokenize(arr/str, '\|\|')[4]"/></name>
            <spot_id><xsl:value-of select="tokenize(arr/str, '\|\|')[3]"/></spot_id>
            <programme_name><xsl:value-of select="str[@name='m.note.degreeprogramme']"/></programme_name>
            <open_access><xsl:value-of select="str[@name='d.permissiontodownload']"/></open_access>
            <restriction_duration><xsl:value-of select="str[@name='d.accessrestriction.duration']"/></restriction_duration>            
        </item>            
    </xsl:template>
    
</xsl:stylesheet>