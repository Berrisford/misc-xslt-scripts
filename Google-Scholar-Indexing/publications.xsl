<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">    
    <xsl:output indent="yes" method="xml" omit-xml-declaration="yes" />
    
    <!-- BA: This script iterates across all records (of a particular content type) in chunks of 1000 (rows) and checks to see if the record -->
    <!--  has been matched by Scopus. If not, it generates the requisite Pure xml entirely from eScolar data. Change $rows, $num_found, $obj_files -->
    <!--  accordingly to test quickly with small sets of data. Change $file_path to match where your files are located. -->
    
    <!--<xsl:variable name="root_url" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/scw/select/'"></xsl:variable>-->
    <xsl:variable name="root_url" select="'http://fedprdir.library.manchester.ac.uk:8086/solr/publicscw/select/'"></xsl:variable>    
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con%5C%3A17+AND+r.isbelongsto.pid:uk*', '&amp;fl=PID,r.isbelongsto.pid,x.state&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?17', '&amp;fl=PID,r.isbelongsto.pid,x.state&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?17', '&amp;fl=PID&amp;omitHeader=true&amp;start=0&amp;rows=0')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+m.preceding.source:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+r.hasauthor.source:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+m.host.title.abbreviated:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+m.genre.status:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+m.title.translated:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+m.note.authors:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=PID:', 'uk-ac-man-scw%5C%3A71283Berris', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=PID%3Auk-ac-man-scw%5C%3A202423&amp;fl=PID,r.isbelongsto.pid,x.state&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3Auk-ac-man-con%5C%3A17+AND+%28r.isbelongsto.pid%3Auk-ac-man-per%5C%3A109461+OR+r.isbelongsto.pid%3Auk-ac-man-per%5C%3A48079%29&amp;fl=PID,r.isbelongsto.pid,x.state&amp;omitHeader=true&amp;start=0')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+f.fileattached.source:%5B*+TO+*%5D+AND+PID%3Auk-ac-man-scw%5C%3A10*', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=PID:', 'uk-ac-man-scw%5C%3A267239', '&amp;fl=PID,r.isbelongsto.pid,x.state&amp;omitHeader=true')"></xsl:variable>-->
    <xsl:variable name="url" select="concat($root_url, '?q=m.abstract:', '%5B*+TO+*%5D+AND+x.state:Active+AND+r.isofcontenttype.pid%3A', 'uk-ac-man-con%5C%3A1', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>
    
    <xsl:variable name="rows" select="1000"/>
    <xsl:variable name="num_found">
        <xsl:value-of select="doc(concat($url, '&amp;start=0&amp;rows=0'))/response/result/@numFound"/>
        <!--<xsl:value-of select="doc(concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?17', '&amp;fl=PID&amp;omitHeader=true&amp;start=0&amp;rows=0'))/response/result/@numFound"/>-->
        <!--<xsl:value-of select="10"/>-->
    </xsl:variable>
        
    <xsl:template match="/">
        <xsl:result-document href="journal-articles-with-abstract/publications.html">
            <html>
                <head>
                    <title><xsl:value-of select="concat('Google crawl file (journal articles with abstract) - ', string(current-dateTime()))"/></title>
                </head>
                <body>
                    <xsl:call-template name="recurse">
                        <xsl:with-param name="start" select="0"/>
                        <xsl:with-param name="end" select="$rows"/>
                    </xsl:call-template>                
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>
        
    <xsl:template name="recurse">
        <xsl:param name="start"/>
        <xsl:param name="end"/>
        <xsl:variable name="obj_response">
            <xsl:copy-of select="doc(concat($url, '&amp;start=', $start, '&amp;rows=', $rows))"/>
        </xsl:variable>       
        
        <xsl:for-each select="$obj_response/response/result/doc">
            <xsl:element name="a">
                <xsl:attribute name="href" select="concat('https://www.escholar.manchester.ac.uk/jrul/item/?pid=', str[@name='PID'])"/>
                <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/jrul/item/?pid=', str[@name='PID'])"/>
            </xsl:element>
            <br />            
        </xsl:for-each>
        
        <xsl:if test="$end &lt; $num_found">
            <xsl:call-template name="recurse">
                <xsl:with-param name="start" select="$end + 1"/>
                <xsl:with-param name="end" select="$end + $rows"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
                   
    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>
    
</xsl:stylesheet>
