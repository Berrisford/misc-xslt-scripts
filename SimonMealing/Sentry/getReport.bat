echo Start time: %DATE% %TIME%
rmdir /s /q reports
TIMEOUT /T 5
mkdir reports
wget "https://api-na.hosted.exlibrisgroup.com/almaws/v1/analytics/reports?apikey=l7xxf1d48c7f698f477c94c5d59b32c1ede8&path=/shared/University of Manchester/Reports/Sentry Export/Sentry User Export - All_Count&limit=1000" --no-check-certificate -O reports/users.xml
wget "https://api-na.hosted.exlibrisgroup.com/almaws/v1/analytics/reports?apikey=l7xxf1d48c7f698f477c94c5d59b32c1ede8&path=/shared/University of Manchester/Reports/Sentry Export/Sentry User Export - All&limit=1000" --no-check-certificate -O reports/report1.xml
echo generating report calls...
java -jar "saxon\saxon9pe.jar" -s:dummy.xml -xsl:generateReportCalls.xsl -o:reportCalls.bat
TIMEOUT /T 10
echo executing report calls...
reportCalls.bat