<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:urn="urn:schemas-microsoft-com:xml-analysis:rowset"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="text" indent="yes"/>
    
    <xsl:variable name="users-count">
        <xsl:value-of select="doc('reports/users.xml')/report/QueryResult/ResultXml/urn:rowset/urn:Row[1]/urn:Column8"/>
    </xsl:variable>
    
    <xsl:variable name="file-count">
        <!--<xsl:value-of select="floor(number($users-count div 1000))"/>-->
        <xsl:value-of select="ceiling(number($users-count div 1000))"/>
        <!--<xsl:value-of select="0"/>-->        
    </xsl:variable>
    
    
    <xsl:template match="/">
        <!--<xsl:result-document href="testcsv.csv">-->        
        <xsl:call-template name="recurse-report-files">
            <xsl:with-param name="count" select="0"/>
        </xsl:call-template>
        <!--</xsl:result-document>-->
    </xsl:template>
    
    <xsl:template name="recurse-report-files">
        <xsl:param name="count" as="xs:integer"/>
        <xsl:variable name="report-name">
            <xsl:value-of select="concat('reports/report', $count + 1, '.xml')"/>
        </xsl:variable>
        <xsl:variable name="rows-doc">
            <xsl:copy-of select="doc($report-name)/report/QueryResult/ResultXml/urn:rowset"/>
            <!--<xsl:copy-of select="doc('report215.xml')/report/QueryResult/ResultXml/rowset"/>-->
        </xsl:variable>
        
        <xsl:for-each select="$rows-doc/urn:rowset/urn:Row">
            <xsl:value-of select="concat(urn:Column5, ',')"/>
            <xsl:value-of select="concat(urn:Column4, ',')"/>
            <xsl:value-of select="concat(urn:Column6, ',')"/>
            <!--<xsl:value-of select="concat(urn:Column2, ',')"/>-->
            <xsl:value-of select="concat(tokenize(urn:Column2, '-')[3], '/', tokenize(urn:Column2, '-')[2], '/', tokenize(urn:Column2, '-')[1], ',')"/>
            <xsl:value-of select="concat(urn:Column3, ',')"/>
            <xsl:value-of select="concat(urn:Column5, ',')"/>
            <xsl:value-of select="urn:Column1"/>
            <xsl:text>&#xD;&#xA;</xsl:text>
        </xsl:for-each>
        
        <xsl:choose>
            <xsl:when test="$count &lt; $file-count">
                <!-- process next report file -->
                <xsl:call-template name="recurse-report-files">
                    <xsl:with-param name="count" select="$count + 1"/>
                </xsl:call-template>                
            </xsl:when>
        </xsl:choose>
        
    </xsl:template>
    
</xsl:stylesheet>