<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="text" indent="yes"/>

    <xsl:variable name="users-base-url">
        <xsl:value-of select="'https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users'"/>
    </xsl:variable>
    
    <xsl:variable name="users-api-key">
        <xsl:value-of select="'l7xx9b3ddcd32d8a419181fec710b3e53ee3'"/>    
    </xsl:variable>
    

    <xsl:variable name="report-base-url">
        <xsl:value-of select="'https://api-na.hosted.exlibrisgroup.com/almaws/v1/analytics/reports'"/>
    </xsl:variable>

    <xsl:variable name="report-api-key">
        <xsl:value-of select="'l7xxebeb02758adb428d82126420223533a2'"/>
    </xsl:variable>
   
    <xsl:template match="/">

        <xsl:result-document href="getReport.bat">
            <xsl:text>echo Start time: %DATE% %TIME%</xsl:text>
            <xsl:text>&#xD;&#xA;</xsl:text>
            
            <!-- BA: first get users report with count - start -->
            <xsl:value-of select="concat('wget &quot;', $users-base-url, '?limit=1&amp;offset=0&amp;apikey=', $users-api-key, '&quot; --no-check-certificate -O users.xml')"/>            
            <xsl:text>&#xD;&#xA;</xsl:text>
            <!-- BA: first get users report with count - end -->
            
            <!-- BA: get first report with resumption token - start -->
            <xsl:value-of select="concat('wget &quot;', $report-base-url, '?apikey=', $report-api-key, '&amp;path=/shared/University of Manchester/Reports/Sentry Export/Export_ALMA_Users_BE&amp;limit=1000&quot;', ' --no-check-certificate -O report1.xml')"/>
            <!-- BA: get first report with resumption token - end -->
            <xsl:text>&#xD;&#xA;</xsl:text>
            <xsl:text>java -jar "C:\saxon\saxon9pe.jar" -s:dummy.xml -xsl:generateReportCalls.xsl -o:reportCalls.bat</xsl:text>
            <xsl:text>&#xD;&#xA;</xsl:text>
            <xsl:text>echo End time: %DATE% %TIME%</xsl:text>            
            <xsl:text>&#xD;&#xA;</xsl:text>
            <xsl:value-of select="'pause'"/>
        </xsl:result-document>
        
    </xsl:template>
   
</xsl:stylesheet>