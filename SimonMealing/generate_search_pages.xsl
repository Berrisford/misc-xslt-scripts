<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">

    <!-- BA: edit to match last page required -->
    <xsl:variable name="end_page" select="393"/>
    
    <xsl:template match="/">        
        <root>
            <!-- BA: edit to match first page required -->
            <xsl:variable name="start_page" select="238"/>
            <xsl:call-template name="iterate">
                <xsl:with-param name="count" select="$start_page"></xsl:with-param>
            </xsl:call-template>
        </root>
    </xsl:template>
    
    <xsl:template name="iterate">
        <xsl:param name="count"/>
        <xsl:variable name="file">
            <xsl:value-of select="concat('soap-search-page', $count, '.xml')" />
        </xsl:variable>

        <!-- BA: edit <Token> and <ReportToken> to match current tokens -->
        <xsl:result-document href="{$file}" method="xml" indent="yes">
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                xmlns:ser="http://serialssolutions.com/">
                <soapenv:Header/>
                <soapenv:Body>
                    <ser:DataExchange>
                        <ser:data><![CDATA[<ApiCallData>
               <Version>1</Version>
               <Api>RequestPagedData</Api>
               <Token>b96f9db2-c475-4de9-8b6a-346b85750105</Token>
               <RequestPagedData>
                  <ReportToken>9521d611-d664-402d-9327-eeebd5c025c4</ReportToken>
                  <PageNumber>]]><xsl:value-of select="$count"/><![CDATA[</PageNumber>
                  <Version>1</Version>
               </RequestPagedData>
            </ApiCallData>]]></ser:data>
                    </ser:DataExchange>
                </soapenv:Body>
            </soapenv:Envelope>            
        </xsl:result-document>            
        
        <xsl:if test="$count &lt; $end_page">
            <xsl:call-template name="iterate">
                <xsl:with-param name="count" select="$count + 1"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
            
</xsl:stylesheet>