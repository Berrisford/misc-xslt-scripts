<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <!--https://analytics-eu.alma.exlibrisgroup.com/analytics/saw.dll?Answers&path=%2Fshared%2FUniversity%20of%20Manchester%2FReports%2FSentry%20Export%2FExport_ALMA_Users_BE-->
    
    <!--https://api-na.hosted.exlibrisgroup.com/almaws/v1/analytics/reports?apikey=l7xxebeb02758adb428d82126420223533a2&path=%2Fshared%2FUniversity%20of%20Manchester%2FReports%2FSentry%20Export%2FExport_ALMA_Users_BE-->
    
    
    <xsl:variable name="base-url">
        <xsl:value-of select="'https://api-na.hosted.exlibrisgroup.com/almaws/v1/analytics/reports'"/>
    </xsl:variable>
    
    <xsl:variable name="api-key">
        <xsl:value-of select="'l7xxebeb02758adb428d82126420223533a2'"/>    
    </xsl:variable>
    
    <xsl:variable name="path">
        <xsl:value-of select="'%2Fshared%2FUniversity%20of%20Manchester%2FReports%2FSentry%20Export%2FExport_ALMA_Users_BE'"/>
    </xsl:variable> 
    
    <xsl:variable name="report-start">
        <xsl:copy-of select="doc(concat($base-url, '?apikey=', $api-key, '&amp;path=', $path))"/>
    </xsl:variable>

    <xsl:variable name="token">
        <xsl:value-of select="$report-start/report/QueryResult/ResumptionToken"/>
    </xsl:variable>

    <xsl:template match="/">        
        <!--<xsl:result-document href="users-ids.xml">-->
        <output>
            <token><xsl:value-of select="$token"/></token>
            <xsl:choose>
                <xsl:when test="doc-available($report-start)">
                    <available>avail...</available>
                    <xsl:apply-templates select="$report-start/report/QueryResult/ResultXml/rowset/Row"/>
                </xsl:when>
                <xsl:otherwise>
                    <not-available>not avail!!!</not-available>
                </xsl:otherwise>
            </xsl:choose>
        </output>
<!--        
        <rows>
            <xsl:call-template name="recurse-rows">
                <xsl:with-param name="is-finished" select="$report-start/report/QueryResult/IsFinished"/>
            </xsl:call-template>
        </rows>
-->        
        <!--</xsl:result-document>-->
    </xsl:template>
    
    
    <xsl:template name="recurse-rows">
        <xsl:param name="is-finished" as="xs:string"/>
        <xsl:variable name="report-url">
            <xsl:value-of select="concat($base-url, '?apikey=', $api-key, '&amp;token=', $token, '&amp;limit=1000')"/>
        </xsl:variable>
<!--        
        <xsl:variable name="report">
            <xsl:copy-of select="doc($report-url)/report"/>
        </xsl:variable>
-->        
<!--        
        <xsl:variable name="new-is-finished">
            
        </xsl:variable>
-->        

<!--
        <xsl:choose>
            <xsl:when test="$is-finished = 'false'">
                <!-\- call report again -\->
                <xsl:call-template name="recurse-rows">
                    <xsl:with-param name="is-finished" select=""/>
                </xsl:call-template>                
            </xsl:when>
        </xsl:choose>
        -->
    </xsl:template>

    <xsl:template match="Row">
        <row>row...</row>
        <xsl:copy-of select="."/>
    </xsl:template>

</xsl:stylesheet>