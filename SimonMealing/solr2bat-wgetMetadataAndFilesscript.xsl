<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	exclude-result-prefixes="xs"
	version="2.0">
	<xsl:output encoding="UTF-8" method="text" indent="no"/>
	<xsl:strip-space elements="*"/>
	<xsl:template match="/">
		<xsl:text>echo Start time: %DATE% %TIME%</xsl:text>
		<xsl:text>&#10;&#13;</xsl:text>
		<xsl:apply-templates select="response/result/doc"/>
		<xsl:text>echo End time: %DATE% %TIME%</xsl:text>
		<xsl:text>&#10;&#13;</xsl:text>
		<xsl:text>pause</xsl:text>
	</xsl:template>
	<xsl:template match="doc">
		<xsl:apply-templates>
			<xsl:with-param name="pid" select="str[@name='PID']"/>
			<xsl:with-param name="dir" select="replace(str[@name='PID'],':','_')"/>
		</xsl:apply-templates>
		<xsl:apply-templates select="arr/str">
			<xsl:with-param name="pid" select="str[@name='PID']"/>
			<xsl:with-param name="dir" select="replace(str[@name='PID'],':','_')"/>
		</xsl:apply-templates>
	</xsl:template>
	<!--fl=PID, f.full-text.source, f.pre-peer-review.source, f.post-peer-review-non-publishers.source, f.post-peer-review-publishers.source, f.supplementary-1.source, f.supplementary-2.source, f.supplementary-3.source, f.supplementary-4.source, f.supplementary-5.source, f.supplementary-6.source, f.supplementary-7.source, f.supplementary-8.source, f.supplementary-9.source, f.supplementary-10.source-->
	<xsl:template match="str[@name='PID']">
		<xsl:param name="pid"/>
		<xsl:param name="dir"/>
		<xsl:value-of select="concat('mkdir ',$dir)"/>
		<xsl:text>&#10;&#13;</xsl:text>
		<xsl:value-of select="concat('wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=',$dir,'\FOXML.xml &quot;http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/',.,'/objectXML&quot;')"/>
		<xsl:text>&#10;&#13;</xsl:text>
		<xsl:value-of select="concat('wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=',$dir,'\RELS-EXT.xml &quot;http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/',.,'/datastreams/RELS-EXT/content&quot;')"/>
		<xsl:text>&#10;&#13;</xsl:text>
		<xsl:value-of select="concat('wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=',$dir,'\EMAIL.xml &quot;http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/',.,'/datastreams/EMAIL/content&quot;')"/>
		<xsl:text>&#10;&#13;</xsl:text>
		<xsl:value-of select="concat('wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=',$dir,'\PRESERVATION.xml &quot;http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/',.,'/datastreams/PRESERVATION/content&quot;')"/>
		<xsl:text>&#10;&#13;</xsl:text>
		<xsl:value-of select="concat('wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=',$dir,'\EVENTLOG.xml &quot;http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/',.,'/datastreams/EVENTLOG/content&quot;')"/>
		<xsl:text>&#10;&#13;</xsl:text>
		<xsl:value-of select="concat('wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=',$dir,'\MHT.mht &quot;http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/',.,'/datastreams/MHT/content&quot;')"/>
		<xsl:text>&#10;&#13;</xsl:text>
		<xsl:value-of select="concat('wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=',$dir,'\EML.eml &quot;http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/',.,'/datastreams/EML/content&quot;')"/>
		<xsl:text>&#10;&#13;</xsl:text>
		<xsl:value-of select="concat('wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=',$dir,'\MSG.msg &quot;http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/',.,'/datastreams/MSG/content&quot;')"/>
		<xsl:text>&#10;&#13;</xsl:text>
		<xsl:value-of select="concat('wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=',$dir,'\XML.xml &quot;http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/',.,'/datastreams/XML/content&quot;')"/>
		<xsl:text>&#10;&#13;</xsl:text>
		<xsl:value-of select="concat('wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=',$dir,'\DC.xml &quot;http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/',.,'/datastreams/DC/content&quot;')"/>
		<xsl:text>&#10;&#13;</xsl:text>		
	</xsl:template>
	
	<xsl:template match="arr[@name='f.fileattached.source']/str">
		<!--<xsl:template match="arr/str">-->
		<xsl:param name="pid"/>
		<xsl:param name="dir"/>
		<!--<xsl:variable name="dsid" select="tokenize(.,'\|\|')[2]"/>-->
		<xsl:variable name="dsid" select="tokenize(.,'\|\|')[1]"/>
		<xsl:variable name="dsname" select="tokenize(.,'\|\|')[2]"/>
		<!--<xsl:variable name="dsname" select="replace($dsname1, '\.', '_')"/>-->
		<!--<xsl:value-of select="concat('wget -\-http-user=fedoraAdmin -\-http-password=bluebanana -\-limit-rate=250k -\-output-document=',$dir,'\', $dsid,' &quot;http://escholarprd.library.manchester.ac.uk:8080/fedora/get/',$pid,'/', $dsid,'&quot;')"/>-->
		<xsl:value-of select="concat('wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=250k --output-document=&quot;',$dir,'\', $dsname,'&quot; &quot;http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/',$pid,'/datastreams/', $dsid,'/content&quot;')"/>
		<xsl:text>&#10;&#13;</xsl:text>
	</xsl:template>
	<xsl:template match="*|@*"/>
</xsl:stylesheet>