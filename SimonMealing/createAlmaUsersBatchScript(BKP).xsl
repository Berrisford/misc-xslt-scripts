<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="text" indent="yes"/>

    <!--https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users?limit=1&offset=0&apikey=l7xx9b3ddcd32d8a419181fec710b3e53ee3-->
    
    <!--https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users?limit=100&offset=0&order_by=last_name, first_name, primary_id&apikey=l7xx9b3ddcd32d8a419181fec710b3e53ee3-->
    
    <!--https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/9256313?user_id_type=all_unique&view=full&expand=none&apikey=l7xx9b3ddcd32d8a419181fec710b3e53ee3-->
    
    <!--https://api-na.hosted.exlibrisgroup.com/almaws/v1/analytics/reports?apikey=l7xxebeb02758adb428d82126420223533a2&path=%2Fshared%2FUniversity%20of%20Manchester%2FReports%2FSentry%20Export%2FExport_ALMA_Users_BE-->
    <!--https://api-na.hosted.exlibrisgroup.com/almaws/v1/analytics/reports?apikey=l7xxebeb02758adb428d82126420223533a2&path=%2Fshared%2FUniversity%20of%20Manchester%2FReports%2FSentry%20Export%2FExport_ALMA_Users_BE-->
    <xsl:variable name="users-base-url">
        <xsl:value-of select="'https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users'"/>
    </xsl:variable>
    
    <xsl:variable name="users-api-key">
        <xsl:value-of select="'l7xx9b3ddcd32d8a419181fec710b3e53ee3'"/>    
    </xsl:variable>
    
    <xsl:variable name="count-url">
        <xsl:value-of select="concat($users-base-url, '?limit=1&amp;offset=0&amp;apikey=', $users-api-key)"/>
    </xsl:variable>
    
    <xsl:variable name="num-records">
        <xsl:value-of select="doc($count-url)/users/@total_record_count"/>
        <!--<xsl:value-of select="3"/>-->
    </xsl:variable>

    <xsl:variable name="iterations">
        <xsl:value-of select="ceiling(number($num-records div 1000))"/>
    </xsl:variable>

    <xsl:variable name="ids-start" as="xs:integer">
        <xsl:value-of select="0"/>
    </xsl:variable>

    <xsl:variable name="report-base-url">
        <xsl:value-of select="'https://api-na.hosted.exlibrisgroup.com/almaws/v1/analytics/reports'"/>
    </xsl:variable>

    <xsl:variable name="report-api-key">
        <xsl:value-of select="'l7xxebeb02758adb428d82126420223533a2'"/>
    </xsl:variable>

    <!--https://api-na.hosted.exlibrisgroup.com/almaws/v1/analytics/reports?apikey=l7xxebeb02758adb428d82126420223533a2&path=%2Fshared%2FUniversity%20of%20Manchester%2FReports%2FSentry%20Export%2FExport_ALMA_Users_BE-->
    
    <!-- BA: get first report with resumtion token - start -->
<!--
    <xsl:variable name="first-report">
        <xsl:copy-of select="doc(concat($report-base-url, '?apikey=', $api-key, '&amp;path=/shared/University of Manchester/Reports/Sentry Export/Export_ALMA_Users_BE'))/report"/>
    </xsl:variable>    
-->
    <!-- BA: get first report with resumtion token - end -->
    
    
    <xsl:template match="/">
        <check>
            <num-records><xsl:value-of select="$num-records"/></num-records>
            <iterations><xsl:value-of select="$iterations"/></iterations>
        </check>
        
        <!--<xsl:call-template name="getFirstReport"/>-->

       <!--<xsl:result-document href="users-ids.xml">-->
<!--        
           <users-ids>
                <xsl:call-template name="recurse-ids">
                   <xsl:with-param name="start" select="0"/>
               </xsl:call-template>
           </users-ids>
-->        
       <!--</xsl:result-document>-->

        <!-- /shared/University of Manchester/Reports/Sentry Export/Export_ALMA_Users_BE  -->

        <xsl:result-document href="getReport.bat">
            <xsl:text>echo Start time: %DATE% %TIME%</xsl:text>
            <xsl:text>&#xD;&#xA;</xsl:text>
            
            <!-- BA: get first users report with count - start -->
            <xsl:value-of select="concat('wget &quot;', $users-base-url, '?limit=1&amp;offset=0&amp;apikey=', $users-api-key, '&quot; --no-check-certificate -O users.xml')"/>            
            <xsl:text>&#xD;&#xA;</xsl:text>
            <xsl:text>TIMEOUT /T 20</xsl:text>
            <xsl:text>&#xD;&#xA;</xsl:text>
            <!-- BA: get first users report with count - end -->
            
            <xsl:value-of select="concat('wget &quot;', $report-base-url, '?apikey=', $report-api-key, '&amp;path=/shared/University of Manchester/Reports/Sentry Export/Export_ALMA_Users_BE&quot;', ' --no-check-certificate -O report1.xml')"/>
            <xsl:text>&#xD;&#xA;</xsl:text>
            <xsl:text>TIMEOUT /T 20</xsl:text>
            <xsl:text>&#xD;&#xA;</xsl:text>
            <xsl:text>java -jar "C:\saxon\saxon9pe.jar" -s:dummy.xml -xsl:generateReportCalls.xsl -o:reportCalls.bat</xsl:text>
            <xsl:text>&#xD;&#xA;</xsl:text>
            <xsl:text>echo End time: %DATE% %TIME%</xsl:text>            
            <xsl:text>&#xD;&#xA;</xsl:text>
            <xsl:value-of select="'pause'"/>
        </xsl:result-document>
        
    </xsl:template>
    
    <xsl:template name="getFirstReport">
        <xsl:variable name="url">
            <xsl:value-of select="concat($report-base-url, '?apikey=', $users-api-key, '&amp;path=%2Fshared%2FUniversity%20of%20Manchester%2FReports%2FSentry%20Export%2FExport_ALMA_Users_BE')"></xsl:value-of>
        </xsl:variable>
        
    </xsl:template>


    <xsl:template name="recurse-ids">
        <xsl:param name="start" as="xs:integer"/>
        <xsl:variable name="ids-url">
            <xsl:value-of select="concat($users-base-url, '?limit=10&amp;offset=', $start, '&amp;order_by=last_name, first_name, primary_id&amp;apikey=', $users-api-key)"/>
        </xsl:variable>
        
        <xsl:variable name="doc-users">
            <xsl:copy-of select="doc($ids-url)/users"/>
        </xsl:variable>
        
        <xsl:for-each select="$doc-users/users/user">
            <id><xsl:value-of select="primary_id/text()"/></id>
        </xsl:for-each>
        
        <xsl:choose>
            <xsl:when test="$start + 10 &lt; $num-records">
                <!-- get all the ids -->
                <xsl:call-template name="recurse-ids">
                    <xsl:with-param name="start" select="$start + 11"/>
                </xsl:call-template>                
            </xsl:when>
        </xsl:choose>
                        
    </xsl:template>
        
</xsl:stylesheet>