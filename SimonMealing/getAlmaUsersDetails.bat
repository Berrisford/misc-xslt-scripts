echo Start time: %DATE% %TIME%
::java -jar "C:\saxon\saxon9pe.jar" -s:files(short).xml -xsl:legacy2pure.xsl -o:output(short).xml
java -jar "C:\saxon\saxon9pe.jar" -s:dummy.xml -xsl:getAlmaUsers.xsl -o:users-ids.xml
TIMEOUT /T 10
java -jar "C:\saxon\saxon9pe.jar" -s:users-ids.xml -xsl:getUsersDetails.xsl -o:users-details.xml
echo End time: %DATE% %TIME%
pause