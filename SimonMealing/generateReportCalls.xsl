<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="text" indent="yes"/>
    
    <xsl:variable name="users-count">
        <xsl:value-of select="doc('reports/users.xml')/users/@total_record_count"/>
    </xsl:variable>
    
    <xsl:variable name="resumption-token">
        <xsl:value-of select="doc('reports/report1.xml')/report/QueryResult/ResumptionToken"/>
    </xsl:variable>

    <xsl:variable name="iterations-count">
        <!--<xsl:value-of select="ceiling(number(($users-count - 25) div 1000))"/>-->
        <!--<xsl:value-of select="floor(number(($users-count - 25) div 1000))"/>-->
        <xsl:value-of select="ceiling(number($users-count div 1000))"/>
    </xsl:variable>

    <xsl:variable name="report-base-url">
        <xsl:value-of select="'https://api-na.hosted.exlibrisgroup.com/almaws/v1/analytics/reports'"/>
    </xsl:variable>

    <xsl:variable name="report-api-key">
        <xsl:value-of select="'l7xxebeb02758adb428d82126420223533a2'"/>
    </xsl:variable>
    
    <xsl:template match="/">        
        <xsl:call-template name="recurse-report-call">
            <xsl:with-param name="count" select="1"/>
        </xsl:call-template>
    </xsl:template>
       
    <xsl:template name="recurse-report-call">
        <xsl:param name="count" as="xs:integer"/>
        <xsl:variable name="report-name">
            <xsl:value-of select="concat('reports/report', $count + 1, '.xml')"/>
        </xsl:variable>
        <xsl:text>&#xD;&#xA;</xsl:text>
        <xsl:value-of select="concat('echo retrieving 1000 report chunk no: ', $count + 1, '...')"/>        
        <xsl:text>&#xD;&#xA;</xsl:text>
        <xsl:value-of select="concat('wget &quot;', $report-base-url, '?token=', $resumption-token, '&amp;apikey=', $report-api-key, '&amp;limit=1000&quot;', ' --no-check-certificate -O ', $report-name)"/>
        <xsl:text>&#xD;&#xA;</xsl:text>
        
        <xsl:choose>
            <xsl:when test="$count &lt; $iterations-count">
                <!-- call report again -->
                <xsl:call-template name="recurse-report-call">
                    <xsl:with-param name="count" select="$count + 1"/>
                </xsl:call-template>                
            </xsl:when>
            <xsl:otherwise>
                <!-- BA: wait for a bit -->
                <xsl:text>TIMEOUT /T 10</xsl:text>
                <xsl:text>&#xD;&#xA;</xsl:text>
                <xsl:text>echo generating csv file...</xsl:text>
                <xsl:text>&#xD;&#xA;</xsl:text>
                <xsl:text>mkdir csv</xsl:text>            
                <xsl:text>&#xD;&#xA;</xsl:text>
                <xsl:text>java -jar "saxon\saxon9pe.jar" -s:dummy.xml -xsl:createCSV.xsl -o:"csv\report.csv"</xsl:text>
                <xsl:text>&#xD;&#xA;</xsl:text>
                <xsl:text>pause</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>

</xsl:stylesheet>