echo Start time: %DATE% %TIME%
mkdir reports
wget "https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users?limit=1&offset=0&apikey=l7xx9b3ddcd32d8a419181fec710b3e53ee3" --no-check-certificate -O reports/users.xml
wget "https://api-na.hosted.exlibrisgroup.com/almaws/v1/analytics/reports?apikey=l7xxebeb02758adb428d82126420223533a2&path=/shared/University of Manchester/Reports/Sentry Export/Export_ALMA_Users_BE&limit=1000" --no-check-certificate -O reports/report1.xml
echo generating report calls...
java -jar "C:\saxon\saxon9pe.jar" -s:dummy.xml -xsl:generateReportCalls.xsl -o:reportCalls.bat
TIMEOUT /T 10
echo executing report calls...
reportCalls.bat