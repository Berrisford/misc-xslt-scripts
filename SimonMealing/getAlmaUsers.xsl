<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>

    <!--https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users?limit=1&offset=0&apikey=l7xx9b3ddcd32d8a419181fec710b3e53ee3-->
    
    <!--https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users?limit=100&offset=0&order_by=last_name, first_name, primary_id&apikey=l7xx9b3ddcd32d8a419181fec710b3e53ee3-->
    
    <!--https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/9256313?user_id_type=all_unique&view=full&expand=none&apikey=l7xx9b3ddcd32d8a419181fec710b3e53ee3-->
    
    <xsl:variable name="base-url">
        <xsl:value-of select="'https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users'"/>
    </xsl:variable>
    
    <xsl:variable name="api-key">
        <xsl:value-of select="'l7xx9b3ddcd32d8a419181fec710b3e53ee3'"/>    
    </xsl:variable>
    
    <xsl:variable name="count-url">
        <xsl:value-of select="concat($base-url, '?limit=1&amp;offset=0&amp;apikey=', $api-key)"/>
    </xsl:variable>
    
    <xsl:variable name="num-records">
        <!--<xsl:value-of select="doc($count-url)/users/@total_record_count"/>-->
        <xsl:value-of select="3"/>
    </xsl:variable>

    <xsl:variable name="ids-start" as="xs:integer">
        <xsl:value-of select="0"/>
    </xsl:variable>
    
    <xsl:template match="/">        
       <!--<xsl:result-document href="users-ids.xml">-->
           <users-ids>
                <xsl:call-template name="recurse-ids">
                   <xsl:with-param name="start" select="0"/>
               </xsl:call-template>
           </users-ids>
       <!--</xsl:result-document>-->
    </xsl:template>
    
    
    <xsl:template name="recurse-ids">
        <xsl:param name="start" as="xs:integer"/>
        <xsl:variable name="ids-url">
            <xsl:value-of select="concat($base-url, '?limit=10&amp;offset=', $start, '&amp;order_by=last_name, first_name, primary_id&amp;apikey=', $api-key)"/>
        </xsl:variable>
        
        <xsl:variable name="doc-users">
            <xsl:copy-of select="doc($ids-url)/users"/>
        </xsl:variable>
        
        <xsl:for-each select="$doc-users/users/user">
            <id><xsl:value-of select="primary_id/text()"/></id>
        </xsl:for-each>
        
        <xsl:choose>
            <xsl:when test="$start + 10 &lt; $num-records">
                <!-- get all the ids -->
                <xsl:call-template name="recurse-ids">
                    <xsl:with-param name="start" select="$start + 11"/>
                </xsl:call-template>                
            </xsl:when>
        </xsl:choose>
                        
    </xsl:template>
        
</xsl:stylesheet>