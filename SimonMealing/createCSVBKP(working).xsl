<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:urn="urn:schemas-microsoft-com:xml-analysis:rowset"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="text" indent="yes"/>
    
    
    <xsl:variable name="users-count">
        <xsl:value-of select="doc('users.xml')/users/@total_record_count"/>
    </xsl:variable>
    
    <xsl:variable name="file-count">
        <xsl:value-of select="floor(number($users-count div 1000))"/>
    </xsl:variable>
    
    
    <xsl:template match="/">
        <xsl:text>in template...</xsl:text>
        
        <xsl:for-each select="report/QueryResult/ResultXml/urn:rowset/urn:Row">
            <xsl:value-of select="concat(urn:Column5, ', ')"/>
            <xsl:value-of select="concat(urn:Column4, ', ')"/>
            <xsl:value-of select="concat(urn:Column6, ', ')"/>
            <xsl:value-of select="concat(urn:Column2, ', ')"/>
            <xsl:value-of select="concat(urn:Column3, ', ')"/>
            <xsl:value-of select="concat(urn:Column5, ', ')"/>
            <xsl:value-of select="concat(urn:Column1, ', ')"/>
        </xsl:for-each>
    </xsl:template>
     
</xsl:stylesheet>