<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="text" indent="yes"/>
    
    <xsl:variable name="users-count">
        <xsl:value-of select="doc('users.xml')/users/@total_record_count"/>
    </xsl:variable>
    
    <xsl:variable name="file-count">
        <xsl:value-of select="floor(number($users-count div 1000))"/>
    </xsl:variable>

    
    <xsl:template match="/">        
        <xsl:call-template name="recurse-report-files">
            <xsl:with-param name="count" select="214"/>
        </xsl:call-template>
    </xsl:template>
       
    <xsl:template name="recurse-report-files">
        <xsl:param name="count" as="xs:integer"/>
        <xsl:variable name="report-name">
            <xsl:value-of select="concat('report', $count + 1, '.xml')"/>
        </xsl:variable>
        <xsl:variable name="rows-doc">
            <!--<xsl:copy-of select="doc($report-name)/report/QueryResult/ResultXml/rowset"/>-->
            <xsl:copy-of select="doc('report215.xml')/report/QueryResult/ResultXml/rowset"/>
        </xsl:variable>
        
        <xsl:for-each select="$rows-doc/rowset/Row">
            <xsl:value-of select="concat(./Column5, ', ')"/>
            <xsl:value-of select="concat(./Column4, ', ')"/>
            <xsl:value-of select="concat(Column6, ', ')"/>
            <xsl:value-of select="concat(Column2, ', ')"/>
            <xsl:value-of select="concat(Column3, ', ')"/>
            <xsl:value-of select="concat(Column5, ', ')"/>
            <xsl:value-of select="concat(Column1, ', ')"/>
        </xsl:for-each>

        <xsl:choose>
            <xsl:when test="$count &lt; $file-count">
                <!-- call report again -->
                <xsl:call-template name="recurse-report-files">
                    <xsl:with-param name="count" select="$count + 1"/>
                </xsl:call-template>                
            </xsl:when>
        </xsl:choose>
        
    </xsl:template>

</xsl:stylesheet>