<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <!--https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users?limit=1&offset=0&apikey=l7xx9b3ddcd32d8a419181fec710b3e53ee3-->
    
    <!--https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users?limit=100&offset=0&order_by=last_name, first_name, primary_id&apikey=l7xx9b3ddcd32d8a419181fec710b3e53ee3-->
    
    <!--https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/9256313?user_id_type=all_unique&view=full&expand=none&apikey=l7xx9b3ddcd32d8a419181fec710b3e53ee3-->
    
    <xsl:variable name="base-url">
        <xsl:value-of select="'https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users'"/>
    </xsl:variable>
    
    <xsl:variable name="api-key">
        <xsl:value-of select="'l7xx9b3ddcd32d8a419181fec710b3e53ee3'"/>    
    </xsl:variable>
            
    <xsl:template match="/">        
        <!--<xsl:result-document href="users-details.xml">-->
            <xsl:variable name="doc-users-ids">
                <xsl:copy-of select="doc('users-ids.xml')/users-ids"/>
            </xsl:variable>
            <users>
                <xsl:for-each select="$doc-users-ids/users-ids/id">
                    <xsl:call-template name="get-user-details">
                        <xsl:with-param name="id" select="./text()"/>
                    </xsl:call-template>
                </xsl:for-each>
            </users>
        <!--</xsl:result-document>-->                           
    </xsl:template>
        
    <xsl:template name="get-user-details">
        <xsl:param name="id"/>
        <!--https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/9256313?user_id_type=all_unique&view=full&expand=none&apikey=l7xx9b3ddcd32d8a419181fec710b3e53ee3-->
        <xsl:variable name="url" select="concat($base-url, '/', $id, '?user_id_type=all_unique&amp;view=full&amp;expand=none&amp;apikey=', $api-key)"/>
        <xsl:variable name="doc-user">
            <xsl:copy-of select="document($url)/user"/>
        </xsl:variable>
        <user>
            <member_no><xsl:value-of select="$doc-user/user/primary_id"/></member_no>
            <category_2><xsl:value-of select="$doc-user/user/last_name"/></category_2>
            <category_1><xsl:value-of select="$doc-user/user/user_group"/></category_1>
            <expiry_date><xsl:value-of select="$doc-user/user/expiry_date"/></expiry_date>
            <card_no><xsl:value-of select="$doc-user/user/user_identifiers/user_identifier/value"/></card_no>
            <secondary_id><xsl:value-of select="$doc-user/user/primary_id"/></secondary_id>
            <email_address><xsl:value-of select="$doc-user/user/contact_info/emails/email/email_address"/></email_address>
        </user>
    </xsl:template>
            
</xsl:stylesheet>