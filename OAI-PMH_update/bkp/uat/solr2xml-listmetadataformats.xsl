<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
	<xsl:output encoding="UTF-8" method="xml" indent="yes" omit-xml-declaration="no"/>
	<xsl:strip-space elements="*"/>
	
	<xsl:template match="/">
		<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" 
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/
			http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
			<responseDate><xsl:value-of select="/response/lst/lst/str[@name='date']"/></responseDate>
			<xsl:element name="request">
				<xsl:attribute name="verb" select="'ListMetadataFormats'"/>
				<xsl:apply-templates select="/response/lst/lst/str[@name='identifier']"/>
				<xsl:value-of select="'http://manchester.ac.uk/escholar/api/oai2'"/>
			</xsl:element>
			<xsl:apply-templates select="/response/lst/lst[exists(str[@name='identifier'])]">
				<xsl:with-param name="identifier" select="/response/lst/lst/str[@name='identifier']"/>
			</xsl:apply-templates>
			<xsl:apply-templates select="/response/lst/lst[not(exists(str[@name='identifier']))]"/>
		</OAI-PMH>
		
	</xsl:template>
	<xsl:template match="str[@name='identifier']">
		<xsl:attribute name="identifier" select="."/>
	</xsl:template>
	
	<xsl:template match="lst[not(exists(str[@name='identifier']))] | result[@name='response'][doc/str[@name='r.isofcontenttype.pid'][.='uk-ac-man-con:20']]">
		<ListMetadataFormats>
			<metadataFormat>
				<metadataPrefix>oai_dc</metadataPrefix>
				<schema>http://www.openarchives.org/OAI/2.0/oai_dc.xsd</schema>
				<metadataNamespace>http://www.openarchives.org/OAI/2.0/oai_dc/</metadataNamespace>
			</metadataFormat>
			<metadataFormat>
				<metadataPrefix>uketd_dc</metadataPrefix>
				<schema>http://www.language-archives.org/OLAC/olac-0.2.xsd</schema>
				<metadataNamespace>http://www.language-archives.org/OLAC/0.2/</metadataNamespace>
			</metadataFormat>
		</ListMetadataFormats>
	</xsl:template>
	
	<xsl:template match="lst[(exists(str[@name='identifier']))]">
		<xsl:apply-templates select="/response/result[@name='response']">
			<xsl:with-param name="identifier" select="str[@name='identifier']"/>
		</xsl:apply-templates>
	</xsl:template>
	
	
	<xsl:template match="result[@name='response'][not(exists(doc))] | result[@name='response'][doc/str[@name='r.isofcontenttype.pid']!='uk-ac-man-con:20']">
		<xsl:param name="identifier"/>
		<error code="idDoesNotExist"><xsl:value-of select="$identifier"/><xsl:text> has the 
			structure of a valid LOC identifier, but it maps to no known item</xsl:text> 
			</error>	
	</xsl:template>
	<xsl:template match="*|@*"/>
</xsl:stylesheet>
