<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
	<xsl:output media-type="application/json;charset=UTF-8" encoding="UTF-8" method="xml" indent="yes" omit-xml-declaration="no"/>
	<xsl:strip-space elements="*"/>
	
	<xsl:template match="/">
		<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" 
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/
			http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
			<responseDate><xsl:value-of select="/response/lst/lst/str[@name='date']"/></responseDate>
			<request verb="ListSets">http://www.manchester.ac.uk/escholar/api/oai2</request>
			<ListSets>
				<set>
					<setSpec>thesis</setSpec>
					<setName>Thesis collection</setName>
					<setDescription>
						<oai_dc:dc 
							xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/" 
							xmlns:dc="http://purl.org/dc/elements/1.1/" 
							xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
							xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/ 
							http://www.openarchives.org/OAI/2.0/oai_dc.xsd">
							<dc:description>This set contains open access electronic theses  from 2010</dc:description>
						</oai_dc:dc>
					</setDescription>
				</set>
				<set>
					<setSpec>fulltext</setSpec>
					<setName>Full text collection</setName>
					<setDescription>
						<oai_dc:dc 
							xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/" 
							xmlns:dc="http://purl.org/dc/elements/1.1/" 
							xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
							xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/ 
							http://www.openarchives.org/OAI/2.0/oai_dc.xsd">
							<dc:description>Collection of full text materials</dc:description>
						</oai_dc:dc>
					</setDescription>
				</set>
			</ListSets>
		</OAI-PMH>
		
	</xsl:template>
</xsl:stylesheet>
