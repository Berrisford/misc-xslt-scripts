<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:dc="http://purl.org/dc/elements/1.1/" 
    xmlns="http://www.openarchives.org/OAI/2.0/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd 
    http://purl.org/dc/elements/1.1/ http://dublincore.org/schemas/xmls/qdc/2008/02/11/dc.xsd
    http://naca.central.cranfield.ac.uk/ethos-oai/2.0/ http://naca.central.cranfield.ac.uk/ethos-oai/2.0/uketd_dc.xsd"
    xmlns:uketdterms="http://naca.central.cranfield.ac.uk/ethos-oai/terms/" 
    xmlns:dcterms="http://purl.org/dc/terms/" 
    xmlns:uketd_dc="http://naca.central.cranfield.ac.uk/ethos-oai/2.0/"
    exclude-result-prefixes="xs xd" version="2.0">
    
    <xsl:output media-type="application/xml;charset=UTF-8" indent="no" encoding="UTF-8" method="xml" omit-xml-declaration="no" />
    <xsl:strip-space elements="*"/>
    
    <xsl:param name="searchurl" select="''" />   
    <xsl:template match="/">
        <xsl:apply-templates select="response" />        
    </xsl:template>

    <xsl:template match="response">
        <OAI-PMH xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd 
            http://purl.org/dc/elements/1.1/ http://dublincore.org/schemas/xmls/qdc/2008/02/11/dc.xsd
            http://naca.central.cranfield.ac.uk/ethos-oai/2.0/ http://naca.central.cranfield.ac.uk/ethos-oai/2.0/uketd_dc.xsd">
            <xsl:apply-templates select="lst/lst/str[@name='date']" />
            <xsl:apply-templates select="lst/lst/str[@name='verb']" mode="head" />
            <xsl:choose>
                <xsl:when test="lst/lst/str[@name='verb']">
                    <xsl:apply-templates select="lst/lst/str[@name='verb']" mode="records" />                                                           
                </xsl:when>
                <xsl:otherwise>
                    <error code="badVerb">
                        <xsl:text>Invalid/missing verb</xsl:text> 
                    </error>	                                                    
                </xsl:otherwise>
            </xsl:choose>                
        </OAI-PMH>               
    </xsl:template>
       
    <xsl:template match="doc">
        <xsl:param name="datetime"/>
                <record>
                    <header>
                        <xsl:choose>
                            <xsl:when test="str[@name='oai.isdeleted'] and str[@name='oai.isdeleted'] = 'Yes'">
                                <xsl:attribute name="status" select="'deleted'" />
                            </xsl:when>
                        </xsl:choose>                                
                        <xsl:apply-templates select="str[@name='PID']" mode="head_identifier" />
                        <xsl:apply-templates select="date[@name='oai.lastmodifieddate']" />
                        <setSpec>thesis</setSpec>
                    </header>
                    <xsl:choose>
                        <xsl:when test="str[@name='oai.isdeleted']and str[@name='oai.isdeleted'] = 'Yes'">
                            <!-- Do nothing -->                    
                        </xsl:when>
                        <xsl:otherwise>                                                
                            <metadata>
                                <uketd_dc:uketddc >
                                    <xsl:apply-templates select="arr[@name='dc.identifier']/str"/>
                                    <xsl:apply-templates select="arr[@name='dc.title']/str" />
                                    <xsl:apply-templates select="arr[@name='dcterms.alternative']/str" />                            
                                    <xsl:apply-templates select="arr[@name='dc.creator']/str"/>
                                    <xsl:apply-templates select="arr[@name='dc.contributor']/str" />
                                    <xsl:apply-templates select="arr[@name='uketdterms.advisor']/str" />                    
                                    <xsl:apply-templates select="str[@name='dcterms.issued']" />                            
                                    <xsl:apply-templates select="arr[@name='dc.subject']/str" />
                                    <xsl:apply-templates select="arr[@name='uketdterms.institution']/str" />
                                    <xsl:apply-templates select="arr[@name='dc.type']/str" />
                                    <xsl:apply-templates select="arr[@name='uketdterms.qualificationlevel']/str" />
                                    <xsl:apply-templates select="arr[@name='uketdterms.qualificationname']/str" />
                                    <xsl:apply-templates select="arr[@name='uketdterms.grantnumber']/str" />
                                    <xsl:apply-templates select="arr[@name='dc.format']/str" />
                                    <xsl:apply-templates select="arr[@name='dc.language']/str" />                            
                                    <xsl:apply-templates select="arr[@name='dc.source']/str" />
                                    <xsl:apply-templates select="arr[@name='dc.relation']/str" />                            
                                    <xsl:apply-templates select="arr[@name='dcterms.isreferencedby']/str" /> 
                                    <xsl:apply-templates select="arr[@name='dcterms.abstract']/str" />
                                    <xsl:apply-templates select="arr[@name='dc.description']/str" />
                                    <xsl:apply-templates select="arr[@name='uketdterms.sponsor']/str" />
                                </uketd_dc:uketddc>                            
                            </metadata>
                        </xsl:otherwise>
                    </xsl:choose>                    
                </record>
    </xsl:template>

    <xsl:template match="doc" mode="identifiers">
        <record>
            <header>
                <xsl:choose>
                    <xsl:when test="str[@name='oai.isdeleted'] and str[@name='oai.isdeleted'] = 'Yes'">
                        <xsl:attribute name="status" select="'deleted'" />
                    </xsl:when>
                </xsl:choose>                                
                <xsl:apply-templates select="str[@name='PID']" mode="head_identifier" />
                <xsl:apply-templates select="date[@name='oai.lastmodifieddate']" />
                <setSpec>thesis</setSpec>
            </header>
        </record>
    </xsl:template>
    
    <xsl:template match="arr[@name='dc.identifier']/str">
        <dc:identifier>
            <xsl:value-of select="."/>
        </dc:identifier>
    </xsl:template>

    <xsl:template match="str[@name='PID']" mode="head_identifier">
        <identifier>
            <xsl:value-of select="concat('oai:escholar.manchester.ac.uk:', replace(., ':', '-'))" />            
        </identifier>
    </xsl:template>
    
    <xsl:template match="arr[@name='dc.relation']/str">
        <dc:relation>
            <xsl:value-of select="." />
        </dc:relation>
    </xsl:template>

    <xsl:template match="arr[@name='dcterms.isreferencedby']/str">
        <dcterms:isReferencedBy>
            <xsl:value-of select="." />
        </dcterms:isReferencedBy>
    </xsl:template>
    
    <xsl:template match="arr[@name='dc.title']/str">
        <dc:title>
            <xsl:value-of select="."/>
        </dc:title>
    </xsl:template>
    
    <xsl:template match="arr[@name='dcterms.alternative']/str">
        <xsl:choose>
            <xsl:when test=". != '' and . != null">
                <dcterms:alternative>
                    <xsl:value-of select="." />                    
                </dcterms:alternative>                
            </xsl:when>
        </xsl:choose>        
    </xsl:template>    

    <xsl:template match="arr[@name='dc.type']/str" >
        <dc:type>
            <xsl:value-of select="." />
        </dc:type>
    </xsl:template>
    
    <xsl:template match="arr[@name='dc.format']/str">
        <dc:format>
            <xsl:value-of select="." />
        </dc:format>
    </xsl:template>

    <xsl:template match="arr[@name='dcterms.abstract']/str">
        <dcterms:abstract>
            <xsl:value-of select="." />
        </dcterms:abstract>
    </xsl:template>
    
    <xsl:template match="arr[@name='dc.description']/str">
        <dc:description>
            <xsl:value-of select="." />
        </dc:description>
    </xsl:template>
            
    <xsl:template match="str[@name='dcterms.issued']">
        <dcterms:issued>
            <xsl:value-of select="." />
        </dcterms:issued>
    </xsl:template>

    <xsl:template match="arr[@name='dc.language']/str">
        <dc:language>
            <xsl:value-of select="."/>                    
        </dc:language>
    </xsl:template>
    
    <xsl:template match="arr[@name='dc.subject']/str">        
        <dc:subject>
            <xsl:value-of select="." />                
        </dc:subject>
    </xsl:template>
    
    <xsl:template match="arr[@name='uketdterms.institution']/str">
        <uketdterms:institution>
            <xsl:value-of select="." />
        </uketdterms:institution>
    </xsl:template>
    
    <xsl:template match="arr[@name='uketdterms.advisor']/str" >
        <uketdterms:advisor>
            <xsl:value-of select="." />
        </uketdterms:advisor>
    </xsl:template>

    <xsl:template match="arr[@name='dc.contributor']/str">
        <dc:contributor>
            <xsl:value-of select="." />
        </dc:contributor>
    </xsl:template>

    <xsl:template match="arr[@name='uketdterms.sponsor']/str">
        <uketdterms:sponsor>
            <xsl:value-of select="." />            
        </uketdterms:sponsor>        
    </xsl:template>
        
    <xsl:template match="arr[@name='uketdterms.qualificationlevel']/str">
        <uketdterms:qualificationlevel>
            <xsl:value-of select="." />
        </uketdterms:qualificationlevel>        
    </xsl:template>

    <xsl:template match="arr[@name='uketdterms.qualificationname']/str">
        <uketdterms:qualificationname>
            <xsl:value-of select="." />
        </uketdterms:qualificationname>        
    </xsl:template>

    <xsl:template match="arr[@name='uketdterms.grantnumber']/str">
        <uketdterms:grantnumber>
            <xsl:value-of select="." />
        </uketdterms:grantnumber>        
    </xsl:template>
    
    <xsl:template match="str[@name='verb']" mode="head">
        <request xmlns:dc="http://purl.org/dc/elements/1.1/">
            <xsl:attribute name="verb">
                <xsl:value-of select="." />        
            </xsl:attribute>
            <xsl:choose>
                <xsl:when test=". = 'GetRecord'">
                    <xsl:attribute name="identifier" select="concat('oai:escholar.manchester.ac.uk:', replace(/response/result/doc/str[@name='PID'], ':', '-'))" />
                    <xsl:attribute name="metadataPrefix" select="'uketd_dc'"/>
                </xsl:when>
                <xsl:when test=". = 'GetRecord'">
                    <xsl:choose>                    
                        <xsl:when test="../str[@name='resumptionToken'] and ../str[@name='resumptionToken'] != '' and ../str[@name='resumptionToken'] != 'null'">
                            <xsl:attribute name="resumptionToken" select="../str[@name='resumptionToken']" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="metadataPrefix" select="'uketd_dc'"/>
                            <xsl:attribute name="set" select="../str[@name='set']"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>                    
            </xsl:choose>
            <xsl:value-of select="'http://manchester.ac.uk/escholar/api/oai2'"/>
        </request>
    </xsl:template>
    
    <xsl:template match="str[@name='verb']" mode="records">
        <xsl:choose>
            <xsl:when test=". = 'ListIdentifiers'">
                <xsl:choose>
                    <xsl:when test="/response/result/doc and /response/result/doc/str[@name='r.isofcontenttype.pid'] = 'uk-ac-man-con:20'">
                        <ListIdentifiers>
                            <xsl:apply-templates select="/response/result/doc" mode="identifiers" />
                            <xsl:apply-templates select="../str[@name='verb']" mode="resumption_token" />                            
                        </ListIdentifiers>                                        
                    </xsl:when>
                    <xsl:otherwise>
                        <error code="noRecordsMatch">
                            <xsl:text>NoRecordsMatch - The combination of the values of the from, until, set and metadataPrefix arguments results in an empty list</xsl:text> 
                        </error>	                        
                    </xsl:otherwise>
                </xsl:choose>                
            </xsl:when>            
            <xsl:when test=". = 'GetRecord'">
                <xsl:choose>
                    <xsl:when test="/response/result/doc and /response/result/doc/str[@name='r.isofcontenttype.pid'] = 'uk-ac-man-con:20'">
                        <GetRecord>
                            <xsl:apply-templates select="/response/result/doc" />                    
                        </GetRecord>                                        
                    </xsl:when>
                    <xsl:otherwise>
                        <error code="idDoesNotExist">
                            <xsl:text>This identifier has the structure of a valid LOC identifier, but it maps to no known item</xsl:text> 
                        </error>	                        
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>            
            <xsl:when test=". = 'ListRecords'">
                <xsl:choose>
                    <xsl:when test="/response/result/doc and /response/result/doc/str[@name='r.isofcontenttype.pid'] = 'uk-ac-man-con:20'">
                        <ListRecords>
                            <xsl:apply-templates select="/response/result/doc" />                    
                            <xsl:apply-templates select="../str[@name='verb']" mode="resumption_token" />                            
                        </ListRecords>                                        
                    </xsl:when>
                    <xsl:otherwise>
                        <error code="noRecordsMatch">
                            <xsl:text>NoRecordsMatch - The combination of the values of the from, until, set and metadataPrefix arguments results in an empty list</xsl:text> 
                        </error>	                        
                    </xsl:otherwise>
                </xsl:choose>                    
            </xsl:when>
            <xsl:otherwise>
                <error code="badVerb">
                    <xsl:text>Unrecognised verb</xsl:text> 
                </error>	                                                    
            </xsl:otherwise>            
        </xsl:choose>        
    </xsl:template>
              
    <xsl:template match="arr[@name='dc.creator']/str">
        <dc:creator>
            <xsl:value-of select="."/>
        </dc:creator>
    </xsl:template>
    
    <xsl:template match="lst/lst/str[@name='date']">
        <responseDate>
            <xsl:value-of select="replace(., 'z', 'Z')" />
        </responseDate>        
    </xsl:template>

    <xsl:template match="date[@name='oai.lastmodifieddate']">
        <datestamp>
            <xsl:value-of select="substring-before(string(.), 'T')" />            
        </datestamp>
    </xsl:template>

    <xsl:template match="arr[@name='dc.source']/str">
        <dc:source>
            <xsl:value-of select="."/>
        </dc:source>
    </xsl:template>
    
    <xsl:template match="str[@name='verb']" mode="resumption_token">
        <xsl:choose>
            <xsl:when test="/response/result/@numFound > ../str[@name='start'] + ../str[@name='rows']">
                <resumptionToken>
                    <xsl:text>uketd_dc:</xsl:text>
                    <xsl:choose>
                        <xsl:when test="../str[@name='from']">
                            <xsl:value-of select="../str[@name='from']" />                                        
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>null</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="../str[@name='until']">
                            <xsl:value-of select="concat(':', ../str[@name='until'])" />                             
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>:null</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>                                        
                    <xsl:value-of select="concat(':', 'thesis', ':')" />
                    <xsl:choose>
                        <xsl:when test="../str[@name='start'] and ../str[@name='rows']">
                            <xsl:value-of select="../str[@name='start'] + ../str[@name='rows']" />                            
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>null</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>                    
                </resumptionToken>                        
            </xsl:when>
            <xsl:when test="/response/result/@numFound &lt;= ../str[@name='start'] + ../str[@name='rows']">
                <xsl:choose>
                    <xsl:when test="../str[@name='resumptionToken'] and ../str[@name='resumptionToken'] != '' and ../str[@name='resumptionToken'] != 'null'">
                        <resumptionToken>
                        </resumptionToken>                            
                    </xsl:when>
                </xsl:choose>                
            </xsl:when>                
        </xsl:choose>        
    </xsl:template>
    
</xsl:stylesheet>