<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    exclude-result-prefixes="xs" version="2.0"
    >
        
    <xsl:output indent="no" encoding="UTF-8" method="xml" omit-xml-declaration="no" media-type="application/xml;charset=UTF-8"/>
    <xsl:strip-space elements="*"/>
    
    <xsl:param name="searchurl" select="''" />   
    <xsl:template match="/">
        <xsl:apply-templates select="response" />        
    </xsl:template>

    <xsl:template match="response">
        <OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" 
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"            
            xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
            <xsl:apply-templates select="lst/lst/str[@name='date']" />
            <xsl:apply-templates select="lst/lst/str[@name='verb']" mode="head" />
            <xsl:choose>
                <xsl:when test="lst/lst/str[@name='verb']">
                    <xsl:apply-templates select="lst/lst/str[@name='verb']" mode="records" />                                                           
                </xsl:when>
                <xsl:otherwise>
                    <error code="badVerb">
                        <xsl:text>Invalid/missing verb</xsl:text> 
                    </error>	                                                    
                </xsl:otherwise>
            </xsl:choose>                
        </OAI-PMH>       
    </xsl:template>
    
    <xsl:template match="doc">
        <record>
            <header>
                <xsl:choose>
                    <xsl:when test="str[@name='oai.isdeleted'] and str[@name='oai.isdeleted'] = 'Yes'">
                        <xsl:attribute name="status" select="'deleted'" />
                    </xsl:when>
                </xsl:choose>                
                <xsl:apply-templates select="str[@name='PID']" mode="head_identifier" />
                <xsl:apply-templates select="date[@name='oai.lastmodifieddate']" />
                <xsl:choose>
                    <xsl:when test="/response/lst/lst/str[@name='set'] and /response/lst/lst/str[@name='set'] != '' and /response/lst/lst/str[@name='set'] != 'null'">
                        <setSpec><xsl:value-of select="/response/lst/lst/str[@name='set']" /></setSpec>
                    </xsl:when>
                </xsl:choose>                                                                            
            </header>
            <xsl:choose>
                <xsl:when test="str[@name='oai.isdeleted']and str[@name='oai.isdeleted'] = 'Yes'">
                    <!-- Do nothing -->                    
                </xsl:when>
                <xsl:otherwise>
                    <metadata>
                            <oai_dc:dc xmlns:dc="http://purl.org/dc/elements/1.1/" 
                                xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
                                xmlns="http://www.openarchives.org/OAI/2.0/"
                                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                                xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/ 
                                http://www.openarchives.org/OAI/2.0/oai_dc.xsd"
                                > 
                            <xsl:apply-templates select="arr[@name='dc.identifier']/str"/>                    
                            <!-- OpenAIRE Specific START -->
                            <xsl:choose>
                                <xsl:when test="/response/lst/lst/str[@name='set'] = 'ec_fundedresources'">
                                    <xsl:choose>
                                        <xsl:when test="arr[@name='dc.relation']/str">
                                            <xsl:apply-templates select="arr[@name='dc.relation']/str" mode="openAIRE" />                                    
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <dc:relation>GRANT NUMBER REQUIRED!!!</dc:relation>                            
                                        </xsl:otherwise>
                                    </xsl:choose>                    
                                    <dc:rights>info:eu-repo/semantics/openAccess</dc:rights>                                                
                                </xsl:when>
                            </xsl:choose>                                       
                            <!-- OpenAIRE Specific END -->                    
                            <xsl:apply-templates select="arr[@name='dc.title']/str" />
                            <xsl:apply-templates select="arr[@name='dc.creator']/str"/>
                            <xsl:apply-templates select="arr[@name='dc.contributor']/str" />
                            <xsl:apply-templates select="str[@name='dc.date']" />
                            <xsl:apply-templates select="arr[@name='dc.subject']/str" />
                            <xsl:apply-templates select="arr[@name='dc.publisher']/str" />
                            <xsl:choose>
                                <!-- OpenAIRE/DRIVER Specific START -->
                                <xsl:when test="/response/lst/lst/str[@name='set'] = 'ec_fundedresources'">
                                    <xsl:apply-templates select="str[@name='r.isofcontenttype.pid']" mode="driver_type" />
                                    <xsl:apply-templates select="arr[@name='dc.type']/str" mode="driver_version" />                            
                                    <xsl:apply-templates select="arr[@name='dc.type']/str" mode="driver_type_other" />                            
                                </xsl:when>
                                <xsl:when test="/response/lst/lst/str[@name='set'] = 'driver'">
                                    <xsl:apply-templates select="str[@name='r.isofcontenttype.pid']" mode="driver_type" />
                                    <xsl:apply-templates select="arr[@name='dc.type']/str" mode="driver_version" />                            
                                    <xsl:apply-templates select="arr[@name='dc.type']/str" mode="driver_type_other" />                            
                                </xsl:when>
                                <!-- OpenAIRE/DRIVER Specific END -->
                                <xsl:otherwise>
                                    <xsl:apply-templates select="arr[@name='dc.type']/str" /> 
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:apply-templates select="arr[@name='dc.format']/str" />
                            <xsl:apply-templates select="arr[@name='dc.language']/str" /> 
                            <xsl:apply-templates select="arr[@name='dc.source']/str" />
                            <xsl:choose>
                                <!-- OpenAIRE/DRIVER Specific START -->
                                <xsl:when test="/response/lst/lst/str[@name='set'] != 'ec_fundedresources'">
                                    <xsl:apply-templates select="arr[@name='dc.relation']/str" />
                                </xsl:when>
                                <!-- OpenAIRE/DRIVER Specific END -->
                            </xsl:choose>
                            <xsl:apply-templates select="arr[@name='dc.description']/str" />
                        </oai_dc:dc>
                    </metadata>
                </xsl:otherwise>                
            </xsl:choose>
        </record>
    </xsl:template>

    <xsl:template match="doc" mode="identifiers">
        <record>
            <header>
                <xsl:choose>
                    <xsl:when test="str[@name='oai.isdeleted'] and str[@name='oai.isdeleted'] = 'Yes'">
                        <xsl:attribute name="status" select="'deleted'" />
                    </xsl:when>
                </xsl:choose>                                
                <xsl:apply-templates select="str[@name='PID']" mode="head_identifier" />
                <xsl:apply-templates select="date[@name='oai.lastmodifieddate']" />
                <xsl:choose>
                    <xsl:when test="/response/lst/lst/str[@name='set'] and /response/lst/lst/str[@name='set'] != '' and /response/lst/lst/str[@name='set'] != 'null'">
                        <setSpec><xsl:value-of select="/response/lst/lst/str[@name='set']" /></setSpec>
                    </xsl:when>
                </xsl:choose>                                                                                            
            </header>
        </record>
    </xsl:template>
    
    <xsl:template match="arr[@name='dc.identifier']/str">
            <dc:identifier xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
                xmlns:dc="http://purl.org/dc/elements/1.1/" 
                xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
                xmlns="http://www.openarchives.org/OAI/2.0/"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                 xsl:exclude-result-prefixes="dc oai_dc xsi xd">
            <xsl:value-of select="."/>
        </dc:identifier>
    </xsl:template>

    <xsl:template match="str[@name='PID']" mode="head_identifier">
        <identifier>
            <xsl:value-of select="concat('oai:escholar.manchester.ac.uk:', replace(., ':', '-'))" />            
        </identifier>
    </xsl:template>
    
    <xsl:template match="arr[@name='dc.relation']/str">
        <dc:relation xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
            xmlns:dc="http://purl.org/dc/elements/1.1/" 
            xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
            xmlns="http://www.openarchives.org/OAI/2.0/"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsl:exclude-result-prefixes="dc oai_dc xsi xd">
            <xsl:value-of select="." />
        </dc:relation>
    </xsl:template>

    <xsl:template match="arr[@name='dc.relation']/str" mode="openAIRE">
        <xsl:choose>
            <xsl:when test="substring(., 1, 34) = 'info:eu-repo/grantAgreement/EC/FP7'">
                <dc:relation xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
                    xmlns:dc="http://purl.org/dc/elements/1.1/" 
                    xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
                    xmlns="http://www.openarchives.org/OAI/2.0/"
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xsl:exclude-result-prefixes="dc oai_dc xsi xd">
                    <xsl:value-of select="." />
                </dc:relation>                
            </xsl:when>
        </xsl:choose>        
    </xsl:template>
    
    <xsl:template match="arr[@name='dc.title']/str">
        <dc:title xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
            xmlns:dc="http://purl.org/dc/elements/1.1/" 
            xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
            xmlns="http://www.openarchives.org/OAI/2.0/"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsl:exclude-result-prefixes="dc oai_dc xsi xd">
            <xsl:value-of select="."/>
        </dc:title>
    </xsl:template>
    
    <xsl:template match="arr[@name='dc.format']/str">
        <dc:format xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
            xmlns:dc="http://purl.org/dc/elements/1.1/" 
            xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
            xmlns="http://www.openarchives.org/OAI/2.0/"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsl:exclude-result-prefixes="dc oai_dc xsi xd">
            <xsl:value-of select="." />
        </dc:format>
    </xsl:template>
    
    <!-- OpenAIRE/DRIVER Specific START -->

    <xsl:template match="str[@name='r.isofcontenttype.pid']" mode="driver_type">
        <dc:type xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
            xmlns:dc="http://purl.org/dc/elements/1.1/" 
            xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
            xmlns="http://www.openarchives.org/OAI/2.0/"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsl:exclude-result-prefixes="dc oai_dc xsi xd">            
            <xsl:choose>
                <xsl:when test=". = 'uk-ac-man-con:1'"> <!-- Journal article -->
                    <xsl:text>info:eu-repo/semantics/article</xsl:text>
                </xsl:when>
                <xsl:when test=". = 'uk-ac-man-con:2'"> <!-- Conference contribution -->
                    <xsl:text>info:eu-repo/semantics/conferenceObject</xsl:text>
                </xsl:when>
                <xsl:when test=". = 'uk-ac-man-con:3'"> <!-- Book contribution -->
                    <xsl:text>info:eu-repo/semantics/bookPart</xsl:text>
                </xsl:when>
                <xsl:when test=". = 'uk-ac-man-con:4'"> <!-- Book -->
                    <xsl:text>info:eu-repo/semantics/book</xsl:text>
                </xsl:when>
                <xsl:when test=". = 'uk-ac-man-con:5'"> <!-- Report -->
                    <xsl:text>info:eu-repo/semantics/report</xsl:text>
                </xsl:when>
                <xsl:when test=". = 'uk-ac-man-con:6'"> <!-- Conference proceeding -->
                    <xsl:text>info:eu-repo/semantics/conferenceObject</xsl:text>
                </xsl:when>
                <xsl:when test=". = 'uk-ac-man-con:7'"> <!-- Composition -->
                    <xsl:text>info:eu-repo/semantics/other</xsl:text>
                </xsl:when>
                <xsl:when test=". = 'uk-ac-man-con:9'"> <!-- Dissertation -->
                    <xsl:text>eu-repo/semantics/masterThesis</xsl:text>
                </xsl:when>
                <xsl:when test=". = 'uk-ac-man-con:10'"> <!-- Exhibition -->
                    <xsl:text>info:eu-repo/semantics/other</xsl:text>
                </xsl:when>
                <xsl:when test=". = 'uk-ac-man-con:11'"> <!-- Exhibition contribution -->
                    <xsl:text>info:eu-repo/semantics/other</xsl:text>
                </xsl:when>
                <xsl:when test=". = 'uk-ac-man-con:13'"> <!-- Patent -->
                    <xsl:text>info:eu-repo/semantics/patent</xsl:text>
                </xsl:when>
                <xsl:when test=". = 'uk-ac-man-con:14'"> <!-- Technical report -->
                    <xsl:text>info:eu-repo/semantics/report</xsl:text>
                </xsl:when>
                <xsl:when test=". = 'uk-ac-man-con:15'"> <!-- Thesis (Legacy) -->
                    <xsl:text>info:eu-repo/semantics/doctoralThesis</xsl:text>
                </xsl:when>
                <xsl:when test=". = 'uk-ac-man-con:16'"> <!-- Working paper -->
                    <xsl:text>info:eu-repo/semantics/workingPaper</xsl:text>
                </xsl:when>
                <xsl:when test=". = 'uk-ac-man-con:17'"> <!-- Journal contribution -->
                    <xsl:text>info:eu-repo/semantics/contributionToPeriodical</xsl:text>
                </xsl:when>
                <xsl:when test=". = 'uk-ac-man-con:18'"> <!-- Newspaper/magazine contribution -->
                    <xsl:text>info:eu-repo/semantics/article</xsl:text>
                </xsl:when>
                <xsl:when test=". = 'uk-ac-man-con:20'"> <!-- Thesis -->
                    <xsl:text>info:eu-repo/semantics/doctoralThesis</xsl:text>
                </xsl:when>
            </xsl:choose>
        </dc:type>
    </xsl:template>

    <xsl:template match="arr[@name='dc.type']/str" mode="driver_version">
        <xsl:choose>
            <xsl:when test=". = 'publishedVersion'">
                <dc:type xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
                    xmlns:dc="http://purl.org/dc/elements/1.1/" 
                    xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
                    xmlns="http://www.openarchives.org/OAI/2.0/"
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xsl:exclude-result-prefixes="dc oai_dc xsi xd">
                    <xsl:text>publishedVersion</xsl:text>            
                </dc:type>
            </xsl:when>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test=". = 'acceptedVersion'">
                <dc:type xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
                    xmlns:dc="http://purl.org/dc/elements/1.1/" 
                    xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
                    xmlns="http://www.openarchives.org/OAI/2.0/"
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xsl:exclude-result-prefixes="dc oai_dc xsi xd">
                    <xsl:text>acceptedVersion</xsl:text>            
                </dc:type>
            </xsl:when>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test=". = 'draft'">
                <dc:type xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
                    xmlns:dc="http://purl.org/dc/elements/1.1/" 
                    xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
                    xmlns="http://www.openarchives.org/OAI/2.0/"
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xsl:exclude-result-prefixes="dc oai_dc xsi xd">
                    <xsl:text>draft</xsl:text>            
                </dc:type>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="arr[@name='dc.type']/str" mode="driver_type_other">
        <!-- doing "AND" this (clumsy) way as joining them all up in one test, seems flakey... -->
        <xsl:choose>
            <xsl:when test=". != 'publishedVersion'">
                <xsl:choose>
                    <xsl:when test=". != 'acceptedVersion'">
                        <xsl:choose>
                            <xsl:when test=". != 'draft'">
                                <dc:type xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
                                    xmlns:dc="http://purl.org/dc/elements/1.1/" 
                                    xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
                                    xmlns="http://www.openarchives.org/OAI/2.0/"
                                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                                    xsl:exclude-result-prefixes="dc oai_dc xsi xd">
                                    <xsl:value-of select="." />
                                </dc:type>                                
                            </xsl:when>
                        </xsl:choose>
                        
                    </xsl:when>
                </xsl:choose>                
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <!-- OpenAIRE/DRIVER Specific END -->
    
    <xsl:template match="arr[@name='dc.type']/str">
        <dc:type xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
            xmlns:dc="http://purl.org/dc/elements/1.1/" 
            xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
            xmlns="http://www.openarchives.org/OAI/2.0/"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsl:exclude-result-prefixes="dc oai_dc xsi xd">
            <xsl:value-of select="." />
        </dc:type>
    </xsl:template>
    
    <xsl:template match="arr[@name='dc.source']/str">
        <dc:source xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
            xmlns:dc="http://purl.org/dc/elements/1.1/" 
            xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
            xmlns="http://www.openarchives.org/OAI/2.0/"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsl:exclude-result-prefixes="dc oai_dc xsi xd">
            <xsl:value-of select="."/>
        </dc:source>
    </xsl:template>
    
    <xsl:template match="arr[@name='dc.description']/str">
        <dc:description xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
            xmlns:dc="http://purl.org/dc/elements/1.1/" 
            xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
            xmlns="http://www.openarchives.org/OAI/2.0/"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsl:exclude-result-prefixes="dc oai_dc xsi xd">
            <xsl:value-of select="." />
        </dc:description>
    </xsl:template>
    
    <xsl:template match="str[@name='dc.date']">
        <dc:date xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
            xmlns:dc="http://purl.org/dc/elements/1.1/" 
            xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
            xmlns="http://www.openarchives.org/OAI/2.0/"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsl:exclude-result-prefixes="dc oai_dc xsi xd">
            <xsl:value-of select="." />
        </dc:date>
    </xsl:template>
    
    <xsl:template match="arr[@name='dc.language']/str">
        <dc:language xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
            xmlns:dc="http://purl.org/dc/elements/1.1/" 
            xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
            xmlns="http://www.openarchives.org/OAI/2.0/"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsl:exclude-result-prefixes="dc oai_dc xsi xd">
            <xsl:value-of select="."/>                    
        </dc:language>
    </xsl:template>
    
    <xsl:template match="arr[@name='dc.subject']/str">        
        <dc:subject xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
            xmlns:dc="http://purl.org/dc/elements/1.1/" 
            xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
            xmlns="http://www.openarchives.org/OAI/2.0/"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsl:exclude-result-prefixes="dc oai_dc xsi xd">
            <xsl:value-of select="." />                
        </dc:subject>
    </xsl:template>
    
    <xsl:template match="arr[@name='dc.publisher']/str">
        <dc:publisher xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
            xmlns:dc="http://purl.org/dc/elements/1.1/" 
            xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
            xmlns="http://www.openarchives.org/OAI/2.0/"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsl:exclude-result-prefixes="dc oai_dc xsi xd">
            <xsl:value-of select="."/>
        </dc:publisher>
    </xsl:template>
    
    <xsl:template match="arr[@name='dc.contributor']/str">
        <dc:contributor xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
            xmlns:dc="http://purl.org/dc/elements/1.1/" 
            xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
            xmlns="http://www.openarchives.org/OAI/2.0/"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsl:exclude-result-prefixes="dc oai_dc xsi xd">
            <xsl:value-of select="."/>
        </dc:contributor>
    </xsl:template>
    
    <xsl:template match="date[@name='oai.lastmodifieddate']">
        <datestamp>
            <xsl:value-of select="substring-before(string(.), 'T')" />            
        </datestamp>
    </xsl:template>
    
    <xsl:template match="str[@name='verb']" mode="head">
        <request>
            <xsl:attribute name="verb">
                <xsl:value-of select="." />        
            </xsl:attribute>
            <xsl:choose>
                <xsl:when test=". = 'GetRecord'">
                    <xsl:attribute name="identifier" select="concat('oai:escholar.manchester.ac.uk:', replace(/response/result/doc/str[@name='PID'], ':', '-'))" />
                    <xsl:attribute name="metadataPrefix" select="'oai_dc'"/>
                </xsl:when>
                <xsl:when test=". != 'GetRecord'">
                    <xsl:choose>                    
                        <xsl:when test="../str[@name='resumptionToken'] and ../str[@name='resumptionToken'] != '' and ../str[@name='resumptionToken'] != 'null'">
                            <xsl:attribute name="resumptionToken" select="../str[@name='resumptionToken']" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="metadataPrefix" select="'oai_dc'"/>
                            <xsl:attribute name="set" select="../str[@name='set']"/>
                            <xsl:attribute name="from" select="../str[@name='from']"/>
                            <xsl:attribute name="until" select="../str[@name='until']"/>                                        
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>                    
            </xsl:choose>
            <xsl:value-of select="'http://manchester.ac.uk/escholar/api/oai2'"/>
        </request>
    </xsl:template>
    
    <xsl:template match="str[@name='verb']" mode="records">
        <xsl:choose>
            <xsl:when test=". = 'ListIdentifiers'">
                <xsl:choose>
                    <xsl:when test="/response/result/doc">
                        <ListIdentifiers>
                            <xsl:apply-templates select="/response/result/doc" mode="identifiers" />                    
                            <xsl:apply-templates select="../str[@name='verb']" mode="resumption_token" />                            
                        </ListIdentifiers>                                        
                    </xsl:when>
                    <xsl:otherwise>
                        <error code="noRecordsMatch">
                            <xsl:text>NoRecordsMatch - The combination of the values of the from, until, set and metadataPrefix arguments results in an empty list</xsl:text> 
                        </error>	                        
                    </xsl:otherwise>
                </xsl:choose>                
            </xsl:when>            
            <xsl:when test=". = 'GetRecord'">
                <xsl:choose>
                    <xsl:when test="/response/result/doc">
                        <GetRecord>
                            <xsl:apply-templates select="/response/result/doc" />                    
                        </GetRecord>                                        
                    </xsl:when>
                    <xsl:otherwise>
                        <error code="idDoesNotExist">
                            <xsl:text>This identifier has the structure of a valid LOC identifier, but it maps to no known item</xsl:text> 
                        </error>	                        
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>            
            <xsl:when test=". = 'ListRecords'">
                <xsl:choose>
                    <xsl:when test="/response/result/doc">
                        <ListRecords>
                            <xsl:apply-templates select="/response/result/doc" />                    
                            <xsl:apply-templates select="../str[@name='verb']" mode="resumption_token" />                            
                        </ListRecords>                                        
                    </xsl:when>
                    <xsl:otherwise>
                        <error code="noRecordsMatch">
                            <xsl:text>NoRecordsMatch - The combination of the values of the from, until, set and metadataPrefix arguments results in an empty list</xsl:text> 
                        </error>	                        
                    </xsl:otherwise>
                </xsl:choose>                    
            </xsl:when>
            <xsl:otherwise>
                <error code="badVerb">
                    <xsl:text>Unrecognised verb</xsl:text> 
                </error>	                                                    
            </xsl:otherwise>            
        </xsl:choose>        
    </xsl:template>
    
    <xsl:template match="arr[@name='dc.creator']/str">
        <dc:creator xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
            xmlns:dc="http://purl.org/dc/elements/1.1/" 
            xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
            xmlns="http://www.openarchives.org/OAI/2.0/"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsl:exclude-result-prefixes="dc oai_dc xsi xd">
            <xsl:value-of select="."/>
        </dc:creator>
    </xsl:template>
        
    <xsl:template match="lst/lst/str[@name='date']">
        <responseDate>
            <xsl:value-of select="replace(., 'z', 'Z')" />
        </responseDate>        
    </xsl:template>

    <xsl:template match="str[@name='verb']" mode="resumption_token">
        <xsl:choose>
            <xsl:when test="/response/result/@numFound > ../str[@name='start'] + ../str[@name='rows']">
                <resumptionToken>
                    <xsl:choose>
                        <xsl:when test="/response/result[@numFound]">
                            <xsl:attribute name="completeListSize" select="/response/result/@numFound" />                    
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="completeListSize" select="'null'" />                                        
                        </xsl:otherwise>
                    </xsl:choose>            
                    <xsl:choose>
                        <xsl:when test="../str[@name='start'] and ../str[@name='rows']">
                            <xsl:attribute name="cursor" select="../str[@name='start']" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="cursor" select="'null'" />
                        </xsl:otherwise>
                    </xsl:choose>                                        
                    <xsl:text>oai_dc:</xsl:text>
                    <xsl:choose>
                        <xsl:when test="../str[@name='from']">
                            <xsl:value-of select="../str[@name='from']" />                                        
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>null</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="../str[@name='until']">
                            <xsl:value-of select="concat(':', ../str[@name='until'])" />                             
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>:null</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>                                        
                    <xsl:choose>
                        <xsl:when test="../str[@name='set'] and ../str[@name='set'] != ''">
                            <xsl:value-of select="concat(':', ../str[@name='set'], ':')" />                             
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>:null:</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>                                                            
                    <xsl:choose>
                        <xsl:when test="../str[@name='start'] and ../str[@name='rows']">
                            <xsl:value-of select="../str[@name='start'] + ../str[@name='rows']" />                            
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>null</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>                    
                </resumptionToken>                        
            </xsl:when>
            <xsl:when test="/response/result/@numFound &lt;= ../str[@name='start'] + ../str[@name='rows']">
                <xsl:choose>
                    <xsl:when test="../str[@name='resumptionToken'] and ../str[@name='resumptionToken'] != '' and ../str[@name='resumptionToken'] != 'null'">
                        <resumptionToken>
                            <xsl:choose>
                                <xsl:when test="/response/result[@numFound]">
                                    <xsl:attribute name="completeListSize" select="/response/result/@numFound" />                    
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:attribute name="completeListSize" select="'null'" />                                        
                                </xsl:otherwise>
                            </xsl:choose>            
                        </resumptionToken>                            
                    </xsl:when>
                </xsl:choose>                
            </xsl:when>                
        </xsl:choose>        
    </xsl:template>
    
    <!-- OpenAIRE Specific START -->
    <xsl:template match="arr[@name='uketdterms.grantnumber']/str" mode="driver">
        <dc:relation xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
            xmlns:dc="http://purl.org/dc/elements/1.1/" 
            xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
            xmlns="http://www.openarchives.org/OAI/2.0/"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsl:exclude-result-prefixes="dc oai_dc xsi xd">
            <xsl:value-of select="." />
        </dc:relation>
    </xsl:template>    
    <!-- OpenAIRE Specific END -->    

</xsl:stylesheet>
