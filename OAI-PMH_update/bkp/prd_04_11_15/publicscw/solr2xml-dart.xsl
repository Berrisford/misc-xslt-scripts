<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:dc="http://purl.org/dc/elements/1.1/" 
    xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
    xmlns="http://www.openarchives.org/OAI/2.0/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"     
    exclude-result-prefixes="xs xd" version="2.0">
    <xsl:include href="/export/home/fedora/publicsolr/publicscw/conf/xslt/solr2citations1.xsl"/>
    <!--  <xsl:include href="solr2citations1.xsl"/>  -->
    <xsl:output media-type="application/xml;charset=UTF-8" indent="no" encoding="UTF-8" method="xml" omit-xml-declaration="no"/>
    <xsl:strip-space elements="*"/>
   
    <xsl:param name="searchurl" select="''" />   
    <xsl:template match="/">
        <xsl:apply-templates select="response" />        
    </xsl:template>
    
    <xsl:template match="response">
        <OAI-PMH xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd">
            <xsl:apply-templates select="lst/lst/str[@name='date']" />
            <xsl:apply-templates select="lst/lst/str[@name='verb']" mode="head" />
            <xsl:choose>
                <xsl:when test="lst/lst/str[@name='verb']">
                    <xsl:apply-templates select="lst/lst/str[@name='verb']" mode="records" />                                                           
                </xsl:when>
                <xsl:otherwise>
                    <error code="badVerb">
                        <xsl:text>Invalid/missing verb</xsl:text> 
                    </error>	                                                    
                </xsl:otherwise>
            </xsl:choose>                
        </OAI-PMH>       
    </xsl:template>
       
    <xsl:template match="doc">
        <record>
            <header>
                <xsl:apply-templates select="str[@name='PID']" mode="head_identifier" />
                <xsl:apply-templates select="date[@name='x.lastmodifieddate']" />
                <setSpec><xsl:value-of select="/response/lst/lst/str[@name='set']" /></setSpec>
            </header>
            <metadata>
                <oai_dc:dc>
                    <xsl:apply-templates select="str[@name='PID']" mode="identifier"/>
                    <xsl:apply-templates select="str[@name='m.title']" />
                    <xsl:apply-templates select="str[@name='m.title.translated']" />
                    <xsl:choose>
                        <xsl:when test="arr[@name='m.name.aut']/str != ''">
                            <xsl:apply-templates select="arr[@name='m.name.aut']/str"/>
                        </xsl:when>
                        <xsl:when test="str[@name='m.note.authors'] != ''">
                            <xsl:apply-templates select="str[@name='m.note.authors']"/>
                        </xsl:when>
                    </xsl:choose>
                    <xsl:apply-templates select="arr[@name='r.hasmainsupervisor.source']/str" />
                    <xsl:apply-templates select="arr[@name='r.hascosupervisor.source']/str" />                            
                    <xsl:apply-templates select="arr[@name='m.name.clb']/str" />
                    <xsl:apply-templates select="str[@name='m.dateissued']" />
                    <xsl:apply-templates select="arr[@name='m.topic']/str" />
                    <xsl:apply-templates select="str[@name='m.publisher']" />
                    <xsl:apply-templates select="str[@name='m.genre.content-type']"/>
                    <xsl:apply-templates select="str[@name='m.genre.form']" />
                    <xsl:apply-templates select="str[@name='f.full-text.source']" />
                    <xsl:apply-templates select="arr[@name='m.typeofresource']/str" />
                    <xsl:apply-templates select="str[@name='m.page.total']" />
                    <xsl:apply-templates select="str[@name='m.languageterm.code']" /> 
                    <xsl:apply-templates select="str[@name='r.isofcontenttype.pid']"/>                            
                    <xsl:apply-templates select="arr[@name='m.preceding.identifier.uri']/str" mode="relation" />
                    <xsl:apply-templates select="str[@name='PID']" mode="relation" />
                    <xsl:apply-templates select="str[@name='m.abstract']" />
                    <xsl:apply-templates select="str[@name='m.note.laymansabstract']" />
                    <xsl:apply-templates select="str[@name='m.note.digitalmaterialnotsubmitted']" />
                    <xsl:apply-templates select="str[@name='m.note.nondigitalmaterialnotsubmitted']" />
                    <xsl:apply-templates select="arr[@name='m.note.general']/str" />                            
                    <xsl:apply-templates select="str[@name='m.otherversion.note']" />                                                        
                </oai_dc:dc>
            </metadata>
        </record>
    </xsl:template>

    <xsl:template match="doc" mode="identifiers">
        <record>
            <header>
                <xsl:apply-templates select="str[@name='PID']" mode="head_identifier" />
                <xsl:apply-templates select="date[@name='x.lastmodifieddate']" />
                <setSpec>thesis</setSpec>
            </header>
        </record>
    </xsl:template>
    
    <xsl:template match="str[@name='r.isofcontenttype.pid']">
        <dc:source>
        <xsl:apply-templates select="." mode="citation"> 
            <xsl:with-param select="'source'" name="format"/> 
        </xsl:apply-templates>
        </dc:source>
    </xsl:template>

    <xsl:template match="str[@name='PID']" mode="pid">
        <xsl:value-of select="."/>
    </xsl:template>
        
    <xsl:template match="str[@name='PID']" mode="identifier">
        <dc:identifier>
            <xsl:value-of select="concat('http://www.manchester.ac.uk/escholar/', .)"/>
        </dc:identifier>
    </xsl:template>
 
    <xsl:template match="str[@name='PID']" mode="head_identifier">
        <identifier>
            <xsl:value-of select="concat('oai:escholar.manchester.ac.uk:', replace(., ':', '-'))" />            
        </identifier>
    </xsl:template>

    <xsl:template match="str[@name='PID']" mode="relation">
        <dc:relation>
            <xsl:value-of select="concat('http://www.manchester.ac.uk/escholar/', .)"/>
        </dc:relation>
    </xsl:template>
            
    <xsl:template match="arr[@name='m.preceding.identifier.uri']/str" mode="relation">
        <dc:relation>
            <xsl:choose>
                <xsl:when test="index-of(., 'www.') &lt; 1">
                    <xsl:value-of select="concat('http://www.', .)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="." />
                </xsl:otherwise>
            </xsl:choose>            
        </dc:relation>
    </xsl:template>
        
    <xsl:template match="str[@name='m.title']">
        <xsl:choose>
            <xsl:when test="../str[@name='m.subtitle'] != ''">
                <dc:title>
                    <xsl:value-of select="concat(., ': ', ../str[@name='m.subtitle'])" />
                </dc:title>
            </xsl:when>
            <xsl:otherwise>
                <dc:title>
                    <xsl:value-of select="."/>
                </dc:title>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
        
    <xsl:template match="str[@name='m.title.translated']">
        <dc:title>
            <xsl:value-of select="." />                    
        </dc:title>
    </xsl:template>    

    <xsl:template match="str[@name='r.isofcontenttype.source']">
        <dc:type>
            <xsl:value-of select="tokenize(., '\|\|')[1]" />            
        </dc:type>
    </xsl:template>    

    <xsl:template match="str[@name='m.genre.form']" >
        <dc:type>
            <xsl:value-of select="." />
        </dc:type>
    </xsl:template>
    
    <xsl:template match="str[@name='m.page.total']" >
        <dc:format>
            <xsl:value-of select="concat(., ' pages')" />
        </dc:format>
    </xsl:template>
    
    <xsl:template match="str[@name='f.full-text.source']">
        <dc:format>
            <xsl:value-of select="tokenize(., '\|\|')[3]" />            
        </dc:format>
    </xsl:template>
    
    <xsl:template match="arr[@name='m.typeofresource']/str">
        <dc:format>
            <xsl:value-of select="." />
        </dc:format>
    </xsl:template>
    
    <xsl:template match="str[@name='m.abstract'] | str[@name='m.note.laymansabstract'] | str[@name='m.note.digitalmaterialnotsubmitted'] | str[@name='m.note.nondigitalmaterialnotsubmitted'] | arr[@name='m.note.general']/str | str[@name='m.otherversion.note']">
        <dc:description>
            <xsl:value-of select="." />
        </dc:description>
    </xsl:template>
        
    <xsl:template match="str[@name='m.dateissued']">
        <dc:date>
            <xsl:choose>
                <xsl:when test="string-length(.) > 10">
                    <xsl:value-of select="substring-before(string(.), 'T')" />            
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="." />
                </xsl:otherwise>
            </xsl:choose>            
        </dc:date>
    </xsl:template>

    <xsl:template match="str[@name='m.genre.content-type']">
        <dc:type>
            <!--            <xsl:value-of select="."/>-->
            <xsl:text>Thesis</xsl:text>
        </dc:type>
    </xsl:template>
    
    <xsl:template match="str[@name='m.languageterm.code']">
        <dc:language>
            <xsl:value-of select="."/>                    
        </dc:language>
    </xsl:template>
    
    <xsl:template match="arr[@name='m.topic']/str">        
        <dc:subject>
            <xsl:value-of select="." />                
        </dc:subject>
    </xsl:template>
    
    <xsl:template match="str[@name='m.publisher']">
        <dc:publisher>
            <xsl:value-of select="."/>
            <xsl:choose>
                <xsl:when test="../str[@name = 'r.isofcontenttype.pid'] = 'uk-ac-man-con:20'">
                    <xsl:apply-templates select="../str[@name='m.placeterm']"/>
                </xsl:when>
            </xsl:choose>
        </dc:publisher>
    </xsl:template>
    
    <xsl:template match="str[@name='m.placeterm']">
        <xsl:value-of select="concat(', ', .)"/>
    </xsl:template>
    
    <xsl:template match="arr[@name='r.hasmainsupervisor.source']/str | arr[@name='r.hascosupervisor.source']/str" >
        <dc:contributor>
            <xsl:value-of select="tokenize(., '\|\|')[1]" />
        </dc:contributor>
    </xsl:template>
  
    <xsl:template match="arr[@name='m.name.clb']/str">
        <dc:contributor>
            <xsl:value-of select="." />
        </dc:contributor>
    </xsl:template>
  
    <xsl:template match="date[@name='x.lastmodifieddate']">
        <datestamp>
            <xsl:value-of select="substring-before(string(.), 'T')" />            
        </datestamp>
    </xsl:template>

    <xsl:template match="str[@name='verb']" mode="head">
        <request xmlns:dc="http://purl.org/dc/elements/1.1/">
            <xsl:attribute name="verb">
                <xsl:value-of select="." />        
            </xsl:attribute>
            <xsl:choose>
                <xsl:when test=". = 'GetRecord'">
                    <xsl:attribute name="identifier" select="concat('oai:escholar.manchester.ac.uk:', replace(/result/doc/str[@name='PID'], ':', '-'))" />
                    <xsl:attribute name="metadataPrefix" select="'oai_dc'"/>
                </xsl:when>
                <xsl:when test=". != 'GetRecord'">
                    <xsl:choose>                    
                        <xsl:when test="../str[@name='resumptionToken'] and ../str[@name='resumptionToken'] != '' and ../str[@name='resumptionToken'] != 'null'">
                            <xsl:attribute name="resumptionToken" select="../str[@name='resumptionToken']" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="metadataPrefix" select="'oai_dc'"/>
                            <xsl:attribute name="set" select="../str[@name='set']"/>
                            <xsl:attribute name="from" select="../str[@name='from']"/>
                            <xsl:attribute name="until" select="../str[@name='until']"/>                                        
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>                    
            </xsl:choose>
            <xsl:value-of select="'http://manchester.ac.uk/escholar/api/oai2'"/>
        </request>
    </xsl:template>
    
    <xsl:template match="str[@name='verb']" mode="records">
        <xsl:choose>
            <xsl:when test=". = 'ListIdentifiers'">
                <xsl:choose>
                    <xsl:when test="/result/doc and /result/doc/str[@name='r.isofcontenttype.pid'] = 'uk-ac-man-con:20'">
                        <ListIdentifiers>
                            <xsl:apply-templates select="../../../result/doc" mode="identifiers" />                    
                            <xsl:apply-templates select="../str[@name='verb']" mode="resumption_token" />                            
                        </ListIdentifiers>                                        
                    </xsl:when>
                    <xsl:otherwise>
                        <error code="noRecordsMatch">
                            <xsl:text>NoRecordsMatch - The combination of the values of the from, until, set and metadataPrefix arguments results in an empty list</xsl:text> 
                        </error>	                        
                    </xsl:otherwise>
                </xsl:choose>                
            </xsl:when>            
            <xsl:when test=". = 'GetRecord'">
                <xsl:choose>
                    <xsl:when test="../../../result/doc and ../../../result/doc/str[@name='r.isofcontenttype.pid'] = 'uk-ac-man-con:20'">
                        <GetRecord>
                            <xsl:apply-templates select="../../../result/doc" />                    
                        </GetRecord>                                        
                    </xsl:when>
                    <xsl:otherwise>
                        <error code="idDoesNotExist">
                            <xsl:text>This identifier has the structure of a valid LOC identifier, but it maps to no known item</xsl:text> 
                        </error>	                        
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>            
            <xsl:when test=". = 'ListRecords'">
                <xsl:choose>
                    <xsl:when test="../../../result/doc and ../../../result/doc/str[@name='r.isofcontenttype.pid'] = 'uk-ac-man-con:20'">
                        <ListRecords>
                            <xsl:apply-templates select="../../../result/doc" />                    
                            <xsl:apply-templates select="../str[@name='verb']" mode="resumption_token" />                            
                        </ListRecords>                                        
                    </xsl:when>
                    <xsl:otherwise>
                        <error code="noRecordsMatch">
                            <xsl:text>NoRecordsMatch - The combination of the values of the from, until, set and metadataPrefix arguments results in an empty list</xsl:text> 
                        </error>	                        
                    </xsl:otherwise>
                </xsl:choose>                    
            </xsl:when>
            <xsl:otherwise>
                <error code="badVerb">
                    <xsl:text>Unrecognised verb</xsl:text> 
                </error>	                                                    
            </xsl:otherwise>            
        </xsl:choose>        
    </xsl:template>
    
    <xsl:template match="arr[@name='m.name.aut']/str | str[@name='m.note.authors']">
        <dc:creator>
            <xsl:value-of select="."/>
        </dc:creator>
    </xsl:template>
                   
    <xsl:template match="lst/lst/str[@name='date']">
        <responseDate>
            <xsl:value-of select="replace(., 'z', 'Z')" />
        </responseDate>        
    </xsl:template>

    <xsl:template match="str[@name='verb']" mode="resumption_token">
        <xsl:choose>
            <xsl:when test="../../../result/@numFound > ../../../lst/lst/str[@name='start'] + ../../../lst/lst/str[@name='rows']">
                <resumptionToken>
                    <xsl:choose>
                        <xsl:when test="../../../result[@numFound]">
                            <xsl:attribute name="completeListSize" select="../../../result/@numFound" />                    
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="completeListSize" select="'null'" />                                        
                        </xsl:otherwise>
                    </xsl:choose>            
                    <xsl:choose>
                        <xsl:when test="../../../lst/lst/str[@name='start'] and ../../../lst/lst/str[@name='rows']">
                            <xsl:attribute name="cursor" select="../../../lst/lst/str[@name='start']" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="cursor" select="'null'" />
                        </xsl:otherwise>
                    </xsl:choose>                                        
                    <xsl:text>oai_dc:</xsl:text>
                    <xsl:choose>
                        <xsl:when test="../../../lst/lst/str[@name='from']">
                            <xsl:value-of select="../../../lst/lst/str[@name='from']" />                                        
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>null</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="../../../lst/lst/str[@name='until']">
                            <xsl:value-of select="concat(':', ../../../lst/lst/str[@name='until'])" />                             
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>:null</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>                                        
                    <xsl:value-of select="concat(':', 'thesis', ':')" />
                    <xsl:choose>
                        <xsl:when test="../../../lst/lst/str[@name='start'] and ../../../lst/lst/str[@name='rows']">
                            <xsl:value-of select="../../../lst/lst/str[@name='start'] + ../../../lst/lst/str[@name='rows']" />                            
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>null</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>                    
                </resumptionToken>                        
            </xsl:when>
            <xsl:when test="../../../result/@numFound &lt;= ../../../lst/lst/str[@name='start'] + ../../../lst/lst/str[@name='rows']">
                <xsl:choose>
                    <xsl:when test="../str[@name='resumptionToken'] and ../str[@name='resumptionToken'] != '' and ../str[@name='resumptionToken'] != 'null'">
                        <resumptionToken>
                            <xsl:choose>
                                <xsl:when test="../../../result[@numFound]">
                                    <xsl:attribute name="completeListSize" select="../../../result/@numFound" />                    
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:attribute name="completeListSize" select="'null'" />                                        
                                </xsl:otherwise>
                            </xsl:choose>            
                        </resumptionToken>                            
                    </xsl:when>
                </xsl:choose>                
            </xsl:when>                
        </xsl:choose>        
    </xsl:template>
    
</xsl:stylesheet>