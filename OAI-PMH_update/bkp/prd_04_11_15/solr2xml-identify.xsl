<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
	<xsl:output media-type="application/json;charset=UTF-8" encoding="UTF-8" method="xml" indent="yes" omit-xml-declaration="no"/>
	<xsl:strip-space elements="*"/>
	
	<xsl:template match="/">
		<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" 
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/
			http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
			<responseDate><xsl:value-of select="/response/lst/lst/str[@name='date']"/></responseDate>
			<request verb="Identify">http://www.manchester.ac.uk/escholar/api/oai2</request>
			<Identify>
				<repositoryName>Manchester eScholar - The University of Manchester's institutional repository</repositoryName><!--MUST-->
				<baseURL>http://www.manchester.ac.uk/escholar/api/oai2</baseURL><!-- ?Redirect -->
				<protocolVersion>2.0</protocolVersion>
				<adminEmail>escholar@manchester.ac.uk</adminEmail>
				<earliestDatestamp>2009-01-01</earliestDatestamp>
				<deletedRecord>persistent</deletedRecord>
				<granularity>YYYY-MM-DD</granularity>
				<compression>identity</compression><!-- NOT_MUST -->
				<description><!-- NOT_MUST -->
					<oai-identifier 
						xmlns="http://www.openarchives.org/OAI/2.0/oai-identifier"
						xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
						xsi:schemaLocation=
						"http://www.openarchives.org/OAI/2.0/oai-identifier
						http://www.openarchives.org/OAI/2.0/oai-identifier.xsd">
						<scheme>oai</scheme>
						<repositoryIdentifier>escholar.manchester.ac.uk</repositoryIdentifier>
						<delimiter>:</delimiter>
						<sampleIdentifier>oai:escholar.manchester.ac.uk:uk-ac-man-scw-123</sampleIdentifier>
					</oai-identifier>
				</description>
			</Identify>
		</OAI-PMH>
	</xsl:template>
</xsl:stylesheet>
