<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
	exclude-result-prefixes="xs xd"
	version="2.0">
	<xsl:output indent="yes"/>
	<xsl:strip-space elements="*"/>

	<xsl:template match="/request/action[@id='addPublication']">
		<reply>
			<response id="permit"/>
		</reply>
		<xsl:apply-templates/>
	</xsl:template>
<!--	
	<xsl:template match="actions/action">
		<reply>
			<response id="permit"/>    
		</reply>        		
	</xsl:template>
-->	
	<!--
	<xsl:template match="actions/action[@id = 'addPublication']">
		<reply>
			<response id="permit"/>    
		</reply>        		
	</xsl:template>
	<xsl:template match="actions/action[@id != 'addPublication']">
		<reply>
			<response id="deny"/>    
		</reply>        		
	</xsl:template>
	-->
	<!--
	<xsl:template match="request/action[@id=''] | request/action[@id!='actionId']">
		<reply xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="reply.xsd">
			<response id="deny"/>    
		</reply>        		
	</xsl:template>
-->
</xsl:stylesheet>