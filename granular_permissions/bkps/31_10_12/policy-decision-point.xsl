<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="xs" version="3.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output exclude-result-prefixes="#all" indent="yes" />
	<xsl:strip-space elements="*" />
	<xsl:variable name="policies" select="doc('policy.xml')" />
<!-- 	* Template matching root of request XML 
		* Gets action/@id into parameter action, this parameter tunnels into other templates 
		* Processes rules in policy.xml that match request node with same action/@id places rule responses in variable responses 
		* For each matched rule it gets subject and resource attributes using the request template (NB: this template checks there is a matching action)
		* Processes $responses using responses template -->
	<xsl:template match="/">
		<xsl:param name="action" select="request/action/@id" tunnel="yes" />
		<xsl:variable name="responses">
			<responses>
				<xsl:apply-templates mode="policy" select="$policies/policy/request[actions/action/@id=$action]/rules/rule">
					<xsl:with-param name="attributes" tunnel="yes">
						<xsl:apply-templates select="request[action/@id=$policies/policy/request/actions/action/@id]" />
					</xsl:with-param>
				</xsl:apply-templates>
			</responses>
		</xsl:variable>
		<xsl:apply-templates select="$responses/responses"/>
	</xsl:template>
<!--	* Returns permit if at least one reponse element exists and all response elements have attribut @id='permit' -->
	<xsl:template match="responses[exists(response)][count(response)=count(response[@id='permit'])]">
		<response id="permit" />
	</xsl:template>
<!--	* Returns deny if no reponse elements or if one reponse element has attribute @id='deny' -->
	<xsl:template match="responses[not(exists(response))]|responses[exists(response[@id='deny'])]">
		<response id="deny" />
	</xsl:template>
	<xsl:template match="request">
		<xsl:apply-templates select="subject,resource">
			<xsl:with-param name="action" select="action/@id" />
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="subject[@id!='']|resource[@id!='']">
		<xsl:param name="action" />
		<xsl:apply-templates mode="policy" select="$policies/policy/request[actions/action/@id=$action]//*">
			<xsl:with-param name="id" select="@id" />
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="subject[@name!='']|resource[@name!='']" mode="policy">
		<xsl:param name="id" />
		<xsl:element name="{@name}">
			<xsl:apply-templates mode="#current" select="uri">
				<xsl:with-param name="id" select="replace($id,':','%5C:')" />
			</xsl:apply-templates>
		</xsl:element>
	</xsl:template>
	<xsl:template match="uri" mode="policy">
		<xsl:param name="id" />
		<xsl:variable name="uri" select="replace(.,'%%id%%',$id)" />
		<xsl:if test="doc-available($uri)">
			<xsl:element name="{@name}">
				<xsl:copy-of select="doc($uri)/response/result" />
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<xsl:template match="rule" mode="policy">
		<xsl:param name="attributes" tunnel="yes" />
		<xsl:variable as="xs:boolean" name="evaluate">
			<xsl:evaluate xpath="concat('if (',xpath,') then true() else false()')">
				<xsl:with-param name="attributes">
					<xsl:copy-of select="$attributes" />
				</xsl:with-param>
			</xsl:evaluate>
		</xsl:variable>
		<xsl:if test="$evaluate">
			<xsl:element name="response">
				<xsl:attribute name="rule" select="@id" />
				<xsl:attribute name="id" select="response/@id" />
			</xsl:element>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
