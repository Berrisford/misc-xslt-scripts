<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	exclude-result-prefixes="xs"
	version="3.0">
	<xsl:output indent="yes"/>
	<xsl:variable name="policies" select="doc('policies.xml')"/>
	<xsl:template match="/">
		<xsl:variable name="resource" select="doc(request/resource)"/>
		<xsl:variable name="action" select="request/action"/>
		<xsl:variable name="rule" select="$policies/policies/policy/rule[action=$action]"/>
		<xsl:variable name="evaluate" as="xs:boolean">
			<xsl:evaluate xpath="$rule/xpath">
				<xsl:with-param name="resource-attributes">
					<xsl:copy-of select="$resource"/>
				</xsl:with-param>
			</xsl:evaluate>
		</xsl:variable>
		<xsl:element name="result">
			<xsl:attribute name="rule" select="$rule/@id"/>
		<xsl:choose>
			<xsl:when test="$evaluate">
					<xsl:value-of select="$rule/result"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>indetermined</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>