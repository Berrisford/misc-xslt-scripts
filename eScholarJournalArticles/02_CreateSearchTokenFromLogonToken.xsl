<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="xs" version="2.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:ser="http://serialssolutions.com/" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" exclude-result-prefixes="xs xsl" cdata-section-elements="ser:data"/>
	<xsl:template match="/">
		<soapenv:Envelope>
			<soapenv:Header />
			<soapenv:Body>
				<ser:DataExchange>
					<ser:data><![CDATA[ 
 <ApiCallData> 
 <Version>1</Version> 
 <Api>RequestReportToken</Api> 
 <Token>3ce34c4f-4587-4b4b-8e23-349ae68b779</Token> 
 <RequestReportToken> 
 <FilterActive>True</FilterActive> 
 <FilterAcademicScholarly>True</FilterAcademicScholarly> 
 <FilterRefereed>False</FilterRefereed>
 <FilterElectronicEdition>False</FilterElectronicEdition> 
 <Version>1</Version> 
 </RequestReportToken> 
 </ApiCallData>]]>
					</ser:data>
				</ser:DataExchange>
			</soapenv:Body>
		</soapenv:Envelope>
	</xsl:template>
</xsl:stylesheet>
