<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	exclude-result-prefixes="xs"
	version="2.0">
	<xsl:output method="text" indent="yes"/>
	<xsl:param name="login" select="'9d80e8f5-a192-4c5a-8ead-b8312d0194ef'"/>
	<xsl:param name="report" select="'207876d4-3cdb-4473-8a7e-e01b631b0214'"/>
	<xsl:param name="start" select="290"/>
	<xsl:param name="end" select="382"/>
	<xsl:template match="/">
		<xsl:for-each select="$start to $end">
			<xsl:variable name="soapsearch-filename" select="concat('soap-search-page',.,'.xml')"/>
			<xsl:text>wget --cookies=on --load-cookies=cookies.txt --keep-session-cookies --save-cookies=cookies.txt http://ulrichsds.serialssolutions.com/UlrichsWebService.asmx --post-file=soap-search-page</xsl:text><xsl:value-of select="."/><xsl:text>.xml --header="Content-Type: text/xml" --output-document=response-page</xsl:text><xsl:value-of select="."/><xsl:text>.xml</xsl:text>
			<xsl:text>&#10;&#13;</xsl:text>
			<xsl:result-document href="{$soapsearch-filename}" method="xml" indent="yes">
				<soapenv:Envelope 
					xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
					xmlns:ser="http://serialssolutions.com/"> 
					<soapenv:Header/> 
					<soapenv:Body> 
						<ser:DataExchange> 
							<ser:data>
								<xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
								<ApiCallData> 
									<Version>1</Version> 
									<Api>RequestPagedData</Api> 
									<Token>
										<xsl:value-of select="$login"/>
									</Token> 
									<RequestPagedData> 
										<ReportToken>
											<xsl:value-of select="$report"/>
										</ReportToken> 
										<PageNumber>
											<xsl:value-of select="."/>
										</PageNumber>
										<Version>1</Version> 
									</RequestPagedData> 
								</ApiCallData>
								<xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
							</ser:data> 
						</ser:DataExchange> 
					</soapenv:Body> 
				</soapenv:Envelope>
			</xsl:result-document>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>