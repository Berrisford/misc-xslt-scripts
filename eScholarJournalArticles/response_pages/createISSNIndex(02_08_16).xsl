<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
    exclude-result-prefixes="xs math soap"
    version="3.0">

    <!-- BA: edit to match last page required -->
    <!--<xsl:variable name="end_page" select="393"/>-->
    <xsl:variable name="end_page" select="393"/>
    
    <xsl:template match="/">        
            <!-- BA: edit to match first page required -->
            <xsl:variable name="start_page" select="1"/>
            
            <xsl:result-document href="issn-index2.xml" method="xml" indent="yes">
                <Journals>
                    <xsl:call-template name="iterate">
                        <xsl:with-param name="count" select="$start_page"></xsl:with-param>
                        <xsl:with-param name="file" select="concat('response-page', $start_page, '.xml')"/>
                    </xsl:call-template>
                </Journals>
            </xsl:result-document>                        

    </xsl:template>
    
    <xsl:template name="iterate">
        <xsl:param name="count"/>
        <xsl:param name="file"/>        
<!--        
        <xsl:variable name="file">
            <xsl:value-of select="concat('response-page', $count, '.xml')" />
        </xsl:variable>
-->
        <xsl:variable name="response_doc">
            <xsl:copy-of select="doc($file)"/>
            <!--<xsl:copy-of select="doc($file)/soap:Envelope/soap:Body/DataExchangeResponse/DataExchangeResult/ReturnValueData/UlrichsDataRecords"/>-->
            <!--<xsl:copy-of select="doc('response-page1.xml')"/>-->
        </xsl:variable>
        
        <xsl:call-template name="record">
            <xsl:with-param name="response_doc" select="$response_doc"/>
        </xsl:call-template>
        
        <xsl:if test="$count &lt; $end_page">
            <xsl:call-template name="iterate">
                <xsl:with-param name="count" select="$count + 1"/>
                <xsl:with-param name="file" select="concat('response-page', $count + 1, '.xml')"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
     
    <xsl:template name="record">
        <xsl:param name="response_doc"/>
        <xsl:for-each select="$response_doc/soap:Envelope/soap:Body/DataExchangeResponse/DataExchangeResult/ReturnValueData/UlrichsDataRecords/Record" xpath-default-namespace="http://serialssolutions.com/">
            <!--<xsl:for-each select="$response_doc/soap:Envelope/soap:Body/DataExchangeResponse/DataExchangeResult/ReturnValueData/UlrichsDataRecords/Record">-->
            <!--<record>test</record>-->
            <!--<xsl:if test="AlternateEditionSet/AlternateEdition/Media = 'Online'">-->
                <xsl:for-each select="AlternateEditionSet/AlternateEdition[Media='Online' and ISSN]">
                    <!--<xsl:if test="Media = 'Online' and ISSN">-->
                    <Journal xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                        xsi:noNamespaceSchemaLocation="issn-index-schema.xsd">
                            <Title>
                                <xsl:value-of select="../../Title"></xsl:value-of>
                            </Title>
                            <KeyTitle>
                                <xsl:value-of select="../../KeyTitle"/>
                            </KeyTitle>
                            <ISSN>
                                <xsl:value-of select="../../ISSN"/>
                            </ISSN>
                            <e-ISSN>
                                <xsl:value-of select="ISSN"/>
                            </e-ISSN>
                            <LCClass>
                                <xsl:value-of select="LCClass"/>
                            </LCClass>
                            <DeweyDecimal-0>
                                <xsl:value-of select="normalize-space(../../DeweyDecimalSet/DeweyDecimal[@id='0'])"/>
                            </DeweyDecimal-0>
                            <DeweyDecimal-1>
                                <xsl:value-of select="normalize-space(../../DeweyDecimalSet/DeweyDecimal[@id='1'])"/>
                            </DeweyDecimal-1>
                            <DeweyDecimal-2>
                                <xsl:value-of select="normalize-space(../../DeweyDecimalSet/DeweyDecimal[@id='2'])"/>
                            </DeweyDecimal-2>
                            <DeweyDecimal-3>
                                <xsl:value-of select="normalize-space(../../DeweyDecimalSet/DeweyDecimal[@id='3'])"/>
                            </DeweyDecimal-3>
                            <DeweyDecimal-4>
                                <xsl:value-of select="normalize-space(../../DeweyDecimalSet/DeweyDecimal[@id='4'])"/>
                            </DeweyDecimal-4>
                            <DeweyDecimal-5>
                                <xsl:value-of select="normalize-space(../../DeweyDecimalSet/DeweyDecimal[@id='5'])"/>
                            </DeweyDecimal-5>
                        </Journal>                        
                    <!--</xsl:if>-->
                </xsl:for-each>
            <!--</xsl:if>-->            
        </xsl:for-each>
    </xsl:template> 
     
</xsl:stylesheet>