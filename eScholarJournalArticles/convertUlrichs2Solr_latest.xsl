<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	exclude-result-prefixes="xs"
	version="2.0">
	<xsl:output method="xml" encoding="utf-8" indent="yes"/>
	<xsl:strip-space elements="*"/>		
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="Records">
		<add>
			<xsl:apply-templates select="Record[DocumentType/Type='JL' or DocumentType/Type='PR' or contains(PublisherInfo/Name, 'Elsevier')]"/>
		</add>
	</xsl:template>
	<xsl:template match="Record">
		<doc>
			<xsl:apply-templates/>
			<xsl:element name="field">
				<xsl:attribute name="name" select="'allfields'"/>
				<xsl:apply-templates mode="allfields"/>
			</xsl:element>
		</doc>
	</xsl:template>
	<xsl:template match="AbbreviatedTitleSet
		| VariantTitleSet
		| PublisherInfo
		| CountryCode">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="SERIAL_UID" priority="10">
		<xsl:element name="field">
			<xsl:attribute name="name" select="'PID'"/>
			<xsl:value-of select="concat('uk-ac-man-jrn:',.)"/>
		</xsl:element>
		<xsl:next-match/>
	</xsl:template>
	<xsl:template match="AbbreviatedTitleSet/AbbreviatedTitle[1] | MedlineAbbreviation[not(exists(../AbbreviatedTitleSet/AbbreviatedTitle))]">
		<xsl:element name="field">
			<xsl:attribute name="name" select="'j.title.abbreviated'"/>
			<xsl:value-of select="normalize-space(.)"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="VariantTitleSet/VariantTitle[1]">
		<xsl:element name="field">
			<xsl:attribute name="name" select="'j.title.alternative'"/>
			<xsl:value-of select="normalize-space(.)"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="PublisherInfo/Name | ParentPublisher[not(exists(../PublisherInfo/Name))]  | CorpAuthorPublisherSponsor[not(exists(../ParentPublisher))][not(exists(../PublisherInfo/Name))]">
		<xsl:element name="field">
			<xsl:attribute name="name" select="'j.publisher'"/>
			<xsl:value-of select="normalize-space(.)"/>
		</xsl:element>
		<xsl:element name="field">
			<xsl:attribute name="name" select="'j.publisher.az'"/>
			<xsl:value-of select="lower-case(substring(normalize-space(.),1,1))"/>
		</xsl:element>		
	</xsl:template>
	<xsl:template match="PublisherInfo/Country | CountryCode[not(exists(../PublisherInfo/Country))]/Name">
		<xsl:element name="field">
			<xsl:attribute name="name" select="'j.publisher.country'"/>
			<xsl:value-of select="normalize-space(.)"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="PublisherInfo/Website1">
		<xsl:element name="field">
			<xsl:attribute name="name" select="'j.website.publisher'"/>
			<xsl:value-of select="normalize-space(.)"/>
		</xsl:element>
	</xsl:template>	
	<xsl:template match="JournalWebsiteURL">
		<xsl:element name="field">
			<xsl:attribute name="name" select="'j.website.journal'"/>
			<xsl:value-of select="normalize-space(.)"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Title | ISSN | SERIAL_UID | StatusCode" priority="5">
		<xsl:element name="field">
			<xsl:attribute name="name" select="concat('j.',lower-case(name()))"/>
			<xsl:value-of select="normalize-space(.)"/>
		</xsl:element>
		<xsl:next-match/>
	</xsl:template>
	<xsl:template match="Title" priority="2">
		<xsl:element name="field">
			<xsl:attribute name="name" select="'j.title.autocomplete'"/>
			<xsl:value-of select="replace(normalize-space(.),' ','_')"/>
		</xsl:element>
		<xsl:element name="field">
			<xsl:attribute name="name" select="'j.title.az'"/>
			<xsl:value-of select="lower-case(substring(normalize-space(.),1,1))"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="PublisherInfo
		| AbbreviatedTitleSet" mode="allfields">
		<xsl:apply-templates mode="#current"/>
	</xsl:template>
	<xsl:template match="Title
		| ISSN
		| PublisherInfo/Name
		| ParentPublisher[not(exists(../PublisherInfo/Name))]
		| CorpAuthorPublisherSponsor[not(exists(../ParentPublisher))][not(exists(../PublisherInfo/Name))]
		| AbbreviatedTitleSet/AbbreviatedTitle[1] 
		| MedlineAbbreviation[not(exists(../AbbreviatedTitleSet/AbbreviatedTitle))]" mode="allfields" priority="10">
		<xsl:for-each select="text()">		
			<xsl:value-of select="normalize-space(.)"/>
			<xsl:text> </xsl:text>
		</xsl:for-each>
		<xsl:next-match/>
	</xsl:template>
	<xsl:template match="Title" mode="allfields" priority="5">
		<xsl:value-of select="substring(replace(normalize-space(.),' ',''),1,10)"/>
		<xsl:text> </xsl:text>
		<xsl:next-match/>
	</xsl:template>
	<xsl:template match="ISSN" mode="allfields" priority="5">
		<xsl:value-of select="replace(.,'-','')"/>
		<xsl:text> </xsl:text>
	</xsl:template>
	<xsl:template match="*|@*" mode="allfields"/>
	<xsl:template match="*|@*"/>
</xsl:stylesheet>