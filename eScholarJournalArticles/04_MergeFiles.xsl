<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
	exclude-result-prefixes="xs xsd xsi soap"
	version="2.0">
	<xsl:output indent="yes" method="xml" encoding="UTF-8"/>
	<xsl:strip-space elements="*"/>
	<xsl:param name="start" select="1" as="xs:integer"/>
	<xsl:param name="rows" select="393" as="xs:integer"/>
	<xsl:template match="/">
		<Records>
		<xsl:for-each select="$start to ($start + $rows - 1)">
			<xsl:apply-templates select="doc(concat('response-page',.,'.xml'))/soap:Envelope/soap:Body"/>
		</xsl:for-each>
		</Records>
	</xsl:template>
	<xsl:template match="soap:Body">
		<xsl:apply-templates select="DataExchangeResponse/DataExchangeResult/ReturnValueData/UlrichsDataRecords/*" xpath-default-namespace="http://serialssolutions.com/"/>
	</xsl:template>
	<xsl:template match="*" xpath-default-namespace="http://serialssolutions.com/">
		<xsl:element name="{name()}">
			<xsl:apply-templates select="*|@*"/>
			<xsl:value-of select="normalize-space(text())"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*">
		<xsl:attribute name="{name()}">
			<xsl:value-of select="."/>
		</xsl:attribute>
	</xsl:template>
	<xsl:template match="text()"/>
<!--	<xsl:template match="text()">
		<xsl:value-of select="."/>
	</xsl:template>-->
</xsl:stylesheet>