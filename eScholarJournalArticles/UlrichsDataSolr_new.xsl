<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output indent="yes" method="xml"/>
    
    <xsl:variable name="root_url" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/journal/select/'"></xsl:variable>

    <xsl:template match="/">
        <add>
        <xsl:apply-templates select="add" />
        </add>
    </xsl:template>
    
    <xsl:template match="add">
        <xsl:for-each select="doc">
            <xsl:variable name="pid" select="field[@name='PID']" />
            <xsl:variable name="url" select="concat($root_url, '?q=PID%3A', replace($pid, ':', '?'), '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>

            <xsl:choose>
                <xsl:when test="not(doc(concat($root_url, '?q=PID%3A', replace($pid, ':', '?'), '&amp;fl=PID&amp;omitHeader=true'))/response/result/doc)">
                    <doc>
                        <xsl:apply-templates/>
                    </doc>
                </xsl:when>
<!--
                <xsl:otherwise>
                    <doc2>
                        <!-\-<url><xsl:value-of select="$url" /></url>-\->
                        <xsl:apply-templates/>
                    </doc2>
                </xsl:otherwise>
-->                
            </xsl:choose>                        
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="*" >
        <xsl:element name="{name()}" >
            <xsl:apply-templates select ="@*"/> 
            <xsl:apply-templates/>
        </xsl:element> 
    </xsl:template> 
    
    <xsl:template match="@*" >
        <xsl:attribute name="{name()}" >
            <xsl:value-of select ="."/> 
        </xsl:attribute> 
    </xsl:template> 
    
    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>

</xsl:stylesheet>