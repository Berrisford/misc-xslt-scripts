<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	exclude-result-prefixes="xs"
	version="2.0">
	<xsl:output method="xml" encoding="utf-8" indent="yes"/>
	<xsl:strip-space elements="*"/>		
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="Records">
		<add>
			<xsl:apply-templates select="Record[DocumentType/Type='JL' or DocumentType/Type='PR' or contains(PublisherInfo/Name, 'Elsevier')]"/>
			<!--<xsl:apply-templates select="Record"/>-->
		</add>
	</xsl:template>
	<xsl:template match="Record">
		<doc>
			<xsl:apply-templates select="Title"/>
			<xsl:apply-templates select="VariantTitleSet/VariantTitle"/>
			<xsl:apply-templates select="ISSN"/>
			<xsl:apply-templates select="SubjectSet/Subject"/>
		</doc>
	</xsl:template>
	<xsl:template match="AbbreviatedTitleSet | VariantTitleSet">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="AbbreviatedTitleSet/AbbreviatedTitle[1] | MedlineAbbreviation[not(exists(../AbbreviatedTitleSet/AbbreviatedTitle))]">
		<xsl:element name="field">
			<xsl:attribute name="name" select="'j.title.abbreviated'"/>
			<xsl:value-of select="normalize-space(.)"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="VariantTitle">
		<alt-title>
			<xsl:value-of select="normalize-space(.)"/>			
		</alt-title>
	</xsl:template>
	<xsl:template match="SubjectSet/Subject">
		<subject>
			<xsl:value-of select="normalize-space(.)"></xsl:value-of>
		</subject>
	</xsl:template>
	<xsl:template match="Title">
		<title>
			<xsl:value-of select="normalize-space(.)"/>
		</title>
	</xsl:template>
	<xsl:template match="ISSN">
		<issn>
			<xsl:value-of select="normalize-space(.)"/>
		</issn>
	</xsl:template>
	<xsl:template match="*|@*"/>
</xsl:stylesheet>