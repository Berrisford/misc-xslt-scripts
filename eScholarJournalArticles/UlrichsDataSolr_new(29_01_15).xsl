<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output indent="yes" method="xml" omit-xml-declaration="yes"/>
    
<!--    <xsl:variable name="root_url" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/journal/select/?fl=PID&amp;omitHeader=true&amp;'"></xsl:variable>
-->
    <xsl:variable name="root_url" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/journal/select/'"></xsl:variable>


    <xsl:template match="/">
        <root>
        <xsl:apply-templates select="add" />
        </root>
    </xsl:template>
    
    <xsl:template match="add">
        <xsl:for-each select="doc">
            <item>
            <xsl:variable name="pid" select="field[@name='PID']" />
                <xsl:variable name="url" select="concat($root_url, '?q=PID%3A', replace($pid, ':', '?'), '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>
            <url>
                <xsl:value-of select="$url"></xsl:value-of>
            </url>
            <pid>
                <xsl:value-of select="$pid" />
            </pid>
            <exists>
                <xsl:choose>
                    <xsl:when test="not(doc-available(concat($root_url, '?q=PID%3A', replace($pid, ':', '?'))))">
                        NEW
                    </xsl:when>
                    <xsl:otherwise>
                        exists
                    </xsl:otherwise>
                </xsl:choose>
            </exists>
            </item>
        </xsl:for-each>
    </xsl:template>
    
</xsl:stylesheet>