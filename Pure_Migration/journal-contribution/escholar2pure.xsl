<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">    
    <xsl:output indent="yes" method="xml"/>
    
    <!-- BA: This script iterates across all records (of a particular content type) in chunks of 1000 (rows) and checks to see if the record -->
    <!--  has been matched by Scopus. If not, it generates the requisite Pure xml entirely from eScolar data. Change $rows, $num_found, $obj_files -->
    <!--  accordingly to test quickly with small sets of data. Change $file_path to match where your files are located. -->
    
    <xsl:variable name="root_url" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/scw/select/'"></xsl:variable>
    <xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con%5C%3A17+AND+r.isbelongsto.pid:uk*', '&amp;fl=PID,r.isbelongsto.pid,x.state&amp;omitHeader=true')"></xsl:variable>
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?17', '&amp;fl=PID,r.isbelongsto.pid,x.state&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?17', '&amp;fl=PID&amp;omitHeader=true&amp;start=0&amp;rows=0')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+m.preceding.source:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+r.hasauthor.source:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+m.host.title.abbreviated:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+m.genre.status:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+m.title.translated:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+m.note.authors:%5B*+TO+*%5D', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=PID:', 'uk-ac-man-scw%5C%3A71283Berris', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=PID%3Auk-ac-man-scw%5C%3A202423&amp;fl=PID,r.isbelongsto.pid,x.state&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3Auk-ac-man-con%5C%3A17+AND+%28r.isbelongsto.pid%3Auk-ac-man-per%5C%3A109461+OR+r.isbelongsto.pid%3Auk-ac-man-per%5C%3A48079%29&amp;fl=PID,r.isbelongsto.pid,x.state&amp;omitHeader=true&amp;start=0')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid:', 'uk-ac-man-con%5C%3A17+AND+f.fileattached.source:%5B*+TO+*%5D+AND+PID%3Auk-ac-man-scw%5C%3A10*', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
    <!--<xsl:variable name="url" select="concat($root_url, '?q=PID:', 'uk-ac-man-scw%5C%3A267239', '&amp;fl=PID,r.isbelongsto.pid,x.state&amp;omitHeader=true')"></xsl:variable>-->
    
    <xsl:variable name="rows" select="1000"/>
    <xsl:variable name="num_found">
        <xsl:value-of select="doc(concat($url, '&amp;start=0&amp;rows=0'))/response/result/@numFound"/>
        <!--<xsl:value-of select="doc(concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?17', '&amp;fl=PID&amp;omitHeader=true&amp;start=0&amp;rows=0'))/response/result/@numFound"/>-->
        <!--<xsl:value-of select="1000"/>-->
    </xsl:variable>
    
    <!-- Retrieve the file document here. We can then just check this to see if a selected eScholar pid is in the document -->
    <xsl:variable name="file_path" select="'C:/PureLegacyImport/uom_output/journal-contribution/'"/>
    <xsl:variable name="obj_files">
        <xsl:copy-of select="doc('files.xml')"/>
    </xsl:variable>    

    <xsl:variable name="obj_structured_keywords">
        <xsl:copy-of select="doc('structured-keywords.xml')/keywords"/>
    </xsl:variable>
    
    <xsl:variable name="obj_open_licences">
        <xsl:copy-of select="doc('open-licences.xml')/licences"/>
    </xsl:variable>

    <xsl:variable name="node_names">
        <node-names>
            <node-name role="author">r.isbelongsto.source</node-name>
            <node-name role="author">r.hasauthor.source</node-name>
            <node-name role="author">r.hascoauthor.source</node-name>
            <node-name role="author">r.hascorrespondingauthor.source</node-name>
            <node-name role="author">r.hascoseniorauthor.source</node-name>
            <node-name role="author">r.hasfirstauthor.source</node-name>
            <node-name role="author">r.haslastauthor.source</node-name>
            <node-name role="author">r.hasseniorauthor.source</node-name>
        </node-names>
    </xsl:variable>    

    <xsl:variable name="excluded_pids">
        <excluded_pids>
            <pid id="uk-ac-man-per:109461">Berrisford|uk-ac-man-per:109461</pid>
            <pid id="uk-ac-man-per:58968">Nilani|uk-ac-man-per:58968</pid>
            <pid id="uk-ac-man-per:129553">Tom Higgins|uk-ac-man-per:129553</pid>
        </excluded_pids>
    </xsl:variable>

    <xsl:variable name="pure_types">
        <pure-types>
            <pure-type type="Original Work">article</pure-type>
            <pure-type type="Editorial">editorial</pure-type>
            <pure-type type="Note">comment-debate</pure-type>
            <pure-type type="Abstract">comment-debate</pure-type>
            <pure-type type="Letter">letter</pure-type>
            <pure-type type="Review">article</pure-type>            
            <pure-type type="Original research">article</pure-type>            
            <pure-type type="Review article">article</pure-type>            
            <pure-type type="">article</pure-type>            
        </pure-types>
    </xsl:variable>
    
    <xsl:template match="/">
        <records>
        <xsl:call-template name="recurse">
            <xsl:with-param name="start" select="0"/>
            <xsl:with-param name="end" select="$rows"/>
        </xsl:call-template>
        </records>
    </xsl:template>
        
    <xsl:template name="recurse">
        <xsl:param name="start"/>
        <xsl:param name="end"/>
        <xsl:variable name="obj_response">
            <xsl:copy-of select="doc(concat($url, '&amp;start=', $start, '&amp;rows=', $rows))"/>
        </xsl:variable>
        <xsl:for-each select="$obj_response/response/result/doc">
            <xsl:variable name="filename" select="replace(str[@name='PID'], ':', '_')"/>
            <xsl:choose>
                <xsl:when test="exists($obj_files/files/file[. = concat($file_path, $filename)])">
                    <!-- ignore... -->
                    <!--<FOUND><xsl:value-of select="str"/></FOUND>-->
                </xsl:when>
                <xsl:otherwise>
                    <xsl:variable name="excluded">
                        <xsl:call-template name="test_exclusion">
                            <xsl:with-param name="doc">
                                <xsl:copy-of select="."/>
                            </xsl:with-param>    
                        </xsl:call-template>                        
                    </xsl:variable>
                    <xsl:variable name="real_person">
                        <xsl:call-template name="test_real_person">
                            <xsl:with-param name="doc">
                                <xsl:copy-of select="."/>
                            </xsl:with-param>    
                        </xsl:call-template>                        
                    </xsl:variable>                    
                    <xsl:if test="not(contains($excluded, 'True')) and contains($real_person, 'True')">
                        <!--<xsl:if test="not(contains($excluded, 'True')) or (contains($excluded, 'True') and str[@name='x.state'] = 'Active')">-->
                        <record>
                            <!--<PID><xsl:value-of select="str"/></PID>-->
                            <xsl:call-template name="unmatched">
                                <!--<xsl:with-param name="pid" select="replace(str, ':', '?')"/>-->
                                <xsl:with-param name="pid" select="replace(str[@name='PID'], ':', '?')"/>
                            </xsl:call-template>
                        </record>                        
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
        
        <xsl:if test="$end &lt; $num_found">
            <xsl:call-template name="recurse">
                <xsl:with-param name="start" select="$end + 1"/>
                <xsl:with-param name="end" select="$end + $rows"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="test_exclusion">
        <xsl:param name="doc"/>
        <result>
            <!--<xsl:copy-of select="$doc"/>-->
            <xsl:for-each select="$doc/doc/arr[@name='r.isbelongsto.pid']/str">
                <xsl:variable name="pid" select="."/>
                <xsl:choose>
                    <xsl:when test="exists($excluded_pids/excluded_pids/pid[@id = $pid])">
                        <xsl:value-of select="'True'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'False'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </result>
    </xsl:template>

    <xsl:template name="test_real_person">
        <!-- BA: Test to exclude items owned only by abcde persons -->
        <xsl:param name="doc"/>
        <result>
            <xsl:for-each select="$doc/doc/arr[@name='r.isbelongsto.pid']/str">
                <xsl:variable name="pid" select="."/>
                <xsl:choose>
                    <xsl:when test="not(contains($pid, 'abcde'))">
                        <xsl:value-of select="'True'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'False'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </result>
    </xsl:template>
    
    <xsl:template name="unmatched">
        <xsl:param name="pid"/>
        <xsl:variable name="obj_doc">
            <xsl:copy-of select="doc(concat($root_url, '?q=PID:', $pid, '&amp;omitHeader=true&amp;start=0&amp;rows=10'))/response/result/doc"/>            
        </xsl:variable>
                
        <ids>
            <!--<escholar-id><xsl:value-of select="$pid"/></escholar-id>-->
            <escholar-id><xsl:value-of select="$obj_doc/doc/str[@name='PID']"/></escholar-id>
            <scopus-id></scopus-id>
            <pubmed-id><xsl:value-of select="$obj_doc/doc/str[@name='m.identifier.pmid']"/></pubmed-id>                    
            <isi-accession-number><xsl:value-of select="$obj_doc/doc/str[@name='m.identifier.isiaccessionnumber']"/></isi-accession-number>                    
        </ids>
        <history>
            <created-date><xsl:value-of select="substring-before($obj_doc/doc/date[@name='x.createddate'], 'T')"/></created-date>
            <last-modified-date><xsl:value-of select="substring-before($obj_doc/doc/date[@name='x.lastmodifieddate'], 'T')"/></last-modified-date>
            <createdby><xsl:value-of select="tokenize($obj_doc/doc/arr[@name='r.iscreatedby.source']/str, '\|\|')[3]"/></createdby>
            <last-modifiedby><xsl:value-of select="tokenize($obj_doc/doc/arr[@name='r.islastmodifiedby.source']/str, '\|\|')[3]"/></last-modifiedby>
        </history>        
        <puretype>
            <xsl:choose>
                <xsl:when test="$obj_doc/doc/str[@name='m.genre.version']">
                    <xsl:variable name="genre" select="$obj_doc/doc/str[@name='m.genre.version']"/>
                    <xsl:value-of select="$pure_types/pure-types/pure-type[@type = $genre]"/>                            
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'article'"/>
                </xsl:otherwise>
            </xsl:choose>
        </puretype>
        <publication-status>
            <xsl:choose>
                <xsl:when test="$obj_doc/doc/str[@name='m.genre.status'] = 'Accepted'">
                    <xsl:value-of select="'inpress'"/>
                </xsl:when>
                <xsl:when test="$obj_doc/doc/str[@name='m.genre.status'] = 'Published'">
                    <xsl:value-of select="'published'"/>
                </xsl:when>
                <xsl:when test="$obj_doc/doc/str[@name='m.genre.status'] = 'Submitted'">
                    <xsl:value-of select="'inprep'"/>
                </xsl:when>
                <xsl:when test="$obj_doc/doc/str[@name='m.genre.status'] = 'In-preparation'">
                    <xsl:value-of select="'inprep'"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'published'"/>                    
                </xsl:otherwise>
            </xsl:choose>
        </publication-status>
        <publication-status-dates>
            <date>
                <xsl:attribute name="status" select="'accepted'"/>
                <xsl:value-of select="$obj_doc/doc/str[@name='m.dateother.accepted']"/>
            </date>
            <date>
                <xsl:attribute name="status" select="'submitted'"/>
                <xsl:value-of select="$obj_doc/doc/str[@name='m.dateother.submitted']"/>
            </date>
        </publication-status-dates>
        <publicationdate>
            <xsl:try>
                <year>
                    <xsl:try>
                        <xsl:choose>
                            <xsl:when test="tokenize($obj_doc/doc/str[@name='m.dateissued'], '-')[1] ne ''">
                                <xsl:value-of select="tokenize($obj_doc/doc/str[@name='m.dateissued'], '-')[1]"/>                            
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="'2100'"/>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:catch>
                            <xsl:value-of select="'2100'"/>                            
                        </xsl:catch>
                    </xsl:try>
                </year>
                <xsl:catch></xsl:catch>
            </xsl:try>                       
            <xsl:try>
                <month>
                    <xsl:value-of select="tokenize($obj_doc/doc/str[@name='m.dateissued'], '-')[2]"/>
                </month>
                <xsl:catch></xsl:catch>
            </xsl:try>                       
            <xsl:try>
                <day>
                    <xsl:value-of select="tokenize($obj_doc/doc/str[@name='m.dateissued'], '-')[3]"/>
                </day>
                <xsl:catch></xsl:catch>
            </xsl:try>                       
        </publicationdate>        
        <release-date><xsl:value-of select="$obj_doc/doc/str[@name='perm.releasedate']"/></release-date>
        <misc>
            <pubmed-central-deposit-version><xsl:value-of select="$obj_doc/doc/str[@name='m.note.pmcversion']"/></pubmed-central-deposit-version>
            <pubmed-central-deposit-date><xsl:value-of select="$obj_doc/doc/str[@name='m.dateother.pmcdeposit']"/></pubmed-central-deposit-date>
        </misc>        
        <original-language>
            <xsl:choose>
                <xsl:when test="$obj_doc/doc/str[@name='m.languageterm.code']">
                    <xsl:value-of select="substring($obj_doc/doc/str[@name='m.languageterm.code'], 1, 3)"/>
                </xsl:when>
                <xsl:when test="$obj_doc/doc/str[@name='t.language']">
                    <xsl:value-of select="$obj_doc/doc/str[@name='t.language']"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'eng'"/>
                </xsl:otherwise>
            </xsl:choose>
        </original-language>
        <titles>            
            <xsl:apply-templates select="$obj_doc/doc/str[@name='m.title'] | $obj_doc/doc/str[@name='m.title.translated']"/>
        </titles>
        <number-of-pages>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.host.page.total']"/>            
        </number-of-pages>         
        <abstract>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.abstract']"/>
        </abstract>
        <keywords>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.topic']/str"/>
        </keywords>
        <structured-keywords>
            <xsl:for-each select="$obj_doc/doc/arr[@name='r.isbelongsto.source']/str[contains(., 'uk-ac-man-per:abcde')]">
                <xsl:variable name="pid" select="tokenize(., '\|\|')[2]"/>
                <structured-keyword>
                    <xsl:value-of select="$obj_structured_keywords/keywords/keyword[@PID = $pid]/uri"/>
                </structured-keyword>
            </xsl:for-each>
        </structured-keywords>                        
        <doi>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.identifier.doi']"/>
        </doi>
        <urls>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.preceding.source']/str"/>
        </urls>

        <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.note.general']/str  | $obj_doc/doc/arr[@name='m.note.funding']/str | $obj_doc/doc/str[@name='m.note.statementonresearchdata']"/>
        
        <xsl:variable name="internal-persons">
            <xsl:call-template name="get_authors">
                <xsl:with-param name="obj_doc">
                    <xsl:copy-of select="$obj_doc/doc"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:variable>
        
        <internal-person-list>
            <xsl:for-each select="distinct-values($internal-persons/internal-persons/internal-person)">                                
                <internalperson>
                    <id><xsl:value-of select="tokenize(., '; ')[1]"/></id>
                    <role><xsl:value-of select="tokenize(., '; ')[2]"/></role>
                    <firstname><xsl:value-of select="tokenize(., '; ')[3]"/></firstname>
                    <lastname><xsl:value-of select="tokenize(., '; ')[4]"/></lastname>
                </internalperson>                                            
            </xsl:for-each>            
        </internal-person-list>
        
        <author-list>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.name.aut.source']/str">
                <xsl:with-param name="role" select="'author'"/>
            </xsl:apply-templates>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.name.clb.source']/str">
                <xsl:with-param name="role" select="'collaborator'"/>                
            </xsl:apply-templates>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='m.name.trl.source']/str">
                <xsl:with-param name="role" select="'translator'"/>                
            </xsl:apply-templates>            
        </author-list>
        
        <authors-as-a-string>            
            <xsl:if test="$obj_doc/doc/str[@name='m.note.authors']">
                <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.note.authors']"/></author-string>
                <role>author</role>                
            </xsl:if>
            <xsl:if test="$obj_doc/doc/str[@name='m.note.collaborators']">
                <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.note.collaborators']"/></author-string>
                <role>collaborator</role>                
            </xsl:if>
            <xsl:if test="$obj_doc/doc/str[@name='m.note.translators']">
                <author-string><xsl:value-of select="$obj_doc/doc/str[@name='m.note.translators']"/></author-string>
                <role>translator</role>                
            </xsl:if>
        </authors-as-a-string>

        <journal-title>
            <xsl:value-of select="concat($obj_doc/doc/str[@name='m.host.title'], '|', $obj_doc/doc/str[@name='m.host.title.abbreviated'])"/>                
        </journal-title>        

        <pages>
            <start-page>
                <xsl:value-of select="$obj_doc/doc/str[@name='m.host.page.start']"/>                
            </start-page>
            <end-page>
                <xsl:value-of select="$obj_doc/doc/str[@name='m.host.page.end']"/>                
            </end-page>
        </pages>
        
        <article-number>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.identifier.articlenumber']"/>
        </article-number>
        
        <volume>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.host.volume']"/>                
        </volume>
        
        <issue>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.host.issue']"/>                
        </issue>
        
        <print-issn>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.host.identifier.issn']"/>
        </print-issn>
        
        <electronic-issn></electronic-issn>
        
        <peer-review>
            <xsl:value-of select="$obj_doc/doc/str[@name='m.ispeerreviewed']"/>                
        </peer-review>

        <journal-link></journal-link>
        
        <attachments>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'REVIEW')]">
                <xsl:with-param name="pid" as="xs:string" select="$obj_doc/doc/str[@name='PID']"/>
            </xsl:apply-templates>    
            <xsl:apply-templates select="$obj_doc/doc/str[@name='f.full-text.source']">
                <xsl:with-param name="pid" as="xs:string" select="$obj_doc/doc/str[@name='PID']"/>
            </xsl:apply-templates>                    
        </attachments>
        
        <additional-files>
            <xsl:apply-templates select="$obj_doc/doc/arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY')]">
                <xsl:with-param name="pid" as="xs:string" select="$obj_doc/doc/str[@name='PID']"/>
            </xsl:apply-templates>                        
        </additional-files>
        
        <licence-type>
            <xsl:variable name="licence_name" select="$obj_doc/doc/str[@name='m.accesscondition.licencetype']"/>
            <xsl:value-of select="$obj_open_licences/licences/licence[@name = $licence_name]/uri"/>
        </licence-type>

        <apcpaid>
            <xsl:choose>
                <xsl:when test="$obj_doc/doc/str[@name='m.note.isapcpaid'] = 'Yes'">
                    <xsl:value-of select="'yes'"/>
                </xsl:when>
                <xsl:when test="$obj_doc/doc/str[@name='m.note.isapcwaived'] = 'Yes'">
                    <xsl:value-of select="'waived'"/>
                </xsl:when>
            </xsl:choose>
        </apcpaid>        

        <visibility>
            <xsl:choose>
                <xsl:when test="$obj_doc/doc/str[@name='x.state'] = 'Active'">
                    <xsl:value-of select="'public'"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'confidential'"/>                            
                </xsl:otherwise>
            </xsl:choose>
        </visibility>
    </xsl:template>
        
    <xsl:template match="arr[@name='m.topic']/str">
        <keyword>
            <xsl:value-of select="normalize-space(.)"/>
        </keyword>
    </xsl:template>
    
    <xsl:template match="arr[@name='m.preceding.source']/str">
        <url>
            <xsl:value-of select="tokenize(., '\|\|')[2]"/>
        </url>
    </xsl:template>
        
    <xsl:template match="arr[@name='m.note.general']">
        <xsl:for-each select="str">
            <xsl:variable name="count">
                <xsl:number/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$count &gt; 1">
                    <xsl:value-of select="concat('; ', normalize-space(.))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="normalize-space(.)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>    

    <xsl:template match="arr[@name='r.isbelongstoorg.source']">
        <xsl:for-each select="str">
            <owner>
                <xsl:value-of select="normalize-space(.)"></xsl:value-of>
            </owner>
        </xsl:for-each>
    </xsl:template>
            
    <xsl:template match="arr[@name='r.hasauthor.source'] | arr[@name='r.isbelongsto.source']">
        <xsl:for-each select="str">
            <author>
                <author-first-name>
                    <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[2]"/>
                </author-first-name>
                <author-last-name>
                    <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[1]"/>
                </author-last-name>
                <role>author</role>
            </author>
        </xsl:for-each>
    </xsl:template>    

    <xsl:template match="str[@name='m.title'] | str[@name='m.title.translated']">
        <title>
            <xsl:if test="@name='m.title'">
                <xsl:attribute name="original" select="'y'"/>
            </xsl:if>
            <xsl:value-of select="normalize-space(.)"/>            
        </title>
    </xsl:template>

    <xsl:template match="arr[@name='m.note.general']/str | arr[@name='m.note.funding']/str | str[@name='m.note.statementonresearchdata']">
        <bibliographical-note>
            <xsl:value-of select="normalize-space(.)"/>
        </bibliographical-note>
    </xsl:template>
        
    <xsl:template match="arr[@name='m.name.aut.source']/str | arr[@name='m.name.clb.source']/str | arr[@name='m.name.trl.source']/str">
        <xsl:param name="role"/>
        <author>
            <first-name>
                <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[2]"/>                
            </first-name>
            <last-name>
                <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[1]"/>                
            </last-name>
            <role><xsl:value-of select="$role"/></role>
        </author>
    </xsl:template>    
    
    <xsl:template name="get_authors">
        <xsl:param name="obj_doc"/>        
        <internal-persons>
            <xsl:for-each select="$node_names/node-names/node-name">
                <xsl:call-template name="author_assoc">                        
                    <xsl:with-param name="node_name">
                        <xsl:copy-of select="."/>
                    </xsl:with-param>
                    <xsl:with-param name="solr_node">
                        <xsl:copy-of select="$obj_doc"/>
                    </xsl:with-param>
                </xsl:call-template>                            
            </xsl:for-each>   
        </internal-persons>        
    </xsl:template>
    
    <xsl:template name="author_assoc">
        <xsl:param name="node_name"/>
        <xsl:param name="solr_node"/>
        <xsl:for-each select="$solr_node/doc/arr[@name=$node_name/node-name]/str">
            <xsl:if test="not(contains(., 'uk-ac-man-per:abcde'))">
                <internal-person>
                    <xsl:value-of select="concat(tokenize(., '\|\|')[3], '; ', $node_name/node-name/@role, '; ', tokenize(tokenize(., '\|\|')[1], ', ')[2], '; ', tokenize(tokenize(., '\|\|')[1], ', ')[1])"/>                    
                </internal-person>                
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="arr[@name='r.hasauthor.source'] | arr[@name='r.isbelongsto.source']">
        <xsl:for-each select="str">
            <author>
                <first-name>
                    <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[2]"/>
                </first-name>
                <last-name>
                    <xsl:value-of select="tokenize(tokenize(., '\|\|')[1], ', ')[1]"/>
                </last-name>
                <role>author</role>
            </author>
        </xsl:for-each>
    </xsl:template>    
    
    <xsl:template match="arr[@name='f.fileattached.source']/str[contains(., 'REVIEW')]">
        <xsl:param name="pid" as="xs:string"/>
        <attachment>
            <file-version>
                <xsl:choose>
                    <xsl:when test="contains(., 'PRE-PEER-REVIEW.') or contains(., 'PRE-PEER-REVIEW-DOCUMENT.')">
                        <xsl:value-of select="'preprint'"/>
                    </xsl:when>
                    <xsl:when test="contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS.') or contains(., 'POST-PEER-REVIEW-NON-PUBLISHERS-DOCUMENT.')">
                        <xsl:value-of select="'authorsversion'"/>
                    </xsl:when>                    
                    <xsl:when test="contains(., 'POST-PEER-REVIEW-PUBLISHERS.') or contains(., 'POST-PEER-REVIEW-PUBLISHERS-DOCUMENT.')">
                        <xsl:value-of select="'publishersversion'"/>
                    </xsl:when>                    
                </xsl:choose>
            </file-version>
            <file-url>
                <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?pid=', $pid, '&amp;dsid=', tokenize(., '\|\|')[1])"/>
            </file-url>
        </attachment>
    </xsl:template>

    <xsl:template match="str[@name='f.full-text.source']">
        <xsl:param name="pid" as="xs:string"/>
        <attachment>
            <file-version>
                <xsl:value-of select="'publishersversion'"/>
            </file-version>
            <file-url>
                <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?pid=', $pid, '&amp;dsid=', tokenize(., '\|\|')[2])"/>
            </file-url>
        </attachment>
    </xsl:template>
    
    <xsl:template match="arr[@name='f.fileattached.source']/str[contains(., 'SUPPLEMENTARY')]">
        <xsl:param name="pid" as="xs:string"/>
        <url>
            <xsl:value-of select="concat('https://www.escholar.manchester.ac.uk/admin/getdatastream.do?pid=', $pid, '&amp;dsid=', tokenize(., '\|\|')[1])"/>            
        </url>
    </xsl:template>
        
    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>
    
</xsl:stylesheet>
