<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:ait="http://www.elsevier.com/xml/ani/ait"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xoe="http://www.elsevier.com/xml/xoe/dtd" 
    xmlns:ce="http://www.elsevier.com/xml/ani/common" 
    xmlns:cto="http://www.elsevier.com/xml/cto/dtd" 
    xmlns:xocs="http://www.elsevier.com/xml/xocs/dtd"            
    exclude-result-prefixes="xs math ait xsi xoe ce cto xocs"
    version="3.0">
    <xsl:output indent="yes" method="xml"/>    
    
    <xsl:variable name="root_uri" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/scw/select/'" />
    
    <xsl:template match="/">
        <records>
            <xsl:apply-templates select="files" />
        </records>
    </xsl:template>

    <xsl:template match="files">
        <xsl:for-each select="file">
            <xsl:variable name="pid" select="replace(tokenize(., '/')[last()], '_', '?')" />
            <xsl:variable name="query" select="concat($root_uri, '?q=PID%3A', replace($pid, ':', '?'))"></xsl:variable>
            <xsl:variable name="obj_solr">
                <xsl:copy-of select="doc($query)"/>
            </xsl:variable>
            <xsl:variable name="scopus_abstract_path" select="concat(tokenize(., '/')[last()], '/', 'abstract-citations-response_scopus_data_20141101.xml')"></xsl:variable>
            <xsl:variable name="obj_scopus_abstract">
                <xsl:copy-of select="doc($scopus_abstract_path)"/>
            </xsl:variable>
            <xsl:variable name="scopus_id" select="$obj_scopus_abstract/abstract-citations-response/identifier-legend/identifier/scopus_id"/>
            <xsl:variable name="scopus_file_path" select="concat(tokenize(., '/')[last()], '/2-s2.0-', $scopus_id, '.xml')"></xsl:variable>
            <xsl:variable name="obj_scopus_file">
                <xsl:copy-of select="doc($scopus_file_path)"/>
            </xsl:variable>
            
            <record>
                <escholar-id><xsl:value-of select="replace(tokenize(., '/')[last()], '_', ':')"/></escholar-id>
                <scopus-id><xsl:value-of select="concat('2-s2.0-', $scopus_id)"/></scopus-id>
                <internal-person-list>
                    <xsl:variable name="node_names">
                        <node-names>
                            <node-name role="author">r.isbelongsto.source</node-name>
                            <node-name role="author">r.hasauthor.source</node-name>
                            <node-name role="coauthor">r.hascoauthor.source</node-name>
                            <node-name role="corresponding_author">r.hascorrespondingauthor.source</node-name>
                            <node-name role="co_senior_author">r.hascoseniorauthor.source</node-name>
                            <node-name role="first_author">r.hasfirstauthor.source</node-name>
                            <node-name role="last_author">r.haslastauthor.source</node-name>
                            <node-name role="senior_author">r.hasseniorauthor.source</node-name>
                            <!--<node-name role="thesis_advisor">r.hasadvisor.source</node-name>-->
                            <!--<node-name role="main_supervisor">r.hasmainsupervisor.source</node-name>-->
                            <!--<node-name role="co_supervisor">r.hascosupervisor.source</node-name>-->
                        </node-names>
                    </xsl:variable>
                    <xsl:for-each select="$node_names/node-names/node-name">
                        <xsl:call-template name="author_assoc">                        
                            <xsl:with-param name="node_name">
                                <xsl:copy-of select="."/>
                            </xsl:with-param>
                            <xsl:with-param name="solr_node">
                                <xsl:copy-of select="$obj_solr"/>
                            </xsl:with-param>
                        </xsl:call-template>                            
                    </xsl:for-each>
                </internal-person-list>

                <xsl:apply-templates select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head" mode="author-list"/>
<!--                
                <authors-as-a-string>
                    <author-string>
                        <xsl:apply-templates select="$obj_scopus_file/xocs:doc/xocs:item/item/bibrecord/head"/>
                    </author-string>
                    <role>author</role>
                </authors-as-a-string>                                
-->                
            </record>
        </xsl:for-each>    
    </xsl:template>
    
    <!-- BA: authors-as-a-string... -->
    <xsl:template match="head">
        <xsl:for-each select="author-group">
            <xsl:variable name="count_author_group">
                <xsl:number/>
            </xsl:variable>
            <xsl:for-each select="author">
                <xsl:variable name="count_author">
                    <xsl:number/>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="$count_author_group &gt; 1 or $count_author &gt; 1">
                        <xsl:value-of select="concat('; ', ce:indexed-name)"></xsl:value-of>                    
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="ce:indexed-name"></xsl:value-of>                    
                    </xsl:otherwise>                
                </xsl:choose>                
            </xsl:for-each>    
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="head" mode="author-list">
        <author-list>            
            <xsl:for-each select="author-group">
                <xsl:for-each select="author">
                    <author>
                        <first-name><xsl:value-of select="preferred-name/ce:given-name"/></first-name>
                        <last-name><xsl:value-of select="preferred-name/ce:surname"/></last-name>
                        <role>author</role>
                    </author>
                </xsl:for-each>    
            </xsl:for-each>
        </author-list>            
    </xsl:template>    
    
    <xsl:template name="author_assoc">
        <xsl:param name="node_name"/>
        <xsl:param name="solr_node"/>
        <xsl:for-each select="$solr_node/response/result/doc/arr[@name=$node_name/node-name]/str">
            <internalperson>
                <id>
                    <xsl:value-of select="tokenize(., '\|\|')[3]"/>                
                </id>                                        
                <role>
                    <xsl:value-of select="$node_name/node-name/@role"></xsl:value-of>
                </role>
            </internalperson>            
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="text()">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>
    
</xsl:stylesheet>