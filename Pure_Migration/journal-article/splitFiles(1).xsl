<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xoe="http://www.elsevier.com/xml/xoe/dtd" 
    xmlns:ce="http://www.elsevier.com/xml/ani/common" 
    xmlns:cto="http://www.elsevier.com/xml/cto/dtd" 
    xmlns:xocs="http://www.elsevier.com/xml/xocs/dtd"            
    exclude-result-prefixes="xs math xsi xoe ce cto xocs"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>

    <xsl:variable name="file_name">
        <xsl:value-of select="'files(200).xml'"></xsl:value-of>
    </xsl:variable>
        
    <xsl:variable name="obj_file">
        <xsl:copy-of select="doc($file_name)"/>
    </xsl:variable>
    
    <xsl:variable name="start" as="xs:integer" select="1"/>        
    <xsl:variable name="end" as="xs:integer" select="count($obj_file//file)"/>
    <xsl:variable name="chunk" as="xs:integer" select="100"/>
            
    <xsl:template match="/">
        <xsl:call-template name="recurse">
            <xsl:with-param name="recurse_start" select="$start" as="xs:integer"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="recurse">
        <xsl:param name="recurse_start" as="xs:integer"/>
        
        <xsl:choose>
            <xsl:when test="$recurse_start lt $end">
                <xsl:variable name="output_file_name">
                    <xsl:value-of select="concat('zzsplit-files/', $recurse_start, '-', $recurse_start + $chunk - 1, '.xml')"/>
                </xsl:variable>

                <xsl:result-document href="{$output_file_name}" method="xml" indent="yes">
                    <files>
                        <xsl:for-each select="$obj_file/files/file">
                            <xsl:variable name="count" as="xs:integer">
                                <xsl:number/>
                            </xsl:variable>
                            <xsl:choose>
                                <xsl:when test="$count lt ($recurse_start + $chunk)">
                                    <xsl:apply-templates select="."/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:if test="$count lt $end">
                                        <xsl:call-template name="recurse">
                                            <xsl:with-param name="recurse_start" select="$count"/>
                                        </xsl:call-template>
                                    </xsl:if>
                                </xsl:otherwise>
                            </xsl:choose>                        
                        </xsl:for-each>
                    </files>            
                </xsl:result-document>
                
            </xsl:when>
        </xsl:choose>
                
    </xsl:template>

    <xsl:template match="file">
        <xsl:copy-of select="."/>
    </xsl:template>

</xsl:stylesheet>