<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output indent="yes" method="xml"/>           
    
    <xsl:template match="/">        
        <files>
            <xsl:apply-templates select="files/file[count(scopus-abstracts/scopus-abstract) gt 1]"/>
        </files>
        
    </xsl:template>
    
    <xsl:template match="file">
        <xsl:copy-of select="."/>
    </xsl:template>
    
</xsl:stylesheet>