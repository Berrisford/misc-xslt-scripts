<?xml version="1.0" encoding="UTF-8"?>
<foxml:digitalObject xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                     VERSION="1.1"
                     PID="uk-ac-man-scw:ma358">
   <foxml:objectProperties>
      <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
   </foxml:objectProperties>
   <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="MODS.0"
                               LABEL="Metadata Object Description Schema record"
                               MIMETYPE="text/xml"
                               SIZE="3550">
         <foxml:xmlContent>
            <mods:mods xmlns:mods="http://www.loc.gov/mods/v3"
                       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       version="3.3"
                       xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd">
               <mods:abstract>Lax legislation and increasing demand for electronics are driving relentless growth in e-waste in the developing world. To reduce the damage caused by e-waste and recover value from end-of-life (EoL) electronics, original equipment manufacturers (OEMs) have created, over the past decades, programs to divert e-waste from landfill to recycling and reuse. Although the subject of intense debate, little is known about such initiatives in terms of levels of participation by OEMs or the extent to which they have succeeded in reducing e-waste in developing economies. To broaden our understanding of these issues, we investigate take-back initiatives in the thriving market of personal computers, i.e.
desktop and laptop computers, in Brazil. Using a multi-method approach (electronic archival data collection and semi-structured interviews with manufacturers), we find evidence that large multinational manufacturers are at the forefront of take-back programs. These initiatives, however, in many ways lag behind those implemented in the United States, a more developed market as far as product take back is concerned. We find the main reasons for the low levels of participation by OEMs in take-back programs to be: high collection costs, low residual values, and lax, unclear and conflicting legislation. Moreover, we propose new avenues of research, in light of our scant knowledge of country-specific, company-specific and product-specific determinants that moderate participation.</mods:abstract>
               <mods:genre authority="local" type="content-type">journal-article</mods:genre>
               <mods:genre type="version">Original research</mods:genre>
               <mods:genre type="form">Academic journal article</mods:genre>
               <mods:language>
                  <mods:languageTerm authority="iso639-3" type="code">eng</mods:languageTerm>
               </mods:language>
               <mods:note type="authors">Joao Quariguasi, LN Van Wassenhove</mods:note>
               <mods:originInfo>
                  <mods:dateIssued encoding="iso8601"/>
                  <mods:publisher>Wiley Online Library</mods:publisher>
               </mods:originInfo>
               <mods:recordInfo>
                  <mods:recordCreationDate encoding="iso8601">2015-05-19T17:06:30.648+01:00</mods:recordCreationDate>
                  <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
               </mods:recordInfo>
               <mods:relatedItem type="host">
                  <mods:titleInfo>
                     <mods:title>Journal of Industrial Ecology</mods:title>
                  </mods:titleInfo>
                  <mods:part>
                     <mods:detail type="issue">
                        <mods:number>0</mods:number>
                     </mods:detail>
                     <mods:detail type="volume">
                        <mods:number/>
                     </mods:detail>
                     <mods:extent unit="page">
                        <mods:start/>
                        <mods:end/>
                        <mods:total>0</mods:total>
                        <mods:list/>
                     </mods:extent>
                  </mods:part>
                  <mods:identifier type="issn"/>
               </mods:relatedItem>
               <mods:relatedItem type="preceding">
                  <mods:titleInfo>
                     <mods:title/>
                  </mods:titleInfo>
                  <mods:identifier type="uri"/>
               </mods:relatedItem>
               <mods:titleInfo>
                  <mods:title>OEM participation in take-back initiatives in Brazil: An analysis of engagement levels and obstacles (accepted for publication)</mods:title>
               </mods:titleInfo>
               <mods:typeOfResource>text</mods:typeOfResource>
            </mods:mods>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="RELS-EXT.1"
                               LABEL="Relationships"
                               MIMETYPE="text/xml"
                               SIZE="920">
         <foxml:xmlContent>
            <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#"
                     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
               <rdf:Description rdf:about="info:fedora/uk-ac-man-scw:ma358">
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:97398"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:1"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:41"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:203"/>
                  <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                  <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:1"/>
               </rdf:Description>
            </rdf:RDF>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
</foxml:digitalObject>
