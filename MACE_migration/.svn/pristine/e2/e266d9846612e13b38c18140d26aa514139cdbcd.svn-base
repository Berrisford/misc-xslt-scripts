<?xml version="1.0" encoding="UTF-8"?>
<foxml:digitalObject xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                     VERSION="1.1"
                     PID="uk-ac-man-scw:ma088">
   <foxml:objectProperties>
      <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
   </foxml:objectProperties>
   <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="MODS.0"
                               LABEL="Metadata Object Description Schema record"
                               MIMETYPE="text/xml"
                               SIZE="3550">
         <foxml:xmlContent>
            <mods:mods xmlns:mods="http://www.loc.gov/mods/v3"
                       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       version="3.3"
                       xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd">
               <mods:abstract>In modern machinery and automobile structures weight reduction and increased durability are the main issues in design. In these applications, lap welded and/or bonded joints are widely used; therefore, tools are needed to accurately predict their fatigue life. This paper is concerned with the fatigue strength of single lap joints formed with thin plates of 6082-T6 aluminium alloy using a high strength two-component epoxy adhesive (Araldite 420 A/B from Hunstman). Experimental SN curves were obtained for resistance spot-welded and weld-bonded lap joints. The fatigue lives of weld-bonded joints were significantly higher than those of resistance spot-welding joints. In addition, fatigue lives were predicted with Morrows modified MansonCoffin (M/MC) and the SmithWatsonTopper (SWT) damage equations. Elasticplastic numerical models were developed, replicating the experimental work, in order to obtain local stress and strain fields. An acceptable agreement was obtained between the numerical predictions and the experimental results. The M/MC damage equation diverged from experimental results for relatively long fatigue lives, while the
SWT equation gave good predictions for all fatigue lives.</mods:abstract>
               <mods:genre authority="local" type="content-type">journal-article</mods:genre>
               <mods:genre type="version">Original research</mods:genre>
               <mods:genre type="form">Academic journal article</mods:genre>
               <mods:language>
                  <mods:languageTerm authority="iso639-3" type="code">eng</mods:languageTerm>
               </mods:language>
               <mods:note type="authors">A Pereira, F Antunes, J Ferreira, Paulo Bartolo</mods:note>
               <mods:originInfo>
                  <mods:dateIssued encoding="iso8601">2014</mods:dateIssued>
                  <mods:publisher>Taylor and Francis Group</mods:publisher>
               </mods:originInfo>
               <mods:recordInfo>
                  <mods:recordCreationDate encoding="iso8601">2015-05-19T17:06:30.648+01:00</mods:recordCreationDate>
                  <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
               </mods:recordInfo>
               <mods:relatedItem type="host">
                  <mods:titleInfo>
                     <mods:title>Journal of Adhesion Science and Technology</mods:title>
                  </mods:titleInfo>
                  <mods:part>
                     <mods:detail type="issue">
                        <mods:number>14</mods:number>
                     </mods:detail>
                     <mods:detail type="volume">
                        <mods:number>28</mods:number>
                     </mods:detail>
                     <mods:extent unit="page">
                        <mods:start>1432</mods:start>
                        <mods:end>1450</mods:end>
                        <mods:total>18</mods:total>
                        <mods:list>1432-1450</mods:list>
                     </mods:extent>
                  </mods:part>
                  <mods:identifier type="issn">0169-4243</mods:identifier>
               </mods:relatedItem>
               <mods:relatedItem type="preceding">
                  <mods:titleInfo>
                     <mods:title/>
                  </mods:titleInfo>
                  <mods:identifier type="uri"/>
               </mods:relatedItem>
               <mods:titleInfo>
                  <mods:title>ASSESSMENT OF THE FATIGUE LIFE OF ALUMINIUM SPOT WELDED AND WELD-BONDED JOINTS</mods:title>
               </mods:titleInfo>
               <mods:typeOfResource>text</mods:typeOfResource>
            </mods:mods>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="RELS-EXT.1"
                               LABEL="Relationships"
                               MIMETYPE="text/xml"
                               SIZE="920">
         <foxml:xmlContent>
            <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#"
                     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
               <rdf:Description rdf:about="info:fedora/uk-ac-man-scw:ma088">
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde39"/>
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:133301"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:1"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:41"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:345"/>
                  <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                  <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:1"/>
               </rdf:Description>
            </rdf:RDF>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
</foxml:digitalObject>
