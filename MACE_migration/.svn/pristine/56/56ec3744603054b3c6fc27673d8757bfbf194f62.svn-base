<?xml version="1.0" encoding="UTF-8"?>
<foxml:digitalObject xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                     VERSION="1.1"
                     PID="uk-ac-man-scw:ma210">
   <foxml:objectProperties>
      <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
   </foxml:objectProperties>
   <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="MODS.0"
                               LABEL="Metadata Object Description Schema record"
                               MIMETYPE="text/xml"
                               SIZE="3550">
         <foxml:xmlContent>
            <mods:mods xmlns:mods="http://www.loc.gov/mods/v3"
                       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       version="3.3"
                       xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd">
               <mods:abstract>The paper reports an experimental study of impingement cooling in a rotating passage of
semi-cylindrical cross section. Cooling fluid is injected from a row of five jet holes along
the centerline of the flat surface of the passage and strikes the concave surface. The
cooling passage rotates orthogonally about an axis parallel to that of the jets. Tests have
been carried out, using water, both within the passage and as the jet fluid, at a fixed
Reynolds number of 15,000, for clockwise and counter-clockwise rotation. Local Nusselt
number measurements, using the liquid-crystal technique, show that under stationary
conditions a high Nusselt number region develops around each impingement point, with
secondary peaks half-way between impingement points. Rotation reduces heat transfer,
leads to the disappearance of all secondary peaks and also, surprisingly, of some of the
primary peaks. Flow visualization tests suggest that these changes in thermal behavior
are caused because rotation increases the spreading rate of the jets. LDA and PIV measurements
are also presented. They show that under stationary conditions the five jets
exhibit a similar behavior, with their cores remai</mods:abstract>
               <mods:genre authority="local" type="content-type">journal-article</mods:genre>
               <mods:genre type="version">Original research</mods:genre>
               <mods:genre type="form">Academic journal article</mods:genre>
               <mods:language>
                  <mods:languageTerm authority="iso639-3" type="code">eng</mods:languageTerm>
               </mods:language>
               <mods:note type="authors">Brian Launder, D Kounadis, Hector Iacovides, J-K Li, X Xu</mods:note>
               <mods:originInfo>
                  <mods:dateIssued encoding="iso8601">2005</mods:dateIssued>
                  <mods:publisher>The American Society of Mechanical Engineers (ASME)</mods:publisher>
               </mods:originInfo>
               <mods:recordInfo>
                  <mods:recordCreationDate encoding="iso8601">2015-05-19T17:06:30.648+01:00</mods:recordCreationDate>
                  <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
               </mods:recordInfo>
               <mods:relatedItem type="host">
                  <mods:titleInfo>
                     <mods:title>Transactions of the ASME, Journal of Turbomachinery</mods:title>
                  </mods:titleInfo>
                  <mods:part>
                     <mods:detail type="issue">
                        <mods:number>0</mods:number>
                     </mods:detail>
                     <mods:detail type="volume">
                        <mods:number>127</mods:number>
                     </mods:detail>
                     <mods:extent unit="page">
                        <mods:start>222</mods:start>
                        <mods:end>229</mods:end>
                        <mods:total>7</mods:total>
                        <mods:list>222-229</mods:list>
                     </mods:extent>
                  </mods:part>
                  <mods:identifier type="issn">0889-504X</mods:identifier>
               </mods:relatedItem>
               <mods:relatedItem type="preceding">
                  <mods:titleInfo>
                     <mods:title/>
                  </mods:titleInfo>
                  <mods:identifier type="uri"/>
               </mods:relatedItem>
               <mods:titleInfo>
                  <mods:title>Experimental  Study of the Flow and Thermal Development of a Row of Cooling Jets Impinging on a Rotating Concave Surface</mods:title>
               </mods:titleInfo>
               <mods:typeOfResource>text</mods:typeOfResource>
            </mods:mods>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="RELS-EXT.1"
                               LABEL="Relationships"
                               MIMETYPE="text/xml"
                               SIZE="920">
         <foxml:xmlContent>
            <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#"
                     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
               <rdf:Description rdf:about="info:fedora/uk-ac-man-scw:ma210">
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde36"/>
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:39615"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:1"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:41"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:345"/>
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:39701"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:1"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:41"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:345"/>
                  <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                  <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:1"/>
               </rdf:Description>
            </rdf:RDF>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
</foxml:digitalObject>
