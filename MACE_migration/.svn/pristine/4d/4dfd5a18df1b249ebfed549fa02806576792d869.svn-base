<?xml version="1.0" encoding="UTF-8"?>
<foxml:digitalObject xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                     VERSION="1.1"
                     PID="uk-ac-man-scw:ma464">
   <foxml:objectProperties>
      <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
   </foxml:objectProperties>
   <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="MODS.0"
                               LABEL="Metadata Object Description Schema record"
                               MIMETYPE="text/xml"
                               SIZE="3550">
         <foxml:xmlContent>
            <mods:mods xmlns:mods="http://www.loc.gov/mods/v3"
                       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       version="3.3"
                       xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd">
               <mods:abstract>The entrainment of sediments in rivers exhibits an intermittent behavior. Incipient motion should therefore be described as a random process requiring a stochastic predictive approach. The effect of near-bed turbulence on grain entrainment and the variation in bed particle stability due to local surface heterogeneity are included into a probabilistic framework. The intuitive evidence that hydrodynamic forces acting on the sediment bed and the resistance to motion of the bed particles are two mutually dependent aspects of a unique process is modeled by introducing a conditional independence hypothesis. Based on this concept, new insights into the stochastic aspects of incipient motion are obtained. For low ratios of the boundary shear stress to the critical shear stress, by including the mutual dependence of different processes the new theoretical development predicts up to 50% larger probabilities of grain removal from the bed surface compared to Grass' original formulation. This follows from the entrainment risk being not only dependent on the distributions of fluid forces and grain resistance, but also on the correlation that these distributions exhibit in relation to the geometrical configuration of the sediment bed. This complex interaction is neglected in existing probabilistic models of sediment transport. It is then demonstrated that such additional contribution explains better the influence of both flow turbulence and particle arrangement on key features of the overall grain entrainment process.</mods:abstract>
               <mods:genre authority="local" type="content-type">journal-article</mods:genre>
               <mods:genre type="version">Original research</mods:genre>
               <mods:genre type="form">Academic journal article</mods:genre>
               <mods:language>
                  <mods:languageTerm authority="iso639-3" type="code">eng</mods:languageTerm>
               </mods:language>
               <mods:note type="authors">A Marion, Andrea Bottacin-Busolin, M Tregnaghi, S Tait</mods:note>
               <mods:originInfo>
                  <mods:dateIssued encoding="iso8601">2012</mods:dateIssued>
                  <mods:publisher>American Geophysical Union</mods:publisher>
               </mods:originInfo>
               <mods:recordInfo>
                  <mods:recordCreationDate encoding="iso8601">2015-05-19T17:06:30.648+01:00</mods:recordCreationDate>
                  <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
               </mods:recordInfo>
               <mods:relatedItem type="host">
                  <mods:titleInfo>
                     <mods:title>Journal of Geophysical Research</mods:title>
                  </mods:titleInfo>
                  <mods:part>
                     <mods:detail type="issue">
                        <mods:number>0</mods:number>
                     </mods:detail>
                     <mods:detail type="volume">
                        <mods:number>117</mods:number>
                     </mods:detail>
                     <mods:extent unit="page">
                        <mods:start>F04004</mods:start>
                        <mods:end>F04004</mods:end>
                        <mods:total>0</mods:total>
                        <mods:list>F04004</mods:list>
                     </mods:extent>
                  </mods:part>
                  <mods:identifier type="issn">0148-0227</mods:identifier>
               </mods:relatedItem>
               <mods:relatedItem type="preceding">
                  <mods:titleInfo>
                     <mods:title/>
                  </mods:titleInfo>
                  <mods:identifier type="uri"/>
               </mods:relatedItem>
               <mods:titleInfo>
                  <mods:title>Stochastic determination of entrainment risk in uniformly sized sediment beds at low transport stages: 1. Theory</mods:title>
               </mods:titleInfo>
               <mods:typeOfResource>text</mods:typeOfResource>
            </mods:mods>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="RELS-EXT.1"
                               LABEL="Relationships"
                               MIMETYPE="text/xml"
                               SIZE="920">
         <foxml:xmlContent>
            <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#"
                     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
               <rdf:Description rdf:about="info:fedora/uk-ac-man-scw:ma464">
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde43"/>
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:130517"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:1"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:41"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:347"/>
                  <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                  <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:1"/>
               </rdf:Description>
            </rdf:RDF>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
</foxml:digitalObject>
