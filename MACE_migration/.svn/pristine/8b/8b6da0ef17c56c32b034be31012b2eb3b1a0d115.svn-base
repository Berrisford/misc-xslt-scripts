<?xml version="1.0" encoding="UTF-8"?>
<foxml:digitalObject xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                     VERSION="1.1"
                     PID="uk-ac-man-scw:ma735">
   <foxml:objectProperties>
      <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
   </foxml:objectProperties>
   <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
      <foxml:datastreamVersion CREATED="2015-05-26T11:07:30.261Z"
                               ID="MODS.0"
                               LABEL="Metadata Object Description Schema record"
                               MIMETYPE="text/xml"
                               SIZE="3550">
         <foxml:xmlContent>
            <mods:mods xmlns:mods="http://www.loc.gov/mods/v3"
                       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       version="3.3"
                       xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd">
               <mods:abstract>This thesis describes the development and validation of an Eddy Viscosity Model (EVM) that is sensitive to the local stress-strain mis-alignment of mean unsteady turbulent flow, in the framework of Unsteady Reynolds Averaged Navier-Stokes (URANS) turbulence models. The new model considers a parameter, Cas, representing the dot product of the strain	tensor	S i j	and	the	stress	anisotropy	tensor	ai j.	This	quantity,	defined	as	Cas	= &amp;#8722;ai jS i j/ &amp;#1113113;2S i jS i j, projects the six equations of the Reynolds stress transport model onto a single equation. With respect to Non-Linear Eddy Viscosity Models (NLEVM) or Explicit Algebraic Reynolds Stress Models (EARSM), the novelty of the present model lies in incorporating time dependent and/or transport effects of the turbulent stresses.
Initially, simple time dependent homogeneous flows are analysed, which are of rele- vance to the unsteady turbulent flow in the cylinder of a piston engine. Oscillating Chan- nel flows are then examined, to assess the models performance in the case where tur- bulence is most influential near to a wall. The model is then applied to two-dimensional flows of direct industrial relevance: the unsteady turbulent flow around an aerofoil beyond stall, and the complex unsteady turbulent motion in the wake of a cylinder. The later case is also calculated for a fully three-dimensional domain. The Cas model is incorporated into the widely used Shear Stress Transport (SST) model to form a three-equation model, and good predictions are returned with the new model, relative to the standard SST model, for many aspects of the flows presented. The small additional computational expense is measured to be around 10 &amp;#8722; 15% compared to the standard SST model.
The Cas model is shown to be a simple and economic modification to an existing modelling scheme that is widely used in industry. While this work represents only the first stage of the development and optimisation process, the general conclusions of this thesis are encouraging enough to warrant further investigation. Observations from the 3D calculations suggest that this model might also be exploited in a Detached Eddy Simula- tion (DES) framework, or as part of a hybrid RANS-LES approach.</mods:abstract>
               <mods:genre authority="local" type="content-type">technical-report</mods:genre>
               <mods:note type="authors">Alistair Revell</mods:note>
               <mods:originInfo>
                  <mods:dateIssued encoding="iso8601">2006</mods:dateIssued>
                  <mods:publisher/>
               </mods:originInfo>
               <mods:recordInfo>
                  <mods:recordCreationDate encoding="iso8601">2015-05-26T11:07:30.261+01:00</mods:recordCreationDate>
                  <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
               </mods:recordInfo>
               <mods:relatedItem type="host">
                  <mods:identifier type="isbn"/>
                  <mods:titleInfo>
                     <mods:title>A stress-strain lag eddy viscosity model for unsteady mean turbulent flows</mods:title>
                  </mods:titleInfo>
                  <mods:relatedItem type="host">
                     <mods:titleInfo>
                        <mods:title/>
                     </mods:titleInfo>
                  </mods:relatedItem>
               </mods:relatedItem>
               <mods:relatedItem type="preceding">
                  <mods:identifier type="uri">http://cfd.mace.manchester.ac.uk/twiki/bin/view/CfdTm/ResPub26</mods:identifier>
               </mods:relatedItem>
               <mods:titleInfo>
                  <mods:title>A stress-strain lag eddy viscosity model for unsteady mean turbulent flows</mods:title>
               </mods:titleInfo>
               <mods:typeOfResource>text</mods:typeOfResource>
            </mods:mods>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
      <foxml:datastreamVersion CREATED="2015-05-26T11:07:30.261Z"
                               ID="RELS-EXT.1"
                               LABEL="Relationships"
                               MIMETYPE="text/xml"
                               SIZE="920">
         <foxml:xmlContent>
            <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#"
                     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
               <rdf:Description rdf:about="info:fedora/uk-ac-man-scw:ma735">
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde36"/>
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:40131"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:1"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:41"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:345"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:2"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:34"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:105"/>
                  <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                  <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:14"/>
               </rdf:Description>
            </rdf:RDF>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
</foxml:digitalObject>
