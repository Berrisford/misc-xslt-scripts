<?xml version="1.0" encoding="UTF-8"?>
<foxml:digitalObject xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                     VERSION="1.1"
                     PID="uk-ac-man-scw:ma204">
   <foxml:objectProperties>
      <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
   </foxml:objectProperties>
   <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="MODS.0"
                               LABEL="Metadata Object Description Schema record"
                               MIMETYPE="text/xml"
                               SIZE="3550">
         <foxml:xmlContent>
            <mods:mods xmlns:mods="http://www.loc.gov/mods/v3"
                       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       version="3.3"
                       xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd">
               <mods:abstract>The analysis and extraction of the appropriate signal's features in structural health monitoring applications is one of the major challenges on which the robustness of the designed systems relies. Many strategies have been developed in the past, which utilise the identification of amplitude-based parameters for the evaluation of structural integrity. However, these parameters usually require a baseline reference, which might be extensively affected by noise, environmental or mounting conditions. This paper illustrates the applicability of Hilbert transform and HilbertHuang transform on the postprocessing of guided ultrasonic waves for evaluating the condition of relatively complex structural health monitoring applications. Two case studies are presented to demonstrate the suitability of the techniques, namely, the damage monitoring of an aluminium repaired panel and the cure level monitoring of symmetric carbon fibreepoxy composite laminates. In the first case study, the technique exhibits sensitivity in propagation paths within damaged areas and shows good agreement with the developed damage, enabling the identification of critical areas. In the second study, the technique demonstrates a significant advantage over the traditionally adopted approaches and predicts accurately the cure level of the polymeric composite system. Copyright  2012 John Wiley &amp; Sons, Ltd.</mods:abstract>
               <mods:genre authority="local" type="content-type">journal-article</mods:genre>
               <mods:genre type="version">Original research</mods:genre>
               <mods:genre type="form">Academic journal article</mods:genre>
               <mods:language>
                  <mods:languageTerm authority="iso639-3" type="code">eng</mods:languageTerm>
               </mods:language>
               <mods:note type="authors">Costas Soutis, S Pavlopoulou, WJ Staszewski</mods:note>
               <mods:originInfo>
                  <mods:dateIssued encoding="iso8601">2012</mods:dateIssued>
                  <mods:publisher>SAGE Publications</mods:publisher>
               </mods:originInfo>
               <mods:recordInfo>
                  <mods:recordCreationDate encoding="iso8601">2015-05-19T17:06:30.648+01:00</mods:recordCreationDate>
                  <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
               </mods:recordInfo>
               <mods:relatedItem type="host">
                  <mods:titleInfo>
                     <mods:title>Structural Health Monitoring</mods:title>
                  </mods:titleInfo>
                  <mods:part>
                     <mods:detail type="issue">
                        <mods:number>0</mods:number>
                     </mods:detail>
                     <mods:detail type="volume">
                        <mods:number>20</mods:number>
                     </mods:detail>
                     <mods:extent unit="page">
                        <mods:start/>
                        <mods:end/>
                        <mods:total>0</mods:total>
                        <mods:list/>
                     </mods:extent>
                  </mods:part>
                  <mods:identifier type="issn">1475-9217</mods:identifier>
               </mods:relatedItem>
               <mods:relatedItem type="preceding">
                  <mods:titleInfo>
                     <mods:title/>
                  </mods:titleInfo>
                  <mods:identifier type="uri"/>
               </mods:relatedItem>
               <mods:titleInfo>
                  <mods:title>Evaluation of instantaneous characteristics of guided ultrasonic waves for structural quality and health monitoring.  Structural Control and Health Monitoring</mods:title>
               </mods:titleInfo>
               <mods:typeOfResource>text</mods:typeOfResource>
            </mods:mods>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="RELS-EXT.1"
                               LABEL="Relationships"
                               MIMETYPE="text/xml"
                               SIZE="920">
         <foxml:xmlContent>
            <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#"
                     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
               <rdf:Description rdf:about="info:fedora/uk-ac-man-scw:ma204">
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:119765"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:1"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:39"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:209"/>
                  <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                  <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:1"/>
               </rdf:Description>
            </rdf:RDF>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
</foxml:digitalObject>
