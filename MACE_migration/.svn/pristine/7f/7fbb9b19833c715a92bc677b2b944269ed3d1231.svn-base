<?xml version="1.0" encoding="UTF-8"?>
<foxml:digitalObject xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                     VERSION="1.1"
                     PID="uk-ac-man-scw:ma030">
   <foxml:objectProperties>
      <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
   </foxml:objectProperties>
   <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="MODS.0"
                               LABEL="Metadata Object Description Schema record"
                               MIMETYPE="text/xml"
                               SIZE="3550">
         <foxml:xmlContent>
            <mods:mods xmlns:mods="http://www.loc.gov/mods/v3"
                       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       version="3.3"
                       xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd">
               <mods:abstract>A numerical approach based on the Lattice Boltzmann and Immersed Boundary methods is proposed to tackle the problem of the interaction of moving and/or deformable slender solids with an incompressible fluid flow. The method makes use of a Cartesian uniform lattice that encompasses both the fluid and the solid domains. The deforming/moving elements are tracked through a series of Lagrangian markers that are embedded in the computational domain. Differently from classical projection methods applied to advance in time the incompressible NavierStokes equations, the baseline Lattice Boltzmann fluid solver is free from pressure corrector step, which is known to affect the accuracy of the boundary conditions. Also, in contrast to other immersed boundary methods proposed in the literature, the proposed algorithm does not require the introduction of any empirical parameter. In the case of rigid bodies, the position of the markers delimiting the surface of an object is updated by tracking both the position of the centre of mass of the object and its rotation using Newton&amp;#700;s laws and the conservation of angular momentum. The dynamics of a flexible slender structure is determined as a function of the forces exerted by the fluid, its flexural rigidity and the tension necessary to enforce the filament inextensibility. For both rigid and deformable bodies, the instantaneous no-slip and impermeability conditions on the solid boundary are imposed via external and localised body forces which are consistently introduced into the Lattice Boltzmann equation. The validation test-cases for rigid bodies include the case of an impulsively started plate and the sedimentation of particles under gravity in a fluid initially at rest. For the case of deformable slender structures we consider the beating of both a single filament and a pair filaments induced by the interaction with an incoming uniformly streaming flow.</mods:abstract>
               <mods:genre authority="local" type="content-type">journal-article</mods:genre>
               <mods:genre type="version">Original research</mods:genre>
               <mods:genre type="form">Academic journal article</mods:genre>
               <mods:language>
                  <mods:languageTerm authority="iso639-3" type="code">eng</mods:languageTerm>
               </mods:language>
               <mods:note type="authors">A Pinelli, Alistair Revell, J Favier</mods:note>
               <mods:originInfo>
                  <mods:dateIssued encoding="iso8601">2014</mods:dateIssued>
                  <mods:publisher>Elsevier Science</mods:publisher>
               </mods:originInfo>
               <mods:recordInfo>
                  <mods:recordCreationDate encoding="iso8601">2015-05-19T17:06:30.648+01:00</mods:recordCreationDate>
                  <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
               </mods:recordInfo>
               <mods:relatedItem type="host">
                  <mods:titleInfo>
                     <mods:title>Journal of Computational Physics</mods:title>
                  </mods:titleInfo>
                  <mods:part>
                     <mods:detail type="issue">
                        <mods:number>0</mods:number>
                     </mods:detail>
                     <mods:detail type="volume">
                        <mods:number/>
                     </mods:detail>
                     <mods:extent unit="page">
                        <mods:start/>
                        <mods:end/>
                        <mods:total>0</mods:total>
                        <mods:list/>
                     </mods:extent>
                  </mods:part>
                  <mods:identifier type="issn">0021-9991</mods:identifier>
               </mods:relatedItem>
               <mods:relatedItem type="preceding">
                  <mods:titleInfo>
                     <mods:title/>
                  </mods:titleInfo>
                  <mods:identifier type="uri"/>
               </mods:relatedItem>
               <mods:titleInfo>
                  <mods:title>A Lattice Boltzmann  Immersed Boundary method to simulate the fluid interaction with moving and slender flexible objects</mods:title>
               </mods:titleInfo>
               <mods:typeOfResource>text</mods:typeOfResource>
            </mods:mods>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="RELS-EXT.1"
                               LABEL="Relationships"
                               MIMETYPE="text/xml"
                               SIZE="920">
         <foxml:xmlContent>
            <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#"
                     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
               <rdf:Description rdf:about="info:fedora/uk-ac-man-scw:ma030">
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde47"/>
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:40131"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:1"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:41"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:345"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:2"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:34"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:105"/>
                  <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                  <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:1"/>
               </rdf:Description>
            </rdf:RDF>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
</foxml:digitalObject>
