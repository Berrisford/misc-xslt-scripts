<?xml version="1.0" encoding="UTF-8"?>
<foxml:digitalObject xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                     VERSION="1.1"
                     PID="uk-ac-man-scw:ma330">
   <foxml:objectProperties>
      <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
   </foxml:objectProperties>
   <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="MODS.0"
                               LABEL="Metadata Object Description Schema record"
                               MIMETYPE="text/xml"
                               SIZE="3550">
         <foxml:xmlContent>
            <mods:mods xmlns:mods="http://www.loc.gov/mods/v3"
                       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       version="3.3"
                       xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd">
               <mods:abstract>Human mesenchymal stem cells (hMSCs) from umbilical cord (UC) blood (UCB) and matrix are tested clinically for a variety of pathologies but in vitro expansion using culture media containing fetal bovine serum (FBS) is essential to achieve appropriate cell numbers for clinical use. Human UCB plasma (hUCBP) can be used as a supplement for hMSCs culture, since UCB is rich in soluble growth factors and due to worldwide increased number of cryopreserved UCB units in public and private banks, without the disadvantages listed for FBS. On the other hand, the culture media enriched in growth factors produced by these hMSCs in expansion (Conditioned medium - CM) can be an alternative to hMSCs application. The CM of the hMSCs from the UC might be a better therapeutic option compared to cell transplantation, as it can benefit from the local tissue response to the secreted molecules without the difficulties and complications associated to the engraftment of the allo- or xeno-transplanted cells. These facts drove us to know the detailed composition of the hUCBP and CM, by 1H-NMR and Multiplexing LASER Bead Technology. hUCBP is an adequate alternative for the FBS and the CM and hUCBP are important sources of growth factors, which can be used in MSCs-based therapies. Some of the major proliferative, chemotactic and immunomodulatory soluble factors (TGF-, G-CSF, GM-CSF, MCP-1, IL-6, IL-8) were detected in high concentrations in CM and even higher in hUCBP. The results from 1H-NMR spectroscopic analysis of CM endorsed a better understanding of hMSCs metabolism during in vitro culture, and the relative composition of several metabolites present in CM and hUCBP was obtained. The data reinforces the potential use of hUCBP and CM in tissue regeneration and focus the possible use of hUCBP as a substitute for the FBS used in hMSCs in vitro culture.</mods:abstract>
               <mods:genre authority="local" type="content-type">journal-article</mods:genre>
               <mods:genre type="version">Original research</mods:genre>
               <mods:genre type="form">Academic journal article</mods:genre>
               <mods:language>
                  <mods:languageTerm authority="iso639-3" type="code">eng</mods:languageTerm>
               </mods:language>
               <mods:note type="authors">A Caseiro, A Luis, A Mauricio, G Ivanova, J Santos, P Barbosa, Paulo Bartolo, T Pereira</mods:note>
               <mods:originInfo>
                  <mods:dateIssued encoding="iso8601">2014</mods:dateIssued>
                  <mods:publisher>Public Library of Science (PLoS)</mods:publisher>
               </mods:originInfo>
               <mods:recordInfo>
                  <mods:recordCreationDate encoding="iso8601">2015-05-19T17:06:30.648+01:00</mods:recordCreationDate>
                  <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
               </mods:recordInfo>
               <mods:relatedItem type="host">
                  <mods:titleInfo>
                     <mods:title>PLoS ONE</mods:title>
                  </mods:titleInfo>
                  <mods:part>
                     <mods:detail type="issue">
                        <mods:number>11</mods:number>
                     </mods:detail>
                     <mods:detail type="volume">
                        <mods:number>9</mods:number>
                     </mods:detail>
                     <mods:extent unit="page">
                        <mods:start>e113769</mods:start>
                        <mods:end>e113769</mods:end>
                        <mods:total>0</mods:total>
                        <mods:list>e113769</mods:list>
                     </mods:extent>
                  </mods:part>
                  <mods:identifier type="issn"/>
               </mods:relatedItem>
               <mods:relatedItem type="preceding">
                  <mods:titleInfo>
                     <mods:title/>
                  </mods:titleInfo>
                  <mods:identifier type="uri"/>
               </mods:relatedItem>
               <mods:titleInfo>
                  <mods:title>MSCs CONDITIONED MEDIA AND UMBILICAL CORD BLOOD PLASMA METABOLOMICS AND COMPOSITION</mods:title>
               </mods:titleInfo>
               <mods:typeOfResource>text</mods:typeOfResource>
            </mods:mods>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="RELS-EXT.1"
                               LABEL="Relationships"
                               MIMETYPE="text/xml"
                               SIZE="920">
         <foxml:xmlContent>
            <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#"
                     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
               <rdf:Description rdf:about="info:fedora/uk-ac-man-scw:ma330">
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde37"/>
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde39"/>
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:133301"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:1"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:41"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:345"/>
                  <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                  <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:1"/>
               </rdf:Description>
            </rdf:RDF>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
</foxml:digitalObject>
