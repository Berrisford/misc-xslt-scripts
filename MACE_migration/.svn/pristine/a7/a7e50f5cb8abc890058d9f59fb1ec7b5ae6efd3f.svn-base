<?xml version="1.0" encoding="UTF-8"?>
<foxml:digitalObject xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                     VERSION="1.1"
                     PID="uk-ac-man-scw:ma308">
   <foxml:objectProperties>
      <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
   </foxml:objectProperties>
   <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="MODS.0"
                               LABEL="Metadata Object Description Schema record"
                               MIMETYPE="text/xml"
                               SIZE="3550">
         <foxml:xmlContent>
            <mods:mods xmlns:mods="http://www.loc.gov/mods/v3"
                       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       version="3.3"
                       xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd">
               <mods:abstract>Frequent failures of the pressuriser heater tubes used in Pressurised Water Reactors (PWRs) have been found. Axial cracks initiating from the tube outer diameter have been detected in some tubes as well as the resulting electrical problems. Replacement of the heater tubes requires an undesirably prolonged plant shutdown. In order to better understand these failures a series of residual stress measurements were carried out to obtain the near surface and through-thickness residual stress profiles in a stainless steel pressuriser heater tube. Three different residual stress measurement techniques were employed namely, Deep-Hole Drilling (DHD), Incremental Centre Hole Drilling (ICHD) and Sachs Boring (SB) to measure the through thickness residual stress distribution in the heater tubes. Results showed that the hoop stresses measured using all three techniques were predominantly tensile at all locations, while the axial stresses were found to be tensile at the surface and both tensile and compressive as they reduce to small magnitudes within the tube. The magnitude of the in-plane shear stresses was small at all measurement depths at all locations. The various measurement methods were found to complement each other well. All the measurements revealed a characteristic profile for the through-thickness residual stress distribution.</mods:abstract>
               <mods:genre authority="local" type="content-type">journal-article</mods:genre>
               <mods:genre type="version">Original research</mods:genre>
               <mods:genre type="form">Academic journal article</mods:genre>
               <mods:language>
                  <mods:languageTerm authority="iso639-3" type="code">eng</mods:languageTerm>
               </mods:language>
               <mods:note type="authors">D M Goudar, E J Kingston, Michael Smith, S Hossain</mods:note>
               <mods:originInfo>
                  <mods:dateIssued encoding="iso8601">2011</mods:dateIssued>
                  <mods:publisher/>
               </mods:originInfo>
               <mods:recordInfo>
                  <mods:recordCreationDate encoding="iso8601">2015-05-19T17:06:30.648+01:00</mods:recordCreationDate>
                  <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
               </mods:recordInfo>
               <mods:relatedItem type="host">
                  <mods:titleInfo>
                     <mods:title>Applied Mechanics and Materials, AMM</mods:title>
                  </mods:titleInfo>
                  <mods:part>
                     <mods:detail type="issue">
                        <mods:number>0</mods:number>
                     </mods:detail>
                     <mods:detail type="volume">
                        <mods:number>70</mods:number>
                     </mods:detail>
                     <mods:extent unit="page">
                        <mods:start>279</mods:start>
                        <mods:end>284</mods:end>
                        <mods:total>5</mods:total>
                        <mods:list>279-284</mods:list>
                     </mods:extent>
                  </mods:part>
                  <mods:identifier type="issn">1662-7482</mods:identifier>
               </mods:relatedItem>
               <mods:relatedItem type="preceding">
                  <mods:titleInfo>
                     <mods:title/>
                  </mods:titleInfo>
                  <mods:identifier type="uri"/>
               </mods:relatedItem>
               <mods:titleInfo>
                  <mods:title>Measurement of residual stresses within a PWR swaged heater tube</mods:title>
               </mods:titleInfo>
               <mods:typeOfResource>text</mods:typeOfResource>
            </mods:mods>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="RELS-EXT.1"
                               LABEL="Relationships"
                               MIMETYPE="text/xml"
                               SIZE="920">
         <foxml:xmlContent>
            <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#"
                     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
               <rdf:Description rdf:about="info:fedora/uk-ac-man-scw:ma308">
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde39"/>
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde41"/>
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde42"/>
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde47"/>
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:122666"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:1"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:41"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:203"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:1"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:41"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:347"/>
                  <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                  <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:1"/>
               </rdf:Description>
            </rdf:RDF>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
</foxml:digitalObject>
