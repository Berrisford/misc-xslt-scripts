<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">    
    <xsl:output encoding="UTF-8" method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <root>
            <count>
                <xsl:value-of select="count(//publication)"/>
            </count>
            <dateTime>
                <xsl:choose>
                    <xsl:when test="contains(string(current-dateTime()), '+')">
                        <xsl:value-of select="concat(substring-before(string(current-dateTime()), '+'), 'Z')"/>                        
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat(string(current-dateTime()), 'Z')"/>                        
                    </xsl:otherwise>
                </xsl:choose>
            </dateTime>            
        </root>
    </xsl:template>
    
</xsl:stylesheet>