<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" name="xml"/>

    <xsl:variable name="root_url" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/nonscw/select/'"></xsl:variable>
    <!--<xsl:variable name="url" select="concat($root_url, '?q=r.isofcontenttype.pid%3A', 'uk-ac-man-con?1', '&amp;fl=PID&amp;omitHeader=true')"></xsl:variable>-->
<!--
    <xsl:variable name="obj_doc">
        <xsl:copy-of select="doc(concat($root_url, '?q=PID:', $pid, '&amp;omitHeader=true&amp;start=0&amp;rows=10'))/response/result/doc"/>            
    </xsl:variable>
 -->
 
    <xsl:template match="/">
        <xsl:for-each select="publications/publication">
            <xsl:variable name="pid_count">
                <xsl:number />
            </xsl:variable>
            <xsl:variable name="pid_name">
                <!-- BA: Change here... -->
                <xsl:choose>
                    <xsl:when test="number($pid_count) lt 10">
                        <xsl:value-of select="concat('ma', '00', $pid_count)" />                        
                    </xsl:when>
                    <xsl:when test="number($pid_count) gt 9 and number($pid_count) lt 100">
                        <xsl:value-of select="concat('ma', '0', $pid_count)" />                                                
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat('ma', $pid_count)" />                                                
                    </xsl:otherwise>
                </xsl:choose>
                <!--<xsl:value-of select="concat('ma', 999 + $pid_count)" />-->                                                
            </xsl:variable>
            <xsl:variable name="file">
                <!-- BA: Change here... -->
                <xsl:value-of select="concat('output/', 'uk-ac-man-scw_', $pid_name, '.xml')" />
            </xsl:variable>
            <xsl:result-document href="{$file}" method="xml" indent="yes">
                <xsl:call-template name="publication_foxml">
                    <xsl:with-param name="pid_name" select="$pid_name"/>
                </xsl:call-template>
            </xsl:result-document>            
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="publication_foxml">
        <xsl:param name="pid_name"/>        
        <xsl:variable name="sequence"><xsl:number/></xsl:variable>
        <foxml:digitalObject VERSION="1.1" xmlns:foxml="info:fedora/fedora-system:def/foxml#">
            <!--<xsl:attribute name="PID"><xsl:call-template name="createPid"><xsl:with-param name="sequence" select="$sequence"/></xsl:call-template></xsl:attribute>-->
            <xsl:attribute name="PID">
                <xsl:call-template name="createPid">
                    <xsl:with-param name="sequence" select="$pid_name"/>
                </xsl:call-template>
            </xsl:attribute>
            <foxml:objectProperties>
                <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
            </foxml:objectProperties>

            <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
                <xsl:element name="foxml:datastreamVersion">
                    <xsl:attribute name="CREATED">
                        <xsl:call-template name="getCurrentDateTime"/>
                    </xsl:attribute>
                    <xsl:attribute name="ID" select="'MODS.0'"/>
                    <xsl:attribute name="LABEL" select="'Metadata Object Description Schema record'"/>
                    <xsl:attribute name="MIMETYPE" select="'text/xml'"/>
                    <xsl:attribute name="SIZE" select="'3550'"/>
                    
                    <foxml:xmlContent>
                        <mods:mods version="3.3" xmlns:mods="http://www.loc.gov/mods/v3"
                            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd">
                            <mods:abstract><xsl:value-of select="paperabstract"/></mods:abstract>
                            <mods:genre authority="local" type="content-type">journal-article</mods:genre>
                            <mods:genre type="version">Original research</mods:genre>
                            <mods:genre type="form">Academic journal article</mods:genre>
                            <mods:language>
                                <mods:languageTerm authority="iso639-3" type="code">eng</mods:languageTerm>
                            </mods:language>
                            <mods:note type="authors"><xsl:value-of select="replace(owners, ',', ', ')"/></mods:note>
                            <mods:originInfo>
                                <!--<mods:dateIssued encoding="iso8601"><xsl:value-of select="concat(publishyear, '-01-01')"/></mods:dateIssued>-->
                                <mods:dateIssued encoding="iso8601"><xsl:value-of select="publishyear"/></mods:dateIssued>
                                <mods:publisher><xsl:value-of select="publisher"/></mods:publisher>
                            </mods:originInfo>
                            <mods:recordInfo>
                                <mods:recordCreationDate encoding="iso8601"><xsl:value-of select="current-dateTime()"/></mods:recordCreationDate>
                                <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
                            </mods:recordInfo>
                            <mods:relatedItem type="host">
                                <mods:titleInfo>
                                    <mods:title><xsl:value-of select="papername"/></mods:title>
                                </mods:titleInfo>
                                <mods:part>
                                    <mods:detail type="issue">
                                        <mods:number><xsl:value-of select="number"/></mods:number>
                                    </mods:detail>
                                    <mods:detail type="volume">
                                        <mods:number><xsl:value-of select="vol"/></mods:number>
                                    </mods:detail>
                                    <mods:extent unit="page">
                                        <xsl:choose>
                                            <xsl:when test="contains(page, '-')">
                                                <mods:start><xsl:value-of select="tokenize(page, '-')[1]"/></mods:start>
                                                <mods:end><xsl:value-of select="tokenize(page, '-')[2]"/></mods:end>
                                                <mods:total><xsl:value-of select="number(tokenize(page, '-')[2]) - number(tokenize(page, '-')[1])"/></mods:total>                                                
                                            </xsl:when>
                                            <xsl:when test="not(contains(page, '-')) and page">
                                                <mods:start><xsl:value-of select="page"/></mods:start>
                                                <mods:end><xsl:value-of select="page"/></mods:end>
                                                <mods:total><xsl:value-of select="0"/></mods:total>                                                                                                
                                            </xsl:when>
                                            <xsl:when test="not(page)">
                                                <mods:start></mods:start>
                                                <mods:end></mods:end>
                                                <mods:total></mods:total>                                                                                                                                                
                                            </xsl:when>
                                        </xsl:choose>
                                        <mods:list><xsl:value-of select="page"/></mods:list>
                                    </mods:extent>
                                </mods:part>
                                <mods:identifier type="issn"><xsl:value-of select="issn-no"/></mods:identifier>
                            </mods:relatedItem>
                            <mods:relatedItem type="preceding">
                                <mods:titleInfo>
                                    <mods:title></mods:title>
                                </mods:titleInfo>
                                <mods:identifier type="uri"><xsl:value-of select="weblink"/></mods:identifier>
                            </mods:relatedItem>                                                       
                            <mods:titleInfo>
                                <mods:title><xsl:value-of select="title"/></mods:title>
                            </mods:titleInfo>
                            <mods:typeOfResource>text</mods:typeOfResource>
                        </mods:mods>
                    </foxml:xmlContent>                    
                </xsl:element>                                
            </foxml:datastream>

            <xsl:choose>
                <xsl:when test="pdf-filename ne ''">
                    <foxml:datastream CONTROL_GROUP="M" ID="FULL-TEXT.PDF" STATE="A" VERSIONABLE="false">
                        <xsl:element name="foxml:datastreamVersion">
                            <xsl:attribute name="ID" select="'FULL-TEXT.PDF.0'"/>
                            <xsl:attribute name="LABEL" select="pdf-filename"/>
                            <xsl:attribute name="MIMETYPE" select="'application/pdf'"/>
                            <foxml:contentLocation TYPE="URL">
                                <xsl:attribute name="REF">
                                    <xsl:value-of select="concat('http://fedprdir.library.manchester.ac.uk/uploaded/mace/', replace(pdf-filename, ' ', ''))"/>
                                </xsl:attribute>
                            </foxml:contentLocation>                            
                        </xsl:element>
                    </foxml:datastream>
                </xsl:when>
            </xsl:choose>            
            
            <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
                <xsl:element name="foxml:datastreamVersion">
                    <xsl:attribute name="CREATED">
                        <xsl:call-template name="getCurrentDateTime"/>
                    </xsl:attribute>
                    <xsl:attribute name="ID" select="'RELS-EXT.1'"/>
                    <xsl:attribute name="LABEL" select="'Relationships'"/>
                    <xsl:attribute name="MIMETYPE" select="'text/xml'"/>
                    <xsl:attribute name="SIZE" select="920"/>
                    <foxml:xmlContent>
                        <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                            <rdf:Description>
<!--
                                <xsl:variable name="pid_name">
                                    <!-\- BA: Change here... -\->
                                    <xsl:value-of select="id" />
                                    
                                </xsl:variable>                                
-->
                                <xsl:attribute name="rdf:about">info:fedora/<xsl:call-template name="createPid"><xsl:with-param name="sequence" select="$pid_name"/></xsl:call-template></xsl:attribute>
                                <!-- BA: Change here... -->
                                <xsl:for-each select="tokenize(esc-group-id, ',')">
                                    <xsl:element name="esc-rel:isBelongsTo">
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of select="concat('info:fedora/uk-ac-man-per:', normalize-space(.))"></xsl:value-of>
                                        </xsl:attribute>
                                    </xsl:element>
                                </xsl:for-each>
                                <xsl:for-each select="tokenize(spotids, ',')">
                                    <xsl:variable name="query">
                                        <xsl:value-of select="concat($root_url, '?q=p.partynumber%3A', normalize-space(.), '&amp;omitHeader=true')"></xsl:value-of>
                                    </xsl:variable>
                                    <xsl:variable name="obj_solr_doc">
                                        <xsl:copy-of select="doc($query)/response/result/doc"/>
                                    </xsl:variable>
                                    <xsl:choose>
                                        <xsl:when test="$obj_solr_doc/doc/str[@name='PID'] ne ''">
                                            <xsl:element name="esc-rel:isBelongsTo">
                                                <xsl:attribute name="rdf:resource">
                                                    <xsl:value-of select="concat('info:fedora/', $obj_solr_doc/doc/str[@name='PID'])"/>
                                                </xsl:attribute>
                                            </xsl:element>
                                            <xsl:for-each select="$obj_solr_doc/doc/arr[@name='r.ismemberof.pid']/str">
                                                <xsl:choose>
                                                    <xsl:when test=". ne ''">
                                                        <xsl:element name="esc-rel:isBelongsToOrg">
                                                            <xsl:attribute name="rdf:resource">
                                                                <xsl:value-of select="concat('info:fedora/', normalize-space(.))"/>
                                                            </xsl:attribute>
                                                        </xsl:element>                                                                                                        
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:for-each>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:for-each>
                                <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                                <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                                <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                                <!-- BA: Change here... -->
                                <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:1"/>
                            </rdf:Description>
                        </rdf:RDF>
                    </foxml:xmlContent>                    
                </xsl:element>
            </foxml:datastream>                    
        </foxml:digitalObject>       
    </xsl:template>

    <xsl:template name="createPid">
        <xsl:param name="sequence"/>
        <!-- BA: Change here... -->
        <xsl:text>uk-ac-man-scw:</xsl:text><xsl:value-of select="$sequence"/>
    </xsl:template>

    <xsl:template name="showDate">
        <xsl:value-of select="Year"/><xsl:text>-&#8206;</xsl:text>
        <xsl:value-of select="Month"/>
        <xsl:text>-&#8206;</xsl:text><xsl:value-of select="Day"/>
    </xsl:template>
    
    <xsl:template name="getCurrentDateTime">
        <xsl:choose>
            <xsl:when test="contains(string(current-dateTime()), '+')">
                <xsl:value-of select="concat(substring-before(string(current-dateTime()), '+'), 'Z')"/>                        
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat(string(current-dateTime()), 'Z')"/>                        
            </xsl:otherwise>
        </xsl:choose>        
    </xsl:template>
    
</xsl:stylesheet>