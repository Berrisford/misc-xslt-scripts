<?xml version="1.0" encoding="UTF-8"?>
<foxml:digitalObject xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     VERSION="1.1"
                     xsi:schemaLocation="info:fedora/fedora-system:def/foxml# http://www.fedora.info/definitions/1/0/foxml1-1.xsd"
                     PID="uk-ac-man-scw:ths0017">
   <foxml:objectProperties>
      <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
   </foxml:objectProperties>
   <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="MODS.0"
                               LABEL="Metadata Object Description Schema record"
                               MIMETYPE="text/xml"
                               SIZE="4986">
         <foxml:xmlContent>
            <mods:mods xmlns:ev="http://www.w3.org/2001/xml-events"
                       xmlns:exf="http://www.exforms.org/exf/1-0"
                       xmlns:f="http://orbeon.org/oxf/xml/formatting"
                       xmlns:fr="http://orbeon.org/oxf/xml/form-runner"
                       xmlns:mods="http://www.loc.gov/mods/v3"
                       xmlns:widget="http://orbeon.org/oxf/xml/widget"
                       xmlns:xforms="http://www.w3.org/2002/xforms"
                       xmlns:xhtml="http://www.w3.org/1999/xhtml"
                       xmlns:xi="http://www.w3.org/2001/XInclude"
                       xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:xxforms="http://orbeon.org/oxf/xml/xforms"
                       xmlns:xxi="http://orbeon.org/oxf/xml/xinclude"
                       version="3.3">
               <mods:abstract>ABSTRACT
Background
Perinatal depression purportedly affects between fifteen and twenty percent of women
giving birth to live babies. Compared with South Asian and White British women, there
has been little epidemiological or qualitative research into perinatal psychopathology
among Black Caribbean women in the UK. Since research has consistently linked
poorer mental health with ethnicity and deprivation, it was hypothesised that Black
Caribbean women who are disproportionately located within deprived areas of the UK,
would be at greater risk of perinatal depression than White British women.
Aims and objectives
In light of the absence of epidemiological and qualitative research into perinatal
depression among Black Caribbean women, the purpose of the study was to:
estimate the prevalence of perinatal depressive symptoms among a cohort of Black
Caribbean women and to compare prevalence levels with White British women
living in the same geographical area
identify psychosocial risk factors associated with above-threshold EPDS scores
develop predictive models for Black Caribbean and White British women
explore Black Caribbean women's beliefs about the genesis, nature, management,
and outcome of perinatal depression and their attitudes to help-seeking
Methods
A multi-method longitudinal study was conducted amongst a self-selected sample of
200 White British and 101 Black Caribbean women in Manchester, UK. Women were
screened in the last trimester of pregnancy using self-completion questionnaires, which
incorporated the Edinburgh Postnatal Depression Scale (EPDS), and again six weeks
following delivery.
A purposive sample of twelve Black Caribbean women, who completed both phases of
the epidemiological study, were interviewed between six and twelve months following
delivery. Interviews were taped, transcribed, and thematically analysed.
Results
Black Caribbean women were significantly more likely to have been lone parents, to
have lived in the most deprived areas of the city, and to have received little or no
13
support from their partners. However, despite being at greater theoretical risk of
depression, they were significantly less likely than White British women to record
above-threshold EPDS scores during pregnancy (p = .
041). Although, postnatally a
greater proportion of Black Caribbean women scored above threshold (EPDS &gt;_12),
results were not statistically significant. In spite of evidence of perinatal depressive
symptoms, Black Caribbean women were also significantly less likely to have received
treatment for depression or postnatal depression.
Using logistic regression analyses, three key variables consistently predicted abovethreshold
EPDS scores: inability to go out and negative feelings immediately following
delivery predicted high depression scores for both groups of women. However, having
been previously treated for depression/postnatal depression predicted postnatal
depression for Black Caribbean (OR 31.46 (CI 2.58 - 383.63) p= .
007) but not for
White British women.
Findings from the qualitative study revealed that Black Caribbean women had difficulty
conceptualising perinatal depression. They attributed this difficulty to socio-cultural
imperatives to minimise distress, unwillingness to acknowledge or discuss depressive
symptoms, reluctance to formulate depressive feelings as depressive illness, and the
absence of social discourse. Drawing on labelling theories and identity formation,
women's personal and social imperatives to be seen as `Strong Black Women', coupled
with fear that pharmacological treatment would lead to eventual re-diagnosis with more
serious mental illnesses emerged as the most powerful barriers to help-seeking. Personal
autonomy, spirituality, and having children were regarded by women as the most
effective protectors from perinatal depression.
Implications for policy and practice
These findings suggest that focusing on 3 key variables might assist healthcare
providers to identify women at risk of perinatal depression. Findings also suggest that
Black Caribbean women used a range of coping strategies to manage adversity and
distress and that policy makers and healthcare providers may need to tailor health
education, service provision, and training of health professionals to address the
particular needs of Black Caribbean women in relation to the recognition, diagnosis,
and treatment of perinatal mental health.</mods:abstract>
               <mods:genre type="content-type" authority="local">thesis</mods:genre>
               <mods:genre type="version">Alternative format</mods:genre>
               <mods:note type="authors">Edge, Dawnette E.</mods:note>
               <mods:note type="degreelevel">Doctor of Philosophy</mods:note>
               <mods:originInfo>
                  <mods:dateIssued encoding="iso8601">2002</mods:dateIssued>
                  <mods:place>
                     <mods:placeTerm>Manchester, UK</mods:placeTerm>
                  </mods:place>
                  <mods:publisher>University of Manchester</mods:publisher>
               </mods:originInfo>
               <mods:part>
                  <mods:extent unit="page">
                     <mods:total/>
                  </mods:extent>
               </mods:part>
               <mods:recordInfo>
                  <mods:recordCreationDate encoding="iso8601">2017-02-10T16:09:30.065Z</mods:recordCreationDate>
                  <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
               </mods:recordInfo>
               <mods:subject>
                  <mods:topic>Health</mods:topic>
                  <mods:topic>Mental Health</mods:topic>
                  <mods:topic>Black women</mods:topic>
                  <mods:topic>Depression, Postnatal</mods:topic>
                  <mods:topic>Mental illness in pregnancy</mods:topic>
                  <mods:topic>Maternal health services</mods:topic>
                  <mods:topic>Great Britain</mods:topic>
                  <mods:topic>Ethnicity</mods:topic>
                  <mods:topic>Health aspects</mods:topic>
                  <mods:topic>Great Britain</mods:topic>
               </mods:subject>
               <mods:titleInfo>
                  <mods:title>Perinatal depression among women of black Caribbean origin : a longitudinal cohort study of prevalence, beliefs and attitudes to help-seeking</mods:title>
               </mods:titleInfo>
               <mods:tableOfContents>TABLE OF CONTENTS
TABLE OF CONTENTS
...............................................................................................
2
LIST OF TABLES
..........................................................................................................
7
LIST OF FIGURES ........................................................................................................
9
LIST OF ABBREVIATIONS
......................................................................................
11
LIST OF APPENDICES ..............................................................................................
12
ABSTRACT ..................................................................................................................
13
DECLARATION ..........................................................................................................
15
COPYRIGHT ...............................................................................................................
15
ACKNOWLEDGEMENTS .........................................................................................
16
DEDICATION ..............................................................................................................
16
THE AUTHOR .............................................................................................................
17
INTRODUCTION TO THE THESIS ........................................................................
18
CHAPTER 1- ETHNICITY AND MENTAL ILLNESS .........................................
23
1.1. INTRODUCTION ...........................................................................................................................
23
1.2. ETHNICITY AND MENTAL ILLNESS
........................................................................................
30
1.2.1. Psychoses, neuroses, and Black Caribbeans
.............................................................................
33
1.2.2. Ethnicity, race, and culture .......................................................................................................
37
1.2.3. Ethnicity and mental health research ........................................................................................
40
1.2.4. Black Caribbean people in Britain
...........................................................................................
43
1.2.4.1. Historical and contemporary perspectives .........................................................................
43
1.2.4.2. Material and social position of Black Caribbeans in contemporary Britain
......................
45
1.3. SUMMARY ....................................................................................................................................
46
CHAPTER 2: PERINATAL DEPRESSION .............................................................
49
2.1. INTRODUCTION ...........................................................................................................................
49
2.2. WOMEN AND DEPRESSION .......................................................................................................
49
2.3. PERINATAL DEPRESSION - HISTORICAL PERSPECTIVES ..................................................
56
2.4. POSTNATAL DEPRESSION .........................................................................................................
58
2
2.4.1. Classification of postnatal depression
......................................................................................
59
2.4.2. Criteria for diagnosis with postnatal depression
.......................................................................
62
2.4.3. Postnatal depression - discreet diagnostic category or continuum ...........................................
64
2.4.4. Aetiology of Postnatal Depression
...........................................................................................
66
2.4.4.1. Biological models ..............................................................................................................
66
2.4.4.2. Psychological models ........................................................................................................
68
2.4.4.3. Social models ....................................................................................................................
69
2.4.4.4. Biopsychosocial models ....................................................................................................
71
2.4.5. Aetiological models and unanswered questions .......................................................................
72
2.4.6. Predicting Postnatal Depression
...............................................................................................
72
2.4.7. Epidemiology of postnatal depression
......................................................................................
74
2.5. DEPRESSION DURING PREGNANCY
.......................................................................................
77
2.5.1. Risk factors for depression during pregnancy ..........................................................................
79
2.5.2. The Epidemiology of Antenatal Depression
............................................................................
80
2.5.3. The relationship between antenatal and postnatal depression
..................................................
81
2.5.4. The measurement of antenatal and postnatal depression
..........................................................
82
2.5.4.1. Perinatal Depression and the EPDS
...................................................................................
83
2.6. SUMMARY
....................................................................................................................................
85
2.7. RESEARCH QUESTIONS
.............................................................................................................
87
2.8. HYPOTHESES
...............................................................................................................................
88
CHAPTER 3: METHODOLOGY ..............................................................................
89
3.1. RATIONALE FOR A MULTI- METHOD APPROACH
..............................................................
89
3.1.1. The quantitative versus qualitative debate
................................................................................
89
3.1.1.1. Quantitative methods .........................................................................................................
89
3.1.1.2. Qualitative methods ...........................................................................................................
90
3.1.1.3. Quantitative and qualitative methods - complimentary paradigms ...................................
91
3.2. MULTI-METHOD APPROACHES - CONTEXT AND RATIONALE FOR THIS STUDY
.......
92
3.4. SUMMARY
....................................................................................................................................
96
CHAPTER 4: QUANTITATIVE METHODS
..........................................................
97
4.1. RATIONALE FOR THE QUANTITATIVE STUDY ....................................................................
97
4.2. AIMS AND OBJECTIVES OF THE QUANTITATIVE STUDY .................................................
97
4.3. RESEARCH DESIGN ....................................................................................................................
98
4.3.1. Research instruments
................................................................................................................
98
4.3.1.1. Design and development of the research instruments
.......................................................
98
4.3.1.2. The Edinburgh Postnatal Depression Scale (EPDS)
..........................................................
98
4.3.2. Rationale for use of the EPDS in this study ...........................................................................
106
4.3.3. Development and piloting of antenatal and postnatal survey questionnaires .........................
107
4.3.3.1. Antenatal Questionnaire
..................................................................................................
108
4.3.3.2. Postnatal Questionnaire ...................................................................................................
111
4.4. SAMPLING AND PROCEDURE ................................................................................................
113
4.4.1. Piloting of questionnaires .......................................................................................................
113
4.4.2. Location of the research .........................................................................................................
115
4.4.3. Sample size and ethnic composition .......................................................................................
116
4.4.3.1. Inclusion and Exclusion Criteria
.....................................................................................
118
4.4.4. Data collection ........................................................................................................................
119
4.4.4.1. Antenatal data collection .................................................................................................
119
4.4.4.2. Postnatal data collection ..................................................................................................
120
4.4.5. Ethical issues
..........................................................................................................................
122
4.5. DATA MANAGEMENT &amp; ANALYSIS
.....................................................................................
125
4.5.1. Data management ...................................................................................................................
125
4.5.2. Data analysis ..........................................................................................................................
127
4.5.2.1. Descriptive statistics ........................................................................................................
127
4.5.2.2. The measurement of depressive symptoms .....................................................................
129
4.5.2.3. The relationship between depression and key predictor variables ...................................
129
4.5.2.4. Predictors of depression for Black Caribbean and White British women .......................
131
3
CHAPTER 5: RESULTS OF THE QUANTITATIVE STUDY
............................ 134
5.1. SECTION 1: RESULTS OF THE ANTENATAL SURVEY
.....................................................1..3 4
5.1.1. Sample size and characteristicso f the sample. ....................................................................1..3. 4
5.1.1.1. Ethnic composition ..........................................................................................................
134
5.1.1.2. Age
..................................................................................................................................
135
5.1.1.3. Marital status ...................................................................................................................
136
5.1.1.4. Socioeconomic status ......................................................................................................
136
5.1.2. Parity
......................................................................................................................................
140
5.1.3. Children in household
............................................................................................................
140
5.1.4. Household composition ..........................................................................................................
141
5.1.5. Partner support .......................................................................................................................
142
5.1.5.1. Length of time with husband or partner ..........................................................................
142
5.1.5.2. Partner support during pregnancy ....................................................................................
142
5.1.6. Contact and support during pregnancy ...................................................................................
143
5.1.6.1. `Regular contact' during pregnancy ................................................................................
143
5.1.6.2. Perceived support during pregnancy ...............................................................................
144
5.1.7. Ability to get out during pregnancy ........................................................................................
145
5.1.8. Antenatal mental health
..........................................................................................................
146
5.1.8.1. History of depression
.......................................................................................................
146
5.1.8.2. History of postnatal depression
.......................................................................................
147
5.1.8.3. History of other mental illness
........................................................................................
147
5.1.9. Antenatal Life Events and Difficulties
...................................................................................
148
5.1.10. Antenatal depression scores - The Edinburgh Postnatal Depression Scale (EPDS)
.............
148
5.1.10.1. EPDS - individual items
................................................................................................
148
5.1.10.2. Mean EPDS scores ........................................................................................................
151
5.1.10.3. EPDS Cut-off scores .....................................................................................................
151
5.1.10.4. Relationship between antenatal variables and antenatal EPDS score - Logistic
Regression analyses ......................................................................................................................
152
5.1.11. SUMMARY OF ANTENATAL FINDINGS
...........................................................................
162
5.1.11.1. Testing the Antenatal Hypotheses
.....................................................................................
164
Hypothesis 1
.....................................................................................................................................
164
Hypothesis 2
.....................................................................................................................................
164
5.2. SECTION 2: RESULTS OF THE POSTNATAL SURVEY
........................................................
166
5.2.1. Sample size and characteristics of the sample ........................................................................
166
5.2.2. Characteristics of non-respondents .........................................................................................
166
5.2.3. Characteristics of the postnatal sample ..................................................................................
170
5.2.3.1. Ethnic composition ..........................................................................................................
170
5.2.3.2. Age
..................................................................................................................................
170
5.2.3.3. Marital status ...................................................................................................................
170
5.2.3.4. Socioeconomic status ..........................................................................................................
171
5.2.4. Reflections on pregnancy .......................................................................................................
172
5.2.4.1. Feelings during pregnancy ..............................................................................................
172
5.2.4.2. Perceptions of partner support during pregnancy ............................................................
173
5.2.4.3. Ability to `go out and meet people' during pregnancy ....................................................
173
5.2.4.4. Regular contact during pregnancy ...................................................................................
174
5.2.5. `About your labour and delivery' - obstetric risk factors
.......................................................
175
5.2.5.1. Perceptions of labour
.......................................................................................................
175
5.2.5.2. Mode of delivery
.............................................................................................................
176
5.2.5.3. Perceptions of control during labour and delivery
...........................................................
176
5.2.5.4. Feelings following delivery
.............................................................................................
177
5.2.6. `Since having your baby' - reflections on early motherhood .................................................
177
5.2.6.1. Babies' health and temperament ......................................................................................
177
5.2.6.2. Postnatal partner support .................................................................................................
177
5.2.6.3. Social support in the postnatal period ..............................................................................
178
5.2.6.4. Sources of postnatal support ............................................................................................
178
5.2.6.5. Ability to go out following childbirth ..............................................................................
179
5.2.6.6. Postnatal life events and difficulties
................................................................................
181
5.2.7. Postnatal mental health
...........................................................................................................
181
5.2.7.1. History of depression
.......................................................................................................
181
4
5.2.7.2. History of postnatal depression
....................................................................................... 182
5.2.7.3. History of other mental illness
........................................................................................ 183
5.2.8. Postnatal depression scores - The Edinburgh Postnatal Depression Scale (EPDS)
................
184
5.2.8.1. EPDS - individual item scores ......................................................................................... 184
5.2.8.2. EPDS Mean scores ..........................................................................................................
186
5.2.8.3. EPDS Cut-off scores .......................................................................................................
187
5.2.8.4. EPDS scores over time .................................................................................................... 188
5.2.8.5. Predicting above-threshold postnatal EPDS scores .........................................................
191
5.2.8.5.1. Predicting above-threshold postnatal EPDS scores from antenatal variables ...............
191
5.2.8.5.2. Predicting above-threshold postnatal EPDS scores from postnatal variables ...............
198
5.2.8.5.3. Predicting above-threshold postnatal EPDS scores from postnatal variables - effect of
antenatal EPDS scores ..................................................................................................................
204
5.2.9. SUMMARY OF POSTNATAL FINDINGS
.............................................................................
207
5.2.9.1. Testing the Postnatal Hypotheses
........................................................................................
211
Hypothesis 3
.....................................................................................................................................
211
Hypothesis 4
.....................................................................................................................................
211
CHAPTER 6: QUALITATIVE METHODS ...........................................................
212
6.1. INTRODUCTION
.........................................................................................................................
212
6.1.1. Objectives of the qualitative study: ........................................................................................
213
6.2. METHOD
......................................................................................................................................
214
6.2.1. Interviewing women -a rationale ...........................................................................................
214
6.2.2. A purposeful sample ...............................................................................................................
215
6.2.3. Instrumentation
.......................................................................................................................
217
6.2.4. Procedure
................................................................................................................................
218
6.2.4.1. The research setting .........................................................................................................
219
6.2.4.2. Attempting to do it right ..................................................................................................
220
6.2.4.3. Data generation and data gathering .................................................................................
223
6.2.4.4. Ethical issues in the field
.................................................................................................
228
6.2.5. Data Analysis
.........................................................................................................................
231
6.2.5.1. Transcription
...................................................................................................................
232
6.2.5.2. Managing the data
...........................................................................................................
233
6.2.5.3. Analysis of the data
.........................................................................................................
235
6.2.5.4. Verification and confirmation of findings
.......................................................................
243
6.3. SUMMARY ..................................................................................................................................
248
CHAPTER 7: WOMEN'S THEORIES ON THE NATURE AND
CONSTRUCTION OF DEPRESSIVE ILLNESS ...................................................
249
7.1. WOMEN'S CONSTRUCTION OF PERINATAL DEPRESSION ..............................................
249
7.1.1. Psychological construction .....................................................................................................
249
7.1.2. Social construction .................................................................................................................
250
7.1.3. Biological construction ...........................................................................................................
252
7.2. WOMEN'S CONSTRUCTION OF THE NATURE OF DEPRESSIVE ILLNESS
.....................
253
7.2.1. Familiarity with formal definitions of depression
..................................................................
254
7.2.2. Depression - continuum versus categorical perspectives .......................................................
256
7.2.3. The relationship between antenatal and postnatal depression
................................................
258
7.2.4. Depression as a sign of (moral? ) weakness ............................................................................
260
7.2.5. Black women, White women, and depressive illness
.............................................................
261
7.2.6. Protectors from depression
.....................................................................................................
266
7.2.7. The symptoms of depression
..................................................................................................
271
7.3. SUMMARY ..................................................................................................................................
274
5
CHAPTER 8: STRATEGIES FOR MANAGING ADVERSITY AND
PSYCHOLOGICAL DISTRESS .............................................................................2. 77
8.1. STRATEGIES FOR DEALING WITH ADVERSITY AND PREVENTING ONSET OF
DEPRESSION
......................................................................................................................................
277
8.2. HELP-SEEKING
..........................................................................................................................
287
8.2.1. Barriers to help-seeking
..........................................................................................................
288
8.2.1.1. Negative interactions with professionals as barrier to help-seeking
................................
291
8.2.2. Resistance to being labelled with depression
.........................................................................
292
8.2.3. Treatment for antenatal and postnatal depression
..................................................................
296
8.2.3.1. Acceptable treatment for antenatal and postnatal depression
..........................................
296
8.2.3.1.1. `You don't talk your business' - conflict between women's needs and social imperatives
......................................................................................................................................................
298
8.2.3.2. Unacceptable treatment for antenatal and postnatal depression
......................................
300
8.3. SUMMARY
..................................................................................................................................
304
CHAPTER 9: DISCUSSION .....................................................................................
307
9.1. INTRODUCTION
.........................................................................................................................
307
9.1.1. The study in context ...............................................................................................................
307
9.2. FINDINGS OF THE STUDY
......................................................................................................
310
9.2.1. EPDS Scores and prediction of perinatal depression
.............................................................
315
9.2.2. Factors which mediate between adversity and onset of depressive symptoms .......................
318
9.2.3. Factors which mediate between depressive symptoms and depressive illness
.......................
322
9.2.4. Factors which influence women's help-seeking behaviours
..................................................
324
9.3. METHODOLOGICAL ISSUES
..................................................................................................
328
9.4. CONCLUSIONS
...........................................................................................................................
334
9.5. IMPLICATIONS FOR POLICY AND PRACTICE
....................................................................
336
9.6. RECOMMENDATIONS FOR FUTURE RESEARCH
..............................................................
340
REFERENCES ...........................................................................................................
342
APPENDICES ............................................................................................................
365</mods:tableOfContents>
               <mods:typeOfResource>text</mods:typeOfResource>
            </mods:mods>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="M"
                     STATE="A"
                     VERSIONABLE="true"
                     ID="Fulltext.pdf">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="Fulltext.pdf.0"
                               LABEL="Full text"
                               MIMETYPE="application/pdf">
         <foxml:contentLocation REF="http://localhost/uploaded/theses/Perinatal-Depression/Fulltext.pdf"
                                TYPE="URL"/>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="RELS-EXT.1"
                               LABEL="Relationships"
                               MIMETYPE="text/xml"
                               SIZE="973">
         <foxml:xmlContent>
            <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#"
                     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
               <rdf:Description rdf:about="info:fedora/uk-ac-man-scw:ths0017">
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde18"/>
                  <esc-rel:wasBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:4"/>
                  <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                  <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:15"/>
               </rdf:Description>
            </rdf:RDF>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
</foxml:digitalObject>
