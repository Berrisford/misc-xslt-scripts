<?xml version="1.0" encoding="UTF-8"?>
<foxml:digitalObject xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     VERSION="1.1"
                     xsi:schemaLocation="info:fedora/fedora-system:def/foxml# http://www.fedora.info/definitions/1/0/foxml1-1.xsd"
                     PID="uk-ac-man-scw:ths0001">
   <foxml:objectProperties>
      <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
   </foxml:objectProperties>
   <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="MODS.0"
                               LABEL="Metadata Object Description Schema record"
                               MIMETYPE="text/xml"
                               SIZE="4986">
         <foxml:xmlContent>
            <mods:mods xmlns:ev="http://www.w3.org/2001/xml-events"
                       xmlns:exf="http://www.exforms.org/exf/1-0"
                       xmlns:f="http://orbeon.org/oxf/xml/formatting"
                       xmlns:fr="http://orbeon.org/oxf/xml/form-runner"
                       xmlns:mods="http://www.loc.gov/mods/v3"
                       xmlns:widget="http://orbeon.org/oxf/xml/widget"
                       xmlns:xforms="http://www.w3.org/2002/xforms"
                       xmlns:xhtml="http://www.w3.org/1999/xhtml"
                       xmlns:xi="http://www.w3.org/2001/XInclude"
                       xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:xxforms="http://orbeon.org/oxf/xml/xforms"
                       xmlns:xxi="http://orbeon.org/oxf/xml/xinclude"
                       version="3.3">
               <mods:abstract>This thesis investigates the  relationship between the spatial  concentration of 
South Asian ethnic groups and experiences of inequality in education, employment 
and health. Ethnic  and racial segregation  has become of  increasing concern in 
Britain  over the  past decade,  but there  has been  little research  that  has 
examined the relationship between  the spatial concentration of  minority ethnic 
groups and their socio-economic outcomes in various domains. This study sets out 
to address this gap in the  literature. In recent years, segregation has  become 
an increasingly ambiguous and value-laden  concept. The shift in its  meaning to 
denote the self-segregation of minority ethnic groups in Britain has made it  an 
increasingly problematic concept for investigating inequality. At the same  time 
the  rise  to  prominence of  research  on  segregation in  the  US  has greatly 
influenced academic research on the matter in Britain. In this thesis I adopt  a 
more  critical  approach  to  segregation  in  Britain  by  framing  South Asian 
geographies within a socialhistorical context by taking account of the nature of 
migration and settlement of South Asians to Britain, and the structural context, 
namely  discriminatory housing  and labour  market pOlicies,  within which  this 
occurred. In light of this, I suggest that a more appropriate measure of spatial 
segregation is  neighbourhood deprivation,  which more  accurately reflects  the 
material disadvantage of many areas of high minority ethnic concentration. Thus, 
the focus of my empirical analysis  looks at the extent to which  the divergence 
in socio-economic outcomes across geographies for South Asian and White  British 
ethnic groups is related to the South Asian concentration of neighbourhoods, and 
the extent to which this is associated with levels of neighbourhood deprivation. 
I also look at the extent to which differences between the South Asian and White 
British ethnic groups are associated with neighbourhood co-ethnic  concentration 
and deprivation. I  address the research  question using two  national datasets. 
Comprehensive neighbourhood data available from 2001 Census tabular data is used 
to  obtain  data measuring  the  ethnic composition  of  neighbourhoods and  the 
employment, education and health outcomes of ethnic groups at the  neighbourhood 
level. With this data I examine the relationship between levels of neighbourhood 
South Asian concentration, levels of neighbourhood deprivation, and outcomes  in 
education, employment and health  for Indian, Pakistani, Bangladeshi,  and White 
British ethnic groups. The second dataset I make use of is the 2005  Citizenship 
Survey which includes  additional requested data  on the ethnic  composition and 
deprivation  levels  of neighbourhoods  for  respondents in  the  survey. I  use 
multilevel logistic  regression methods  to determine  the extent  to which  the 
neighbourhood  context  matters. I  find  neighbourhood deprivation  to  be more 
important  in explaining  the divergence  in education,  employment, and  health 
across  geographies than  levels of  South Asian  concentration for  all  ethnic 
groups.  I  find  the negative  association  between  Pakistani and  Bangladeshi 
concentration and education and employment outcomes to be explained when  levels 
of neighbourhood deprivation are considered.  In terms of inequality, while,  on 
average,  the  Indian  group  report  better  or  equal  socio-economic outcomes 
compared  with  the  White  British  group,  the  disadvantage  experienced   by 
Pakistanis  and   Bangladeshis  is   not  explained   by  levels   of  co-ethnic 
concentration. Factors such as human  capital and household income are  shown to 
be  more important.  My evidence  suggests that  policy approaches  to  tackling 
inequality should focus on area deprivation, rather than the ethnic  composition 
of neighbourhoods.</mods:abstract>
               <mods:genre type="content-type" authority="local">thesis</mods:genre>
               <mods:genre type="version">Alternative format</mods:genre>
               <mods:note type="authors">Kapoor, Nisha</mods:note>
               <mods:note type="degreelevel">Doctor of Philosophy</mods:note>
               <mods:originInfo>
                  <mods:dateIssued encoding="iso8601">2010</mods:dateIssued>
                  <mods:place>
                     <mods:placeTerm>Manchester, UK</mods:placeTerm>
                  </mods:place>
                  <mods:publisher>University of Manchester</mods:publisher>
               </mods:originInfo>
               <mods:part>
                  <mods:extent unit="page">
                     <mods:total/>
                  </mods:extent>
               </mods:part>
               <mods:recordInfo>
                  <mods:recordCreationDate encoding="iso8601">2017-02-10T16:09:30.065Z</mods:recordCreationDate>
                  <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
               </mods:recordInfo>
               <mods:subject>
                  <mods:topic>Sociology</mods:topic>
                  <mods:topic>Education</mods:topic>
                  <mods:topic>Employment</mods:topic>
                  <mods:topic>Health</mods:topic>
                  <mods:topic>Inequality</mods:topic>
                  <mods:topic>Discrimination</mods:topic>
                  <mods:topic>Great Britain</mods:topic>
                  <mods:topic>Segregation</mods:topic>
                  <mods:topic>Great Britain</mods:topic>
                  <mods:topic>Immigrants</mods:topic>
                  <mods:topic>Employment</mods:topic>
                  <mods:topic>Great Britain</mods:topic>
                  <mods:topic>Immigrants</mods:topic>
                  <mods:topic>Social conditions</mods:topic>
                  <mods:topic>South Asians</mods:topic>
                  <mods:topic>Education</mods:topic>
                  <mods:topic>Great Britain</mods:topic>
                  <mods:topic>South Asians</mods:topic>
                  <mods:topic>Medical care</mods:topic>
                  <mods:topic>Great Britain</mods:topic>
                  <mods:topic>South Asians</mods:topic>
                  <mods:topic>Great Britain</mods:topic>
                  <mods:topic>Ethnic identity</mods:topic>
                  <mods:topic>South Asians</mods:topic>
                  <mods:topic>Great Britain</mods:topic>
                  <mods:topic>Social conditions</mods:topic>
               </mods:subject>
               <mods:titleInfo>
                  <mods:title>Deconstructing segregation: Exploring south Asian geographies and inequality in Britain</mods:title>
               </mods:titleInfo>
               <mods:tableOfContents>CONTENTS
List of figures .......................... II II' •••••••••• II ••• II II. II I" II II II •••••••••••••••• II 11.11 •• II II ••• II 11.1 5
List of tables ..................................................................................................... 6
List of maps ...................................................................................................... 8
Abstract ............................................................................................................. 9
Declaration ................................................ II •• II ••• II •••• II ••• I •••••••••••••••••••••• I •••••••• II 10
Copyright statement ........................................................ II II ••••••• 11.11 11.1. II ••• I. II 10
Acknowledgements ........................................................................................ 11
The Author ............................... , ........... I •••••••••••••••••••••••••••••••••••••••••••••••••••••••••• 11
List of abbreviations ...................................................................................... 12
1 Introduction ............................................................................................. 13
Organisation of the thesis ........................................................................................ 17
2 Theorising 'segregation' In the context of inequality .......................... 23
Introduction ............................................................................................................. 23
Unpacking 'segregation' .......................................................................................... 25
Economic re-structuring and urban poverty ............................................................. 27
The salience of residential segregation ................................................................... 30
Urban marginality and the role of the state .............................................................. 33
Structuring segregation in Britain ............................................................................. 36
Tackling inequality - different policy approaches .................................................... .40
Summary ................................................................................................................. 42
3 Segregation In Britain: exploring the complexities of South Asian
migration and settlement ............................................................................... 45
Introduction ............................................................................................................. 45
The meaning of segregation in Britain .................................................................... .4 7
Spatial distribution of South Asians in Britain ......................................................... .49
The role of structure in locating South Asian groups ................................................ 59
Community, solidarity and resistance ...................................................................... 70
Summary ................................................................................................................. 73
4 Data and Method ..................................................................................... 75
Introduction ............................................................................................................. 75
Research Questions ................................................................................................ 76
2
Methodological Approach ........................................................................................ 76
Data Sources .......................................................................................................... 77
The 2005 Citizenship Survey Sample ...................................................................... 82
Response Rates and Missing Data ......................................................................... 84
Concepts ................................................................................................................. 86
Methods .................................................................................................................. 95
Summary ............................................................................................................... 104
5 Education inequalities and the neighbourhood context ................... 105
Introduction ........................................................................................................... 105
Inequality, segregation and school attainment. ...................................................... 106
Relating the school and the neighbourhood .......................................................... 110
No qualifications rates and the neighbourhood context ......................................... 110
Educational inequality: the complexity of locality ................................................... 115
Capturing inequalities in education ........................................................................ 120
Modelling no qualifications .................................................................................... 122
Ethnic differences in education .............................................................................. 125
Ethnic inequalities in education and the neighbourhood context.. .......................... 128
Discussion ............................................ ·· ....... · ....................................................... 133
Summary ............................................................................................................... 136
6 Employment Inequalities and the neighbourhood context ............... 138
Introduction ................................. · .. · ...... ····· .. · .... ··· ................................................. 138
Interrogating 'segregation' in employment ............................................................. 139
Geographical location and spatial mismatch ......................................................... 140
Assimilation and the ecological hypothesis ............................................................ 142
Unemployment rates and the neighbourhood context ............................................ 145
The complexity of spatial inequalities in employment ............................................ 149
Capturing inequalities in employment .................................................................... 153
Modelling unemployment ...................................................................................... 155
Ethnic differences in employment.. ........................................................................ 158
The neighbourhood context and inequality in unemployment ................................ 160
Discussion .................... · .. ·· .. · .. ·· .. · .. · .. · .. ···· .. ·· .. ·· .. ··· ........ ·· .. ·· ... " .............................. 166
Summary ................. · .. · .. ·· .. ·· .. ·· .... · .. ···· .. ·· .. · ............................................................ 171
7 Health inequalities and the neighbourhood context .......................... 172
Introduction ...................... ·· .......... ·· .. · .. · .. · .... ··· .......... ·· .. ······ .. · .... · ........................... 172
Contextualising health ..................... · .. · .. ·· .... ·· ... ·· ................................................... 173
Limiting long term illness and the neighbourhood context ..................................... 181
3
Capturing inequalities in health ............................................................................. 185
Modelling limiting long term illness ........................................................................ 188
Ethnic differences in health ................................................................................... 191
Ethnic inequalities in health and the neighbourhood context ................................. 193
Discussion ............................................................................................................. 200
Summary ............................................................................................................... 204
8 Conclusions ........................................................................................... 206
Theorising minority ethnic spatial concentrations .................................................. 208
Is South Asian neighbourhood concentration associated with poor socio-economic
status? .................................................................................................................. 209
Addressing inequality ............................................................................................ 214
Limitations of research .......................................................................................... 216
Rethinking 'segregation' in the British context ....................................................... 218
Implications for policy ............................................................................................ 220
Areas for further research ..................................................................................... 222
Final remarks ........................................................................................................ 223
Appendix 1: Tables showing the local authority districts with the largest
concentrations of Indians, Pakistanis and Bangladeshis between 1971·
2001 . .............................................................................................................. 225
Appendix 2: Method for matching Census tabular data on education
measured for wards with neighbourhood deprivation measured for lower
super output areas ....................................................................................... 228
Appendix 3: Method for adjusting Census tabular data measuring limiting
long term illness to account for age ........................................................... 230
Appendix 4: Self·reported poor health rates in neighbourhoods measured
by levels of South Asian COncentration and by levels of deprivation ..... 231
Appendix 5: Reduced neighbourhood sample multilevel model results
predicting ethnic differences In no qualification levels ............................ 233
Appendix 6: Reduced neighbourhood sample multilevel model results
predicting ethnic differences in male unemployment ............................... 234
Appendix 7· Reduced neighbourhood sample multilevel model results
predicting ethnic differences In LL TI. ......................................................... 235
References .. II •••••••• II ••••••••••• I ••••••••••••••••••••••••• I •••• II •• II ••• II ••••••••••• , •••• I •••••••••••••••• 236
Word Count: 79,782.
4
List of figures
Figure 3. 1- Distribution of ethnic groups across Census LSOAs measured by levels of
co-ethnic concentration in 2001 .................................................................................. 54
Figure 3.2 Distribution of ethnic groups across LSOAs measured by their 2004
deprivation score ......................................................................................................... 59
Figure 4. 1 Distribution of ethnic groups across neighbourhoods (MSOAs) measured by
levels of co-ethnic concentration in the 2005 Citizenship Survey ................................ 85
Figure 4.2 Distribution of ethnic groups across neighbourhoods (MSOAs) measured by
deprivation levels in the 2005 Citizenship Survey. ....................................................... 86
Figure 5.1 No qualification rates by size of family social network (no. of relatives in
regular contact with) for ethnic groups ....................................................................... 119
Figure 5.2 Predicted probability of no qualifications by level of neighbourhood co-ethnic
concentration for the South Asian groups, and South Asian concentration for the White
group (Model 5 results) ............................................................................................. 132
Figure 5.3 Predicted probability of having no qualifications for ethnic groups by level of
neighbourhood deprivation (ModelS results) ............................................................ 133
Figure 6.1 Male unemployment rates (%) by qualification and ethnic group .............. 151
Figure 6.2 Predicted male unemployment rate by levels of neighbourhood co-ethnic
concentration for the South Asian groups and by levels of South Asian concentration
for the White group (Model 5 results) ........................................................................ 166
Figure 6.3 Predicted male unemployment rate by levels of neighbourhood deprivation
(Model 5 results) ....................................................................................................... 166
Figure 6.4 Predicted unemployment rate for Pakistani/Bangladeshi and Indian men
living in neighbourhoods measured by levels of co-ethnic concentration and
deprivation . ............................................................................................................... 169
Figure 7. 1 LL TI rates for ethnic groups by level of household income, age 16 and over .
................................................................................................................................. 187
Figure 7.2 Predicted LL TI rate by levels of neighbourhood co-ethnic concentration for
the South Asian groups and by levels of South Asian concentration for the White group
(Model 6 results) ....................................................................................................... 199
Figure 7.3 Predicted LLTI rate by level of neighbourhood deprivation (Model 6 results)
................................................................................................................................. 199
5
List of tables
Table 4.1 Correlations between IMD and unemployment, LL TI, and no qualifications. 97
Table 5.1 No qualification rates by levels of neighbourhood South Asian concentration
for ethnic groups aged 16-24 .................................................................................... 113
Table 5.2 No qualification rates by levels of neighbourhoods deprivation for ethnic
groups, aged 16-24 .......................................................................... · ... ····················· 114
Table 5.3 No qualification rates (%) by size of family social network for each ethnic
group ........................................................................................................................ 119
Table 5.4 Multi/evel variance components model predicting log odds of no qualifications
for ethnic groups, aged 16 and over ....................................................................... ··· 126
Table 5.5 Multilevel logistic regression model predicting the log odds of no
qualifications for each ethnic group using the 2005 Citizenship Survey ..................... 127
Table 5.6 Multilevel logistic regression model predicting ethnic differences (log odds) in
no qualifications for adults aged 16 and over . ........................................................... 129
Table 6. 1 Unemployment rates by levels of neighbourhood South Asian concentration
for ethnic groups, aged 16-74 ................................................................................... 147
Table6.2 Unemployment rates by levels of neighbourhood deprivation for ethnic groups
aged 16-74 ................................................................................................................ 149
Table 6.3 Male unemployment rates (%) by qualification and ethnic group ............... 152
Table 6.4 Multilevel variance components model predicting log odds of male
unemployment for ethnic groups, aged 20-60 ........................................................... 158
Table 6.5 Multilevel logistic regression model predicting log odds of male
unemployment for each ethnic group, aged 20-60 ..................................................... 160
Table 6.6 Multi/evellogistic regression models predicting ethnic differences (log odds)
in male unemployment, aged 20-60 .......................................................................... 163
Table 7.1 LL TI rates by levels of neighbourhood South Asian concentration for ethnic
groups, all ages ......................................................................................................... 182
Table 7.2 LL TI rates by levels of neighbourhood deprivation for ethnic groups, all ages .
................................................................................................................................. 184
Table 7.3 % of ethnic groups with a reported LL TI by household income .................. 187
Table 7.4 Multilevel variance components model predicting log odds of LL TI for ethnic
groups, aged 16 and over ......................................................................................... 191
Table 7.5 Multilevel logistic regression model predicting the log odds of LL TI for each
ethnic group using the 2005 Citizenship Survey ........................................................ 192
Table 7.6 Multilevel logistic regression models predicting ethnic differences (log odds)
in LL TI, for men and women aged 16 and over . ........................................................ 196
Table A 1.1 Top twenty districts with largest concentrations of Bangladeshis 1971-2001
................................................................................................................................. 225
6
Table A1.2 Top twenty districts with largest concentrations of Pakistanis, 1971-2001
................................................................................................................................. 226
Table A 1.3 Top twenty districts with largest concentrations of Indians, 1971-2001 ... 227
Table A4.1 Self-reported health rates by levels of neighbourhood South Asian
concentration for ethnic groups, all ages . .................................................................. 231
Table A4.2 Self-reported health rates by levels of neighbourhood deprivation for ethnic
groups, all ages ......................................................................................................... 232
Table A5.1 Multilevel logistic regression model predicting ethnic differences (log odds)
in no qualifications, men and women aged 16 and over ............................................ 233
Table A6.1 Multi/evellogistic regression model predicting ethnic differences (log odds)
in male unemployment, aged 20-60 . ......................................................................... 234
Table A 7.1 Multilevel logistic regression models predicting ethnic differences (log odds)
in LL TI, for men and women aged 16 and over ......................................................... 235
7
List of maps
Map 3.1 Concentration of South Asian population in Birmingham EDs, 1971 ............. 51
Map 3.2 Concentration of South Asian population in Birmingham EDs, 1981 ............ 52
Map 3.3 Concentration of South Asian population in Birmingham LSOAs, 2001 ......... 53
Map 3.4 1971 Townsend deprivation scores for Birmingham wards ......................... 55
Map 3.51981 Townsend deprivation scores for Birmingham wards ........................ 56
Map 3.61991 Townsend deprivation scores for Birmingham wards ......................... 57
Map 3.72001 Townsend deprivation scores for Birmingham wards ........................ 58</mods:tableOfContents>
               <mods:typeOfResource>text</mods:typeOfResource>
            </mods:mods>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="M"
                     STATE="A"
                     VERSIONABLE="true"
                     ID="Fulltext.pdf">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="Fulltext.pdf.0"
                               LABEL="Full text"
                               MIMETYPE="application/pdf">
         <foxml:contentLocation REF="http://localhost/uploaded/theses/Deconstructing-Segregation/Fulltext.pdf"
                                TYPE="URL"/>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="RELS-EXT.1"
                               LABEL="Relationships"
                               MIMETYPE="text/xml"
                               SIZE="973">
         <foxml:xmlContent>
            <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#"
                     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
               <rdf:Description rdf:about="info:fedora/uk-ac-man-scw:ths0001">
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde18"/>
                  <esc-rel:wasBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:2"/>
                  <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                  <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:15"/>
               </rdf:Description>
            </rdf:RDF>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
</foxml:digitalObject>
