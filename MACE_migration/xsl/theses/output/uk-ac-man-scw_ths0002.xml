<?xml version="1.0" encoding="UTF-8"?>
<foxml:digitalObject xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     VERSION="1.1"
                     xsi:schemaLocation="info:fedora/fedora-system:def/foxml# http://www.fedora.info/definitions/1/0/foxml1-1.xsd"
                     PID="uk-ac-man-scw:ths0002">
   <foxml:objectProperties>
      <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
   </foxml:objectProperties>
   <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="MODS.0"
                               LABEL="Metadata Object Description Schema record"
                               MIMETYPE="text/xml"
                               SIZE="4986">
         <foxml:xmlContent>
            <mods:mods xmlns:ev="http://www.w3.org/2001/xml-events"
                       xmlns:exf="http://www.exforms.org/exf/1-0"
                       xmlns:f="http://orbeon.org/oxf/xml/formatting"
                       xmlns:fr="http://orbeon.org/oxf/xml/form-runner"
                       xmlns:mods="http://www.loc.gov/mods/v3"
                       xmlns:widget="http://orbeon.org/oxf/xml/widget"
                       xmlns:xforms="http://www.w3.org/2002/xforms"
                       xmlns:xhtml="http://www.w3.org/1999/xhtml"
                       xmlns:xi="http://www.w3.org/2001/XInclude"
                       xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:xxforms="http://orbeon.org/oxf/xml/xforms"
                       xmlns:xxi="http://orbeon.org/oxf/xml/xinclude"
                       version="3.3">
               <mods:abstract>Through an intersectional, embodied (psycho-spatial and synaesthetic), and
processual theoretical matrix, my thesis unpacks the complex mechanisms
underpinning the visual identification of subjects and artworks as "Desi" - the Hindi
word meaning "from my country" - within the geographical and conceptual space that
roughly includes the US, UK, and the South Asian subcontinent. To add further
traction to my investigation, I yoke "queer" to Desi. The latter as two overlapping,
and at other times competing, sets of visual identifications, serve to bring to the fore,
rather than subsume, the complexities of the broader spectrum of intersecting visual
identifications connected to both.
I examine the production and consumption of post- I 980s British and
American Desi art to explore queer Desi visualities as intersectional with not only
sexuality, race, and nationality, but also faith, gender, and class. I also describe
contemporary artworks that immerse the spectator, making him or her aware of the
inseparability of the viewing subject, artwork, and implied authorial subject. In so
doing, these artworks highlight the supplementarity of visual identification - the
illimitable "etc. " that trails any investigation of identity - and thereby re-work Desi
along multiple axes - addressing issues of faith, spirituality, and religion, norms of
gender and sexuality, stability of geographic al/nati onal and temporal borders, and the
politics of migration of bodies and objects.
To flesh out the tensions inherent in queer and Desi as identifications, I
explore the civic and commercial production of two areas of the city of Manchester,
UK: the Gay Village, an area of queer bars and clubs, and Curry Mile, named for its
many South Asian restaurants and shops. I unpack how these spaces are not only
produced as felt separation through a synaesthetic exploration of space, but also reworked
by the subjects who traverse them by honing in on the psycho-spatial aspects
of visual identification. Finally, through consideration of a series of public art
projects I produced in Manchester, which developed in tandem with this theoretical
investigation, I theorize how public art can potentially surface the "queer" in Curry
Mile and the "Desi" in the Gay Village, and thereby engender a more ethical and
embodied public sphere.
6</mods:abstract>
               <mods:genre type="content-type" authority="local">thesis</mods:genre>
               <mods:genre type="version">Alternative format</mods:genre>
               <mods:note type="authors">Patel, Alpesh Kantilal</mods:note>
               <mods:note type="degreelevel">Doctor of Philosophy</mods:note>
               <mods:originInfo>
                  <mods:dateIssued encoding="iso8601">2009</mods:dateIssued>
                  <mods:place>
                     <mods:placeTerm>Manchester, UK</mods:placeTerm>
                  </mods:place>
                  <mods:publisher>University of Manchester</mods:publisher>
               </mods:originInfo>
               <mods:part>
                  <mods:extent unit="page">
                     <mods:total/>
                  </mods:extent>
               </mods:part>
               <mods:recordInfo>
                  <mods:recordCreationDate encoding="iso8601">2017-02-10T16:09:30.065Z</mods:recordCreationDate>
                  <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
               </mods:recordInfo>
               <mods:subject>
                  <mods:topic>Art</mods:topic>
                  <mods:topic>Culture</mods:topic>
                  <mods:topic>South Asian American gays</mods:topic>
                  <mods:topic>Homosexuality and art</mods:topic>
                  <mods:topic>Race and Art</mods:topic>
                  <mods:topic>South Asian art</mods:topic>
                  <mods:topic>Minority groups</mods:topic>
                  <mods:topic>Great Britain</mods:topic>
                  <mods:topic>Minority groups</mods:topic>
                  <mods:topic>United States</mods:topic>
                  <mods:topic>Gay men</mods:topic>
                  <mods:topic>Great Britain</mods:topic>
                  <mods:topic>Identity.</mods:topic>
               </mods:subject>
               <mods:titleInfo>
                  <mods:title>Queer Desi visual culture across the "brown Atlantic"</mods:title>
               </mods:titleInfo>
               <mods:tableOfContents>LIST OF CONTENTS
LIST OF FIGURES 4
ABSTRACT
..................................................................................................................6
DECLARATION and COPYRIGHT STATEMENT
..............................................
7
ACKNOWLEDGEMENTS
........................................................................................8
CHAPTER ONE/Introduction
...................................................................................9
Towards Defining Desi: Paul Gilroy's "Black Atlantic, " the "Brown Atlantic. " and
its Critiques
..............................................................................................................
12
Desi Visuality, or Visual Identification, as Processual and Embodied
...................
14
Desi Visuality as Intersectional
...............................................................................
18
Theorizing Queer Desi as Intersectional/Supplemental Visual Identifications
.......
21
Theorizing Queer as a Destabilizing Tool and as an Identification
.........................
26
Desi Artworks and Aesthetics in/and Institutional Space
........................................
Desi Aesthetic s/Vi suality in Urban Space
...............................................................
32
Brief Chapter Summaries
.........................................................................................
36
CHAPTER TWO/Politics of Post-1980s British and American Desi Art:
Production, Circulation, and Reception
..................................................................
38
Rise of Postcolonial Theory in Art Criticism
..........................................................
39
Identity and Post-Identity Politics in the Art World
................................................
43
(De)constructing Desi Art Histories: Race, Class, and Nation
................................
46
Production and Circulation of Arnerican Desi Art: Gender and Nationality
...........
54
Contemporary Inter/national Art Biennials and the Rise of Interest in "Indian Art"
.................................................................................................................................
56
Critical Reception of British Desi Art: Nationality and Gender
..............................
64
Anish Kapoor as Bntish/Asian/Artist
......................................................................
71
Supplemental/Intersectional Desi Visual Identification
..........................................
77
CHAPTER THREE/Desi Aesthetics: Pastiche, Parody, and Embodiment
.........
79
(Mis)Recognition of Desi: Stephen Dean's Use of Colour
.....................................
81
Embodiment in/and Classical, Modernist, and Postmodernist Aesthetic Theory
....
84
Towards a Desi Aesthetic
........................................................................................
90
Sublime Desi: Rina Banerjee's Pink Taj Mahal
......................................................
97
Uncanny Desi: Shahzia Sikander's Burka-clad Eve and Muslim Kali
..................
103
Carnivalesque Desi: Raqib Shaw's Orgiastic Flora and Fauna
.............................
109
Space as Place
........................................................................................................
114
CHAPTER FOUR/Production of Queer/Desi/Space as Perceived, Conceived, and
Directly Lived: Manchester, UK ............................................................................
117
Space as Perceived, Conceived, and Directly Lived
.............................................
118
Theorizing Queer, Desi Fldnene
............................................................................
121
Sinooth/Haptic and Striated/Optical Space
............................................................
122
Locating Feeling 'Thinking Desi: rsycho-spatiality
..............................................
124
(De)construction of the Gay Village: Manchester, UK
.........................................
128
Marketing Manchester: Gay as Ethnic, Ethnic as Gay
...........................................
III
Sites! Sights of Knowledge, Gay \'Illage: Directly Lived and Commercially
ConceiNed..
............................................................................................................
138
Theorizing Queer Desi Visuality as PerformatiN e .................................................
141
I
(De)constructing Curry Mile
................................................................................. 144
Curry Mile as Striated Space: Performance of South Asian-ness
.........................
148
Politics of Perfon-nance of Racial Masculinity and Issues of Class, CuFrý Nlile
...
152
Queer Desi Space
................................................................................................... 15 7
Queering Queer Desi Space: The Elusive Third Space
.........................................
162
CHAPTER FIVE/Conclusion: Role of Aesthetics in Re-shaping a
"Cosmopolitan" Manchester .................................................................................. 164
Towards a Theorization of an Ethical and Embodied Public Sphere
....................
165
Sphere: dreamz
...................................................................................................... 168
Cultural Marketing, Canal Street and Curry Mile
.................................................
171
Marketing Victorian-era Manchester: Class, Culture, and Embodiment
...............
176
Multi-Sensonal Organization of Urban Space and Cultural Marketing
................
180
Sphere: dreamz Re-Sited: Queering Curry Mile and the Oxford Road Cultural
Corridor
..................................................................................................................
183
Queer Urban Walking, D&amp;ives, and Psychogeographies
......................................
188
Currying Canal Street and Tasting/Hearing Desi
..................................................
192
Towards a Conclusion: Notes on Reception of Mixing It Up
................................
193
FIGURES .................................................................................................................. 198
APPENDIX ...............................................................................................................2 52
BIBLIOGRAPHY ....................................................................................................2 59
Word Count: 82,738 words
I
LIST OF FIGURES
Figure 1.1: Cover, TheNeiv Yorker magazine, 5 November 2001
............................ 198
Figure 2.1: Sutapa Biswas, Houseivives ivith Steak-Knives, 1985
.......................... ...
199
Figure 2.2: Unknown artist, Kali (Indian Poster Art), unknown date
.......................
199
Figure 2.3: Artemisia Gentileschi, Judith Beheading Holofernes, 1614-20
........... ...
200
Figure 2.4: Caravaggio, Judith Beheading Holq/ernes, 1598-1599
....................... ...
200
Figure 2.5: Robert Rauschenberg, White, 1951
...................................................... ...
200
Figure 2.6: Anish Kapoor, 1000 Names, 1985
........................................................ ..
201
Figure 2.7: Installation view of Anish Kapoor's Void Field, 1989
........................ ...
201
Figure 3.1: Film stills from Stephen Dean's Pulse, 2001
....................................... ...
202
Figure 3.2: Stephen Dean, Untitled (Mots Croises), 1995
....................................... ..
203
Figure 3.3: Stephen Dean, Account, 2001
............................................................... ..
203
Figure 3.4: Stephen Dean, Ladder, 2006
................................................................ ..
203
Figure 3.5: Rina Banerjee, Take Me, Take Me, Take Me
...
To the Palace of Lovc
...
204
Figure 3.6: Taj Mahal, 1631-1654, Agra, India
......................................................... .
204
Figure 3.7: Detail of Take Me, Take Me, Take Me... To the Palace of Love, 2003
. ...
205
Figure 3.8: Detail of Take Me, Take Me, Take Me... To the Palace of Love, 2003
. ...
206
Figure 3.9: Rina Banerjee, Optical Unconscious
.................................................... ..
207
Figure 3.10: Rina Baneij ee, Contagious Spaces, Preserving Pinkeye
................... ...
208
Figure 3.11: Details of Contagious Spaces, Preserving Pinkeye
........................... ...
208
Figure 3.12: Shahzia Sikander, Venus's Wonderland, 1995-97
............................. ...
209
Figure 3.13: Botticelli, The Birth of Venus, ca. 1485
............................................. ...
210
Figure 3.14: Detail of Botticelli, The Birth of Venus, ca. 1485
.............................. ...
210
Figure 3.15: Shahzia Sikander, Fleshy Weapons, 1997
.......................................... ...
211
Figure 3.16: Raqib Shaw, Garden of Earthýl, Delights X, 2005
............................. ...
212
Figure 3.17: Hieronymus Bosch, The Garden of Earthly Delights, 1503-1504
..... ...
212
Figure 3.18: Details of Shaw's Garden of Earthly Delights X, 2005
..................... ...
213
Figure 3.19: Raqib Shaw, Garden of Earthly Delights V1,2004
............................ ...
214
Figure 3.20: Rina Baneij ee, Lure of Places, 2006
.................................................. ...
215
Figure 4.1: Map of Manchester City South Partnership
......................................... ...
216
Figure 4.2: Spread from Marketing Manchester's Manchester-The Destination
. ...
217
Figure 4.3: Posters, vanilla club, Manchester, UK, February 2008
........................ ...
218
Figure 4.4: Poster, vanilla and Essential clubs, Manchester, UK
........................... ...
219
Figure 4.5: Website Homepage Banner, vanilla club, Manchester
........................ ...
219
Figure 4.6: Posters, Essential club, Manchester, UK, various months. 2007
......... ...
220
Figure 4.7: Poster, Essential club, Manchester, UK, March 2007
.......................... ...
221
4
Figure 4.8: Calvin Klein CK Advertisement
.............................................................
221
Figures 4.9a-f. Photos of walk down Curry Mile, Manchester, UK
..........................
222
Figure 4.10: Flephant sculpture, Curry Mile, Manchester, UK
.................................
223
Figures 4.11 a-c: Paintings, Shahenshah Restaurant, Curry Mile, Manchester
..........
224
Figures 4.12a-c: Paintings, Shahenshah Restaurant, Curry Mile, Manchestcl ........... 225
Figures 4.13a-c: Signage of Curry Mile restaurant, Manchester. UK
.......................
226
Figure 4.14: Front of EastzFast restaurant take away menu. Manchester, UK
.........
227
Figures 4.15 and 4.16: Posters, HornieSexual Party, Manchester, UK
.....................
228
Figure 5.1: Sphere, Sphere: (Irean7z, Sackville Park, Manchester, Summer 2006
....
229
Figure 5.2: Details of Sphere: drean7z
.......................................................................
230
Figure 5.3: Details of Sphere: drean7z
.......................................................................
231
Figure 5.4: Details ot'Sphel-e-. dream:
.......................................................................
232
Figure 5.5: Details of Sphere: dl-eam:
.......................................................................
233
Figure 5.6: Details of Sphere: di-ealn.. .......................................................................
234
Figure 5.7: Details of Sphere: dremw
.......................................................................
235
Figure 5.8: Central Rangoll design of Sphere: dream:
.............................................
236
Figure 5.9: Details of Sphere: di-eaniz
.......................................................................
237
Figure 5.10: Schematic ofOxford Road Cultural Corridor
.......................................
238
Figure 5.11: Marketing Manchester's "Manchester: A Guide for LGBT Visitors".. 239
Figure 5.12: Mixing It Up postcard (front)
................................................................
240
Figure 5.13: Mixing It Up postcard (back)
................................................................
241
Figure 5.14: Sphere, Sphere: dreaniz installation, part of Mixing It Up
....................
242
Figure 5.15: Sphere, Sphere: drealn: installation, part of Mixing It Up
....................
243
Figure 5.16: Detail of Sphei-e: di-ealnz installation, part of Mixing It Up
..................
244
Figure 5.17: Sphere, organza screen of Sj? here: dreantz, part of Mixing It Uj)
.........
245
Figure 5.18: Sphere, stills from video, A W(ikening, part ot'Mixing It Up
................
246
Figure 5.19: Sphere, untilled wall mural, part ol'Mixing It Up
.................................
247
Figure 5.20: Doorstep Collective, Queer Urban Walk, Curry Mile
..........................
248
Figure 5.2 1: Year 12 student psychogeographies ......................................................
250
Figure 5.22: Paul Stanley, Generosiýl, Cake Prqject, part of Ali-ving It Up
...............
251</mods:tableOfContents>
               <mods:typeOfResource>text</mods:typeOfResource>
            </mods:mods>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="M"
                     STATE="A"
                     VERSIONABLE="true"
                     ID="Fulltext.pdf">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="Fulltext.pdf.0"
                               LABEL="Full text"
                               MIMETYPE="application/pdf">
         <foxml:contentLocation REF="http://localhost/uploaded/theses/Queer-Desi-visual/Fulltext.pdf"
                                TYPE="URL"/>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="RELS-EXT.1"
                               LABEL="Relationships"
                               MIMETYPE="text/xml"
                               SIZE="973">
         <foxml:xmlContent>
            <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#"
                     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
               <rdf:Description rdf:about="info:fedora/uk-ac-man-scw:ths0002">
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde18"/>
                  <esc-rel:wasBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:2"/>
                  <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                  <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:15"/>
               </rdf:Description>
            </rdf:RDF>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
</foxml:digitalObject>
