<?xml version="1.0" encoding="UTF-8"?>
<foxml:digitalObject xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     VERSION="1.1"
                     xsi:schemaLocation="info:fedora/fedora-system:def/foxml# http://www.fedora.info/definitions/1/0/foxml1-1.xsd"
                     PID="uk-ac-man-scw:ths0029">
   <foxml:objectProperties>
      <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
   </foxml:objectProperties>
   <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="MODS.0"
                               LABEL="Metadata Object Description Schema record"
                               MIMETYPE="text/xml"
                               SIZE="4986">
         <foxml:xmlContent>
            <mods:mods xmlns:ev="http://www.w3.org/2001/xml-events"
                       xmlns:exf="http://www.exforms.org/exf/1-0"
                       xmlns:f="http://orbeon.org/oxf/xml/formatting"
                       xmlns:fr="http://orbeon.org/oxf/xml/form-runner"
                       xmlns:mods="http://www.loc.gov/mods/v3"
                       xmlns:widget="http://orbeon.org/oxf/xml/widget"
                       xmlns:xforms="http://www.w3.org/2002/xforms"
                       xmlns:xhtml="http://www.w3.org/1999/xhtml"
                       xmlns:xi="http://www.w3.org/2001/XInclude"
                       xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:xxforms="http://orbeon.org/oxf/xml/xforms"
                       xmlns:xxi="http://orbeon.org/oxf/xml/xinclude"
                       version="3.3">
               <mods:abstract>This dissertation reports on communicative strategies in the Italian of Igbo-Nigerian
immigrants living in the city of Padova (North-Eastern Italy). The informants of this
study are mainly employed as factory workers and are acquiring the Italian language
without any formal education. All of them speak the Igbo language, English and
Nigerian Pidgin English, some of them also Yoruba and French.
The literature is rich in studies that analyze SLA linguistic outcomes, most of which
assume that learners seek a full mastering of the TL. However, this is not the case for
immigrant workers, whose primary urge is to communicate effectively at their
workplace or in everyday life. Taking into account this fallacy, this dissertation
proposes an innovative approach to the analysis of communicative strategies in nonguided
SLA.
The salient features of this approach are as follows. First, it treats immigrant speakers
qua effective communication achievers, rather than language learners. Second, it
focuses on the communicative interaction. Third, it regards individual linguistic
strategies as language innovations potentially initiating language change (Croft, 2000).
Fourth, it sees non-guided SLA as a contact phenomenon and adopts a unified contact
approach which puts all contact phenomena under the same umbrella (Winford, 2003;
Myers-Scotton, 2002). Finally, it views creolization as akin to non-guided SLA
(Mufwene, 1996; Chaudenson, 1994a).
Two major sets of communicative strategies have been analyzed using the proposed
approach: code-switching and reanalysis. The analysis reveals that English codeswitching
in Italian discourse mainly operates as a lexical gap filler. Some of these
switches are instances of smooth code-switching, which is better interpreted as
replicated unmarked code-switching from the Nigerian linguistic repertoire (Myers-
Scotton, 1993).
This study also shows that reanalysis is a major strategy for the informants to create
new meanings. Examples such as the multifunctional preposition per `for' or the use of
the existential form c'e `there's' to express the meaning of possession also illustrate
how previously acquired languages can influence the direction of change.</mods:abstract>
               <mods:genre type="content-type" authority="local">thesis</mods:genre>
               <mods:genre type="version">Alternative format</mods:genre>
               <mods:note type="authors">Goglia, Francesco</mods:note>
               <mods:note type="degreelevel">Doctor of Philosophy</mods:note>
               <mods:originInfo>
                  <mods:dateIssued encoding="iso8601">2005</mods:dateIssued>
                  <mods:place>
                     <mods:placeTerm>Manchester, UK</mods:placeTerm>
                  </mods:place>
                  <mods:publisher>University of Manchester</mods:publisher>
               </mods:originInfo>
               <mods:part>
                  <mods:extent unit="page">
                     <mods:total/>
                  </mods:extent>
               </mods:part>
               <mods:recordInfo>
                  <mods:recordCreationDate encoding="iso8601">2017-02-10T16:09:30.065Z</mods:recordCreationDate>
                  <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
               </mods:recordInfo>
               <mods:subject>
                  <mods:topic>Language</mods:topic>
                  <mods:topic>Linguistics</mods:topic>
                  <mods:topic>Foreign workers, African</mods:topic>
                  <mods:topic>Igbo (African people)</mods:topic>
                  <mods:topic>Igbo diaspora</mods:topic>
                  <mods:topic>Code switching (Linguistics)</mods:topic>
                  <mods:topic>Sociolinguistics</mods:topic>
                  <mods:topic>Italy</mods:topic>
                  <mods:topic>Nigerians</mods:topic>
                  <mods:topic>Italy</mods:topic>
                  <mods:topic>Social conditions</mods:topic>
                  <mods:topic>Padova (Italy)</mods:topic>
               </mods:subject>
               <mods:titleInfo>
                  <mods:title>Communicative strategies in the Italian of Igbo-Nigerian immigrants in Padova (Italy): a contact linguistic approach</mods:title>
               </mods:titleInfo>
               <mods:tableOfContents>Table of contents
List of Figures
List of Tables
List of Abbreviations
Abstract
Declaration
Copyright Statement
Acknowledgements
The Author
0. Introduction
1 The sociolinguistic context
1.0 Introduction
1.1 The sociolinguistic situation in Nigeria
1.1.1 Nigerian English
1.1.2 Nigerian Pidgin English
1.1.3 Igbo
1.1.4 Features of multilingualism in Igboland
1.2 Features of Igbo-Nigerian immigration in the Veneto region
1.2.1 The Igbo community in Padova
1.3 The sociolinguistic situation in the Veneto region
1.3.1 Linguistic repertoire of the community
1.4 Conclusions
2 Fieldwork and informants
2.0 Introduction
2.1 Data collection
2.2 The corpus of the present study
2.2.1 The interviews
2.2.2 The interview schedule
2.2.3 Transcription conventions
2.2.4 The informants
2.2.5 Biographies of the informants
2.3 Conclusions
3A contact-linguistic approach to the study of non-guided SLA
3.0 Introduction
3.1 A speaker-baseadp proach
3.1.1 SLA as a creative process
3.1.2 The role of the speaker in language change
3.1.3 Innovations and language change
3.1.4 Variation and language change
3.2 SLA as a contact phenomenon
3.2.1 Immigration as a contact setting
3.2.2 Code-switching and non-guided SLA
3.2.3 Creolization and non-guided SLA
3.2.3.1 Theories of creolization involving SLA
5
5
6
7
8
9
10
11
12
16
17
18
19
20
23
25
27
33
34
35
38
39
40
40
44
44
46
47
48
50
58
59
60
62
64
66
68
69
70
73
75
78
82
2
3.2.3.1.1 Gradual basilectalization 82
3.2.3.1.2 The constructive approach 83
3.2.3.1.3 Adults as creolizers 85
3.2.4 Code-switching and creolization 86
3.3 Conclusions 87
4 Concepts and terminology for the analysis of communicative strategies 89
4.0 Introduction 90
4.1 Issues of code-switching 91
4.1.1 The grammatical approach 93
4.1.2 The conversational approach 97
4.1.3 The sociolinguistic approach 98
4.1.4 The code-switching vs. borrowing dispute 100
4.2 The notion of reanalysis 103
4.2.1 Reanalysis and non-guided SLA 104
4.2.2 Reanalysisa s a listener-baseds emanticc hange 106
4.2.3 Language contact and reanalysis 108
4.2.4 Reanalysis vs. grammaticalization 110
4.3 The notion of grammaticalization 112
4.3.1 The notion of grammaticalization and its application to L2
data 114
4.3.2 Language contact and grammaticalization 116
4.4 Iconicity in non-guided SLA 117
4.5 Conclusions 118
5. Code-switching as a communicative strategy in L2 119
5.0 Introduction 120
5.1 Insertions 121
5.1.1 Lexical gap filling 123
5.1.1.1 Appeal for help 127
5.1.1.2 Self-repairs 129
5.1.1.3 Smooth code-switching as a replicated unmarked choice? 132
5.1.1.4 Accommodation of insertions in Italian discourse 138
5.1.1.4.1 Switched nouns 138
5.1.1.4.1.1 Definite articles 140
5.1.1.4.1.2 Morphosyntactic integration 141
5.1.1.4.2 Switched verbs 144
5.1.1.4.3 Switched adjectives 146
5.1.2 Anaphoric insertions 148
5.1.3 Switched demonstrative adjectives 150
5.1.4 Switched connectives 152
5.2 Alternations 156
5.2.1 Reformulations 158
5.2.2 Side comments 161
5.3 Conclusions 163
6 Reanalysis of Italian forms 165
6.0 Introduction 166
6.1 Anaphoric sentence introducer ma 167
6.1.1 Igbo sentence introducer ma 168
3
6.1.2 Anaphoric value of Italian ma 170
6.2 Multifunctional preposition per 172
6.2.1 One multifunctional preposition in L2s and creoles 176
6.2.2 Prepositions in Igbo and NPE 178
6.3 Conjunction anche 181
6.3.1 Grammaticalization of coordination 183
6.3.2 Coordination in Igbo and NPE 183
6.4 Quantitative adjective troppo 185
6.5 Conclusions 187
7 Reanalysis of the existential c'e to express possession 188
7.0 Introduction 189
7.1 Forms in the corpus and the communicative strategy 190
7.2 Interrelation between existence and possession 198
7.3 Possessivea nd existential forms in SLA and creoles 201
7.4 Grammaticalizationo f possessivec onstructions 203
7.5 Have-constructionsin Igbo and NPE 204
7.6 Conclusions 206
8. Conclusions
References
Appendix 1
Appendix 2
209
215
234
239
4</mods:tableOfContents>
               <mods:typeOfResource>text</mods:typeOfResource>
            </mods:mods>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="M"
                     STATE="A"
                     VERSIONABLE="true"
                     ID="Fulltext.pdf">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="Fulltext.pdf.0"
                               LABEL="Full text"
                               MIMETYPE="application/pdf">
         <foxml:contentLocation REF="http://localhost/uploaded/theses/Communicative-strategies/Fulltext.pdf"
                                TYPE="URL"/>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="RELS-EXT.1"
                               LABEL="Relationships"
                               MIMETYPE="text/xml"
                               SIZE="973">
         <foxml:xmlContent>
            <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#"
                     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
               <rdf:Description rdf:about="info:fedora/uk-ac-man-scw:ths0029">
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde18"/>
                  <esc-rel:wasBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:2"/>
                  <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                  <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:15"/>
               </rdf:Description>
            </rdf:RDF>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
</foxml:digitalObject>
