<?xml version="1.0" encoding="UTF-8"?>
<foxml:digitalObject xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     VERSION="1.1"
                     xsi:schemaLocation="info:fedora/fedora-system:def/foxml# http://www.fedora.info/definitions/1/0/foxml1-1.xsd"
                     PID="uk-ac-man-scw:ths0020">
   <foxml:objectProperties>
      <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
   </foxml:objectProperties>
   <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="MODS.0"
                               LABEL="Metadata Object Description Schema record"
                               MIMETYPE="text/xml"
                               SIZE="4986">
         <foxml:xmlContent>
            <mods:mods xmlns:ev="http://www.w3.org/2001/xml-events"
                       xmlns:exf="http://www.exforms.org/exf/1-0"
                       xmlns:f="http://orbeon.org/oxf/xml/formatting"
                       xmlns:fr="http://orbeon.org/oxf/xml/form-runner"
                       xmlns:mods="http://www.loc.gov/mods/v3"
                       xmlns:widget="http://orbeon.org/oxf/xml/widget"
                       xmlns:xforms="http://www.w3.org/2002/xforms"
                       xmlns:xhtml="http://www.w3.org/1999/xhtml"
                       xmlns:xi="http://www.w3.org/2001/XInclude"
                       xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:xxforms="http://orbeon.org/oxf/xml/xforms"
                       xmlns:xxi="http://orbeon.org/oxf/xml/xinclude"
                       version="3.3">
               <mods:abstract>This is a study of migration from the parish of St. Elizabeth
in Jamaica, 1954-1962. The thesis attempts to integrate the
literature relating to debates in the sociology of migration and the
sociology of race and ethnic relations. We begin with a discussion
of theoretical issues raised in the literature with regard to
migration and labour. These raised questions about structure and
agency. We emphasise the passage between cultures and the
significance of biographies, work histories and kinship.
Chapters 3 and 4 examine the context of migration in the
1940s and 1950s from the background of rural Jamaican society and
focuses upon the parish of St. Elizabeth and a series of interviews
with some septuagenarians.
Chapters 5 to 11 focus upon the life experiences of the
peopl~ who migrated. We pay partic~lar attention to the reasons
for migration, experiences of British society and racism, how they
obtained jobs and pursued their concern for a better life. In this
we raise questions about the way in which aspects of St. Elizabethan
culture fitted them to cope with British society. We pay
particular attention to kinship, savings and credit schemes and
their attitudes towards education as a source of social mobility.
4
Chapters 12 and 13 look at the children of the migrants and
particularly their experience of education and family relations.
In this we identify important sources of variation in the experience
of the second generation. We explore these in Chapters 14 and 15
through questions relating to social class and social identity.
Throughout the thesis has relied upon detailed informal
interviews, life history and participant observation as research
techniques. These were all used to facilitate our understanding of
the qualitative side of the migrants' experience.
5</mods:abstract>
               <mods:genre type="content-type" authority="local">thesis</mods:genre>
               <mods:genre type="version">Alternative format</mods:genre>
               <mods:note type="authors">Sterling, Louis</mods:note>
               <mods:note type="degreelevel">Doctor of Philosophy</mods:note>
               <mods:originInfo>
                  <mods:dateIssued encoding="iso8601">1992</mods:dateIssued>
                  <mods:place>
                     <mods:placeTerm>Manchester, UK</mods:placeTerm>
                  </mods:place>
                  <mods:publisher>University of Manchester</mods:publisher>
               </mods:originInfo>
               <mods:part>
                  <mods:extent unit="page">
                     <mods:total/>
                  </mods:extent>
               </mods:part>
               <mods:recordInfo>
                  <mods:recordCreationDate encoding="iso8601">2017-02-10T16:09:30.065Z</mods:recordCreationDate>
                  <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
               </mods:recordInfo>
               <mods:subject>
                  <mods:topic>Social History</mods:topic>
                  <mods:topic>Migration</mods:topic>
                  <mods:topic>Immigrants</mods:topic>
                  <mods:topic>Families</mods:topic>
                  <mods:topic>Jamaica</mods:topic>
                  <mods:topic>Jamaicans</mods:topic>
                  <mods:topic>Great Britain</mods:topic>
                  <mods:topic>Social conditions</mods:topic>
                  <mods:topic>Jamaicans</mods:topic>
                  <mods:topic>Kinship</mods:topic>
                  <mods:topic>Racism</mods:topic>
                  <mods:topic>Great Britain</mods:topic>
                  <mods:topic>History</mods:topic>
                  <mods:topic>20th century</mods:topic>
                  <mods:topic>West Indians</mods:topic>
                  <mods:topic>Great Britain</mods:topic>
                  <mods:topic>History</mods:topic>
                  <mods:topic>20th century</mods:topic>
                  <mods:topic>Multiculturalism</mods:topic>
                  <mods:topic>Great Britain</mods:topic>
                  <mods:topic>Manchester (England)</mods:topic>
                  <mods:topic>Ethnic relations</mods:topic>
                  <mods:topic>St. Elizabeth (Jamaica)</mods:topic>
               </mods:subject>
               <mods:titleInfo>
                  <mods:title>From St. Elizabeth, Jamaica to Manchester, England: the dynamics of migration</mods:title>
               </mods:titleInfo>
               <mods:tableOfContents>TABLE OF CONTENTS
Page
TABLE OF CONTENTS 2
ABSTRACT 4
ACKNOWLEDGEMENTS 6
INTRODUCTION 7
CHAPTER 1 THEORIES OF MIGRATION 18
CHAPTER 2 LABOUR AND MIGRATION 47
CHAPTER 3 ECONOMY AND SOCIETY IN RURAL JAMAICA 63
CHAPTER 4 FAMILY AND HOUSEHOLD 100
CHAPTER 5 REASONS FOR MIGRATION 123
CHAPTER 6 THE EXPERIENCE OF RACISM 152
CHAPTER 7 GETTING A JOB 172
CHAPTER 8 HOUSING AND RESIDENCE 199
CHAPTER 9 PARTNERS: THE SOCIAL ORGANISATION 236
OF ROTATING SAVINGS AND CREDIT
SOCIETIES AMONG EXILIC JAMAICANS
CHAPTER 10 HOUSEHOLD - FAMILY STRUCTURE 273
CHAPTER 11 MIGRANTS' EXPECTATIONS 290
CHAPTER 12 EDUCATIONAL EXPERIENCES AND SOCIAL 302
MOBILITY
CHAPTER 13 HOUSEHOLD STRUCTURE AND DOMESTIC 372
ARRANGEMENTS
CHAPTER 14 LABOUR AND CLASS 393
CHAPTER 15 RACISM, ETHNICITY AND IDENTITY 417
2
CONCLUSION 437
APPENDIX ONE METHODS 442
APPENDIX TWO THE SAMPLE 450
BIBLIOGRAPHY 459</mods:tableOfContents>
               <mods:typeOfResource>text</mods:typeOfResource>
            </mods:mods>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="M"
                     STATE="A"
                     VERSIONABLE="true"
                     ID="Fulltext.pdf">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="Fulltext.pdf.0"
                               LABEL="Full text"
                               MIMETYPE="application/pdf">
         <foxml:contentLocation REF="http://localhost/uploaded/theses/From-St-Elizabeth/Fulltext.pdf"
                                TYPE="URL"/>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="RELS-EXT.1"
                               LABEL="Relationships"
                               MIMETYPE="text/xml"
                               SIZE="973">
         <foxml:xmlContent>
            <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#"
                     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
               <rdf:Description rdf:about="info:fedora/uk-ac-man-scw:ths0020">
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde18"/>
                  <esc-rel:wasBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:2"/>
                  <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                  <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:15"/>
               </rdf:Description>
            </rdf:RDF>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
</foxml:digitalObject>
