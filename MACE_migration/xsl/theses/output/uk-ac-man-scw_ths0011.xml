<?xml version="1.0" encoding="UTF-8"?>
<foxml:digitalObject xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     VERSION="1.1"
                     xsi:schemaLocation="info:fedora/fedora-system:def/foxml# http://www.fedora.info/definitions/1/0/foxml1-1.xsd"
                     PID="uk-ac-man-scw:ths0011">
   <foxml:objectProperties>
      <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
   </foxml:objectProperties>
   <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="MODS.0"
                               LABEL="Metadata Object Description Schema record"
                               MIMETYPE="text/xml"
                               SIZE="4986">
         <foxml:xmlContent>
            <mods:mods xmlns:ev="http://www.w3.org/2001/xml-events"
                       xmlns:exf="http://www.exforms.org/exf/1-0"
                       xmlns:f="http://orbeon.org/oxf/xml/formatting"
                       xmlns:fr="http://orbeon.org/oxf/xml/form-runner"
                       xmlns:mods="http://www.loc.gov/mods/v3"
                       xmlns:widget="http://orbeon.org/oxf/xml/widget"
                       xmlns:xforms="http://www.w3.org/2002/xforms"
                       xmlns:xhtml="http://www.w3.org/1999/xhtml"
                       xmlns:xi="http://www.w3.org/2001/XInclude"
                       xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:xxforms="http://orbeon.org/oxf/xml/xforms"
                       xmlns:xxi="http://orbeon.org/oxf/xml/xinclude"
                       version="3.3">
               <mods:abstract>This thesis explores the issue of "justice" as experienced by indigenous
people in Chiapas. It starts by reconstructing the memories of the "ancient
injustices" as recalled by some of the inhabitants of the Tzeltal community
of San Jer6nimo Tulij6. The work also traces the permeation of state
institutions as an alien political culture into indigenous institutions, through
the consolidation of their ejido and their entrance into development statesponsored
projects in the region. I argue that problems of political
factionalism, as experience today in San Jer6nimo, are the product of
conflicts that developed as a consequence of the entrance of the state into
the community. These political divisions are exacerbated today by the
practices of counterinsurgency that the state has deployed in Chiapas in
its war against the Zapatista Army of National Liberation (EZLN). This is
the context of my ethnographic and filmed documentary study of the
Community Human Rights Defenders' Network in his efforts to bring "fair"
justice to the cases that are brought before judicial institutions in a highly
politicised environment in which the real basis of the charges brought
against individuals are seldom transparent, and indigenous defendants
and their legal advocates face deeply rooted structural racism. The
Community Defenders' conception of justice points to the urgent need to
reform the judicial system in Mexico, which has failed to convince its
citizens that it provides access for everyone on an equal basis.
The thesis demonstrates that the meaning of "autonomy" for these
members of Zapatista autonomous communities does not lie in the
demand for respect for indigenous judicial practices as represented by
local "usos y costumbres" but in practices that empower indigenous
people to defend themselves more effectively within the framework of
official law and legal institutions, thereby strengthening their capacity for
self-determination within the larger national society.</mods:abstract>
               <mods:genre type="content-type" authority="local">thesis</mods:genre>
               <mods:genre type="version">Alternative format</mods:genre>
               <mods:note type="authors">Navarro-Smith, Alejandra</mods:note>
               <mods:note type="degreelevel">Doctor of Philosophy</mods:note>
               <mods:originInfo>
                  <mods:dateIssued encoding="iso8601">2005</mods:dateIssued>
                  <mods:place>
                     <mods:placeTerm>Manchester, UK</mods:placeTerm>
                  </mods:place>
                  <mods:publisher>University of Manchester</mods:publisher>
               </mods:originInfo>
               <mods:part>
                  <mods:extent unit="page">
                     <mods:total/>
                  </mods:extent>
               </mods:part>
               <mods:recordInfo>
                  <mods:recordCreationDate encoding="iso8601">2017-02-10T16:09:30.065Z</mods:recordCreationDate>
                  <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
               </mods:recordInfo>
               <mods:subject>
                  <mods:topic>Sociology</mods:topic>
                  <mods:topic>Social History</mods:topic>
                  <mods:topic>Law</mods:topic>
                  <mods:topic>Indigenous peoples</mods:topic>
                  <mods:topic>Civil rights</mods:topic>
                  <mods:topic>Mexico</mods:topic>
                  <mods:topic>Indigenous peoples</mods:topic>
                  <mods:topic>History</mods:topic>
                  <mods:topic>19th century</mods:topic>
                  <mods:topic>Self-determination, National</mods:topic>
                  <mods:topic>Mexico</mods:topic>
                  <mods:topic>Chiapas</mods:topic>
                  <mods:topic>Chiapas (Mexico)</mods:topic>
                  <mods:topic>Ethnic relations</mods:topic>
                  <mods:topic>History</mods:topic>
                  <mods:topic>Politics and government.</mods:topic>
               </mods:subject>
               <mods:titleInfo>
                  <mods:title>Structural racism and the indigenous struggle for land, justice and autonomy in Chiapas, Mexico</mods:title>
               </mods:titleInfo>
               <mods:tableOfContents>Contents
LIST OF TABLES
...................................................................................................................7.. ...
LIST OF DIAGRAMS AND PROTOTYPES
.......................................................................7.. .
LIST OF PHOTOGRAPHS
....................................................................................................7.. ..
LIST OF VIDEO CLIPS
........................................................................................................8.. ...
LIST OF MAPS .......................................................................................................................8.. ...
GLOSSARY ................................................................................................................9.. ................
ABSTRACT. ..............................................................................................................1..1.. ..............
DECLARATION
...................................................................................................................1..2.. .
COPYRIGHT
.........................................................................................................................1..3.. .
FOREWORD
.........................................................................................................................1..4.. .
ACKNOWLEDGMENTS
.....................................................................................................1..5..
INTRODUCTION
.................................................................................................................2..2.. .
THE CONTEXT OF THE RESEARCH .............................................................................. 27
DEFINING THE "VALUABLE" CATEGORIES FOR RESEARCH
.................................... 29
Taking the defence in your own hands
..........................................................
30
THE ENTRANCE TO THE COMMUNITY: THE IDENTITY OF THE RESEARCHER
...........
32
Problems of access in a tense environment ofpoliticalfactionalism .................
33
ON METHODOLOGY
...................................................................................................
34
MANY PRACTICES OF ANTHROPOLOGY
...................................................................
35
TOWARDS A CONCEPTUALISATION OF AN ANTHROPOLOGICAL PROJECT IN A
CONTEXT OF LOW INTENSITY WAR, VIOLENCE AND/OR POLITICAL OPPRESSION
....................................................................................................................................
38
a) Committed relationships ...........................................................................3..8..
b) The researcher as a witness of war. a proposition for a politically
engaged practice of anthropology ...............................................................4..4.
THE RESEARCHER AS A PRIVILEGED OBSERVER: A THEORETICAL PROBLEM
.........
45
c) The aim of the project: producing counter-narratives to hegemonic
explanations of violence .........................................................................................
46
Distance and the development of collaborative relationships: a
methodological problem ...................................................................................
48
Text, image and knowledge
.............................................................................
52
THE DEVELOPMENT OF A SUITABLE METHODOLOGY: THE DEVELOPMENT OF AN
OBSERVATIONAL COLLABORATIVE FILMMAKING RELATIONSHIP .........................
52
ON CONCEPTUALISING COLLABORATIVE RELATIONSHIPS
.......................................
57
3
FIRST SECTION
DEBT CONTRACTING AND PEONAGE. .....................................................6..1.. ....
CHAPTER 1. ANCIENT INJUSTICE REMEMBERED
.....................................6. 2
To WORKI N THER ANCHOP ANTELJA':T ESTIMONYO FD ON NEMESIOC RUZ
........
65
"El trago "- the drink
.......................................................................................6..8..
Free labourforce
.............................................................................................6..9.. .
The roadt o Yajal6n. .............................................................................6..9.. ........
The clearing of the landing-strip
.............................................................7..0.. .....
Don Nemesioo penedh is eyest o mistreatment ..............................................7..1.. ..
Overloadedw orking timetablesa ndp hysicalp unishment ...........................7..1.
Languaged ivide, lack of schoolinga nda lcoholism:s ocialh orizonsf or
indigenousp eoples ...............................................................................7..4.. ........
Peasanpt eopleh ado wners ....................................................................7..6.. .......
CHAPTER 2. COMPETING INTERESTS: DIVERGENT PROJECTS OVER
THE TROPICAL RAIN FOREST LAND .............................................................7..8.
Tensions and disputes in Bachaj6n, more reasons to migrate into the tropical
rain forest
......................................................................................................... .
83
Memories of Don Pedro Guzm6n, the founder of San Jer6nimo Tulijd
.........
85
Traditional lifestyle in Jetj6 and the lost of autonomy ..............................8. 6
Things have not changedm uch for indigenous people .............................8. 7
Juez, musician and peasant. .....................................................................8..8..
Hunting and gathering forfestivities: the ritual exploitation of the Lacandon
tropical rainforest .............................................................................................. ..
89
Wood exploitation in the region: the interests of the capitalist enterprise ...... ...
90
Work in the timber camps as remembered by the first generation of men
raised in San Jer6nimo Tulijd
.......................................................................... 9. 3
Testimonies
................................................................................................ ..
93
"Hooking" indigenous people to work in the timber camps ..................... 94
Testimony of Don Mariano Mendez, deacon of San Jer6nimo Tulijd
.... ...
97
The track to Tsendalcs
.........................................................................9..7..
Testimony of Don Narciso M6ndez, autonomouse jidatario ......................9 7
The timberland camp of Pico de Oro
....................................................9..7
Labour and trade relationships with caxlanes. ..................................................9..8
The fiscal .................................................................................................9..8.. .
The rancher Don Atanasio ......................................................................... .. 99
The teacher of the Ministry of Education
...............................................1..0 0
The governmental anti-malaria campaign. ..............................................1..0 1
Don Pedro GuzmAn was never made to feel ashamed. ................................1. 02
DONATANASIO L6PEz
........................................................................................1..0..3
The work of the chicleros ................................................................................1..0..3
The bad habits of Don Tano
...........................................................................1..0..4
You cannot come here because this is all Don Tano's land
.............................1. 07
The tragedy of Don Atanasio
..........................................................................1..0..9
4
SECOND SECTION
ETHNOGRAPHY OF THE POLITICAL FORCES IN THE REGION OF
SAN JER6NIMO TULIJA, CHIAPAS
CHAPTER 3. THE POLITICS OF THE CONSTITUTION OF THE ENDO:
THE STATE'S APPROPRIATION OF POLITICAL CONTROL AT THE
LOCALLEVEL ....................................................................................................1..1..4.
THE EMERGENCEO F THE STATE IN CHIAPAS FOR INDIGENOUSP EOPLE ................
114
THE POLITICAL MODEL OF THE MEXICAN NATION IN THE 1940S
.........................
116
Contradiction and ambiguity at thefoundations: a revolutionary state is
created to retain the power of the new political elites .......................................
116
THE REGIONAL POLITICAL CONTEXT ......................................................................
116
National lands are available! The hope generating machine starts operating
.........................................................................................
116
TO GET EJIDO LAND: THE CASE OF SAN JER6NIMO TULIJA
...................................
119
The endowment of the ejido and the three extensions .......................................
121
The abuses continue in the new village ..............................................................
123
The engineer Eloy Borras Aguilar
.....................................................................
125
Ambiguity of state agents in ejidos: subjects of trust and penalisers ................
127
Theforestales took care that the montafia alta was not destroyed
...............
127
We used to hide in the bush due'to-the leaflets
.........................................
127
Some orders of apprehensionw ere released. ...........................................1. 28
Don Arternio helped us to end with the abuse on the part of the forestry
agents ..........................................................................................................
130
You shouldn't be afraid, I am your leader! A federal politician as protector
and benefactor .................................................................................................
131
A problem arises between people of the ejido and the forestales
............ 131
Arrest warrant against the ejidal commisioner of San Jer6nimo Tuliji
...
132
To seek for help at the regional level
......................................................1..3 2
Travel to meet the national leaders. .........................................................1..3 2
The solution is granted ...............................................................................
133
Working togetherfor common benefits
...................................*.. .. ......................
134
Problems with the limits with Bachaj6n ........................................................
135
The construction of a bridge over the river, working for the community ....
138
The decline of a leader's authority .................................................................
139
A testimony on the problem of the wood exploitation: Don Pancho on his
experience as Comisariado Ejidal in 1984 ....................................................
140
What can we do! The company will cut all trees ......................................
140
The partners of COFOLASA .....................................................................
144
They finally took the trucks with them .....................................................
145
Suspicion, conspiracy and cooptation: political division in the community .....
147
FINAL CONSIDERATION...S.. .....................................................................................
151
Coorporativismo: official ideology of the post revolutionary state ..............
152
Whitening the population and anti-imperialist sentiments: the state discourse
of progress .......................................................................................................
153
Wellbeing for everyone through capitalist development and aspirations to
European culture: the paradox in the state policies for progress ..................
155
CHAPTER 4 DIVISIVE EFFECTS OF THE FAILURE OF STATE
SPONSORED PRODUCTIVE PROJECTS ...........................................................
157
EVOLUTION OF CONFLICTS AND DIVISION ..............................................................
158
Brief introduction to the events that people in San Jer6nimo identify with the
political division amongst its population ...........................................................
159
POLITICAL DIVISION, FIRST ACT: THE IIEL AGUILA" COOPERATIVE .....................
159
5
Discord started when cash became available, testimony of Vicente Mdndez
Gutidrrez on the "El Aguila" cooperative. ...................................................1..5 9
Work and problems in the cooperative. .......................................................1..6 3
Moral influences in the formation of a leader: cooperativism and the duty of
service to the community ............................................................................1..6. 5
POLITICALD IVISION,S ECONDA CT: THEB ANKC REDITSF ORR AISINGC ATTLE .... 168
The bank wanted to take our land away 11te stimony of Don Rafael Gutijrrez
G6mez,a utonomous agente ejidal ...................................................................1..7. 3
ANALYSISO FT HED IVISIVEE FFECTOS FS TATEP ROMOTED EVELOPMENT
PROJECT..S.. .........................................................................................................1..7..7.
CONCLUSIONS
.....................................................................................................1..8..1.
CHAPTER S. POLITICAL DIVISIONISM: THE STRUGGLE FOR
LEGITIMACY OF POLITICAL CONTROL IN SAN JER6NIMO TULIJA
...............................................................................................................................1..8..3.. .
Power relationships: ethnography of power forces among the
political factions in the region ........................................................1..8 3
THE TRIUMPHO FP A13LOS ALAZARF EEDST HEH OPEO FR ESTORINGPE ACEIN
CHIAPAS
.............................................................................................................1..8..5..
THE POLITICALD IVISIONI N SANJ ER6NImoT ULIJA
.............................................1. 87
SPATIALP OLITICALD IVISIONO FT HEC OMMUNITY ..............................................1..8 9
The disputefor the control of the Casa Ejidal
.................................................1..9 2
Chronicle of the disputefor the control of the Agency. ....................................1. 93
THE INSTALLATIONO FA MILITARYB ASEI N THES CHOOLP LAYGROUND ............ 194
GENERATION"P EDROG UZMANH ERNANDEZ1, 999-2002:T HE GRADUATION
CEREMONYO FT HET ELESECONDARSYC HOOL0 97
.............................................1..9 7
The hand shaking ceremony: political tension in the telesecundaria students'
graduation .....................................................................................................2..0..0..
The Commander [coronel] is an important person: the routinisation of the
Federal Army in every day life
.......................................................................2..0..1
THE HUMANR IGHTD EFENDERIS CALLEDT O RECORDT HEI NCIDENTW HEREA
MILITARYO FFICERT HREATENEHDI GHS CHOOLS TUDENT.S .. ..............................2. 02
DIVISIVEE FFECTSIN THEC OMMUNITYR ESULTINGFR OMT HEF EDERALA RMY
PRESENC..E.. .......................................................................................................2..0..5..
THIRD SECTION
II
THE INDIGENOUS STRUGGLE TO PERMEATE STATE INSTITUTIONS
CHAPTER 6. INTERETHNIC RELATIONS, SOCIAL CHANGE AND THE
STRUGGLE FOR CONSTITUTIONAL JUSTICE IN CHIAPAS
....................2 08
THE FIRSTJ OURNEYT O SANJ ER6NIMOT ULIJAA NDT HES ECONDC LASSC OACH
STATIONR: EFLECTIONOSN SPACIALISERDA CISM.. .............................................2..1 2
FIGHTINGIN STITUTIONALISERDA CISMIN THEM EXICANJ USTICES YSTEM
........
217
Chronicle of the difficulty in accessing institutional procedures in the Mexican
Systemo fJustice: the case of the 5 men under house arrest ......* ** ....................
218
HUMAN RIGHTSA S A LEGALR ESOURCTEO CLAIMT HER IGHTT O DUEP ROCESS22. 5
HISTORICALP ATTERNSO FI NTERACTIONBE TWEENT HES TATEA ND INDIGENOUS
PEOPLE.S... ..........................................................................................................2..2..5..
THE CONCEPOT FC ONSTITUTIONMALU LTICULTURALISRMEV IEWEDA T THEL IGHT
OFI NTERETHNICIN TERACTIONISN MEXICO. ......................................................2..3. 0
THE CONTEXTH: UMANR IGHTSD EFENCEIN THEC ONTEXTO FT HEH UMAN ý kIGHTSD EFENDERNSE TWORKT: HES EARCHF ORA NDO BSTACLETSO OBTAINING
"FAIR" JUSTICE ...................................................................................................2..3..5..
6
CONTEXT OF LA RED
................................................................................................
240
ON THE PRACTICE OF AUTONOMY AND SELF-DETERMINATION IN THE TEACHING
AND EXERCISE OF HUMAN RIGHTS DEFENCE
..........................................................
240
Frayba vs. Red de Defensores Comunitarios. Some reflections on general
interethnic relations ............................................................................................
240
CHAPTER 7. INDIGENOUS DEMANDS AND STATE RESPONSES: FOUR
LEGAL CASES IN THE AUTONOMOUS MUNICIPALITY OF RICARDO
FLORES MAG6N
................................................................................................2..4..3.
THE UNDERSTANDING OF HUMAN RIGHTS DEFENCE IN THE CONTEXT OF THE
NETWORK OF COMMUNITY HUMAN RIGHTS DEFENDERS
.....................................
243
Low intensity war ..............................................................................................
245
THE NETWORK OF COMMUNITY HUMAN RIGHTS DEFENDERS IN RELATION TO
THESTATE
................................................................................................................
245
DEFINING THE CASES: ABUSE OF STATE AUTHORITIES OR CRIMES
.......................
247
CASE 1. POLITICALLY MOTIVATED HOUSE ARREST. LEVELS OF GOVERNMENT
INVOLVED IN THE CASE: MUNICIPAL, STATE AND FEDERAL
..................................
249
Thefacts
..............................................................................................................
249
CONTEXT OF COUNTERINSURGENCY STRATEGIES IN CHIAPAS
.............................
255
CONCLUSIONS TO CASE I........................................................................................ 257
CASE 2. CORRUPT ARREST ORDERS
........................................................................
257
Context
.................................................................................................................
257
Thefacts (21110108)
............................................................................................
258
Parties involved in the conflict ...........................................................................
259
Process of involvement and relationships established between the different
levels of government . ..........................................................................................
260
Activities undertaken by the community defýnder
.............................................
260
Results
..................................................................................................................
261
CASE 3. KEEPING A MURDERER IN JAIL
..................................................................
261
Thefacts
..............................................................................................................
261
Parties involved
...................................................................................................
262
Process of involvement and activities undertaken by the community defender
.............................................................................................................................
263
Type of relations established between whom .....................................................
263
Conclusions on the case ......................................................................................
264
CASE 4. ATTACK ON AUTONOMOUS MUNICIPALITIES ............................................
264
General background of attacks on autonomous indigenous governments in
Chiapas
................................................................................................................
264
The breaking news ..............................................................................................
265
Thefacts
..............................................................................................................
265
Different versions describing the problem .........................................................
267
Process of involvement and activities undertaken by the community defender
..... .......................................................................................................................
268
Results
..................................................................................................................
269
CONCLUSIONS: STATE-INDIGENOUS ORGANIZATIONS INTERACTIONS REVIEWED IN
THE LIGHT OF CONFLICTS, NEGOTIATIONS AND DEMANDS ARISING FROM THE
FOUR CASES PRESENTED
..........................................................................................
269
CONCLUSION FINAL CONSIDERATIONS FOR THE DISCUSSION ON
STRUCTURAL RACISM AND THE INDIGENOUS STRUGGLE FOR
LAND, JUSTICE AND AUTONOMY .................................................................2..7. 3
AN INDIGENOUS NOTION OF AUTONOMY ................................................................
273
THE PERMEATION OF THE STATE INTO INDIGENOUS INSTITUTIONS: INDIGENOUS
INCORPORATION INTO THE POLITICAL SYSTEM THROUGH THE FORMATION OF
EJIDOS.. .....................................................................................................................
274
7
THE INDIGENOUS STRUGGLE FOR JUSTICE AND THE STATE'S RESISTANCE TO
CHANGE ....................................................................................................................
274
To CHALLENGE TRADITIONAL PATERNALISTIC PRACTICES TOWARDS INDIGENOUS
PEOPLE: SMALL ACTS TO ELIMINATE STRUCTURAL RACISM .................................
275
MAKING THE RESEARCH AND ITS OUTCOMES ACCESSIBLE TO THE PEOPLE OF THE
STUDY .......................................................................................................................
276
BIBLIOGRAPHY ......................................................................................................
278
Other sources of reference ..................................................................................
290
Research Reports . ..........................................................................................
290
Websites ..........................................................................................................
290
Filmography
....................................................................................................
291
List of Tables
TABLE I CHRONOLOGY OF THE ENDOWMENT OF THE EJIDO AND THE THREE
EXTENSIONS IN SAN JER6NIMO TULIJA
...................................................
121
TABLE 2. HYPOTHETIC ESTIMATE OF REAL PRICES AND PROFITS PER
CATTLE HEAD, MADE BY VICENTE AS AN EXAMPLE OF HIS
ANALYSIS OF CREDITS IN 1987
..............................................................
170
TABLE 3. HYPOTHETIC CAPITAL PROJECTIONM ADE BY TECHNICIANS
IN 1987
.....................................................................................................
170
TABLE 4. HYPOTHETIC LOSS WITH LOWER PRICES THAN ESTIMATED
AT FINAL SALE IN 1987 ............................................................................
170
List of Diagrams and Prototypes
DIAGRAM I OF HOW THE COMMUNITY DEFENDERS IMAGINE THEIR RELATION TO
THEIR COMMUNITIES, AUTHORITIES AND EXTERNAL ADVICERS
......
242
PROTOTIPE I INTERACTIVE DESIGN TO NAVIGATE BETWEEN FILM SEQUENCES,
CONCEPTS, MAIN TEXT OF THE THESIS AND OTHER REFERENCES
DIRECTLY RELEVANT TO THE SEQUENCES OF THE DOCUMENTARY.. 277
List of Photographs
PHOTO I DON NEMESIO WITH DORA CARMELA, HIS WIFE, OUTSIDE OF THEIR
KITCHEN .................................................................................................
62
PHOTO 2 FIRST CARNIVAL CELEBRATION IN SAN JER6NIMO TULIJA
SINCE THE FOUNDATION OF THE COMMUNITY,
2001
.......................................................................................................
78
PHOTO 3 MAHOGAMY TREE IN THE LACANDON RAIN FOREST, 1909.
ILLUSTRATION PUBLISHED IN DE VOS
(1988A: 161)
...........................................................................................
79
PHOTO4 WORKERSO FT HET IMBERC AMPA oUA AZUL, 1946.
PHOTO: GERTRUDE DUBY, PUBLISHED IN DE VOS (I 988A: 16 1)
.......
94
PHOTO 5 MAHOGAMY TREE, LACANDON RAIN FOREST, 1930. PHOTO PUBLISHED
IN DE VOS (1988A: 161)
..........................................................................
95
8
PHOTO 6 HACHEROS LABRANDO UNA TROZA, 1946. PHOTO: GERTRUDE DuBy,
PUBLISHED IN DE VOS (1988A: 161)
......................................................
96
PHOTO 7 TRUNKS IN A TUMBO, 1946.
PHOTO: GERTRUDE DuBy, PUBLISHED IN DE VOS (I 988A: 161)
.........
96
PHOTO 8 JOHN STEPHENS "RIDING" ON AN INDIAN, 1840.
ILLUSTRATION PUBLISHED IN DE VOS (1988B: 57)
..............................
101
PHOTO 9 DON PANCHO AND HIS WIFE. SAN JER6NIMO TULTJA,
NOVEMBE2R0 02. .......................................................................1..5..0.. ...
PHOTO 10 SOLDIERS WALK ABOUT IN THE STREET WEARING THEIR
UNIFORMSA ND GUNS.. .......................................................................1..9 7
PHOTO II SOLDIER VIDEORECORDING IN A PUBLIC EVENT IN THE LOCAL HIGH
SCHOO..L.. ..................................................................................1..9..7.. .....
PHOTO 12 MILITARY JEEPS IN THE BASE IN SAN JER6NIMO TULIJA
................
197
PHOTO 13 "WE ARE THE BEST. No ONE IS ABOVE US. MEXICAN ARMY. 13"
REGIMENT". ANNOUNCEMENT FACING THE STREET WHERE THE
MILITARY BASE IS LOCATED ................................................
197
List of Video Clips
VIDEO CLIP I JTATIK MARIANO WEEDING HIS MILPA BEFORE CELEBRATING LA
FIESTA DELA dRUZ
...........................................................
49
VIDEO CLIP 2 POLICEMAN ENTERS THE OFFICE OF THE PUBLIC PERSECUTOR AND
IGNORES THE CAMERA AND THE FILM-MAKER
.............................
53
VIDEO CLIP 3 THE COMMUNITY DEFENDER SPEAKS A13OUT THE POLITICAL
DIVISION IN SAN JER6NIMO TULIA
................................. 191
VIDEO CLIP 4 GRADUATION IN THE TELESECUNDARIA
...............................
200
VIDEO CLIP 5 TESTIMONY OF MILITARY HARASSMENT TO HIGH SCHOOL
STUDENTS ...............................................................
202
VIDEO CLIP 6 CONFERENCE PRESS ON THE 30TH DAY OF HOUSE
ARREST... ......................................................................
253
VIDEO CLIP 7 COMMUNITY DEFENDER IN INTERVIEW WITH MEMBERS OF THE
INTERNATIONAL COMMISSION FOR THE OBSERVATION OF HUMAN
RIGHTS
...............................................................................
254
List of Maps
MAP I LACANDON RAIN FOREST REGION. MAIN RIVERS AND
LAKES
........................................................................................................
80
MAP 2 MIGRATION FLows INTO THE LACANDON RAIN FOREST REGION
FROM 1954 TOI 981
...................................................................................
84
MAP 3 THE PIGS ROUTE ..........................................................................................
87
MAP 4 SAN JER6NIMO TULUA REGION
...............................................................
92</mods:tableOfContents>
               <mods:typeOfResource>text</mods:typeOfResource>
            </mods:mods>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="M"
                     STATE="A"
                     VERSIONABLE="true"
                     ID="Fulltext.pdf">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="Fulltext.pdf.0"
                               LABEL="Full text"
                               MIMETYPE="application/pdf">
         <foxml:contentLocation REF="http://localhost/uploaded/theses/Structural-racism/Fulltext.pdf"
                                TYPE="URL"/>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
      <foxml:datastreamVersion CREATED="2017-02-10T16:09:30.065Z"
                               ID="RELS-EXT.1"
                               LABEL="Relationships"
                               MIMETYPE="text/xml"
                               SIZE="973">
         <foxml:xmlContent>
            <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#"
                     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
               <rdf:Description rdf:about="info:fedora/uk-ac-man-scw:ths0011">
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde18"/>
                  <esc-rel:wasBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:2"/>
                  <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                  <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:15"/>
               </rdf:Description>
            </rdf:RDF>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
</foxml:digitalObject>
