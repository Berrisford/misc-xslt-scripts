<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" name="xml"/>
    
    <!--<xsl:variable name="root_url" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/nonscw/select/'"></xsl:variable>-->
    <xsl:variable name="root_url" select="'http://localhost:8085/solr/nonscw/select/'"></xsl:variable>
    
    <xsl:template match="/">
        <!--<xsl:for-each select="//publication">-->
        <xsl:for-each select="//publication[attachment]">
            <xsl:variable name="pid_count">
                <xsl:number />
            </xsl:variable>
            <xsl:variable name="pid_name">
                <!-- BA: Change here... -->
                <xsl:choose>
                    <xsl:when test="number($pid_count) lt 10">
                        <xsl:value-of select="concat('ths', '000', $pid_count)" />                        
                    </xsl:when>
                    <xsl:when test="number($pid_count) gt 9 and number($pid_count) lt 100">
                        <xsl:value-of select="concat('ths', '00', $pid_count)" />                                                
                    </xsl:when>
                    <xsl:when test="number($pid_count) gt 99 and number($pid_count) lt 1000">
                        <xsl:value-of select="concat('ths', '0', $pid_count)" />                                                
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat('ths', $pid_count)" />                                                
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="file">
                <!-- BA: Change here... -->
                <xsl:value-of select="concat('output/', 'uk-ac-man-scw_', $pid_name, '.xml')" />
            </xsl:variable>
            <xsl:result-document href="{$file}" method="xml" indent="yes">
                <xsl:call-template name="publication_foxml">
                    <xsl:with-param name="pid_name" select="$pid_name"/>
                </xsl:call-template>
            </xsl:result-document>            
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template name="publication_foxml">
        <xsl:param name="pid_name"/>        
        <xsl:variable name="sequence"><xsl:number/></xsl:variable>
        <foxml:digitalObject VERSION="1.1" xmlns:foxml="info:fedora/fedora-system:def/foxml#"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="info:fedora/fedora-system:def/foxml# http://www.fedora.info/definitions/1/0/foxml1-1.xsd">
            <xsl:attribute name="PID">
                <xsl:call-template name="createPid">
                    <xsl:with-param name="sequence" select="$pid_name"/>
                </xsl:call-template>
            </xsl:attribute>
            <foxml:objectProperties>
                <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
            </foxml:objectProperties>
            
            <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
                <xsl:element name="foxml:datastreamVersion">
                    <xsl:attribute name="CREATED">
                        <xsl:call-template name="getCurrentDateTime"/>
                    </xsl:attribute>
                    <xsl:attribute name="ID" select="'MODS.0'"/>
                    <xsl:attribute name="LABEL" select="'Metadata Object Description Schema record'"/>
                    <xsl:attribute name="MIMETYPE" select="'text/xml'"/>
                    <xsl:attribute name="SIZE" select="'4986'"/>
                    
                    <foxml:xmlContent>
                        <mods:mods version="3.3" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:exf="http://www.exforms.org/exf/1-0"
                            xmlns:f="http://orbeon.org/oxf/xml/formatting" xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                            xmlns:fr="http://orbeon.org/oxf/xml/form-runner" xmlns:mods="http://www.loc.gov/mods/v3"
                            xmlns:widget="http://orbeon.org/oxf/xml/widget" xmlns:xforms="http://www.w3.org/2002/xforms"
                            xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xi="http://www.w3.org/2001/XInclude"
                            xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xxforms="http://orbeon.org/oxf/xml/xforms" xmlns:xxi="http://orbeon.org/oxf/xml/xinclude">
                            
                            <mods:abstract><xsl:value-of select="description"/></mods:abstract>
                            <mods:genre type="content-type" authority="local">thesis</mods:genre>
                            <mods:genre type="version">Alternative format</mods:genre>
                            <mods:note type="authors"><xsl:value-of select="author"/></mods:note>
                            <mods:note type="degreelevel"><xsl:value-of select="degree-level"/></mods:note>
                            <mods:originInfo>
                                <mods:dateIssued encoding="iso8601"><xsl:value-of select="date"/></mods:dateIssued>
                                <mods:place>
                                    <mods:placeTerm>Manchester, UK</mods:placeTerm>
                                </mods:place>
                                <mods:publisher>University of Manchester</mods:publisher>
                            </mods:originInfo>
                            <mods:part>
                                <mods:extent unit="page">
                                    <mods:total></mods:total>
                                </mods:extent>
                            </mods:part>
                            <mods:recordInfo>
                                <mods:recordCreationDate encoding="iso8601"><xsl:value-of select="current-dateTime()"/></mods:recordCreationDate>
                                <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
                            </mods:recordInfo>
                            <mods:subject>
                                <xsl:for-each select="tokenize(categorisation, ';')">
                                    <mods:topic><xsl:value-of select="normalize-space(.)"/></mods:topic>                                    
                                </xsl:for-each>
                                <xsl:for-each select="tokenize(comment, ';')">
                                    <mods:topic><xsl:value-of select="normalize-space(.)"/></mods:topic>                                    
                                </xsl:for-each>
                            </mods:subject>
                            <mods:titleInfo>
                                <mods:title><xsl:value-of select="title"/></mods:title>
                            </mods:titleInfo>
                            <mods:tableOfContents><xsl:value-of select="toc"/></mods:tableOfContents>
                            <mods:typeOfResource>text</mods:typeOfResource>
                        </mods:mods>
                    </foxml:xmlContent>                    
                </xsl:element>                                
            </foxml:datastream>
            
            <foxml:datastream CONTROL_GROUP="M" STATE="A" VERSIONABLE="true">
                <xsl:attribute name="ID" select="'Fulltext.pdf'"/>
                <foxml:datastreamVersion>
                    <xsl:attribute name="CREATED">
                        <xsl:call-template name="getCurrentDateTime"/>
                    </xsl:attribute>
                    <xsl:attribute name="ID" select="'Fulltext.pdf.0'"/>
                    <xsl:attribute name="LABEL" select="'Full text'"/>
                    <xsl:attribute name="MIMETYPE">
                        <xsl:value-of select="'application/pdf'"/>
                    </xsl:attribute>
                    <foxml:contentLocation>
                        <xsl:variable name="pid">
                            <xsl:call-template name="createPid">
                                <xsl:with-param name="sequence" select="$pid_name"/>
                            </xsl:call-template>                                        
                        </xsl:variable>
                        <xsl:attribute name="REF">
                            <xsl:value-of select="concat('http://localhost/uploaded/theses/', replace(attachment, ' ', '-'))"/>
                        </xsl:attribute>
                        <xsl:attribute name="TYPE" select="'URL'"/>                                    
                    </foxml:contentLocation>
                </foxml:datastreamVersion>
            </foxml:datastream>                        
<!--            
            <foxml:datastream ID="FULL-TEXT.PDF" STATE="A" CONTROL_GROUP="M" VERSIONABLE="false">
                <foxml:datastreamVersion ID="FULL-TEXT.0" LABEL="Full text"
                    CREATED="2015-01-07T07:11:35.539Z" MIMETYPE="application/pdf" SIZE="-1">
                    <foxml:contentLocation TYPE="INTERNAL_ID"
                        REF="uk-ac-man-scw:245003+FULL-TEXT.PDF+FULL-TEXT.0"/>
                </foxml:datastreamVersion>
            </foxml:datastream>
-->            

            <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
                <xsl:element name="foxml:datastreamVersion">
                    <xsl:attribute name="CREATED">
                        <xsl:call-template name="getCurrentDateTime"/>
                    </xsl:attribute>
                    <xsl:attribute name="ID" select="'RELS-EXT.1'"/>
                    <xsl:attribute name="LABEL" select="'Relationships'"/>
                    <xsl:attribute name="MIMETYPE" select="'text/xml'"/>
                    <xsl:attribute name="SIZE" select="973"/>
                    <foxml:xmlContent>
                        <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                            <!--<rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">-->
                            <rdf:Description>
                                
                                <xsl:attribute name="rdf:about">info:fedora/<xsl:call-template name="createPid"><xsl:with-param name="sequence" select="$pid_name"/></xsl:call-template></xsl:attribute>
                                <!-- BA: Change here... -->
                                <!-- BA: 12/10/11 - manually hardcode virtual person here... -->
                                <xsl:element name="esc-rel:isBelongsTo">
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of select="'info:fedora/uk-ac-man-per:abcde18'"/>
                                    </xsl:attribute>
                                </xsl:element>
                                
                                <xsl:element name="esc-rel:wasBelongsToOrg">
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of select="concat('info:fedora/', normalize-space(faculty-id))"/>
                                    </xsl:attribute>
                                </xsl:element>                                                                                                        
                                
                                <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                                <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                                <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                                <!-- BA: Change here... -->
                                <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:15"/>
                            </rdf:Description>
                        </rdf:RDF>
                    </foxml:xmlContent>                    
                </xsl:element>
            </foxml:datastream>                    
        </foxml:digitalObject>       
    </xsl:template>
    
    <xsl:template name="createPid">
        <xsl:param name="sequence"/>
        <!-- BA: Change here... -->
        <xsl:text>uk-ac-man-scw:</xsl:text><xsl:value-of select="$sequence"/>
    </xsl:template>
    
    <xsl:template name="showDate">
        <xsl:value-of select="Year"/><xsl:text>-&#8206;</xsl:text>
        <xsl:value-of select="Month"/>
        <xsl:text>-&#8206;</xsl:text><xsl:value-of select="Day"/>
    </xsl:template>
    
    <xsl:template name="getCurrentDateTime">
        <xsl:choose>
            <xsl:when test="contains(string(current-dateTime()), '+')">
                <xsl:value-of select="replace(concat(substring-before(string(current-dateTime()), '+'), 'Z'), 'ZZ', 'Z')"/>                        
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="replace(concat(string(current-dateTime()), 'Z'), 'ZZ', 'Z')"/>                        
            </xsl:otherwise>
        </xsl:choose>        
    </xsl:template>
    
</xsl:stylesheet>