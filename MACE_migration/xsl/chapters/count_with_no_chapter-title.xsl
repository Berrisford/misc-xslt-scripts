<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">    
    <xsl:output encoding="UTF-8" method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <root>
            <no-chapter-title-count>
                <xsl:value-of select="count(//publication[chapter-title = ''])"/>
            </no-chapter-title-count>
            <dateTime>
                <xsl:choose>
                    <xsl:when test="contains(string(current-dateTime()), '+')">
                        <xsl:value-of select="concat(substring-before(string(current-dateTime()), '+'), 'Z')"/>                        
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat(string(current-dateTime()), 'Z')"/>                        
                    </xsl:otherwise>
                </xsl:choose>
            </dateTime>
<!--            
            <dodgy-spotids>
                <xsl:apply-templates select="//spotids"/>
            </dodgy-spotids>
-->
            <no-chapter-titles>
                <xsl:apply-templates select="//chapter-title[. = '']"/>
            </no-chapter-titles>            
        </root>
    </xsl:template>
    
    <xsl:template match="chapter-title">
        <no-chapter-title>
            <book-title>
                <xsl:value-of select="../book-title"/>
            </book-title>
            <isbn>
                <xsl:value-of select="../isbn"/>                
            </isbn>            
            <book-year>
                <xsl:value-of select="../book-year"/>                
            </book-year>
            <publisher>
                <xsl:value-of select="../publisher"/>                
            </publisher>
            <editors>
                <xsl:value-of select="../editors"/>                
            </editors>
        </no-chapter-title>
    </xsl:template>
    
    
    <xsl:template match="spotids">
        <xsl:for-each select="tokenize(., ',')">
            <xsl:choose>        
                <xsl:when test="string-length(normalize-space(.)) lt 7">
                    <dodgy-spotid><xsl:value-of select="."/></dodgy-spotid>
                </xsl:when>
            </xsl:choose>        
        </xsl:for-each>
    </xsl:template>
    
</xsl:stylesheet>