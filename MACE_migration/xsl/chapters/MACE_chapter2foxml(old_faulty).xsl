<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" name="xml"/>
    
    <xsl:variable name="root_url" select="'http://fedprdir.library.manchester.ac.uk:8085/solr/nonscw/select/'"></xsl:variable>
 
    <xsl:template match="/">
        <xsl:for-each select="publications/publication">
            <xsl:variable name="pid_count">
                <xsl:number />
            </xsl:variable>
            <xsl:variable name="pid_name">
                <!-- BA: Change here... -->                
                <xsl:value-of select="concat('ma', 1634 + $pid_count)" />                                                                
            </xsl:variable>
            <xsl:variable name="file">
                <!-- BA: Change here... -->
                <xsl:value-of select="concat('output/', 'uk-ac-man-scw_', $pid_name, '.xml')" />
            </xsl:variable>
            <xsl:result-document href="{$file}" method="xml" indent="yes">
                <xsl:call-template name="publication_foxml">
                    <xsl:with-param name="pid_name" select="$pid_name"/>
                </xsl:call-template>
            </xsl:result-document>            
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="publication_foxml">
        <xsl:param name="pid_name"/>        
        <foxml:digitalObject VERSION="1.1" xmlns:foxml="info:fedora/fedora-system:def/foxml#"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="info:fedora/fedora-system:def/foxml# http://www.fedora.info/definitions/1/0/foxml1-1.xsd">
            <xsl:attribute name="PID"><xsl:call-template name="createPid">
                <xsl:with-param name="sequence" select="$pid_name"/>
            </xsl:call-template></xsl:attribute>
            <foxml:objectProperties>
                <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
            </foxml:objectProperties>

            <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
                <xsl:element name="foxml:datastreamVersion">
                    <xsl:attribute name="CREATED">
                        <xsl:call-template name="getCurrentDateTime"/>
                    </xsl:attribute>
                    <xsl:attribute name="ID" select="'MODS.0'"/>
                    <xsl:attribute name="LABEL" select="'Metadata Object Description Schema record'"/>
                    <xsl:attribute name="MIMETYPE" select="'text/xml'"/>
                    <xsl:attribute name="SIZE" select="'3550'"/>
                    
                    <foxml:xmlContent>
                        <mods:mods version="3.3" xmlns:mods="http://www.loc.gov/mods/v3"
                            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd">
                            
                            <mods:abstract><xsl:value-of select="abstract"/></mods:abstract>
                            <mods:genre authority="local" type="content-type">book-contribution</mods:genre>
                            <mods:genre type="version"><xsl:value-of select="chapter-title"/></mods:genre>
                            <mods:note type="authors"><xsl:value-of select="replace(owners, ',', ', ')"/></mods:note>
                            <mods:note type="editors"><xsl:value-of select="editors"/></mods:note>
                            <mods:originInfo>
                                <xsl:choose>
                                    <xsl:when test="book-month ne ''">
                                        <xsl:variable name="month">
                                            <xsl:value-of select="format-number(
                                                string-length(substring-before(
                                                'JanFebMarAprMayJunJulAugSepOctNovDec',
                                                substring(book-month,1,3))) div 3 + 1,'00')"/>
                                        </xsl:variable>                                
                                        <mods:dateIssued encoding="iso8601"><xsl:value-of select="concat(book-year, '-', $month)"/></mods:dateIssued>                                        
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <mods:dateIssued encoding="iso8601"><xsl:value-of select="book-year"/></mods:dateIssued>                                                                                
                                    </xsl:otherwise>
                                </xsl:choose>
                                <mods:publisher><xsl:value-of select="publisher"/></mods:publisher>
                                <!--<mods:edition><xsl:value-of select="editions"/></mods:edition>-->                                
                            </mods:originInfo>
                            <mods:recordInfo>
                                <mods:recordCreationDate encoding="iso8601"><xsl:value-of select="current-dateTime()"/></mods:recordCreationDate>
                                <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
                            </mods:recordInfo>
                            <mods:relatedItem type="host">
                                <mods:identifier type="isbn"><xsl:value-of select="isbn"/></mods:identifier>                                                            
                                <mods:titleInfo>
                                    <mods:title><xsl:value-of select="book-title"/></mods:title>
                                </mods:titleInfo>                                
                                <mods:originInfo>
                                    <mods:edition><xsl:value-of select="editions"/></mods:edition>
                                </mods:originInfo>                                
                            </mods:relatedItem>                            
                            <mods:relatedItem type="preceding">
                                <mods:identifier type="uri"><xsl:value-of select="url"/></mods:identifier>
                            </mods:relatedItem>                                                       
                            <mods:titleInfo>
                                <mods:title><xsl:value-of select="chapter-title"/></mods:title>
                            </mods:titleInfo>
                            <mods:typeOfResource>text</mods:typeOfResource>                            
                        </mods:mods>
                    </foxml:xmlContent>                    
                </xsl:element>                                
            </foxml:datastream>

            <xsl:choose>
                <xsl:when test="pdf-filename ne ''">
                    <foxml:datastream CONTROL_GROUP="M" ID="FULL-TEXT.PDF" STATE="A" VERSIONABLE="false">
                        <xsl:element name="foxml:datastreamVersion">
                            <xsl:attribute name="ID" select="'FULL-TEXT.PDF.0'"/>
                            <xsl:attribute name="LABEL" select="pdf-filename"/>
                            <xsl:attribute name="MIMETYPE" select="'application/pdf'"/>
                            <foxml:contentLocation TYPE="URL">
                                <xsl:attribute name="REF">
                                    <xsl:value-of select="concat('http://fedprdir.library.manchester.ac.uk/uploaded/mace/', replace(pdf-filename, ' ', ''))"/>
                                </xsl:attribute>
                            </foxml:contentLocation>                            
                        </xsl:element>
                    </foxml:datastream>
                </xsl:when>
            </xsl:choose>            
            
            <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
                <xsl:element name="foxml:datastreamVersion">
                    <xsl:attribute name="CREATED">
                        <xsl:call-template name="getCurrentDateTime"/>
                    </xsl:attribute>
                    <xsl:attribute name="ID" select="'RELS-EXT.1'"/>
                    <xsl:attribute name="LABEL" select="'Relationships'"/>
                    <xsl:attribute name="MIMETYPE" select="'text/xml'"/>
                    <xsl:attribute name="SIZE" select="920"/>
                    <foxml:xmlContent>
                        <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                            <rdf:Description>
                                <xsl:attribute name="rdf:about">info:fedora/<xsl:call-template name="createPid"><xsl:with-param name="sequence" select="$pid_name"/></xsl:call-template></xsl:attribute>
                                <!-- BA: Change here... -->
                                <xsl:for-each select="tokenize(esc-group-id, ',')">
                                    <xsl:element name="esc-rel:isBelongsTo">
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of select="concat('info:fedora/uk-ac-man-per:', normalize-space(.))"></xsl:value-of>
                                        </xsl:attribute>
                                    </xsl:element>
                                </xsl:for-each>
                                <xsl:for-each select="tokenize(spotids, ',')">
                                    <xsl:variable name="query">
                                        <xsl:value-of select="concat($root_url, '?q=p.partynumber%3A', normalize-space(.), '&amp;omitHeader=true')"></xsl:value-of>
                                    </xsl:variable>
                                    <xsl:variable name="obj_solr_doc">
                                        <xsl:copy-of select="doc($query)/response/result/doc"/>
                                    </xsl:variable>
                                    <xsl:choose>
                                        <xsl:when test="$obj_solr_doc/doc/str[@name='PID'] ne ''">
                                            <xsl:element name="esc-rel:isBelongsTo">
                                                <xsl:attribute name="rdf:resource">
                                                    <xsl:value-of select="concat('info:fedora/', $obj_solr_doc/doc/str[@name='PID'])"/>
                                                </xsl:attribute>
                                            </xsl:element>
                                            <xsl:for-each select="$obj_solr_doc/doc/arr[@name='r.ismemberof.pid']/str">
                                                <xsl:choose>
                                                    <xsl:when test=". ne ''">
                                                        <xsl:element name="esc-rel:isBelongsToOrg">
                                                            <xsl:attribute name="rdf:resource">
                                                                <xsl:value-of select="concat('info:fedora/', normalize-space(.))"/>
                                                            </xsl:attribute>
                                                        </xsl:element>                                                                                                        
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:for-each>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:for-each>
                                <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                                <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                                <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                                <!-- BA: Change here... -->
                                <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:3"/>
                            </rdf:Description>
                        </rdf:RDF>
                    </foxml:xmlContent>                    
                </xsl:element>
            </foxml:datastream>            
        </foxml:digitalObject>       
    </xsl:template>

    <xsl:template name="createPid">
        <xsl:param name="sequence"/>
        <!-- BA: Change here... -->
        <xsl:text>uk-ac-man-scw:</xsl:text><xsl:value-of select="$sequence"/>
    </xsl:template>

    <xsl:template name="showDate">
        <xsl:value-of select="Year"/><xsl:text>-&#8206;</xsl:text>
        <xsl:value-of select="Month"/>
        <xsl:text>-&#8206;</xsl:text><xsl:value-of select="Day"/>
    </xsl:template>
    
    <xsl:template name="getCurrentDateTime">
        <xsl:choose>
            <xsl:when test="contains(string(current-dateTime()), '+')">
                <xsl:value-of select="concat(substring-before(string(current-dateTime()), '+'), 'Z')"/>                        
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat(string(current-dateTime()), 'Z')"/>                        
            </xsl:otherwise>
        </xsl:choose>        
    </xsl:template>
    
</xsl:stylesheet>