<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">    
    <xsl:output encoding="UTF-8" method="xml" indent="yes"/>
    <xsl:variable name="pid_root" select="'uk-ac-man-scw_ma'"/>
    <xsl:variable name="start" select="1635"/>
    <xsl:variable name="end" select="1729"/>
    
    <xsl:template match="/">
        <pids>
            <xsl:apply-templates select="//chapter-title"/>
        </pids>            
    </xsl:template>
    
    
</xsl:stylesheet>