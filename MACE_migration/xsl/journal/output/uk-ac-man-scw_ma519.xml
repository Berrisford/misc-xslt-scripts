<?xml version="1.0" encoding="UTF-8"?>
<foxml:digitalObject xmlns:foxml="info:fedora/fedora-system:def/foxml#"
                     VERSION="1.1"
                     PID="uk-ac-man-scw:ma519">
   <foxml:objectProperties>
      <foxml:property NAME="info:fedora/fedora-system:def/model#state" VALUE="Active"/>
   </foxml:objectProperties>
   <foxml:datastream CONTROL_GROUP="X" ID="MODS" STATE="A" VERSIONABLE="true">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="MODS.0"
                               LABEL="Metadata Object Description Schema record"
                               MIMETYPE="text/xml"
                               SIZE="3550">
         <foxml:xmlContent>
            <mods:mods xmlns:mods="http://www.loc.gov/mods/v3"
                       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       version="3.3"
                       xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd">
               <mods:abstract>The mission of the European Network on Neutron Techniques Standardization for Structural Integrity (NeT) is to develop experimental and numerical techniques and standards for the reliable characterisation of residual stresses in structural welds. NeT was established in 2002 by the European Union Joint Research Centre, and over 35 organisations from Europe and beyond are involved in the network. NeT operates on a contribution in kind basis from industrial, academic, and research facility partners. Each problem examined by NeT is tackled by creating a dedicated Task Group which undertakes measurement and modelling studies and the interpretation of the results. Task Group 1 (TG1) was formed to examine the benchmark problem of a single weld bead laid down on the top surface of an austenitic steel plate. This weld geometry produces a strongly three-dimensional residual stress distribution, with similar characteristics to a weld repair, in a compact, portable specimen, which is amenable to residual stress measurement using diverse methods. The single weld pass is relatively straightforward to model using finite element methods, and allows full moving heat source residual stress simulations to be performed in practical time scales. However, because the weld bead is laid onto a relatively thin plate, the predicted stresses are very sensitive to key modelling assumptions such as total heat input and the thermal transient time history, making the bead-on-plate a challenging benchmark problem. Two parallel round robin activities were performed as part of TG1, covering residual stress measurement and residual stress prediction. Four nominally identical bead-on-plate specimens were manufactured under closely controlled and documented conditions, and appropriate materials' characterisation testing was performed on both plate and weld material. Two protocol documents were written to define and control the experimental residual stress measurement and finite element simulation round robins, respectively. The bead-on-plate specimens were then circulated to TG1 measurement round robin participants to perform residual stress measurements. In parallel, the finite element simulation protocol was circulated to TG1 simulation round robin participants to allow them to predict the transient welding temperature history and the weld residual stresses in advance of measurements being available. The results from this initial round robin are referred to as Phase 1 results and form the basis of the technical papers contained within this special issue. It is noted that prior to the Phase 1 round robin, the weld fusion boundary profile was measured in test beads deposited at the same time and using the same welding conditions as the benchmark bead-on-plate specimens. The weld efficiency, , was not specified during the Phase 1 round robin. Analysts were expected to deduce the efficiency by matching both the fusion boundary position and transient temperatures during welding. However, after completion of Phase 1, two of the bead-on-plate specimens were destructively examined to confirm the weld fusion boundary geometry, and a number of finite element sensitivity analyses performed to confirm the importance of selected finite element solution variables. The insights gained from the round robin activities were then incorporated into a Phase 2 finite element simulation protocol. This reduced the number of free solution variables available to participants, and was focussed on examining those aspects of the problem not fully understood after completion of the first round robin. In particular, an efficiency of 75% was specified in the final Phase 2 version of the protocol. The Phase 2 simulation round robin is currently under way.</mods:abstract>
               <mods:genre authority="local" type="content-type">journal-article</mods:genre>
               <mods:genre type="version">Original research</mods:genre>
               <mods:genre type="form">Academic journal article</mods:genre>
               <mods:language>
                  <mods:languageTerm authority="iso639-3" type="code">eng</mods:languageTerm>
               </mods:language>
               <mods:note type="authors">C E Turski, Michael Smith</mods:note>
               <mods:originInfo>
                  <mods:dateIssued encoding="iso8601">2009</mods:dateIssued>
                  <mods:publisher>Elsevier Science</mods:publisher>
               </mods:originInfo>
               <mods:recordInfo>
                  <mods:recordCreationDate encoding="iso8601">2015-05-19T17:06:30.648+01:00</mods:recordCreationDate>
                  <mods:recordContentSource>Manchester eScholar</mods:recordContentSource>
               </mods:recordInfo>
               <mods:relatedItem type="host">
                  <mods:titleInfo>
                     <mods:title>International Journal of Pressure Vessels and Piping</mods:title>
                  </mods:titleInfo>
                  <mods:part>
                     <mods:detail type="issue">
                        <mods:number>1</mods:number>
                     </mods:detail>
                     <mods:detail type="volume">
                        <mods:number>86</mods:number>
                     </mods:detail>
                     <mods:extent unit="page">
                        <mods:start>02</mods:start>
                        <mods:end>Jan</mods:end>
                        <mods:total>NaN</mods:total>
                        <mods:list>02-Jan</mods:list>
                     </mods:extent>
                  </mods:part>
                  <mods:identifier type="issn">0308-0161</mods:identifier>
               </mods:relatedItem>
               <mods:relatedItem type="preceding">
                  <mods:titleInfo>
                     <mods:title/>
                  </mods:titleInfo>
                  <mods:identifier type="uri"/>
               </mods:relatedItem>
               <mods:titleInfo>
                  <mods:title>The NeT residual stress measurement and analysis round robin on a single weld bead-on-plate specimen</mods:title>
               </mods:titleInfo>
               <mods:typeOfResource>text</mods:typeOfResource>
            </mods:mods>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
   <foxml:datastream CONTROL_GROUP="X" ID="RELS-EXT" STATE="A" VERSIONABLE="false">
      <foxml:datastreamVersion CREATED="2015-05-19T17:06:30.648Z"
                               ID="RELS-EXT.1"
                               LABEL="Relationships"
                               MIMETYPE="text/xml"
                               SIZE="920">
         <foxml:xmlContent>
            <rdf:RDF xmlns:esc-rel="http://www.escholar.manchester.ac.uk/esc/rel/v1#"
                     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
               <rdf:Description rdf:about="info:fedora/uk-ac-man-scw:ma519">
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde39"/>
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde41"/>
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde42"/>
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:abcde47"/>
                  <esc-rel:isBelongsTo rdf:resource="info:fedora/uk-ac-man-per:122666"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:1"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:41"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:203"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:1"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:41"/>
                  <esc-rel:isBelongsToOrg rdf:resource="info:fedora/uk-ac-man-org:347"/>
                  <esc-rel:isCreatedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isLastModifiedBy rdf:resource="info:fedora/uk-ac-man-per:admin"/>
                  <esc-rel:isDerivationOf rdf:resource="info:fedora/uk-ac-man-scw:base"/>
                  <esc-rel:isOfContenttype rdf:resource="info:fedora/uk-ac-man-con:1"/>
               </rdf:Description>
            </rdf:RDF>
         </foxml:xmlContent>
      </foxml:datastreamVersion>
   </foxml:datastream>
</foxml:digitalObject>
