<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
   <xsl:output indent="yes" method="xml"/>
   
   <xsl:template match="/">
       <xsl:apply-templates select="pdfx" />
   </xsl:template>
    
   <xsl:template match="pdfx">
       <PubmedArticleSet>
           <PubmedArticle>
               <MedlineCitation Owner="'Owner'" Status="'Status'">
                   <PMID Version="1">-</PMID>
                   <DateCreated>
                       <Year><xsl:value-of select="year-from-date(current-date())"></xsl:value-of></Year>
                       <Month><xsl:value-of select="month-from-date(current-date())"></xsl:value-of></Month>
                       <Day><xsl:value-of select="day-from-date(current-date())"></xsl:value-of></Day>
                   </DateCreated>
                   <Article PubModel="PubModel">
                       <Journal>
                           <ISSN IssnType="IssnType"></ISSN>
                           <JournalIssue CitedMedium="CitedMedium">
                               <Volume></Volume>
                               <Issue></Issue>
                               <PubDate>
                                   <Year>1900</Year>
                                   <Month>1</Month>
                                   <Day>1</Day>
                               </PubDate>
                           </JournalIssue>
                           <Title>
                               <xsl:for-each select="article/front/outsider[@type='header']">
                                   <xsl:variable name="title_count"><xsl:number/></xsl:variable>
                                   <xsl:call-template name="title">
                                       <xsl:with-param name="title_count" select="$title_count"></xsl:with-param>
                                   </xsl:call-template>
                               </xsl:for-each>
                           </Title>
                           <ISOAbbreviation></ISOAbbreviation>
                       </Journal>
                       <ArticleTitle><xsl:value-of select="article/front/title-group/article-title"></xsl:value-of></ArticleTitle>
                       <Pagination>
                           <xsl:for-each select="article/front/outsider[@type='header']">
                               <xsl:choose>
                                   <xsl:when test="substring-before(substring-after(., 'pages '), ' ') ne ''">
                                       <MedlinePgn><xsl:value-of select="substring-before(substring-after(., 'pages '), ' ')"/></MedlinePgn>                                   
                                   </xsl:when>
                               </xsl:choose>                               
                           </xsl:for-each>
                           
                       </Pagination>
                       <Abstract>
                           <AbstractText><xsl:value-of select="article/front/abstract/text()"/></AbstractText>
                       </Abstract>
                       <Affiliation>
                           <xsl:for-each select="article/front/footnote">
                               <xsl:variable name="count"><xsl:number/></xsl:variable>                           
                               <xsl:call-template name="footnote">
                                   <xsl:with-param name="counter" select="$count"/>
                               </xsl:call-template>                               
                           </xsl:for-each>
                       </Affiliation>
                       <AuthorList>
                           <xsl:for-each select="article/front/contrib-group/contrib[@contrib-type='author']/name">
                               <xsl:call-template name="author"/>
                           </xsl:for-each>
                       </AuthorList>

                       <!--<Language>eng</Language>-->
                       <PublicationTypeList>
                           <PublicationType>Journal Article</PublicationType>
                       </PublicationTypeList>                                              

                   </Article>
<!--
                   <MedlineJournalInfo>
                       <Country>China</Country>
                       <MedlineTA>World J Biol Chem</MedlineTA>
                       <NlmUniqueID>101546471</NlmUniqueID>
                       <ISSNLinking>1949-8454</ISSNLinking>
                   </MedlineJournalInfo>
-->                   
               </MedlineCitation>

               <PubmedData>
                   <PublicationStatus>ppublish</PublicationStatus>
                   <ArticleIdList>
                        <ArticleId IdType="doi"><xsl:value-of select="/pdfx/meta/doi"/></ArticleId>                                                      
                   </ArticleIdList>
               </PubmedData>
                              
           </PubmedArticle>
       </PubmedArticleSet>
   </xsl:template>

    <xsl:template name="author">
        <xsl:choose>
            <xsl:when test="not(contains(., '|'))">
                <xsl:variable name="author_name" select="tokenize(., ' ')"></xsl:variable>
                <xsl:choose>
                    <xsl:when test="count($author_name) &gt; 3">
                        <ForeName><xsl:value-of select="."/></ForeName>
                    </xsl:when>
                    <xsl:when test="count($author_name) &lt; 4">
                        <Author>
                            <LastName>
                                <xsl:try>
                                    <xsl:choose>
                                        <xsl:when test="$author_name[3]">
                                            <xsl:value-of select="$author_name[3]"/>                            
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="$author_name[2]"/>                            
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <xsl:catch>
                                        <xsl:value-of select="$author_name[2]"/>
                                    </xsl:catch>
                                </xsl:try>
                            </LastName>
                            <ForeName>
                                <xsl:value-of select="$author_name[1]"></xsl:value-of>
                            </ForeName>
                            <Initials>
                                <xsl:try>
                                    <xsl:if test="$author_name[3]">
                                        <xsl:value-of select="$author_name[2]"></xsl:value-of>
                                    </xsl:if>
                                    <xsl:catch></xsl:catch>
                                </xsl:try>
                            </Initials>
                        </Author>                        
                    </xsl:when>
                </xsl:choose>                                
            </xsl:when>
            <xsl:otherwise>
                <xsl:try>
                    <xsl:variable name="journal" select="tokenize(., '\|')"/>
                    <ForeName>
                        <xsl:value-of select="normalize-space($journal[3])"></xsl:value-of>
                    </ForeName>
                    <LastName/>
                    <xsl:catch></xsl:catch>                    
                </xsl:try>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>

    <xsl:template name="footnote">
        <xsl:param name="counter"/>
        <xsl:choose>
            <xsl:when test="$counter &gt; 1">
                <xsl:text>; </xsl:text>
            </xsl:when>
        </xsl:choose>
        <xsl:value-of select="text()"/>
    </xsl:template>

    <xsl:template name="title">
        <xsl:param name="title_count"/>
        <xsl:choose>
            <xsl:when test="$title_count &gt; 1">
                <xsl:text>; </xsl:text>
            </xsl:when>
        </xsl:choose>
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>

    <xsl:template match="text()" >
        <xsl:value-of select="normalize-space(.)" />
    </xsl:template>
        
</xsl:stylesheet>