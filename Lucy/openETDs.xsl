<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math" exclude-result-prefixes="xs math"
    version="3.0">

    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <xsl:result-document href="openETDs2xls.xml">
        <results>
            <xsl:for-each select="//doc">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </results>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="doc">
        <xsl:variable name="person-pid" select="substring-after(str[@name='r.isbelongstostudent.pid']/text(), ':')"/>
        <xsl:variable name="person-url">
            <xsl:value-of select="concat('http://escholarprd.library.manchester.ac.uk:8085/solr/nonscw/select?q=PID%3Auk-ac-man-per%5C%3A', $person-pid, '&amp;wt=xml&amp;indent=true')"/>    
        </xsl:variable>
        <xsl:variable name="person-solr">
            <xsl:copy-of select="doc($person-url)"/>
        </xsl:variable>
        <etd>
            <window-id>
                <xsl:value-of select="str[@name='PID']"/>
            </window-id>
            <student-name>
                <xsl:value-of select="str[@name='r.isbelongstostudent.displayname']"/>
            </student-name>
            <student-pid>
                <xsl:value-of select="str[@name='r.isbelongstostudent.pid']"/>
            </student-pid>
            <library-card-no>
                <xsl:value-of select="str[@name='e.partynumber']"/>
            </library-card-no>
            
            <xsl:for-each select="$person-solr/response/result/doc/arr[@name='r.ismemberof.pid']/str">
                <xsl:variable name="count" select="position()"/>
                <xsl:variable name="org-url">
                    <xsl:value-of select="concat('http://escholarprd.library.manchester.ac.uk:8085/solr/nonscw/select?q=PID%3Auk-ac-man-org%5C%3A', substring-after(./text(), ':'), '&amp;wt=xml&amp;indent=true')"/>
                </xsl:variable>
                <xsl:variable name="org-solr">
                    <xsl:copy-of select="doc($org-url)"/>
                </xsl:variable>
                <xsl:variable name="org-name" select="concat('organisation-', $count)"/>
                <xsl:element name="{$org-name}">
                    <xsl:value-of select="$org-solr/response/result/doc/str[@name='o.name']"/>
                </xsl:element>
            </xsl:for-each>
            <date-opened>
                <xsl:value-of select="str[@name='e.date.opened']"/>
            </date-opened>
            <expected-close-date>
                <xsl:value-of select="str[@name='e.expectedclosedate']"/>
            </expected-close-date>            
            <opened-by>
                <xsl:value-of select="str[@name='r.isopenedby.displayname']"/>
            </opened-by>
            <programme-name>
                <xsl:value-of select="str[@name='e.programmename']"/>
            </programme-name>
            <programme-id>
                <xsl:value-of select="str[@name='e.programmeid']"/>
            </programme-id>
            <plan-name>
                <xsl:value-of select="str[@name='e.planname']"/>
            </plan-name>
            <plan-id>
                <xsl:value-of select="str[@name='e.planid']"/>
            </plan-id>
            <qualification>
                <xsl:value-of select="str[@name='e.qualification']"/>
            </qualification>
        </etd>
    </xsl:template>

</xsl:stylesheet>
