echo Start time: %DATE% %TIME%
 java -Xms4000m -Xmx4000m -jar "C:\saxon\saxon9pe.jar" -s:etds.xml -xsl:get-solr.xsl -o:sorted-solr-results.xml
echo End time: %DATE% %TIME%
pause