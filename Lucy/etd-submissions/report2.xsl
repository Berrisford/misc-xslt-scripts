<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:variable name="result-doc">
        <xsl:copy-of select="doc('solr-results(short).xml')/response/result"/>
    </xsl:variable>
    
    <xsl:template match="/">
        <xsl:apply-templates select="//doc[str[@name='r.isofetdtype.pid'] eq 'uk-ac-man-etdtype:1']"/>

    </xsl:template>
    
    <xsl:template match="doc">
        <xsl:variable name="student-pid" select="str[@name='r.isbelongstostudent.pid']"/>
        <!--<student-name><xsl:value-of select="str[@name='e.displayname']"/></student-name>-->

        <xsl:choose>
            <xsl:when test="not(exists($result-doc/result/doc[(str[@name='r.isbelongstostudent.pid'] eq $student-pid) and (str[@name='r.isofetdtype.pid'] eq 'uk-ac-man-etdtype:3')]))">
                <found-one></found-one>
            </xsl:when>
        </xsl:choose>
        
        <!--
        <xsl:for-each select="$result-doc/result/doc[str[@name='r.isbelongstostudent.pid'] eq $student-pid]">
            <xsl:choose>
                <xsl:when test="not(exists(./str[@name='r.isofetdtype.pid'] eq 'uk-ac-man-etdtype:3'))">
                    <etd-pid><xsl:value-of select="str[@name='PID']"/></etd-pid>
                    <student-name><xsl:value-of select="str[@name='e.displayname']"/></student-name>                    
                </xsl:when>                
                <xsl:otherwise>
                    <otherwise></otherwise>
                </xsl:otherwise>                
            </xsl:choose>             
        </xsl:for-each>
        -->
<!--
        <xsl:for-each select="$result-doc/result/doc[str[@name='r.isbelongstostudent.pid'] eq $student-pid]">
            <xsl:choose>
                <xsl:when test="str[@name='r.isofetdtype.pid'] eq 'uk-ac-man-etdtype:3'"></xsl:when>
                <xsl:otherwise>
                    <etd-pid><xsl:value-of select="str[@name='PID']"/></etd-pid>
                    <student-name><xsl:value-of select="str[@name='e.displayname']"/></student-name>
                </xsl:otherwise>
            </xsl:choose>             
        </xsl:for-each>
        -->
<!--        
        <xsl:for-each select="$result-doc/result/doc[str[@name='r.isofetdtype.pid'] eq 'uk-ac-man-etdtype:3']">
            <xsl:choose>
                <xsl:when test="str[@name='r.isbelongstostudent.pid'] eq $student-pid"></xsl:when>
                <xsl:otherwise>
                    <etd-pid><xsl:value-of select="./str[@name='PID']"/></etd-pid>
                    <student-name><xsl:value-of select="./str[@name='e.displayname']"/></student-name>
                </xsl:otherwise>
            </xsl:choose>             
        </xsl:for-each>
-->        

    </xsl:template>
    
</xsl:stylesheet>