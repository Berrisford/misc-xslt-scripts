<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes" />
    
    <!--<xsl:variable name="url" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/etd/select?q=(r.isofetdtype.pid%3Auk-ac-man-etdtype%5C%3A1+OR%0Ar.isofetdtype.pid%3Auk-ac-man-etdtype%5C%3A3)+AND%0Ae.windowstate%3ACLOSED+AND%0Ax.createddate%3A%5B2009-01-01T00%3A00%3A000Z+TO+NOW%5D+AND%0A-e.submissionstate%3AREJECTED%0A&amp;rows=20000&amp;fl=PID%2C+x.createddate%2C+r.isbelongstostudent.pid%2C+e.partynumber%2C+e.displayname%2C+e.windowstate%2C+e.submissionstate%2C+e.expectedopendate%2C+e.expectedclosedate%2C+r.iscreatedby.displayname&amp;wt=xml&amp;indent=true'" />-->
    <!--<xsl:variable name="url" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/etd/select?q=(r.isofetdtype.pid%3Auk-ac-man-etdtype%5C%3A1+OR%0Ar.isofetdtype.pid%3Auk-ac-man-etdtype%5C%3A3)+AND%0Ae.windowstate%3ACLOSED+AND%0Ax.createddate%3A%5B2009-01-01T00%3A00%3A000Z+TO+NOW%5D+AND%0A-e.submissionstate%3AREJECTED%0A&amp;rows=20000&amp;fl=PID%2C+x.createddate%2C+r.isbelongstostudent.pid%2C+e.partynumber%2C+e.displayname%2C+e.windowstate%2C+e.submissionstate%2C+e.expectedopendate%2C+e.expectedclosedate%2C+r.iscreatedby.displayname%2C++r.isofetdtype.pid&amp;wt=xml&amp;indent=true&amp;sort=x.createddate desc'" />-->
    <xsl:variable name="url" select="'http://escholarprd.library.manchester.ac.uk:8085/solr/etd/select?q=(r.isofetdtype.pid%3Auk-ac-man-etdtype%5C%3A1+OR%0Ar.isofetdtype.pid%3Auk-ac-man-etdtype%5C%3A3+OR%0Ar.isofetdtype.pid%3Auk-ac-man-etdtype%5C%3A4)+AND%0Ax.createddate%3A%5B2009-01-01T00%3A00%3A000Z+TO+NOW%5D+AND%0A-e.submissionstate%3AREJECTED+AND%0A-e.windowstate%3ACANCELLED%0A&amp;sort=x.createddate+desc&amp;rows=20000&amp;fl=PID%2C+x.createddate%2C+r.isbelongstostudent.pid%2C+e.partynumber%2C+e.displayname%2C+e.windowstate%2C+e.submissionstate%2C+e.expectedopendate%2C+e.expectedclosedate%2C+r.iscreatedby.displayname%2C++r.isofetdtype.pid%2C++e.date.submitted&amp;wt=xml&amp;indent=true'" />
    

    <xsl:template match="/">        
        <!--<xsl:result-document href="solr-result.xml">-->
            <xsl:copy-of select="doc($url)"/>
        <!--</xsl:result-document>-->
    </xsl:template>
    
</xsl:stylesheet>