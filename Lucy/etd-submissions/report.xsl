<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:variable name="result-doc">
        <xsl:copy-of select="doc('sorted-solr-results.xml')/response/result"/>
    </xsl:variable>
    
    <xsl:template match="/">
        <xsl:result-document href="students.xml">
        <students>
            <xsl:apply-templates select="//doc[str[@name='r.isofetdtype.pid'] eq 'uk-ac-man-etdtype:1']"/>
        </students>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="doc">
        <xsl:variable name="student-pid" select="str[@name='r.isbelongstostudent.pid']"/>

        <xsl:choose>
            <xsl:when test="not(exists($result-doc/result/doc[(str[@name='r.isbelongstostudent.pid'] eq $student-pid) and (str[@name='r.isofetdtype.pid'] eq 'uk-ac-man-etdtype:3') and (str[@name='r.isofetdtype.pid'] eq 'uk-ac-man-etdtype:4')]))">
                <student>
                    <studentPID><xsl:value-of select="str[@name='r.isbelongstostudent.pid']"/></studentPID>
                    <studentName><xsl:value-of select="str[@name='e.displayname']"/></studentName>
                    <studentSpotID><xsl:value-of select="str[@name='e.partynumber']"/></studentSpotID>
                    <etdPID><xsl:value-of select="str[@name='PID']"/></etdPID>
                    <createdDate><xsl:value-of select="date[@name='x.createddate']"/></createdDate>
                    <openDate><xsl:value-of select="str[@name='e.expectedopendate']"/></openDate>
                    <closeDate><xsl:value-of select="str[@name='e.expectedclosedate']"/></closeDate>
                    <submittedDate><xsl:value-of select="str[@name='e.date.submitted']"/></submittedDate>
                    <windowState><xsl:value-of select="str[@name='e.windowstate']"/></windowState>                    
                    <submissionState><xsl:value-of select="str[@name='e.submissionstate']"/></submissionState>
                    <creatorName><xsl:value-of select="str[@name='r.iscreatedby.displayname']"/></creatorName>
                </student>
            </xsl:when>
        </xsl:choose>
        
    </xsl:template>
    
</xsl:stylesheet>