# Misc XSLT Scripts

These are miscellaneous XSLT scripts created for various purposes over the years.  At the very least, they might be used as snippets and/or provide insight into XSLT development.  

## Contact  
Contact [Berrisford.Edwards@manchester.ac.uk](mailto:Berrisford.Edwards@manchester.ac.uk) for further info.  

## Contents Synopsis  
### anja  
This script operates upon MARC XML; it retrieves **distinct-values** based upon other filters.  It gets the first 2 distinct characters (digits) for each distinct subfield with attribute code='a', within a datafield with attribute  tag='082' within each record.  It is an example usage of:  
- *distinct-values()*  
- *variable passing*  
- *named template calling*  

### CARCANET_emails (defunct)  
These are the set of inter-related scripts that were used to retrieve all items (with attachments) related to the CARCANET (email) project.  It uses batch scripts to make *wget* calls and run xslt scripts. (*WGet* is therefore a dependency of this project.) Other batch scripts are generated and run to produce other results until the final output is produced and files retrieved.  These files were then ingested into the CARCANET project.  The instructions for this are found in **Notes.txt**.  

### eScholarJournalArticles (defunct)  
This is a set of scripts that update the number of Journals subscribed to in the (defunked) eScholar system.  It did this by retrieving the journals list from an online *Ulrichs* DB.  It uses a combination of batch scripts and xslt.  The complete instructions for updating the list of journals using these scripts are found in the **How to Update Ulrichs Journal Data.docx** document.  

### eThesisWindowDates  
This is a utility script used to maintain and update eThesis window close dates.  **This process is still active and relevant and is run when needed (see Nilani)**  
**NB:** The dates in the *select.xml* document need to be updated accordingly.  
The process is described thus (adjust directory/file locations accordingly):

1. Run the following query (adjust *e.expectedclosedate* dates accordingly)
[http://escholarprd.library.manchester.ac.uk:8085/solr/etd/select?q=e.expectedclosedate%3A%5B2019-09-25+TO+2019-09-29%5D+AND+e.submissionstate%3A+NO_SUBMISSION+AND+r.isofetdtype.pid%3Auk-ac-man-etdtype%5C%3A1&rows=100&fl=PID&wt=xml&indent=true](http://escholarprd.library.manchester.ac.uk:8085/solr/etd/select?q=e.expectedclosedate%3A%5B2019-09-25+TO+2019-09-29%5D+AND+e.submissionstate%3A+NO_SUBMISSION+AND+r.isofetdtype.pid%3Auk-ac-man-etdtype%5C%3A1&rows=100&fl=PID&wt=xml&indent=true)
2. Right-Click and save file as "D:\eThesisWindowDates\select.xml"  
3. Run "D:\eThesisWindowDates\generateETDData.bat".  This will generate the file "D:\date-etddata.xml"  
4. Start the Tomcat 6 server  
5. In the browser run [http://localhost:8080/admin/updateETDs.do](http://localhost:8080/admin/updateETDs.do)  
6. Repeat daily as required.  

### Google-Scholar-Indexing (defunct)  
This script generates a Google crawl file, allowing for Google Scholar indexing (defunct).  

### grantsAndJournals2xml (defunct)  
These scripts convert grants and journal xml data retrieved from eSholar solr into another ingestable xml format.  

### granular_permissions (defunct)  
These are the scripts that reglate the granular user permissions enforced in eScholar (defunct).  

### GuideToSpecialCollections  
These are the scripts related to converting Guide to special collections items exported from eMu into another format fit for ingest into T4. It relates to the new Guide to Special Collections A-Z.  The instructions for use are found in the **Template/NarrativeTemplate(HTML5).html** document.  

### HelenReport  
This is a script that selects items without authors.  The output is then imported into Excel for ease of use.  

### JanW  
This script converts xml from Preservica into DC format.  
It is also the xslt that transforms luna data into a formats that can be viewed and edited in the Preservica viewer/editor. It exemplifies:  
1. Maintaining document order  
2. recursion  
3. variable passing  
4. generating unique, dynamic identifiers

### Leanda  
This script converts solr data (from eScholar) into another format that is imported into Excel to be more human readable.  It is an example of:  
- using the *position()* function to create an arbitrary number of unique nodes that are easily imported and viewed in Excel.  

### Lucy  
These are scripts that convert solr data (eScholar) into other formats which are imported into Excel.  It exemplifies:  
1. *position()* to create unique nodes
2. *count()*  
3. *doc()* to load dynamic responses into a variable  

### MACE_migration  
These scripts generate *foxml* from item publication data.  This was used to migrate MACE items to another system.  It exemplifies:
1. dynamic variable generation  
2. foxml and it's generation 
3. using *result-document* to push xsl output into dynamically created documents  
4. parameter passing to templates  
5. string manipulation  
6.  *current-dateTime()* and date manipulation  
7. casting 1 type to another     

### nilanis  
Miscellaneous scripts created for Nilani over the years...
#### character-encoding-fix  
A script that applies nested string replace manipulation to a fixed set Extended ASCII (utf8) characters.  

#### createStudentThesis  
Creates xml output in a desired format from solr (eScholar) data.  Exemplifies:  
1. including other (local) files in a script.  
2. *xsl:strip-space*  
3. *xsl:preserve-space*  

#### CRIS_Activity-Elsevier  
Simple transformation from one xml format to another.  

#### distinct-items  
An example use of *distinct-values* to return distinct values.  

#### filter-by-date  
Transform exemplifying:  
1. using *position()* in test clauses to test the location of an element within it parent container
2. use of *last()* to refer to last child  
3. strong typing e.g. *as="xs:date"*  

#### generate-iranian-pids  
Good simple example of **recursion**  

#### generateCoverPage  
Exemplifies use of *xsl fo* (formatting object) to generate document.  **Still relevant/active for creating eThesis coverpage**  

#### main-supervisor, multipleMainSupervisor  
This script iterates across all records (of a particular content type) in chunks of 1000 (rows) and checks to see if the record has been matched by Scopus. If not, it generates the requisite Pure xml entirely from eScolar data. Change $rows, $num_found, $obj_files accordingly to test quickly with small sets of data. Change $file_path to match where your files are located. Exemplifies:  
1. recursion  
2. importing local files  
3. generating arbitrary numbers of unique nodes (translating to multiple columns in Excel)  
4. importing xml into Excel  

#### randomXML  
Transform ememplifying:  
- Iterating across all (arbitrarily named) child elements in order  

#### sort-supervisor-files  
Transform exemplifying:  
1. outputing transform result into multiple output documents  
2. checking that a remote file exists before attemting to operate on it  

#### thesis-format-report  
Just an example of transforming and formatting data into another form.  In this case, it is imported into Excel to make it more human readable  

#### transform-talk-event  
Simple example of transforming xml input into another xml format  

### OAI-PMH_update (defunct)  
These are backups of the xsl transforms used within eScholar to transform (solr) data into formats suitable to the various harvesters (defunct)  

### PDFX  
The xsl transform used in the pdf upload utility  

### Primo  
An example of transforming retrieved data.  A blank 'dummy' file is used simply because xsl must have an xml input in order to execute  

### Pure_Migration, PureLegacyImport  
These are copies of the same set of files (unsure of what the differences are so kept both for completeness).  They are all the files/processes that were used and undertaken to migrate all eScholar data/records into the new CRIS system - Pure. The instructions/guidance for this is found in the **\PureLegacyImport\uom_output\handoverNotes.docx** document. The **\Pure_Migration\journal-article** folder is the best most definitive place to start as all scripts were developed here first for the *journal-article* content type then adapted (customised) to apply to each of the other content types.  It is a non-trivial solution employing multiple sequential steps.  

### scopusJournals  
This is an example of generating records from  results retrieved from Scopus.  Instructions are in the **sample-journal-articles\HowTo.txt** document  

### Scott  
This is another example of transforming solr data into another format, generating arbitrary unique nodes (converted to Excel columns) and displaying in Excel  

#### Altmetrics  
Some early provisional tests around Altmetrics data  

### SimonMealing  
This is all the information around retrieving and manipulating **Sentry** data.  It kicks off with the **getCSV.bat** file.  WGet is a dependency of this project. It exmplifies:  
1. Interaction with pre-constructed Alma reports  
2. Dynamic batch script generation and execution  

### SpringXSLT  
Simple script exemplifying generating etd window xml  

### transform-project  
This is the project generating all the files and foxml relating to the creation and ingesting of all the ***Iranian Newspapers***. The *xsl* folder contains all the (intuitively named) scripts and outputs relevant to each archive.  
