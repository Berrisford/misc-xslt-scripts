<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:commons="v3.commons.pure.atira.dk"
    xmlns:v1="v1.unified.clipping.pure.atira.dk" 
    xmlns:v3="v3.commons.pure.atira.dk"
    exclude-result-prefixes="xs math commons v1 v3"
    version="3.0"
    >
    
    
    <xsl:output method="xml" indent="yes" encoding="UTF-8" />
    
    <xsl:template match="/">
        <!--<xsl:result-document href="clippings_ingest.xml">-->
        <v1:clippings xmlns:v1="v1.unified.clipping.pure.atira.dk" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:commons="v3.commons.pure.atira.dk" xsi:schemaLocation="v1.unified.clipping.pure.atira.dk file:./unifiedClipping.xsd">                 
            <xsl:apply-templates select="/clippings/clipping"/>                
        </v1:clippings>        
        <!--</xsl:result-document>-->
    </xsl:template>
   
   <xsl:template match="clipping">
       <xsl:element name="v1:clipping">
            <xsl:attribute name="id" select="clipping_attr_id"/>
            <xsl:attribute name="type" select="replace(clipping_attr_type, ' ', '')"/>
            <xsl:attribute name="managedInPure" select="'false'"/>
            
           <v1:title>
               <xsl:choose>
                   <xsl:when test="string-length(clipping_title) &lt; 251">
                       <xsl:value-of select="clipping_title"/>                       
                   </xsl:when>
                   <xsl:otherwise>
                       <xsl:value-of select="concat(substring(clipping_title, 0, 250), '...')"/>                                              
                   </xsl:otherwise>
               </xsl:choose>
           </v1:title>

           <v1:description><xsl:value-of select="clipping_description"/></v1:description>
                   
           <v1:startDate><xsl:value-of select="format-date(clipping_start_date, '[D01]-[M01]-[Y0001]')"/></v1:startDate>

           <xsl:element name="v1:managedBy">
               <xsl:attribute name="lookupHint" select="'orgSync'"/>
               <xsl:attribute name="lookupId" select="clipping_managed_by"/>
           </xsl:element>
                                  
           <!--<v1:visibility>Public</v1:visibility>-->
           <v1:visibility>Restricted</v1:visibility>
           
           <xsl:element name="v1:mediaReferences">
               <xsl:element name="v1:mediaReference">
                   <xsl:attribute name="type" select="media_ref_attr_type"/>
                   <xsl:attribute name="id" select="clipping_attr_id"/>
                   <v1:title><xsl:value-of select="clipping_title"/></v1:title>
                   
                   <xsl:element name="v1:persons">
                       <xsl:call-template name="split-persons">
                           <xsl:with-param name="strIds" select="media_ref_person_attr_id"/>
                           <xsl:with-param name="org" select="clipping_managed_by"/>
                       </xsl:call-template>                                             
                   </xsl:element>
                   
                   <v1:country><xsl:value-of select="media_ref_country"/></v1:country>
                   <v1:date><xsl:value-of select="format-date(clipping_start_date, '[D01]-[M01]-[Y0001]')"/></v1:date>
                   <v1:url><xsl:value-of select="media_ref_url"/></v1:url>
                   <v1:medium><xsl:value-of select="media_ref_medium"/></v1:medium>
                   <v1:mediaType><xsl:value-of select="replace(media_ref_media_type, ' ', '')"/></v1:mediaType>
                   <v1:degreeOfRecognition><xsl:value-of select="replace(media_ref_degree_of_recognition, ' ', '')"/></v1:degreeOfRecognition>
                   <v1:author><xsl:value-of select="media_ref_author"/></v1:author>
                   <v1:extent><xsl:value-of select="media_ref_extent"/></v1:extent>                   
               </xsl:element>               
           </xsl:element>           
        </xsl:element>      
   </xsl:template>
    
   <xsl:template name="split-persons">
       <xsl:param name="strIds"/>
       <xsl:param name="org"/>
       <xsl:choose>
           <xsl:when test="contains($strIds, '; ')">
               <xsl:element name="v1:person">
                   <xsl:attribute name="id" select="substring-before($strIds, '; ')"/>
                   <xsl:element name="v1:person">
                       <xsl:attribute name="lookupId" select="substring-before($strIds, '; ')"/>
                       <xsl:attribute name="lookupHint" select="'personSync'"/>
                       <xsl:attribute name="origin" select="'internal'"/>
                   </xsl:element>
                   <xsl:element name="v1:organisations">
                       <xsl:element name="v1:organisation">
                           <xsl:attribute name="lookupId" select="$org"/>
                           <xsl:attribute name="lookupHint" select="'orgSync'"/>
                           <xsl:attribute name="origin" select="'internal'"/>
                       </xsl:element>
                   </xsl:element>
               </xsl:element>
               <xsl:call-template name="split-persons">
                   <xsl:with-param name="strIds" select="substring-after($strIds, '; ')"/>
                   <xsl:with-param name="org" select="$org"/>
               </xsl:call-template>               
           </xsl:when>
           <xsl:otherwise>
               <xsl:element name="v1:person">
                   <xsl:attribute name="id" select="$strIds"/>
                   <xsl:element name="v1:person">
                       <xsl:attribute name="lookupId" select="$strIds"/>
                       <xsl:attribute name="lookupHint" select="'personSync'"/>
                       <xsl:attribute name="origin" select="'internal'"/>
                   </xsl:element>
                   <xsl:element name="v1:organisations">
                       <xsl:element name="v1:organisation">
                           <xsl:attribute name="lookupId" select="$org"/>
                           <xsl:attribute name="lookupHint" select="'orgSync'"/>
                           <xsl:attribute name="origin" select="'internal'"/>
                       </xsl:element>
                   </xsl:element>
               </xsl:element>                             
           </xsl:otherwise>           
       </xsl:choose>
   </xsl:template>
   
</xsl:stylesheet>