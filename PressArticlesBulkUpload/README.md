# README #

## Overview ##
This documents the research impact project.  It transforms research impact data from an Excel spreadsheet to an xml format (defined by the impact schema - impact.xsd) that can then be bulk imported into Pure.

## Transformation Instructions  

1. Create intermediate mapping file:  Excel doesn't support mapping of complex nodes or mapping of attributes so a flat intermediate file MUST be created that maps from Excel, then the result of the XML output from Excel is then transformed to the final XML format defined by the "unifiedClipping.xsd" schema.  This only needs to be done once.  Guidelines for intermediate mapping file:  
	1. All nodes MUST be direct descendants of the root node  
	2. Convert ALL attributes to nodes  
	3. Create a sample xml document of the flattened structure  
	4. Using oXygen, generate a schema (.xsd) from the flattened xml document  
	5. In oXygen, use the schema editor to edit the schema to set the types, occurrences etc... to what they should be  
2. With spreadsheet:  
	1. Remove spaces in column names  
	2. (Optionally) Sort useful columns in the order in which they appear in the xml mapping file with these instructions: https://www.youtube.com/watch?v=tQSapXiW5mM  
	3. In the "Developer" tab, import the schema (.xsd) and map the column titles to the relevant nodes  
	6. Export the xml (https://www.youtube.com/watch?v=5nb78TdXM8k). Save the file with name "clippings_xls2xml.xml"  
3. Transform the output from the Excel spreadsheet using transformation xslt ("xlsx2clippingXml.xsl"). To do this, run the "xlsx2clippingXml.bat" script. 
4. In Pure: [https://test.pure.manchester.ac.uk/admin/workspace.xhtml](https://test.pure.manchester.ac.uk/admin/workspace.xhtml)    
	1. Upload the final xml ("clippings_ingest.xml") into Pure using "Bulk upload" thus:  
		1. Top Menu: "Administrator" tab  
		2. Left Menu:  "Bulk Import" > "Press/Media"
		3. Open Import Wizard > follow wizard instructions  
	2. Test the items using thus:  
		1. Paste the clippings ID of one of the ingested items into the search bar  
		2. If the item with that ID is returned, it was successfully ingested  
		
PS: To delete an existing item, simply search for the item (as above), click to select the item, then in the *bottom right* of the pop-up, there is a red "X".  Click it and confirm to delete the item.   		

## Contact: ##
[Berrisford.Edwards@manchester.ac.uk](mailto:Berrisford.Edwards@manchester.ac.uk) OR  
Nilani Ganeshwaran (DLDT Senior Developer)  [Nilani.Ganeshwaran@manchester.ac.uk](mailto:Nilani.Ganeshwaran@manchester.ac.uk)   