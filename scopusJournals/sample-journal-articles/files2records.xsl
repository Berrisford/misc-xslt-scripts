<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    xmlns:ait="http://www.elsevier.com/xml/ani/ait" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xoe="http://www.elsevier.com/xml/xoe/dtd" 
    xmlns:ce="http://www.elsevier.com/xml/ani/common" 
    xmlns:cto="http://www.elsevier.com/xml/cto/dtd" 
    xmlns:xocs="http://www.elsevier.com/xml/xocs/dtd"    
    version="3.0">
    <xsl:output indent="yes" method="xml"/>
    
    <xsl:template match="/">
        <records>
            <xsl:for-each select="files/file">
                <xsl:call-template name="file"/>
            </xsl:for-each>
        </records>
    </xsl:template>
    
    <xsl:template name="file">
        <xsl:variable name="pid" select="concat('uk-ac-man-scw:', substring-after(substring-before(., '\2-s2'), 'uk-ac-man-scw_'))"/>
        <xsl:variable name="doc_path" select="concat(replace($pid, ':', '_'), '/', '2-s2', substring-after(., '2-s2'))"></xsl:variable>
        <record>
            <pid><xsl:value-of select="concat('uk-ac-man-scw:', substring-after(substring-before(., '\2-s2'), 'uk-ac-man-scw_'))"></xsl:value-of></pid>
            <!--<path><xsl:value-of select="$doc_path"/></path>-->
            <title><xsl:value-of select="doc($doc_path)/xocs:doc/xocs:item/item/bibrecord/head/citation-title/titletext"/></title>
            <doi><xsl:value-of select="doc($doc_path)/xocs:doc/xocs:meta/xocs:doi"/></doi>
            <scopus-id><xsl:value-of select="doc($doc_path)/xocs:doc/xocs:meta/xocs:eid"/></scopus-id>
        </record>
    </xsl:template>
    
</xsl:stylesheet>