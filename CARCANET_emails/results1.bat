echo Start time: %DATE% %TIME%

mkdir uk-ac-man-emm_2012011001e6038
wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-emm_2012011001e6038\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emm:2012011001e6038/objectXML"
wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-emm_2012011001e6038\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emm:2012011001e6038/datastreams/RELS-EXT/content"
wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-emm_2012011001e6038\EMAIL.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emm:2012011001e6038/datastreams/EMAIL/content"
wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-emm_2012011001e6038\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emm:2012011001e6038/datastreams/PRESERVATION/content"
wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-emm_2012011001e6038\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emm:2012011001e6038/datastreams/EVENTLOG/content"
wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-emm_2012011001e6038\MHT.mht "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emm:2012011001e6038/datastreams/MHT/content"
wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-emm_2012011001e6038\EML.eml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emm:2012011001e6038/datastreams/EML/content"
wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-emm_2012011001e6038\MSG.msg "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emm:2012011001e6038/datastreams/MSG/content"
wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-emm_2012011001e6038\XML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emm:2012011001e6038/datastreams/XML/content"
wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-emm_2012011001e6038\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emm:2012011001e6038/datastreams/DC/content"
wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=250k --output-document="uk-ac-man-emm_2012011001e6038\Raphael blurb 16.1.04.doc" "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emm:2012011001e6038/datastreams/FILE_2012011001e6038_1/content"

echo End time: %DATE% %TIME%

pause