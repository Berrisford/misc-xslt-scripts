echo Start time: %DATE% %TIME%

mkdir uk-ac-man-col_cpa

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-col:cpa/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-col:cpa/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-col:cpa/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-col:cpa/datastreams/DC/content"


::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012010

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012010/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012010/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012010/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012010/datastreams/DC/content"


mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/datastreams/FITS/content"

::wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/datastreams/PST/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/datastreams/FITS/content"

::wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/datastreams/PST/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/datastreams/FITS/content"

::wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/datastreams/PST/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/datastreams/FITS/content"

::wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/datastreams/PST/content"



::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012011

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012011/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012011/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012011/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012011/datastreams/DC/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/datastreams/FITS/content"

::wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/datastreams/PST/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/datastreams/FITS/content"

::wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/datastreams/PST/content"


:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2013032/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2013032/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2013032/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012010/datastreams/DC/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/datastreams/FITS/content"

::wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/datastreams/PST/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/datastreams/FITS/content"

::wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/datastreams/PST/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/datastreams/FITS/content"

::wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/datastreams/PST/content"


echo End time: %DATE% %TIME%

pause