echo Start time: %DATE% %TIME%

mkdir uk-ac-man-col_cpa

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-col:cpa/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-col:cpa/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-col:cpa/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-col:cpa/datastreams/DC/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012010

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012010/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012010/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012010/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012010/datastreams/DC/content"


mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/datastreams/FITS/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010001/datastreams/PST/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\uk-ac-man-emf_2012010001f16

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\uk-ac-man-emf_2012010001f16\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010001f16/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\uk-ac-man-emf_2012010001f16\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010001f16/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\uk-ac-man-emf_2012010001f16\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010001f16/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\uk-ac-man-emf_2012010001f16\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010001f16/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\uk-ac-man-emf_2012010001f84

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\uk-ac-man-emf_2012010001f84\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010001f84/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\uk-ac-man-emf_2012010001f84\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010001f84/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\uk-ac-man-emf_2012010001f84\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010001f84/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\uk-ac-man-emf_2012010001f84\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010001f84/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\uk-ac-man-emf_2012010001f217

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\uk-ac-man-emf_2012010001f217\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010001f217/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\uk-ac-man-emf_2012010001f217\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010001f217/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\uk-ac-man-emf_2012010001f217\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010001f217/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010001\uk-ac-man-emf_2012010001f217\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010001f217/datastreams/EMAILFOLDER/content"




mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/datastreams/FITS/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010002/datastreams/PST/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\uk-ac-man-emf_2012010002f9

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\uk-ac-man-emf_2012010002f9\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010002f9/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\uk-ac-man-emf_2012010002f9\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010002f9/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\uk-ac-man-emf_2012010002f9\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010002f9/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\uk-ac-man-emf_2012010002f9\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010002f9/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\uk-ac-man-emf_2012010002f20

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\uk-ac-man-emf_2012010002f20\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010002f20/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\uk-ac-man-emf_2012010002f20\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010002f20/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\uk-ac-man-emf_2012010002f20\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010002f20/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010002\uk-ac-man-emf_2012010002f20\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010002f20/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/datastreams/FITS/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010003/datastreams/PST/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\uk-ac-man-emf_2012010003f1

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\uk-ac-man-emf_2012010003f1\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010003f1/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\uk-ac-man-emf_2012010003f1\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010003f1/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\uk-ac-man-emf_2012010003f1\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010003f1/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010003\uk-ac-man-emf_2012010003f1\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010003f1/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/datastreams/FITS/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012010004/datastreams/PST/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\uk-ac-man-emf_2012010004f48

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\uk-ac-man-emf_2012010004f48\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010004f48/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\uk-ac-man-emf_2012010004f48\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010004f48/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\uk-ac-man-emf_2012010004f48\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010004f48/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012010\uk-ac-man-ems_2012010004\uk-ac-man-emf_2012010004f48\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012010004f48/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012011

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012011/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012011/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012011/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012011/datastreams/DC/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/datastreams/FITS/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011001/datastreams/PST/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f5

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f5\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f5/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f5\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f5/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f5\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f5/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f5\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f5/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f57

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f57\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f57/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f57\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f57/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f57\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f57/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f57\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f57/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f66

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f66\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f66/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f66\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f66/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f66\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f66/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f66\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f66/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f85

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f85\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f85/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f85\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f85/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f85\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f85/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f85\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f85/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f112

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f112\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f112/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f112\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f112/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f112\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f112/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f112\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f112/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f119

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f119\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f119/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f119\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f119/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f119\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f119/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011001\uk-ac-man-emf_2012011001f119\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011001f119/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/datastreams/FITS/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2012011002/datastreams/PST/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f18

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f18\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011002f18/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f18\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011002f18/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f18\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011002f18/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f18\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011002f18/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f153

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f153\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011002f153/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f153\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011002f153/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f153\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011002f153/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f153\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011002f153/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f181

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f181\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011002f181/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f181\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011002f181/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f181\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011002f181/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f181\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011002f181/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f186

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f186\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011002f186/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f186\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011002f186/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f186\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011002f186/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2012011\uk-ac-man-ems_2012011002\uk-ac-man-emf_2012011002f186\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2012011002f186/datastreams/EMAILFOLDER/content"




mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2013032/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2013032/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2013032/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ema:2012010/datastreams/DC/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/datastreams/FITS/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032001/datastreams/PST/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\uk-ac-man-emf_2013032001f19

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\uk-ac-man-emf_2013032001f19\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032001f19/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\uk-ac-man-emf_2013032001f19\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032001f19/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\uk-ac-man-emf_2013032001f19\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032001f19/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\uk-ac-man-emf_2013032001f19\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032001f19/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\uk-ac-man-emf_2013032001f29

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\uk-ac-man-emf_2013032001f29\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032001f29/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\uk-ac-man-emf_2013032001f29\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032001f29/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\uk-ac-man-emf_2013032001f29\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032001f29/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\uk-ac-man-emf_2013032001f29\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032001f29/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\uk-ac-man-emf_2013032001f51

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\uk-ac-man-emf_2013032001f51\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032001f51/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\uk-ac-man-emf_2013032001f51\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032001f51/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\uk-ac-man-emf_2013032001f51\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032001f51/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032001\uk-ac-man-emf_2013032001f51\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032001f51/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/datastreams/FITS/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032002/datastreams/PST/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f86

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f86\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032002f86/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f86\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032002f86/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f86\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032002f86/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f86\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032002f86/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f94

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f94\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032002f94/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f94\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032002f94/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f94\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032002f94/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f94\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032002f94/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f107

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f107\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032002f107/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f107\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032002f107/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f107\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032002f107/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f107\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032002f107/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f113

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f113\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032002f113/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f113\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032002f113/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f113\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032002f113/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032002\uk-ac-man-emf_2013032002f113\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032002f113/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\EAD.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/datastreams/EAD/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\PSTR.html "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/datastreams/PSTR/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\PRESERVATION.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/datastreams/PRESERVATION/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\EVENTLOG.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/datastreams/EVENTLOG/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\FITS.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/datastreams/FITS/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\PST.pst "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-ems:2013032003/datastreams/PST/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\uk-ac-man-emf_2013032003f26

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\uk-ac-man-emf_2013032003f26\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032003f26/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\uk-ac-man-emf_2013032003f26\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032003f26/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\uk-ac-man-emf_2013032003f26\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032003f26/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\uk-ac-man-emf_2013032003f26\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032003f26/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\uk-ac-man-emf_2013032003f147

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\uk-ac-man-emf_2013032003f147\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032003f147/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\uk-ac-man-emf_2013032003f147\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032003f147/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\uk-ac-man-emf_2013032003f147\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032003f147/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\uk-ac-man-emf_2013032003f147\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032003f147/datastreams/EMAILFOLDER/content"



mkdir uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\uk-ac-man-emf_2013032003f273

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\uk-ac-man-emf_2013032003f273\FOXML.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032003f273/objectXML"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\uk-ac-man-emf_2013032003f273\RELS-EXT.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032003f273/datastreams/RELS-EXT/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\uk-ac-man-emf_2013032003f273\DC.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032003f273/datastreams/DC/content"

wget --http-user=fedoraAdmin --http-password=bluebanana --limit-rate=500k --output-document=uk-ac-man-col_cpa\uk-ac-man-ema_2013032\uk-ac-man-ems_2013032003\uk-ac-man-emf_2013032003f273\EMAILFOLDER.xml "http://escholarprd.library.manchester.ac.uk:8080/fedora/objects/uk-ac-man-emf:2013032003f273/datastreams/EMAILFOLDER/content"


echo End time: %DATE% %TIME%

pause